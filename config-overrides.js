const { override, addDecoratorsLegacy, fixBabelImports } = require('customize-cra');
const webpack = require('webpack');
const addLessLoader = require('customize-cra-less-loader');
const path = require('path');

const web3React = wp => config => {
  const fallback = config.resolve.fallback || {};
  Object.assign(fallback, {
    crypto: require.resolve('crypto-browserify'),
    stream: require.resolve('stream-browserify'),
    assert: require.resolve('assert'),
    http: require.resolve('stream-http'),
    https: require.resolve('https-browserify'),
    os: require.resolve('os-browserify'),
    url: require.resolve('url'),
    buffer: require.resolve('buffer'),
    zlib: false,
    fs: false,
    path: require.resolve('path-browserify'),
  });
  config.resolve.fallback = fallback;
  config.plugins = (config.plugins || []).concat([
    new wp.ProvidePlugin({
      process: 'process/browser',
      Buffer: ['buffer', 'Buffer'],
    }),
  ]);

  return config;
};

const ignoreWarnings = value => config => {
  config.ignoreWarnings = value;
  return config;
};

module.exports = {
  webpack: override(
    fixBabelImports('import', {
      libraryName: 'antd',
      libraryDirectory: 'es',
      style: true,
    }),
    addLessLoader({
      lessLoaderOptions: {
        lessOptions: {
          javascriptEnabled: true,
          modifyVars: {},
        },
      },
      resolve: {
        alias: {
          '@': path.resolve(__dirname, 'src'),
        },
      },
    }),
    ignoreWarnings([/Failed to parse source map/]),
    addDecoratorsLegacy(),
    web3React(webpack),

    config => {
      config.resolve = {
        ...config.resolve,
        extensions: ['.js', '.jsx', '.json'],
        alias: { ...config.resolve.alias, '@': path.resolve(__dirname, 'src') },
      };
      return config;
    },
  ),
};
