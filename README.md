# MCC WEB USER Frontend

### Environment requirement

> Source config with create-react-app/react-app-rewired
>
> Operating System: `Window` or `Ubuntu 20.04.6 LTS`
>
> NodeJS: `v18.17.0`
>
> Yarn: `v1.22.19`

### Install Dependencies

> Install project dependencies: `yarn install`

### Development (For Developer)

> Create file .env look like .env.example
>
> Run: `yarn start`
>
> Open browser at: `http://localhost:3000`
>
> Please install flug-in eslint/prettier into your IDE for code convention auto checking
