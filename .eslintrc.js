module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: ['react-app'],
  overrides: [],
  plugins: ['react'],
  rules: {
    'comma-dangle': 'off',
    'react/display-name': 'off',
    'react/prop-types': 'off',
    'no-console': 'warn',
    'jsx-a11y/anchor-is-valid': 'off',
    semi: 'off',
    eqeqeq: 'off',
  },
  parserOptions: {
    parser: 'babel-eslint',
    ecmaVersion: 'latest',
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
      modules: true,
      experimentalObjectRestSpread: true,
    },
    requireConfigFile: false,
    project: ['./jsconfig.json'],
  },
};
