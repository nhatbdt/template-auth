// eslint-disable-next-line no-underscore-dangle
const Config = { ...window._CONFIG, ...process.env };

export const PAGINATION_PAGE_SIZE = 12;

export default Config;
