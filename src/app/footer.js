import { Component } from 'react';
// import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link, withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';
import Typography from '../components/typography';
import Media from '../utils/media';
import { COLOR_TWITTER_ICON, COLOR_TELEGRAM_ICON } from '../theme/images';
import { Colors } from '../theme';

const FooterSt = styled.footer`
  z-index: 2;
  width: 100%;
  display: flex;
  background: #040405;
  box-shadow: 0px -2px 4px rgba(0, 0, 0, 0.05);
  /* height: 90px; */
  height: 60px;
  align-items: center;
  /* padding: 30px 60px; */
  padding: 0 60px;
  justify-content: space-between;
  p,
  a {
    color: ${Colors.TEXT};
  }

  .col_left {
    display: flex;
    align-items: center;

    .menu_link {
      display: flex;
      align-items: center;
      .link_item {
        padding: 0px 15px;
        font-weight: 400;
        font-size: 16px;
      }

      .link_item:first-child {
        position: relative;
        &::after {
          position: absolute;
          content: '';
          width: 4px;
          height: 4px;
          right: 0;
          top: 50%;
          transform: translateY(-50%);
          background-color: #282828;
          border-radius: 4px;
        }
      }
    }
  }

  .col_right {
    .twitter_icon {
      margin-right: 15px;
    }
  }

  .copyright-text {
    padding-right: 15px;
    position: relative;
    border-right: 1px solid rgba(40, 40, 40, 0.2);

    &::before {
      content: '';
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    display: none;
  }
`;
@withRouter
@inject(stores => ({}))
@observer
@withTranslation('common')
class Footer extends Component {
  render() {
    const { t } = this.props;

    // eslint-disable-next-line no-unused-vars
    const MENU_FOOTER = [
      {
        name: t('footer.nft_items'),
        target: '/nft-items',
        isNftItem: true,
      },
      {
        name: t('footer.faq'),
        target: '/faqs',
      },
      {
        name: t('footer.guide'),
        target: '/guide',
      },
    ];

    const MENU_FOOTER_BOTTOM = [
      {
        name: t('footer.about'),
        target: '/about-us',
      },
      {
        name: t('footer.service_term'),
        target: '/service-terms',
      },
      {
        name: t('footer.privacy'),
        target: '/privacy-agreement',
      },
      {
        name: t('footer.faq'),
        target: '/faqs',
      },
      {
        name: t('footer.contact'),
        target: '/contact',
      },
    ];

    return (
      <FooterSt>
        <div className="col_left">
          <Typography className="large copyright-text">{t('footer.copy_right')}</Typography>
          <div className="menu_link">
            {MENU_FOOTER_BOTTOM.map((item, index) => (
              <Typography key={index} className="link_item">
                <Link to={item.target}>{item.name}</Link>
              </Typography>
            ))}
          </div>
        </div>
        <div className="col_right">
          {/* <a
            href='https://twitter.com/primez_jp?s=21&t=BzSy7y26ej_bQ6xueMNAYg'
            target='_blank'
            className='twitter_icon'
          > */}
          <span
            style={{
              marginRight: '10px',
            }}
          >
            <img src={COLOR_TWITTER_ICON} alt="icon twitter" />
          </span>
          {/* </a> */}
          <span>
            <img src={COLOR_TELEGRAM_ICON} alt="icon telegram" />
          </span>
        </div>
      </FooterSt>
    );
  }
}

export default Footer;
