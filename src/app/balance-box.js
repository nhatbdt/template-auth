import React from 'react';
import useBalance from '../hooks/useTokenBalance';
import Typography from '../components/typography';
import Format from '../utils/format';
import { inject, observer } from 'mobx-react';

function BalanceBox({ account, currentChainId }) {
  const { balance, currency } = useBalance({ account, currentChainId });
  return (
    <>
      <Typography size="huge" bold>
        {Format.price(balance)}
      </Typography>
      <Typography bold>{currency}</Typography>
    </>
  );
}

// export default BalanceBox;
export default inject(stores => ({
  authStore: stores.auth,
}))(
  observer(({ authStore, ...props }) => {
    return (
      <BalanceBox
        {...props}
        account={authStore?.initialData?.publicAddress}
        currentChainId={authStore.currentChainId}
      />
    );
  }),
);
