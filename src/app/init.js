import { Component } from 'react'
import PropTypes from 'prop-types'
import * as yup from 'yup';
import { inject } from 'mobx-react'

import Request from '../utils/request'
import Storage from '../utils/storage'
import authStore from '../store/auth'
import Web3 from '../utils/web3other'
import i18n from '../translations/i18n'
import { logout } from '../utils/auth'
import Misc from '../utils/misc'

yup.setLocale({
  mixed: {
    required: 'required'
  },
  string: {
    email: 'email'
  }
})

@inject((stores) => ({
  productsStore: stores.products
}))
class Init extends Component {
  static propTypes = {
    productsStore: PropTypes.object
  }

  state = {
    inited: false
  }

  async componentDidMount() {
    const { productsStore } = this.props
    const token = Storage.get('ACCESS_TOKEN')
    const userId = Storage.get('USER_ID')

    const web3Info = await Web3?.getWeb3Instance()
    let web3
    let accounts
    
    if (web3Info) {
      web3 = web3Info?.web3
      if (!Misc.isMobile) {
        accounts = await web3?.eth?.getAccounts()
        if (accounts?.length > 0) {
          await web3?.currentProvider?.enable()
        }
      }

      if (token && accounts?.length <= 0) {
        logout()
      }

      if (!Misc.isMobile) {
        web3?.currentProvider?.on('accountsChanged', () => {
          this._handleLogout()
        })

        web3?.currentProvider?.on('chainChanged', () => {
          this._handleLogout()
        })

        web3?.currentProvider?.on('disconnect', () => {
          this._handleLogout()
        })
      }
    }

    if (window.location.pathname.slice(0, 3) === '/en') {
      i18n.changeLanguage('en')
    }

    const priceRateResult = await productsStore.getPriceRate()
    
    if (priceRateResult.success) {
      const usdToJpy = +priceRateResult.data.find((item) => item.name === 'USD_TO_JPY').value
      const gokuToJpy = +priceRateResult.data.find((item) => item.name === 'GOKU_TO_JPY').value
      const ethToNbng = +priceRateResult.data.find((item) => item.name === 'ETH_TO_NBNG').value
      const ethToGoku = +priceRateResult.data.find((item) => item.name === 'ETH_TO_GOKU').value
      const nbngToGoku = +priceRateResult.data.find((item) => item.name === 'NBNG_TO_GOKU').value

      authStore.setInitialData({
        userId,
        usdToJpy,
        gokuToJpy,
        ethToNbng,
        ethToGoku,
        nbngToGoku
      })
    }

    if (token) {
      Request.setAccessToken(token)
      authStore.setLoggedIn(true)
      await authStore.getInitialData()
    }

    this.setState({ inited: true })

    // this._hidePreloading()

    if (!token) return

    try {
      // if(Misc.isMobile) {
      await web3?.currentProvider?.enable()
      // }
      const account = await web3.eth.getAccounts()

      if (
        token
        && account[0]
      ) {
        authStore.setInitialData({
          publicAddress: account[0],
          userId
        })
        if (Misc.isMobile) {
          web3?.currentProvider?.on('accountsChanged', () => {
            
            this._handleLogout()
          })

          web3?.currentProvider?.on('chainChanged', () => {
            this._handleLogout()
          })

          web3?.currentProvider?.on('disconnect', () => {
            this._handleLogout()
          })
        }
        const { nbng, eth } = await Web3.getBalance(account[0])

        authStore.setNbngBalance(nbng || 0)
        authStore.setBalance({
          eth: eth || 0
        })
      }
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e)
    }
  }

  _handleLogout = () => {
    const tokenChange = Storage.get('ACCESS_TOKEN')
    const userIdChange = Storage.get('USER_ID')
    if (tokenChange && userIdChange) {
      logout(true)
    }
  }

  // _hidePreloading() {
  //   const preloading = document.getElementsByClassName('preloading')[0]

  //   const fadeEffect = setInterval(() => {
  //     if (!preloading.style.opacity) {
  //       preloading.style.opacity = 1
  //     }
  //     if (preloading.style.opacity === '1') {
  //       preloading.style.opacity = 0
  //     } else {
  //       clearInterval(fadeEffect)
  //       preloading.style.display = 'none'
  //     }
  //   }, 500)
  // }

  render() {
    const { children } = this.props
    const { inited } = this.state

    return inited ? children : null
  }
}

export default Init
