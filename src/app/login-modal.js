import { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { inject, observer } from 'mobx-react'
import { Translation } from 'react-i18next'

import Modal from '../components/modal'
import Typography from '../components/typography'
import Clickable from '../components/clickable'
import { Images } from '../theme'
import Web3 from '../utils/web3other'
import Toast from '../components/toast'
import Storage from '../utils/storage'
import Request from '../utils/request'
import Misc from '../utils/misc'

const StyledDiv = styled.div`
  padding: 17px 10px 0;

  .title {
    font-size: 22px;
    margin-bottom: 24px;
  }

  .subtitle {
    margin-bottom: 37px;
  }

  .title-2 {
    margin-bottom: 17px;
  }

  .login-button {
    height: 64px;
    border-radius: 8px;
    display: flex;
    justify-content: space-between;
    padding: 0 25px;
    color: white;
    background: rgb(18, 22, 28);
    background: linear-gradient(90deg, rgba(18, 22, 28, 1) 0%, rgba(56, 38, 112, 1) 100%);
    align-items: center;

    img {
      width: 34px;
    }

    &.metamask {
      background: rgb(245, 132, 31);
      background: linear-gradient(90deg, rgba(245, 132, 31, 1) 0%, rgba(255, 244, 113, 1) 100%);
      margin-bottom: 42px;
    }

    &.walletconnect {
      background: rgb(245, 132, 31);
      background: linear-gradient(90deg, rgb(106, 163, 208) 0%, rgb(208, 227, 239) 100%);
      margin-bottom: 42px;
    }
  }

  .faq-link {
    margin-top: 7px;
  }
`
export const WALLET_LIST = [
  {
    name: 'Metamask',
    link: 'https://metamask.app.link'
  },
  {
    name: 'Trust',
    link: 'https://link.trustwallet.com/open_url?coin_id=60&url='
  }
]

let instance

@inject((stores) => ({
  authStore: stores.auth
}))
@observer
class LoginModal extends Component {
  static propTypes = {
    authStore: PropTypes.object
  }

  state = {
    isOpen: false,
    loading: null
  }

  static setInstance = (ref) => {
    instance = ref
  }

  static open = () => {
    if (instance) {
      return instance.open()
    }

    return null
  }

  open = () => {
    this.setState({
      isOpen: true
    })
  }

  close = () => this.setState({ isOpen: false })

  _generateDappUrl = ({ link, name }) => {
    if (link) {
      if (name === 'Metamask') {
        const dappUrl = `${window.location.href}`.trim()
        if (dappUrl.search('https://') !== -1) {
          const pageUrl = `${link}/dapp/${dappUrl.replace('https://', '')}`
          window.location.href = pageUrl
        }
      }
    }
  };

  _onLogin = async (type, t) => {
    const { authStore } = this.props
    const { loading } = this.state

    if (loading) return null

    try {
      const { web3 } = await Web3.getWeb3Instance()

      let publicAddress

      if (!window.ethereum && Misc.isMobile) {
        this._generateDappUrl(WALLET_LIST[0])
      }
      
      const [account] = await web3.currentProvider.enable()

      const isNetworkValid = await Web3.checkValidNetwork()

      if (!isNetworkValid) {
        // eslint-disable-next-line no-throw-literal
        throw { error: 'ERROR_NETWORK_INCORRECT' }
      }
      publicAddress = account

      this.setState({
        loading: type
      })

      const loginResult = await authStore.login({
        publicAddress
      })
      const {  eth } = await Web3.getBalance(publicAddress)
      if (loginResult.success) {
        Storage.set('ACCESS_TOKEN', loginResult.data.accessToken)
        Storage.set('USER_ID', loginResult.data.userId)
        Request.setAccessToken(loginResult.data.accessToken)

        authStore.setLoggedIn(true)

        authStore.setInitialData({
          ethBalance: eth,
          nbngBalance: 0,
          publicAddress,
          userId: loginResult.data.userId
        })
        await authStore.getInitialData()

        this.close()
      }
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e)
      if (Misc.isMobile) {
        Web3.removeWeb3Instance()
      }

      if (e.error === 'ERROR_NETWORK_INCORRECT') {
        Toast.error(t('error-messages:NETWORK_INCORRECT'))
      } else {
        Toast.error(t('login.messages.cannot_login'))
      }
      Web3.setupNetwork()
    }

    return this.setState({
      loading: null
    })
  }

  render() {
    const { isOpen, loading } = this.state

    return (
      <Modal
        open={isOpen}
        onCancel={this.close}
        destroyOnClose
      >
        <Translation ns="auth">
          {(t) => (
            <StyledDiv>
              <Typography className="title" bold center>
                {t('login.title')}
              </Typography>
              <Typography className="subtitle" bold center secondary size="small">
                {t('login.subtitle_1')}
                <br />
                {t('login.subtitle_2')}
              </Typography>
              <Typography bold center size="large" className="title-2">
                {t('login.select')}
              </Typography>
              {Misc.isMobile && !window.ethereum ? (
                <Clickable
                  className="login-button walletconnect"
                  onClick={() => this._onLogin('wallet_connect', t)}
                >
                  <Typography size="large" bold>
                    WalletConnect{loading === 'wallet_connect' ? ': Logged in...' : ''}
                  </Typography>
                  <img src={Images.COLOR_WALLET_CONNECT_ICON} alt="" />
                </Clickable>
              ) : Misc.isMobile && window.ethereum ? (
                <Clickable
                  className="login-button walletconnect"
                  onClick={() => this._onLogin('meta_mask', t)}
                >
                  <Typography size="large" bold>
                    WalletConnect{loading === 'wallet_connect' ? ': Logged in...' : ''}
                  </Typography>
                  <img src={Images.COLOR_WALLET_CONNECT_ICON} alt="" />
                </Clickable>
              ) : (
                <Clickable
                  className="login-button metamask"
                  onClick={() => this._onLogin('meta_mask', t)}
                >
                  <Typography size="large" bold>MetaMask{loading === 'meta_mask' ? ': Logged in...' : ''}</Typography>
                  <img src={Images.COLOR_METAMASK_ICON} alt="" />
                </Clickable>
              )}
              <Typography center size="small" secondary>
                {t('login.problem')}
              </Typography>
              <Typography
                center
                size="small"
                underline
                link
                className="faq-link"
                onClick={() => window.open('https://rakuza.io/guide', '_self')}
              >
                {t('login.faq')}
              </Typography>
            </StyledDiv>
          )}
        </Translation>
      </Modal>
    )
  }
}

export default LoginModal
