import React, { Component, Suspense, lazy } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import styled from 'styled-components';

import Request from '../utils/request';
import Loading from '../components/loading';
import Page from '../components/page';
import ScrollToTop from '../utils/scrollToTop';
import Header from './header';

const HomeScreen = lazy(() => import('../screen/home'));
const ProductDetails = lazy(() => import('../screen/product-details'));

// const Home = lazy(() => import('../pages/home'));
// const Categories = lazy(() => import('../pages/categories'));
// const Products = lazy(() => import('../pages/products'));
// const ProductDetails = lazy(() => import('../pages/product-details'));

const UserProfile = lazy(() => import('../pages/user-profile'));
const Settings = lazy(() => import('../pages/settings'));
const NotFound = lazy(() => import('../pages/not-found'));
const ContactForm = lazy(() => import('../pages/user-profile/contact-form'));

const VerticalBox = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
`;

const PrivateRoute = ({ condition, redirect, ...props }) => {
  condition = condition();

  if (condition) return <Route {...props} />;
  return <Redirect to={redirect} />;
};
PrivateRoute.propTypes = {
  condition: PropTypes.func,
  redirect: PropTypes.string,
};

@withRouter
class Routes extends Component {
  static propTypes = {
    location: PropTypes.object,
    history: PropTypes.object,
  };

  _renderLazyComponent = (LazyComponent, params) => props => <LazyComponent {...props} {...params} />;

  render() {
    const currentLang = localStorage.getItem('currentLang');
    
    if (!currentLang) {
      localStorage.setItem('currentLang', 'ja');
    }
    return (
      <VerticalBox>
        <Header />
        <Suspense
          fallback={
            <Page>
              <Loading size="large" />
            </Page>
          }
        >
          <ScrollToTop>
            <Switch>
              <Route exact path="/:lang/" component={this._renderLazyComponent(HomeScreen)} />
              <Route
                exact
                path="/:lang/product-details/:id"
                component={this._renderLazyComponent(ProductDetails)}
              />
              {/* <Route exact path="/:lang/categories" component={this._renderLazyComponent(Categories)} />
              <Route
                exact
                path="/:lang/products/:sortBy/:categoryId/:currency/:status"
                component={this._renderLazyComponent(Products)}
              />
              <Route exact path="/:lang/products" component={this._renderLazyComponent(Products)} />
              <Route exact path="/:lang/products/category/:id" component={this._renderLazyComponent(Products)} />

              <Route exact path="/:lang/products/key-word/:query" component={this._renderLazyComponent(Products)} />
              <Route exact path="/:lang/product-details/:id" component={this._renderLazyComponent(ProductDetails)} />
              <Route
                exact
                path="/:lang/product-details/:id/:sellAction"
                component={this._renderLazyComponent(ProductDetails)}
              /> */}

              <PrivateRoute
                exact
                condition={() => Request.getAccessToken()}
                redirect="/ja"
                path="/:lang/settings"
                component={this._renderLazyComponent(Settings)}
              />
              <PrivateRoute
                exact
                condition={() => Request.getAccessToken()}
                redirect="/ja"
                path="/:lang/my-page"
                component={this._renderLazyComponent(UserProfile)}
              />
              <PrivateRoute
                exact
                condition={() => Request.getAccessToken()}
                redirect="/ja"
                path="/:lang/my-page/:tab"
                component={this._renderLazyComponent(UserProfile)}
              />

              <Route path="/:lang/not-found" component={this._renderLazyComponent(NotFound)} />
              <PrivateRoute
                exact
                condition={() => Request.getAccessToken()}
                redirect="/ja"
                path="/:lang/contact-form"
                component={this._renderLazyComponent(ContactForm)}
              />
              <Redirect to="/ja/" />
            </Switch>
          </ScrollToTop>
        </Suspense>
      </VerticalBox>
    );
  }
}

export default Routes;
