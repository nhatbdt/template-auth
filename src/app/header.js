import { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { inject, observer } from 'mobx-react'
import lodash from 'lodash'
import { withRouter } from 'react-router-dom'
import { withTranslation } from 'react-i18next'
import { Menu, Dropdown, Popover } from 'antd'

import Button from '../components/button'
import Typography from '../components/typography'
import Clickable from '../components/clickable'
import Thumbnail from '../components/thumbnail'
import Input from '../components/input'
import { Images, Colors } from '../theme'
import Misc from '../utils/misc'
import Format from '../utils/format'
import Media from '../utils/media'
import { events } from '../services'
import { logout } from '../utils/auth'
import LoginModal from './login-modal'

const HeaderContainer = styled.header`
  width: 100%;
  height: 152px;
  padding: 0 52px;
  z-index: 2;
  display: flex;
  position: fixed;
  z-index: 9;
  background: #ffffff;

  .left-box {
    flex: 1;
    display: flex;
    align-items: center;

    .logo {
      margin-right: 34px;

      img {
        width: 140px;
      }
    }

    .menu-button {
      margin-right: 15px;

      img {
        width: 20px;
      }
    }

    .heat-up-button {
      display: flex;
      align-items: center;

      img {
        margin-right: 16px;
      }

      p {
        color: ${Colors.PRIMARY};
      }
    }

    .language-box {
      display: flex;
      align-items: center;
      margin-right: 15px;

      .flag-icon {
        width: 20px;
        box-shadow: 0px 0px 3px 1px #00000021;
        margin-right: 5px;
        min-width: 0;
        margin-top: 2px;
      }

      .arrow-icon {
        width: 10px;
        min-width: 0;
        margin-left: 5px;
      }
    }
  }

  .center-box {
    width: 600px;
    display: flex;
    align-items: center;
    margin: 0 20px;
    
    .search-box {
      width: 100%;
      display: flex;
      background-color: rgba(51,51,51,0.05);
      padding: 3px;
      height: 57px;
      border-radius: 30px;
      overflow: hidden;
      
      .search-box-type {
        background-color: white;
        width: 170px;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 30px 0 0 30px;
        
        img {
          opacity: 0.7;
          margin-left: 10px;
        }
      }
      
      .search-input {
        height: auto;
        box-shadow: none;
        border-radius: 0;
        background-color: transparent;
        
        .search-icon {
          width: 13px;
        }

        .ant-input-suffix {
          .ant-input-clear-icon {
            width: 20px;
            height: 20px;
            background-image: url("${Images.BLACK_CLOSE_ICON}");
            background-repeat: no-repeat;
            background-position: center;

            svg {
              display: none;
            }
          }
        }
      }
    }
    
  }

  .right-box {
    flex: 1;
    display: flex;
    justify-content: flex-end;
    align-items: center;

    .currency-item {
      background-color: red;
    }

    .price-unit-box {
      margin-right: 37px;
      display: flex;

      img {
        margin-left: 17px;
      }

      ._label {
        margin-bottom: -3px;
        margin-top: 3px;
      }
    }

    .login-button {
      padding: 0 55px;
    }

    .user-box {
      display: flex;
      height: 57px;
      background-image: linear-gradient(99deg, #eed3ff 1%, #b9fdfd 95%);
      padding-left: 35px;
      padding-right: 4px;
      align-items: center;
      border-radius: 30px;

      ._left-box {
        min-width: 126px;

        ._horizotal {
          display: flex;
          align-items: flex-end;
          justify-content: center;

          > * {
            &:first-child {
              color: ${Colors.BLACK};
              margin-right: 5px;
            }

            &:last-child {
              color: ${Colors.alpha(Colors.BLACK, 0.35)};
              margin-bottom: 3px;
            }
          }
        }
      }

      ._avatar-box {
        margin-left: 12px;
        position: relative;

        ._dot {
          position: absolute;
          top: -1px;
          left: -1px;
          width: 15px;
          height: 15px;
          background-color: ${Colors.PRIMARY};
          border-radius: 8px;
          border: 3px solid #b9fdfd;
        }
      }
    }

    .item-box {
      display: flex;
      align-items: center;
      margin-right: 26px;
      margin-top: 5px;

      img {
        margin-right: 15px;
        width: 30px;
        height: 30px;
      }

      ._vertical {
        ._horizontal {
          display: flex;
          align-items: flex-end;
          margin-top: -5px;

          ._label {
            font-size: 13px;
            color: ${Colors.BLACK};
            margin-left: 5px;
            margin-bottom: 3px;
          }

          ._number {
            color: ${Colors.BLACK};
          }
        }
      }
    }
  }

  .menu-panel {
    background-color: white;
    position: fixed;
    top: 152px;
    bottom: 0;
    left: 0;
    right: 0;
    
    .menu-panel-content {
      display: flex;
      justify-content: center;
      align-items: center;
      z-index: 1;
      padding-top: 151px;
      .side {
        border-right: 1px solid #f1f1f1;
  
        &:last-child {
          border-right: 0;
          padding-left: 96px;
        }
  
        &:first-child {
          padding-right: 96px;
        }
  
        .left-menu-item {
          margin-bottom: 18px;
          
          &:hover {
            text-decoration: underline;
          }
  
          &:last-child {
            margin-bottom: 0;
          }
  
          .typography {
            font-size: 28px;
          }
        }
  
        .right-menu-group {
          margin-bottom: 40px;
  
          &:last-child {
            margin-bottom: 0;
          }
  
          .right-menu-group-title {
            opacity: 0.7;
            margin-bottom: 12px;
          }
  
          .right-menu-item {
            opacity: 0.5;
            margin-bottom: 4px;
  
            &:hover {
              text-decoration: underline;
            }
          }
        }
      }
    }

    .language-box {
      display: none;
    }
  }

  ${Media.lessThan(Media.SIZE.XXL)} {
    height: 122px;

    .menu-panel {
      background-color: white;
      position: fixed;
      top: 121px;
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    height: 76px;
    padding: 0 20px;

    .left-box {
      .logo {
        margin-right: 14px;

        img {
          width: 70px;
        }
      }
      
      .language-box {
        display: none;
      }
    }

    .center-box {
      position: fixed;
      bottom: 0;
      left: 0;
      z-index: 9;
      padding: 10px;
      background: rgba(0, 0, 0, 0.8) none repeat scroll 0% 0%;
      width: 100%;
      margin: 0;

      .search-box {
        height: 45px;
        margin: 0 10px;

        .search-box-type {
          background-color: #ddd;
        }

        .search-input {
          background-color: #fff;
          border-radius: 0 20px 20px 0;
        }
      }
    }

    .right-box {
      .login-button {
        padding: 0 20px;
      }

      .item-box-wish {
        // display: none;
      }

      .user-box {
        height: 47px;
        padding-left: 15px;
        padding-right: 2px;

        ._left-box {
          min-width: 100px;

          ._horizotal {
            > * {
              &:first-child {
                margin-right: 2px;
                font-size: 16px;
              }

              &:last-child {
                margin-bottom: 2px;
                font-size: 11px;
              }
            }
          }
        }

        ._avatar-box {
          margin-left: 5px;

          .avatar-image {
            width: 43px !important;
            height: 43px !important;
          }
        }
      }
    }

    .menu-panel {
      overflow: auto;
      top: 75px;
      
      .menu-panel-content {
        padding-top: 100px;
        .side {  
          width: 50%;
          &:last-child {
            padding: 0 20px;
          }
    
          &:first-child {
            padding: 0 20px;
          }
    
          .left-menu-item {
            .typography {
              font-size: 20px;
            }
          }
        }
      }
      
      .language-box-mobile {
        display: flex;
        align-items: center;
        margin-top: 20px;
        justify-content: center;
        .language-box {
          display: flex;
          align-items: center;
          margin-right: 15px;
      
          .flag-icon {
            width: 20px;
            box-shadow: 0px 0px 3px 1px #00000021;
            margin-right: 5px;
            min-width: 0;
            margin-top: 2px;
          }
      
          .arrow-icon {
            width: 10px;
            min-width: 0;
            margin-left: 5px;
          }
        }
      }
    }
    .menu-button {
      padding: 0 14px;
      margin-right: 0;
    }
  }

  ${Media.lessThan(Media.SIZE.SM)} {
    .right-box {
      .login-button {
        padding: 0 20px;
      }

      .item-box-wish {
        margin-right: 3px;
        img {
          margin-right: 3px;
          width: 20px;
          height: 20px;
        }

        ._number {
          font-size: 12px;
          margin-bottom: 0;
        }

        ._vertical {
          ._horizontal {
            ._label {
              font-size: 10px;
              margin-bottom: 1px;
            }
          } 
        } 
      }

      .user-box {
        height: 47px;
        padding-left: 8px;

        ._left-box {
          min-width: 100px;
          .small {
            font-size: 10px;
          }

          ._horizotal {
            > * {
              &:first-child {
                margin-right: 2px;
                font-size: 10px;
              }

              &:last-child {
                margin-bottom: 2px;
                font-size: 11px;
              }
            }
          }
        }

        ._avatar-box {
          ._dot {
            top: -4px;
            left: -4px;
          }
          margin-left: 1px;

          .avatar-image {
            width: 27px !important;
            height: 27px !important;
          }
        }
      }
    }
  }
`
const StyledPopoverContent = styled.div`
  display: flex;
  flex-direction: column;
  a {
    margin-bottom: 10px;
    user-select: none;
    color: ${Colors.BLACK};
    
    &:last-child {
      margin-bottom: 0;
    }
  }
`
const FlagItemBox = styled(Clickable)`
  display: flex;
  align-items: center;
  font-weight: bold;

  img {
    margin-top: 2px;
    width: 20px;
    margin-right: 5px;
    box-shadow: 0px 0px 3px 1px #00000021;
  }
`
// const CURRENCY = ['eth', 'nbng']
// const CURRENCY = ['nbng']

@withTranslation('common')
@withRouter
@inject((stores) => ({
  authStore: stores.auth,
  usersStore: stores.users,
  productsStore: stores.products
}))
@observer
class Header extends Component {
  static propTypes = {
    authStore: PropTypes.object,
    location: PropTypes.object,
    productsStore: PropTypes.object
  }

  state = {
    isOpenMenuPanel: false,
    searchType: 'ALL',
    previousPathName: '',
    isOpenPopover: false,
  }

  _onLogout = (e) => {
    const { location } = window
    const { pathname } = location

    e.preventDefault()
    if (pathname.includes('/category/rakuzadao_exchange/')) {
      logout(true)
    } else {
      logout()
    }

    this.setState({
      isOpenMenuPanel: false,
      isOpenPopover: false
    })
  }

  _onOpenMyPage = (e) => {
    const { history } = this.props
    this.setState({
      isOpenMenuPanel: false,
      isOpenPopover: false
    })
    e.preventDefault()

    history.push('/my-page')
  }

  _onOpenMyPageIssuing = (e) => {
    e.preventDefault()
    const { history } = this.props
    history.push('/issuing-nft/manager-nft')
    this.setState({ isOpenMenuPanel: false, isOpenPopover: false })
  }

  _onLoginButtonClick = () => {
    LoginModal.open()
  }

  _onBackHome = () => {
    const { history, productsStore } = this.props

    productsStore.newProducts.reverseLoaded()
    productsStore.regenerateRandomNumber()

    history.push('/')
    this.setState({ isOpenMenuPanel: false })
  }

  _onToggleMenuPanel = () => {
    this.setState((state) => ({
      isOpenMenuPanel: !state.isOpenMenuPanel
    }))
  }

  _onChangeLanguage = (language) => {
    const { i18n, location, history } = this.props

    i18n.changeLanguage(language, () => {
      let newPath = location.pathname.replace('/en/', '/')
      newPath = newPath.replace('/ja/', '/')

      history.replace(newPath)
    })
  }

  _onSearchChange = (value) => {
    const { history, location } = this.props
    const { searchType, previousPathName } = this.state
    value = value.target.value.trim()
    
    if (location.pathname !== '/search/keyword' && value.trim()) {
      history.push(`/search/keyword/${value}`)
    }
    if (!value.trim()) {
      history.replace(previousPathName)
    }
    if (value.trim()) {
      this._searchDebounce({
        value: value.trim(),
        searchType
      })
    }
  }

  _handleSavePathLocation = () => {
    const { location } = this.props
    if (!location.pathname.includes('/search/keyword')) {
      this.setState({
        previousPathName: location.pathname.slice(3)
      })
    }
  }

  _searchDebounce = lodash.debounce((value) => {
    events.headerSearchChange.emit(value)
  }, 300)

  _showValueSearch = () => {
    const { location, i18n } = this.props

    if (location.pathname.includes('/search/keyword/')) {
      const path = location.pathname
      const { language } = i18n

      return path.replace(`/${language}/search/keyword/`, '')
    }
  }

  _onCurrencySelect = (currency) => {
    const { authStore } = this.props
    authStore.setCurrency(currency)

    events.topCurrencyChange.emit(currency)
  }

  _onMenuPanelClick = (item) => {
    const { history } = this.props

    if (item.path) {
      history.push(item.path)
    } else if (item.mailto) {
      window.location.href = `mailto:${item.mailto}`
    } else {
      window.open(item.url, '_self')
    }

    this.setState({ isOpenMenuPanel: false })
  }

  _onChangeSearchType = (type) => {
    this.setState({
      searchType: type
    })
  }

  _renderMenuPanel = () => {
    const { t } = this.props
    const { isOpenMenuPanel } = this.state

    if (!isOpenMenuPanel) return null

    const MENU_1 = [{
      name: 'TOP',
      path: '/'
    }, {
      name: t('footer.about_rakuza.about_rakuza'),
      url: 'https://rakuza.io/about/index.html'
    }, {
      name: t('footer.about_rakuza.instruction'),
      url: 'https://rakuza.io/guide/index.html'
    }, {
      name: t('footer.about_rakuza.event_info'),
      url: 'https://rakuza.io/guide/index.html'
    }, {
      name: t('footer.about_rakuza.faq'),
      url: 'https://rakuza.io/faq/index.html'
    }]
    const MENU_2 = [{
      name: t('footer.about_us.title'),
      items: [{
        name: t('footer.about_us.operation'),
        url: 'https://rakuza.io/company/index.html'
      }, {
        name: t('footer.about_us.contact'),
        mailto: 'info@rakuza.io'
      }]
    }, {
      name: t('footer.policy.title'),
      items: [{
        name: t('footer.policy.notation'),
        url: 'https://rakuza.io/sct/index.html'
      }, {
        name: t('footer.policy.terms'),
        url: 'https://rakuza.io/terms_of_use/index.html'
      }, {
        name: t('footer.policy.policy'),
        url: 'https://rakuza.io/policy/index.html'
      }]
    }]

    return (
      <div className="menu-panel">
        <div className="menu-panel-content">
          <div className="side">
            {MENU_1.map((item, index) => (
              <Clickable
                key={index}
                className="left-menu-item"
                onClick={() => this._onMenuPanelClick(item)}
              >
                <Typography bold>
                  {item.name}
                </Typography>
              </Clickable>
            ))}
          </div>
          <div className="side">
            {MENU_2.map((parent, index) => (
              <div className="right-menu-group" key={index}>
                <Typography className="right-menu-group-title" bold>
                  {parent.name}
                </Typography>
                {parent.items.map((child, childIndex) => (
                  <Clickable
                    key={childIndex}
                    className="right-menu-item"
                    onClick={() => this._onMenuPanelClick(child)}
                  >
                    <Typography>
                      {child.name}
                    </Typography>
                  </Clickable>
                ))}
              </div>
            ))}
          </div>
        </div>
        <div className="language-box-mobile">
          {this._renderMultiLanguage()}
        </div>
      </div>
    )
  }

  _renderMultiLanguage = () => {
    const { i18n } = this.props
    const items = [
      { label: <Menu key='en'>
          <Menu.Item key='en'>
            <FlagItemBox
            effect={false}
            onClick={() => this._onChangeLanguage('en')}
          >
            <img src={Images[`${"en".toUpperCase()}_FLAG`]} alt="" />
            <Typography bold size="large">
              EN
            </Typography>
            </FlagItemBox>
          </Menu.Item>
        </Menu>
        , key: 'en' 
      }, // remember to pass the key prop
      { label: <Menu key='ja'>
        <Menu.Item key='ja'>
          <FlagItemBox
          effect={false}
          onClick={() => this._onChangeLanguage('ja')}
        >
          <img src={Images[`${"ja".toUpperCase()}_FLAG`]} alt="" />
          <Typography bold size="large">
            JA
          </Typography>
          </FlagItemBox>
        </Menu.Item>
      </Menu>, key: 'ja' },
    ];
    return (
      <Dropdown
        trigger="click"
        menu={{ items }
        }
      >
        <Clickable className="language-box">
          <img className="flag-icon" src={Images[`${i18n.language.toUpperCase()}_FLAG`]} alt="" />
          <Typography bold size="large">
            {i18n.language === 'en' ? 'EN' : 'JP'}
          </Typography>
          <img className="arrow-icon" src={Images.BLACK_CHEVRON_DOWN_ICON} alt="" />
        </Clickable>
      </Dropdown>
    )
  }

  _onGotoMyPage = () => {
    const { history } = this.props
    history.push('/my-page/wish-list')
  }

  const

  render() {
    const { authStore, location, t } = this.props
    const { isOpenMenuPanel, isOpenPopover } = this.state

    // const isShowCurrencySelect = location.pathname.includes('/search/keyword') || location.pathname === '/'
    const isShowSearchInput = location.pathname !== '/events'
      && !location.pathname.includes('product-details')


    return (
      <HeaderContainer>
        <div className="left-box">
          <Clickable className="logo" onClick={this._onBackHome}>
            <img src={Images.LOGO} alt="" />
          </Clickable>
          <Button
            background={Colors.BLACK}
            className="menu-button"
            onClick={this._onToggleMenuPanel}
          >
            <img
              src={isOpenMenuPanel ? Images.WHITE_SOLID_CLOSE_ICON : Images.WHITE_MENU_ICON}
              alt=""
            />
          </Button>
          {this._renderMultiLanguage()}
          
        </div>
        {isShowSearchInput && (
          <div className="center-box">
            <div
              className="search-box"
              onClick={this._handleSavePathLocation}
            >
              <Input
                onFocus={() => { this.setState({ isOpenMenuPanel: false }) }}
                onChange={this._onSearchChange}
                placeholder={t('header.search_placeholder')}
                className="search-input"
                prefix={(
                  <img
                    src={Images.GRAY_SEARCH_ICON}
                    alt=""
                    className="search-icon"
                  />
                )}
                allowClear
                value={this._showValueSearch()}
              />
            </div>
          </div>
        )}
        <div className="right-box">
          {authStore.loggedIn ? (
            <>
              <Clickable
                className="item-box item-box-wish"
                // onClick={this._onGotoMyPage}
              >
                <img src={Images.RED_WISH_ICON} alt="" />
                <div className="_vertical">
                  <Typography size="small" secondary>WISH</Typography>
                  <div className="_horizontal">
                    <Typography
                      size="huge"
                      bold
                      className="_number"
                    >
                      {authStore?.initialData?.wishCount}
                    </Typography>
                    <Typography size="small" className="_label">item</Typography>
                  </div>
                </div>
              </Clickable>
              <Popover
                open={isOpenPopover}
                trigger="click"
                content={(
                  <StyledPopoverContent>
                    <a href="/" onClick={this._onOpenMyPage}>{t('header.my_page')}</a>
                    { authStore.initialData.creator
                      ? (
                        <a href="/" onClick={this._onOpenMyPageIssuing}>
                          {t('header.issuing_nft')}
                        </a>
                      )
                      : '' }
                    <a href="/" onClick={this._onLogout}>{t('header.logout')}</a>
                  </StyledPopoverContent>
                )}
                placement="bottomRight"
                onClick={() => this.setState({
                  isOpenPopover: !isOpenPopover
                })}
              >
                <Clickable className="user-box">
                  <div className="_left-box">
                    <div className="_horizotal">
                      <Typography size="huge" bold>
                        {Format.price(authStore.initialData.ethBalance)}
                      </Typography>
                      <Typography secondary bold>ETH</Typography>
                    </div>
                    <Typography size="small">
                      {Misc.trimPublicAddress(authStore.initialData.publicAddress || '', 6)}
                    </Typography>
                  </div>
                  <div className="_avatar-box">
                    <div className="_dot" />
                    <Thumbnail
                      rounded
                      size={49}
                      url={authStore.initialData.userImage}
                      className="avatar-image"
                      placeholderUrl={Images.USER_PLACEHOLDER}
                    />
                  </div>
                </Clickable>
              </Popover>
            </>
          ) : (
            <>
              <Button
                className="login-button"
                onClick={this._onLoginButtonClick}
              >
                {t('header.login')}
              </Button>
            </>
          )}
        </div>
        {this._renderMenuPanel()}
      </HeaderContainer>
    )
  }
}

export default Header
