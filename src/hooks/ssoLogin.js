import { useContext } from 'react';
import queryString from 'query-string';

import { MobXProviderContext } from 'mobx-react';
import { useLocation } from 'react-router-dom';

import Storage from '../utils/storage';
import Request from '../utils/request';
import Confirmable from '../components/confirmable';
import { logout } from '../utils/auth';
import { useWeb3Auth } from '../contexts/web3auth/web3auth';
import { getWeb3Metamask } from '../utils/web3';
import { logout as logoutLocal } from '../utils/auth';
import Toast from '../components/toast';

export const useSSOLogin = () => {
  const { auth } = useContext(MobXProviderContext);
  const { search } = useLocation();
  const { logout: web3AuthLogout } = useWeb3Auth();

  const authToken = queryString.parse(search)?.authToken;
  const userId = Number(queryString.parse(search)?.userId);
  const publicAddress = queryString.parse(search)?.publicAddress;

  const getMatchedAccountByMetamask = async (walletAddress = '') => {
    const web3Info = await getWeb3Metamask();
    const accounts = await web3Info?.eth?.getAccounts();
    if (accounts?.length) {
      return accounts.find(x => x.toLowerCase() === walletAddress);
    }
  };

  const loginSSO = async () => {
    Storage.set('ACCESS_TOKEN', authToken);
    Storage.set('USER_ID', userId);
    Storage.set('PUBLIC_ADDRESS', publicAddress);
    Request.setAccessToken(authToken);

    await auth.setLoggedIn(true);
    try {
      const { data, success } = await auth.getInitialData();
      await auth.setInitialData({
        publicAddress: data.publicAddress,
        userId: userId,
      });
      if (success) {
        const sessionData = data?.web3SessionData;
        if (sessionData !== null) {
          const parseSession = JSON.parse(sessionData);
          const idSession = parseSession.idSession;
          if (parseSession.cachedAdapter == 'metamask') {
            try {
              const walletAddress = data?.publicAddress?.toLowerCase();
              let matchedAccount = await getMatchedAccountByMetamask(walletAddress);

              if (!matchedAccount) {
                Confirmable.open({
                  content: `Please connect Metamask wallet ${walletAddress} corresponding to logged account`,
                  hideCancelButton: true,
                  hideOkButton: true,
                  width: 450,
                });

                const requestPermission = await window.ethereum.request({
                  method: 'wallet_requestPermissions',
                  params: [{ eth_accounts: {} }],
                });

                const requestedAccounts = requestPermission?.[0]?.caveats?.[0]?.value;
                matchedAccount = requestedAccounts?.find(x => x.toLowerCase() === walletAddress);
                if (!matchedAccount) {
                  const ok = await Confirmable.open({
                    content: `Please connect Metamask wallet ${walletAddress} corresponding to logged account`,
                    hideCancelButton: true,
                    width: 450,
                  });
                  if(ok) {
                    await logout();
                    await auth.logout();
                    await logoutLocal();
                  }
                }
              }
            } catch (error) {
              await logout();
              await auth.logout();
              await logoutLocal();
            }
          }
          Storage.set('openlogin_store', idSession);
          const cachedAdapter = parseSession.cachedAdapter;
          localStorage.setItem('Web3Auth-cachedAdapter', cachedAdapter);
          Storage.set('SSO_ADAPTER', idSession);
        } else {
          await web3AuthLogout();
          await logoutLocal();
          await Confirmable.open({
            content: ('Login session has expired, Please login in again!'),
            hideCancelButton: true,
          });
        }
      }
    } catch (error) {
      Toast.error(error?.message);
    }
  };

  return { loginSSO };
};

export default useSSOLogin;
