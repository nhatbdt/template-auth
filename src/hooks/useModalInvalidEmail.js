import { useContext } from 'react';
import { ModalContext } from '../components/modal-verify-email';

export default function useModalInvalidEmail() {
  const { open, hide } = useContext(ModalContext);
  return { open, hide };
}
