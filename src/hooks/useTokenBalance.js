import { useEffect, useState } from 'react';
import * as ethers from 'ethers';
import BigNumber from 'bignumber.js';
// import useRefresh from './useRefresh';
import { CHAIN_LIST } from '../constants/chains';

export const useBalance = ({ account, currentChainId }) => {
  const [balance, setBalance] = useState(0);
  // const { fastRefresh } = useRefresh();

  useEffect(() => {
    const simpleRpcProvider = new ethers.providers.JsonRpcProvider(CHAIN_LIST[currentChainId]?.rpcUrls[0]);
    const fetchBalance = async () => {
      const walletBalance = await simpleRpcProvider.getBalance(account);
      setBalance(new BigNumber(walletBalance.toString()).dividedBy(10 ** CHAIN_LIST[currentChainId]?.decimals));
    };

    if (account) {
      fetchBalance();
    }
    // }, [account, currentChainId, setBalance, fastRefresh]);
  }, [account, currentChainId, setBalance]);

  return { balance, currency: CHAIN_LIST[currentChainId]?.currency };
};

export default useBalance;
