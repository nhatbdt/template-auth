export const GACHA_MODEL_LIST = [
  {
    thumbnail: process.env.REACT_APP_CLOUD_FRONT_URL + '/gacha/01.png',
    bgColor: 'transparent',
    value: 1,
    aspectRatioValue: '9:18',
    red: {
      win: '/gacha/win/Black-red-win.mp4',
      lose:'/gacha/lose/Black-black.mp4',
    },
    black:{
      win: '/gacha/win/Black-black-win.mp4',
      lose:'/gacha/lose/Black-red.mp4',
    }
  },
  {
    thumbnail: process.env.REACT_APP_CLOUD_FRONT_URL + '/gacha/02.png',
    bgColor: 'transparent',
    value: 2,
    aspectRatioValue: '9:18',
    red: {
      win: '/gacha/win/Pink-red-win.mp4',
      lose: '/gacha/lose/Pink-black.mp4',
    },
    black: {
      win: '/gacha/win/Pink-black-win.mp4',
      lose: '/gacha/lose/Pink-red.mp4',
    }
  },
  {
    thumbnail: process.env.REACT_APP_CLOUD_FRONT_URL + '/gacha/03.png',
    bgColor: 'transparent',
    value: 3,
    aspectRatioValue: '9:18',
    red: {
      win: '/gacha/win/Yellow-red-win.mp4',
      lose: '/gacha/lose/Yellow-black.mp4',
    },
    black: {
      win: '/gacha/win/Yellow-black-win.mp4',
      lose: '/gacha/lose/Yellow-red.mp4',
    }
  },
  {
    thumbnail: process.env.REACT_APP_CLOUD_FRONT_URL + '/gacha/04.png',
    bgColor: 'transparent',
    value: 4,
    aspectRatioValue: '9:18',
    red: {
      win: '/gacha/win/Blue-red-win.mp4',
      lose: '/gacha/lose/Blue-black.mp4',
    },
    black: {
      win: '/gacha/win/Blue-black-win.mp4',
      lose: '/gacha/lose/Blue-red.mp4',
    }
  },
];

// MISS: 'MISS',
// GREAT: 'GREAT',
// PERFECT: 'PERFECT',
export const GACHA_RESULTS = [
  {
    result: 'MISS',
    effects: {
      background: 'linear-gradient(89.83deg, #7E8184 3.13%, #C1C1C1 42.14%, #7E8184 99.67%)',
    },
    intro: '/gacha/introduction/standard-start.mp4',
  },
  {
    result: 'GREAT',
    effects: {
      background: 'linear-gradient(89.83deg, #DD37E2 3.13%, #D164F7 42.14%, #9835C6 99.67%)',
    },
    intro: '/gacha/introduction/win-start.mp4',
  },
  {
    result: 'PERFECT',
    effects: {
      background: 'linear-gradient(89.83deg, #D6AC40 3.13%, #FFD02C 42.14%, #D7A117 99.67%)',
    },
    intro: '/gacha/introduction/Premium-start.mp4',
  },
];

export const GACHA_QUIZ_LIST = [
  {
    colorMode: 'RED',
    quizVideo: '/gacha/quiz/red.mp4',
  },
  {
    colorMode: 'BLACK',
    quizVideo: '/gacha/quiz/black.mp4',
  }
];