export const PRODUCT_STATUSES = {
  NEW: 'NEW',
  SALE: 'SALE',
  SOLD: 'SOLD'
}

export const LENDING_STATUS = {
  WAIT_LENDING: 'WAIT_LENDING',
  LENDING: 'LENDING',
  IN_LENDING: 'IN_LENDING',
  WAIT_BORROW: 'WAIT_BORROW',
  BORROW: 'BORROW',
  FINISH: 'FINISH',
  FAIL: 'FAIL',
  EXPIRATION: 'EXPIRATION',
  WAIT_CANCEL: 'WAIT_CANCEL',
  CANCEL: 'CANCEL',
  WAIT_WITHDRAW: 'WAIT_WITHDRAW',
}

export const LENDING_TITLE = {
  LENDING: 'LENDING',
  BORROW: 'BORROW',
  CANCEL: 'CANCEL',
  WITHDRAW: 'WITHDRAW',
}

export const STAKING_STATUS = {
  UN_STAKING_PROGRESS: 'UN_STAKING_PROGRESS',
  UN_STAKING: 'UN_STAKING',
  STAKING_PROGRESS: 'STAKING_PROGRESS',
  STAKING: 'STAKING',
  FAIL: 'FAIL',
  FINISH: 'FINISH',
}