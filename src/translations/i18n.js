import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import lodash from 'lodash';

import { events } from '../services';
import validationMessages from './validation-messages.json';
import common from './common.json';
import auth from './auth.json';
import productDetails from './product-details.json';
import products from './products.json';
import userProfile from './user-profile.json';
import settings from './settings.json';
import home from './home.json';
import idols from './idols.json';
import eventsJson from './events.json';
import cultureJson from './cultures.json';
import stakingJson from './staking.json';

const INIT_LANGUAGE = 'ja';

const resources = lodash.merge(
  validationMessages,
  common,
  auth,
  productDetails,
  products,
  userProfile,
  settings,
  home,
  idols,
  eventsJson,
  cultureJson,
  stakingJson,
);

i18n.use(initReactI18next).init({
  resources,
  lng: INIT_LANGUAGE,
  fallbackLng: INIT_LANGUAGE,
  interpolation: {
    escapeValue: false,
  },
});

i18n.off('languageChanged');
i18n.on('languageChanged', language => {
  events.languageChange.emit(language);
});

export default i18n;
