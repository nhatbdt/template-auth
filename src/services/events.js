import { EventEmitter } from 'fbemitter'

const emitter = new EventEmitter()

const createEmitter = (key) => ({
  on: (handler) => emitter.addListener(key, handler),
  emit: (payload) => emitter.emit(key, payload),
  off: (token) => token.remove && token.remove()
})

// Create event emitters here
export const topCurrencyChange = createEmitter('TOP_CURRENCY_CHANGE')
export const headerSearchChange = createEmitter('HEADER_SEARCH_CHANGE')
export const languageChange = createEmitter('LANGUAGE_CHANGE')
