import { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { inject, observer } from 'mobx-react'
import { withTranslation, useTranslation } from 'react-i18next'
import lodash from 'lodash'

import Page from '../../components/page'
import Container from '../../components/container'
import Typography from '../../components/typography'
import FetchableList from '../../components/fetchable-list'
import Footer from '../../app/footer'
import ProductCard from '../../components/product-card'
import Misc from '../../utils/misc'
import Media from '../../utils/media'
import { firestore } from '../../services/firebase'

const Content = styled.div`
  padding-top: 5px;
  padding-bottom: 162px;

  .title-box {
    margin-bottom: 20px;
    display: flex;
    justify-content: space-between;
    
    ._horizontal {
      display: flex;
      align-items: flex-end;
      
      ._total {
        margin-left: 20px;
        margin-bottom: 3px;
      }
    }
  }
  
  .top-product-section {
    margin-bottom: 49px;
    display: none;
    
    &.show {
      display: block;
    }
  }
  
  ${Media.lessThan(Media.SIZE.MD)} {
    .title-box {
      margin-bottom: 15px;
      justify-content: flex-start;
      flex-direction: column;

      ._horizontal {
        margin-bottom: 10px;

        .section-title {
          font-size: 20px;
        }

        ._total {
          color: #717171;
        }
      }
    }
  }
`

let BANNERS = []
let ADS = []

@withTranslation(['home', 'ads'])
@inject((stores) => ({
  productsStore: stores.products
}))
@observer
class Home extends Component {
  static propTypes = {
    productsStore: PropTypes.object
  }

  state = {
    banners: BANNERS,
    ads: ADS
  }

  componentDidMount() {
    this._fetchData()
  }

  componentWillUnmount() {
    const { productsStore } = this.props

    productsStore.setHomePageLatestOffset(this._page.getScrollOffset())
  }

  _fetchData = async () => {
    const { i18n, productsStore } = this.props

    if (productsStore.homePageLatestOffset && productsStore.newProducts.loaded) {
      setTimeout(() => {
        this._page.scrollTo(productsStore.homePageLatestOffset)
      }, 0)
    }

    const bannerJob = firestore.bannersLangs()
      .where('displayStatus', '==', 'PUBLIC')
      .where('lang', '==', i18n.language.toUpperCase())
      .get()
    const adsJob = firestore.adsLangs()
      .where('displayStatus', '==', 'PUBLIC')
      .where('lang', '==', i18n.language.toUpperCase())
      .get()
    
    const [bannerSnapshot, adsSnapshot] = await Promise.all([bannerJob, adsJob])
    
    let banners = []
    let ads = []
    bannerSnapshot.forEach((doc) => banners.push(doc.data()))
    adsSnapshot.forEach((doc) => ads.push(doc.data()))
    banners = lodash.sortBy(banners, ['priority'])
    ads = lodash.sortBy(ads, ['position'])
    BANNERS = banners
    ADS = ads

    await productsStore.getProducts({
      findType: 'NEW_PRODUCT',
      langKey: i18n.language.toUpperCase(),
      sortBy: 'RANDOM',
      randomNumber: productsStore.randomNumber
    }, { concat: false })

    this.setState({
      banners,
      ads
    })
    
  }

  _onCategoryClick = (category) => {
    const { history } = this.props

    history.push(`/search/category/${category.id}`)
  }

  _renderProductItem = (item) => {
    const { history } = this.props

    return (
      <>
        <ProductCard
              onClick={() => {
                if (item.artistNickname === 'kojomasayuki') {
                  history.push(`/author/kojomasayuki/product/${item.id}`)
                } else {
                  history.push(`/product-details/${item.id}`)
                }
              }}
              item={item}
            />
      </>
    )
  }

  _renderNewProducts = () => {
    const { productsStore, t, i18n } = this.props
    const { ads } = this.state
    let displayItems = productsStore.newProducts.items

    ads.forEach((item) => {
      if (displayItems.length > +item.position) {
        displayItems = Misc.insert(displayItems, +item.position - 1, [{
          ...item,
          type: 'ads'
        }])
      }
    })

    return (
      <section>
        <div className="title-box">
          <div className="_horizontal">
            <Typography size="giant" className="section-title" bold>
              {t('home:new_product')}
            </Typography>
            <Typography className="_total">
              <b>{productsStore.newProducts.total}</b> {t('home:item')}
            </Typography>
          </div>
        </div>
        <FetchableList
          ref={(ref) => { this._newProductFetchableList = ref }}
          colSpan={{
            xs: 12
          }}
          autoFetchOnMount={false}
          keyExtractor={(item, index) => index}
          noDataMessage={t('common:no_item')}
          action={productsStore.getProducts}
          items={displayItems}
          total={productsStore.newProducts.total}
          page={productsStore.newProducts.page}
          length={productsStore.newProducts.items.length}
          payload={{
            findType: 'NEW_PRODUCT',
            langKey: i18n.language.toUpperCase(),
            sortBy: 'RANDOM',
            randomNumber: productsStore.randomNumber
          }}
          renderItem={this._renderProductItem}
        />
      </section>
    )
  }

  render() {
    return (
      <Page
        ref={(ref) => { this._page = ref }}
        onEndReached={() => {
          this._newProductFetchableList.loadMore()
        }}
      >
        <Container fluid>
          <Content>
            {this._renderNewProducts()}
          </Content>
        </Container>
        <Footer />
      </Page>
    )
  }
}

const HomeExport = (props) => {
  const { i18n } = useTranslation()

  return <Home {...props} key={i18n.language} />
}


export default HomeExport
