import { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { withTranslation } from 'react-i18next'
import { inject, observer } from 'mobx-react'
import moment from 'moment'

import { Images, Colors } from '@/theme'
import Typography from '@/components/typography'
import Thumbnail from '@/components/thumbnail'
import NoDataBox from '@/components/no-data-box'
import Media from '@/utils/media'
import Misc from '@/utils/misc'
import Format from '@/utils/format'

const StyledDiv = styled.div`
  margin-top: 90px;
  
  .top-box {
    display: flex;
    padding-bottom: 29px;
    border-bottom: 2px solid rgb(0 0 0 / 15%);
    margin-bottom: 17px;

    .typography {
      font-size: 13px;
    }
  }

  .table-outer {
    table {
      width: 100%;

      thead {
        tr {
          th {
            font-size: 12px;
            font-weight: normal;
            color: #b7b7b7;
            text-align: left;

            &:nth-child(1) {
              width: 15%;
            }

            &:nth-child(2) {
              width: 10%;
            }

            &:nth-child(3) {
              width: 10%;
            }

            &:nth-child(4) {
              width: 22%;
            }

            &:nth-child(5) {
              width: 10%;
            }

            &:nth-child(6) {
              width: 22%;
            }

            &:nth-child(7) {
              width: 20%;
            }
          }
        }
      }

      tbody {
        tr {
          height: 53px;
          border-bottom: 1px solid #f1f1f1;

          td {
            .transfer-icon {
              opacity: 0.3;
              width: 28px;
            }

            .horizon {
              display: flex;
              align-items: center;

              .avatar {
                margin-right: 9px;
                min-width: 0;
              }

              .user-name {
                font-size: 13px;
                color: ${Colors.RED_2};
                overflow: hidden;
                text-overflow: ellipsis;
                max-width: 180px;
                white-space: nowrap;
                flex: 1;
                margin-right: 5px;
              }
            }

            .date {
              color: #696969;
            }
          }
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .table-outer {
      overflow-x: auto;

      table {
        table-layout:fixed;
        
        thead {
          tr {
            th {
              &:nth-child(1) {
                width: 160px;
              }

              &:nth-child(2) {
                width: 100px;
              }

              &:nth-child(3) {
                width: 100px;
              }

              &:nth-child(4) {
                width: 200px;
              }

              &:nth-child(5) {
                width: 50px;
              }

              &:nth-child(6) {
                width: 200px;
              }

              &:nth-child(7) {
                width: 150px;
              }
            }
          }
        }
      }
    }
  }
`

@withTranslation('product_details')
@inject((stores) => ({
  product: stores.products.currentProductDetails,
  productsStore: stores.products
}))
@observer
class TransactionHistoriesBox extends Component {
  static propTypes = {
    product: PropTypes.object,
    productsStore: PropTypes.object
  }

  state = {
    initial: true,
    data: []
  }

  async componentDidMount() {
    const { productsStore, product } = this.props

    const { success, data } = await productsStore.getTransactionHistories({
      productId: product.id
    })

    if (success) {
      this.setState({
        data,
        initial: false
      })
    }
  }

  _renderItem = (item, index) => {
    const { t } = this.props
    const timeFormat = 'YYYY/M/D HH:mm:ss'

    return (
      <tr key={index}>
        <td>
          {item.type === 'BID'
            ? t('transaction_history_status.auction') : item.type === 'NORMAL'
              ? t('transaction_history_status.normal') : item.type === 'OFFER'
                ? t('transaction_history_status.offer') : t('transaction_history_status.resale')}
        </td>
        <td>
          <Typography size="large" bold poppins>
            {Format.price(item.price)}
          </Typography>
        </td>
        <td>
          {item.currency}
        </td>
        <td>
          <div className="horizon">
            <Thumbnail
              size={30}
              rounded
              className="avatar"
              placeholderUrl={Images.USER_PLACEHOLDER}
              url={item.sellerAvatar}
            />
            <Typography poppins className="user-name">
              {item.sellerName || Misc.trimPublicAddress(item.sellerPublicAddress, 6)}
            </Typography>
          </div>
        </td>
        <td>
          <img className="transfer-icon" src={Images.BLACK_TRANSFER_ICON} alt="" />
        </td>
        <td>
          <div className="horizon">
            <Thumbnail
              size={30}
              rounded
              className="avatar"
              placeholderUrl={Images.USER_PLACEHOLDER}
              url={item.buyerAvatar}
            />
            <Typography poppins className="user-name">
              {item.buyerName || Misc.trimPublicAddress(item.buyerPublicAddress, 6)}
            </Typography>
          </div>
        </td>
        <td>
          <Typography size="small" className="date">
            {moment(item.createdAt).format(timeFormat)}
          </Typography>
        </td>
      </tr>
    )
  }

  render() {
    const { t } = this.props
    const { data, initial } = this.state
    return (
      <StyledDiv>
        <div className="top-box">
          <Typography>
            {t('transaction_histories')}
          </Typography>
        </div>
        {!initial && data.length === 0 ? (
          <NoDataBox message={t('no_histories')} />
        ) : (
          <div className="table-outer">
            <table>
              <thead>
                <tr>
                  <th>{t('common:event')}</th>
                  <th>{t('common:price')}</th>
                  <th>{t('common:header.currency')}</th>
                  <th>{t('common:seller')}</th>
                  <th> </th>
                  <th>{t('common:buyer')}</th>
                  <th>{t('common:date_time')}</th>
                </tr>
              </thead>
              <tbody>
                {data.map(this._renderItem)}
              </tbody>
            </table>
          </div>
        )}
      </StyledDiv>
    )
  }
}

export default TransactionHistoriesBox
