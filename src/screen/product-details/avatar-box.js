import { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import classnames from 'classnames'
import { inject, observer } from 'mobx-react'
import { withTranslation } from 'react-i18next'

import Typography from '@/components/typography'
import MagnifyImage from '@/components/magnify-image'
// import Button from '@/components/button'
import { Colors, Images } from '@/theme'
import Media from '@/utils/media'

const StyledDiv = styled.div`
  margin-bottom: 53px;
  
  .avatar-box-content {
    height: 463px;
    background-size: cover;
    background-position: center;

    .mask {
      position: relative;
      backdrop-filter: blur(25px);
      background-color: rgb(255 255 255 / 50%);
      width: 100%;
      height: 100%;
      display: flex;
      justify-content: center;
      align-items: center;

      ._avatar {
        width: 256px;
        height: 256px;
        display: flex;
        justify-content: center;
        align-items: center;

        > div {
          > div {
            &:last-child {
              z-index: 1000;
            }
          }
        }
        
        .heat-it-button {
          position: absolute;
          bottom: 0;
          width: 208px;
          display: flex;
          justify-content: center;
          align-items: center;
          margin-bottom: -28px;
          box-shadow: 0 0 12px 0 rgba(0, 0, 0, 0.11);

          img {
            margin-right: 11px;
          }

          ._text {
            color: ${Colors.PRIMARY};
          }
        }
      }
      
      ._frame {
        width: 625px;
        height: 525px;
        background-image: url("${Images.PRODUCT_FRAME}");
        background-repeat: no-repeat;
        background-position: 3px center;
        background-size: contain;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: space-between;
        
        ._frame-inner {
          margin-top: 139px;
          height: 208px;
          width: 339px;
          margin-left: -3px;
          display: flex;
          justify-content: center;
          align-items: center;
        }
        
        .child-info {
          display: flex;
          margin-bottom: 113px;
          align-items: center;
          
          .child-image {
            height: 40px;
            margin-right: 20px;
          }
          
          ._right {
            display: flex;
            flex-direction: column;
            align-items: center;
            padding-top: 11px;
            
            .serial-number {
              color: white;
              font-family: 'SimpleFont';
              font-size: 10px;
              
              &.big {
                font-size: 30px;
                margin-top: -10px;
              }
            }
          }
        }
      }
    }
  }
  
  .auction-end-noti-box {
    height: 76px;
    background-color: #fce010;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  
  &.license {
    margin-bottom: 0;
    
    .avatar-box-content {
      height: 700px;
      margin-bottom: 0;
      
      .mask {
        ._avatar {
          position: relative;
          width: 516px;
          height: 506px;
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .avatar-box-content {
      height: auto;
      padding: 15% 0;
      border-radius: 8px;

      .mask {
        background-color: transparent;

        ._avatar {
          > div {
            > div {
              display: none;
            }
          }
        }

        ._frame {
          ._frame-inner {
            height: auto!important;
            width: 160px!important;
            margin-top: 170px;

            img {
              height: auto!important;
              width: 100%!important;
            }
          }

          .child-info {
            margin-bottom: 10px;
            
            ._right {
              .serial-number {
                color: black;
              }
            }
          }
        }
      }
    }

    &.license {
      .avatar-box-content {
        height: auto;
        padding: 0;
        border-radius: 0;
        
        .mask {
          background-color: rgb(255 255 255 / 50%);
          
          ._avatar {
            padding: 75px 0;
            height: auto;
            
            > div {
              height: auto!important;
              width: 100%!important;
              
              img {
                height: auto!important;
                width: 100%!important;
              }
            }
          }
        }
      }
    }
  }
`

@withTranslation('product_details')
@inject((stores) => ({
  product: stores.products.currentProductDetails
}))
@observer
class AvatarBox extends Component {
  static propTypes = {
    product: PropTypes.object,
    license: PropTypes.bool
    // onClick: PropTypes.func
  }

  state = {}

  render() {
    const { product, license, t } = this.props
    const childCount = product.parentProductInfo?.countChild || product.countChild
    const isShowAuctionEndedNoti = product.productBid
      && !['NEW', 'INIT'].includes(product.productBid?.status)
      && license
      && product.offerNftTransactionStatus !== 'SUCCESS'
    const imageUrl = (product.isMulti && !product.isParent)
      ? product.parentProductInfo?.imageUrl : product.imageUrl

    return (
      <StyledDiv
        className={classnames({ license })}
      >
        <div
          className="avatar-box-content"
          style={license
            ? { backgroundImage: `url(${imageUrl})` }
            : { backgroundColor: product.backgroundColor }}
        >
          <div className="mask">
            {product.isMulti ? (
              <div className="_frame">
                <div className="_frame-inner">
                  <MagnifyImage
                    url={imageUrl}
                    maxSize={[339, 208]}
                  />
                </div>
                {childCount > 0 && (
                  <section className="child-info">
                    {!product.isParent && (
                      <img
                        className="child-image"
                        src={product.imageUrl}
                        alt=""
                      />
                    )}
                    <div className="_right">
                      <Typography className="serial-number">
                        {product.isParent ? t('remain') : '<SERIAL NUMBER>'}
                      </Typography>
                      <Typography className="serial-number big">
                        {product.isParent ? product.remainingChildProductNumber : product.childIndex}/{childCount}
                      </Typography>
                    </div>
                  </section>
                )}
              </div>
            ) : (
              <div className="_avatar">
                <MagnifyImage
                  url={imageUrl}
                  maxSize={license ? 500 : 256}
                />
                {/* <Button className="heat-it-button" background="white" onClick={onClick}> */}
                {/*  <img src={Images.COLOR_FIRE_ICON} alt="" /> */}
                {/*  <Typography bold large className="_text"> */}
                {/*    HEAT IT ! */}
                {/*  </Typography> */}
                {/* </Button> */}
              </div>
            )}
          </div>
        </div>
        {isShowAuctionEndedNoti && (
          <div className="auction-end-noti-box">
            <Typography bold size="huge">
              {t('auction_has_ended')}
            </Typography>
          </div>
        )}
      </StyledDiv>
    )
  }
}

export default AvatarBox
