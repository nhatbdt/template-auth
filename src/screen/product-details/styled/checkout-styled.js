import styled from 'styled-components'
import Media from '@/utils/media'
import { Colors } from '@/theme'

const StyledDiv = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 40px;

  ._left {
    display: flex;
    flex-direction: column;

    ._price-box {
      display: flex;
      align-items: center;

      ._label {
        margin-right: 15px;
      }

      ._yen-price {
        font-size: 15px;
        margin-left: 4px;
      }

      ._timer {
        b {
          font-size: 34px;
          font-weight: 500;
          margin: 0 4px
        }

        .price-custom {
          b {
            font-size: 34px;
            font-weight: 500;
            margin: 0 4px;
            ${Media.lessThan(Media.SIZE.XXL)} {
              font-size: 28px;
            }
          }
          ${Media.lessThan(Media.SIZE.MD)} {
            text-align: right;
            b {
              display: inline-block;
              font-size: 23px;
            }
          }
        }
      }
    }


    .transaction-text {
      margin-top: 5px;
      display: flex;

      .transaction-link {
        color: ${Colors.PRIMARY};
        margin-left: 15px;
      }
    }
  }

  ._right {
    display: flex;
    flex-direction: column;
    align-items: center;

    .holding-caution {
      color: ${Colors.PRIMARY};
      margin-top: 10px;
    }
    .button-group {
      text-align: center;
    }
  }

  .checkout-button {
    width: 238px;
    padding: 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    
    ._small {
      font-size: 12px;
    }
  }

  .offer-button {
    margin-top: 10px;
    width: 238px;
    padding: 0;
    color: #e51109;
    border: 1px solid #e51109;
  }
  
  .bid-button {
    margin-top: 10px;
    width: 238px;
    padding: 0;
  }
  
  ${Media.lessThan(Media.SIZE.MD)} {
    position: fixed;
    bottom: 0;
    left: 0;
    right: 0;
    background-color: white;
    box-shadow: 0 0 30px rgb(0 0 0 / 7%);
    flex-direction: column;
    align-items: unset;
    padding: 15px;
    padding-top: 5px;
    z-index: 10000;
    margin-bottom: 0;

    ._left {
      ._price-box {
        justify-content: space-between;
        margin-bottom: 10px;

        ._inner {
          display: flex;
          flex-direction: column;
          align-items: flex-end;
        }

        ._yen-price {
          font-size: 12px;
          color: #b0b0b0;
        }

        ._timer {
          &.price-custom {
            b {
              display: block;
            }
          }
          font-size: 12px;

          b {
            font-size: 23px;
          }
        }
      }
    }
  }
`

export default StyledDiv
