import { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { inject, observer } from 'mobx-react'
import {
  Chart,
  LineElement,
  PointElement,
  LineController,
  CategoryScale,
  LinearScale,
  Filler,
  Tooltip
} from 'chart.js'
import zoomPlugin from 'chartjs-plugin-zoom'
import moment from 'moment'
import { withTranslation } from 'react-i18next'

import Typography from '@/components/typography'
import Media from '@/utils/media'
import Format from '@/utils/format'

Chart.defaults.font.size = 12
Chart.defaults.font.family = 'Poppins'

Chart.register(
  LineElement,
  PointElement,
  LineController,
  CategoryScale,
  LinearScale,
  Filler,
  zoomPlugin,
  Tooltip
)

const StyledDiv = styled.div`
  margin-top: 34px;

  .currency-label {
    margin-left: 20px;
  }

  .info-box {
    padding: 17px 0;
    display: flex;
    background-color: rgb(255 71 47 / 5%);
    border-radius: 6px;
    display: flex;
    justify-content: center;
    margin-top: 28px;
    justify-content: space-around;

    .item-box {
      .typography {
        &:first-child {
          color: rgba(0, 0, 0, 0.25);
        }

        &:last-child {
          font-size: 24px;

          span {
            font-size: 16px;
            margin-left: 5px;
          }
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .info-box {
      flex-direction: column;
      margin-bottom: 20px;
      align-items: center;

      .item-box {
        margin-bottom: 20px;
        display: flex;
        flex-direction: column;
        align-items: center;

        &:last-child {
          margin-bottom: 0;
        }
      }
    }
  }
`

@withTranslation()
@inject((stores) => ({
  product: stores.products.currentProductDetails,
  productsStore: stores.products
}))
@observer
class ChartBox extends Component {
  static propTypes = {
    product: PropTypes.object,
    productsStore: PropTypes.object
  }

  state = {
    averagePrice: null,
    totalPrice: null
  }

  componentDidMount() {
    this._chart = new Chart(this._canvas, {
      type: 'line',
      data: {
        datasets: [{
          backgroundColor: 'rgb(239 83 60 / 15%)',
          borderColor: '#ff472f',
          fill: true
        }]
      },
      options: {
        responsive: true,
        layout: {
          padding: {
            top: 0
          }
        },
        elements: {
          line: {
            tension: 0.3
          }
        },
        plugins: {
          zoom: {
            zoom: {
              wheel: {
                enabled: true
              },
              pinch: {
                enabled: true
              },
              drag: {
                enabled: true
              },
              mode: 'x'
            }
          },
          tooltip: {
            callbacks: {
              label(context) {
                return ` ${context.formattedValue} GOKU`
              }
            }
          }
        }
      }
    })

    this._fetchData()
  }

  _fetchData = async () => {
    const { productsStore, product } = this.props

    const { success, data } = await productsStore.getPriceChartData({
      productId: product.id
    })

    if (success) {
      this.setState({
        averagePrice: data.averagePrice,
        totalPrice: data.totalPrice
      })

      this._updateChart(data.data)
    }
  }

  _updateChart = (data) => {
    const labels = data.map((item) => moment(item.createdAt).format('M/D'))
    const newData = data.map((item) => item.price)

    this._chart.data.labels = labels
    this._chart.data.datasets[0].data = newData
    this._chart.update()
  }

  render() {
    const { t } = this.props
    const { averagePrice, totalPrice } = this.state

    return (
      <StyledDiv>
        <Typography className="currency-label" size="small">
          GOKU
        </Typography>
        <canvas height={80} ref={(ref) => { this._canvas = ref }} />
        <div className="info-box">
          <div className="item-box">
            <Typography size="small">
              {t('product_details:all_average_price')}
            </Typography>
            <Typography bold>
              {averagePrice ? Format.price(averagePrice) : '-'}
              <span>
                GOKU
              </span>
            </Typography>
          </div>
          <div className="item-box">
            <Typography size="small">
              {t('product_details:all_volume')}
            </Typography>
            <Typography bold>
              {totalPrice ? Format.price(totalPrice) : '-'}
              <span>
                GOKU
              </span>
            </Typography>
          </div>
        </div>
      </StyledDiv>
    )
  }
}

export default ChartBox
