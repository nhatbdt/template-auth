import { Component } from 'react'
import PropTypes from 'prop-types'
import moment from 'moment'
import { inject, observer } from 'mobx-react'
import { withTranslation } from 'react-i18next'
import { firestore } from '@/services/firebase'

import {timeOutFireBase} from '@/constants/timeOut'
import Web3 from '@/utils/web3other'
import Button from '@/components/button'
import Typography from '@/components/typography'
import MaskLoading from '@/components/mask-loading'
import Clickable from '@/components/clickable'
import Confirmable from '@/components/confirmable'
import { Colors } from '@/theme'
import { login } from '@/utils/conditions'
import Misc from '@/utils/misc'
import Format from '@/utils/format'
import Configs from '@/configs'
import CURRENCIES from '@/constants/currencies'

import ProductTogetherModal from './buy-together/product-modal'
import {
  onSign,
  onError,
  onCheckPrice,
  onCheckNetwork,
  onCheckBalance
} from './utils/checkout-normal'
import StyledDiv from './styled/checkout-styled'

@withTranslation(['product_details', 'common'])
@inject((stores) => ({
  paymentStore: stores.payment,
  authStore: stores.auth,
  product: stores.products.currentProductDetails,
  productsStore: stores.products
}))
@observer
class CheckoutBox extends Component {
  static propTypes = {
    product: PropTypes.object,
    authStore: PropTypes.object,
    paymentStore: PropTypes.object,
    productsStore: PropTypes.object
  }

  constructor(props) {
    super(props)
    const { product } = props
    const { startTime, endTime, currentTime } = product
    this._isTimeValid = product.typeSale !== 'LIMITED' || (moment(currentTime).diff(moment(startTime), 's') >= 0
      && moment(endTime).diff(moment(currentTime), 's') >= 0)
  }

  componentWillUnmount() {
    if (this._unsubscribe) {
      this._unsubscribe()
    }
  }

  _orderListener = (documentId) => new Promise((resolve, reject) => {
    const timeOut = setTimeout(() => {
      this._unsubscribe()
      // eslint-disable-next-line prefer-promise-reject-errors
      reject('ERROR_TIMEOUT')
    }, timeOutFireBase)

    this._unsubscribe = firestore
      .orders()
      .doc(documentId)
      .onSnapshot((snapshot) => {
        const data = snapshot.data()

        if (data.status) {
          resolve(data)

          this._unsubscribe()
          clearTimeout(timeOut)
        }
      }, (e) => reject(e))
  })

  _onTransactionLinkClick = (transaction) => {
    window.open(`${Configs.ETHERSCAN_DOMAIN}/tx/${transaction}`)
  }

  _purchase = async (signature, product) => {
    const { authStore, t, paymentStore } = this.props

    const purchaseResult = await paymentStore.purchareProduct({
      productId: product.id,
      signature
    })

    if (!purchaseResult.success) {
      throw purchaseResult.data
    }

    if (purchaseResult.data.holding) {
      product.setHolding(true)

      throw 'PRODUCT_HOLDING'
    }

    // if (!purchaseResult.data.orderId) {
    //   throw 'OUT_OF_WALLET'
    // }

    this._orderId = purchaseResult.data.orderId

    const fireStoreOrder = await firestore.orders().add({
      productId: product.id,
      status: false,
      createdAt: moment().format()
    })

    const { publicAddress } = authStore.initialData

    const checkAllowResult = await paymentStore.checkAllowPurchase({
      productId: product.id,
      firestoreOrderId: fireStoreOrder.id
    })

    if (!checkAllowResult.success) {
      throw checkAllowResult.data
    }

    const orderResult = await this._orderListener(fireStoreOrder.id)

    if (!orderResult.userCanBuy) {
      throw 'CANNOT_CHECKOUT'
    }

    // if (!orderResult.paymentAddress || !orderResult.hasPublicKey) {
    //   throw 'OUT_OF_WALLET'
    // }

    MaskLoading.open({
      message: (
        <>
          {t('messages.caution_1')}
          <br />
          {t('messages.caution_2')}
        </>
      )
    })

    const gasLimit = 300000

    const data = purchaseResult.data.smartContractVerifySignature

    const Instance = await Web3.getWeb3Instance()
    const nonce = await Instance.web3.eth.getTransactionCount(publicAddress)
    const contractExchangeAddress = Configs.RAKUZA_CONTRACT_ADDRESS

    let transactionId

    await Instance.web3.eth.sendTransaction({
      from: publicAddress,
      to: contractExchangeAddress,
      data,
      gasLimit,
      value: product.currency === 'ETH' ? Instance.web3.utils.toWei(product.price.toString(), 'ether') : ''
    }, async (error, hash) => {
      if (error) {
        return error
      }

      let startTime = new Date().getTime()
      // eslint-disable-next-line no-shadow
      let hashResult
      let limitTimeFlg = true

      do {
        hashResult = await Web3.checkTransactionIsConfirming(
          hash,
          nonce,
          product.currency,
          publicAddress,
          product.currency === 'ETH' ? 'BuyNFTETH' : 'BuyNFTNormal',
          contractExchangeAddress
        )

        if (hashResult.cancelFlg) {
          limitTimeFlg = new Date().getTime() <= (startTime + (45 * 1000))
        }
        transactionId = await hashResult.hash
      }
      while (!transactionId && limitTimeFlg)

      if (transactionId) {
        firestore.logs().add({
          productId: product.id,
          createdAt: moment().format('YYYY/MM/DD HH:mm'),
          currency: product?.currency,
          from: authStore.initialData.publicAddress,
          to: product?.user?.publicAddress,
          transactionId,
          reselling: !!product.productResell?.reselling,
          // contractAddress,
          price: product?.price,
          balances: this._balances,
          userAgent: navigator?.userAgent,
          onMobile: Misc.isMobile
        })
        const confirmResult = await paymentStore.confirmPurchaseProduct({
          ...purchaseResult.data,
          contractTransactionId: transactionId
        })

        if (!confirmResult.success) throw confirmResult.data
        else {
          const { weth, eth, usdt } = await Web3.getBalance(publicAddress)

          authStore.setBalance({
            weth: weth || 0,
            eth: eth || 0,
            usdt: usdt || 0
          })

          MaskLoading.close()

          Confirmable.open({
            content: (
              <>
                {t('messages.success_2')}
              </>
            ),
            hideCancelButton: true
          })

          product.setData({
            status: 'SOLD',
            buying: true
          })
        }
      } else {
        MaskLoading.close()
        Confirmable.open({
          content: (
            <>
              {t('TRANSACTION_CANCEL')}
            </>
          ),
          hideCancelButton: true
        })
        throw { error: 'TRANSACTION_CANCEL' }
      }
    })

    // const transactionId = await onTransfer(publicAddress, t, data, gasLimit, product.currency, product.price)
    // firestore.logs().add({
    //   productId: product.id,
    //   createdAt: moment().format('YYYY/MM/DD HH:mm'),
    //   currency: product?.currency,
    //   from: authStore.initialData.publicAddress,
    //   to: product?.user?.publicAddress,
    //   transactionId,
    //   reselling: !!product.productResell?.reselling,
    //   // contractAddress,
    //   price: product?.price,
    //   balances: this._balances,
    //   userAgent: navigator?.userAgent,
    //   onMobile: Misc.isMobile
    // })
    // const confirmResult = await paymentStore.confirmPurchaseProduct({
    //   ...purchaseResult.data,
    //   contractTransactionId: transactionId
    // })

    // if (!confirmResult.success) throw confirmResult.data
    // else {
    //   if (product.currency === 'NBNG') {
    //     authStore.setNbngBalance(authStore.initialData.nbngBalance - product.price)
    //   }

    //   MaskLoading.close()

    //   Confirmable.open({
    //     content: (
    //       <>
    //         {t('messages.success_2')}
    //       </>
    //     ),
    //     hideCancelButton: true
    //   })

    //   product.setData({
    //     status: 'SOLD',
    //     buying: true
    //   })
    // }
  }

  _onValidate = async (product) => {
    const { productsStore, t, authStore } = this.props
    await onCheckPrice(product, productsStore, t)
    await onCheckNetwork()
    this._balances = await onCheckBalance(product, authStore)
  }

  _onCheckout = async (product, isCheckoutChildProduct = false) => {
    const { paymentStore, t, authStore } = this.props

    // this._orderId = null

    MaskLoading.open({
      message: (
        <>
          {t('messages.waiting_1')}
          <br />
          {t('messages.waiting_2')}
        </>
      )
    })

    this.isCancelUpdate = false


    try {
      if (product.currency === CURRENCIES.ETH) {
        const signature = await onSign(authStore)
        await this._onValidate(product)
        await this._purchase(signature, product)
      } else {
        const signature = await onSign(authStore)

        const { publicAddress } = authStore.initialData
        const paymentAddress = Configs.RAKUZA_CONTRACT_ADDRESS

        const Instance = await Web3.getWeb3Instance()
        const amountMax = Configs.AMOUNT_MAX
        const gasPrice = await Instance.web3.eth.getGasPrice()

        const contractErc20 = Instance[product.currency === 'USDT'
          ? 'usdtContract' : product.currency === 'GOKU'
            ? 'gokuContract' : 'wethContract']

        const allowance = await Web3[product.currency === CURRENCIES.USDT
          ? 'allowanceUsdt' : product.currency === CURRENCIES.GOKU
            ? 'allowanceGoku' : 'allowanceWeth'](
              publicAddress,
              paymentAddress
            )

        if (allowance >= product.price) {
          await this._onValidate(product)
          await this._purchase(signature, product)
        } else {
          //= =====================================
          if (allowance !== 0) {
            const firstNonce = await Instance.web3.eth.getTransactionCount(publicAddress)

            await contractErc20.methods.approve(paymentAddress, 0)
              .send({
                from: publicAddress,
                gasPrice: Math.floor(gasPrice * 1.2)
              }, async (error, hash) => {
                if (error) {
                  return error
                }
                let startTime = new Date().getTime()
                let approveHash
                let hashResult
                let limitTimeFlg = true

                do {
                  hashResult = await Web3.checkTransactionIsConfirming(
                    hash,
                    firstNonce,
                    product.currency,
                    publicAddress
                  )

                  if (hashResult.cancelFlg) {
                    limitTimeFlg = new Date().getTime() <= (startTime + (45 * 1000))
                  }
                  approveHash = await hashResult.hash
                }
                while (!approveHash && limitTimeFlg)

                if (!approveHash) {
                  this.close()
                  Confirmable.open({
                    content: t('error-messages:SOMETHING_WENT_WRONG'),
                    hideCancelButton: true
                  })
                  throw { error: 'SOMETHING_WENT_WRONG' }
                }

                const nonce = await Instance.web3.eth.getTransactionCount(publicAddress)
                await contractErc20.methods.approve(paymentAddress, amountMax)
                  .send({
                    from: publicAddress,
                    gasPrice: Math.floor(gasPrice * 1.2)
                  }, async (newError, newHash) => {
                    if (newError) {
                      return newError
                    }
                    let approveHashNew

                    let startTimeNew = new Date().getTime()
                    let hashResultNew
                    let limitTimeFlgNew = true
                    do {
                      hashResultNew = await Web3.checkTransactionIsConfirming(
                        newHash,
                        nonce,
                        product.currency,
                        publicAddress
                      )

                      if (hashResultNew.cancelFlg) {
                        limitTimeFlgNew = new Date().getTime() <= (startTimeNew + (45 * 1000))
                      }

                      approveHashNew = await hashResultNew.hash
                    }
                    while (!approveHashNew && limitTimeFlgNew)

                    const newAllowance = await Web3[product.currency === CURRENCIES.USDT
                      ? 'allowanceUsdt' : product.currency === CURRENCIES.GOKU
                        ? 'allowanceGoku' : 'allowanceWeth'](
                          publicAddress,
                          paymentAddress
                        )
                    if (!approveHash) {
                      MaskLoading.close()
                      Confirmable.open({
                        content: (
                          <>
                            {t('TRANSACTION_CANCEL')}
                          </>
                        ),
                        hideCancelButton: true
                      })
                      throw { error: 'TRANSACTION_CANCEL' }
                    } else if (newAllowance < product.price && approveHash) {
                      MaskLoading.close()
                      Confirmable.open({
                        content: (
                          <>
                            {t('common:NOT_ENOUGHT_MONEY_APPROVAL')}
                          </>
                        ),
                        hideCancelButton: true
                      })
                      throw { error: 'NOT_ENOUGHT_MONEY_APPROVAL' }
                    } else if (approveHash) {
                      await this._onValidate(product)
                      await this._purchase(signature, product)
                    }
                  })
              })

            return
          }

          if (allowance === 0) {
            const nonce = await Instance.web3.eth.getTransactionCount(publicAddress)
            await contractErc20.methods.approve(paymentAddress, amountMax)
              .send({
                from: publicAddress,
                gasPrice: Math.floor(gasPrice * 1.2)
              }, async (error, hash) => {
                if (error) {
                  return error
                }
                let approveHash
                let startTime = new Date().getTime()
                let hashResult
                let limitTimeFlg = true
                do {
                  hashResult = await Web3.checkTransactionIsConfirming(
                    hash,
                    nonce,
                    product.currency,
                    publicAddress
                  )

                  if (hashResult.cancelFlg) {
                    limitTimeFlg = new Date().getTime() <= (startTime + (40 * 1000))
                  }

                  approveHash = await hashResult.hash
                }
                while (!approveHash && limitTimeFlg)

                const newAllowance = await Web3[product.currency === CURRENCIES.USDT
                  ? 'allowanceUsdt' : product.currency === CURRENCIES.GOKU
                    ? 'allowanceGoku' : 'allowanceWeth'](
                      publicAddress,
                      paymentAddress
                    )
                if (!approveHash) {
                  MaskLoading.close()
                  Confirmable.open({
                    content: (
                      <>
                        {t('TRANSACTION_CANCEL')}
                      </>
                    ),
                    hideCancelButton: true
                  })
                  throw { error: 'TRANSACTION_CANCEL' }
                } else if (newAllowance < product.price && approveHash) {
                  MaskLoading.close()
                  Confirmable.open({
                    content: (
                      <>
                        {t('common:NOT_ENOUGHT_MONEY_APPROVAL')}
                      </>
                    ),
                    hideCancelButton: true
                  })
                  throw { error: 'NOT_ENOUGHT_MONEY_APPROVAL' }
                } else if (approveHash) {
                  await this._onValidate(product)
                  await this._purchase(signature, product)
                }
              })
          }

          // const isApproveMax = await onCheckApproveMax(t, authStore, product)
          // if (isApproveMax) {
          //   const signature = await onSign(authStore)
          //   await this._onValidate(product)
          //   await this._purchase(signature, product)
          // }
        }
      }
    } catch (e) {
      // if (this._orderId) {
      //   paymentStore.cancelPurchaseProduct({
      //     productId: product.id,
      //     orderId: this._orderId
      //   })
      // }
      if (e?.message?.includes('User denied transaction signature')) {
        this.isCancelUpdate = true
      }
      onError(e, t)

      // eslint-disable-next-line no-console
      console.error(e)
    }

    if (this.isCancelUpdate) {
      paymentStore.cancelPurchaseProductNormal({
        productId: product.id,
        orderId: this._orderId
      })
    }

    MaskLoading.close()
  }

  _onClickOffer = async () => {
    this._bigModal.open()
  }

  _onClickBuyProductChild = () => {
    this._productModal.open()
  }

  _onCheckoutProductChild = (data) => {
    this._productModal.close()

    this._onCheckout(data, true)
  }

  render() {
    const { authStore, product, t, i18n } = this.props
    const isCheckoutButtonActive = authStore.initialData.userId !== product?.userId
      && this._isTimeValid && product.isCheckoutValid && !product.productBuying

    const { gokuToJpy, usdToJpy } = authStore.initialData
    const jpyPrice = product?.tokenType === 'MULTI' ? product?.yenChildPrice : product.yenPrice

    return (
      <StyledDiv>
        <div className="_left">
          <div className="_price-box">
            <Typography className="_label">
              {product.status === 'SOLD' ? t('sold_price') : t('available_price')}
            </Typography>
            <div className="_inner">
              <Typography poppins className="_timer" size="large" bold>
                {product.isParent ? (
                  <>
                    <b>{Format.price(product?.childPrice)}</b>
                    {product.currency}
                  </>
                ) : (
                  <>
                    {(product.status === 'SOLD' && !product.buying) ? (
                      <div className="price-custom">
                        {i18n.language.toUpperCase() === 'JA' ? (
                          <>
                            <b>{Format.price(jpyPrice)} {t('common:yen')}</b>
                            {/* ({`${Format.price(product.yenPrice / gokuToJpy)}`} GOKU) */}
                            ({`${Format.price(product.price)}`} {product.currency})
                          </>
                        ) : (
                          <>
                            <b>{Format.price(jpyPrice / usdToJpy)} USD</b>
                            {/* ({`${Format.price(product.yenPrice / gokuToJpy)}`} GOKU) */}
                            ({`${Format.price(product.price)}`} {product.currency})
                          </>
                        )}
                      </div>
                    ) : (
                      <>
                        <b>{Format.price(product.price)}</b>
                        {product.currency}
                      </>
                    )}
                  </>
                )}
              </Typography>
              {product.status !== 'SOLD' && (
                <Typography className="_yen-price" bold>
                  {`（${Format.price(jpyPrice)}${t('common:yen')} / ${Format.price(jpyPrice / usdToJpy)} USD)`}
                </Typography>
              )}
            </div>
          </div>
          {product.owned && product.transactionId && (
            <div className="transaction-text">
              {t('transaction_hash')}:
              <Clickable
                className="transaction-link"
                onClick={() => this._onTransactionLinkClick(product.transactionId)}
              >
                {Misc.trimPublicAddress(product.transactionId, 6)}
              </Clickable>
            </div>
          )}
        </div>
        <div className="_right">
          {!product.owned && product.status !== 'SOLD' && (
            <>
              {product.productResell?.reselling ? (
                <>
                  {!product.isParent && (
                    <Button
                      disabled={!isCheckoutButtonActive}
                      className="checkout-button"
                      background={Colors.RED_3}
                      onClick={login(() => this._onCheckout(product, false))}
                    >
                      {i18n.language === 'en' ? (
                        <>
                          {t('buy_now')}
                          <span className="_small">
                            {t('with_expect_price')}
                          </span>
                        </>
                      ) : (
                        <>
                          <span className="_small">
                            {t('with_expect_price')}
                          </span>
                          {t('buy_now')}
                        </>
                      )}
                    </Button>
                  )}
                </>
              ) : (
                <>
                  {!product.isParent && (
                    <Button
                      disabled={!isCheckoutButtonActive}
                      className="checkout-button"
                      background={Colors.RED_3}
                      onClick={login(() => this._onCheckout(product, false))}
                    >
                      {product.holding ? t('during_trading') : t('buy_now')}
                    </Button>
                  )}
                </>
              )}
              {product.holding && (
                <Typography className="holding-caution" size="small">
                  （{t('holding_message')}）
                </Typography>
              )}
            </>
          )}
          {product.isAllowOffer && product.canOfferFlag && !product.owned && (
            <Button
              disabled={product.productBuying}
              className="offer-button"
              background="white"
              onClick={login(this._onClickOffer)}
            >
              {t('offer.put_a_limit_price')}
            </Button>
          )}
          {/* <OfferModal ref={(ref) => { this._bigModal = ref }} /> */}
          <ProductTogetherModal
            ref={(ref) => { this._productModal = ref }}
            productId={product.id}
            i18n={i18n}
            onCheckout={this._onCheckoutProductChild}
          />
        </div>
      </StyledDiv>
    )
  }
}

export default CheckoutBox
