import { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import classnames from 'classnames'
import { withTranslation } from 'react-i18next'
import { inject, observer } from 'mobx-react'

import Thumbnail from '@/components/thumbnail'
import Typography from '@/components/typography'
import { Colors, Images } from '@/theme'
import Format from '@/utils/format'
import Media from '@/utils/media'
import Misc from '@/utils/misc'

const StyledDiv = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 28px;
  
  .auction-end-noti-box {
    height: 76px;
    background-color: #fce010;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 6px;
    margin-bottom: 44px;
  }
  
  .product-description {
    margin-bottom: 5px;
  }
  
  .product-name {
    font-size: 32px;
    margin-bottom: 51px;
  }
  
  .user-info-box {
    max-width: 670px;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding-bottom: 17px;
    border-bottom: 2px solid rgb(0 0 0 / 15%);
    
    ._side-box {
      display: flex;
      align-items: center;
      
      .user-label {
        margin-right: 28px;
      }
      
      .user-avatar {
        margin-right: 3px;
      }
      
      .user-name {
        color: ${Colors.RED_2};
        font-size: 13px;
        margin-right: 8.5px;
      }
      
      .user-number {
        font-size: 14px;
      }
      
      .field {
        display: flex;
        align-items: center;
        margin-right: 34px;
        
        &:last-child {
          margin-right: 0;
        }
        
        img {
          margin-right: 9px;
        }
        
        .heat-label {
          color: ${Colors.RED_2};
        }
      }
    }
  }
  
  &.license {
    align-items: center;
    padding-top: 0;
    margin-bottom: 0;
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    margin-bottom: 20px;
    
    .product-name {
      font-size: 22px;
      margin-bottom: 23px;
    }

    .user-info-box {
       max-width: unset;
       padding-bottom: 12px;
       border-bottom: 1px solid rgb(0 0 0 / 10%);
      
      ._side-box {
        .user-label {
          margin-right: 20px;
          font-size: 13px;
        }

         .user-avatar {
           width: 20px;
           height: 20px;
         }
      }
    }
  }
`

@withTranslation('product_details')
@inject((stores) => ({
  product: stores.products.currentProductDetails
}))
@observer
class NameBox extends Component {
  static propTypes = {
    product: PropTypes.object,
    license: PropTypes.bool
  }

  state = {}

  render() {
    const { product, license, t } = this.props
    return (
      <StyledDiv className={classnames({ license })}>
        {product.productBid
          && !['NEW', 'INIT'].includes(product.productBid?.status)
          && !license
          && product.offerNftTransactionStatus !== 'SUCCESS'
          && (
            <div className="auction-end-noti-box">
              <Typography bold size="huge">
                {t('auction_has_ended')}
              </Typography>
            </div>
          )}
        <Typography size="large" secondary className="product-description">
          {product.title}
        </Typography>
        <Typography className="product-name" bold>
          {product.name}
        </Typography>
        <div className="user-info-box">
          <div className="_side-box">
            <Typography size="large" className="user-label">
              {t('owner')}
            </Typography>
            {product.isParent ? (
              <Typography>-</Typography>
            ) : (
              <>
                <Thumbnail
                  className="user-avatar"
                  size={30}
                  rounded
                  url={product.ownerAvatar}
                  placeholderUrl={Images.USER_PLACEHOLDER}
                />
                <Typography className="user-name" bold>
                  @ {product.ownerName || Misc.trimPublicAddress(product.user.publicAddress, 4)}
                </Typography>
              </>
            )}
          </div>
          <div className="_side-box">
            <div className="field">
              <img src={Images.GRAY_EYE_ICON} alt="" />
              <Typography secondary>
                {Format.abridgeNumber(product.countView)}
              </Typography>
            </div>
            <div className="field">
              <img src={Images.GRAY_WISH_ICON} alt="" />
              <Typography secondary>
                {Format.abridgeNumber(product?.wishCount || 0)}
              </Typography>
            </div>
            {/* <div className="field"> */}
            {/*  <img src={Images.COLOR_FIRE_ICON} alt="" /> */}
            {/*  <Typography className="heat-label"> */}
            {/*    {Format.abridgeNumber(product?.heatCount || 0)} */}
            {/*  </Typography> */}
            {/* </div> */}
          </div>
        </div>
      </StyledDiv>
    )
  }
}

export default NameBox
