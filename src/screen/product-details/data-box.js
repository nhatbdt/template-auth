import { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { withTranslation } from 'react-i18next'
import { inject, observer } from 'mobx-react'

import Clickable from '@/components/clickable'
import Typography from '@/components/typography'
// import Thumbnail from '@/components/thumbnail
import TextSeeMore from '@/components/text-see-more'
import { Colors, Images } from '@/theme'
import Misc from '@/utils/misc'
import Configs from '@/configs'

const StyledDiv = styled.div`
  display: flex;
  flex-direction: column;

  .data-section {
    margin-bottom: 28px;
    border-bottom: 2px solid rgb(0 0 0 / 15%);
    padding-bottom: 29px;

    &:last-child {
      margin-bottom: 0;
      border-bottom: none;
    }

    ._top-box {
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin-bottom: 27px;

      ._title {
        color: ${Colors.BLACK};
      }

      ._name {
        color: #b7b7b7;
      }
    }

    ._description {
      white-space: pre-wrap;
      word-break: break-word;
      color: ${Colors.BLACK};
      opacity: 0.7;
      margin-bottom: 29px;
    }

    .field-group {
      .field {
        margin-bottom: 15px;
        display: flex;
        font-size: 13px;


        &:last-child {
          margin-bottom: 0;
        }

        ._name {
          width: 135px;
          color: #b7b7b7;
          font-weight: bold;
        }

        ._value {
          flex: 1;
          word-break: break-word;
          white-space: pre-wrap;
          color: ${Colors.BLACK};
          
          &.red {
            color: ${Colors.RED_2};
          }
          
          &.right {
            text-align: right;
          }

          ._copyright-icon {
            margin-left: 5px;
          }
        }
        
        &.vertical {
          flex-direction: column;
          
          ._name {
            width: auto;
            margin-bottom: 6px;
          }
        }
      }
    }

    .transaction-box {
      background-color: #fff8f7;
      border-radius: 6px;
      padding: 22px 26px 25px;
      margin-bottom: 28px;

      ._transaction-item {
        display: flex;
        align-items: center;
        padding-bottom: 16px;
        border-bottom: dashed 1px #e2e2e2;
        margin-bottom: 20px;

        &:last-child {
          padding-bottom: 0;
          margin-bottom: 0;
          border-bottom: none;
        }

        ._label {
          font-size: 13px;
          width: 101px;
        }

        ._value {
          font-size: 13px;
          width: 99px;
          white-space: pre-line;

          &.red {
            color: ${Colors.RED_2};
          }
        }

        .right-icons {
          color: #cacaca;
        }

        .left {
          display: flex;
          justify-content: flex-end;
          align-items: center;
          flex: 1;

          ._avatar {
            margin-right: 9px;
          }

          ._name {
            font-size: 13px;
            color: ${Colors.RED_2};
          }
        }
      }
    }
    
    ._frame-box {
      flex: 1;
      display: flex;
      justify-content: space-between;
      
      ._frame-image {
        padding-right: 40px;
        
        ._frame-outer {
          width: 160px;
          height: 160px;
          display: flex;
          justify-content: center;
          align-items: center;
          
          ._big-frame {
            border: 14px solid #e6e6e6;
            position: relative;
            
            .big-size-width {
              position: absolute;
              top: -28px;
              left: 50%;
              transform: translateX(-50%);
            }
            
            .big-size-height {
              position: absolute;
              top: 50%;
              right: -35px;
              transform: translateY(-50%);
            }
            
            .small-size-width {
              position: absolute;
              top: 2px;
              left: 50%;
              transform: translateX(-50%);
            }
            
            .small-size-height {
              position: absolute;
              top: 50%;
              right: 10px;
              transform: translateY(-50%);
            }
          }
        }
      }
    }
  }
`

const LineStyle = styled.div`
  height: 1px;
  background-image: linear-gradient(to right,rgb(230,228,228) 60%,rgba(255,255,255,0) 0%);
  background-size: 6px 3px;
  width: 100%;
  margin: 30px 0;
`

@withTranslation('product_details')
@inject((stores) => ({
  product: stores.products.currentProductDetails
}))
@observer
class DataBox extends Component {
  static propTypes = {
    product: PropTypes.object
  }

  state = {}

  _onOwnerAddressClick = (publicAddress) => {
    window.open(`${Configs.ETHERSCAN_DOMAIN}/address/${publicAddress}`)
  }

  render() {
    const { product, t } = this.props
    let { bigSizeWidth, bigSizeHeight, smallSizeWidth, smallSizeHeight } = product
    let cssWidth = '100%'
    let cssHeight = '100%'

    if (bigSizeWidth > bigSizeHeight) {
      cssWidth = '100%'
      cssHeight = `${(bigSizeHeight / bigSizeWidth) * 100}%`
    }

    if (bigSizeWidth < bigSizeHeight) {
      cssHeight = '100%'
      cssWidth = `${(bigSizeWidth / bigSizeHeight) * 100}%`
    }

    return (
      <StyledDiv>
        <div className="data-section">
          <div className="field-group">
            <div className="field">
              <div className="_name">
                {t('basic_info.name')}
              </div>
              <div className="_value">
                {product.name || '-'}
              </div>
            </div>
            <div className="field">
              <div className="_name">
                {t('basic_info.work_type')}
              </div>
              <div className="_value">
                {product.workType || '-'}
              </div>
            </div>
          </div>
          <LineStyle />
          <div className="_top-box">
            <Typography className="_name">
              {t('basic_info.basic_info')}
            </Typography>
            {/* <Clickable> */}
            {/*  <img src={Images.GRAY_SUB_ICON} alt="" /> */}
            {/* </Clickable> */}
          </div>
          <TextSeeMore text={product.description} />
          <div className="field-group">
            <div className="field">
              <div className="_name">
                {t('basic_info.title')}
              </div>
              <div className="_value">
                {product.title || '-'}
              </div>
            </div>

            <div className="field">
              <div className="_name">
                {t('basic_info.character')}
              </div>
              <div className="_value">
                {product.character || '-'}
              </div>
            </div>
            <div className="field">
              <div className="_name">
                {t('basic_info.year')}
              </div>
              <div className="_value">
                {product.manufactureYear ? `${product.manufactureYear}年` : '-'}
              </div>
            </div>
            <div className="field">
              <div className="_name">
                {t('basic_info.original')}
              </div>
              <div className="_value">
                {product.original || '-'}
              </div>
            </div>
            <div className="field">
              <div className="_name">
                {t('basic_info.original_studio')}
              </div>
              <div className="_value">
                {product.originalStudio || '-'}
              </div>
            </div>
            <div className="field">
              <div className="_name">
                {t('basic_info.copyright_owner')}
              </div>
              <div className="_value">
                <p>
                  {product.copyrightOwner || '-'}
                  {product.license && (
                    <img className="_copyright-icon" src={Images.BLUE_CIRCLE_TICK_ICON} alt="" />
                  )}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="data-section">
          <div className="_top-box">
            <Typography className="_title">
              {t('transaction_info.transaction_info')}
            </Typography>
            {/* <Clickable> */}
            {/*  <img src={Images.GRAY_SUB_ICON} alt="" /> */}
            {/* </Clickable> */}
          </div>
          {/* <div className="transaction-box"> */}
          {/*  <div className="_transaction-item"> */}
          {/*    <Typography secondary className="_label"> */}
          {/*      取引手数料 */}
          {/*    </Typography> */}
          {/*    <Typography className="_value red" bold> */}
          {/*      0.001% */}
          {/*    </Typography> */}
          {/*    <div className="right-icons"> */}
          {/*      {'>>>'} */}
          {/*    </div> */}
          {/*    <div className="left"> */}
          {/*      <Thumbnail */}
          {/*        className="_avatar" */}
          {/*        size={30} */}
          {/*        rounded */}
          {/*        url="https://picsum.photos/id/156/100" */}
          {/*      /> */}
          {/*      <Typography className="_name" bold> */}
          {/*        @ A.Toriyama */}
          {/*      </Typography> */}
          {/*    </div> */}
          {/*  </div> */}
          {/*  <div className="_transaction-item"> */}
          {/*    <Typography secondary className="_label"> */}
          {/*      現物保管者 */}
          {/*    </Typography> */}
          {/*    <Typography className="_value"> */}
          {/*      RAKUZA財団 */}
          {/*    </Typography> */}
          {/*    <div className="right-icons" /> */}
          {/*    <div className="left"> */}
          {/*      <Typography className="_name"> */}
          {/*        MAPで確認 */}
          {/*      </Typography> */}
          {/*    </div> */}
          {/*  </div> */}
          {/* </div> */}
          <div className="_frame-box">
            <div className="field-group">
              <div className="field vertical">
                <div className="_name">
                  {t('transaction_info.certificate')}
                </div>
                <div className="_value">
                  {product.categories[0]?.name}
                </div>
              </div>
              <div className="field vertical">
                <div className="_name">
                  {t('transaction_info.forehead_size')}
                </div>
                <div className="_value">
                  {bigSizeWidth}mm×{bigSizeHeight}mm
                </div>
              </div>
              <div className="field vertical">
                <div className="_name">
                  {t('transaction_info.product_size')}
                </div>
                <div className="_value">
                  {smallSizeWidth}mm×{smallSizeHeight}mm
                </div>
              </div>
            </div>
            <div className="_frame-image">
              <div className="_frame-outer">
                <div className="_big-frame" style={{ width: cssWidth, height: cssHeight }}>
                  <Typography size="tiny" className="big-size-width">{bigSizeWidth}</Typography>
                  <Typography size="tiny" className="big-size-height">{bigSizeHeight}</Typography>
                  <Typography size="tiny" className="small-size-width">{smallSizeWidth}</Typography>
                  <Typography size="tiny" className="small-size-height">{smallSizeHeight}</Typography>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="data-section">
          <div className="_top-box">
            <Typography className="_title">
              {t('chain_info.chain_info')}
            </Typography>
            {/* <Clickable> */}
            {/*  <img src={Images.GRAY_SUB_ICON} alt="" /> */}
            {/* </Clickable> */}
          </div>
          <div className="field-group">
            <div className="field">
              <div className="_name">
                {t('chain_info.owner_address_1')}
              </div>
              <div className="_value red right">
                {Misc.trimPublicAddress(product?.user?.publicAddress, 4)}
              </div>
            </div>
            <div className="field">
              <div className="_name">
                {t('chain_info.contract_address_1')}
              </div>
              <div className="_value red right">
                <Clickable onClick={() => this._onOwnerAddressClick(product.contractAddress)}>
                  {Misc.trimPublicAddress(product.contractAddress, 4)}
                </Clickable>
              </div>
            </div>
            <div className="field">
              <div className="_name">
                {t('chain_info.token_id')}
              </div>
              <div className="_value right">
                {product.tokenId || '-'}
              </div>
            </div>
            <div className="field">
              <div className="_name">
                {t('chain_info.blockchain')}
              </div>
              <div className="_value right">
                Ethereum
              </div>
            </div>
            <div className="field">
              <div className="_name">
                {t('chain_info.token')}
              </div>
              <div className="_value right">
                RAKUZA
              </div>
            </div>
          </div>
        </div>
      </StyledDiv>
    )
  }
}

export default DataBox
