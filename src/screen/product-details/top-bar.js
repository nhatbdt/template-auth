import { Component } from 'react'
import classnames from 'classnames'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Popover } from 'antd'
import {
  FacebookShareButton,
  TwitterShareButton,
  LineShareButton
} from 'react-share'
import { withTranslation } from 'react-i18next'
import { inject, observer } from 'mobx-react'

import Container from '@/components/container'
import Clickable from '@/components/clickable'
import Tag from '@/components/tag'
import { Images } from '@/theme'
import Configs from '@/configs'
import Media from '@/utils/media'

const StyledDiv = styled.div`
  margin-bottom: 40px;
  
  ._content {
    display: flex;
    justify-content: space-between;
    padding-top: 28px;
    
    .side-box {
      display: flex;
      
      .tag {
        margin-right: 16px;
      }
      
      .button-group {
        display: flex;
        box-shadow: 0 0 12px 0 rgba(0, 0, 0, 0.11);
        height: 32px;
        border-radius: 17px;
        overflow: hidden;
        background-color: white;
        margin-right: 10px;
        
        &:last-child {
          margin-right: 0;
        }
        
        div {
          height: 100%;
          width: 37px;
          display: flex;
          justify-content: center;
          align-items: center;
          border-right: 1px solid #70707012;
          
          &.rounded {
            width: 33px;
            
            img {
              width: 18px;
              margin-left: 1px;
            }
          }
        }

        .wish-image {
          width: 23px;
          height: 23px;
        }

        .un-wish {
          opacity: 0.5;
        }
      }
    }
  }
  
  &.absolute {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
  }
  
  &.license {
    margin-bottom: 0;
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    margin-bottom: 0;
    
    ._content {
      .side-box {
        flex-wrap: wrap;
        margin-right: 15px;
        
        .tag {
          margin-right: 5px;
          margin-bottom: 5px;
        }
      }
    }
  }
`

const ShareBox = styled.div`
  display: flex;
  flex-direction: column;
  
  button {
    margin-bottom: 6px;
    
    &:last-child {
      margin-bottom: 0;
    }
  }
`

@withTranslation('common')
@inject((stores) => ({
  product: stores.products.currentProductDetails,
  productsStore: stores.products,
  authStore: stores.auth,
  usersStore: stores.users
}))
@observer
class TopBar extends Component {
  static propTypes = {
    absolute: PropTypes.bool,
    license: PropTypes.bool,
    product: PropTypes.object,
    productsStore: PropTypes.object,
    authStore: PropTypes.object
  }

  state = {}

  _onToggleFavourite = async () => {
    const { productsStore, product, authStore } = this.props
    const res = await productsStore.createInteraction({
      productId: product?.id,
      actionCode: 'WISH'
    })
    console.log("res---------->", res);
    if (res?.success) {
      authStore.setWishCount(product?.isUserWished ? 1 : -1)
    }
  }

  render() {
    const { absolute, product, license, t, i18n, authStore } = this.props
    const shareUrl = "https://facebook.com"

    return (
      <StyledDiv className={classnames({ absolute, license })}>
        <Container>
          <div className="_content">
            <div className="side-box">
              <Tag
                size="big"
                type={product.holding ? 'holding' : product.status === 'NEW'
                  ? (product.productBid ? 'before-auction' : 'comming-soon')
                  : product.status === 'SALE'
                    ? (product.productBid
                      ? 'during-auction' : product.productResell?.reselling
                        ? 'resale' : 'sale'
                    )
                    : product.buying ? 'during-trading' : 'sold'}
              >
                {product.holding ? t('product_status.holding') : product.status === 'NEW'
                  ? (product.productBid ? t('product_status.before_auction') : t('product_status.coming_soon'))
                  : product.status === 'SALE'
                    ? (product.productBid
                      ? t('product_status.during_auction') : product.productResell?.reselling
                        ? t('product_status.resale') : t('product_status.sale')
                    )
                    : product.buying ? t('product_status.during_trading') : t('product_status.sold')}
              </Tag>
              {product.imageType && (
                <Tag
                  size="big"
                  background="#555555"
                  showIcon
                  active={!!product.license}
                >
                  {product.imageType}
                </Tag>
              )}
              {product.categories?.map((category, index) => (
                <Tag key={index} size="big" background="#f4f4f4" light>{category.name}</Tag>
              ))}
            </div>
            <div className="side-box">
              <div className="button-group">
                {authStore.loggedIn && (
                  <Clickable onClick={this._onToggleFavourite} className={`${!product.isUserWished && 'un-wish'}`}>
                    <img
                      src={Images.RED_WISH_ICON}
                      alt=""
                      className="wish-image"
                    />
                  </Clickable>
                )}

                {/* <Clickable> */}
                {/*  <img src={Images.GRAY_ROTATE_ICON} alt="" /> */}
                {/* </Clickable> */}
                {/* <Clickable> */}
                {/*  <img src={Images.GRAY_STAR_ICON} alt="" /> */}
                {/* </Clickable> */}
                <Popover
                  content={(
                    <ShareBox>
                      <FacebookShareButton
                        url={shareUrl}
                      >
                        Facebook
                      </FacebookShareButton>
                      <TwitterShareButton
                        url={shareUrl}
                      >
                        Twitter
                      </TwitterShareButton>
                      <LineShareButton
                        url={shareUrl}
                      >
                        Line
                      </LineShareButton>
                    </ShareBox>
                  )}
                  placement="bottomRight"
                  trigger="click"
                >
                  <Clickable className="rounded">
                    <img src={Images.GRAY_SHARE_ICON} alt="" />
                  </Clickable>
                </Popover>
              </div>
              {/* <div className="button-group"> */}
              {/*  <Clickable className="rounded"> */}
              {/*    <img src={Images.BLACK_THREE_DOT_ICON} alt="" /> */}
              {/*  </Clickable> */}
              {/* </div> */}
            </div>
          </div>
        </Container>
      </StyledDiv>
    )
  }
}

export default TopBar
