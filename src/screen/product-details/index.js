import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { inject, observer } from 'mobx-react'
import { withTranslation, useTranslation } from 'react-i18next'
import styled from 'styled-components'

import Page from '@/components/page'
import TabBar from '@/components/tab-bar'
import Container from '@/components/container'
import Loading from '@/components/loading'
import Misc from '@/utils/misc'
import { Images } from '@/theme'
import { login } from '@/utils/conditions'
import Confirmable from '@/components/confirmable'
import Media from '@/utils/media'
import TopBar from './top-bar'
import AvatarBox from './avatar-box'
import NameBox from './name-box'
import DataBox from './data-box'
import CheckoutBox from './checkout-box'
import BackBanner from './back-banner'
import SellBox from './sell-box'
import ChartBox from './chart-box'
import TransactionHistoriesBox from './transaction-histories-box'
// import ProductChildHistories from './buy-together/buy-together-history'

const StyledDiv = styled.div`
  /* padding-bottom: 125px; */
  .tab-bar {
    position: relative;
    background-image: url("${Images.MY_PAGE_BACKGROUND}");
    z-index: 1;
    
    .tab-bar-mask {
      background-color: rgb(255 255 255 / 70%);
      height: 71px;
      display: flex;
      
      .container {
        display: flex;
        justify-content: space-between;
        align-items: center;
        
        > div {
          &:first-child {
            height: 71px;
            
            .tab-item {
              padding-bottom: 0;
            }
          }
        }
      }
    }
  }
  
  .top-section {
    position: relative;
  }
  
  .name-section {
    padding-top: 50px;
    margin-bottom: 55px;
  }
  
  .content-section {
    display: flex;
    
    ._side {
      margin-right: 89px;
      
      &:first-child {
        width: 463px;
      }
      
      &:last-child {
        margin-right: 0;
        flex: 1;
      }

      .history-container {
        display: flex;

        table {
          width: 100%;
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .tab-bar {
      .tab-bar-mask {
        height: 60px;

        .container {
          justify-content: space-between;
          
          > div {
            flex: 1;
            justify-content: space-between;
            
            &:first-child {
              display: none;
            }
          }
        }
      }
    }
    
    .content-section {
      flex-direction: column-reverse;

      ._side {
        margin-right: 0;

        &:first-child {
          width: auto;
          flex: 1;
        }

        .history-container {
          display: block;
  
          table {
            width: 100%;
          }
        }

      }
    }
  }
`

@withTranslation()
@inject((stores) => ({
  productsStore: stores.products,
  authStore: stores.auth,
  product: stores.products.currentProductDetails
}))
@observer
class ProductDetails extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
    authStore: PropTypes.object,
    product: PropTypes.object
  }

  constructor(props) {
    super(props)
    const { match } = props

    this.state = {
      initing: true
    }

    this._isShowBackBanner = match.path === '/:lang/events/:eventId/:bannerId/product-details/:id'
  }

  componentDidMount() {
    this._onGetProductDetail()

    this._updatePriceInterval = setInterval(this._updatePrice, 600000)
  }

  componentWillUnmount() {
    clearInterval(this._updatePriceInterval)
  }

  _updatePrice = async () => {
    const { productsStore, product } = this.props

    const { success, data } = await productsStore.getProductPrice({ productId: product.id })

    if (success && product.price !== data.price) {
      product.setData({
        price: data.price,
        yenPrice: data.yenPrice
      })
    }
  }

  _onGetProductDetail = async () => {
    const { productsStore, match, history, i18n } = this.props

    const { data, success } = await productsStore.getProductDetails({
      id: match.params.id,
      langKey: i18n.language.toUpperCase()
    })

    if (success) {
      if (!data) {
        history.replace('/')

        return
      }
    }

    this.setState({
      initing: false
    })
  }

  _onTabChange = (key) => {
    const { history } = this.props

    history.push(`/my-page/${key}`)
  }

  _onHeatIt = () => {
    const { product, productsStore, t } = this.props

    productsStore.createInteraction({
      actionCode: 'HEAT_IT',
      productId: product.id
    }).then(({ success, data }) => {
      if (success) {
        product.updateHeatCount()
      } else {
        const errorMsg = t(`product_details:offers.${data.error}`)
        Confirmable.open({
          content: errorMsg,
          hideCancelButton: true
        })
      }
    })
  }

  _renderMyPageTabBar = () => {
    const { t, authStore, product } = this.props
    if (authStore.initialData.userId !== product?.userId || !product.productResell) return null

    const TABS = [{
      name: t('user_profile:purchased_products'),
      key: 'my-product',
      icon: Images.BLACK_IMAGE_ICON
    }, {
      name: t('user_profile:gifts'),
      key: 'gifts',
      icon: Images.BLACK_GIFT_ICON
    }]

    return (
      <div className="tab-bar">
        <div className="tab-bar-mask">
          <Container>
            <TabBar
              items={TABS}
              activeTabKey="my-product"
              onChange={this._onTabChange}
            />
            <SellBox />
          </Container>
        </div>
      </div>
    )
  }

  _renderInfoSide = () => {
    const { product } = this.props

    return (
      <div className="_side">
        <NameBox />
        {product?.productBid && product.offerNftTransactionStatus !== 'SUCCESS' ? (
          // <BidBox />
          <>nobid</>
        ) : (
          <>
            <CheckoutBox />
            {/* <OfferNotificationBar /> */}
            {!product?.isMulti && (
              <ChartBox />
            )}
          </>
        )}
        {!product?.isMulti && (
          // <OffersBox />
          <>no OffersBox</>
        )}
        {/* {product?.isMulti && (
          <ProductChildHistories />
        )} */}
      </div>
    )
  }

  _renderLicenseTemplate = () => {
    const { product } = this.props

    return (
      <>
        <section className="top-section">
          <AvatarBox license onClick={login(this._onHeatIt)} />
          <TopBar absolute license />
        </section>
        <Container>
          <div className="name-section">
            <NameBox license />
          </div>
          <div className="content-section">
            <div className="_side">
              <DataBox />
            </div>
            <div className="_side">
              {product.productBid && product.offerNftTransactionStatus !== 'SUCCESS' ? (
                // <BidBox />
                <>no bid</>
              ) : (
                <>
                  <CheckoutBox />
                  {/* <OfferNotificationBar /> */}
                  {!product.isMulti && (
                    <ChartBox />
                  )}
                </>
              )}
              {!product.isMulti && (
                // <OffersBox />
                <>No offer box</>
              )}
              {product.isMulti && (
                // <ProductChildHistories />
                <>No ProductChildHistories</>
              )}
            </div>
          </div>
          {!product?.isParent && (
            <TransactionHistoriesBox />
          )}
          {this._isShowBackBanner && (
            <BackBanner />
          )}
        </Container>
      </>
    )
  }

  _renderNonLicenseTemplate = () => {
    const { product } = this.props

    return (
      <>
        <section className="top-section">
          <TopBar />
        </section>
        <Container>
          <div className="content-section">
            <div className="_side">
              <AvatarBox onClick={login(this._onHeatIt)} />
              {Misc.isMobile && this._renderInfoSide()}
              <DataBox />
            </div>
            {!Misc.isMobile && this._renderInfoSide()}
          </div>
          {!product?.isParent && (
            <TransactionHistoriesBox />
          )}
          {this._isShowBackBanner && (
            <BackBanner />
          )}
        </Container>
      </>
    )
  }

  _renderContent = () => {
    const { product } = this.props

    return !!product?.id && (
      <>
        {this._renderMyPageTabBar()}
        {(product?.license || product?.isMulti) ? this._renderLicenseTemplate() : this._renderNonLicenseTemplate()}
      </>
    )
  }

  render() {
    const { initing } = this.state

    return (
      <Page>
        <StyledDiv>
          {initing ? <Loading /> : this._renderContent()}
        </StyledDiv>
      </Page>
    )
  }
}

export default inject((stores) => ({
  authStore: stores.auth
}))(observer(({ authStore, ...props }) => {
  const { i18n } = useTranslation()

  return <ProductDetails {...props} key={`${i18n.language}-${authStore.loggedIn}`} />
}))
