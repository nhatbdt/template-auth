import { useEffect, useState } from 'react'
import { withRouter } from 'react-router-dom'
import { withTranslation } from 'react-i18next'

import Banner from '@/components/banner'
import { firestore } from '@/services/firebase'
import { history } from '@/store'

export default withTranslation()(withRouter(({ i18n, match }) => {
  const [item, setItem] = useState()

  const fetchData = async () => {
    const banners = []

    const snapshot = await firestore.bannersLangs()
      .where('lang', '==', i18n.language.toUpperCase())
      .where('bannerId', '==', match.params.bannerId)
      .get()

    snapshot.forEach((doc) => banners.push(doc.data()))

    setItem(banners[0])
  }

  useEffect(() => {
    fetchData()
  }, [])

  if (!item) return null

  return (
    <Banner
      hideSeeMoreLink
      item={item}
      onClickDetail={() => history.goBack()}
    />
  )
}))
