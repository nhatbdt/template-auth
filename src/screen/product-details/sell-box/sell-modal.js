import { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { Formik, Form } from 'formik'
import * as yup from 'yup'
import { inject, observer } from 'mobx-react'
import { Translation } from 'react-i18next'
import classnames from 'classnames'
import lodash from 'lodash'
import moment from 'moment'
import numeral from 'numeral'

import Input from '@/components/input'
import DateTimePicker from '@/components/date-time-picker'
import Modal from '@/components/modal'
import Clickable from '@/components/clickable'
import Loading from '@/components/loading'
import Button from '@/components/button'
import HintTooltip from '@/components/hint-tooltip'
import Checkbox from '@/components/checkbox'
import Field from '@/components/field'
import Confirmable from '@/components/confirmable'
import Typography from '@/components/typography'
import Web3 from '@/utils/web3other'
import { Colors, Images } from '@/theme'
import Format from '@/utils/format'
import Configs from '@/configs'
import CURRENCIES from '@/constants/currencies'
import { onSign } from '../utils/checkout-normal'
import StyledDiv from './styled'

const SUPPORT_CURRENCIES = [CURRENCIES.GOKU, CURRENCIES.USDT, CURRENCIES.ETH]

const normalValidationSchema = yup.object().shape({
  sellPrice: yup.number()
    .nullable()
    .required('RPICE_REQUIRED'),
  acceptTerms: yup.bool().oneOf([true])
})

const auctionValidationSchema = yup.object().shape({
  sellPrice: yup.number()
    .nullable()
    .required('RPICE_REQUIRED'),
  acceptTerms: yup.bool().oneOf([true]),
  startTime: yup.string().nullable().required('START_TIME_REQUIRED'),
  endTime: yup.string().nullable().required('END_TIME_REQUIRED')
})

@inject((stores) => ({
  authStore: stores.auth,
  productsStore: stores.products,
  sellStore: stores.sell,
  product: stores.products.currentProductDetails

}))
@observer
class SellModal extends Component {
  static propTypes = {
    product: PropTypes.object,
    authStore: PropTypes.object,
    sellStore: PropTypes.object,
    productsStore: PropTypes.object
  }

  constructor(props) {
    super(props)

    this.state = {
      isOpen: false,
      loading: false,
      initing: true,
      priceRates: null,
      currentTab: 'NORMAL',
      selectedCurrency: CURRENCIES.GOKU,
      registerSellData: null,
      initialValues: {
        acceptTerms: false
      }
    }
  }

  open = async () => {
    const { productsStore, sellStore, product } = this.props

    this.setState({
      isOpen: true,
      currentTab: 'NORMAL',
      selectedCurrency: CURRENCIES.GOKU,
      initing: true
    })

    const [registerSellResult, getPriceRateResult] = await Promise.all([
      sellStore.registerSell({
        productId: product.id,
        resellType: 'NORMAL',
        currency: CURRENCIES.GOKU
      }),
      productsStore.getPriceRate()
    ])

    if (getPriceRateResult.success && registerSellResult.success) {
      const priceRates = lodash.reduce(getPriceRateResult.data, (result, item) => {
        result[item.name] = +item.value
        return result
      }, {})

      const convertedCommissionFee = this._convertCommissionFee(
        registerSellResult.data.commissionFee,
        registerSellResult.data.currency,
        CURRENCIES.GOKU,
        priceRates
      )

      this.setState({
        priceRates,
        registerSellData: registerSellResult.data,
        convertedCommissionFee,
        initing: false
      })
    } else {
      this.close()
    }
  }

  close = () => this.setState({ isOpen: false })

  _onTabSelect = async (item) => {
    const { sellStore, product } = this.props
    const { priceRates, selectedCurrency, currentTab } = this.state

    if (currentTab === item.value) return

    this.setState({
      initing: true
    })

    let startTime = moment().set('second', 0)
    let modStartTimeMinute = startTime.minute() % 5
    modStartTimeMinute = modStartTimeMinute === 0 ? 5 : modStartTimeMinute
    startTime = startTime.add(5 - modStartTimeMinute + 10, 'm')

    const initialValues = {
      acceptTerms: false,
      startTime,
      endTime: moment(startTime).add(30, 'm')
    }

    const registerSellResult = await sellStore.registerSell({
      productId: product.id,
      resellType: item.value,
      currency: selectedCurrency
    })

    if (registerSellResult.success) {
      const convertedCommissionFee = this._convertCommissionFee(
        registerSellResult.data.commissionFee,
        registerSellResult.data.currency,
        selectedCurrency,
        priceRates
      )

      this.setState({
        registerSellData: registerSellResult.data,
        convertedCommissionFee,
        initing: false,
        currentTab: item.value,
        initialValues
      })
    } else {
      this.setState({
        initing: false,
        currentTab: item.value,
        initialValues
      })
    }
  }

  _convertCommissionFee = (fee, originCurrency, newCurrency, priceRates) => {
    const { ETH_TO_GOKU, NBNG_TO_GOKU, USD_TO_JPY } = priceRates

    const gokuCommissionFee = this._getGokuPrice(
      fee,
      originCurrency,
      priceRates
    )

    return newCurrency === CURRENCIES.ETH
      ? gokuCommissionFee / ETH_TO_GOKU
      : newCurrency === CURRENCIES.NBNG
        ? gokuCommissionFee / NBNG_TO_GOKU
        : newCurrency === CURRENCIES.USDT ? gokuCommissionFee / USD_TO_JPY
          : gokuCommissionFee
  }

  _sell = async (signature, values, t) => {
    const { sellStore, product, authStore } = this.props
    const { selectedCurrency, registerSellData, currentTab } = this.state

    const approvePublicAddress = Configs.RAKUZA_CONTRACT_ADDRESS
    const contractAddressAdmin = Configs.ERC721_CONTRACT_ADDRESS

    await Web3.requireApproveOnMobile(t)

    let approveTokenTransactionHash

    // const approveTokenTransactionHash = await Web3.approveErc721(
    //   authStore.initialData.publicAddress,
    //   approvePublicAddress,
    //   product.tokenId,
    //   t
    // )

    const Instance = await Web3.getWeb3Instance()

    const nonce = await Instance.web3.eth.getTransactionCount(authStore.initialData.publicAddress)
    const gasPrice = await Instance.web3.eth.getGasPrice()
    // console.log('instanceMethod', userInstance)

    await Instance.erc721contract.methods.approve(
      approvePublicAddress,
      product.tokenId.toString()
    ).send({
      from: authStore.initialData.publicAddress,
      gasPrice: Math.floor(gasPrice * 1.3)
    }, async (error, hash) => {
      console.log("err", error, hash);
      if (error) {
        return error
      }

      let startTime = new Date().getTime()
      let hashResult
      let limitTimeFlg = true
      do {
        hashResult = await Web3.checkTransactionIsConfirming(
          hash,
          nonce,
          product.currency,
          authStore.initialData.publicAddress,
          'Approval',
          contractAddressAdmin
        )

        if (hashResult.cancelFlg) {
          limitTimeFlg = new Date().getTime() <= (startTime + (45 * 1000))
        }
        approveTokenTransactionHash = await hashResult.hash
      }
      while (!approveTokenTransactionHash && limitTimeFlg)

      if (approveTokenTransactionHash) {
        const sellResult = await sellStore.sell(currentTab === 'NORMAL' ? {
          currency: selectedCurrency,
          signature,
          approveTokenTransactionHash,
          productId: product.id,
          productResellId: registerSellData.productResellId,
          resellType: 'NORMAL',
          price: values.sellPrice
        } : {
          currency: selectedCurrency,
          signature,
          approveTokenTransactionHash,
          productId: product.id,
          productResellId: registerSellData.productResellId,
          resellType: 'AUCTION',
          startTime: values.startTime.format('x'),
          endTime: values.endTime.format('x'),
          price: values.sellPrice,
          expectDisplayPrice: values.expectDisplayPrice,
          expectPrice: values.expectPrice
        })

        if (!sellResult.success) {
          throw sellResult.data
        } else {
          product.productResell.setData({
            resellCancelable: true,
            confirmResell: true
          })
          product.setData({
            canOfferFlag: false,
            offerNftTransactionStatus: null
          })

          Confirmable.open({
            content: t('product_details:sell.sell_success'),
            hideCancelButton: true,
            otherFunctionHanldle: () => { window.location.reload() }
          })
        }
        this.close()

        this.setState({
          loading: false
        })
      } else {
        this.close()

        this.setState({
          loading: false
        })
        Confirmable.open({
          content: t('error-messages:SOMETHING_WENT_WRONG'),
          hideCancelButton: true
        })
      }
    })

    // const sellResult = await sellStore.sell(currentTab === 'NORMAL' ? {
    //   currency: selectedCurrency,
    //   signature,
    //   approveTokenTransactionHash,
    //   productId: product.id,
    //   productResellId: registerSellData.productResellId,
    //   resellType: 'NORMAL',
    //   price: values.sellPrice
    // } : {
    //   currency: selectedCurrency,
    //   signature,
    //   approveTokenTransactionHash,
    //   productId: product.id,
    //   productResellId: registerSellData.productResellId,
    //   resellType: 'AUCTION',
    //   startTime: values.startTime.format('x'),
    //   endTime: values.endTime.format('x'),
    //   price: values.sellPrice,
    //   expectDisplayPrice: values.expectDisplayPrice,
    //   expectPrice: values.expectPrice
    // })

    // if (!sellResult.success) {
    //   throw sellResult.data
    // } else {
    //   product.productResell.setData({
    //     resellCancelable: true,
    //     confirmResell: true
    //   })
    //   product.setData({
    //     canOfferFlag: false,
    //     offerNftTransactionStatus: null
    //   })

    //   Confirmable.open({
    //     content: t('product_details:sell.sell_success'),
    //     hideCancelButton: true
    //   })
    // }

    // return null
  }

  _onSubmit = async (values, t) => {
    const { product, authStore } = this.props
    const { currentTab } = this.state

    if (product.offerNftTransactionStatus && currentTab === 'AUCTION') {
      const ok = await Confirmable.open({
        content: t('product_details:sell.check_offer_when_resell')
      })

      if (!ok) {
        return this.close()
      }
    }

    this.setState({
      loading: true
    })
    console.log("values", values)
    try {
      const signature = await onSign(authStore)
      await this._sell(signature, values, t)
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e)
      Confirmable.open({
        content: t(`error-messages:${e.error || 'SOMETHING_WENT_WRONG'}`),
        hideCancelButton: true
      })
    }

    this.close()

    this.setState({
      loading: false
    })

    return null
  }

  _validate = async (values, t) => {
    const { sellStore, product } = this.props
    const { convertedCommissionFee, selectedCurrency, currentTab, registerSellData } = this.state
    let errors = {}

    if (currentTab === 'AUCTION') {
      const startTime = values.startTime.format('x')
      const endTime = values.endTime.format('x')

      const validateSellResult = await sellStore.validateSell({
        productId: product.id,
        productResellId: registerSellData.productResellId,
        resellType: currentTab,
        startTime,
        endTime,
        price: +values.sellPrice || +numeral(convertedCommissionFee).format('0.000')
      })

      if (!validateSellResult.success) {
        errors = {
          ...errors,
          endTime: t(`error-messages:${validateSellResult.data.error}`)
        }
      }
    }

    // if (+values.sellPrice <= convertedCommissionFee) {
    //   errors = {
    //     ...errors,
    //     sellPrice: t('validation_messages:PRICE_BIGGER_WITH_FEE', {
    //       x: `${Format.price(convertedCommissionFee)} ${selectedCurrency}`
    //     })
    //   }
    // }

    if (values.expectPrice && +values.expectPrice <= +values.sellPrice) {
      errors = {
        ...errors,
        expectPrice: t('validation_messages:PRICE_BIGGER', {
          x: `${Format.price(+values.sellPrice)} ${selectedCurrency}`
        })
      }
    }

    if (
      currentTab === 'AUCTION'
      && values.startTime
      && values.endTime
      && (values.endTime.diff(values.startTime, 'm') < 10)
    ) {
      errors = {
        ...errors,
        endTime: t('validation_messages:MORE_THAN_10_MINUTES')
      }
    }

    return errors
  }

  _onSelectCurrency = (currnecy) => {
    const { priceRates, registerSellData } = this.state

    const convertedCommissionFee = this._convertCommissionFee(
      registerSellData.commissionFee,
      registerSellData.currency,
      currnecy,
      priceRates
    )

    this.setState({
      selectedCurrency: currnecy,
      convertedCommissionFee
    })
  }

  _getGokuPrice = (price, currency, priceRates) => {
    const { ETH_TO_GOKU, NBNG_TO_GOKU } = priceRates

    return currency === CURRENCIES.ETH
      ? price * ETH_TO_GOKU : currency === CURRENCIES.NBNG
        ? price * NBNG_TO_GOKU : price
  }

  _getJPYPrice = (price, currency, priceRates) => {
    const {
      USD_TO_JPY,
      ETH_TO_GOKU
    } = priceRates

    return currency === CURRENCIES.ETH
      ? price * ETH_TO_GOKU : currency === CURRENCIES.USDT
        ? price * USD_TO_JPY : price
  }

  _renderNumberBox = (price) => {
    const { priceRates, selectedCurrency } = this.state

    if (!priceRates) return null
    const { ETH_TO_GOKU, GOKU_TO_JPY, USD_TO_JPY } = priceRates

    const jpyPrice = this._getJPYPrice(price, selectedCurrency, priceRates)
    return (
      <div className="number-box">
        <Typography poppins size="small">{Format.price(jpyPrice / ETH_TO_GOKU) || 0} ETH</Typography>
        <div className="number-box-divider" />
        {/* <Typography poppins size="small">{Format.price(jpyPrice / NBNG_TO_GOKU)} NBNG</Typography> */}
        <Typography poppins size="small">{Format.price(jpyPrice / USD_TO_JPY)} USDT</Typography>
        <div className="number-box-divider" />
        <Typography poppins size="small">{Format.price(jpyPrice * GOKU_TO_JPY)} JPY</Typography>
      </div>
    )
  }

  _renderPriceField = (name, label, hint, values, errors, t) => {
    const { selectedCurrency } = this.state

    return (
      <>
        <div className="label-box">
          <Typography size="big" bold>
            {label}
          </Typography>
          <HintTooltip content={hint} />
        </div>
        <div className="field-box">
          <div className="input-wrapper">
            <div className="price_input">
              <div className="input-unit-box">
                <div className="nbng-icon">
                  <img
                    src={selectedCurrency === CURRENCIES.GOKU
                      ? Images.COLOR_GOKU_ICON
                      : selectedCurrency === CURRENCIES.NBNG
                        ? Images.BLACK_NBNG_ICON
                        : selectedCurrency === CURRENCIES.USDT
                          ? Images.COLOR_TETHER_ICON
                          : Images.BLACK_ETH_ICON}
                    alt=""
                  />
                </div>
                <Typography poppins bold size="huge">
                  {selectedCurrency}
                </Typography>
              </div>
            </div>
            <div className="_input-outter">
              <Field
                className="input-box"
                type="number"
                name={name}
                maxLength={9}
                placeholder={0}
                simple
                component={Input}
              />
              {this._renderNumberBox(values[name])}
            </div>
          </div>
          {errors[name] && (
            <Typography className="error-text" size="small" primary>
              {t(`validation_messages:${errors[name]}`)}
            </Typography>
          )}
        </div>
      </>
    )
  }

  _renderNormalPanel = (values, errors, t) => {
    const { selectedCurrency } = this.state

    return (
      <div className="_panel">
        <div className="currencies-box">
          <div className="label-box">
            <Typography size="big" bold>
              {t('product_details:sell.currency')}
            </Typography>
            <HintTooltip content={t('product_details:sell.hint_1')} />
          </div>
          <div className="_right-box">
            {SUPPORT_CURRENCIES.map((currency) => (
              <Clickable
                key={currency}
                className={classnames({ active: selectedCurrency === currency })}
                onClick={() => this._onSelectCurrency(currency)}
              >
                <Typography size="large">
                  {currency}
                </Typography>
              </Clickable>
            ))}
          </div>
        </div>
        {this._renderPriceField(
          'sellPrice',
          t('product_details:sell.price'),
          t('product_details:sell.hint_2'),
          values,
          errors,
          t
        )}
      </div>
    )
  }

  _renderAuctionPanel = (values, errors, t) => {
    const { selectedCurrency } = this.state

    return (
      <div className="_panel">
        <div className="label-box">
          <Typography size="big" bold>
            {t('product_details:sell.period')}
          </Typography>
          <HintTooltip content="" />
        </div>
        <div className="field-group">
          <Field
            showError
            inputLabel={t('product_details:sell.begin')}
            name="startTime"
            component={DateTimePicker}
          />
          <Field
            showError
            inputLabel={t('product_details:sell.end')}
            name="endTime"
            component={DateTimePicker}
          />
        </div>
        <div className="currencies-box">
          <div className="label-box">
            <Typography size="big" bold>
              {t('product_details:sell.currency')}
            </Typography>
            <HintTooltip content="" />
          </div>
          <div className="_right-box">
            {SUPPORT_CURRENCIES.map((currency) => (
              <Clickable
                key={currency}
                className={classnames({ active: selectedCurrency === currency })}
                onClick={() => this._onSelectCurrency(currency)}
              >
                <Typography size="large">
                  {currency}
                </Typography>
              </Clickable>
            ))}
          </div>
        </div>
        {this._renderPriceField(
          'sellPrice',
          t('product_details:sell.start_price'),
          null,
          values,
          errors,
          t
        )}
        {this._renderPriceField(
          'expectDisplayPrice',
          t('product_details:sell.expect_price'),
          null,
          values,
          errors,
          t
        )}
        {this._renderPriceField(
          'expectPrice',
          t('product_details:sell.decision_price'),
          null,
          values,
          errors,
          t
        )}
      </div>
    )
  }

  percentage = (value, percent) => (value / 100) * percent

  _renderForm = ({ handleSubmit, values, errors }, t) => {
    const {
      loading,
      currentTab,
      selectedCurrency,
      convertedCommissionFee,
      registerSellData
    } = this.state

    const { isEvent } = this.props

    // let receivePrice = values.sellPrice - convertedCommissionFee
    const { adminPercent } = registerSellData
    const { artistPercent } = registerSellData

    const adminFee = this.percentage(values.sellPrice, adminPercent)
    const artistFee = this.percentage(values.sellPrice, artistPercent)
    const totalEarned = values.sellPrice - adminFee - artistFee

    // auction fee
    const expectAdminFee = this.percentage(values.expectDisplayPrice, adminPercent)
    const expectArtistFee = this.percentage(values.expectDisplayPrice, artistPercent)
    const expectTotalEarned = values.expectDisplayPrice - expectAdminFee - expectArtistFee

    const decisionAdminFee = this.percentage(values.expectPrice, adminPercent)
    const decisionArtistFee = this.percentage(values.expectPrice, artistPercent)
    const decisionTotalEarned = values.expectPrice - decisionAdminFee - decisionArtistFee

    // const receivePrice = isEvent
    //   ? (values.sellPrice - adminFee - artistFee)
    //   : (values.sellPrice - convertedCommissionFee)

    const receivePrice = totalEarned

    const TAB_ITEMS = [{
      name: t('product_details:sell.normal'),
      value: 'NORMAL'
    }, {
      name: t('product_details:sell.auction'),
      value: 'AUCTION'
    }]
    const TAB_ITEMS_EVENT = [{
      name: t('product_details:sell.normal'),
      value: 'NORMAL'
    }]
    const INFO_DATA = currentTab === 'NORMAL' ? [{
      visible: true,
      name: t('product_details:sell.form'),
      value: currentTab === 'NORMAL'
        ? t('product_details:sell.normal') : t('product_details:sell.auction')
    }, {
      visible: true,
      name: t('product_details:sell.currency'),
      value: selectedCurrency
    }, {
      visible: true,
      name: t('product_details:sell.listing_price'),
      value: `${Format.price(values.sellPrice || 0)} ${selectedCurrency}`
    }, {
      visible: adminPercent !== 0,
      name: t('product_details:sell.transaction_fee'),
      value: `${Format.price(adminFee || 0)} ${selectedCurrency}`
    }, {
      visible: artistPercent !== 0,
      name: t('product_details:sell.artist_fee'),
      value: `${Format.price(artistFee || 0)} ${selectedCurrency}`
    },
    // {
    //   visible: !isEvent,
    //   name: t('product_details:sell.blockchain_fee'),
    //   value: `${Format.price(convertedCommissionFee)} ${selectedCurrency}`
    // },
    {
      visible: true,
      line: true,
      name: t('product_details:sell.earned_amount'),
      value: `${Format.price(receivePrice)} ${selectedCurrency}`
    }] : [{
      visible: true,
      name: t('product_details:sell.form'),
      value: currentTab === 'NORMAL'
        ? t('product_details:sell.normal') : t('product_details:sell.auction')
    }, {
      formatDay: true,
      visible: true,
      name: t('product_details:sell.period_setting'),
      value: (`${
        values.startTime ? values.startTime.format('YYYY.MM.DD HH:mm') : '-'
      } ${t('product_details:sell.from_period')} ${
        values.endTime ? values.endTime.format('YYYY.MM.DD HH:mm') : '-'
      }`)
    }, {
      visible: true,
      name: t('product_details:sell.currency'),
      value: selectedCurrency
    }, {
      visible: true,
      name: t('product_details:sell.start_price'),
      value: `${Format.price(values.sellPrice || 0)} ${selectedCurrency}`
    },
    {
      paddingLeft: true,
      visible: adminPercent !== 0,
      name: t('product_details:sell.transaction_fee'),
      value: `${Format.price(adminFee || 0)} ${selectedCurrency}`
    }, {
      paddingLeft: true,
      visible: artistPercent !== 0,
      name: t('product_details:sell.artist_fee'),
      value: `${Format.price(artistFee || 0)} ${selectedCurrency}`
    },
    {
      paddingLeft: true,
      visible: adminPercent !== 0 || artistPercent !== 0,
      line: true,
      name: t('product_details:sell.earned_amount'),
      value: `${Format.price(receivePrice)} ${selectedCurrency}`
    },
    {
      paddingTop: true,
      visible: true,
      name: t('product_details:sell.expect_price'),
      value: `${Format.price(values.expectDisplayPrice || 0)} ${selectedCurrency}`
    },
    {
      paddingLeft: true,
      visible: adminPercent !== 0,
      name: t('product_details:sell.transaction_fee'),
      value: `${Format.price(expectAdminFee || 0)} ${selectedCurrency}`
    }, {
      paddingLeft: true,
      visible: artistPercent !== 0,
      name: t('product_details:sell.artist_fee'),
      value: `${Format.price(expectArtistFee || 0)} ${selectedCurrency}`
    },
    {
      paddingLeft: true,
      visible: adminPercent !== 0 || artistPercent !== 0,
      line: true,
      name: t('product_details:sell.earned_amount'),
      value: `${Format.price(expectTotalEarned)} ${selectedCurrency}`
    },
    {
      paddingTop: true,
      visible: true,
      name: t('product_details:sell.decision_price'),
      value: `${Format.price(values.expectPrice || 0)} ${selectedCurrency}`
    },
    {
      paddingLeft: true,
      visible: adminPercent !== 0,
      name: t('product_details:sell.transaction_fee'),
      value: `${Format.price(decisionAdminFee || 0)} ${selectedCurrency}`
    }, {
      paddingLeft: true,
      visible: artistPercent !== 0,
      name: t('product_details:sell.artist_fee'),
      value: `${Format.price(decisionArtistFee || 0)} ${selectedCurrency}`
    },
    {
      paddingLeft: true,
      visible: adminPercent !== 0 || artistPercent !== 0,
      line: true,
      name: t('product_details:sell.earned_amount'),
      value: `${Format.price(decisionTotalEarned)} ${selectedCurrency}`
    }
    // {
    //   visible: !isEvent,
    //   name: t('product_details:sell.blockchain_fee'),
    //   value: `${Format.price(convertedCommissionFee)} ${selectedCurrency}`
    // }
    ]

    const TAB_CUSTOM = isEvent ? TAB_ITEMS_EVENT : TAB_ITEMS

    return (
      <Form>
        <div className="modal-header">
          <Typography bold>
            {t('product_details:sell.resell')}
          </Typography>
        </div>
        <div className="content">
          <div className="content-side">
            <div className="tab-box">
              {TAB_CUSTOM.map((item) => (
                <Clickable
                  onClick={() => this._onTabSelect(item)}
                  key={item.value}
                  className={classnames('tab-item', { active: item.value === currentTab })}
                >
                  {item.name}
                </Clickable>
              ))}
            </div>
            {currentTab === 'NORMAL' && this._renderNormalPanel(values, errors, t)}
            {currentTab === 'AUCTION' && this._renderAuctionPanel(values, errors, t)}
          </div>
          <div className="divider">
            <div className="line" />
            <div className="transfer-icon">
              <img src={Images.BLACK_TRANSFER_ICON} alt="" />
            </div>
          </div>
          <div className="content-side">
            <div className="top-box">
              <div className="label-box">
                <Typography size="big" bold>
                  {t('product_details:sell.content')}
                </Typography>
                <HintTooltip content={t('product_details:sell.hint_3')} />
              </div>
              {INFO_DATA.map((item, index) => (
                <>
                  {
                    item.visible && (
                      <Fragment key={index}>
                        {
                          (item.line && item.paddingLeft) ? (
                            <div className="data-field-divider padding-left" />
                          ) : (item.line && !item.paddingLeft) ? (
                            <div className="data-field-divider" />
                          ) : ''
                        }
                        <div
                          className={classnames(
                            { 'padding-left': item.paddingLeft },
                            { 'padding-top': item.paddingTop },
                            { 'format-day': item.formatDay },
                            'text-field'
                          )}
                          key={index}
                        >
                          <Typography className="_name" size="large">
                            {item.name}
                          </Typography>
                          <Typography className="_value" size="large">
                            {item.value}
                          </Typography>
                        </div>
                      </Fragment>
                    )
                  }
                </>
              ))}
            </div>
            <div className="bottom-box">
              <Field
                showError={false}
                className="accept-terms-checkbox"
                name="acceptTerms"
                component={Checkbox}
              >
                {t('product_details:bid.accept')}
              </Field>
              <div className="action-box">
                <Button
                  disabled={!values.acceptTerms}
                  onClick={handleSubmit}
                  background={Colors.RED_2}
                  loading={loading}
                >
                  {t('product_details:sell.resell')}
                </Button>
                <Clickable
                  className="cancel-button"
                  onClick={this.close}
                >
                  <Typography primary bold size="big">
                    {t('product_details:sell.cancel')}
                  </Typography>
                </Clickable>
              </div>
            </div>
          </div>
        </div>
      </Form>
    )
  }

  render() {
    const { isOpen, initialValues, loading, initing, currentTab } = this.state

    return (
      <Modal
        visible={isOpen}
        onCancel={this.close}
        destroyOnClose
        maskClosable={!loading}
        closable={!loading}
        width={1370}
        padding={0}
      >
        <StyledDiv>
          {initing ? (
            <Loading size="small" className="loading" />
          ) : (
            <Translation>
              {(t) => (
                <Formik
                  enableReinitialize
                  validateOnChange={false}
                  validateOnBlur={false}
                  initialValues={initialValues}
                  /* eslint-disable-next-line no-return-await */
                  validate={async (values) => (await this._validate(values, t))}
                  validationSchema={currentTab === 'NORMAL' ? normalValidationSchema : auctionValidationSchema}
                  onSubmit={(values) => this._onSubmit(values, t)}
                  component={(data) => this._renderForm(data, t)}
                />
              )}
            </Translation>
          )}
        </StyledDiv>
      </Modal>
    )
  }
}

export default SellModal
