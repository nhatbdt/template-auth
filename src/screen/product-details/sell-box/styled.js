import styled from 'styled-components'
import Media from '@/utils/media'
import { Colors, Images } from '@/theme'

const StyledDiv = styled.div`
  .loading {
    height: 550px;
  }

  .modal-header {
    height: 110px;
    display: flex;
    align-items: center;
    padding: 0 66px;

    p {
      font-size: 22px;
    }
  }

  .content {
    background-color: white;
    display: flex;
    padding: 36px 66px 66px;

    .label-box {
      display: flex;
      align-items: center;

      .hint {
        margin-left: 10px;
        border: solid 1px #e51109;
        width: 18px;
        min-width: 18px;
        height: 18px;
        border-radius: 9px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: #e51109;
        font-size: 15px;
      }
    }

    .content-side {
      flex: 1;

      &:first-child {
        flex: 1.3;

        .tab-box {
          display: flex;
          margin-bottom: 20px;

          .tab-item {
            flex: 1;
            border: solid 1px #e5e5e5;
            height: 70px;
            display: flex;
            justify-content: center;
            align-items: center;
            transition: background-color 0.2s;
            background-color: #fdfdfd;
            font-size: 16px;

            &:first-child {
              // border-radius: 8px 0 0 8px;
              border-top-left-radius: 8px;
              border-bottom-left-radius: 8px;
              border-right: none;
            }

            &:last-child {
              // border-radius: 0 8px 8px 0;
              border-top-right-radius: 8px;
              border-bottom-right-radius: 8px;
              border-left: 1px solid #ef5842;
            }

            &.active {
              background-color: #fef2f2;
              color: #ef5842;

              &:first-child {
                border: 1px solid #ef5842;
                border-right: none;
              }

              &:last-child {
                border: 1px solid #ef5842;
              }
            }
          }
        }

        ._panel {
          .field-group {
            display: flex;
            margin-bottom: 29px;
            margin-top: 10px;
            
            > div {
              margin-right: 17px;
              flex: 1;
              
              &:last-child {
                margin-right: 0;
              }
            }
          }
          
          .currencies-box {
            display: flex;
            justify-content: space-between;
            align-items: center;
            margin-bottom: 27px;

            ._right-box {
              display: flex;

              div {
                height: 47px;
                width: 121px;
                display: flex;
                justify-content: center;
                align-items: center;
                border-radius: 8px;
                background-color: #efefef;
                border: solid 1px #efefef;
                margin-right: 13px;

                &:last-child {
                  margin-right: 0;
                }

                .typography {
                  color: #000000;
                  opacity: 0.25;
                }

                &.active {
                  background-color: #fef2f2;
                  border: solid 1px #ef533c;

                  .typography {
                    color: #ef533c;
                    opacity: 1;
                  }
                }
              }
            }
          }

          .field-box {
            margin-top: 10px;
            margin-bottom: 23px;
            
            &:last-child {
              margin-bottom: 0;
            }

            .input-wrapper {
              display: flex;
              border: solid 1px #dedede;
              border-radius: 8px;
              overflow: hidden;
              height: 77px;

              .price_input {
                display: flex;

                .input-unit-box {
                  padding: 0 20px;
                  display: flex;
                  align-items: center;
                  justify-content: center;

                  .nbng-icon {
                    width: 33px;
                    height: 33px;
                    background-color: #ffffff;
                    border-radius: 17px;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    box-shadow: 0 0 6px 0 rgb(0 0 0 / 16%);
                    margin-right: 8px;

                    img {
                      width: 23px;
                    }
                  }

                  .arrow-icon {
                    margin-left: 20px;
                  }
                }
              }

              ._input-outter {
                flex: 1;
                background: white;
                display: flex;
                flex-direction: column;

                .input-box {
                  flex: 1;

                  > div {
                    height: 100%;
                  }

                  input {
                    border: none;
                    border-radius: 0;
                    font-weight: bold;
                    font-size: 24px;
                    font-family: 'Poppins', sans-serif;
                    padding: 0 22px;
                    text-align: right;
                    height: 100%;
                    padding-top: 10px;

                    &:hover, &:focus {
                      box-shadow: none;
                    }
                  }

                  .error-box {
                    display: none;
                  }
                }

                .number-box {
                  display: flex;
                  justify-content: flex-end;
                  padding-bottom: 7px;
                  padding-right: 24px;

                  .typography {
                    opacity: 0.4;
                  }

                  .number-box-divider {
                    width: 1px;
                    background: black;
                    opacity: 0.08;
                    margin: 0 10px;
                  }
                }
              }
            }

            .error-text {
              text-align: right;
              margin-top: 5px;
            }
          }
        }
      }

      &:last-child {
        .top-box {
          margin-bottom: 150px;

          .label-box {
            margin-bottom: 27px;
          }

          .data-field-divider {
            height: 1px;
            background: linear-gradient(to right,#b0b0b0 30%,rgba(255,255,255,0) 0%);
            background-size: 6px 3px;
            margin-bottom: 10px;
            &.padding-left {
              margin-left: 8px;
            }
          }

          .text-field {
            display: flex;
            margin-bottom: 10px;

            &:last-child {
              margin-bottom: 0;
            }

            ._name {
              width: 250px;
              color: #b7b7b7;
              font-size: 14px;
            }

            ._value {
              color: #e51109;
              font-size: 14px;
              width: calc(100% - 250px);
            }
            &.padding-left {
              ._name {
                padding-left: 8px;
              }
  
              ._value {
              }
            }
            &.padding-top {
              padding-top: 10px;
            }
            &.format-day {
              ._value {
                width: 135px;
                word-break: break-word;
              }
            }
          }
        }

        .bottom-box {
          .accept-terms-checkbox {
            .ant-checkbox-wrapper {
              > span {
                &:last-child {
                  font-size: 12px;
                  opacity: 0.5;
                }
              }
            }
          }

          .action-box {
            display: flex;
            align-items: center;
            margin-top: 33px;
            justify-content: center;

            button {
              height: 73px;
              border-radius: 40px;
              padding: 0 73px;
              font-size: 18px;
            }

            .cancel-button {
              margin-left: 41px;
            }
          }
        }
      }
    }

    .divider {
      display: flex;
      align-items: center;
      justify-content: center;
      padding: 0 60px;

      .line {
        width: 1px;
        height: 100%;
        background-color: #707070;
        opacity: 0.3;
      }

      .transfer-icon {
        background-color: white;
        position: absolute;
        padding-bottom: 6px;

        img {
          width: 25px;
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .modal-header {
      height: 50px;
      padding: 0 20px;

      p {
        font-size: 18px;
      }
    }

    .content {
      flex-direction: column;
      padding: 15px;

      .content-side {
        &:first-child {
          .tab-box {
            margin-bottom: 20px;

            .tab-item {
              height: 50px;
            }
          }

          ._panel {
            .currencies-box {
              justify-content: flex-start;
              align-items: flex-start;
              flex-direction: column;
              margin-bottom: 20px;

              ._right-box {
                margin-top: 10px;

                div {
                  height: 42px;
                  width: 80px;
                  margin-right: 10px;
                }
              }
            }

            .field-box {
              margin-top: 10px;

              .input-wrapper {
                flex-direction: column;
                height: auto;

                .price_input {
                  display: flex;
                  justify-content: center;
                  padding-top: 5px;

                  .input-unit-box {
                    padding: 0;

                    .nbng-icon {
                      width: 30px;
                      height: 30px;

                      img {
                        width: 21px;
                      }
                    }
                  }
                }

                ._input-outter {
                  .input-box {
                    input {
                      padding-top: 0;
                      padding: 0 15px;
                    }
                  }

                  .number-box {
                    flex-direction: column;
                    align-items: flex-end;
                    padding-right: 15px;

                    .number-box-divider {
                      display: none;
                    }
                  }
                }
              }
            }
          }
        }

        &:last-child {
          .top-box {
            margin-bottom: 50px;

            .label-box {
              margin-bottom: 20px;
            }

            .text-field {
              margin-bottom: 7px;

              ._name {
                width: 180px;
                font-size: 14px;
              }

              ._value {
                font-size: 14px;
                width: calc(100% - 180px);
              }
            }
          }

          .bottom-box {
            .action-box {
              margin-top: 20px;
              flex-direction: column;

              button {
                height: 60px;
                padding: 0 43px;
                font-size: 16px;
              }

              .cancel-button {
                margin-left: 0;
                margin-top: 20px;
                margin-bottom: 20px;

                .typography {
                  font-size: 14px;
                }
              }
            }
          }
        }
      }

      .divider {
        padding: 20px 0;

        .line {
          height: 1px;
          width: 100%;
        }

        .transfer-icon {
          padding-bottom: 0;
          padding: 0 5px;
          margin-bottom: 5px;

          img {
            width: 22px;
          }
        }
      }
    }
  }
`

export default StyledDiv
