import { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { withTranslation } from 'react-i18next'
import { inject, observer } from 'mobx-react'
import { withRouter } from 'react-router-dom'

import Button from '@/components/button'
import Typography from '@/components/typography'
import Confirmable from '@/components/confirmable'
import Toast from '@/components/toast'
import Web3 from '@/utils/web3other'
import Media from '@/utils/media'
import SellModal from './sell-modal'

const StyledDiv = styled.div`
  display: flex;
  align-items: center;

  .status-outer {
    display: flex;
    align-items: center;
    
    .status-divider {
      width: 1px;
      height: 15px;
      background-color: #b3b3b3;
      margin: 0 20px;
    }
  }

  .sell-button {
    padding: 0 30px;
    height: 45px;
    margin-left: 20px;
    ${Media.lessThan(Media.SIZE.MD)} {
      height: 56px;
    }
  }

  &.author-sell {
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    .status-outer {
      flex-direction: column;
      justify-content: flex-start;
      align-items: flex-start;

      .status-divider {
        display: none;
      }
    }
    .sell-button {
      margin-top: 10px;
      width: 100%;
      margin-left: 0;
      border-radius: 0;
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .status-outer {
      flex-direction: column;
      justify-content: center;
      align-items: flex-start;
      
      .typography {
        font-size: 11px;
      }
      
      .status-divider {
        display: none;
      }
    }
  }
`

@withRouter
@withTranslation('product_details')
@inject((stores) => ({
  product: stores.products.currentProductDetails,
  authStore: stores.auth,
  sellStore: stores.sell
}))
@observer
class SellBox extends Component {
  static propTypes = {
    product: PropTypes.object,
    sellStore: PropTypes.object,
    authStore: PropTypes.object
  }

  state = {
    loading: false
  }

  componentDidMount() {
    const { match, product } = this.props
    const { sellAction } = match.params
    const { resellCancelable } = product.productResell
    const { productBuying } = product

    if (sellAction === 'sell' && !resellCancelable && !productBuying) {
      setTimeout(() => {
        this._sellModal.open()
      }, 300)
    }

    if (sellAction === 'cancel' && resellCancelable && !productBuying) {
      setTimeout(() => {
        this._onCancelSell()
      }, 300)
    }
  }

  _sign = async () => {
    const { authStore } = this.props
    const userNonceResult = await authStore.getUserNonce(authStore.initialData.userId)

    if (!userNonceResult.success) {
      throw userNonceResult.data
    }

    const signature = await Web3.sign(
      userNonceResult.data.nonce,
      authStore.initialData.publicAddress
    )

    return signature
  }

  _onCancelSell = async () => {
    const { sellStore, product, t } = this.props

    const ok = await Confirmable.open({
      content: t('sell.are_you_sure_to_cancel_listing'),
      cancelButtonText: t('common:close_modal'),
      acceptButtonText: t('common:ok')
    })

    if (!ok) return null

    try {
      this.setState({
        loading: true
      })

      const signature = await this._sign()

      const { success } = await sellStore.cancelSell({
        signature,
        productId: product.id,
        resellType: 'NORMAL'
      })

      if (success) {
        Confirmable.open({
          content: t('sell.cancel_sell_success'),
          hideCancelButton: true
        })

        product.setData({
          status: 'SOLD',
          productBid: null
        })

        product.productResell.setData({
          resellCancelable: false,
          confirmResell: false
        })
      }
    } catch (e) {
      Toast.error(t('sell.cancel_sell_failed'))
    }

    this.setState({
      loading: false
    })

    return null
  }

  render() {
    const { product, t, className, isEvent, backgroundButton } = this.props
    const { loading } = this.state
    const { confirmResell, resellCancelable, reselling } = product.productResell
    const { productBuying } = product

    return (
      <StyledDiv className={className}>
        <div className="status-outer">
          {product.offerNftTransactionStatus && !productBuying && (
            <div className="offer-status-box">
              <Typography primary>
                {t(`offer.seller_status.${product.offerNftTransactionStatus}`)}
              </Typography>
            </div>
          )}
          {(product.offerNftTransactionStatus && !productBuying) && (confirmResell || productBuying) && (
            <div className="status-divider" />
          )}
          {(confirmResell || productBuying) && (
            <Typography primary className="resell-status">
              {confirmResell ? t('sell.resel_is_confirming') : productBuying ? t('sell.resel_is_buying') : ''}
            </Typography>
          )}
        </div>
        {resellCancelable && !productBuying ? (
          <Button
            loading={loading}
            className="sell-button"
            onClick={() => this._onCancelSell()}
            style={{ background: backgroundButton }}
          >
            {t('sell.cancel_listing')}
          </Button>
        ) : (!productBuying && !reselling && (
          <Button
            className="sell-button"
            onClick={() => this._sellModal.open()}
            style={{ background: backgroundButton }}
          >
            {t('sell.resell')}
          </Button>
        ))}
        <SellModal
          ref={(ref) => { this._sellModal = ref }}
          isEvent={isEvent}
        />
      </StyledDiv>
    )
  }
}

export default SellBox
