/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/anchor-is-valid */
import { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { inject, observer } from 'mobx-react'
import moment from 'moment'
import { withTranslation } from 'react-i18next'
import classnames from 'classnames'
import { withRouter } from 'react-router-dom'

import Typography from '@/components/typography'
import Thumbnail from '@/components/thumbnail'
import Button from '@/components/button'
import { Images } from '@/theme'
import Format from '@/utils/format'
import Misc from '@/utils/misc'
import Loading from '@/components/loading'
import { StyledDiv } from './styles'

@withRouter
@withTranslation('product_details')
@inject((stores) => ({
  product: stores.products.currentProductDetails,
  productsStore: stores.products
}))
@observer
class BuyTogetherHistory extends PureComponent {
  static propTypes = {
    product: PropTypes.object,
    productsStore: PropTypes.object
  }

  state = {
    loading: false
  }

  _onGotoDetail = (item) => () => {
    const { history, match } = this.props

    if (match.path === '/:lang/product-details/:id') {
      history.push(`/product-details/${item.productId}/force-refresh`)
    } else {
      history.push(`/product-details/${item.productId}`)
    }
  }

  componentDidMount = () => {
    this._getHistoriesProductChild(1)
  }

  _getHistoriesProductChild = async (page, concat) => {
    const { product, productsStore } = this.props

    this.setState({
      loading: true
    })

    await productsStore.getChildProductHistories({
      productId: product.isParent ? product.id : product.parentProductId,
      limit: 10,
      page
    }, concat)

    this.setState({
      loading: false
    })
  }

  _loadmore = () => {
    const { productsStore } = this.props

    this._getHistoriesProductChild(productsStore.childProductHistories.page + 1, true)
  }

  _renderItem = (item, index) => {
    const timeFormat = 'YYYY/M/D HH:mm:ss'
    const { t } = this.props

    return (
      <tr key={index} className={classnames({ success: item.buyingFlag })}>
        <td>
          <Typography size="large" bold poppins>
            {index + 1}
          </Typography>
        </td>
        <td>
          <Typography size="large" bold poppins>
            {item?.childIndex}
          </Typography>
        </td>
        <td>
          <div className="horizon">
            <Thumbnail
              size={30}
              rounded
              className="avatar"
              placeholderUrl={Images.USER_PLACEHOLDER}
              url={item.userImage}
            />
            <Typography poppins className="user-name">
              {item.userName || Misc.trimPublicAddress(item.userPublicAddress, 6)}
            </Typography>
          </div>
        </td>
        <td>
          <Typography size="large" bold poppins>
            {Format.price(item.price)}
          </Typography>
        </td>
        <td>
          <Typography size="large" bold poppins>
            {item.currency}
          </Typography>
        </td>
        <td>
          <Typography size="small" className="date">
            {moment(item.productCreatedAt).format(timeFormat)}
          </Typography>
        </td>
        <td>
          <a onClick={this._onGotoDetail(item)}>{t('product_details:buy_together.detail')} {' >>'}</a>
        </td>
      </tr>
    )
  }

  render() {
    const { t, productsStore } = this.props
    const { loading } = this.state

    return (
      <>
        <StyledDiv>
          <Typography className="title">
            {t('product_details:buy_together.history_title')}
          </Typography>
          <div className="table-container">
            <table>
              <thead>
                <tr>
                  <th>#</th>
                  <th>{t('product_details:buy_together.index')}</th>
                  <th>{t('product_details:buy_together.owner')}</th>
                  <th>{t('product_details:buy_together.price')}</th>
                  <th>{t('common:header.currency')}</th>
                  <th>{t('common:date_time')}</th>
                  <th>{' '}</th>
                </tr>
              </thead>
              <tbody>
                {productsStore.childProductHistories.items.map(this._renderItem)}
              </tbody>
            </table>
            {(!loading && productsStore.childProductHistories.total <= 0) && (
              <p className="data-null large">{t('product_details:buy_together.data_not_found')}</p>
            )}
          </div>
          {loading && (
            <Loading />
          )}
          {productsStore.childProductHistories.items.length < productsStore.childProductHistories.total && (
            <div className="button-loadmore">
              <Button
                onClick={this._loadmore}
              >
                {t('product_details:buy_together.loadmore')}
              </Button>
            </div>
          )}
        </StyledDiv>
      </>
    )
  }
}

export default BuyTogetherHistory
