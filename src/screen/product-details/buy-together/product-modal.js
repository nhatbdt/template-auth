import { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { inject, observer } from 'mobx-react'
import { Translation } from 'react-i18next'

import Modal from '@/components/modal'
import Button from '@/components/button'
import Loading from '@/components/loading'
import Typography from '@/components/typography'
import { Colors } from '@/theme'
import { StyledModal } from './styles'

@inject((stores) => ({
  productsStore: stores.products,
  childProduct: stores.products.currentChildProductDetails
}))
@observer

class ProductModal extends PureComponent {
  static propTypes = {
    productsStore: PropTypes.object,
    productId: PropTypes.number,
    onCheckout: PropTypes.func,
    childProduct: PropTypes.object
  }

  state = {
    isOpen: false,
    loading: false,
    error: ''
  }

  open = () => {
    this.setState({ isOpen: true })
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (this.state.isOpen !== prevState.isOpen) {
      this._getProductChildItem()
    }
  }

  _getProductChildItem = async () => {
    this.setState({
      loading: true
    })
    if (this.state.isOpen) {
      try {
        const { productsStore, i18n, productId } = this.props
        const res = await productsStore.getProductChildItem({
          productId,
          langKey: i18n.language.toUpperCase()
        })
        this.setState({
          loading: false
        })
        if (!res?.data) {
          this.setState({
            error: 'Error get product, please try again!'
          })
        }
      } catch (error) {
        this.setState({
          loading: false
        })
      }
    }
  }

  close = () => this.setState({ isOpen: false })

  _onCheckout = async () => {
    const { childProduct } = this.props
    this.props.onCheckout(childProduct)
  }

  render() {
    const { isOpen, loading, error } = this.state
    const { childProduct } = this.props

    return (
      <Modal
        visible={isOpen}
        onCancel={this.close}
        destroyOnClose
        maskClosable={!loading}
        closable={!loading}
        width={500}
      >
        <StyledModal>
          {loading ? (
            <Loading />
          ) : (
            <>
              {error ? (
                <p className="error">{error}</p>
              ) : (
                <Translation>
                  {(t) => (
                    <>
                      {childProduct ? (
                        <Typography bold className="modal-title title-checkout" center>
                          {t('product_details:buy_together.mess_checkout')}
                        </Typography>
                      ) : (
                        <p className="data-null">{t('product_details:buy_together.data_not_found')}</p>
                      )}
                    </>
                  )}
                </Translation>
              )}
            </>
          )}
          <Translation>
            {(t) => (
              <div className="button-group">
                <Button
                  onClick={() => this.close()}
                  background={Colors.DARK}
                >
                  {t('product_details:buy_together.cancel')}
                </Button>
                <Button
                  onClick={this._onCheckout}
                  background={Colors.RED_2}
                  disabled={loading || error || !childProduct}
                >
                  {t('product_details:buy_together.checkout')}
                </Button>
              </div>
            )}
          </Translation>
        </StyledModal>
      </Modal>
    )
  }
}

export default ProductModal
