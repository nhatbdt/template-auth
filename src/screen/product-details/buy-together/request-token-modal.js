import { PureComponent } from 'react'
import { inject, observer } from 'mobx-react'
import { Translation } from 'react-i18next'
import PropTypes from 'prop-types'

import Modal from '@/components/modal'
import Button from '@/components/button'
import Typography from '@/components/typography'
import { Colors } from '@/theme'
import { StyledModal } from './styles'

@inject((stores) => ({
  productsStore: stores.products
}))
@observer

class RequestTokenModal extends PureComponent {
  static propTypes = {
    loading: PropTypes.bool,
    error: PropTypes.string,
    onConfirm: PropTypes.func,
    requestParentTokenStatus: PropTypes.string
  }

  state = {
    isOpen: false
  }

  open = () => {
    this.setState({ isOpen: true })
  }

  close = () => this.setState({ isOpen: false })

  render() {
    const { isOpen } = this.state
    const { loading, onConfirm, error, requestParentTokenStatus } = this.props

    return (
      <Modal
        visible={isOpen}
        onCancel={this.close}
        destroyOnClose
        maskClosable={!loading}
        closable={!loading}
        width={500}
      >
        <StyledModal>
          <Translation>
            {(t) => (
              <>
                <Typography bold className="modal-title" center>
                  {requestParentTokenStatus === 'SUCCESS' && (
                    <>{t('product_details:buy_together.SUCCESS')}</>
                  )}
                  {requestParentTokenStatus === 'NOT_ENOUGH_CHILD_PRODUCT' && (
                    <>{t('product_details:buy_together.NOT_ENOUGH_CHILD_PRODUCT')} </>
                  )}
                  {requestParentTokenStatus === 'TRANSACTION_FEE' && (
                    <>{t('product_details:buy_together.TRANSACTION_FEE')}</>
                  )}
                  {requestParentTokenStatus === 'REQUEST_PERMISSON' && (
                    <>{t('product_details:buy_together.confirm_request_parent_token')}</>
                  )}
                  {requestParentTokenStatus === 'RETURN_CHILD_PRODUCT' && (
                    <>{t('product_details:buy_together.RETURN_CHILD_PRODUCT')}</>
                  )}

                  {requestParentTokenStatus === 'TOKEN_PENDING ' && (
                    <>{t('product_details:buy_together.TOKEN_PENDING')}</>
                  )}
                  {requestParentTokenStatus === 'FAIL ' && (
                    <>{t('product_details:buy_together.FAIL')}</>
                  )}

                </Typography>
                <div className="button-group">
                  <Button
                    onClick={() => this.close()}
                    background={Colors.DARK}
                  >
                    <span>{t('product_details:buy_together.cancel')}</span>
                  </Button>
                  {(requestParentTokenStatus === 'REQUEST_PERMISSON'
                    || requestParentTokenStatus === 'TRANSACTION_FEE')
                    && (
                    <Button
                      onClick={onConfirm}
                      background={Colors.RED_2}
                      disabled={loading}
                      loading={loading}
                    >
                      <span>{t('product_details:buy_together.confirm')}</span>
                    </Button>
                    )}
                </div>
                {error && (
                  <p className="error">{error}</p>
                )}
              </>
            )}
          </Translation>
        </StyledModal>
      </Modal>
    )
  }
}

export default RequestTokenModal
