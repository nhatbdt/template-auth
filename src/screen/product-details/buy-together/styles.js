import styled from 'styled-components'
import { Colors } from '@/theme'
import Media from '@/utils/media'

export const StyledDiv = styled.div`
padding-top: 31px;
width: 100%;

.table-container {
  overflow: auto;
  max-height: 400px;
  width: 100%;

  .data-null {
    margin: 30px 0;
    text-align: center;
    font-weight: bold;
    font-size: 16px;
  }
}

.title {
  color: #333333;
  font-size: 13px;
}

.button-loadmore {
  width: 100%;
  justify-content: center;
  display: flex;
  margin-top: 20px;

  button {
    background: #fff;
    border: 1px solid ${Colors.RED_2};
    color: ${Colors.RED_2};
  }
}

table {
  width: 100%;
  margin-top: 28px;

  thead {
    tr {
      th {
        font-size: 12px;
        font-weight: normal;
        color: #b7b7b7;
        &:nth-child(1) {
          width: 7%;
          padding-left: 19px;
        }

        &:nth-child(2) {
          width: 8%;
        }

        &:nth-child(3) {
          width: 30%;
        }

        &:nth-child(4) {
          width: 15%;
        }

        &:nth-child(5) {
          width: 15%;
        }

        &:nth-child(6) {
          width: 15%;
        }

        &:nth-child(7) {
          width: 10%;
        }
      }
    }
  }

  tbody {
    tr {
      height: 53px;
      border-bottom: 1px solid #f1f1f1;

      td {
        &:nth-child(1) {
          padding-left: 19px;
        }
        
        &:nth-child(5) {
          text-align: right;
        }
        
        .status-text {
          &.success { 
            color: #03ad0a;
          }
        }
        
        .horizon {
          display: flex;
          align-items: center;

          .avatar {
            margin-right: 9px;
            min-width: 0;
          }

          .user-name {
            font-size: 13px;
            color: ${Colors.RED_2};
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 180px;
            white-space: nowrap;
            flex: 1;
            margin-right: 5px;
          }

          .fire-icon {
            margin-right: 23px;
          }
          
          .heat-number {
            color: ${Colors.PRIMARY};
          }
        }

        .date {
          color: #696969;
        }
        
        .cancel-button {
          height: 32px;
          font-size: 11px;
          padding: 0 15px;
        }
      }
    }
  }
  
  &.ended {
    tbody {
      tr {
        height: 53px;
        border-bottom: 1px solid #f1f1f1;

        // &:nth-child(1), &:nth-child(2), &:nth-child(3) {
        //   background-color: transparent;
        //   background-image: none;
        // }
        
        // &.success {
        //   background-image: linear-gradient(to bottom, rgba(255, 248, 247, 0), #ffa69a);
        //   border-bottom: none;
        // }
      }
    }
  }
}

${Media.lessThan(Media.SIZE.MD)} {
  padding-top: 0;
  margin-bottom: 30px;
  width: 100%;

  table {
    margin-top: 15px;
    table-layout: fixed;

    thead {
      tr {
        th {
          &:nth-child(1) {
            width: 100px;
          }

          &:nth-child(2) {
            width: 100px;
          }

          &:nth-child(3) {
            width: 200px;
          }

          &:nth-child(4) {
            width: 150px;
          }

          &:nth-child(5) {
            width: 100px;
          }

          &:nth-child(6) {
            width: 100px;        
          }
          
          &:nth-child(7) {
            width: 150px;
          }
        }
      }
    }
    tbody {
    tr {
      height: 53px;
      border-bottom: 1px solid #f1f1f1;

      &:nth-child(1) {
        //background-image: linear-gradient(to bottom, rgba(255, 248, 247, 0), #ffa69a);
        //border-bottom: none;
      }
      
      td {
        .cancel-button {
          margin-left: -80px;
          height: 32px;
          font-size: 11px;
          padding: 0 15px;
        }
      }
    }
  }
  }
}
`
export const StyledModal = styled.div`
  .info {
    display: flex;
    justify-content: space-between;
    margin-bottom: 15px;

    img {
      width: 200px;
      height: 200px;
      border-radius: 5px;
      object-fit: cover;
    }
  }
  .button-group {
    justify-content: center;
    display: flex;
    margin-top: 20px;

    button {
      &:last-child {
        margin-left: 10px;
      }
    }
  }
  .title-checkout {
    margin-bottom: 20px;
    font-size: 15px;
  }

  .error {
    color: ${Colors.RED_2};
    text-align: center;
    margin-top: 10px
  }

  .data-null {
    text-align: center;
    margin: 30px 0;
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    
  }
`
