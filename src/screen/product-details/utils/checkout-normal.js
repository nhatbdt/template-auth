/* eslint-disable no-throw-literal */
import Web3 from '@/utils/web3other'
import Confirmable from '@/components/confirmable'
import CURRENCIES from '@/constants/currencies'
import Format from '@/utils/format'

const onCheckNetwork = async () => {
  const isNetworkValid = await Web3.checkValidNetwork()

  if (!isNetworkValid) {
    throw 'NETWORK_INCORRECT'
  }
}

const onCheckPrice = async (product, productsStore, t, nickname = '') => {
  const getProductPriceResult = nickname
    ? await productsStore.getAuthorProductPrice({ productId: product.id, nickname })
    : await productsStore.getProductPrice({ productId: product.id })

  if (!getProductPriceResult.success || !getProductPriceResult.data) throw 'ERROR_GET_PRODUCT_PRICE_NULL'

  if (product.price !== getProductPriceResult.data.price) {
    const ok = await Confirmable.open({
      content: t('change_price_message_eth', {
        currentPrice: Format.price(product.price),
        changedPrice: Format.price(getProductPriceResult.data.price),
        currency: product.currency
      })
    })

    if (!ok) throw { error: 'CANCELED' }

    product.setData({
      price: getProductPriceResult.data.price,
      yenPrice: getProductPriceResult.data.yenPrice
    })
  }
}

const onCheckBalance = async (product, authStore) => {
  const { nbng, eth, goku, usdt } = await Web3.getBalance(authStore.initialData.publicAddress)
  let isEnoughBalance = true
  if (product.currency === CURRENCIES.ETH) {
    if (product.price > eth) isEnoughBalance = false
  } else if (product.currency === CURRENCIES.NBNG) {
    if (product.price > nbng) isEnoughBalance = false
  } else if (product.currency === CURRENCIES.USDT) {
    if (product.price > usdt) isEnoughBalance = false
  } else if (product.currency === CURRENCIES.GOKU) {
    if (product.price > goku) isEnoughBalance = false
  }

  if (!isEnoughBalance) {
    throw 'BALANCE_NOT_ENOUGH'
  }

  return { nbng, eth, goku }
}

const onSign = async (authStore) => {
  const userNonceResult = await authStore.getUserNonce(authStore.initialData.userId)

  if (!userNonceResult.success) {
    throw userNonceResult.data
  }

  const signature = await Web3.sign(
    userNonceResult.data.nonce,
    authStore.initialData.publicAddress
  )

  return signature
}

const onTransfer = async (publicAddress, t, data, gasLimit, currency, price) => {
  const transactionHash = await Web3.excuteTransaction(
    publicAddress,
    t,
    data,
    gasLimit,
    currency,
    price
  )

  return transactionHash
}

const onCheckApproveMax = async (t, authStore, product) => {
  const publicAddress = authStore.initialData.publicAddress
  const data = await Web3.approveMax(publicAddress, t, product.currency)
  if (data) return true
  return false
}

const onError = (e, t) => {
  let errorMessage = t('messages.transaction_failed')
  if (e === 'NETWORK_INCORRECT') {
    errorMessage = t('messages.network_incorrect')
  } else if (e === 'BALANCE_NOT_ENOUGH') {
    errorMessage = t('messages.balance_not_enough')
  } else if (e === 'PRODUCT_HOLDING') {
    errorMessage = (
      <>
        {t('messages.product_holding_1')}
        <br /><br />
        {t('messages.product_holding_2')}
      </>
    )
  } else if (e === 'CANNOT_CHECKOUT') {
    errorMessage = (
      <>
        {t('messages.cannot_checkout_1')}
        <br /><br />
        {t('messages.cannot_checkout_2')}
      </>
    )
  } else if (
    e === 'OUT_OF_WALLET'
    || ['ERROR_MASTER_PUBLIC_KEY_NOT_FOUND', 'ERROR_MASTER_PUBLIC_KEY_IS_USING']
      .includes(e.error)
  ) {
    errorMessage = (
      <>
        {t('messages.out_of_wallet_1')}
        <br />
        {t('messages.out_of_wallet_2')}
      </>
    )
  } else if (e.error === 'ERROR_PRODUCT_NOT_FOUND') {
    errorMessage = (
      <>
        {t('messages.error_product_not_found_1')}
        <br /><br />
        {t('messages.error_product_not_found_2')}
      </>
    )
  } else if (e.error === 'ERROR_USER_CAN_BUY_ONE_PRODUCT') {
    errorMessage = (
      <>
        {t('messages.error_user_can_buy_one_product_1')}
        <br />
        {t('messages.error_user_can_buy_one_product_2')}
      </>
    )
  } else if (e.error === 'ERROR_USER_VERIFY_SIGNATURE_FAIL') {
    errorMessage = (
      <>
        {t('messages.error_user_verify_signature_fail_1')}
        <br />
        {t('messages.error_user_verify_signature_fail_2')}
      </>
    )
  } else if (e.error === 'ERROR_USER_NOT_OWN_TOKEN') {
    errorMessage = (
      <>
        ERROR_USER_NOT_OWN_TOKEN
      </>
    )
  } 
  else if (e === 'ERROR_GET_PRODUCT_PRICE_NULL') {
    errorMessage = (
      <>
        ERROR_GET_PRODUCT_PRICE_NULL
      </>
    )
  }

  if (e.error !== 'CANCELED') {
    Confirmable.open({
      content: errorMessage,
      hideCancelButton: true
    })
  }
}

export {
  onCheckApproveMax,
  onTransfer,
  onSign,
  onError,
  onCheckNetwork,
  onCheckPrice,
  onCheckBalance
}