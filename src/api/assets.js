import { MainApi, ExternalApi } from './endpoint'

// Upload files to generated presigned S3 URL
export const uploadFiles = ({ files, type }) => new Promise(async (resolve, reject) => {
  try {
    const signedPayload = Array.from(files)
      .map((file) => {
        const fileNamePaths = file.name.split('.')
        return ({
          fileName: fileNamePaths[0],
          fileType: (fileNamePaths[fileNamePaths.length - 1]).toLowerCase(),
          imageFolderType: type
        })
      })
    const signedResult = await MainApi.post('/images/pre-signed', signedPayload)
    const promiseArray = []

    signedResult.data.forEach(async (item, index) => {
      promiseArray.push(ExternalApi.put(item.preSignedURL, files[index]))
    })
    await Promise.all(promiseArray)

    resolve({
      data: signedResult.data.map((item) => item.url)
    })
  } catch (e) {
    reject(e)
  }
})
