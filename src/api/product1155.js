import { MainApi } from './endpoint'

export async function getProductErc1155Details(payload) {
  return MainApi.get(`/user/product/erc1155-type/${payload.id}`, payload)
}

export function getListProductOwner1155(payload) {
  return MainApi.get('/user/product/owned', payload)
}
