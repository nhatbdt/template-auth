import { MainApi } from './endpoint'

export function getIdentity({ userId }) {
  return MainApi.get(`/user/identity/${userId}`)
}

export function registerIdentity({ userId, ...payload }) {
  return MainApi.put(`/user/identity/${userId}`, payload)
}
