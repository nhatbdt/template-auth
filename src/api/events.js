import { MainApi } from './endpoint'

export function getEvents(payload) {
  return MainApi.get('/event', payload)
}

export function getEventDetails({ id, ...payload }) {
  return MainApi.get(`/event/${id}`, payload)
}
