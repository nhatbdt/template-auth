import { MainApi } from './endpoint'

export function getIdols(payload) {
  return MainApi.get('/user/idol', payload)
}

export function getIdolDetails({ id, langKey, ...payload }) {
  return MainApi.get(`/user/idol/${id}?langKey=${langKey}`, payload)
}

export function getIdolsCulture(payload) {
  return MainApi.get('/user/culture/nickname', payload)
}

export function getCollections(payload) {
  return MainApi.get('/user/collection', payload)
}

export function getCollectionDetail({ id, ...payload }) {
  return MainApi.get(`/user/collection/${id}`, payload)
}

export function getCollectionList({ id, ...payload }) {
  return MainApi.get(`/user/product/collection/${id}`, payload)
}
