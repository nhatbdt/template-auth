import { MainApi } from './endpoint'

export function getUserInfo(id) {
  return MainApi.get(`/user/identity/${id}`)
}

export function updateUserInfo({ id, ...payload }) {
  return MainApi.put(`/user/identity/${id}`, payload)
}
