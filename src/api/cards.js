import { MainApi } from './endpoint'

export function getCards({id, ...payload}) {
  return MainApi.get(`/user/idol/${id}/cards`, payload)
}
