import { MainApi } from './endpoint'

export function getListCategory(payload) {
  return MainApi.get('/user/category', payload)
}

export function getCategoryDetail({id, langKey}) {
  return MainApi.get(`/user/category/${id === "all" ? "" : id}?langKey=${langKey}`)
}
