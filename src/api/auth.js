import { MainApi } from './endpoint'

export function getUserNonce(userId) {
  return MainApi.get(`/user/identity/nonce/${userId}`)
}

export function verifySignature(payload) {
  return MainApi.post('/user/authentication/verify-signature', payload)
}

export function login(payload) {
  return MainApi.post('/user/authentication/sign-in', payload)
}

export function getInitialData(payload) {
  return MainApi.get('/user/initial', payload)
}

export function updateAccelerationRate(payload) {
  const { accelerationRate, id } = payload
  return MainApi.put(`/user/artist/gas-rate/${id}`, { accelerationRate: accelerationRate.toString() || '' })
}

