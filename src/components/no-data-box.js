import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withTranslation } from 'react-i18next';

import Typography from './typography';

const StyledDiv = styled.div`
  display: flex;
  justify-content: center;
  padding: 10px 0;
  width: 100%;
  .typography {
    opacity: 0.6;
    color: #65676b;
  }
`;

const NoDataBox = withTranslation('common')(({ message, className, t }) => (
  <StyledDiv className={className}>
    <Typography description bold size="large">
      {message || t('no_data')}
    </Typography>
  </StyledDiv>
));
NoDataBox.propTypes = {
  message: PropTypes.string,
};

export default NoDataBox;
