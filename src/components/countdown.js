import { Component } from 'react'
import styled from 'styled-components'
import Countdown, { zeroPad } from 'react-countdown';
import { withTranslation } from 'react-i18next';

const StyledCountdown = styled.div`
  width: 100%;

  .customer-countdown {
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    color: #fff;

    .box-time-card {
      font-size: 16px;
      font-weight: bold;
    }
    
    .box-time {
      .value {
        font-size: 24px;
        font-weight: 500;
      }

      .label {
        font-size: 18px;
        font-weight: 400;
        color: #A8AEBA;
      }
    }
  }
`

@withTranslation('product_details')
class CountdownComponent extends Component {
  _renderer = (values) => {
    const { t, small, isCard } = this.props;
    const { completed } = values
    const { days, hours, minutes, seconds } = values.formatted

    return isCard ? (
      <div className='customer-countdown'>
        <div className='box-time-card'>
          {zeroPad(hours)}:{zeroPad(minutes)}:{zeroPad(seconds)}
        </div>
      </div>
    ) : (
      <div className='customer-countdown'>
        <div className='box-time'>
          <div className='value'>{completed ? '0' : zeroPad(days)}</div>
          <div className='label'>{t(`product_details:lending.${small ? 'small_days' : 'days'}`)}</div>
        </div>
        <div className='box-time'>
          <div className='value'>{completed ? '0' : zeroPad(hours)}</div>
          <div className='label'>{t(`product_details:lending.${small ? 'small_hours' : 'hours'}`)}</div>
        </div>
        <div className='box-time'>
          <div className='value'>{completed ? '0' : zeroPad(minutes)}</div>
          <div className='label'>{t(`product_details:lending.${small ? 'small_minutes' : 'minutes'}`)}</div>
        </div>
        <div className='box-time'>
          <div className='value'>{completed ? '0' : zeroPad(seconds)}</div>
          <div className='label'>{t(`product_details:lending.${small ? 'small_seconds' : 'seconds'}`)}</div>
        </div>
      </div>
    )
  }

  render() {
    const { className, isCard, ...props } = this.props
    return (
      <StyledCountdown className={className} >
        <Countdown
          renderer={this._renderer}
          daysInHours={!!isCard}
          {...props}
        />
      </StyledCountdown>
    )
  }
}

export default CountdownComponent
