import React from 'react';
import PropTypes from 'prop-types';
import Media from '../utils/media';
import styled from 'styled-components';
import Clickable from './clickable';

const StyledDiv = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  align-items: baseline;
  margin-bottom: 15px;
  .section-heading-title {
    margin-right: 15px;
    margin-bottom: 0;
    font-size: 24px;
    line-height: 24px;
    font-weight: bold;
    color: #fff;
    /* font-family: 'Noto Sans JP', sans-serif; */
  }
  .section-heading-readmore {
    color: #00deec;
    font-size: 13px;
    text-transform: uppercase;
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .section-heading-title {
      font-size: 25px;
    }
    .section-heading-readmore {
      font-size: 11px;
    }
  }
`;

const SectionHeading = ({ title, readmore, linkReadmore, className, onClickViewAll }) => (
  <StyledDiv className={className}>
    <h2 className="section-heading-title">{title}</h2>
    {readmore && (
      <Clickable onClick={() => onClickViewAll(linkReadmore)} className="section-heading-readmore">
        {readmore}
      </Clickable>
    )}
  </StyledDiv>
);

SectionHeading.propTypes = {
  title: PropTypes.string,
  readmore: PropTypes.string,
  linkReadmore: PropTypes.string,
  className: PropTypes.string,
  onClickViewAll: PropTypes.func,
};

SectionHeading.defaultProps = {
  title: '',
  readmore: '',
  linkReadmore: '',
  className: '',
};
export default SectionHeading;
