import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import Format from '../utils/format';
import Clickable from './clickable';
import Typography from './typography';
import Thumbnail from './thumbnail';
import { Images } from '../theme';

const StyledDiv = styled(Clickable)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  gap: 15px;
  padding-bottom: 15px;
  border-radius: 20px;
  padding: 12px;
  &:hover {
    box-shadow: 0 8px 25px rgba(0, 0, 0, 0.1);
  }
  img {
    /* width: 87px;
    height: 56px; */
    width: 174px;
    height: 112px;
    max-width: 100%;
    max-height: 100%;
    border-radius: 6px;
    object-fit: cover;
  }
  .collection-content {
    font-weight: 700;
    font-size: 16px;
    line-height: 23px;
    letter-spacing: -0.025em;
    display: flex;
    align-items: center;
    gap: 12px;

    .typography {
      font-weight: 700;
      font-size: 16px;
      line-height: 23px;
      letter-spacing: -0.025em;
      color: #fff;
    }
    .collection-image {
      display: flex;
      align-items: center;
      gap: 20px;

      .thumbnail {
        border-radius: 10px;
      }

      .collection-ranking {
        font-weight: 700;
        font-size: 20px;
        line-height: 29px;
        text-align: center;
        color: #fff;
      }
    }
  }
  .collection-price {
    display: flex;
    align-items: center;
    font-weight: 700;
    font-size: 16px;
    line-height: 23px;
    color: #fff;
    gap: 10px;

    .currency-icon {
      width: 20px;
      height: auto;
    }

    .value {
      min-width: 60px;
      text-align: right;
    }
  }
`;

const ProductCardTop = ({ item, index, ...props }) => {
  return (
    <StyledDiv {...props}>
      <div className="collection-content">
        <div className="collection-image">
          <div className="collection-ranking">{index}</div>
          <Thumbnail className="thumbnail" size={66} url={item?.imageUrl} alt="top-nft" />
        </div>
        <Typography bold title={item.name}>
          {item.name?.length > 20 ? `${item.name.slice(0, 20)}...` : item.name}
        </Typography>
      </div>
      <div className="collection-price">
        <img src={Images[item?.currency]} className="currency-icon" alt="currency" width={14} height={24} />
        <div className="value">
          <span>{Format.price(item?.price) || 0} </span>
          <span>{item?.currency}</span>
        </div>
      </div>
    </StyledDiv>
  );
};
ProductCardTop.propTypes = {
  onClick: PropTypes.func,
  item: PropTypes.object,
  index: PropTypes.number,
};

export default ProductCardTop;
