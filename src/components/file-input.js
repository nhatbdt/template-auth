import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import classNames from 'classnames';
import lodash from 'lodash';

import FileReader from '../utils/file-reader';
import { Colors } from '../theme';
import Thumbnail from './thumbnail';
import Button from './button';
import Clickable from './clickable';
import { withTranslation } from 'react-i18next';
import ERROR_MESSAGES from '../translations/error-messages';

const StyledDiv = styled.div`
  display: block;
  width: ${props => props.size}px;
  height: ${props => props.size}px;
  background-position: center;
  background-size: cover;

  &.rounded {
    border-radius: 50%;
  }

  &.contain {
    background-size: contain;
    background-repeat: no-repeat;
  }

  .border-thumbnail {
    border-radius: 50%;
    position: absolute;
    border: 1px solid #e5e5e5;
  }

  .remove-button {
    position: absolute;
    right: -10px;
    top: 11px;
    border: 1px solid ${Colors.PRIMARY};
    border-radius: 10px;
    width: 20px;
    height: 20px;
    display: flex;
    align-items: center;
    justify-content: center;
    opacity: 1;

    img {
      width: 10px;
      height: 10px;
    }
  }

  .file-content-box {
    position: relative;
  }

  .file-input {
    display: none;
  }
`;

@withTranslation()
class FileInput extends Component {
  static propTypes = {
    field: PropTypes.object,
    form: PropTypes.object,
    onChange: PropTypes.func,
    type: PropTypes.oneOf(['image', 'file']),
    value: PropTypes.any,
    accept: PropTypes.string,
    url: PropTypes.string,
    placeholderUrl: PropTypes.string,
    size: PropTypes.number,
    maxAllowedSize: PropTypes.number,
    rounded: PropTypes.bool,
    contain: PropTypes.bool,
  };

  state = {
    imageUrl: null,
    file: null,
  };

  _onChange = async e => {
    const { field, form, onChange, type, accept, maxAllowedSize, i18n } = this.props;
    const file = e.target.files[0];

    if (!file) return;
    if (file?.size > maxAllowedSize * 1024 * 1024) {
      const errorMessages = ERROR_MESSAGES[i18n.language]['error-messages'];
      alert(errorMessages.ERROR_OVER_ALLOWED_SIZE);
      return;
    }

    if (onChange) onChange(e);

    if (type === 'image' && accept?.includes(file?.type)) {
      const imageUrl = await FileReader.getBase64(file);
      this.setState({
        imageUrl,
        file,
      });

      if (field && form) {
        form.setFieldValue(field.name, {
          file,
          imageUrl,
        });
      }
    }

    if (type === 'file' && accept?.includes(file?.type)) {
      this.setState({
        file,
      });

      if (field && form) {
        form.setFieldValue(field.name, {
          file,
        });
      }
    }
  };

  _onClick = e => {
    e.preventDefault();
    this._input.click();
  };

  clear = e => {
    this._onRemoveClick(e);
  };

  _onRemoveClick = e => {
    e.stopPropagation();
    const { field, form, type } = this.props;

    this._input.value = null;

    if (type === 'file') {
      if (field && form) {
        form.setFieldValue(field.name, null);
      }
    } else if (type === 'image') {
      if (field && form) {
        form.setFieldValue(field.name, null);
        this.setState({
          imageUrl: null,
        });
      }
    }
  };

  render() {
    const {
      field,
      form,
      value,
      style,
      className,
      type,
      url,
      placeholderUrl,
      size,
      rounded,
      contain,
      accept,
      ...props
    } = this.props;
    const { imageUrl, file } = this.state;

    return (
      <StyledDiv
        {...(field && { id: `formik-field-${field.name}` })}
        {...props}
        className={classNames(
          className,
          { rounded, contain, error: lodash.get(form, `errors.${field?.name}`) },
          'file-input',
        )}
      >
        {type === 'image' && (
          <Clickable onClick={this._onClick}>
            <Thumbnail
              name="imageUrl"
              className="border-thumbnail"
              size={size || 180}
              style={{
                borderRadius: '50%',
                backgroundImage: `url('${imageUrl || field?.value || placeholderUrl}')`,
              }}
            />
          </Clickable>
        )}
        {type === 'file' && (
          <Button onClick={this._onClick}>{file?.name || field?.value || value || 'Select a file'}</Button>
        )}
        <input
          {...props}
          ref={ref => {
            this._input = ref;
          }}
          type="file"
          accept={accept}
          className="file-input"
          onChange={this._onChange}
        />
      </StyledDiv>
    );
  }
}

export default FileInput;
