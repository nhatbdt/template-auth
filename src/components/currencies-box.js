import React from 'react';
import classnames from 'classnames';
import { useTranslation } from 'react-i18next';
import { Tooltip } from 'antd';
import styled from 'styled-components';

// import CURRENCIES from '../constants/currencies';
import Clickable from './clickable';
import { Colors } from '../theme';
import Media from '../utils/media';
import Typography from './typography';

const CurrenciesBoxStyled = styled.div`
  .currencies-box {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 27px;

    .label-box {
      display: flex;
      align-items: center;
      color: #282828 !important;

      .hint {
        margin-left: 10px;
        border: solid 1px #045AFC;
        width: 18px;
        height: 18px;
        border-radius: 9px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: #045AFC;
        font-size: 15px;
      }
    }

    ._right-box {
      display: flex;

      div {
        height: 40px;
        width: 80px;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 8px;
        border: solid 1px #dedede;
        margin-right: 13px;

        &:last-child {
          margin-right: 0;
        }

        .typography {
          opacity: 0.25;
        }

        &.active {
          background-color: ${Colors.BLUE_5};
          border: none;

          .typography {
            color: ${Colors.WHITE};
            opacity: 1;
          }
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .currencies-box {
      justify-content: flex-start;
      align-items: flex-start;
      flex-direction: column;
      margin-bottom: 20px;

      ._right-box {
        margin-top: 10px;

        div {
          height: 42px;
          width: 80px;
          margin-right: 10px;
        }
      }
    }
  }
`;

// const CURRENCIES_RESELL = [CURRENCIES.ETH, CURRENCIES.PRX];
function CurrenciesBox({ onClick, selectedCurrency, currencies }) {
  const { t } = useTranslation();

  const handleClick = currency => {
    if (onClick) onClick(currency);
  };

  return (
    <CurrenciesBoxStyled>
      <div className="currencies-box">
        <div className="label-box">
          <Typography size="big">{t('product_details:sell.currency')}</Typography>
          <Tooltip
            title={
              <Typography style={{ color: '#333333' }} size="small">
                {t('product_details:sell.hint_1')}
              </Typography>
            }
            color="white"
            trigger="click"
          >
            <Clickable className="hint">?</Clickable>
          </Tooltip>
        </div>
        <div className="_right-box">
          {/* {CURRENCIES_RESELL.map(currency => ( */}
          {currencies.map(currency => (
            <Clickable
              key={currency}
              className={classnames({ active: selectedCurrency === currency })}
              onClick={() => handleClick(currency)}
            >
              <Typography size="large">{currency}</Typography>
            </Clickable>
          ))}
        </div>
      </div>
    </CurrenciesBoxStyled>
  );
}

export default CurrenciesBox;
