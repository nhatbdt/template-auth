import React from 'react';
import styled from 'styled-components';
import { Colors } from '../theme';

function AspectRatio({ children, ratio, bgColor, bdRadius }) {
  const ratioStyle = {
    '--ratio': '56.25%',
    '--bgColor': bgColor ?? Colors.BOX_BACKGROUND,
    '--bdRadius': `${bdRadius}px` ?? 'none',
  };
  switch (ratio) {
    case '1:1':
      ratioStyle['--ratio'] = '100%'; /* 1:1 Aspect Ratio */
      break;

    case '2:7':
      ratioStyle['--ratio'] = '350%'; /* 2:7 Aspect Ratio (divide 7 by 2 = 3.5) */
      break;

    case '4:3':
      ratioStyle['--ratio'] = '75%'; /* 4:3 Aspect Ratio (divide 3 by 4 = 0.75) */
      break;

    case '3:2':
      ratioStyle['--ratio'] = '66.66%'; /* 3:2 Aspect Ratio (divide 2 by 3 = 0.6666)  */
      break;

    case '16:9':
      ratioStyle['--ratio'] = '56.25%'; /* 16:9 Aspect Ratio (divide 9 by 16 = 0.5625) */
      break;

    case '18:9':
      ratioStyle['--ratio'] = '50%'; /* 18:9 Aspect Ratio (divide 9 by 18 = 0.5)  */
      break;

    case '9:18':
      ratioStyle['--ratio'] = '200%'; /* 9:18 Aspect Ratio (divide 18 by 9 = 2)  */
      break;

    default:
      ratioStyle['--ratio'] = '100%'; /* 1:1 Aspect Ratio */
      break;
  }
  return (
    <AspectStyled className="aspect-container" style={ratioStyle}>
      <div className="aspect-content">{children}</div>
    </AspectStyled>
  );
}

export default AspectRatio;

const AspectStyled = styled.div`
  overflow: hidden;
  background-color: var(--bgColor);
  position: relative;
  width: 100%;
  border-radius: var(--bdRadius);
  padding-top: var(--ratio);
  cursor: default;
  .aspect-content {
    border-radius: var(--bdRadius);
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    text-align: center;
  }
`;
