import React from 'react';
import styled from 'styled-components';
import { useTranslation } from 'react-i18next';
import { inject, observer } from 'mobx-react';
import lodash from 'lodash';

import CURRENCIES from '../constants/currencies';
import Typography from './typography';
import Field from './field';
import InputNumber from './input-number';
import { Colors, Images } from '../theme';
import Media from '../utils/media';
import Format from '../utils/format';
import { getRateByCurrency } from '../utils/rates';
import { MAX_INPUT_VALUE } from '../constants/common';

const ProductInputPriceStyled = styled.div`
  .field-box {
    margin-top: 10px;

    .input-wrapper {
      display: flex;
      /* border: solid 1px rgba(255, 255, 255, 0.05); */
      border: solid 1px ${Colors.BORDER};
      border-radius: 8px;
      overflow: hidden;
      /* height: 77px; */

      .price_input {
        display: flex;

        .input-unit-box {
          padding: 0 20px;
          display: flex;
          align-items: center;
          justify-content: center;

          .nbng-icon {
            width: 33px;
            height: 33px;
            /* background-color: #ffffff; */
            border-radius: 17px;
            display: flex;
            justify-content: center;
            align-items: center;
            box-shadow: 0 0 6px 0 rgb(0 0 0 / 16%);
            margin-right: 8px;
            justify-content: center;
            align-items: center;
            overflow: hidden;

            img {
              width: 25px;
              height: 25px;
              object-fit: cover;
            }
          }

          .arrow-icon {
            margin-left: 20px;
          }
        }
      }

      ._input-outter {
        flex: 1;
        display: flex;
        flex-direction: column;
        /* max-width: 287.53px; */
        max-width: 100%;
        overflow: hidden;

        .input-box {
          flex: 1;

          /* > div {
            height: 100%;
          } */

          input {
            border: none;
            border-radius: 0;
            font-weight: bold;
            font-size: 20px;
            font-family: 'Poppins', sans-serif;
            padding: 10px 22px 0;
            text-align: right;
            height: 100%;
            min-height: 38px;
            color: ${Colors.TEXT};
            background-color: transparent;

            &:hover,
            &:focus {
              box-shadow: none;
            }
          }

          .error-box {
            display: none;
          }
        }

        .number-box {
          display: flex;
          justify-content: flex-end;
          align-items: center;
          gap: 4px 16px;
          padding-bottom: 7px;
          padding-right: 24px;

          .typography {
            opacity: 0.4;
            text-align: right;
          }

          .number-box-divider {
            width: 1px;
            background: black;
            opacity: 0.08;
            margin: 0 10px;
          }
        }
      }
    }

    ._input-outter {
      .input-value {
        .simple.input {
          color: ${Colors.TEXT};
          border: 1px solid #dedede;
        }
      }
    }

    .error-text {
      height: 20px;
      text-align: right;
      margin-top: 5px;
      color: red;
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .field-box {
      margin-top: 10px;

      .input-wrapper {
        flex-direction: column;
        height: auto;

        .price_input {
          display: flex;
          justify-content: flex-start;
          padding-top: 5px;
          padding-left: 10px;

          .input-unit-box {
            padding: 0;

            .nbng-icon {
              width: 30px;
              height: 30px;

              img {
                width: 21px;
              }
            }
          }
        }

        ._input-outter {
          .input-box {
            input {
              padding-top: 0;
              padding: 0 15px;
            }
          }

          .number-box {
            flex-direction: column;
            align-items: flex-end;
            padding-right: 15px;

            .number-box-divider {
              display: none;
            }
          }
        }
      }
    }
  }
`;

function ProductInputPrice({ selectedCurrency, values, errors, productsStore, authStore, inputName = 'sellPrice' }) {
  const { t } = useTranslation();

  const fetchPrice = async () => {
    const priceRateResult = await productsStore.getPriceRate();
    const priceRates = lodash.reduce(
      priceRateResult?.data,
      (result, item) => {
        result[item?.name] = +item?.value;
        return result;
      },
      {},
    );

    if (priceRateResult.success) {
      authStore.setInitialData({
        ...authStore.initialData,
        ...priceRates,
      });
    }
  };

  React.useEffect(() => {
    fetchPrice();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderNumberBox = price => {
    const masterData = authStore.initialData;
    // const { ethToJpy, prxToJpy } = authStore.initialData;
    // const pricesJpy = selectedCurrency === CURRENCIES.ETH ? ethToJpy * price : prxToJpy * price;

    return (
      <div className="number-box">
        <Typography poppins size="small">
          {Format.price(price) || 0} {selectedCurrency}
        </Typography>
        {/* <div className="number-box-divider" /> */}
        <Typography poppins size="small">
          {/* {Format.price(pricesJpy)} JPY */}
          {Format.price(price * getRateByCurrency(CURRENCIES[selectedCurrency], masterData))}
          &nbsp;
          {CURRENCIES.JPY}
        </Typography>
      </div>
    );
  };

  return (
    <ProductInputPriceStyled>
      <div className="label-box">
        <Typography size="big" bold>
          {t('product_details:sell.price')}
        </Typography>
        {/* <Tooltip
          title={
            <Typography style={{ color: '#333333' }} size="small">
              {t('product_details:sell.hint_2')}
            </Typography>
          }
          color="white"
          trigger="click"
        >
          <Clickable className="hint">?</Clickable>
        </Tooltip> */}
      </div>
      <div className="field-box">
        <div className="input-wrapper">
          <div className="price_input">
            <div className="input-unit-box">
              <div className="nbng-icon">
                <img src={Images[selectedCurrency]} alt="" />
              </div>
              <Typography poppins bold size="huge">
                {selectedCurrency}
              </Typography>
            </div>
          </div>
          <div className="_input-outter">
            <Field
              className="input-box bold"
              type="number"
              name={inputName}
              maxLength={9}
              placeholder="0"
              boundedbelow={1}
              simple
              max={MAX_INPUT_VALUE}
              component={InputNumber}
              min={0.0001}
            />
            {renderNumberBox(values[inputName])}
          </div>
        </div>
        <Typography className="error-text" size="small" primary>
          {errors[inputName] || ''}
        </Typography>
      </div>
    </ProductInputPriceStyled>
  );
}

export default inject(stores => ({
  authStore: stores.auth,
  productsStore: stores.products,
}))(
  observer(({ authStore, productsStore, ...props }) => (
    <ProductInputPrice {...props} authStore={authStore} productsStore={productsStore} />
  )),
);
