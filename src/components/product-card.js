import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { useTranslation } from 'react-i18next';
import numeral from 'numeral';
import { MobXProviderContext } from 'mobx-react';

import { BLUE_1 } from '../theme/colors';
import { GRAY_STAR_ICON } from '../theme/images';

import Media from '../utils/media';
import Format from '../utils/format';
import Thumbnail from './thumbnail';
import Clickable from './clickable';
import Typography from './typography';
import { getRateByCurrency } from '../utils/rates';
import CURRENCIES from '../constants/currencies';

function useStores() {
  return React.useContext(MobXProviderContext);
}

const StyledDiv = styled(Clickable)`
  position: relative;
  display: flex;
  background-color: #313343;
  border-radius: 4px;
  // overflow: hidden;
  box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.04);
  height: 100%;

  .product-card-left-box {
    flex: 1;
    display: flex;
    flex-direction: column;
    min-width: 0;

    .image-box {
      position: relative;
      width: 100%;
      padding-top: 100%;
      overflow: hidden;
      border-top-left-radius: 6px;
      border-top-right-radius: 6px;

      .count-offer {
        position: absolute;
        top: 10px;
        right: 10px;
        background: #fff100;
        border-radius: 2px;
        padding: 0 4px;
        p {
          color: #222;
          font-size: 12px;
        }
      }

      .image {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0px;
        left: 0px;
        background-position: center center;

        &.image-blur {
          filter: blur(15px);
        }
      }
      .item-18 {
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        top: 0;
        .item-18-inside {
          display: flex;
          justify-content: center;
          align-items: center;
          height: 100%;
          flex-direction: column;

          p {
            font-size: 12px;
            padding-top: 15px;
            color: #ffffff;
          }
        }
      }
    }

    .info-outer-box {
      .product-card-info-box {
        background-color: #2b2c3c;
        padding: 10px;

        ._product-card-description {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
          font-size: 11px;
          opacity: 0.5;
          ${Media.lessThan(Media.SIZE.MD)} {
            font-size: 10px;
          }
        }

        ._product-card-name {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
          font-size: 12px;
          line-height: 16px;
        }
      }

      ._product-card-price-box {
        display: flex;
        padding: 10px;
        flex-direction: column;

        ._horizontal {
          display: flex;
          justify-content: space-between;
          margin-bottom: 8px;
          align-items: flex-start;
          ${Media.lessThan(Media.SIZE.MD)} {
            margin-bottom: 4px;
            padding-left: 11px;
          }
          &:last-child {
            margin-bottom: 0;
          }

          .category {
            font-size: 12px;
            padding-top: 5px;
          }

          ._status-text {
            font-size: 11px;
            color: #cccccc;
            padding-top: 5px;
            display: flex;
            align-items: center;
            ._dot {
              display: flex;
              width: 9px;
              height: 9px;
              border-radius: 50%;
              margin-right: 6px;
              background-color: #db1d31;
            }

            &.holding ._dot {
              background-color: #d2c436;
            }

            &.before-auction ._dot {
              background-color: #fff1ab;
            }

            &.during-auction ._dot {
              background-color: #ffe310;
            }

            &.comming-soon ._dot {
              background-color: #a8a8a8;
            }

            &.sale ._dot {
              background-color: #2fc42f;
            }

            &.resale ._dot {
              background-color: #1b8beb;
            }
          }

          ._price {
            font-size: 20px;
            font-weight: bold;
            ._price-amount {
              font-size: 20px;
              &._price-amount-custom {
                font-size: 12px;
              }
              span {
                font-size: 11px;
                margin-left: 3px;
                font-weight: bold;
              }
              ${Media.lessThan(Media.SIZE.MD)} {
                font-size: 17px;
                line-height: 22px;
                span {
                  line-height: 17px;
                }
              }
            }
          }

          ._product-card-field-group {
            display: flex;
            margin-right: 5px;

            .product-card-field {
              margin-right: 23px;

              &:last-child {
                margin-right: 0;
              }
            }
          }

          .product-card-field {
            display: flex;
            justify-content: flex-end;
            img {
              margin-right: 4px;
            }

            .typography {
              font-size: 11px;
              opacity: 0.5;
            }
          }

          &._product-card {
            border-top: 1px solid #414352;
            padding-top: 10px;
            ${Media.lessThan(Media.SIZE.MD)} {
              padding-top: 6px;
              padding-bottom: 6px;
            }
            ._product-card-field-group {
              justify-content: space-between;
              flex: 1;
              align-items: center;
              .product-card-field {
                align-items: center;
                .offer {
                  color: #9899a1;
                  font-size: 12px;
                  // padding-left: 5px;
                }
              }
            }
          }

          ._item-offer {
            > p {
              padding-right: 5px;
            }
            ._tag-box {
              background-color: rgb(254, 192, 2);
              width: 70px;
              height: 22px;
              border-radius: 5px;
              display: flex;
              -webkit-box-pack: center;
              justify-content: center;
              -webkit-box-align: center;
              align-items: center;
            }

            .camelcase-price {
              font-size: 14px;
              span {
                font-size: 11px;
                margin-left: 3px;
                font-weight: bold;
              }
            }
          }
        }
      }
    }
  }

  .product-card-right-box {
    margin-left: 15px;
    flex: 1;

    .typography {
      white-space: pre-wrap;
      word-break: break-word;
      text-align: justify;
    }
  }

  .number-ranking {
    position: absolute;
    top: -5px;
    left: -5px;
    background-color: #fff235;
    width: 30px;
    height: 30px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 5px;

    p {
      color: #000;
      font-size: 19px;
      font-weight: bold;
      margin-top: -2px;
      margin-left: -2px;
    }
  }

  &.smallInMobile {
    ${Media.lessThan(Media.SIZE.MD)} {
      height: 123px;

      .product-card-left-box {
        display: flex;
        flex-direction: row;

        .image-box {
          width: 123px;
          height: 100%;
          padding-top: 0;
          border-top-right-radius: 0;
          border-bottom-left-radius: 4px;
          .item-18 .item-18-inside p {
            font-size: 10px;
          }
        }

        .info-outer-box {
          flex: 1;
          display: flex;
          flex-direction: column;
          border-top-right-radius: 4px;
          overflow: hidden;

          .product-card-info-box {
            padding: 6px 11px 5px 11px;
          }

          ._product-card-price-box {
            padding: 3px 11px 0 0;
            flex: 1;
            justify-content: center;
            display: flex;
            flex-direction: column;
            ._horizontal .category {
              padding-top: 3px;
            }
          }
        }
      }
    }
  }
  &.card-light {
    background-color: #eaf0fc;
    .product-card-left-box {
      .info-outer-box {
        .product-card-info-box {
          background-color: rgba(255, 255, 255, 0.6);
        }
        ._product-card-price-box ._horizontal._product-card {
          border-top: 1px solid rgba(0, 0, 0, 0.2);
        }
        ._product-card-price-box ._horizontal ._item-offer ._tag-box {
          background-color: ${BLUE_1};
        }
      }
      .image-box .count-offer {
        background-color: ${BLUE_1};
        color: #ffffff;
        p {
          color: #ffffff;
        }
      }
    }
  }
`;

const ProductCard = ({
  item,
  showDescription,
  smallInMobile,
  index = 0,
  showNumberRanking = false,
  showCountOffer = false,
  className,
  ...props
}) => {
  const { i18n } = useTranslation();
  const { auth } = useStores();
  // const { usdToJpy } = auth.initialData;
  const masterData = auth.initialData;

  const { t } = useTranslation('common');
  const price =
    item?.tokenType === 'MULTI'
      ? item?.childPrice
      : item.productBid?.biggestBidPrice
      ? item.productBid?.biggestBidPrice
      : item.price;
  const yenPrice =
    item?.tokenType === 'MULTI'
      ? item?.yenChildPrice
      : item.productBid?.biggestBidPriceYen
      ? item.productBid?.biggestBidPriceYen
      : item.yenPrice;
  const statusText = item?.holding
    ? t('product_status.holding')
    : item?.status === 'NEW'
    ? item?.productBid
      ? t('product_status.before_auction')
      : t('product_status.coming_soon')
    : item?.status === 'SALE'
    ? item?.productBid
      ? t('product_status.during_auction')
      : item?.reselling
      ? t('product_status.resale')
      : t('product_status.sale')
    : t('product_status.sold');
  const isAgeEnough = localStorage.getItem('IS_AGE_ENOUGH');

  const isShowAgeDescription = !isAgeEnough && !item?.parentProductId;

  const _renderCamelCasePrice = (price, currency) => {
    const isShowPriceSuffix = price - parseInt(price, 10) > 0;
    price = numeral(price).format('0,0.000');

    return (
      <Typography className="camelcase-price">
        {price.slice(0, price.length - 4)}
        <span className="bold">
          {isShowPriceSuffix ? price.slice(price.length - 4, price.length) : ''}
          &nbsp;
          {currency}
        </span>
      </Typography>
    );
  };

  return (
    <StyledDiv {...props} className={classnames(className, { padding: item.padding, smallInMobile })}>
      <div className="product-card-left-box">
        <div className="image-box" style={{ backgroundColor: item.backgroundColor || 'rgb(255,206,206)' }}>
          <Thumbnail
            className={`image ${item?.ageRestricted && isShowAgeDescription && 'image-blur'}`}
            contain={!!item.padding}
            url={item.imageThumbnailUrls?.[0] || item.imageUrl}
          />
          {showCountOffer && item?.countOffer && item?.countOffer > 0 ? (
            <div className="count-offer">
              <Typography>{item?.countOffer > 100 ? '99+' : item?.countOffer}</Typography>
            </div>
          ) : (
            ''
          )}
          {item?.ageRestricted && isShowAgeDescription && (
            <div className="item-18">
              <div className="item-18-inside">
                {/* <img src={Images.ICON_18} alt="icon 18+" /> */}
                <Typography className="_product-card-description">{t('limit_content')}</Typography>
              </div>
            </div>
          )}
        </div>
        <div className="info-outer-box">
          {(item.idolName || item.title) && (
            <div className="product-card-info-box">
              <Typography className="_product-card-description">{item.idolName}</Typography>
              <Typography className="_product-card-name">{item.title}</Typography>
            </div>
          )}
          <div className="_product-card-price-box">
            <div className="_horizontal">
              <div>
                <Typography
                  className={classnames(
                    '_status-text',
                    item?.holding
                      ? 'holding'
                      : item?.status === 'NEW'
                      ? item?.productBid
                        ? 'before-auction'
                        : 'comming-soon'
                      : item?.status === 'SALE'
                      ? item?.productBid
                        ? 'during-auction'
                        : item?.reselling
                        ? 'resale'
                        : 'sale'
                      : 'sold',
                  )}
                >
                  <span className="_dot" />
                  <span className="_text">{statusText}</span>
                </Typography>
                {/* <Typography className="category">
                  {item?.productAttributes?.[0]?.name}
                </Typography> */}
              </div>
              <div className="_price bold">
                <Typography
                  poppins
                  className={`_price-amount ${price.toString().length > 10 && '_price-amount-custom'}`}
                >
                  {Format.price(price)}
                  <span className="bold">{item?.currency}</span>
                </Typography>
                <div className="product-card-field">
                  <Typography poppins>
                    {/* {Format.price(yenPrice)} JPY */}
                    {i18n.language.toUpperCase() === 'JA'
                      ? `${Format.price(yenPrice)}JPY`
                      : `${Format.price(yenPrice / getRateByCurrency(CURRENCIES.USD, masterData))} ${CURRENCIES.USD}`}
                    {/* : `${Format.price(yenPrice / usdToJpy)} USD`} */}
                  </Typography>
                </div>
              </div>
            </div>
            {item.offerNftTransactionStatus === 'SUCCESS' && (
              <div className="_horizontal">
                <Typography className="_label" size="small" secondary poppins>
                  Offer
                </Typography>
                <div className="_item-offer">
                  {!item.offerHighestPrice ? (
                    <div className="_tag-box">
                      <Typography size="small">Open</Typography>
                    </div>
                  ) : (
                    <div className="_number-box">
                      {_renderCamelCasePrice(item.offerHighestPrice, item.offerHighestPriceCurrency)}
                    </div>
                  )}
                </div>
              </div>
            )}
            <div className="_horizontal _product-card">
              <div className="_product-card-field-group">
                <div className="product-card-field">
                  {/* <Typography poppins>
                    {Format.abridgeNumber(item.countView)}
                  </Typography> */}
                  <Typography poppins className="offer">
                    {item?.itemType ? t(`product_type.${item?.itemType}`) : ''}
                  </Typography>
                </div>
                <div className="product-card-field">
                  <img src={GRAY_STAR_ICON} alt="" />
                  <Typography poppins>{Format.abridgeNumber(item?.countFavorite || 0)}</Typography>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {showDescription && (
        <div className="product-card-right-box">
          <Typography size="big" bold>
            {item.description}
          </Typography>
        </div>
      )}
      {showNumberRanking && (
        <div className="number-ranking bold">
          <Typography size="big" bold className="bold">
            {index + 1}
          </Typography>
        </div>
      )}
    </StyledDiv>
  );
};
ProductCard.propTypes = {
  onClick: PropTypes.func,
  showDescription: PropTypes.bool,
  smallInMobile: PropTypes.bool,
  item: PropTypes.object,
};
ProductCard.defaultProps = {
  smallInMobile: true,
};

export default ProductCard;
