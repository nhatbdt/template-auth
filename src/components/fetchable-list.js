import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Row, Col } from 'antd';
import { observer } from 'mobx-react';
import classnames from 'classnames';

// import Configs from '../configs';
import Loading from '../components/loading';
import Button from '../components/button';
import NoDataBox from '../components/no-data-box';
import Media from '../utils/media';
import { useTranslation } from 'react-i18next';

const StyledDiv = styled.div`
  padding-bottom: 70px;
  ${Media.lessThan(Media.SIZE.MD)} {
    padding-bottom: 150px;
  }
  .fetchable-no-data {
    padding: 60px 0;
    border-radius: 5px;
  }

  .bottom-box {
    display: flex;
    justify-content: center;
    margin-top: 50px;

    .see-more-button {
      background-color: #045afc !important;
      height: 40px;
      padding: 0 30px;
    }
  }
`;
@observer
class FetchableList extends Component {
  static propTypes = {
    renderItem: PropTypes.func,
    keyExtractor: PropTypes.func,
    action: PropTypes.func.isRequired,
    payload: PropTypes.object,
    items: PropTypes.array.isRequired,
    total: PropTypes.number,
    page: PropTypes.number,
    onFetched: PropTypes.func,
    loading: PropTypes.bool,
    noDataMessage: PropTypes.string,
    pagination: PropTypes.bool,
    autoFetchOnMount: PropTypes.bool,
    colSpanXl: PropTypes.number,
    colSpanLg: PropTypes.number,
    colSpanMd: PropTypes.number,
    colSpanXs: PropTypes.number,
    limit: PropTypes.number,
    colClassName: PropTypes.string,
    gutter: PropTypes.array,
    length: PropTypes.number,
    showLoadMoreButton: PropTypes.bool,
    loadMoreText: PropTypes.string,
    loadMoreColor: PropTypes.string,
  };

  static defaultProps = {
    pagination: true,
    autoFetchOnMount: true,
    keyExtractor: () => null,
  };

  state = {
    initing: true,
    loadingMore: false,
    newPayload: {},
  };

  async componentDidMount() {
    const { autoFetchOnMount } = this.props;

    if (autoFetchOnMount) {
      await this._fetchData(1);
    }

    this.setState({
      initing: false,
    });
  }

  _fetchData = async (page, concat) => {
    const { action, onFetched, payload, limit } = this.props;
    const { newPayload } = this.state;

    const result = await action(
      {
        page,
        limit: limit || 10,
        ...payload,
        ...newPayload,
      },
      { page, concat },
    );

    if (onFetched) {
      onFetched(result, { page });
    }
  };

  fetchDataWithNewPayload = async (newPayload = {}) => {
    // this.state.newPayload = newPayload;
    this.setState(newPayload);

    await this._fetchData(1);
  };

  setNewPayload = newPayload => {
    // this.state.newPayload = newPayload;
    this.setState(newPayload);
  };

  loadMore = async () => {
    await this._onLoadMore();
  };

  _onLoadMore = async () => {
    const { page, total, pagination, limit } = this.props;

    let pageTotal = total / (limit || 10);
    pageTotal = parseInt(pageTotal, 10) + (pageTotal - parseInt(pageTotal, 10) > 0 ? 1 : 0);

    if (!pagination || this._isLoadingMore || page > pageTotal - 1) return;

    this._isLoadingMore = true;

    this.setState({
      loadingMore: true,
    });

    await this._fetchData(page + 1, true);

    this.setState({
      loadingMore: false,
    });

    this._isLoadingMore = false;
  };

  _renderItem = (item, index) => {
    const { renderItem, keyExtractor, colSpanXl, colSpanLg, colSpanMd, colSpanSM, colSpanXs, colClassName } =
      this.props;

    return (
      <Col
        className={colClassName || ''}
        xl={colSpanXl || 4}
        lg={colSpanLg || 6}
        md={colSpanMd || 8}
        sm={colSpanSM || 12}
        xs={colSpanXs || 24}
        key={keyExtractor(item, index)}
      >
        {renderItem(item, index)}
      </Col>
    );
  };

  render() {
    const {
      items,
      className,
      loading,
      noDataMessage,
      gutter,
      showLoadMoreButton,
      loadMoreText,
      pagination,
      total,
      t,
      loadMoreColor,
    } = this.props;
    const { loadingMore, initing } = this.state;

    return (
      <StyledDiv className={className}>
        {!initing && !loading && (
          <Row
            gutter={
              gutter || [
                { xs: 15, sm: 15, md: 20 },
                { xs: 15, sm: 15, md: 20 },
              ]
            }
          >
            {items.map(this._renderItem)}
          </Row>
        )}
        {items.length === 0 && !loading && !initing && (
          <div className="fetchable-no-data">
            <NoDataBox message={noDataMessage} />
          </div>
        )}
        {(initing || loading) && <Loading />}
        {loadingMore && (
          <div className="bottom-box">
            <Loading size="small" />
          </div>
        )}
        {!initing && !loading && pagination && showLoadMoreButton && !loadingMore && items.length < total && (
          <div className="bottom-box">
            <Button
              className={classnames('loadmore-button', loadMoreColor)}
              onClick={this._onLoadMore}
              background="#045AFC"
            >
              {loadMoreText || t('common:load_more')}
            </Button>
          </div>
        )}
      </StyledDiv>
    );
  }
}

// export default FetchableList;
const FetchableListWrapper = props => {
  const { i18n, t } = useTranslation('common');

  return <FetchableList {...props} key={i18n.language} t={t} />;
};

export default FetchableListWrapper;
