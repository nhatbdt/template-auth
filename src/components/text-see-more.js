/* eslint-disable consistent-return */
import React from 'react'
import { withTranslation } from 'react-i18next'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyleDiv = styled.div`
  position: relative;
  margin-bottom: 30px;
  p {
    font-size: 13px;
  }
  .button-container {
    width: 100%;
    text-align: center;
    position: absolute;
    bottom: 0;
    background: rgba(255, 255, 255, 0.9);
    padding: 10px 0;
    button {
      background: transparent;
      border: none;
      cursor: pointer;

      .icon {
        padding-left: 20px;
      }
    }
  }
  .button-show-less {
    width: 100%;
    text-align: center;
    margin-top: 10px;
    button {
      border: none;
      cursor: pointer;

      .icon {
        padding-left: 20px;
      }
    }
  }
`

const TextSeeMore = ({ text, t, textNumShow = 400 }) => {
  const [showMore, setShowMore] = React.useState(false)
  
  if (!text) return null


  const getText = () => {
    if (text?.length <= textNumShow) return text
    if (text?.length > textNumShow && showMore) {
      return (
        <>
          <p>{text}</p>
          <div className="button-show-less">
            <button
              onClick={() => setShowMore(false)}
            >
              <span>{t('show_less')}</span>
              <span className="icon">{' - '}</span>
            </button>
          </div>
        </>
      )
    }
    if (text?.length > textNumShow) {
      return (
        <>
          <p>{text.slice(0, textNumShow)}</p>
          <div className="button-container">
            <button
              onClick={() => setShowMore(true)}
            >
              <span>{t('show_more')}</span>
              <span className="icon">{' + '}</span>
            </button>
          </div>
        </>
      )
    }
  }

  return (
    <StyleDiv>
      {getText()}
    </StyleDiv>
  )
}

TextSeeMore.propTypes = {
  text: PropTypes.string
}

export default withTranslation('common')(TextSeeMore)
