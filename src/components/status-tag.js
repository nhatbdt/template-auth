import React from 'react';
import { Tag } from 'antd';
import classNames from 'classnames';
import styled from 'styled-components';
import { LENDING_STATUS, STAKING_STATUS } from '../constants/statuses'

const TagStyled = styled(Tag)`
  &.status {
    display: flex;
    min-width: 86px;
    min-height: 24px;
    border-radius: 9999px;
    border: none;
    .status-name {
      margin: auto;
      font-weight: 500;
      max-width: 300px;
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
    }
  }
`;

function StatusTag({ item, text, className }) {
  const lendingStatus = item?.lendingDetailDTO?.status || item?.lendingStatus
  const stakingStatus = item?.stakingStatus
  
  return (
    <TagStyled
      className={classNames('status', className)}
      bordered="false"
      color={
        lendingStatus === LENDING_STATUS.LENDING ? 'blue' :
          stakingStatus === (STAKING_STATUS.STAKING || STAKING_STATUS.STAKING_PROGRESS) ? 'gold' :
            (item?.offerNftTransactionStatus === 'NEW' && item?.canOfferFlag) ? 'red' :
              lendingStatus === LENDING_STATUS.IN_LENDING ? 'gold'
                : item?.holding
                  ? 'purple'
                  : item?.status === 'NEW'
                    ? item?.productBid
                      ? 'magenta'
                      : 'blue'
                    : item?.status === 'SALE'
                      ? item?.productBid
                        ? 'red'
                        : item?.reselling
                          ? 'red'
                          : 'green'
                      : 'cyan'
      }
    >
      <span className="status-name">{text}</span>
    </TagStyled>
  );
}

export default StatusTag;
