import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Media from '../utils/media';
import { Colors } from '../theme';
import { Dropdown, Menu, Button } from 'antd';
import styled from 'styled-components';
import { withTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';
import { DownOutlined } from '@ant-design/icons';

const StyledDiv = styled.div`
  display: flex;
  align-items: center;
  flex-wrap: wrap;
  padding: 10px 0;
  margin-bottom: 30px;
  border-bottom: 1px solid ${Colors.WHITE_12};
  .search-item {
    border-right: 1px solid ${Colors.WHITE_20};
    margin-right: 30px;
    padding: 14px 20px 14px 10px;
    .ant-btn {
      border: none;
      display: flex;
      align-items: center;
      justify-content: space-between;
      height: auto;
      min-width: 170px;
      padding: 0;
    }
  }
  ${Media.lessThan(Media.SIZE.MD)} {
    border-bottom: none;
    padding: 0;
    margin-top: 10px;
    background: #101119;
    border-radius: 4px;
    .search-item {
      width: 100%;
      margin-right: 0;
      border-right: none;
      border-bottom: 1px solid ${Colors.WHITE_20};
      padding: 0;
      .ant-btn {
        padding: 11px 12px;
        width: 100%;
      }
      &:first-child {
        .ant-btn {
          border-radius: 4px 4px 0 0;
        }
      }
      &:last-child {
        border-bottom: none;
        .ant-btn {
          border-radius: 0 0 4px 4px;
        }
      }
    }
  }
`;

@withRouter
@withTranslation('common')
class PersonSearch extends Component {
  static propTypes = {
    className: PropTypes.string,
    defaultSearchCountry: PropTypes.string,
    defaultSearchCategory: PropTypes.string,
    defaultSearchAlphabet: PropTypes.string,
  };

  constructor(props) {
    super(props);
    const { defaultSearchCountry, defaultSearchCategory, defaultSearchAlphabet } = props;

    this.state = {
      searchCountry: defaultSearchCountry || 'all',
      searchCategory: defaultSearchCategory || 'all',
      searchAlphabet: defaultSearchAlphabet || 'all',
    };
  }

  _onSearchCountrySelected = e => {
    this.setState({
      searchCountry: e.key,
    });
  };

  _onSearchCategorySelected = e => {
    this.setState({
      searchCategory: e.key,
    });
  };

  _onSearchAlphabetSelected = e => {
    this.setState({
      searchAlphabet: e.key,
    });
  };

  render() {
    const { className } = this.props;
    const { searchCountry, searchCategory, searchAlphabet } = this.state;

    const SEARCH_COUNTRIES = [
      {
        name: '全ての国',
        value: 'all',
      },
      {
        name: '日本',
        value: 'japan',
      },
      {
        name: 'イングランド',
        value: 'england',
      },
    ];
    const SEARCH_CATEGORIES = [
      {
        name: 'カテゴリを選択',
        value: 'all',
      },
      {
        name: 'Idol',
        value: 'idol',
      },
    ];
    const SEARCH_ALPHABET = [
      {
        name: 'アルファベット',
        value: 'all',
      },
      {
        name: 'A',
        value: 'a',
      },
      {
        name: 'B',
        value: 'b',
      },
      {
        name: 'C',
        value: 'c',
      },
    ];

    return (
      <StyledDiv className={className}>
        <div className="search-item">
          <Dropdown
            trigger="click"
            overlay={
              <Menu>
                {SEARCH_COUNTRIES.map(item => (
                  <Menu.Item key={item.value} onClick={this._onSearchCountrySelected}>
                    {item.name}
                  </Menu.Item>
                ))}
              </Menu>
            }
          >
            <Button>
              {searchCountry === 'all' ? SEARCH_COUNTRIES[0].name : SEARCH_COUNTRIES[1].name} <DownOutlined />
            </Button>
          </Dropdown>
        </div>
        <div className="search-item">
          <Dropdown
            trigger="click"
            overlay={
              <Menu>
                {SEARCH_CATEGORIES.map(item => (
                  <Menu.Item key={item.value} onClick={this._onSearchCategorySelected}>
                    {item.name}
                  </Menu.Item>
                ))}
              </Menu>
            }
          >
            <Button>
              {searchCategory === 'all' ? SEARCH_CATEGORIES[0].name : SEARCH_CATEGORIES[1].name} <DownOutlined />
            </Button>
          </Dropdown>
        </div>
        <div className="search-item">
          <Dropdown
            trigger="click"
            overlay={
              <Menu>
                {SEARCH_ALPHABET.map(item => (
                  <Menu.Item key={item.value} onClick={this._onSearchAlphabetSelected}>
                    {item.name}
                  </Menu.Item>
                ))}
              </Menu>
            }
          >
            <Button>
              {searchAlphabet === 'all' ? SEARCH_ALPHABET[0].name : SEARCH_ALPHABET[1].name} <DownOutlined />
            </Button>
          </Dropdown>
        </div>
      </StyledDiv>
    );
  }
}

export default PersonSearch;
