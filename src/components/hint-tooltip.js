import { Tooltip } from 'antd';
import PropTypes from 'prop-types';

import Typography from '../components/typography';
import Clickable from '../components/clickable';

const HintTooltip = ({ content }) => (
  <Tooltip
    title={
      <Typography style={{ color: '#333333' }} size="small">
        {content}
      </Typography>
    }
    color="white"
    trigger="click"
  >
    <Clickable className="hint">?</Clickable>
  </Tooltip>
);
HintTooltip.propTypes = {
  content: PropTypes.string,
};

export default HintTooltip;
