import { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { DatePicker, TimePicker } from 'antd'
import moment from 'moment'

import Typography from '@/components/typography'

const StyledDiv = styled.div`
  border: solid 1px #dedede;
  border-radius: 8px;
  padding: 8px 14px;
  
  .horizontal {
    display: flex;
    width: 100%;
    
    ._divider {
      width: 1px;
      height: 22px;
      background-color: #dedede;
      margin: 0 15px;
    }
    
    .ant-picker {
      flex: 1;
      border: none;
      box-shadow: none;
      padding: 0;
      
      &:last-child {
        flex: 0.8;
      }
    }
  }
`

class DateTimePicker extends Component {
  static propTypes = {
    field: PropTypes.object,
    form: PropTypes.object,
    value: PropTypes.any,
    inputLabel: PropTypes.string
  }

  _onChange = (type, value) => {
    const { field, form } = this.props

    let saveDateTime = ''

    if (type === 'date') {
      const prevTime = field.value ? field.value.format('HH:mm') : '00:00'
      saveDateTime = `${value.format('YYYY-MM-DD')} ${prevTime}`
    } else {
      const prevDate = field.value ? field.value.format('YYYY-MM-DD') : moment().format('YYYY-MM-DD')
      saveDateTime = `${prevDate} ${value.format('HH:mm')}`
    }

    saveDateTime = moment(saveDateTime)

    if (field && form) form.setFieldValue(field.name, saveDateTime)
  }

  _disabledDate = (current) => current < moment().subtract(1, 'd').endOf('day')

  render() {
    const { field, inputLabel } = this.props

    return (
      <StyledDiv>
        <Typography size="small" secondary>
          {inputLabel}
        </Typography>
        <div className="horizontal">
          <DatePicker
            disabledDate={this._disabledDate}
            showToday={false}
            format="YYYY.MM.DD"
            placeholder=""
            value={field.value}
            allowClear={false}
            onChange={(value) => this._onChange('date', value)}
          />
          <div className="_divider" />
          <TimePicker
            showNow={false}
            minuteStep={5}
            format="HH:mm"
            placeholder=""
            value={field.value}
            allowClear={false}
            onSelect={(value) => this._onChange('time', value)}
          />
        </div>
      </StyledDiv>
    )
  }
}

export default DateTimePicker
