import React from 'react';
import styled from 'styled-components';
import { useTranslation } from 'react-i18next';
import { Menu, Dropdown } from 'antd';
import lodash from 'lodash';

import Typography from './typography';
import Clickable from './clickable';
import Field from './field';
import InputNumber from './input-number';
import CURRENCIES from '../constants/currencies';
import { Colors, Images } from '../theme';
import { inject, observer } from 'mobx-react';
// import HintTooltip from './hint-tooltip';
import Media from '../utils/media';
import Format from '../utils/format';
import { getRateByCurrency } from '../utils/rates';
import { MAX_INPUT_VALUE } from '../constants/common';

const ProductInputAmountStyled = styled.div`
  .label-box {
    .typography {
      font-weight: 500;
      font-size: 16px;
      color: #fff;
    }
  }
  .field-box {
    margin-top: 10px;
    max-width: 363px;
    ${Media.lessThan(Media.SIZE.MD)} {
      margin-top: 10px;
    }
    .input-price-wrapper {
      display: flex;
      border: 1px solid #65676b;
      border-radius: 8px;
      overflow: hidden;
      min-height: 54px;
      ${Media.lessThan(Media.SIZE.MD)} {
        /* flex-direction: column; */
        height: auto;
      }

      .price_input {
        display: flex;
        ${Media.lessThan(Media.SIZE.MD)} {
          display: flex;
          justify-content: flex-start;
          padding-top: 5px;
          padding-left: 10px;
        }

        .input-unit-box {
          padding: 0 20px;
          display: flex;
          align-items: center;
          justify-content: center;
          ${Media.lessThan(Media.SIZE.MD)} {
            padding: 0;
            height: 54px;
          }

          .typography {
            color: #fff;
            font-weight: 700;
            font-size: 18px;
          }

          .nbng-icon {
            width: 33px;
            height: 33px;
            background-color: #1b1e24;
            border-radius: 17px;
            display: flex;
            justify-content: center;
            align-items: center;
            box-shadow: 0 0 6px 0 rgb(0 0 0 / 16%);
            margin-right: 8px;
            justify-content: center;
            align-items: center;
            overflow: hidden;
            ${Media.lessThan(Media.SIZE.MD)} {
              width: 30px;
              height: 30px;
            }

            img {
              width: 25px;
              height: 25px;
              /* object-fit: cover; */
              ${Media.lessThan(Media.SIZE.MD)} {
                width: 21px;
              }
            }
          }

          .arrow-icon {
            margin-left: 20px;
          }
        }
      }

      ._input-outter {
        flex: 1;
        // background: white;
        height: 100%;
        display: flex;
        flex-direction: column;
        /* max-width: 221.88px; */
        max-width: 100%;
        overflow: hidden;

        .input-box {
          flex: 1;
          height: 100%;
          color: #282828;
          font-size: 18px;

          > div {
            height: 100%;
          }

          input {
            background-color: transparent;
            border: none;
            border-radius: 0;
            font-weight: bold;
            font-size: 20px;
            padding: 10px 17px 0 17px;
            text-align: right;
            height: 100%;
            color: #fff;
            opacity: 0.4;
            ${Media.lessThan(Media.SIZE.MD)} {
              padding-top: 0;
              padding: 0 15px;
            }

            &:hover,
            &:focus {
              box-shadow: none;
            }
          }

          .error-box {
            display: none;
          }
        }
        .number-box {
          display: flex;
          justify-content: flex-end;
          gap: 2px 8px;
          flex-wrap: wrap;
          padding-bottom: 6px;
          padding-right: 17px;
          ${Media.lessThan(Media.SIZE.MD)} {
            flex-direction: column;
            align-items: flex-end;
            padding-right: 15px;
          }

          .typography {
            opacity: 0.4;
            text-align: right;
            color: ${Colors.TEXT};
            font-weight: 400;
            font-size: 10px;
          }

          .number-box-divider {
            width: 1px;
            background: black;
            opacity: 0.08;
            margin: 0 10px;
            ${Media.lessThan(Media.SIZE.MD)} {
              display: none;
            }
          }
        }
      }
    }

    .error-text {
      text-align: right;
      margin-top: 5px;
      color: red;
    }
  }
`;

function ProductInputAmount({
  titleInput,
  selectedCurrency,
  values,
  errors,
  errorsValue,
  authStore,
  productsStore,
  onSelect,
  currencies,
  className,
}) {
  const { t } = useTranslation();

  const fetchPrice = async () => {
    const priceRateResult = await productsStore.getPriceRate();
    const priceRates = lodash.reduce(
      priceRateResult?.data,
      (result, item) => {
        result[item?.name] = +item?.value;
        return result;
      },
      {},
    );

    if (priceRateResult.success) {
      authStore.setInitialData({
        ...authStore.initialData,
        ...priceRates,
      });
    }
  };
  React.useEffect(() => {
    fetchPrice();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderNumberBox = price => {
    const masterData = authStore.initialData;
    // const { ethToJpy, prxToJpy, usdToJpy } = authStore.initialData;
    // const pricesJpy = selectedCurrency === CURRENCIES.WETH ? ethToJpy * price : prxToJpy * price;

    return (
      <div className="number-box">
        <Typography poppins size="small">
          {/* {Format.price(pricesJpy / usdToJpy)} USD */}
          {Format.price(
            (price * getRateByCurrency(CURRENCIES[selectedCurrency], masterData)) /
              +getRateByCurrency(CURRENCIES.USD, masterData),
          )}
          &nbsp;
          {CURRENCIES.USD}
        </Typography>
        {/* <div className="number-box-divider" /> */}
        <Typography poppins size="small">
          {/* {Format.price(pricesJpy)} JPY */}
          {Format.price(price * getRateByCurrency(CURRENCIES[selectedCurrency], masterData))}
          &nbsp;
          {CURRENCIES.JPY}
        </Typography>
      </div>
    );
  };

  const errorException = ['MINIMUM_PRICE_THAN_OR_EQUAL'];

  return (
    <ProductInputAmountStyled className={className}>
      <div className="normal-panel">
        <div className="label-box">
          <Typography size="big" bold>
            {titleInput || t('product_details:offer.set_amount')}
          </Typography>
          {/* <HintTooltip content={t('product_details:offer.hint_1')} /> */}
        </div>
        <div className="field-box">
          <div className="input-price-wrapper">
            <Dropdown
              trigger="click"
              overlayClassName="custom-dropdown"
              overlay={
                <Menu>
                  {currencies?.map((item, index) => (
                    <Menu.Item key={index} onClick={() => onSelect(item)}>
                      {item}
                    </Menu.Item>
                  ))}
                </Menu>
              }
            >
              <Clickable className="price_input">
                <div className="input-unit-box">
                  <div className="nbng-icon">
                    <img
                      // src={selectedCurrency === CURRENCIES.WETH ? Images.BLUE_ETH_ICON : Images.WHITE_ETH_ICON}
                      src={Images[selectedCurrency]}
                      alt=""
                    />
                  </div>
                  <Typography poppins bold size="huge">
                    {selectedCurrency}
                  </Typography>
                  <img src={Images.COLOR_DOWN_ICON} alt="" className="arrow-icon" />
                </div>
              </Clickable>
            </Dropdown>
            <div className="_input-outter">
              <Field
                className="input-box bold"
                type="number"
                name="offerPrice"
                maxLength={9}
                placeholder={0}
                simple
                max={MAX_INPUT_VALUE}
                min={0}
                component={InputNumber}
              />
              {renderNumberBox(values.offerPrice)}
            </div>
          </div>

          {errors.offerPrice && errorException.includes(errors.offerPrice) && (
            <Typography className="error-text" size="small" primary>
              {t(`validation_messages:${errors.offerPrice}`, {
                x: errorsValue,
              })}
            </Typography>
          )}

          {errors.offerPrice && !errorException.includes(errors.offerPrice) && (
            <Typography className="error-text" size="small" primary>
              {t(`validation_messages:${errors.offerPrice}`)}
            </Typography>
          )}
        </div>
      </div>
    </ProductInputAmountStyled>
  );
}

export default inject(stores => ({
  authStore: stores.auth,
  productsStore: stores.products,
}))(
  observer(({ authStore, productsStore, ...props }) => (
    <ProductInputAmount {...props} authStore={authStore} productsStore={productsStore} />
  )),
);
