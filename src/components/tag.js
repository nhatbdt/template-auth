import styled from 'styled-components';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import { Colors, Images } from '../theme';
import Media from '../utils/media';

const StyledButton = styled.button`
  border: none;
  // background-color: ${Colors.PRIMARY};
  border-radius: 4px;
  color: white;
  font-size: 10px;
  height: 20px;
  padding: 0 18px;
  cursor: pointer;
  transition: opacity 0.2s;
  outline: none;
  display: flex;
  align-items: center;

  &:hover {
    opacity: 0.8;
  }

  .left-box {
    width: 20px;
    height: 100%;
    margin-left: 18px;
    background-color: #b7b7b7;
    display: flex;
    justify-content: center;
    align-items: center;

    img {
      width: 14px;
    }
  }

  &.showIcon {
    padding-right: 0;
    overflow: hidden;
  }

  &.active {
    .left-box {
      background-color: #3ec8e0;
    }
  }

  &.light {
    color: #9b9b9b;
  }

  &.big {
    height: 24px;
    padding: 0 20px;
    font-size: 12px;

    &.showIcon {
      padding-right: 0;
    }

    .left-box {
      width: 28px;

      img {
        width: 16px;
      }
    }
  }

  ${Media.lessThan(Media.SIZE.XXL)} {
    padding: 0 8px;

    .left-box {
      margin-left: 8px;
    }

    &.big {
      padding: 0 12px;

      .left-box {
        margin-left: 12px;
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    font-size: 9px;
    height: 19px;
    padding: 0 4px;

    .left-box {
      width: 18px;
      margin-left: 4px;

      img {
        width: 12px;
      }
    }

    &.big {
      padding: 0 5px;

      .left-box {
        width: 20px;
        margin-left: 5px;

        img {
          width: 15px;
        }
      }
    }
  }
`;

const Tag = ({ children, showIcon, active, background, light, size, ...props }) => (
  <StyledButton
    {...props}
    className={classnames({ showIcon, active, light }, size, 'tag')}
    style={{ backgroundColor: background || Colors.PRIMARY }}
  >
    {children}
    {showIcon && (
      <div className="left-box">
        <img src={active ? Images.WHITE_CIRCLE_TICK_ICON : Images.GRAY_CIRCLE_TICK_ICON} alt="" />
      </div>
    )}
  </StyledButton>
);
Tag.propTypes = {
  showIcon: PropTypes.bool,
  active: PropTypes.bool,
  background: PropTypes.string,
  light: PropTypes.bool,
  size: PropTypes.oneOfType(['big']),
};

export default Tag;
