import styled from 'styled-components';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import Format from '../utils/format';
import Media from '../utils/media';
import Clickable from './clickable';
import Typography from './typography';

const Collection = styled.div`
  border-radius: 6px;
  height: 100%;
  .collection-box {
    border-radius: 6px;
    height: 100%;
    display: flex;
    flex-direction: column;
  }
  .collection-item-image {
    position: relative;
    border-radius: 6px 6px 0 0;
    overflow: hidden;
    .image-box {
      position: relative;
      padding-top: 46%;
      img {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        object-fit: cover;
        border-radius: 6px 6px 0 0;
      }
    }
  }
  .collection-content {
    padding: 24px 16px;
    ${Media.lessThan(Media.SIZE.MD)} {
      padding: 14px 16px;
      display: flex;
      flex-wrap: wrap;
    }
    .collection-item-name {
      flex-direction: column;
      align-items: center;
      justify-content: center;
      width: 100%;
      text-align: center;
      ${Media.lessThan(Media.SIZE.MD)} {
        width: 60%;
      }
      .collection-name {
        color: inherit;
        font-size: 17px;
        font-weight: bold;
        text-align: center;
        overflow: hidden;
        display: -webkit-box;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
      }
      .creator-name {
        color: inherit;
        font-size: 11px;
        font-weight: normal;
        opacity: 0.5;
        text-align: center;
        overflow: hidden;
        display: -webkit-box;
        -webkit-line-clamp: 2;
        -webkit-box-orient: vertical;
      }
      ${Media.lessThan(Media.SIZE.MD)} {
        .collection-name {
          font-size: 12px;
          text-align: left;
        }
        .creator-name {
          font-size: 10px;
          text-align: left;
        }
      }
    }
    .collection-statistical {
      padding: 16px 0;
      ${Media.lessThan(Media.SIZE.XL)} {
        padding: 16px 0;
      }
      ${Media.lessThan(Media.SIZE.MD)} {
        padding: 0;
        width: 40%;
      }
      ul {
        display: flex;
        list-style: none;
        padding: 0;
        margin: 0;
        width: 100%;
        color: inherit;
        justify-content: center;
        li {
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
          text-align: center;
          font-size: 12px;
          opacity: 0.5;
          padding: 0 16px;
          ${Media.lessThan(Media.SIZE.MD)} {
            font-size: 10px;
            padding: 0 5px;
          }
        }
        li + li {
          border-left-width: 1px;
          border-left-style: solid;
        }
      }
    }
    .collection-description {
      font-size: 12px;
      overflow: hidden;
      display: -webkit-box;
      -webkit-line-clamp: 2;
      -webkit-box-orient: vertical;
      ${Media.lessThan(Media.SIZE.MD)} {
        font-size: 10px;
        margin-top: 14px;
      }
    }
  }
`;

const CollectionCard = ({ item, nickname, className, ...props }) => {
  const { t } = useTranslation('common');
  const history = useHistory();

  return (
    <Collection {...props}>
      {item?.link ? (
        <a
          href={item?.link}
          className="collection-box"
          style={{
            backgroundColor: item.backgroundColor || '#FFF2F8',
            color: item.itemColor || '#D04786',
          }}
        >
          <div className="collection-item-image">
            <div className="image-box">
              <img src={item.header} alt={item.name} />
            </div>
          </div>
          <div className="collection-content">
            <div className="collection-item-name">
              <Typography className="collection-name bold">{item.name}</Typography>
              <Typography className="creator-name">{item.idolName}</Typography>
            </div>
            <div className="collection-statistical">
              <ul>
                <li>
                  <span>{Format.price(item.itemCount)}</span>
                  {item.itemCount > 1 ? <span>{t('creator.items')}</span> : <span>{t('creator.item')}</span>}
                </li>
                <li>
                  <span>{Format.price(item.viewCount)}</span>
                  {item.viewCount > 1 ? <span>{t('creator.views')}</span> : <span>{t('creator.view')}</span>}
                </li>
              </ul>
            </div>
            <div className="collection-description">{item.intro}</div>
          </div>
        </a>
      ) : (
        <Clickable
          onClick={() => history.push(`/cultures/${item?.cultureNickname}/collection/${item?.id}/none/none/none`)}
          className="collection-box"
          style={{
            backgroundColor: item.backgroundColor || '#FFF2F8',
            color: item.itemColor || '#D04786',
          }}
        >
          <div className="collection-item-image">
            <div className="image-box">
              <img src={item.header} alt={item.name} />
            </div>
          </div>
          <div className="collection-content">
            <div className="collection-item-name">
              <Typography className="collection-name">{item.name}</Typography>
              <Typography className="creator-name">{item.idolName}</Typography>
            </div>
            <div className="collection-statistical">
              <ul>
                <li>
                  <span>{Format.price(item.itemCount)}</span>
                  {item.itemCount > 1 ? <span>{t('creator.items')}</span> : <span>{t('creator.item')}</span>}
                </li>
                <li>
                  <span>{Format.price(item.viewCount)}</span>
                  {item.viewCount > 1 ? <span>{t('creator.views')}</span> : <span>{t('creator.view')}</span>}
                </li>
              </ul>
            </div>
            <div className="collection-description">{item.intro}</div>
          </div>
        </Clickable>
      )}
    </Collection>
  );
};
CollectionCard.propTypes = {
  onClick: PropTypes.func,
  item: PropTypes.object,
  nickname: PropTypes.string,
  className: PropTypes.string,
};

export default CollectionCard;
