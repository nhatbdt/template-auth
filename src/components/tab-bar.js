import PropTypes from 'prop-types';
import styled from 'styled-components';
import classnames from 'classnames';

import Clickable from './clickable';
import Typography from './typography';
import Media from '../utils/media';
import { Colors } from '../theme';

const StyledDiv = styled.div`
  padding: 0 20px;
  display: flex;
  position: relative;
  border: none;
  overflow-x: auto;
  overflow-y: clip;
  &:after {
    content: '';
    position: absolute;
    left: 0;
    right: 0;
    bottom: 1px;
    height: 1px;
    width: 100%;
    z-index: 0;
    transition: background-color 0.2s;
    background-color: rgba(255, 255, 255, 0.2);
  }

  .tab-item {
    display: flex;
    align-items: center;
    justify-content: center;
    transition: background-color 0.3s, border-bottom-color 0.2s;
    /* opacity: 0.3; */
    position: relative;
    height: 36px;
    padding: 0 5px;
    background-color: #1b1e24;
    border-radius: 9999px;

    .typography {
      /* font-family: 'Noto Sans JP'; */
      font-style: normal;
      font-weight: 500;
      font-size: 14px;
      white-space: nowrap;
      color: ${Colors.TEXT};
    }

    &:after {
      content: '';
      position: absolute;
      left: 0;
      right: 0;
      bottom: 0px;
      height: 2px;
      border-radius: 2px;
      transition: background-color 0.2s;
      opacity: 0;
      z-index: 2;
    }

    .name {
      position: relative;
      .number {
        position: absolute;
        top: 0;
        right: 0;
        background-color: #3cc8fc;
        width: 20px;
        height: 20px;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 50%;

        .number-text {
          color: #222;
          font-size: 10px;
          font-weight: bold;
        }
      }
    }

    &.active {
      /* opacity: 1; */
      background-color: #00153c;

      .typography {
        /* color: #3CC8FC; */
        color: #045afc;
      }

      /* &::after {
        opacity: 1;
        background-color: #3CC8FC;
      } */

      .name {
        .number {
          .number-text {
            color: #222 !important;
          }
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .tab-item {
      flex: 1;
      padding: 0;

      .typography {
        font-size: 12px;
      }
    }
  }

  ${Media.lessThan(Media.SIZE.XS)} {
    padding: 0;
    &.mypage-tab {
      .tab-item {
        .typography {
          font-size: 11px;
        }
      }
    }
  }
  ${Media.lessThan(Media.SIZE.XXXS)} {
    &.mypage-tab {
      .tab-item {
        .typography {
          font-size: 9px;
        }
      }
    }
  }

  &.tab-yellow {
    &:after {
      display: none;
    }
    .tab-item {
      .typography {
        padding: 11px 18px;
      }
    }
  }

  &.tab-blue {
    padding: 0;
    display: flex;
    &:after {
      background-color: ${Colors.GRAY_10};
    }
    .tab-item {
      .name {
        .number {
          background-color: ${Colors.BLUE_1};
          .number-text {
            color: #ffffff !important;
          }
        }
      }
      .typography {
        color: ${Colors.GRAY_6};
        font-size: 14px;
        letter-spacing: 0;
        padding: 11px 18px;
        font-weight: bold;
        text-align: center;
      }
      &.active {
        opacity: 1;

        .typography {
          color: ${Colors.BLACK_5};
        }

        &::after {
          opacity: 1;
          background-color: ${Colors.BLUE_1};
        }
      }
    }

    ${Media.lessThan(Media.SIZE.MD)} {
      .tab-item {
        flex: 1;
        padding: 0;

        .typography {
          font-size: 12px;
        }
      }
    }

    ${Media.lessThan(Media.SIZE.XS)} {
      padding: 0;

      &.mypage-tab {
        .tab-item {
          .typography {
            font-size: 11px;
          }
        }
      }
    }

    ${Media.lessThan(Media.SIZE.XXXS)} {
      &.mypage-tab {
        .tab-item {
          .typography {
            font-size: 9px;
          }
        }
      }
    }
  }

  &.tab-blue-mypage {
    padding: 0;
    display: flex;
    &:after {
      background-color: ${Colors.GRAY_10};
    }
    .tab-item {
      opacity: 0.6;
      .name {
        .number {
          background-color: ${Colors.BLUE_1};
          .number-text {
            color: #ffffff !important;
          }
        }
      }
      .typography {
        color: #9aa8bd;
        font-size: 14px;
        letter-spacing: 0;
        padding: 11px 18px;
        font-weight: bold;
        text-align: center;
      }
      &.active {
        opacity: 1;

        .typography {
          color: ${Colors.BLUE_1};
        }

        &::after {
          opacity: 1;
          background-color: ${Colors.BLUE_1};
        }
      }
    }

    ${Media.lessThan(Media.SIZE.MD)} {
      .tab-item {
        flex: 1;
        padding: 0;

        .typography {
          font-size: 12px;
        }
      }
    }

    ${Media.lessThan(Media.SIZE.XS)} {
      padding: 0;

      &.mypage-tab {
        .tab-item {
          .typography {
            font-size: 11px;
          }
        }
      }
    }

    ${Media.lessThan(Media.SIZE.XXXS)} {
      &.mypage-tab {
        .tab-item {
          .typography {
            font-size: 9px;
          }
        }
      }
    }
  }

  &.is-dark {
    padding: 0;
    &:after {
      background-color: rgba(255, 255, 255, 0.2);
      bottom: 0px;
    }
    .tab-item {
      opacity: 1;
      ${Media.lessThan(Media.SIZE.MD)} {
        font-size: 11px;
        flex: 0;
        padding: 0 15px;
        height: 38px;
      }
      &:after {
        border-radius: 0;
      }
      .typography {
        font-size: 14px;
        color: rgba(255, 255, 255, 0.4);
        text-transform: uppercase;
        ${Media.lessThan(Media.SIZE.MD)} {
          font-size: 11px;
        }
      }
      &.active {
        .typography {
          font-weight: bold;
          color: rgba(255, 255, 255, 1);
        }
        &:after {
          background: #ffffff;
        }
      }
    }
  }

  &.is-light {
    padding: 0;
    &:after {
      background-color: rgba(0, 0, 0, 0.1);
    }
    .tab-item {
      opacity: 1;
      ${Media.lessThan(Media.SIZE.MD)} {
        font-size: 11px;
        flex: 0;
        padding: 0 15px;
        height: 38px;
      }
      &:after {
        border-radius: 0;
      }
      .typography {
        font-size: 14px;
        color: rgba(34, 34, 34, 0.8);
        text-transform: uppercase;
        ${Media.lessThan(Media.SIZE.MD)} {
          font-size: 11px;
        }
      }
      &.active {
        .typography {
          font-weight: bold;
          color: rgba(34, 34, 34, 1);
        }
        &:after {
          background: #d89deb;
        }
      }
    }
  }
  &.fighter {
    .tab-item {
      &.active {
        &:after {
          background: #bf0000;
        }
      }
    }
  }
`;

const TabBar = ({ items, onChange, activeTabKey, className }) => (
  <StyledDiv className={`${className} TAB-BAR`}>
    {items.map(item => (
      <Clickable
        key={item.key}
        className={classnames('tab-item', { active: activeTabKey === item.key })}
        onClick={() => onChange(item.key)}
      >
        <div className="name">
          <Typography size="large" bold>
            {item.name}
          </Typography>
          {item.value > 0 && (
            <div className="number">
              <Typography size="large" className="number-text bold">
                {item.value > 100 ? '99+' : item.value}
              </Typography>
            </div>
          )}
        </div>
      </Clickable>
    ))}
  </StyledDiv>
);
TabBar.propTypes = {
  items: PropTypes.array,
  onChange: PropTypes.func,
  activeTabKey: PropTypes.any,
  className: PropTypes.any,
};

export default TabBar;
