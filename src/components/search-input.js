import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import classnames from 'classnames';
import { Menu, Dropdown } from 'antd';
import { withTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';

import { Images, Colors } from '../theme';
import Typography from './typography';
import Clickable from './clickable';
import Input from './input';
import Media from '../utils/media';
import { history } from '../store';

const StyledDiv = styled.div`
  display: flex;
  height: 40px;
  border-radius: 30px;
  overflow: hidden;
  width: 100%;
  max-width: 700px;

  .search-type-box {
    display: flex;
    align-items: center;
    min-width: 160px;
    padding: 0 15px 0 25px;
    justify-content: space-between;
    background: rgba(39, 109, 214, 0.08);

    .typography {
      font-size: 13px;
      color: #2664c1;
    }
  }

  .search-box-divider {
    width: 1px;
    background-color: #343434;
    height: 50px;
    align-self: center;
  }

  .input-wrapper {
    flex: 1;
    display: flex;

    .input {
      flex: 1;
      height: auto;
      border: none;
      color: inherit;
      font-size: 13px;
      background: transparent;
    }

    .search-button {
      width: 50px;
      // background-color: ${Colors.PRIMARY};
      display: flex;
      justify-content: center;
      align-items: center;

      img {
        width: 16px;
      }
    }
  }

  &.large {
    height: 68px;

    .input-wrapper {
      .search-button {
        width: 68px;

        img {
          width: auto;
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    // flex-direction: column;
    // height: auto;

    .search-type-box {
      width: auto;
      justify-content: space-between;
      padding-left: 11px;
      padding-right: 22px;
      height: 40px;

      .typography {
        margin-right: 0;
      }
    }

    .search-box-divider {
      width: 100%;
      height: 1px;
    }

    .input-wrapper {
      flex: auto;
      height: 40px;

      .input {
        padding: 0 11px;
      }

      .search-button {
        width: 40px;

        img {
          width: 14px;
        }
      }
    }

    &.large {
      height: auto;

      .input-wrapper {
        .search-button {
          width: 40px;

          img {
            width: 14px;
          }
        }
      }
    }
  }
`;

@withRouter
@withTranslation('common')
class SearchInput extends Component {
  static propTypes = {
    size: PropTypes.string,
    placeholder: PropTypes.string,
    defaultSearchValue: PropTypes.string,
    defaultSearchType: PropTypes.string,
    onSearch: PropTypes.func,
    onHeaderSearch: PropTypes.func,
    preventChangeSearchType: PropTypes.bool,
    styleType: PropTypes.string,
    t: PropTypes.object,
  };

  constructor(props) {
    super(props);
    const { defaultSearchValue, defaultSearchType } = props;

    this.state = {
      searchType: defaultSearchType || 'idols',
      value: defaultSearchValue || '',
    };
  }

  _onSearch = () => {
    const { match, onSearch, onHeaderSearch } = this.props;
    let { value, searchType } = this.state;
    value = value.trim().replace('%', '');
    const encodeValue = encodeURIComponent(value);

    if (searchType === 'idols') {
      if (match.path.includes('/:lang/idols')) {
        onSearch(value);
      } else if (value) {
        history.push(`/idols/key-word/${encodeValue}`);
      }
    } else if (match.path.includes('/:lang/products')) {
      onSearch(value);
    } else if (value) {
      history.push(`/products/none/none/none/none?query=${encodeValue}`);
    }

    if (onHeaderSearch) onHeaderSearch();
  };

  _onSearchChange = e => {
    this.setState({
      value: e.target.value,
    });
  };

  _onKeyUp = e => {
    if (e.key === 'Enter' || e.keyCode === 13) {
      this._onSearch();
    }
  };

  _onSearchTypeSelected = e => {
    this.setState({
      searchType: e.key,
    });
  };

  render() {
    const { t, size, placeholder, className, preventChangeSearchType, styleType } = this.props;
    const { searchType, value } = this.state;

    const SEARCH_TYPES = [
      {
        name: t('header.av_idols'),
        value: 'idols',
      },
      {
        name: t('header.products'),
        value: 'products',
      },
    ];

    return (
      <StyledDiv className={classnames(size, className, styleType)}>
        {preventChangeSearchType ? null : (
          <Dropdown
            trigger="click"
            overlay={
              <Menu>
                {SEARCH_TYPES.map(item => (
                  <Menu.Item key={item.value} onClick={this._onSearchTypeSelected}>
                    {item.name}
                  </Menu.Item>
                ))}
              </Menu>
            }
          >
            <Clickable className="search-type-box">
              <Typography size="large">
                {searchType === 'idols' ? SEARCH_TYPES[0].name : SEARCH_TYPES[1].name}
              </Typography>
              <img src={Images.GRAY_CHEVRON_DOWN_ICON} alt="icon" />
            </Clickable>
          </Dropdown>
        )}
        {/* <div className="search-box-divider" /> */}
        <div className="input-wrapper">
          <Input value={value} onChange={this._onSearchChange} onKeyUp={this._onKeyUp} placeholder={placeholder} />
          <Clickable className="search-button" onClick={this._onSearch}>
            <img src={styleType === 'gray' ? Images.GRAY_SEARCH_ICON : Images.YELLOW_SEARCH_ICON} alt="icon" />
          </Clickable>
        </div>
      </StyledDiv>
    );
  }
}

export default SearchInput;
