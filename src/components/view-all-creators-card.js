import PropTypes from 'prop-types'
import { useHistory } from 'react-router-dom'
import CreatorCard from './creator-card'

const ViewAllCreatorsCard = ({
  item
}) => {
  const history = useHistory()
  // eslint-disable-next-line no-shadow
  const handleClick = (item) => {
    history.push(`/idol-details/${item.id}`)
  }

  return (
    <CreatorCard item={item} onClick={() => handleClick(item)} />
  )
}
ViewAllCreatorsCard.propTypes = {
  item: PropTypes.object
}

export default ViewAllCreatorsCard
