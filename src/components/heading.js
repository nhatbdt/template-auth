import React from 'react';
import PropTypes from 'prop-types';
import Typography from '../components/typography';
import Media from '../utils/media';
import styled from 'styled-components';

const StyledDiv = styled.div`
  display: flex;
  margin: 40px 0;
  align-items: center;
  flex-wrap: wrap;
  .title-container {
    display: flex;
    align-items: center;
  }
  .event-box {
    display: flex;
    margin-right: 10px;
    justify-content: space-between;
    transform: skew(-20deg);
    margin-top: 5px;
    &.event-box-product {
      .box1,
      .box2 {
        width: 4px;
        height: 16px;
        background-color: #fff235;
      }
      .box1 {
        width: 6px;
      }
      .box2 {
        background-color: #40414a;
      }
    }
    .box1,
    .box2 {
      width: 4px;
      height: 30px;
      background-color: #fff235;
    }
    .box2 {
      background-color: #40414a;
    }
  }
  .section-heading-title {
    margin-right: 29px;
    font-size: 1.625rem;
    font-weight: 500;
    text-transform: uppercase;
    color: rgba(255, 255, 255, 1);
  }
  .section-heading-content {
    color: #909198;
    font-size: 0.875rem;
    margin-top: 8px;
    text-transform: uppercase;
    color: rgba(255, 255, 255, 0.5);
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .section-heading-title {
      margin-right: 12px;
      font-size: 1.125rem;
    }
    .section-heading-content {
      font-size: 0.75rem;
      margin-top: 6px;
    }
  }
`;

const Heading = ({ title, content, className }) => (
  <StyledDiv className={className}>
    <div className="title-container">
      <div className="event-box event-box-product">
        <div className="box1" />
        <div className="box2" />
      </div>
      <h2 className="section-heading-title">{title}</h2>
    </div>
    <Typography className="section-heading-content">{content}</Typography>
  </StyledDiv>
);

Heading.propTypes = {
  title: PropTypes.string,
  content: PropTypes.string,
  className: PropTypes.string,
};

Heading.defaultProps = {
  title: '',
  content: '',
  className: '',
};
export default Heading;
