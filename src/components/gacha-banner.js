import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import numeral from 'numeral';
import Marquee from "react-fast-marquee";
// import { observer, MobXProviderContext } from 'mobx-react';
import { observer } from 'mobx-react';

import { Images } from '../theme';
import Media from '../utils/media';
import Thumbnail from './thumbnail';
import Clickable from './clickable';
import CURRENCIES from '../constants/currencies';
import { getRateByCurrency } from '../utils/rates';
import auth from '../store/auth';

// function useStores() {
//   return React.useContext(MobXProviderContext);
// }

const StyledDiv = styled(Clickable)`
  position: relative;
  display: flex;
  background-color: transparent;
  border-radius: 20px;
  overflow: hidden;
  height: 100%;

  &:hover {
    box-shadow: 0 8px 25px rgba(0, 0, 0, 0.1);
  }
  .product-card-left-box {
    color: inherit !important;
  }

  .product-card-left-box {
    flex: 1;
    display: flex;
    flex-direction: column;
    min-width: 0;

    .content {
      /* font-family: 'Noto Sans JP'; */
      color: #fff;
      height: 100%;
      padding: 12px;
      background: rgba(0, 0, 0, 0.2);
      backdrop-filter: blur(67px);
      /* background: linear-gradient(182.93deg, #781c4d 2.53%, #420c3a 97.76%); */
      background-color: #15171c;

      .usd-value .camelcase-price {
        color: #A8AEBA;
      }

      .marquee-container.effect-run {
        margin-bottom: 10px;

        .child {
          margin-right: 10px;
        }

        .wrapper-run-items {
          display: inline-block;
          text-align: center;
          padding: 5px 10px;
          border-radius: 50px;
          border: 1px solid #fff;
        }

        .label {
          color: #fff;
        }

        .value {
          margin-left: 5px;
        }
      }

      .text-right {
        text-align: right;
      }
      .gacha-title {
        font-weight: 700;
        font-size: 18px;
        line-height: 26px;
      }
      .label {
        font-weight: 500;
        font-size: 16px;
        color: #bbbbbb;
      }
      .value {
        font-weight: 500;
        font-size: 16px;
        line-height: 23px;
      }
      .category-name {
        color: #fff;
        font-style: normal;
        font-weight: 700;
        font-size: 18px;
      }
      .product-infor {
        display: flex;
        justify-content: space-between;
        align-items: flex-start;
        .usd-value {
          font-size: 12px;
        }

        .value {
          color: #045afc;
        }
      }
      .creator-infor {
        flex: 1;
        display: flex;
        justify-content: flex-start;
        align-items: center;
        gap: 10px;
        .creator-avata {
          border-radius: 50%;
        }
      }
    }

    .image-box {
      position: relative;
      width: 100%;
      padding-top: 100%;
      overflow: hidden;
      ${Media.lessThan(Media.SIZE.SM)} {
        padding-top: 50%;
      }

      .product-card-field {
        position: absolute;
        bottom: 13px;
        left: 0;
        right: 0;
        height: 44px;
        z-index: 2;
        display: flex;
        justify-content: center;
        align-items: center;
        .countdown {
          height: 100%;
          width: 158px;
          padding: 10px;
          background-color: rgba(0, 0, 0, 0.3);
          backdrop-filter: blur(10px);
          border-radius: 42px;
          display: flex;
          justify-content: center;
          align-items: center;
          gap: 10px;
          .typography {
            color: #fff;
            /* font-family: 'Noto Sans JP'; */
            font-style: normal;
            font-weight: 700;
            font-size: 16px;
          }
        }
      }

      .image {
        width: 100%;
        height: 100%;
        max-height: 400px;
        position: absolute;
        top: 0px;
        left: 0px;
        background-position: center center;

        &.image-blur {
          filter: blur(15px);
        }
      }
      .item-18 {
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        top: 0;
        .item-18-inside {
          display: flex;
          justify-content: center;
          align-items: center;
          height: 100%;
          flex-direction: column;

          p {
            font-size: 12px;
            padding-top: 15px;
            color: #ffffff !important;
          }
        }
      }
    }
  }

  &.smallInMobile {
    ${Media.lessThan(Media.SIZE.MD)} {
      height: 123px;

      .product-card-left-box {
        display: flex;
        flex-direction: row;

        .image-box {
          width: 123px;
          height: 100%;
          padding-top: 0;
          .item-18 .item-18-inside p {
            font-size: 10px;
          }
        }

        .info-outer-box {
          flex: 1;
          display: flex;
          flex-direction: column;
          border-top-right-radius: 4px;
          overflow: hidden;

          .product-card-info-box {
            padding: 6px 11px 5px 11px;
          }

          ._product-card-price-box {
            padding: 3px 11px 0 0;
            flex: 1;
            justify-content: center;
            display: flex;
            flex-direction: column;
            ._horizontal .category {
              padding-top: 3px;
            }
          }
        }
      }
    }
  }
`;

const GachaBanner = observer(({ item, smallInMobile, styleType, numShowText = 18, ...props }) => {
  const { t } = useTranslation('common');
  const masterData = auth.initialData;

  const currencyFormat = (price, currency) => {
    const isShowPriceSuffix = price - parseInt(price, 10) > 0;
    price = numeral(price).format('0,0.000');

    return (
      <div className="camelcase-price">
        {price.slice(0, price.length - 4)}
        <span className="bold">
          {isShowPriceSuffix ? price.slice(price.length - 4, price.length) : ''}
          &nbsp;
          {currency}
        </span>
      </div>
    );
  };

  const renderRunComponents = Array.isArray(item?.productSummaryDTOS) ? item.productSummaryDTOS.map((item, index) => (
    <div className='wrapper-run-items' key={index}>
      <span className='label'>{t(`common:gacha.${item.gachaRarity}`)}:</span>
      <span className='value'>{item.unusedProduct} {t('common:creator.items')}</span>
    </div>
  )) : []

  return (
    <StyledDiv {...props} className={`${styleType} PRODUCT-CARD-NEW`}>
      <div className="product-card-left-box" style={{ color: item.itemColor || '#cccccc' }}>
        <div className="image-box">
          <Thumbnail className="image" contain={!!item.padding} url={item?.imageUrl || Images.GACHA_MODAL_IMAGE} />
        </div>
        <div className="content">
          <Marquee className='effect-run'>
            {renderRunComponents}
          </Marquee>

          {/* <div className="product-infor">
            {item?.title && (
              <div className="left gacha-title">
                {item.title?.length > 30 ? `${item.title?.slice(0, 30)?.toUpperCase()}...` : item?.title?.toUpperCase()}
              </div>
            )}
          </div> */}
          <div className="product-infor">
            <div className="left">
              <div className="creator-infor">
                <div className="create-name">
                  <p className="label">{t('price')}</p>
                  {/* <p className="value">{`${item?.yenFee} ${CURRENCIES.JPY}`}</p> */}
                  <p className="value">{currencyFormat(item?.yenFee, CURRENCIES.JPY)}</p>
                  <i className="usd-value">
                    {currencyFormat(item?.yenFee / getRateByCurrency(CURRENCIES.USD, masterData), CURRENCIES.USD)}
                  </i>
                </div>
              </div>
            </div>
            <div className="right">
              <p className="label text-right">{t('total_items')}</p>
              <div className="value text-right">{item?.unusedProduct}</div>
            </div>
          </div>
        </div>
      </div>
    </StyledDiv>
  );
});

GachaBanner.propTypes = {
  onClick: PropTypes.func,
  showDescription: PropTypes.bool,
  smallInMobile: PropTypes.bool,
  item: PropTypes.object,
  gachaInfo: PropTypes.object,
  styleType: PropTypes.string,
};
GachaBanner.defaultProps = {
  smallInMobile: false,
};

export default GachaBanner;
