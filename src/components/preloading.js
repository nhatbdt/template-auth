import PropTypes from 'prop-types';
import classnames from 'classnames';
import styled from 'styled-components';
import { Colors } from '../theme';

const PreloadingDiv = styled.div`
  background: ${Colors.BOX_BACKGROUND};
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 99;
  opacity: 1;
  transition: opacity 0.3s;
  svg {
    position: absolute;
    width: 0;
    height: 0;
  }
  .wrap {
    width: 400px;
    height: 900px;
    position: relative;
    -webkit-animation: scrollup 4s linear infinite;
    animation: scrollup 4s linear infinite;
    filter: url('#goo');
    @media only screen and (max-width: 768px) {
      zoom: 0.5;
    }
  }
  @-webkit-keyframes scrollup {
    from {
      transform: scale(0.75) translateY(100px);
    }
    to {
      transform: scale(0.75) translateY(-100px);
    }
  }
  @keyframes scrollup {
    from {
      transform: scale(0.75) translateY(100px);
    }
    to {
      transform: scale(0.75) translateY(-100px);
    }
  }
  .wrap .angle {
    position: absolute;
    width: 200px;
    height: 200px;
    left: calc(50% - 50px);
    filter: contrast(10px);
  }
  .wrap .angle:first-of-type {
    -webkit-animation: rolldown 4s ease-in-out infinite;
    animation: rolldown 4s ease-in-out infinite;
  }
  @-webkit-keyframes rolldown {
    0% {
      transform: rotate(0deg);
    }
    10% {
      transform: rotate(180deg);
    }
    100% {
      transform: rotate(180deg);
    }
  }
  @keyframes rolldown {
    0% {
      transform: rotate(0deg);
    }
    10% {
      transform: rotate(180deg);
    }
    100% {
      transform: rotate(180deg);
    }
  }
  .wrap .angle:first-of-type:before {
    -webkit-animation: rolldown2 4s ease-in-out infinite;
    animation: rolldown2 4s ease-in-out infinite;
    transform-origin: top right;
  }
  @-webkit-keyframes rolldown2 {
    0% {
      transform: rotate(0deg);
    }
    12.5% {
      transform: rotate(0deg);
    }
    25% {
      transform: rotate(180deg);
    }
    100% {
      transform: rotate(180deg);
    }
  }
  @keyframes rolldown2 {
    0% {
      transform: rotate(0deg);
    }
    12.5% {
      transform: rotate(0deg);
    }
    25% {
      transform: rotate(180deg);
    }
    100% {
      transform: rotate(180deg);
    }
  }
  .wrap .angle:nth-of-type(2) {
    -webkit-animation: rolldown3 4s ease-in-out infinite;
    animation: rolldown3 4s ease-in-out infinite;
    transform-origin: left;
  }
  @-webkit-keyframes rolldown3 {
    0% {
      transform: rotate(0deg);
    }
    25% {
      transform: rotate(0deg);
    }
    37.5% {
      transform: rotate(-180deg);
    }
    100% {
      transform: rotate(-180deg);
    }
  }
  @keyframes rolldown3 {
    0% {
      transform: rotate(0deg);
    }
    25% {
      transform: rotate(0deg);
    }
    37.5% {
      transform: rotate(-180deg);
    }
    100% {
      transform: rotate(-180deg);
    }
  }
  .wrap .angle:nth-of-type(2):before {
    -webkit-animation: rolldown4 4s ease-in-out infinite;
    animation: rolldown4 4s ease-in-out infinite;
    transform-origin: top left;
  }
  @-webkit-keyframes rolldown4 {
    0% {
      transform: rotate(0deg);
    }
    37.5% {
      transform: rotate(0deg);
    }
    50% {
      transform: rotate(-180deg);
    }
    100% {
      transform: rotate(-180deg);
    }
  }
  @keyframes rolldown4 {
    0% {
      transform: rotate(0deg);
    }
    37.5% {
      transform: rotate(0deg);
    }
    50% {
      transform: rotate(-180deg);
    }
    100% {
      transform: rotate(-180deg);
    }
  }
  .wrap .angle:nth-of-type(3) {
    -webkit-animation: rolldown5 4s ease-in-out infinite;
    animation: rolldown5 4s ease-in-out infinite;
  }
  @-webkit-keyframes rolldown5 {
    0% {
      transform: rotate(0deg);
    }
    50% {
      transform: rotate(0deg);
    }
    62.5% {
      transform: rotate(180deg);
    }
    100% {
      transform: rotate(180deg);
    }
  }
  @keyframes rolldown5 {
    0% {
      transform: rotate(0deg);
    }
    50% {
      transform: rotate(0deg);
    }
    62.5% {
      transform: rotate(180deg);
    }
    100% {
      transform: rotate(180deg);
    }
  }
  .wrap .angle:nth-of-type(3):before {
    -webkit-animation: rolldown6 4s ease-in-out infinite;
    animation: rolldown6 4s ease-in-out infinite;
    transform-origin: top right;
  }
  @-webkit-keyframes rolldown6 {
    0% {
      transform: rotate(0deg);
    }
    62.5% {
      transform: rotate(0deg);
    }
    75% {
      transform: rotate(180deg);
    }
    100% {
      transform: rotate(180deg);
    }
  }
  @keyframes rolldown6 {
    0% {
      transform: rotate(0deg);
    }
    62.5% {
      transform: rotate(0deg);
    }
    75% {
      transform: rotate(180deg);
    }
    100% {
      transform: rotate(180deg);
    }
  }
  .wrap .angle:nth-of-type(4) {
    -webkit-animation: rolldown7 4s ease-in-out infinite;
    animation: rolldown7 4s ease-in-out infinite;
    transform-origin: left;
  }
  @-webkit-keyframes rolldown7 {
    0% {
      transform: rotate(0deg);
    }
    75% {
      transform: rotate(0deg);
    }
    87.5% {
      transform: rotate(-180deg);
    }
    100% {
      transform: rotate(-180deg);
    }
  }
  @keyframes rolldown7 {
    0% {
      transform: rotate(0deg);
    }
    75% {
      transform: rotate(0deg);
    }
    87.5% {
      transform: rotate(-180deg);
    }
    100% {
      transform: rotate(-180deg);
    }
  }
  .wrap .angle:nth-of-type(4):before {
    -webkit-animation: rolldown8 4s ease-in-out infinite;
    animation: rolldown8 4s ease-in-out infinite;
    transform-origin: top left;
  }
  @-webkit-keyframes rolldown8 {
    0% {
      transform: rotate(0deg);
    }
    87.5% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(-180deg);
    }
  }
  @keyframes rolldown8 {
    0% {
      transform: rotate(0deg);
    }
    87.5% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(-180deg);
    }
  }
  .wrap .angle:nth-of-type(1) {
    top: calc((50% - 250px) + (0 * 50px));
  }
  .wrap .angle:nth-of-type(1):before {
    content: '';
    position: absolute;
    width: 50%;
    height: 50%;
    background: linear-gradient(to right, #276fda, #254e8c 150%);
  }
  .wrap .angle:nth-of-type(even):before {
    -webkit-clip-path: polygon(0 0, 0% 100%, 100% 50%);
    clip-path: polygon(0 0, 0% 100%, 100% 50%);
  }
  .wrap .angle:nth-of-type(odd):before {
    -webkit-clip-path: polygon(100% 0, 100% 100%, 0 50%);
    clip-path: polygon(100% 0, 100% 100%, 0 50%);
  }
  .wrap .angle:nth-of-type(2) {
    top: calc((50% - 250px) + (1 * 50px));
  }
  .wrap .angle:nth-of-type(2):before {
    content: '';
    position: absolute;
    width: 50%;
    height: 50%;
    background: linear-gradient(to right, #276fda, #254e8c 150%);
  }
  .wrap .angle:nth-of-type(even):before {
    -webkit-clip-path: polygon(0 0, 0% 100%, 100% 50%);
    clip-path: polygon(0 0, 0% 100%, 100% 50%);
  }
  .wrap .angle:nth-of-type(odd):before {
    -webkit-clip-path: polygon(100% 0, 100% 100%, 0 50%);
    clip-path: polygon(100% 0, 100% 100%, 0 50%);
  }
  .wrap .angle:nth-of-type(3) {
    top: calc((50% - 250px) + (2 * 50px));
  }
  .wrap .angle:nth-of-type(3):before {
    content: '';
    position: absolute;
    width: 50%;
    height: 50%;
    background: linear-gradient(to right, #276fda, #254e8c 150%);
  }
  .wrap .angle:nth-of-type(even):before {
    -webkit-clip-path: polygon(0 0, 0% 100%, 100% 50%);
    clip-path: polygon(0 0, 0% 100%, 100% 50%);
  }
  .wrap .angle:nth-of-type(odd):before {
    -webkit-clip-path: polygon(100% 0, 100% 100%, 0 50%);
    clip-path: polygon(100% 0, 100% 100%, 0 50%);
  }
  .wrap .angle:nth-of-type(4) {
    top: calc((50% - 250px) + (3 * 50px));
  }
  .wrap .angle:nth-of-type(4):before {
    content: '';
    position: absolute;
    width: 50%;
    height: 50%;
    background: linear-gradient(to right, #276fda, #254e8c 150%);
  }
  .wrap .angle:nth-of-type(even):before {
    -webkit-clip-path: polygon(0 0, 0% 100%, 100% 50%);
    clip-path: polygon(0 0, 0% 100%, 100% 50%);
  }
  .wrap .angle:nth-of-type(odd):before {
    -webkit-clip-path: polygon(100% 0, 100% 100%, 0 50%);
    clip-path: polygon(100% 0, 100% 100%, 0 50%);
  }
`;
const Preloading = ({ className }) => (
  <PreloadingDiv className={classnames(className, 'preloading-custom')}>
    <div className="wrap">
      <div className="angle" />
      <div className="angle" />
      <div className="angle" />
      <div className="angle" />
    </div>
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg">
      <defs>
        <filter id="goo">
          <fegaussianblur in="SourceGraphic" result="blur" stddeviation="12" />
          <fecolormatrix in="blur" mode="matrix" result="goo" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" />
          <fecomposite in2="goo" in="SourceGraphic" operator="atop" />
        </filter>
      </defs>
    </svg>
  </PreloadingDiv>
);

Preloading.propTypes = {
  className: PropTypes.string,
};

export default Preloading;
