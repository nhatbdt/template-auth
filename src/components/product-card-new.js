import React, { useState } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import classnames from 'classnames';
import { useTranslation } from 'react-i18next';
// import numeral from 'numeral';
import { observer, MobXProviderContext } from 'mobx-react';
import moment from 'moment';

import { Colors, Images } from '../theme';
import Media from '../utils/media';
import Format from '../utils/format';
import Misc from '../utils/misc';
import Thumbnail from './thumbnail';
import Clickable from './clickable';
import Typography from './typography';
import StatusTag from './status-tag';
import { getStatusText } from '../utils/products';
import { getRateByCurrency } from '../utils/rates';
import CURRENCIES from '../constants/currencies';
import CountdownComponent from './countdown';
import { LENDING_STATUS } from '../constants/statuses';

function useStores() {
  return React.useContext(MobXProviderContext);
}

const StyledDiv = styled(Clickable)`
  flex: 1;
  /* min-width: 312px;
  max-width: 412px; */
  padding: 15px;
  position: relative;
  display: flex;
  border-radius: 20px;
  overflow: hidden;
  background: #15171c;
  box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);

  &:hover {
    box-shadow: 0 8px 25px rgba(0, 0, 0, 0.1);
  }
  .product-card-left-box {
    color: inherit !important;
  }

  .item-name {
    font-weight: 700;
    font-size: 16px;
    line-height: 23px;
    color: #fff;
    overflow: hidden;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    ${Media.lessThan(Media.SIZE.XXL)} {
      font-size: 15px;
    }
    ${Media.lessThan(Media.SIZE.MD)} {
      font-size: 10px;
    }
    &.time-title {
      color: #65676b;
      font-size: 12px;
      line-height: 16px;
    }
    &.item-creator {
      font-size: 12px;
      color: #65676b !important;
    }
  }
  .time {
    height: 18px;
    font-size: 12px;
    color: #65676b !important;
  }

  .item-artist-name {
    font-size: 11px;
    opacity: 0.5;
    ${Media.lessThan(Media.SIZE.MD)} {
      font-size: 10px;
    }
  }

  .price-jpy {
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;
    color: ${Colors.TEXT} !important;
  }

  .product-card-left-box {
    flex: 1;
    display: flex;
    flex-direction: column;
    min-width: 0;

    .image-box {
      position: relative;
      width: 100%;
      padding-top: 100%;
      overflow: hidden;
      border-radius: 20px;

      .product-card-field {
        position: absolute;
        top: 15px;
        left: 15px;
        z-index: 2;
        display: flex;
        justify-content: flex-end;
        align-items: center;
        .typography {
          font-size: 12px;
          color: #65676b !important;
          ${Media.lessThan(Media.SIZE.MD)} {
            font-size: 10px;
          }
        }
        &.start {
          .star-icon {
            margin-right: 5px;
            color: #32fa5e;
          }
          .typography {
            font-size: 12px;
            color: #32fa5e !important;
            ${Media.lessThan(Media.SIZE.MD)} {
              font-size: 10px;
            }
          }
        }
      }
      .lending-timer {
        position: absolute;
        bottom: 13px;
        left: 0;
        right: 0;
        height: 44px;
        z-index: 2;
        display: flex;
        justify-content: center;
        align-items: center;
        .timer {
          height: 100%;
          width: 158px;
          padding: 10px;
          background: #001d49;
          backdrop-filter: blur(10px);
          border-radius: 42px;
          display: flex;
          justify-content: center;
          align-items: center;
          gap: 7px;
          .typography {
            color: #045afc;
            font-style: normal;
            font-weight: 700;
            font-size: 16px;
          }

          .label-expiration {
            color: #045afc;
            font-size: 16px;
          }
          
          .countdown-card {
            width: auto;
          }

          .customer-countdown {
            .box-time-card {
              font-size: 16px;
              line-height: 1.2;
              color: #045afc;
            }
          }
        }
      }

      .image {
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0px;
        left: 0px;
        background-position: center center;

        &.image-blur {
          filter: blur(15px);
        }
      }
      .item-18 {
        position: absolute;
        left: 0;
        right: 0;
        bottom: 0;
        top: 0;
        .item-18-inside {
          display: flex;
          justify-content: center;
          align-items: center;
          height: 100%;
          flex-direction: column;

          p {
            font-size: 12px;
            padding-top: 15px;
            color: #ffffff !important;
          }
        }
      }
    }

    .info-outer-box {
      height: 100%;
      /* background: linear-gradient(117.54deg, rgba(255, 255, 255, 0.5) -19.85%, rgba(235, 235, 235, 0.367354) 4.2%, rgba(224, 224, 224, 0.287504) 13.88%, rgba(212, 212, 212, 0.21131) 27.98%, rgba(207, 207, 207, 0.175584) 37.8%, rgba(202, 202, 202, 0.143432) 44.38%, rgba(200, 200, 200, 0.126299) 50.54%, rgba(196, 196, 196, 0.1) 60.21%); */
      /* box-shadow: 0px 1px 24px -1px rgba(0, 0, 0, 0.18); */
      backdrop-filter: blur(24px);

      border-radius: 6px;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
      .product-card-info-box {
        background-color: #2b2c3c;
        padding: 10px;

        ._product-card-description {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
          font-size: 11px;
          opacity: 0.5;
          ${Media.lessThan(Media.SIZE.MD)} {
            font-size: 10px;
          }
        }

        ._product-card-name {
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
          font-size: 12px;
          line-height: 16px;
        }
      }

      ._product-card-price-box {
        display: flex;
        flex-direction: column;
        height: 100%;
        justify-content: space-between;

        .product-header {
          display: flex;
          justify-content: space-between;
          margin-bottom: 16px;
          align-items: center;

          .product-name {
            font-weight: 700;
            font-size: 16px;
            color: #fff;
          }

          .currency-icon {
            width: 26px;
            height: 26px;
          }
        }
        ._status-text {
          font-size: 18px !important;
          font-weight: bold;
          color: #cccccc;
          padding-top: 5px;
          display: flex;
          align-items: center;
          .text-light {
            font-weight: 700;
            font-size: 14px;
            line-height: 20px;
            color: #045afc;
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            width: 95%;
            max-width: 400px;
          }
          .text-dark {
            color: #cccccc !important;
          }
          ._dot {
            display: flex;
            width: 9px;
            height: 9px;
            border-radius: 50%;
            margin-right: 6px;
            background-color: #db1d31;
          }
          &.holding ._dot {
            background-color: #d2c436;
          }

          &.before-auction ._dot {
            background-color: #fff1ab;
          }

          &.during-auction ._dot {
            background-color: #ffe310;
          }

          &.comming-soon ._dot {
            background-color: #a8a8a8;
          }

          &.sale ._dot {
            background-color: #2fc42f;
          }

          &.resale ._dot {
            background-color: #1b8beb;
          }
        }

        ._horizontal {
          display: flex;
          justify-content: space-between;
          align-items: flex-start;
          ${Media.lessThan(Media.SIZE.MD)} {
            margin-bottom: 4px;
            padding-left: 0px;
          }
          &:last-child {
            margin-bottom: 0;
          }

          .category {
            font-size: 12px;
            padding-top: 5px;
          }

          ._left {
            display: flex;
            justify-content: flex-start;
            align-items: center;
            margin-top: 15px;

            .lender {
              margin-left: 10px;

              .label {
                font-weight: 500;
                font-size: 14px;
                line-height: 20px;
                color: ${Colors.TEXT};
              }
              .lender-name {
                font-weight: 500;
                font-size: 14px;
                line-height: 20px;
                color: #045afc;
              }
            }
            .product-status {
              display: flex;
              min-width: 86px;
              min-height: 24px;
              border-radius: 9999px;
              border: none;
              > * {
                margin: auto;
              }
            }
          }

          ._price {
            font-size: 20px;
            font-weight: bold;
            padding-left: 10px;
            ${Media.lessThan(Media.SIZE.MD)} {
              width: 50%;
              padding-left: 5px;
              display: flex;
              flex-direction: column;
              align-items: flex-end;
            }
            .price-label {
              font-weight: 500;
              font-size: 14px;
              line-height: 20px;
              text-align: right;
              color: ${Colors.TEXT};
            }
            ._price-amount {
              font-weight: 500;
              font-size: 16px;
              line-height: 23px;
              text-align: right;
              color: #045afc;
              &._price-amount-custom {
                font-size: 16px;
              }
              /* span {
                font-size: 12px;
                margin-left: 3px;
                font-weight: bold;
              } */
              /* ${Media.lessThan(Media.SIZE.XXL)} {
                font-size: 15px;
              } */
              ${Media.lessThan(Media.SIZE.MD)} {
                font-size: 10px;
                line-height: 14px;
                text-align: right;
                /* span {
                  line-height: 14px;
                  font-size: 10px;
                } */
              }
            }
          }

          ._product-card-field-group {
            display: flex;
            .product-card-field {
              margin-right: 23px;

              &:last-child {
                margin-right: 0;
              }
            }
          }

          .product-card-field {
            display: flex;
            justify-content: flex-end;
            .typography {
              font-size: 12px;
              color: #65676b !important;
              ${Media.lessThan(Media.SIZE.MD)} {
                font-size: 10px;
              }
            }
            &.start {
              .star-icon {
                margin-right: 5px;
                color: #32fa5e;
              }
              .typography {
                font-size: 12px;
                color: #32fa5e !important;
                ${Media.lessThan(Media.SIZE.MD)} {
                  font-size: 10px;
                }
              }
            }
          }

          &._product-card {
            ._product-card-field-group {
              justify-content: space-between;
              flex: 1;
              align-items: center;
              .product-card-field {
                align-items: baseline;
                .offer {
                  color: #9899a1;
                  font-size: 12px;
                  // padding-left: 5px;
                }
              }
            }
          }

          ._item-offer {
            > p {
              padding-right: 5px;
            }
            ._tag-box {
              background-color: rgb(254, 192, 2);
              width: 70px;
              height: 22px;
              border-radius: 5px;
              display: flex;
              -webkit-box-pack: center;
              justify-content: center;
              -webkit-box-align: center;
              align-items: center;
              color: #ffffff;
            }

            .camelcase-price {
              font-size: 14px;
              span {
                font-size: 11px;
                font-weight: bold;
              }
            }
          }
        }
      }
    }
  }

  .product-card-right-box {
    margin-left: 15px;
    flex: 1;

    .typography {
      white-space: pre-wrap;
      word-break: break-word;
      text-align: justify;
    }
  }

  .number-ranking {
    position: absolute;
    left: 30px;
    top: 30px;
    background: rgba(0, 0, 0, 0.3);
    backdrop-filter: blur(10px);
    width: 40px;
    height: 40px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 999px;

    p {
      color: aqua;
      font-size: 26px;
      font-weight: bold;
      margin-top: -2px;
      margin-left: -2px;
    }
  }

  &.smallInMobile {
    ${Media.lessThan(Media.SIZE.MD)} {
      height: 123px;

      .product-card-left-box {
        display: flex;
        flex-direction: row;

        .image-box {
          width: 123px;
          height: 100%;
          padding-top: 0;
          .item-18 .item-18-inside p {
            font-size: 10px;
          }
        }

        .info-outer-box {
          flex: 1;
          display: flex;
          flex-direction: column;
          border-top-right-radius: 4px;
          overflow: hidden;

          .product-card-info-box {
            padding: 6px 11px 5px 11px;
          }

          ._product-card-price-box {
            flex: 1;
            justify-content: center;
            display: flex;
            flex-direction: column;
            ._horizontal .category {
              padding-top: 3px;
            }
          }
        }
      }
    }
  }
`;

const ProductCard = observer(
  ({
    item,
    showDescription,
    smallInMobile,
    index = 0,
    showNumberRanking = false,
    isShowFavoriteIcon = true,
    styleType,
    isShowCategoryName = false,
    isShowEndTime = false,
    isShowLending = false,
    isShowStatus = false,
    numShowText = 30,
    ...props
  }) => {
    const { i18n } = useTranslation();
    const { auth } = useStores();
    const [isExpiration, setIsExpiration] = useState(item.lendingStatus === LENDING_STATUS.EXPIRATION);
    // const { usdToJpy } = auth.initialData;
    const masterData = auth.initialData;

    const { t } = useTranslation('common');
    const price = item?.productBid?.biggestBidPrice || item.price;
    const yenPrice = item?.productBid?.biggestBidPriceYen || item.yenPrice;
    const statusText = getStatusText(item, t);

    const isStatusLending = item?.lendingTitle === 'LENDING'

    // const _renderCamelCasePrice = (price, currency) => {
    //   const isShowPriceSuffix = price - parseInt(price, 10) > 0;
    //   price = numeral(price).format('0,0.000');

    //   return (
    //     <Typography className="camelcase-price">
    //       <span className="bold">{price.slice(0, price.length - 4)}</span>
    //       <span className="bold">
    //         {isShowPriceSuffix ? price.slice(price.length - 4, price.length) : ''}
    //         &nbsp;
    //         {currency}
    //       </span>
    //     </Typography>
    //   );
    // };

    return (
      <StyledDiv {...props} className={`${styleType} product-card-new`}>
        <div className="product-card-left-box" style={{ color: item.itemColor || '#cccccc' }}>
          <div className="image-box">
            {isShowFavoriteIcon && (
              <div className="product-card-field start">
                <span className="star-icon">
                  <img src={item?.isUserWished ? Images.RED_HEART_ICON : Images.GREY_HEART_ICON} alt="favorite" />
                </span>
              </div>
            )}
            {isShowLending && (
              <div className="lending-timer">
                {/* <div className="timer">
                  <img src={Images.BLUE_TIMER_ICON} alt="" />
                  <Typography bold large>
                    {item?.offerExpiration} Days
                  </Typography>
                </div> */}
                <div className="timer">
                  <img src={Images.BLUE_TIMER_ICON} alt="" />
                  {!!isExpiration ? (
                    <div className='label-expiration'>{t('product_details:lending.expired')}</div>
                  ) : (
                    <CountdownComponent
                      className="countdown-card"
                      date={item?.lendingDetailDTO?.offerExpirationEndTime || item?.offerExpirationEndTime}
                      small
                      isCard
                      onComplete={() => setIsExpiration(true)}
                    />
                  )}
                </div>
              </div>
            )}
            <Thumbnail className="image" contain={!!item.padding} url={item.imageThumbnailUrls?.[0] || item.imageUrl} />
          </div>
          <div className="info-outer-box">
            <div className="_product-card-price-box">
              <div className="_horizontal1 _product-card">
                <div className="_product-card-field-group">
                  <div className="product-card-field">
                    <Typography
                      className={classnames(
                        '_status-text',
                        {
                          'holding': item.holding,
                          'before-auction': item?.status === 'NEW' && item?.productBid,
                          'comming-soon': item?.status === 'NEW' && !item?.productBid,
                          'during-auction': item?.status === 'SALE' && item?.productBid,
                          'resale': item?.status === 'SALE' && !item?.productBid && item?.reselling,
                          'sale': item?.status === 'SALE' && !item?.productBid && !item?.reselling,
                          'sold': item?.status === 'SOLD'
                        }
                      )}
                    >
                      <span className="text-light">
                        {/* {isShowCategoryName ? Misc.trimString(item.categoryName, numShowText) : statusText} */}
                        {/* {isShowCategoryName ? item.categoryName : statusText} */}
                        {Misc.trimString(item.categoryName, numShowText)}
                      </span>
                    </Typography>
                  </div>
                </div>
              </div>
              <div className="product-header">
                <div className="product-name">
                  <Typography className="item-name bold">{Misc.trimString(item.name, numShowText)}</Typography>
                </div>
                {/* <img src={Images.WHITE_ETH_ICON} alt="" width={12} /> */}
                <img className="currency-icon" src={Images[item?.currency]} alt="" />
              </div>
              <div className="_horizontal">
                <div className="_left">
                  {/* {isShowLending && (
                    <>
                      <Thumbnail className="avata" size={40} url={item?.ownerAvatar || Images.GRAY_AVATA_DEFAULT} />
                      <div className="lender">
                        <p className="label">{t('lender')}</p>
                        <p className="lender-name">{Misc.trimString(item.username, numShowText)}</p>
                      </div>
                    </>
                  )} */}
                  {<StatusTag item={item} text={statusText} />}
                </div>
                <div className="_price bold">
                  {isShowLending && <p className="price-label">{t('price_per_day')}</p>}
                  <Typography
                    poppins
                    className={`_price-amount ${(!isStatusLending ? price : item?.pricePerDay)?.toString().length > 10 && '_price-amount-custom'}`}
                  >
                    {Format.price(!isStatusLending ? price : item?.pricePerDay)}&nbsp;
                    <span className="bold">{item?.currency}</span>
                  </Typography>
                  <div className="product-card-field">
                    <span className="price-jpy">
                      {!isStatusLending ? (
                        i18n.language.toUpperCase() === 'JA'
                          ? `${Format.price(yenPrice)}JPY`
                          : `${Format.price(yenPrice / getRateByCurrency(CURRENCIES.USD, masterData))} ${CURRENCIES.USD}`
                      ) : (
                        i18n.language.toUpperCase() === 'JA'
                          ? `${Format.price(item?.pricePerDay * getRateByCurrency(CURRENCIES[item?.currency], masterData))}JPY`
                          : `${Format.price(
                            (item?.pricePerDay * getRateByCurrency(CURRENCIES[item?.currency], masterData)) /
                            +getRateByCurrency(CURRENCIES.USD, masterData),
                          )} ${CURRENCIES.USD}`
                      )}
                      {/* : `${Format.price(yenPrice / usdToJpy)} USD`} */}
                    </span>
                  </div>
                  {isShowEndTime && (
                    <Typography className="price-jpy time">
                      {moment(item?.productBid?.endTime).format('YYYY-MM-DD HH:mm')}
                    </Typography>
                  )}
                </div>
              </div>

              {/* {item.offerNftTransactionStatus === 'SUCCESS' && (
                <div className="_horizontal">
                  <Typography className="_label" size="small" secondary poppins>
                    Offer
                  </Typography>
                  <div className="_item-offer">
                    {!item.offerHighestPrice ? (
                      <div className="_tag-box">
                        <Typography size="small">Open</Typography>
                      </div>
                    ) : (
                      <div className="_number-box">
                        {_renderCamelCasePrice(item.offerHighestPrice, item.offerHighestPriceCurrency)}
                      </div>
                    )}
                  </div>
                </div>
              )} */}
            </div>
          </div>
        </div>
        {showDescription && (
          <div className="product-card-right-box">
            <Typography size="big" bold>
              {item.description}
            </Typography>
          </div>
        )}
        {showNumberRanking && (
          <div className="number-ranking">
            <Typography size="big" bold className="bold">
              {index + 1}
            </Typography>
          </div>
        )}
      </StyledDiv>
    );
  },
);
ProductCard.propTypes = {
  onClick: PropTypes.func,
  showDescription: PropTypes.bool,
  smallInMobile: PropTypes.bool,
  item: PropTypes.object,
  styleType: PropTypes.string,
};
ProductCard.defaultProps = {
  smallInMobile: false,
};

export default ProductCard;
