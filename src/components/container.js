import styled from 'styled-components';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Media from '../utils/media';

const Div = styled.div`
  max-width: 1260px;
  box-sizing: border-box;
  margin: 0 auto;
  padding: 0 20px;
  width: 100%;

  &.container-custom {
    padding-bottom: 0;
  }

  &.fluid {
    max-width: 1672px;
  }

  ${Media.lessThan(Media.SIZE.XL)} {
    max-width: 1000px;
  }

  ${Media.lessThan(Media.SIZE.LG)} {
    max-width: 760px;
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    max-width: 580px;
  }

  ${Media.lessThan(Media.SIZE.SM)} {
    max-width: initial;
  }
`;

const Container = ({ fluid, children, className }) => {
  className = classNames(className, { fluid }, 'container');

  return <Div className={className}>{children}</Div>;
};
Container.propTypes = {
  fluid: PropTypes.bool,
};

export default Container;
