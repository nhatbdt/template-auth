import { withTranslation } from 'react-i18next';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import classnames from 'classnames';
import { Colors } from '../theme';

const StyledButton = styled.div`
  .switch-button {
    border-radius: 30px;
    overflow: hidden;
    text-align: center;
    font-size: 18px;
    letter-spacing: 1px;
    color: #222;
    position: relative;
    display: flex;
    height: 30px;
    background-color: #7f7f7f;

    .button {
      transition: transform 300ms linear;
      border: none;
      border-radius: 30px;
      cursor: pointer;
      flex: 1;
      background-color: transparent;

      &:first-child {
        span {
          color: #222;
        }
      }

      span {
        display: flex;
        font-size: 14px;
        color: #222;
        white-space: nowrap;
        font-size: 11px;
        align-items: center;
        justify-content: center;
      }

      &:last-child {
        background-color: ${Colors.PRIMARY};
      }
    }
  }

  &.checked {
    .switch-button {
      .button {
        background-color: ${Colors.PRIMARY};

        &:last-child {
          background-color: transparent;
        }
      }
    }
  }

  &.disabled {
    opacity: 0.3;
  }
`;

const ButtonSwitch = props => {
  const { checked, t, className, disabled } = props;

  const onClick = e => {
    if (disabled) return;

    e.stopPropagation();
    const { onChange } = props;

    if (onChange) {
      onChange(!checked);
    }
  };

  return (
    <StyledButton className={classnames(className, { checked, disabled })}>
      <div className="switch-button">
        <button onClick={onClick} className="button">
          <span>{t('offer.accept_offer')}</span>
        </button>
        <button onClick={onClick} className="button">
          <span>{t('offer.reject_offer')}</span>
        </button>
      </div>
    </StyledButton>
  );
};

ButtonSwitch.propTypes = {
  onChange: PropTypes.func,
  checked: PropTypes.bool,
  disabled: PropTypes.bool,
};

export default withTranslation('product_details')(ButtonSwitch);
