import PropTypes from 'prop-types';
import styled from 'styled-components';
import classnames from 'classnames';
import { withTranslation } from 'react-i18next';

import Typography from '../components/typography';
import Clickable from '../components/clickable';
import ZoomHoverBackground from '../components/zoom-hover-background';
import { Colors } from '../theme';
import Media from '../utils/media';
// import { inject, observer } from 'mobx-react';

const StyledDiv = styled.div`
  height: 100%;

  .banner-zoom-hover {
    border-radius: 20px;
    height: 100%;
    color: white;
    display: flex;

    ._content {
      border-radius: 20px;
      display: flex;
      justify-content: flex-end;
      align-items: center;
      width: 100%;
      height: 100%;
      position: relative;
      padding-right: 80px;

      ._top-left-text {
        position: absolute;
        top: 42px;
        left: 49px;
      }

      ._bottom-left-text {
        position: absolute;
        bottom: 42px;
        left: 49px;
        text-decoration: underline;
      }

      ._close-button {
        position: absolute;
        top: 35px;
        right: 38px;
      }

      .sign-image {
        border-radius: 20px;
        margin-right: 80px;
      }

      .texts-box {
        .big-title {
          margin-top: 10px;
          margin-bottom: 15px;
          line-height: 1.2;
          white-space: pre-wrap;
        }

        .sub-title {
          margin-top: 20px;
          white-space: pre-wrap;
          line-height: 1.4;
        }
      }
    }
  }

  &.red {
    .banner-zoom-hover {
      ._content {
        ._top-left-text,
        ._bottom-left-text {
          color: ${Colors.RED_2};
        }

        .texts-box {
          .big-title,
          .title,
          .sub-title {
            color: ${Colors.RED_2};
          }
        }
      }
    }
  }

  &.blue {
    .banner-zoom-hover {
      ._content {
        ._top-left-text,
        ._bottom-left-text {
          color: #367dd2;
        }

        .texts-box {
          .big-title,
          .title,
          .sub-title {
            color: #367dd2;
          }
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    // height: 250px;

    .banner-zoom-hover {
      ._content {
        justify-content: flex-start;
        align-items: flex-start;
        flex-direction: column;
        padding: 20px;
        padding-right: 20px;

        ._top-left-text {
          position: static;
          top: 0;
          left: 0;
          margin-bottom: 20px;
        }

        ._bottom-left-text {
          display: none;
        }

        .sign-image {
          display: none;
        }

        .texts-box {
          .title {
            font-size: 20px;
            font-weight: 500;
          }

          .big-title {
            margin-top: 15px;
            font-size: 24px;
          }

          .sub-title {
            margin-top: 20px;
            line-height: 1.4;
          }
        }
      }
    }
  }
`;

const Banner = withTranslation()(({ product, item, onClickDetail, hideSeeMoreLink, t }) => (
  <StyledDiv className={classnames({ red: item.type === 'RED', blue: item.type === 'BLUE' })}>
    <ZoomHoverBackground
      url={item.backgroundImage}
      mobileUrl={item.mobileBackground}
      bgColor={item?.bgColor}
      className="banner-zoom-hover"
    >
      <Clickable className="_content" onClick={() => onClickDetail(item)}>
        <Typography className="_top-left-text" bold>
          {item.category}
        </Typography>
        {!hideSeeMoreLink && (
          <Typography className="_bottom-left-text" bold>
            {t('common:see_more')}
          </Typography>
        )}
        {item.labelImage && <img src={item.labelImage} alt="" className="sign-image" />}
        <div className="texts-box">
          <Typography className="title" size="giant" bold>
            {item.title}
          </Typography>
          <Typography size="enormous" className="big-title" bold>
            {item.bigTitle}
          </Typography>
          <Typography className="sub-title" bold size="large">
            {item.subTitle}
          </Typography>
        </div>
      </Clickable>
    </ZoomHoverBackground>
  </StyledDiv>
));

Banner.propTypes = {
  item: PropTypes.object,
  onClickDetail: PropTypes.func,
  hideSeeMoreLink: PropTypes.bool,
};

export default Banner;
