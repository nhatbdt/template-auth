import PropTypes from 'prop-types';
import styled from 'styled-components';

import Media from '../utils/media';

const StyledDiv = styled.div`
  border-radius: 0;
  overflow: hidden;
  position: relative;
  height: 100px;

  ._img-background {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background-color: ${props => props.bgColor};
    background-image: url('${props => props.url}');
    background-repeat: no-repeat;
    background-size: cover;
    background-position: center;
    transition: transform 0.2s;
  }

  ._mask {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    opacity: 0.5;
  }

  ._body {
    position: relative;
    width: 100%;
    height: 100%;
  }

  &:hover {
    ._img-background {
      transform: scale(1.05);
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    ._img-background {
      background-image: url('${props => props.mobileUrl || props.url}');
    }
  }
`;

const ZoomHoverBackground = ({ url, mobileUrl, className, children, mask, bgColor }) => (
  <StyledDiv url={url} mobileUrl={mobileUrl} bgColor={bgColor} className={className}>
    <div className="_img-background" />
    {mask && <div className="_mask" style={{ backgroundColor: mask }} />}
    <div className="_body">{children}</div>
  </StyledDiv>
);
ZoomHoverBackground.propTypes = {
  url: PropTypes.string,
  mobileUrl: PropTypes.string,
  mask: PropTypes.bool,
  bgColor: PropTypes.string,
};

export default ZoomHoverBackground;
