import PropTypes from 'prop-types';
import styled from 'styled-components';

import { Images } from '../theme';
import Typography from './typography';
import Clickable from './clickable';
import SearchInput from './search-input';
import Media from '../utils/media';

const StyledDiv = styled.div`
  position: relative;
  height: 68px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #151724;
  padding: 0 33px;
  z-index: 1;
  backdrop-filter: blur(20px) opacity(0);

  .left-box {
    margin-right: 20px;
    display: flex;
    align-items: center;

    .title {
      white-space: nowrap;
    }

    .mobile-search-button {
      display: none;
      margin-left: 8px;
      width: 26px;
      height: 26px;
      background-color: #fff100;
      align-items: center;
      justify-content: center;
      border-radius: 4px;

      img {
        width: 12px;
      }
    }
  }

  .search-input {
    background-color: rgba(33, 35, 48, 0.6);
    background-blend-mode: screen;
  }

  .settings-button {
    margin-right: 37px;
    display: flex;
    margin-left: 20px;

    .settings-icon {
      margin-right: 19px;
    }

    .typography {
      white-space: nowrap;
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    background-color: transparent;
    padding: 0 20px;

    .left-box {
      margin-left: 0;

      //.mobile-search-button {
      //  display: flex;
      //}
    }

    //.search-input {
    //  display: none;
    //}

    .settings-button {
      margin-right: 0;

      .settings-icon {
        margin-right: 9px;
        width: 14px;
      }

      .typography {
        font-size: 12px;
      }
    }
  }
`;

const ToolBar = ({ title, className, defaultSearchType, defaultSearchValue, onSearch, preventChangeSearchType }) => (
  <StyledDiv className={className}>
    <div className="left-box">
      <Typography bold size="giant" className="title">
        {title}
      </Typography>
      <Clickable className="mobile-search-button">
        <img src={Images.BLACK_SEARCH_ICON} alt="" />
      </Clickable>
    </div>
    <SearchInput
      defaultSearchType={defaultSearchType}
      defaultSearchValue={defaultSearchValue}
      className="search-input"
      onSearch={onSearch}
      preventChangeSearchType={preventChangeSearchType}
    />
    <Clickable className="settings-button">
      {/* <img className="settings-icon" src={Images.WHITE_SETTING_ICON} alt="" /> */}
      {/* <Typography size="large"> */}
      {/*  絞り込み */}
      {/* </Typography> */}
    </Clickable>
  </StyledDiv>
);
ToolBar.propTypes = {
  title: PropTypes.string,
  defaultSearchType: PropTypes.string,
  defaultSearchValue: PropTypes.string,
  onSearch: PropTypes.func,
  preventChangeSearchType: PropTypes.bool,
};

export default ToolBar;
