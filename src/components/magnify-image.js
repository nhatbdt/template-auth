import { Component } from 'react'
import PropTypes from 'prop-types'
import ReactImageMagnify from 'react-image-magnify'
import lodash from 'lodash'

class MagnifyImage extends Component {
  static propTypes = {
    url: PropTypes.string,
    maxSize: PropTypes.any
  }

  state = {
    inited: false,
    width: 0,
    height: 0
  }

  componentDidMount() {
    let { url, maxSize } = this.props
    const img = new Image()

    if (lodash.isNumber(maxSize)) maxSize = [maxSize, maxSize]

    img.src = url

    img.onload = () => {
      let { width, height } = img

      if ((maxSize[0] / maxSize[1]) > (img.width / img.height)) {
        // eslint-disable-next-line prefer-destructuring
        height = maxSize[1]
        width = (maxSize[1] * img.width) / img.height
      } else {
        // eslint-disable-next-line prefer-destructuring
        width = maxSize[0]
        height = (maxSize[0] * img.height) / img.width
      }

      this.setState({
        inited: true,
        width,
        height,
        zoomWidth: 1000,
        zoomHeight: (1000 * img.height) / img.width
      })
    }
  }

  render() {
    const { url } = this.props
    const { inited, width, height, zoomWidth, zoomHeight } = this.state

    if (!inited) return null

    return (
      <ReactImageMagnify
        isFluidWidth
        smallImage={{
          alt: 'Wristwatch by Ted Baker London',
          src: url,
          width,
          height
        }}
        largeImage={{
          src: url,
          width: zoomWidth,
          height: zoomHeight
        }}
        enlargedImageContainerDimensions={{
          width: 500,
          height: 500
        }}
      />
    )
  }
}

export default MagnifyImage
