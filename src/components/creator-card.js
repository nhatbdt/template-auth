import styled from 'styled-components';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import Format from '../utils/format';
import Media from '../utils/media';
import Clickable from './clickable';
import Typography from './typography';

const Creator = styled(Clickable)`
  height: 100%;
  .creator-item-image {
    position: relative;
    border-radius: 6px;
    overflow: hidden;
    .image-box {
      position: relative;
      padding-top: 160%;
      img {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        object-fit: cover;
        border-radius: 6px;
      }
    }
    .creator-item-name {
      height: 40%;
      position: absolute;
      bottom: 0;
      display: flex;
      flex-direction: column;
      align-items: flex-start;
      justify-content: flex-end;
      width: 100%;
      padding: 30px 22px 16px 22px;
      background: rgb(0, 0, 0);
      background: linear-gradient(0deg, rgba(0, 0, 0) 0%, rgba(0, 0, 0, 0) 100%);
      .culture-name {
        color: #ffffff;
        font-size: 12px;
        font-weight: bold;
        overflow: hidden;
        display: -webkit-box;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
      }
      .creator-name {
        color: #ffffff;
        font-size: 15px;
        font-weight: bold;
        text-transform: uppercase;
        overflow: hidden;
        display: -webkit-box;
        -webkit-line-clamp: 1;
        -webkit-box-orient: vertical;
      }
      ${Media.lessThan(Media.SIZE.MD)} {
        padding: 20px 12px 10px 12px;
        .culture-name {
          font-size: 10px;
        }
        .creator-name {
          font-size: 10px;
        }
      }
    }
  }
  .creator-content {
    .creator-statistical {
      padding: 16px 0;
      ${Media.lessThan(Media.SIZE.XL)} {
        padding: 16px 0;
      }
      ${Media.lessThan(Media.SIZE.MD)} {
        padding: 13px 0;
      }
      ul {
        display: flex;
        list-style: none;
        padding: 0;
        margin: 0;
        width: 100%;
        li {
          display: flex;
          flex-direction: column;
          align-items: center;
          text-align: center;
          font-size: 12px;
          opacity: 0.5;
          padding: 0 10px;
          width: calc(100% / 3);
          ${Media.lessThan(Media.SIZE.XL)} {
            padding: 0 5px;
          }
          ${Media.lessThan(Media.SIZE.MD)} {
            font-size: 10px;
          }
        }
        li + li {
          border-left: 1px solid rgba(51, 51, 51, 0.15);
        }
      }
    }
    .creator-description {
      font-size: 12px;
      overflow: hidden;
      display: -webkit-box;
      -webkit-line-clamp: 3;
      -webkit-box-orient: vertical;
      ${Media.lessThan(Media.SIZE.MD)} {
        font-size: 10px;
      }
    }
  }
`;

const CreatorCard = ({ item, className, ...props }) => {
  const { t } = useTranslation('common');

  return (
    <Creator {...props} className={className}>
      <div className="creator-item-image">
        <div className="image-box">
          <img src={item.thumbnail} alt={item.name} />
        </div>
        <div className="creator-item-name">
          <Typography className="culture-name bold">{item.cultureName}</Typography>
          <Typography className="creator-name bold">{item.name}</Typography>
        </div>
      </div>
      <div className="creator-content">
        <div className="creator-statistical">
          <ul>
            <li>
              <span>{Format.price(item.productCount)}</span>
              {item.productCount > 1 ? <span>{t('creator.items')}</span> : <span>{t('creator.item')}</span>}
            </li>
            <li>
              <span>{Format.price(item.viewCount)}</span>
              {item.viewCount > 1 ? <span>{t('creator.views')}</span> : <span>{t('creator.view')}</span>}
            </li>
            <li>
              <span>{Format.price(item.favoriteCount)}</span>
              {item.favoriteCount > 1 ? <span>{t('creator.favorites')}</span> : <span>{t('creator.favorite')}</span>}
            </li>
          </ul>
        </div>
        <div className="creator-description">{item.description}</div>
      </div>
    </Creator>
  );
};
CreatorCard.propTypes = {
  onClick: PropTypes.func,
  showDescription: PropTypes.bool,
  smallInMobile: PropTypes.bool,
  item: PropTypes.object,
  className: PropTypes.string,
};

export default CreatorCard;
