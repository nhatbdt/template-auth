import { Switch } from 'antd';
import React from 'react';
import styled from 'styled-components';

const StyledSwitch = styled(Switch)`
  &.ant-switch {
    background-color: rgba(255, 255, 255, 0.2);
  }

  &.ant-switch-checked {
      background-color: #1890ff;
  }
`

const SwitchButton = (props) => {
  return (
    <StyledSwitch {...props} />
  )
};
export default SwitchButton;
