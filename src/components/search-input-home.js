import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import classnames from 'classnames';
import { withTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';
import { inject, observer } from 'mobx-react';
import { Images, Colors } from '../theme';
import Clickable from './clickable';
import Input from './input';
import Media from '../utils/media';
import { history } from '../store';

const StyledDiv = styled.div`
  display: flex;
  height: 40px;
  border-radius: 30px;
  overflow: hidden;
  width: 100%;
  max-width: 700px;

  .menu-category {
    background-color: red;
  }

  .search-type-box {
    display: flex;
    align-items: center;
    gap: 8px;
    min-width: 160px;
    padding: 0 15px 0 25px;
    justify-content: space-between;
    background: #efefef;

    .typography {
      font-size: 16px;
      color: #282828;
    }
  }

  .search-box-divider {
    width: 0.5px;
    background: #efefef;
    height: 50px;
    align-self: center;
  }

  .input-wrapper {
    flex: 1;
    display: flex;

    .input {
      flex: 1;
      height: auto;
      border: none;
      color: #c0c0c0;
      font-size: 13px;
      background: transparent;
      ::placeholder {
        color: #c0c0c0;
        opacity: 1; /* Firefox */
      }
      :-ms-input-placeholder {
        /* Internet Explorer 10-11 */
        color: #c0c0c0;
      }
      ::-ms-input-placeholder {
        /* Microsoft Edge */
        color: #c0c0c0;
      }
    }

    .search-button {
      width: 50px;
      /* background-color: ${Colors.PRIMARY}; */
      display: flex;
      justify-content: center;
      align-items: center;

      /* img {
        width: 16px;
      } */
    }
  }

  &.large {
    height: 68px;

    .input-wrapper {
      .search-button {
        width: 68px;

        img {
          width: auto;
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .search-type-box {
      width: auto;
      justify-content: space-between;
      padding-left: 11px;
      padding-right: 22px;
      height: 40px;

      .typography {
        margin-right: 0;
      }
    }

    .search-box-divider {
      height: 1px;
    }

    .input-wrapper {
      flex: auto;
      height: 40px;

      .input {
        padding: 0 11px;
      }

      .search-button {
        width: 40px;

        img {
          width: 14px;
        }
      }
    }

    &.large {
      height: auto;

      .input-wrapper {
        .search-button {
          width: 40px;

          img {
            width: 14px;
          }
        }
      }
    }
  }

  /* ul {
    li {
      color: #fff !important;
    }
  } */
`;

@withRouter
@inject(stores => ({
  categoriesStore: stores.categories,
}))
@observer
@withTranslation('common')
class SearchInputHome extends Component {
  static propTypes = {
    onSearch: PropTypes.func,
    onHeaderSearch: PropTypes.func,
    categoriesStore: PropTypes.object,
    defaultCategoryIdSelect: PropTypes.object,
    defaultSearchValue: PropTypes.object,
    // i18n: PropTypes.object,
    location: PropTypes.object,
    // t: PropTypes.object,
    size: PropTypes.any,
    className: PropTypes.any,
  };

  constructor(props) {
    super(props);

    const { defaultCategoryIdSelect, defaultSearchValue, categoriesStore } = props;
    const { listCategory } = categoriesStore;

    this.state = {
      categoryIdSelect: defaultCategoryIdSelect || '',
      value: defaultSearchValue || '',
      listCategory: listCategory || [],
    };
  }

  async componentDidMount() {
    this.setCategories();
  }

  setCategories = async () => {
    const { categoriesStore, i18n } = this.props;
    const res = await categoriesStore.getListCategory({ langKey: i18n.language.toUpperCase(), limit: 100, page: 1 });
    if (res.success) {
      this.setState({ listCategory: res.data.result });
    }
  };

  componentDidUpdate = async (newProps, newState) => {
    if (this.props.location !== newProps.location) {
      this.setCategories();
    }
  };

  _onSearch = () => {
    // const { onHeaderSearch } = this.props;
    // let { value, categoryIdSelect } = this.state;
    // value = value.trim().replace('%', '');
    // const encodeValue = encodeURIComponent(value);

    // history.replace(`/category/${categoryIdSelect}?query=${encodeValue}`);

    // if (onHeaderSearch) onHeaderSearch();
    let { value } = this.state;
    value = value.trim().replace('%', '');
    const encodeValue = encodeURIComponent(value);
    // history.replace(`/search-products/?query=${encodeValue}`);
    history.push(`/search-products/?query=${encodeValue}`);
  };

  _onClear = () => {
    this.setState(
      {
        value: '',
      },
      () => {
        this._onSearch();
      },
    );
  };

  _onSearchChange = e => {
    this.setState({
      value: e.target.value,
    });
  };

  _onKeyUp = e => {
    if (e.key === 'Enter' || e.keyCode === 13) {
      this._onSearch();
    }
  };

  _onChooseCategory = id => {
    this.setState({
      categoryIdSelect: id,
    });
  };

  render() {
    // const { t, size, className, categoriesStore } = this.props;
    const { t, size, className } = this.props;
    // const { listCategory } = categoriesStore;
    // const { value, categoryIdSelect } = this.state;
    const { value } = this.state;

    // console.log("categoryIdSelect", this.state, listCategory.toJSON());

    return (
      <StyledDiv className={classnames(size, className)}>
        {/* <Dropdown
          trigger="click"
          overlay={
            <Menu className="menu-category">
              {listCategory?.map(item => (
                <Menu.Item key={item.id} onClick={() => this._onChooseCategory(item.id)}>
                  {item.name}
                </Menu.Item>
              ))}
            </Menu>
          }
        >
          <Clickable className="search-type-box">
            <Typography size="large">
              {!categoryIdSelect ? t('header.select_category') : listCategory.find(x => x.id === categoryIdSelect).name}
            </Typography>
            <img src={Images.GRAY_CHEVRON_DOWN_ICON} alt="icon" />
          </Clickable>
        </Dropdown>
        <div className="search-box-divider" /> */}
        <div className="input-wrapper">
          <Input
            value={value}
            onChange={this._onSearchChange}
            onKeyUp={this._onKeyUp}
            placeholder={t('header.search')}
          />
          <Clickable className="search-button" onClick={this._onSearch}>
            <img src={Images.COLOR_SEARCH_ICON} alt="icon" width={20} height={20} />
          </Clickable>
          {/* {value ? (
            <Clickable className="search-button" onClick={this._onClear}>
              <CloseOutlined />
            </Clickable>
          ) : (
            <>
              {categoryIdSelect ? (
                <Clickable className="search-button" onClick={this._onSearch}>
                  <img src={Images.GRAY_SEARCH_ICON} alt="icon" />
                </Clickable>
              ) : null}
            </>
          )} */}
        </div>
      </StyledDiv>
    );
  }
}

export default SearchInputHome;
