export const clearEmpty = (obj) => {
  if (!Object.keys(obj).length) return {}
  return Object.keys(obj).reduce((acc, key) => {
    if (obj[key] === undefined || obj[key] === null || obj[key] === '') return acc
    acc[key] = obj[key]
    return acc
  }, {})
}
