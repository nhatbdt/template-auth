import authStore from '../store/auth';
// import LoginModal from '../app/login-modal';

const login = func => params => {
  if (authStore.loggedIn) {
    func(params);
  } else {
    // LoginModal.open();
    authStore.toggleOpenModalLogin();
  }
};

const hasConnectMetamask = func => {
  return async params => {
    if (window.ethereum) {
      try {
        const accounts = await window.ethereum.request({
          method: 'eth_requestAccounts',
        });

        const { publicAddress } = authStore.initialData;
        // eslint-disable-next-line no-console
        console.log(accounts, publicAddress);
        if (!publicAddress || !accounts) {
          throw new Error('public address cannot null');
        } else if (accounts[0]?.toLowerCase() != publicAddress?.toLowerCase()) {
          alert('wallet connected incorrect');
          return;
        }

        return func(params);
      } catch (error) {
        // eslint-disable-next-line no-console
        console.log(error);
      }
    }
  };
};

export { login, hasConnectMetamask };
