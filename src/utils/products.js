import { CHAIN_LIST } from '../constants/chains';
import { LENDING_STATUS, STAKING_STATUS } from '../constants/statuses'

export const getStatusText = (item, t) => {
  // return item?.holding
  //   ? t('product_status.holding')
  //   : item?.status === 'NEW'
  //   ? item?.productBid
  //     ? t('product_status.before_auction')
  //     : t('product_status.coming_soon')
  //   : item?.status === 'SALE'
  //   ? item?.productBid
  //     ? t('product_status.during_auction')
  //     : // : item?.reselling
  //     item?.reselling || item?.productResell?.reselling
  //     ? t('product_status.resale')
  //     : t('product_status.sale')
  //   : t('product_status.sold');
  if (item?.holding) return t('product_status.holding')

  if (
    item?.lendingDetailDTO?.status === LENDING_STATUS.LENDING
    || item?.lendingStatus === LENDING_STATUS.LENDING
  ) return t('product_status.lending')
  if (
    item?.lendingDetailDTO?.status === LENDING_STATUS.IN_LENDING
    || item?.lendingStatus === LENDING_STATUS.IN_LENDING
  ) return t('product_status.in_lending')

  if (item?.stakingStatus === STAKING_STATUS.STAKING || item?.stakingStatus === STAKING_STATUS.STAKING_PROGRESS) 
    return t('product_status.staking')
  
  if (item?.status === 'NEW') {
    return item?.productBid
      ? t('product_status.before_auction')
      : t('product_status.coming_soon')
  }

  if (item?.status === 'SALE') {
    return item?.productBid
      ? t('product_status.during_auction')
      : (item?.reselling || item?.productResell?.reselling)
        ? t('product_status.resale')
        : t('product_status.sale')
  }

  if (item?.offerNftTransactionStatus === 'NEW' && item?.canOfferFlag) return t('product_status.processing_offer')

  if (item?.status === 'SOLD') return t('product_status.sold')

  // return item?.holding
  //   ? t('product_status.holding')
  //   : item?.status === 'NEW'
  //     ? item?.productBid
  //       ? t('product_status.before_auction')
  //       : t('product_status.coming_soon')
  //     : item?.status === 'SALE'
  //       ? item?.productBid
  //         ? t('product_status.during_auction')
  //         : item?.reselling
  //           ? t('product_status.resale')
  //           : t('product_status.sale')
  //       : t('product_status.sold');

  // item?.buying
  // ? t('product_status.during_trading')
  // : t('product_status.sold');
};

export const getAllowedCurrencies = () => {
  const arrErc20Token = [];
  for (let key in CHAIN_LIST) {
    if (CHAIN_LIST.hasOwnProperty(key)) {
      const value = CHAIN_LIST[key].erc20Token;
      for (let k in value) {
        if (value.hasOwnProperty(k)) {
          const val = value[k];
          arrErc20Token.push(val.symbol);
        }
      }
    }
  }

  const uniqueAllowedTokens = [...new Set(arrErc20Token)];

  const currencyOptions = uniqueAllowedTokens.map(item => ({ label: item, value: item }));
  return currencyOptions;
};
