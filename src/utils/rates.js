export const getRateByCurrency = (currency, masterData) => {
  let rate = 0;
  switch (currency) {
    case 'USDT':
      rate = masterData['USDT_TO_JPY'];
      break;

    case 'USD':
      rate = masterData['USD_TO_JPY'];
      break;

    case 'MATIC':
      rate = masterData['MATIC_TO_JPY'];
      break;

    case 'WMATIC':
      rate = masterData['MATIC_TO_JPY'];
      break;

    case 'POINT':
      rate = masterData['POINT_TO_JPY'];
      break;

    default:
      break;
  }

  return rate;
};
