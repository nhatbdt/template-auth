import USER_PLACEHOLDER from '../resources/images/user-placeholder.jpeg';
import FAVICON from '../resources/images/favicon.png';
import LOGO from '../resources/images/logo.png';

// Color icons
import COLOR_METAMASK_ICON from '../resources/images/icons/color-metamask-icon.svg';
import COLOR_WALLET_CONNECT_ICON from '../resources/images/icons/color-wallet-connect-icon.png';
import COLOR_TWITTER_ICON from '../resources/images/icons/TWITER_COLOR.svg';
import COLOR_TELEGRAM_ICON from '../resources/images/icons/TELEGRAM_COLOR.svg';
import COLOR_ENTER_ICON from '../resources/images/icons/Icon_Enter.svg';
import COLOR_SEARCH_ICON from '../resources/images/icons/Icon_Search.svg';
import COLOR_CURRENCY_ICON from '../resources/images/icons/currency_icon.svg';
import COLOR_DOWN_ICON from '../resources/images/icons/icon_down.svg';
import COLOR_SORTBY_ICON from '../resources/images/icons/icon_sortby.svg';
import COLOR_FILTER_ICON from '../resources/images/icons/filter_ICON.svg';
import COLOR_MSG_ICON from '../resources/images/icons/color-message-icon.svg';
import COLOR_FACEBOOK_ICON from '../resources/images/icons/facebook_icon.svg';
import COLOR_GOOGLE_ICON from '../resources/images/icons/google_icon.svg';
// import COLOR_TWITTER_ICON from '../resources/images/icons/twitter_icon.svg';
import COLOR_DISCORD_ICON from '../resources/images/icons/discord_icon.svg';
import ICON_UPLOAD_FILE from '../resources/images/icons/icon_upload_file.svg';
import ICON_EMAIL from '../resources/images/icons/icon-email.png';

// currencies
import MATIC from '../resources/images/currencies/matic.png';
import WMATIC from '../resources/images/currencies/matic.png';
import USDT from '../resources/images/currencies/usdt.png';

// Gray icons
import GRAY_STAR_ICON from '../resources/images/icons/gray-star-icon.svg';
import BLUE_SETTING_ICON from '../resources/images/icons/blue-setting-icon.svg';
import GRAY_MSG_ICON from '../resources/images/icons/gray-message-icon.svg';
import GRAY_ADVANCED_ICON from '../resources/images/icons/gray-advanced-icon.svg';
import GRAY_LOGOUT_ICON from '../resources/images/icons/gray-logout-icon.svg';
import GRAY_EDIT_ICON from '../resources/images/icons/gray-edit-icon.svg';
import GRAY_USER_ICON from '../resources/images/icons/gray-user-icon.svg';
import GRAY_EMAIL_ICON from '../resources/images/icons/gray-email-icon.svg';
import GRAY_VIEW_ICON from '../resources/images/icons/gray-view-icon.svg';
import GRAY_UNVIEW_ICON from '../resources/images/icons/gray-unview-icon.svg';
import GRAY_COPY_ICON from '../resources/images/icons/gray-copy-icon.svg';
import GRAY_AVATA_DEFAULT from '../resources/images/icons/gray-avata-default-icon.svg';
import GRAY_ARROW_DOWN_ICON from '../resources/images/icons/gray-arrow-down-icon.svg';
import GRAY_ARROW_UP_ICON from '../resources/images/icons/gray-arrow-up-icon.svg';
import GRAY_COUNTDOWN_ICON from '../resources/images/icons/countdown-icon.svg';

// White icons
import WHITE_CHEVRON_DOWN_ICON from '../resources/images/icons/white-chevron-down-icon.svg';
import WHITE_CHEVRON_RIGHT_ICON from '../resources/images/icons/white-chevron-right-icon.svg';
import WHITE_SETTING_ICON from '../resources/images/icons/white-setting-icon.svg';
import WHITE_CHEVRON_UP_ICON from '../resources/images/icons/white-chevron-up-icon.svg';
import WHITE_COPPY_ICON from '../resources/images/icons/coppy-icon.svg';
import WHITE_EMAIL_ICON from '../resources/images/icons/email_icon.svg';

// BLACK icons
import BLACK_SEARCH_ICON from '../resources/images/icons/black-search-icon.svg';
import BLACK_TRANSFER_ICON from '../resources/images/icons/black-transfer-icon.svg';

// Flags
import EN_FLAG from '../resources/images/flags/en.png';
import JA_FLAG from '../resources/images/flags/ja.png';
import ZH_FLAG from '../resources/images/flags/zh.png';

import YELLOW_SEARCH_ICON from '../resources/images/icons/yellow-search-icon.svg';
import FILTER_ICON from '../resources/images/icons/filter-icon.svg';
import FILTER_BLACK_ICON from '../resources/images/icons/filter-black-icon.svg';
import FILTER_WHITE_ICON from '../resources/images/icons/filter-white-icon.svg';
import SORT_ICON from '../resources/images/icons/sort-icon.svg';

// Gray icons
import GRAY_TRANSFER_ICON from '../resources/images/icons/gray-transfer-icon.svg';
import GRAY_CHEVRON_DOWN_ICON from '../resources/images/icons/gray-chevron-down-icon.svg';
import GRAY_SEARCH_ICON from '../resources/images/icons/gray-search-icon.svg';
import GREY_HEART_ICON from '../resources/images/icons/grey-heart-icon.png';
import RED_HEART_ICON from '../resources/images/icons/red-heart-icon.png';
import GRAY_SHARE_ICON from '../resources/images/icons/gray-share-icon.svg';

import GET_STARTED_1 from '../resources/images/started/started1.png';
import GET_STARTED_2 from '../resources/images/started/started2.png';
import GET_STARTED_3 from '../resources/images/started/started3.png';
import FILTER_BLUE_ICON from '../resources/images/icons/filter-blue-icon.svg';

// menu

import MENU_HOME_ICON from '../resources/images/icons/menu-home-icon.svg';
import MENU_HOME_ACTIVE_ICON from '../resources/images/icons/menu-home-active-icon.svg';
import MENU_SEARCH_ICON from '../resources/images/icons/menu-search-icon.svg';
import MENU_SEARCH_ACTIVE_ICON from '../resources/images/icons/menu-search-active-icon.svg';
import MENU_WALLET_ICON from '../resources/images/icons/menu-wallet-icon.svg';
import MENU_WALLET_ACTIVE_ICON from '../resources/images/icons/menu-wallet-active-icon.svg';
import MENU_MORE_ICON from '../resources/images/icons/menu-more-icon.svg';
import MENU_MORE_ACTIVE_ICON from '../resources/images/icons/menu-more-active-icon.svg';

// Creators Images
import WHITE_SORT from '../resources/images/icons/white-down-icon.svg';
import OWNER_ICON from '../resources/images/icons/owner.svg';
import USER_ICON from '../resources/images/icons/user.svg';
import TOTAL_ICON from '../resources/images/icons/total.svg';
import COUNTDOWN_ICON from '../resources/images/icons/countdown.png';
import BLUE_TIMER_ICON from '../resources/images/icons/blue-timer-icon.svg';

import BG_MYPAGE from '../resources/images/bg-cover.png';
import SECTION_BG_1 from '../resources/images/section-bg-1.png';

import NEW_COPY_ICON from '../resources/images/icons/new-copy-icon.svg';
import BLACK_ETH_ICON from '../resources/images/icons/black-eth-icon.svg';
import WHITE_ETH_ICON from '../resources/images/icons/white-eth-icon.svg';
import BLUE_ETH_ICON from '../resources/images/icons/blue-eth-icon.svg';

import FAQ1 from '../resources/images/faq1.png';
import FAQ2 from '../resources/images/faq2.png';
import HERO from '../resources/images/hero.jpg';
import IETH from '../resources/images/i_eth.png';
import IMTM from '../resources/images/i_mtm.png';
import ZENTAI from '../resources/images/zentai.png';
import BG_GUIDE from '../resources/images/bg-guide.jpg';
import NFT_MAKETPLACE from '../resources/images/nft-maketplace.svg';
import STEP_1_1 from '../resources/images/step1_1.png';
import STEP_1_2 from '../resources/images/step1_2.png';
import STEP_2_1 from '../resources/images/step2_1.png';
import STEP_2_2 from '../resources/images/step2_2.png';
import STEP_4_1 from '../resources/images/step4_1.png';
import STEP_5_1 from '../resources/images/step5_1.png';
import STEP_6_1 from '../resources/images/step6_1.png';

import CREDIT_STEP_1 from '../resources/images/credit-card/step-1.jpg';
import CREDIT_STEP_2 from '../resources/images/credit-card/step-2.jpg';
import CREDIT_STEP_3 from '../resources/images/credit-card/step-3.jpg';

import TELEGRAM_ICON from '../resources/images/icons/telegram.svg';
import ABOUT_US_ICON from '../resources/images/about-us-icon.png';
import ABOUT_US_BACKGROUND from '../resources/images/about-us-background.png';
import ABOUT_US_BANNER_ICON_BACKGROUND from '../resources/images/about-us-banner-icon.png';
import FAQS_ARROW_ICON from '../resources/images/faqs-arrow-icon.png';

import SHARE_FACEBOOK_ICON from '../resources/images/icons/share_facebook.svg';
import SHARE_TWITTER_ICON from '../resources/images/icons/share_twitter.svg';
import SHARE_QR_ICON from '../resources/images/icons/share_qr.svg';
import SHARE_LINK_ICON from '../resources/images/icons/share-link-icon.svg';
import SHARE_DOWNLOAD_ICON from '../resources/images/icons/share-dowload-icon.svg';
import PRICE_ICON from '../resources/images/icons/price_icon.svg';
import SURFIX_ICON from '../resources/images/icons/surfix.svg';
import BLUE_TRANSFER_ICON from '../resources/images/icons/blue-transfer-icon.svg';

// GACHA
import GACHA_MODAL_IMAGE from '../resources/images/gacha-modal-demo.png';
import GACHA_BACKGROUND_IMAGE from '../resources/images/gacha-background.png';
import GACHA_LOGO_IMAGE from '../resources/images/gacha-logo.png';
import GACHA_QUEST_IMAGE from '../resources/images/gacha-quest.png';
import PAYMENT from '../resources/images/payment.svg';
import PRODUCT_BG from '../resources/images/gacha/product-bg.png';
import SPIKE from '../resources/images/gacha/spike.svg';

import STAKING_MONEY_ICON from '../resources/images/icons/staking-icon-money.svg';
import STAKING_CURRENCY_ICON from '../resources/images/icons/staking-icon-currency.svg';
import STAKING_BG_TOP_ICON from '../resources/images/icons/staking-bg-top.png';

import BLACK_CLOSE_ICON from '../resources/images/icons/black-close-icon.svg'
import RED_WISH_ICON from '../resources/images/icons/red-wish-icon.svg'
import BLACK_CHEVRON_DOWN_ICON from '../resources/images/icons/black-chevron-down-icon.svg'
import WHITE_MENU_ICON from '../resources/images/icons/white-menu-icon.svg'
import WHITE_SOLID_CLOSE_ICON from '../resources/images/icons/white-menu-icon.svg'

import GRAY_WISH_ICON from '@/resources/images/icons/gray-wish-icon.svg'
import GRAY_EYE_ICON from '@/resources/images/icons/gray-eye-icon.svg'
import BLACK_GIFT_ICON from '@/resources/images/icons/gray-eye-icon.svg'
import BLACK_IMAGE_ICON from '@/resources/images/icons/black-image-icon.svg'
import MY_PAGE_BACKGROUND from '@/resources/images/my-page-background.jpg'
import COLOR_TETHER_ICON from '@/resources/images/icons/color-tether-icon.png'
import BLACK_NBNG_ICON from '@/resources/images/icons/black-nbng-icon.svg'
import COLOR_GOKU_ICON from '@/resources/images/icons/color-goku-icon.svg'
import BLUE_CIRCLE_TICK_ICON from '@/resources/images/icons/blue-circle-tick-icon.svg'
import PRODUCT_FRAME from '@/resources/images/product-frame.png'
import GRAY_CIRCLE_TICK_ICON from '@/resources/images/icons/gray-circle-tick-icon.svg'
import WHITE_CIRCLE_TICK_ICON from '@/resources/images/icons/white-circle-tick-icon.svg'

export {
  WHITE_CIRCLE_TICK_ICON,
  GRAY_CIRCLE_TICK_ICON,
  PRODUCT_FRAME,
  BLUE_CIRCLE_TICK_ICON,
  COLOR_GOKU_ICON,
  BLACK_NBNG_ICON,
  COLOR_TETHER_ICON,
  MY_PAGE_BACKGROUND,
  BLACK_IMAGE_ICON,
  BLACK_GIFT_ICON,
  GRAY_EYE_ICON,
  GRAY_WISH_ICON,
  WHITE_SOLID_CLOSE_ICON,
  WHITE_MENU_ICON,
  BLACK_CHEVRON_DOWN_ICON,
  RED_WISH_ICON,
  BLACK_CLOSE_ICON,
  USER_PLACEHOLDER,
  FAVICON,
  LOGO,
  COLOR_METAMASK_ICON,
  COLOR_WALLET_CONNECT_ICON,
  COLOR_TWITTER_ICON,
  COLOR_TELEGRAM_ICON,
  COLOR_ENTER_ICON,
  COLOR_SEARCH_ICON,
  COLOR_CURRENCY_ICON,
  COLOR_DOWN_ICON,
  COLOR_SORTBY_ICON,
  COLOR_FILTER_ICON,
  COLOR_MSG_ICON,
  GRAY_STAR_ICON,
  GRAY_MSG_ICON,
  BLUE_SETTING_ICON,
  WHITE_CHEVRON_DOWN_ICON,
  WHITE_CHEVRON_RIGHT_ICON,
  WHITE_SETTING_ICON,
  WHITE_CHEVRON_UP_ICON,
  WHITE_COPPY_ICON,
  BLACK_SEARCH_ICON,
  BLACK_TRANSFER_ICON,
  EN_FLAG,
  JA_FLAG,
  ZH_FLAG,
  YELLOW_SEARCH_ICON,
  FILTER_ICON,
  FILTER_BLACK_ICON,
  FILTER_WHITE_ICON,
  SORT_ICON,
  GRAY_TRANSFER_ICON,
  GRAY_CHEVRON_DOWN_ICON,
  GRAY_SEARCH_ICON,
  GREY_HEART_ICON,
  RED_HEART_ICON,
  GRAY_SHARE_ICON,
  GET_STARTED_1,
  GET_STARTED_2,
  GET_STARTED_3,
  FILTER_BLUE_ICON,
  MENU_HOME_ICON,
  MENU_HOME_ACTIVE_ICON,
  MENU_SEARCH_ICON,
  MENU_SEARCH_ACTIVE_ICON,
  MENU_WALLET_ICON,
  MENU_WALLET_ACTIVE_ICON,
  MENU_MORE_ICON,
  MENU_MORE_ACTIVE_ICON,
  WHITE_SORT,
  OWNER_ICON,
  USER_ICON,
  TOTAL_ICON,
  BG_MYPAGE,
  NEW_COPY_ICON,
  FAQ1,
  FAQ2,
  HERO,
  IETH,
  IMTM,
  ZENTAI,
  BG_GUIDE,
  STEP_1_1,
  STEP_1_2,
  STEP_2_1,
  STEP_2_2,
  STEP_4_1,
  STEP_5_1,
  STEP_6_1,
  CREDIT_STEP_1,
  CREDIT_STEP_2,
  CREDIT_STEP_3,
  TELEGRAM_ICON,
  BLACK_ETH_ICON,
  WHITE_ETH_ICON,
  BLUE_ETH_ICON,
  WHITE_EMAIL_ICON,
  COLOR_FACEBOOK_ICON,
  COLOR_GOOGLE_ICON,
  COLOR_DISCORD_ICON,
  ICON_UPLOAD_FILE,
  COUNTDOWN_ICON,
  SECTION_BG_1,
  GRAY_ADVANCED_ICON,
  BLUE_TIMER_ICON,
  GRAY_LOGOUT_ICON,
  GRAY_EDIT_ICON,
  GRAY_USER_ICON,
  ABOUT_US_ICON,
  ABOUT_US_BACKGROUND,
  ABOUT_US_BANNER_ICON_BACKGROUND,
  FAQS_ARROW_ICON,
  GRAY_EMAIL_ICON,
  GRAY_VIEW_ICON,
  GRAY_UNVIEW_ICON,
  GRAY_COPY_ICON,
  GRAY_AVATA_DEFAULT,
  GRAY_ARROW_DOWN_ICON,
  GRAY_ARROW_UP_ICON,
  GRAY_COUNTDOWN_ICON,
  NFT_MAKETPLACE,
  MATIC,
  WMATIC,
  USDT,
  SHARE_FACEBOOK_ICON,
  SHARE_TWITTER_ICON,
  SHARE_QR_ICON,
  SHARE_LINK_ICON,
  SHARE_DOWNLOAD_ICON,
  PRICE_ICON,
  SURFIX_ICON,
  BLUE_TRANSFER_ICON,
  STAKING_MONEY_ICON,
  STAKING_CURRENCY_ICON,
  STAKING_BG_TOP_ICON,
  GACHA_MODAL_IMAGE,
  GACHA_BACKGROUND_IMAGE,
  GACHA_LOGO_IMAGE,
  GACHA_QUEST_IMAGE,
  PAYMENT,
  SPIKE,
  PRODUCT_BG,
  ICON_EMAIL,
};
