import { Component } from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import { withTranslation, useTranslation } from 'react-i18next';
import numeral from 'numeral';
// import classnames from 'classnames'
import { Image } from 'antd';

import Page from '../../components/page';
import Container from '../../components/container';
import Typography from '../../components/typography';
import ProductCard from '../../components/product-card-new';
import FetchableList from '../../components/fetchable-list';
import Footer from '../../app/footer';
// import { Images } from '@/theme'

import { StyledDiv } from './styled';

@withTranslation()
@inject(stores => ({
  creatorsStore: stores.creators,
  productStore: stores.products,
}))
@observer
class CreatorDetail extends Component {
  static propTypes = {
    creatorsStore: PropTypes.object,
    productStore: PropTypes.object,
  };

  state = {
    creator: {},
    loading: true,
  };

  async componentDidMount() {
    const { creatorsStore, match, i18n } = this.props;
    const { id } = match.params;
    const res = await creatorsStore.getCreatorDetail({ creatorId: id, langKey: i18n.language.toUpperCase() });
    if (res.success) {
      this.setState({ creator: res.data, loading: false });
    }
  }

  _renderInfoBox = () => {
    const { t } = this.props;
    const { creator } = this.state;

    return (
      <div>
        <div className="creator-name">
          <h1 className="creator-name">{creator.name || 'Unknown'}</h1>
          <div className="item-count">
            <strong>
              {numeral(creator?.totalProduct || 0).format('0,0')} {t('idols:item_count')}
            </strong>
          </div>
          <div className="favorite-count">
            <strong>
              {numeral(creator?.totalWish || 0).format('0,0')} {t('idols:favorites')}
            </strong>
          </div>
        </div>
        <Typography className="creator-description" dangerouslySetInnerHTML={{ __html: creator.description }} />
      </div>
    );
  };

  _renderProductItem = item => {
    const { history } = this.props;

    return (
      <ProductCard
        onClick={() => history.push(`/product-details/${item?.id}`)}
        item={item}
        isShowCategoryName
        isShowTime
      />
    );
  };

  _renderListCreator = () => {
    const { productStore, i18n, match, t } = this.props;
    const { id } = match.params;

    return (
      <FetchableList
        className="list-top-product"
        ref={ref => {
          this._newProductFetchableList = ref;
        }}
        keyExtractor={(item, index) => index}
        noDataMessage={t('common:no_item')}
        action={productStore.getProductByCreatorId}
        items={productStore.creatorProducts.items}
        page={productStore.creatorProducts.page}
        total={productStore.creatorProducts.total}
        colSpanXs={24}
        colSpanMd={12}
        colSpanXl={6}
        limit={12}
        payload={{
          findType: 'NEW_PRODUCT',
          langKey: i18n.language.toUpperCase(),
          creatorId: id,
        }}
        renderItem={this._renderProductItem}
      />
    );
  };

  render() {
    const { creator, loading } = this.state;

    return (
      <Page
        onEndReached={() => {
          this._newProductFetchableList.loadMore();
        }}
      >
        <Container>
          <StyledDiv>
            <div className="top-header-box">
              <div className="creator-header-background" style={{ backgroundImage: `url(${creator.backgroundUrl})` }} />
              <div className="header-content">
                <div className="creator-header-avatar">
                  <Image preview={false} src={creator.imageUrl} />
                </div>
                {this._renderInfoBox()}
              </div>
            </div>
            <div className="creator-main-container">{!loading && this._renderListCreator()}</div>
          </StyledDiv>
        </Container>
        <Footer />
      </Page>
    );
  }
}

const CreatorDetailWrapper = props => {
  const { i18n } = useTranslation();

  return <CreatorDetail {...props} key={i18n.language} />;
};

export default CreatorDetailWrapper;
