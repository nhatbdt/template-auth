import styled from 'styled-components';
import { WHITE_50, WHITE_12, WHITE_40, WHITE_35 } from '../../theme/colors';
import Media from '../../utils/media';

const StyledDiv = styled.div`
  /* background-attachment: fixed;
  background-repeat: no-repeat;
  background-size: 100% auto;
  ${Media.lessThan(Media.SIZE.XXL)} {
    background-size: 100% auto;
    background-position: top center;
  }
  ${Media.lessThan(Media.SIZE.MD)} {
    padding-top: 60px;
    background-size: auto 100vw;
  } */
  .top-header-box {
    position: relative;
    .creator-header-background {
      position: relative;
      width: 100%;
      height: 340px;
      margin-top: 140px;
      border-radius: 20px;

      background-repeat: no-repeat;
      background-size: 100% auto;
      ${Media.lessThan(Media.SIZE.XXL)} {
        background-size: 100% auto;
        background-position: top center;
      }
      ${Media.lessThan(Media.SIZE.MD)} {
        padding-top: 60px;
        background-size: auto 100vw;
      }
      @media only screen and (max-width: 374px) {
        height: 345px;
      }
    }
    .creator-header-content {
      /* color: #000;
      background-color: lightblue; */
      /* position: absolute;
      bottom: -160px;
      left: 0; */
      width: 100%;
      height: 100px;
      display: flex;
      align-items: flex-end;
      z-index: 2;
      /* &:before {
        content: '';
        background: rgb(33,35,48);
        background: linear-gradient(0deg, rgba(33,35,48,0.6) 0%, rgba(33,35,48,0.4) 25%, rgba(33,35,48,0) 100%);
        width: 100%;
        position: absolute;
        height: 50%;
        bottom: 160px;
        z-index: 0;
      } */
      ${Media.lessThan(Media.SIZE.MD)} {
        position: relative;
        bottom: unset;
        /* &:before {
          background: rgb(33,35,48);
          background: linear-gradient(0deg, rgba(33,35,48,1) 0%, rgba(33,35,48,0.5) 25%, rgba(33,35,48,0) 100%);
          top: 0;
          left: 0;
          bottom: auto;
          position: fixed;
          z-index: 0;
          height: 100%;
        } */
        .creator-header-content-container {
          z-index: 1;
        }
      }
      .heading-title {
        font-size: 15px;
        margin-bottom: 16px;
        ${Media.lessThan(Media.SIZE.MD)} {
          font-size: 13px;
          margin-bottom: 13px;
        }
      }
      .creator-header-box {
        background-color: lightblue;
        width: 100%;
        /* display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        align-items: flex-end; */
        ${Media.lessThan(Media.SIZE.MD)} {
        }
        .creator-header-box-left {
          width: 25%;
          padding: 40px 30px 15px 30px;
          ${Media.lessThan(Media.SIZE.XXL)} {
            width: 35%;
            padding: 30px 20px 15px 20px;
          }
          ${Media.lessThan(Media.SIZE.XXL)} {
            width: 35%;
            padding: 30px 10px;
          }
          ${Media.lessThan(Media.SIZE.MD)} {
            width: 100%;
            padding: 15px 0;
          }
          ul {
            list-style: none;
            margin: 0;
            display: flex;
            padding: 0;
            li {
              width: calc(100% / 3);
              display: flex;
              flex-direction: column;
              align-items: center;
              padding: 15px 30px;
              color: #ffffff;
              ${Media.lessThan(Media.SIZE.XXL)} {
                padding: 15px;
              }
              ${Media.lessThan(Media.SIZE.MD)} {
                padding: 12px 6px;
              }
              strong {
                font-size: 19px;
                margin-bottom: 8px;
                ${Media.lessThan(Media.SIZE.MD)} {
                  font-size: 15px;
                }
              }
              span {
                font-size: 12px;
                color: ${WHITE_50};
                display: flex;
                align-items: center;
                text-transform: capitalize;
                img {
                  max-height: 16px;
                  margin-right: 8px;
                }
                ${Media.lessThan(Media.SIZE.MD)} {
                  font-size: 10px;
                  img {
                    margin-right: 6px;
                  }
                }
              }
              + li {
                border-left: 1px solid rgba(112, 112, 112, 0.24);
                ${Media.lessThan(Media.SIZE.MD)} {
                  border-color: ${WHITE_12};
                }
              }
            }
          }
        }
        .creator-header-box-right {
          width: 75%;
          padding: 40px 45px;
          border-radius: 4px;
          /* box-shadow: 0 0 5px rgba(0,0,0,0.04);
          background-color: rgba(32, 34, 45, 0.9); */
          z-index: 2;
          /* @supports ((-webkit-backdrop-filter: none) or (backdrop-filter: none)) {
            background-color: rgba(32, 34, 45, 0);
            backdrop-filter: blur(20px) brightness(0.6);
            -webkit-backdrop-filter: blur(20px) brightness(0.6);
          } */
          &::-webkit-scrollbar {
            width: 8px;
            height: 8px;
            border-radius: 20px;
          }

          ${Media.lessThan(Media.SIZE.XXL)} {
            width: 65%;
            padding: 30px 25px;
            max-height: 430px;
            overflow: auto;
          }
          ${Media.lessThan(Media.SIZE.MD)} {
            width: 100%;
            padding: 28px 17px;
            max-height: unset;
            overflow: visible;
          }
          .creator-info-box {
            display: flex;
            flex-wrap: wrap;
          }
          .creator-info-box-left {
            width: 100%;
            padding-right: 40px;
            ${Media.lessThan(Media.SIZE.XXL)} {
              padding-right: 25px;
            }
            ${Media.lessThan(Media.SIZE.MD)} {
              width: 100%;
              padding: 0;
              border-right: none;
            }
            .creator-info-heading {
              display: flex;
              justify-content: space-between;
              flex-wrap: wrap;
              margin-bottom: 50px;
              ${Media.lessThan(Media.SIZE.XL)} {
                margin-bottom: 20px;
              }
              .info-heading-name {
                width: calc(100% - 50px);
                ${Media.lessThan(Media.SIZE.MD)} {
                  width: 100%;
                  text-align: center;
                  display: flex;
                  flex-direction: column;
                  justify-content: center;
                  align-items: center;
                  margin-bottom: 14px;
                }
                .text-category {
                  margin-bottom: 15px;
                  letter-spacing: 1px;
                  ${Media.lessThan(Media.SIZE.MD)} {
                    margin-bottom: 5px;
                    font-size: 11px;
                  }
                }
                /* .creator-name {
                  font-size: 32px;
                  font-weight: bold;
                  margin-bottom: 5px;
                  overflow: hidden;
                  display: -webkit-box;
                  -webkit-line-clamp: 1;
                  -webkit-box-orient: vertical;
                  ${Media.lessThan(Media.SIZE.XL)} {
                    font-size: 24px;
                  }
                  ${Media.lessThan(Media.SIZE.MD)} {
                    font-size: 22px;
                  }
                } */
                .creator-subname {
                  text-transform: uppercase;
                  letter-spacing: 2px;
                  color: ${WHITE_40};
                  ${Media.lessThan(Media.SIZE.MD)} {
                    font-size: 10px;
                  }
                }
              }
              .info-heading-social {
                width: 50px;
                .social-box {
                  display: flex;
                  justify-content: flex-end;
                  img {
                    height: 16px;
                  }
                  .social-button + .social-button {
                    margin-left: 15px;
                  }
                }
                ${Media.lessThan(Media.SIZE.MD)} {
                  width: 100%;
                  .social-box {
                    display: flex;
                    justify-content: center;
                  }
                }
              }
            }
          }
          .creator-info-box-right {
            width: 34%;
            padding-left: 40px;
            ${Media.lessThan(Media.SIZE.XXL)} {
              padding-left: 25px;
            }
            ${Media.lessThan(Media.SIZE.MD)} {
              width: 100%;
              padding: 0;
            }
            .creator-detail-info {
              ul {
                list-style: none;
                padding: 0 0 16px 0;
                margin-bottom: 16px;
                border-bottom: 1px dashed ${WHITE_12};
                ${Media.lessThan(Media.SIZE.MD)} {
                  padding: 0 0 12px 0;
                  margin-bottom: 12px;
                }
                &:last-child {
                  margin-bottom: 0;
                  border-bottom: none;
                }
                li {
                  display: flex;
                  padding: 6px 0;
                  justify-content: space-between;
                  color: #ffffff;
                  font-size: 13px;
                  strong {
                    color: ${WHITE_35};
                    margin-right: 15px;
                  }
                  span {
                    text-align: right;
                  }
                  ${Media.lessThan(Media.SIZE.MD)} {
                    font-size: 12px;
                    padding: 4px 0;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  .header-content {
    display: flex;
    flex-wrap: wrap;
    min-height: 100px;
    width: 100%;

    .creator-description {
      color: #282828;
    }
    .creator-header-avatar {
      z-index: 2;
      margin-left: 60px;
      margin-top: -100px;
      background-color: #fff;
      width: 200px;
      height: 200px;
      padding: 4px;
      border-radius: 50%;
      border: 5px solid #fff;
      overflow: hidden;
      img {
        border-radius: 50%;
      }
      .ant-image {
        height: 100%;
        .ant-image-img {
          height: 100%;
          object-fit: cover;
        }
      }
    }
    .creator-name {
      display: flex;
      flex-wrap: wrap;
      justify-content: flex-start;
      align-items: center;
      gap: 6px;
      color: #282828;
      .item-count,
      .favorite-count {
        color: #045afc;
        padding-left: 6px;
        border-left: 1px solid #65676b;
      }
    }
  }
  .creator-main-container {
    /* background: rgba(33, 35, 48, 0.9); */
    margin-top: 0;
    padding-top: 60px;
    padding-bottom: 100px;
    /* position: relative; */
    z-index: 1;
    .section-title {
      margin-bottom: 30px;
    }
    ${Media.lessThan(Media.SIZE.MD)} {
      background: rgba(33, 35, 48, 1);
      padding-top: 20px;
      padding-bottom: 60px;
      z-index: 2;
      .section-title {
        margin-bottom: 12px;
        margin-top: 20px;
      }
    }

    .list-top-product {
      padding: 20px;
    }
  }
`;

export { StyledDiv };
