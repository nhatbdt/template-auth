import { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation, useTranslation } from 'react-i18next';
import QS from 'query-string';
// import { Pagination } from 'antd';

import Page from '../../components/page';
import FetchableList from '../../components/fetchable-list';
import Footer from '../../app/footer';
import Media from '../../utils/media';
import ProductCard from '../../components/product-card-new';
import CategoryInfo from './category-info';
import { Images } from '../../theme';

const StyledDiv = styled.div`
  width: 100%;
  margin: 0 auto;
  position: relative;
  ${Media.lessThan(Media.SIZE.MD)} {
    padding: 0 15px;
  }
  .list-items {
    padding: 38px 0px 25px;
    /* max-width: 1400px; */
    max-width: 100%;
    margin: 0 auto;
  }
  .button-scroll {
    position: fixed;
    bottom: calc(100vh - 370px);
    right: 40px;
    background: red;
    z-index: 11111;
    width: 32px;
    height: 32px;
    background: #376587;
    border-radius: 81px;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .category-detail-wrap {
    /* padding-left: 60px;
    padding-right: 60px; */
    max-width: 1540px;
    margin: 0 auto;
    padding: 40px;
    ${Media.lessThan(Media.SIZE.MD)} {
      padding: 15px;
    }

    .pagination {
      margin-bottom: 45px;

      .ant-pagination-prev,
      .ant-pagination-next {
        display: none;
      }

      .ant-pagination-item {
        width: 44px;
        height: 44px;
        border-radius: 100%;
        background: #ffffff;
        border: 1px solid #e5e7eb;

        a {
          height: 44px;
          width: 44px;
          display: flex;
          justify-content: center;
          align-items: center;
          font-size: 16px;
          color: #282828;
        }
      }

      .ant-pagination-item-active {
        background: #045afc;
        border-color: #045afc;

        a {
          color: #ffffff;
        }
      }
    }
  }
`;

@withTranslation()
@withRouter
@inject(stores => ({
  productsStore: stores.products,
}))
@observer
class CategoryDetails extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
  };

  state = {
    isShowButtonScroll: false,
  };

  _renderProductItem = (item, index) => {
    const { history } = this.props;

    return (
      <ProductCard
        onClick={() => history.push(`/product-details/${item.id}`)}
        item={item}
        index={index}
        className="product-card"
        styleType="black"
      />
    );
  };

  onScrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  onScroll = offset => {
    // const { isShowButtonScroll } = this.state
    // if (offset <= 0 && !isShowButtonScroll) {
    //   this.setState({ isShowButtonScroll: !this.state.isShowButtonScroll })
    // }
    // if (offset > 0 && isShowButtonScroll) {
    //   this.setState({ isShowButtonScroll: false })
    // }
  };

  paramsToObject = entries => {
    const result = {};
    for (const [key, value] of entries) {
      // each 'entry' is a [key, value] tupple
      result[key] = value;
    }
    return result;
  };

  objectToString = obj => {
    let str = [];
    for (let p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + '=' + encodeURIComponent(obj[p]));
      }
    return str.join('&');
  };

  render() {
    const { productsStore, match, location, t, i18n } = this.props;
    const { isShowButtonScroll } = this.state;
    const query = QS.parse(location.search).query || '';
    const { id } = match.params;
    const guster = [
      { xs: 10, sm: 15, md: 20, xl: 20 },
      { xs: 24, sm: 15, md: 20, xl: 20 },
    ];

    // const queryObj = this.paramsToObject(new URLSearchParams(location.search));

    return (
      <Page
        onEndReached={() => {
          this._fetchableList.loadMore();
        }}
        // onScroll={() => {}}
      >
        <StyledDiv>
          <div className="category-main-container category-detail-wrap">
            <CategoryInfo t={t} />
            <div className="list-items">
              <FetchableList
                className="list-top-product"
                ref={ref => {
                  this._fetchableList = ref;
                }}
                keyExtractor={(item, index) => index}
                noDataMessage={t('common:no_item')}
                action={productsStore.getProductItems}
                items={productsStore.productItems.items}
                page={productsStore.productItems.page}
                total={productsStore.productItems.total}
                colSpanXl={6}
                colSpanLg={8}
                colSpanMd={8}
                colSpanSm={24}
                colSpanXs={24}
                limit={12}
                gutter={guster}
                payload={{
                  query,
                  findType: 'NEW_PRODUCT',
                  langKey: i18n.language.toUpperCase(),
                  sortBy: 'LAST_MODIFIED',
                  categoryId: id,
                }}
                renderItem={this._renderProductItem}
                showLoadMoreButton
              />
            </div>

            {/* {productsStore.productItems.items.length < productsStore.productItems.total && (
              <div className="pagination">
                <Pagination
                  onChange={(page, pageSize) => {
                    const { match, history } = this.props;

                    const str = this.objectToString({
                      ...queryObj,
                      page: page,
                    });

                    history.push(`/category/${match.params.id}?${str}`);
                  }}
                  total={productsStore.productItems.total}
                  responsive
                  pageSize={16}
                  current={parseInt(queryObj.page) || 1}
                />
              </div>
            )} */}
          </div>
          {isShowButtonScroll && (
            <Link className="button-scroll" onClick={this.onScrollTop}>
              <img src={Images.WHITE_CHEVRON_UP_ICON} alt="scroll top" />
            </Link>
          )}
        </StyledDiv>
        <Footer />
      </Page>
    );
  }
}

const CategoryDetailsWrapper = props => {
  const { i18n } = useTranslation();

  return <CategoryDetails {...props} key={i18n.language} />;
};

export default CategoryDetailsWrapper;
