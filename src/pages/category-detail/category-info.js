import { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';
import numeral from 'numeral';

import ARROW_ICON from '../../resources/images/category_arrow.svg';
import Media from '../../utils/media';
import { Colors } from '../../theme';
import classNames from 'classnames';

const StyledDiv = styled.div`
  .container {
    width: 100%;
    max-width: 1540px;
    /* max-width: 1320px; */
    padding: 0 40px;
    margin: 0 auto;
    ${Media.lessThan(Media.SIZE.MD)} {
      padding: 0 15px;
    }
  }
  .category-info-section {
    height: 340px;
    background-position: top;
    background-repeat: no-repeat;
    background-size: cover;
    margin-top: 82px;
    border-radius: 20px;

    ${Media.lessThan(Media.SIZE.LG)} {
      margin-top: 52px;
    }
    ${Media.lessThan(Media.SIZE.MD)} {
      margin-top: 65px;
      min-height: 450px;
    }
    .category-info-section-inner {
      height: 100%;
      padding-bottom: 5vw;
      background: rgba(17, 17, 17, 0.2);
      background: linear-gradient(0deg, #0c2338 0%, rgba(12, 35, 56, 0) 100%);
      display: flex;
      width: 100%;
      align-items: flex-end;
      ${Media.lessThan(Media.SIZE.MD)} {
        padding-bottom: 0;
      }
      .category-info-container {
        max-width: 1400px;
        padding-left: 20px;
        padding-right: 20px;
      }
    }
    .category-info-box {
      width: 100%;
      max-width: 600px;
      display: flex;
      align-items: center;
      ${Media.lessThan(Media.SIZE.MD)} {
        text-align: center;
        display: flex;
        flex-direction: column;
        width: 100%;
        align-items: center;
        margin: 0 auto;
      }
      .category-statistical {
        // flex: 1;
        ul {
          display: flex;
          list-style: none;
          padding: 0;
          margin: 0;
          justify-content: space-between;
          width: 100%;
          li {
            padding: 0 15px;
            /* width: calc(100% / 3); */
            text-align: center;
            border-right: 1px solid rgba(255, 255, 255, 0.15);
            display: flex;
            flex-direction: column;
            color: rgba(255, 255, 255, 0.5);
            white-space: nowrap;
            &:first-child {
              border-left: 1px solid rgba(255, 255, 255, 0.15);
            }

            span {
              text-transform: capitalize;
              color: #cbd7e8;
              &:first-child {
                font-weight: bold;
              }
            }
          }
        }
        ${Media.lessThan(Media.SIZE.MD)} {
          margin-bottom: 8px;
          order: 4;
          width: 100%;
          padding: 0 10px;
          ul {
            li {
              font-size: 10px;
            }
          }
        }
        &.category-statistical-custom {
          ul {
            li {
              // padding: 0 30px;
            }
          }
        }
      }
      .category-name {
        color: #ffffff;
        font-weight: bold;
        margin-right: 30px;
        ${Media.lessThan(Media.SIZE.MD)} {
          font-size: 14px;
          order: 2;
        }
      }
      .category-description {
        color: #ffffff;
        font-size: 13px;
        max-height: 150px;
        overflow: auto;
        max-width: 600px;
        ${Media.lessThan(Media.SIZE.XL)} {
          max-height: 120px;
        }
        ${Media.lessThan(Media.SIZE.MD)} {
          font-size: 10px;
          order: 3;
          margin-bottom: 15px;
        }
      }
    }
  }

  .category-description {
    margin-top: 15px;
    font-size: 14px;
    font-weight: 400;
    line-height: 20px;
    max-width: 600px;
  }

  /* .category-main-container {
    padding: 20px 0;
    &.fighter {
      background: #000000;
    }
    .category-tab-content {
      padding: 40px 0;
    }
    .list-creator {
      margin: 0 -20px;
      display: flex;
      flex-wrap: wrap;
      .creator-col {
        padding: 0 20px;
        width: 20%;
        margin-bottom: 40px;
        .creator-card {
          .creator-content {
            color: #ffffff;
            .creator-statistical ul li {
              border-color: rgba(255, 255, 255, 0.15);
            }
          }
        }
      }
      ${Media.lessThan(Media.SIZE.XL)} {
        margin: 0 -15px;
        .creator-col {
          padding: 0 7.5px;
          width: 25%;
        }
      }
      ${Media.lessThan(Media.SIZE.MD)} {
        margin: 0 -10px;
        .creator-col {
          padding: 0 5px;
          width: 50%;
        }
      }
    }
    &.light {
      background: #ffffff;
    }
  } */

  .category-infor {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-top: 20px;

    .wrap-title {
      display: flex;
      align-items: center;
      height: 36px;
      /* line-height: 36px; */
      ${Media.lessThan(Media.SIZE.MD)} {
        flex-direction: column;
        align-items: flex-start;
      }

      .category-name {
        color: ${Colors.TEXT};
        font-weight: bold;
        font-size: 36px;
        /* line-height: 36px; */
        margin: 0;
        ${Media.lessThan(Media.SIZE.LG)} {
          font-size: 26px;
        }
        ${Media.lessThan(Media.SIZE.MD)} {
          font-size: 20px;
        }
        ${Media.lessThan(Media.SIZE.XS)} {
          font-size: 16px;
        }
      }

      .slash {
        width: 1px;
        height: 30px;
        background: #65676b;
        margin: 0 17px;
        ${Media.lessThan(Media.SIZE.MD)} {
          display: none;
        }
      }

      .count {
        color: #045afc;
        font-weight: 500;
        font-size: 26px;
        text-transform: capitalize;
        ${Media.lessThan(Media.SIZE.LG)} {
          font-size: 20px;
        }
        ${Media.lessThan(Media.SIZE.MD)} {
          font-size: 16px;
        }
        ${Media.lessThan(Media.SIZE.XS)} {
          font-size: 14px;
        }
      }
    }

    .wrap-pagination {
      display: flex;
      justify-content: center;
      align-items: center;

      .arrow-icon {
        height: 40px;
        width: 40px;
        border: 2px solid #e5e7eb;
        display: flex;
        justify-content: center;
        align-items: center;
        border-radius: 100%;
        cursor: pointer;
      }

      .arrow-left {
        transform: rotate(180deg);
        margin-right: 10px;
      }

      .disable {
        border: none;
        cursor: default;
      }
    }
  }
`;

@withTranslation()
@withRouter
@inject(stores => ({
  categoriesStore: stores.categories,
}))
@observer
class CategoryInfo extends Component {
  static propTypes = {
    categoriesStore: PropTypes.object,
  };

  state = {
    initial: true,
    categoryDetailInfo: {},
    categoryIds: [],
  };

  componentDidMount() {
    this._fetchData();
  }

  _fetchData = async () => {
    const { categoriesStore, match, i18n, history } = this.props;
    const id = match.params.id;
    const { success, data } = await categoriesStore.getCategoryDetail({
      // id: match.params.id,
      id: 'all',
      langKey: i18n.language.toUpperCase(),
      limit: 100,
    });

    if (success) {
      const categoryDetail = data?.result?.find(item => item.id?.toString() === id);
      const ids = data?.result?.map(item => item.id);
      if (data?.total > 0 && ids?.length > 0 && !categoryDetail) {
        history.push(`/category/${ids[0]}`);
      } else {
        this.setState({
          categoryIds: ids,
          categoryDetailInfo: categoryDetail,
          initial: false,
        });
      }
    } else {
      this.setState({
        categoryIds: [],
        categoryDetailInfo: {},
        initial: false,
      });
    }
  };

  render() {
    const { t, history } = this.props;
    const { categoryDetailInfo, categoryIds } = this.state;
    const html = categoryDetailInfo?.description || '';
    const div = document.createElement('div');
    div.innerHTML = html;
    // const categoryDescription = div.textContent || div.innerText || ""
    // console.log(categoryDetailInfo?.name);

    return (
      <StyledDiv>
        <section
          className="category-info-section"
          style={{
            backgroundImage: `url('${categoryDetailInfo.headerUrl}')`, // || Images.CULTURE_BACKGROUND
          }}
        />

        <div className="category-infor">
          {categoryDetailInfo?.name && (
            <div className="wrap-title">
              <h1 className="category-name bold">{categoryDetailInfo?.name}</h1>

              <div className="slash" />

              <div className="count">
                {numeral(categoryDetailInfo?.productCount).format('0,0')}
                &nbsp;
                {categoryDetailInfo?.productCount > 1 ? t('common:creator.items') : t('common:creator.item')}
              </div>
            </div>
          )}

          <div className="wrap-pagination">
            <div
              className={classNames('arrow-icon', 'arrow-left', {
                disable: categoryIds?.indexOf(categoryDetailInfo?.id) < 1,
              })}
              onClick={() => {
                const index = categoryIds.indexOf(categoryDetailInfo?.id) - 1;
                if (index >= 0) {
                  const hadId = categoryIds[index];
                  history.push(`/category/${hadId}`);
                }
              }}
            >
              <img src={ARROW_ICON} alt="arrow-icon" />
            </div>
            <div
              className={classNames('arrow-icon', 'arrow-right', {
                disable: categoryIds?.indexOf(categoryDetailInfo?.id) >= categoryIds?.length - 1,
              })}
              onClick={() => {
                const index = categoryIds.indexOf(categoryDetailInfo?.id) + 1;
                if (index <= categoryIds?.length - 1) {
                  const hadId = categoryIds[index];
                  history.push(`/category/${hadId}`);
                }
              }}
            >
              <img src={ARROW_ICON} alt="arrow-icon" />
            </div>
          </div>
        </div>
      </StyledDiv>
    );
  }
}

export default CategoryInfo;
