import React from 'react';
import styled from 'styled-components';
import Page from '../../components/page';
import { Col, Layout, Row } from 'antd';
import Media from '../../utils/media';
import Footer from '../../app/footer';
// import { Images } from '../../theme';
import { useTranslation } from 'react-i18next';

export default function PrivacyAgreement() {
  const { t, i18n } = useTranslation('common');
  return (
    <Page>
      <PrivacyAgreementStyled>
        <PrivacyAgreementRow className="top">
          <ColTopStyled className="text-center" span={24}>
            <p>{t('privacy_agreement.title')}</p>
          </ColTopStyled>

          <ColTopStyled span={24} className="content">
            <div dangerouslySetInnerHTML={{ __html: i18n.language.toUpperCase() === 'JA' ? ContentJA : ContentEN }} />
          </ColTopStyled>
        </PrivacyAgreementRow>
      </PrivacyAgreementStyled>

      <Footer />
    </Page>
  );
}

export const PrivacyAgreementStyled = styled(Layout)`
  &.ant-layout {
    font-family: sans-serif;
    margin: 0px auto;
    margin-top: 92px;
    background: #0f1114;
    color: #ffffff;
    height: 100%;
    max-width: 1540px;
  }

  ${Media.lessThan(Media.SIZE.SM)} {
    &.ant-layout {
      margin-top: 60px;
      width: 100%;
      padding: 0 10px;
      padding-bottom: 80px;
    }
  }
`;

export const ColTopStyled = styled(Col)`
  &.text-center {
    padding-top: 30px;
    padding-bottom: 30px;
    border-bottom: 1px solid rgba(41, 45, 51, 0.5);

    p {
      font-style: normal;
      font-weight: 700;
      line-height: 64px;
      color: #ffffff;
      font-size: 26px;
      line-height: 39px;
    }
  }

  &.content {
    padding-top: 20px;
    padding-bottom: 30px;
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;
    color: #a8aeba;
    font-family: sans-serif;

    div {
      font-weight: 400;
      font-size: 14px;
      line-height: 20px;
      color: #a8aeba;
    }
  }

  ${Media.lessThan(Media.SIZE.SM)} {
    &.left {
      width: 100%;

      .bottom {
        width: 100%;
      }
    }
  }
`;

export const PrivacyAgreementRow = styled(Row)`
  &.top {
    width: calc(1440px - 250px + 60px);
    margin: 0px auto;
  }

  ${Media.lessThan(Media.SIZE.SM)} {
    &.top {
      width: 100%;
      padding: 0;
    }
  }
`;

const ContentEN = `
Privacy Policy
<br /><br />
Angela Global Co., Ltd. PRIVACY POLICY
<br /><br />
Angela Global Co., Ltd. (the “Company”) is committed to maintaining robust privacy protections for its users. Our Privacy Policy (“Privacy Policy”) is designed to help you understand how we collect, use, and safeguard the information you provide to us and to assist you in making informed decisions when using our Service.
<br /><br />
For purposes of this Agreement, “Site” refers to the Company’s website, which can be accessed at https://www. NFT-mkt.io/.
“Service” refers to the Company’s services accessed via the Site, in which users can purchase NFTs.
The terms “we,” “us,” and “our” refer to the Company.
“You” refers to you, as a user of our Site or our Service.
By accessing our Site or our Service, you accept our Privacy Policy and Terms of Use, and you consent to our collection, storage, use and disclosure of your Personal Information as described in this Privacy Policy.
<br /><br />
I. INFORMATION WE COLLECT
<br /><br />
We collect “Non-Personal Information” and “Personal Information.” Non-Personal Information includes information that cannot be used to personally identify you, such as anonymous usage data, general demographic information we may collect, referring/exit pages and URLs, platform types, preferences you submit and preferences that are generated based on the data you submit and number of clicks. Personal Information includes your email, which you submit to us through the registration process at the Site.
<br /><br />
1. Information collected via Technology
<br /><br />
To activate the Service, you do not need to submit any Personal Information other than your email address. To use the Service thereafter, you need to submit further Personal Information. However, in an effort to improve the quality of the Service, we track information provided to us by your browser or by our software application when you view or use the Service, such as the website you came from (known as the “referring URL”), the type of browser you use, the device from which you connected to the Service, the time and date of access, and other information that does not personally identify you. We track this information using cookies, or small text files which include an anonymous unique identifier. Cookies are sent to a user’s browser from our servers and are stored on the user’s computer hard drive. Sending a cookie to a user’s browser enables us to collect non-Personal information about that user and keep a record of the user’s preferences when utilizing our services, both on an individual and aggregate basis. For example, the Company may use cookies to collect the following information:
The Company may use both persistent and session cookies; persistent cookies remain on your computer after you close your session and until you delete them, while session cookies expire when you close your browser.
<br /><br />
2. Information you provide us by registering for an account
<br /><br />
In addition to the information provided automatically by your browser when you visit the Site, to become a subscriber to the Service you will need to create a personal profile. You can create a profile by registering with the Service and entering your email address and creating a username and a password. By registering, you are authorizing us to collect, store and use your email address in accordance with this Privacy Policy.
<br /><br />
3. Minor’s Privacy
<br /><br />
The Site and the Service are not directed to anyone under the age of 18. The Site does not knowingly collect or solicit information from anyone under the age of 18 or allow anyone under the age of 18 to sign up for the Service. If we learn that we have gathered personal information from anyone under the age of 18 without the consent of a parent or guardian, we will delete that information as soon as possible.
<br /><br />
II. HOW WE USE AND SHARE INFORMATION
<br /><br />
Personal Information:
<br /><br />
Except as otherwise stated in this Privacy Policy, we do not sell, trade, rent or otherwise share for marketing purposes your Personal Information with third parties without your consent. We do share Personal Information with vendors who are performing services for the Company, such as the servers for our email communications who are provided access to user’s email address for purposes of sending emails from us. Those vendors use your Personal Information only at our direction and in accordance with our Privacy Policy.
<br /><br />
In general, the Personal Information you provide to us is used to help us communicate with you. For example, we use Personal Information to contact users in response to questions, solicit feedback from users, provide technical support, and inform users about promotional offers.
<br /><br />
We may share Personal Information with outside parties if we have a good-faith belief that access, use, preservation or disclosure of the information is reasonably necessary to meet any applicable legal process or enforceable governmental request; to enforce applicable Terms of Service, including investigation of potential violations; address fraud, security or technical concerns; or to protect against harm to the rights, property, or safety of our users or the public as required or permitted by law.
<br /><br />
Non-Personal Information
<br /><br />
<br /><br />
In general, we use Non-Personal Information to help us improve the Service and customize the user experience. We also aggregate Non-Personal Information to track trends and analyze use patterns on the Site. This Privacy Policy does not limit in any way our use or disclosure of Non-Personal Information and we reserve the right to use and disclose such Non-Personal Information to our partners, advertisers and other third parties at our discretion.
<br /><br />
In the event we undergo a business transaction such as a merger, acquisition by another company, or sale of all or a portion of our assets, your Personal Information may be among the assets transferred. You acknowledge and consent that such transfers may occur and are permitted by this Privacy Policy, and that any acquirer of our assets may continue to process your Personal Information as set forth in this Privacy Policy. If our information practices change at any time in the future, we will post the policy changes to the Site so that you may opt out of the new information practices. We suggest that you check the Site periodically if you are concerned about how your information is used.
<br /><br />
III. HOW WE PROTECT INFORMATION
<br /><br />
We implement security measures designed to protect your information from unauthorized access. Your account is protected by your account password, and we urge you to take steps to keep your personal information safe by not disclosing your password and by logging out of your account after each use. We further protect your information from potential security breaches by implementing certain technological security measures including encryption, firewalls, and secure socket layer technology. However, these measures do not guarantee that your information will not be accessed, disclosed, altered, or destroyed by breach of such firewalls and secure server software. By using our Service, you acknowledge that you understand and agree to assume these risks.

<br /><br />
IV. YOUR RIGHTS REGARDING THE USE OF YOUR PERSONAL INFORMATION
<br /><br />
You have the right at any time to prevent us from contacting you for marketing purposes. When we send a promotional communication to a user, the user can opt out of further promotional communications by following the unsubscribe instructions provided in each promotional e-mail. You can also indicate that you do not wish to receive marketing communications from us in the Site. Please note that notwithstanding the promotional preferences you indicate by either unsubscribing or opting out in the Site, we may continue to send you administrative emails including, for example, periodic updates to our Privacy Policy.
<br /><br />
V. LINKS TO OTHER WEBSITES
<br /><br />
As part of the Service, we may provide links to or compatibility with other websites or applications. However, we are not responsible for the privacy practices employed by those websites or the information or content they contain. This Privacy Policy applies solely to information collected by us through the Site and the Service. Therefore, this Privacy Policy does not apply to your use of a third-party website accessed by selecting a link on our Site or via our Service. To the extent that you access or use the Service through or on another website or application, then the privacy policy of that other website or application will apply to your access or use of that site or application. We encourage our users to read the privacy statements of other websites before proceeding to use them.
<br /><br />

<br /><br />
VI. CHANGES TO OUR PRIVACY POLICY
<br /><br />
The Company reserves the right to change this policy and our Terms of Service at any time. We will notify you of significant changes to our Privacy Policy by sending a notice to the primary email address specified in your account or by placing a prominent notice on our site. Significant changes will go into effect 30 days following such notification. Non-material changes or clarifications will take effect immediately. You should periodically check the Site and this privacy page for updates.
<br /><br />

<br /><br />
VII. CONTACT US
<br /><br />
If you have any questions regarding this Privacy Policy or the practices of this Site, please contact us by sending an email to info@ NFT.io.
<br /><br />
Last Updated: This Privacy Policy was last updated on April 11, 2022.
`;

const ContentJA = `
プライバシーポリシー
<br /><br />
株式会社アンジェラグローバル プライバシーポリシー
<br /><br />
Angela Global Co., Ltd. (以下「当社」) は、ユーザーの堅牢なプライバシー保護を維持することに尽力しています。 当社のプライバシー ポリシー (「プライバシー ポリシー」) は、お客様が当社に提供した情報を当社がどのように収集、使用、保護するかを理解し、当社のサービスを使用する際に十分な情報に基づいた意思決定ができるよう支援することを目的としています。
<br /><br />
本契約の目的上、「サイト」とは、https://www からアクセスできる当社の Web サイトを指します。 NFT-mkt.io/.
「本サービス」とは、ユーザーが本サイトを通じてNFTを購入できる当社のサービスをいいます。
「当社」、「私たち」、および「当社の」という用語は当社を指します。
「あなた」とは、当社のサイトまたは当社のサービスのユーザーとしてのあなたを指します。
当社のサイトまたは当社のサービスにアクセスすることにより、お客様は当社のプライバシー ポリシーおよび利用規約に同意し、本プライバシー ポリシーに記載されている個人情報の収集、保管、使用および開示に同意したものとみなされます。
<br /><br />
I. 当社が収集する情報
<br /><br />
当社は「非個人情報」と「個人情報」を収集します。 非個人情報には、匿名の使用状況データ、当社が収集する一般的な人口統計情報、参照/終了ページおよび URL、プラットフォームの種類、お客様が送信した設定およびデータに基づいて生成された設定など、お客様を個人的に特定するために使用できない情報が含まれます。 送信した内容とクリック数。 個人情報には、サイトの登録プロセスを通じてお客様が当社に送信した電子メールが含まれます。
<br /><br />
1. テクノロジーを介して収集される情報
<br /><br />
サービスを有効にするために、電子メール アドレス以外の個人情報を送信する必要はありません。 その後サービスを使用するには、さらに個人情報を送信する必要があります。 ただし、サービスの品質を向上させるため、当社は、お客様が本サービスを閲覧または使用する際に、お客様のブラウザまたは当社のソフトウェア アプリケーションによって当社に提供される情報（アクセス元の Web サイト（「参照 URL」と呼ばれます）など）を追跡します。 ）、使用するブラウザの種類、本サービスに接続したデバイス、アクセス日時、その他個人を特定しない情報。 当社は、Cookie、または匿名の一意の識別子を含む小さなテキスト ファイルを使用してこの情報を追跡します。 Cookie は当社のサーバーからユーザーのブラウザに送信され、ユーザーのコンピュータのハードドライブに保存されます。 ユーザーのブラウザに Cookie を送信することにより、当社はそのユーザーに関する非個人情報を収集し、当社のサービスを利用する際のユーザーの好みの記録を個別および集合ベースで保存することができます。 たとえば、当社は Cookie を使用して次の情報を収集する場合があります。
当社は永続的な Cookie とセッション Cookie の両方を使用する場合があります。 セッションを閉じた後も永続的な Cookie は削除するまでコンピュータ上に残りますが、セッション Cookie はブラウザを閉じると期限切れになります。
<br /><br />
2. アカウント登録によりお客様が当社に提供する情報
<br /><br />
サイトにアクセスしたときにブラウザによって自動的に提供される情報に加えて、サービスの購読者になるには、個人プロフィールを作成する必要があります。 サービスに登録し、電子メール アドレスを入力し、ユーザー名とパスワードを作成することで、プロファイルを作成できます。 登録することにより、お客様は、本プライバシー ポリシーに従って当社がお客様の電子メール アドレスを収集、保存、使用することを承認したものとみなされます。
<br /><br />
3. 未成年者のプライバシー
<br /><br />
本サイトおよび本サービスは、18 歳未満の者を対象としたものではありません。本サイトは、18 歳未満の者に対して故意に情報を収集または要求したり、18 歳未満の者が本サービスにサインアップすることを許可したりするものではありません。 当社が親または保護者の同意なしに 18 歳未満の者から個人情報を収集したことが判明した場合、当社はその情報をできるだけ早く削除します。
<br /><br />
II. 情報の使用および共有方法
<br /><br />
個人情報：
<br /><br />
このプライバシーポリシーに別段の記載がある場合を除き、当社は、お客様の同意なしに、マーケティング目的でお客様の個人情報を販売、取引、賃貸、またはその他の方法で第三者と共有することはありません。 当社は、当社から電子メールを送信する目的でユーザーの電子メール アドレスへのアクセスを提供される当社の電子メール通信用サーバーなど、会社のためにサービスを実行するベンダーと個人情報を共有します。 これらのベンダーは、当社の指示があり、当社のプライバシー ポリシーに従ってのみお客様の個人情報を使用します。
<br /><br />
一般に、お客様が当社に提供した個人情報は、お客様とのコミュニケーションを支援するために使用されます。 たとえば、当社は個人情報を使用して、質問に応じてユーザーに連絡したり、ユーザーからのフィードバックを求めたり、技術サポートを提供したり、プロモーション特典についてユーザーに通知したりします。
<br /><br />
適用される法的手続きまたは法的強制力のある政府の要請に応じるために、情報へのアクセス、使用、保存、または開示が合理的に必要であると誠意を持って判断した場合、当社は個人情報を外部の当事者と共有することがあります。 潜在的な違反の調査を含む、該当する利用規約を施行するため。 詐欺、セキュリティ、または技術的な懸念に対処する。 または、法律で要求または許可されている場合に、ユーザーまたは公衆の権利、財産、または安全への危害から保護するため。
<br /><br />
非個人情報
<br /><br />
<br /><br />
一般に、当社はサービスを改善し、ユーザー エクスペリエンスをカスタマイズするために非個人情報を使用します。 また、傾向を追跡し、サイト上の使用パターンを分析するために、非個人情報も集約します。 このプライバシー ポリシーは、当社による非個人情報の使用または開示をいかなる形でも制限するものではなく、当社はその裁量により、かかる非個人情報を当社のパートナー、広告主およびその他の第三者に使用および開示する権利を留保します。
<br /><br />
当社が合併、他社による買収、資産の全部または一部の売却などの商取引を行った場合、譲渡される資産の中にお客様の個人情報が含まれる可能性があります。 お客様は、そのような転送が発生する可能性があり、本プライバシー ポリシーによって許可されていること、および当社資産の取得者が本プライバシー ポリシーに規定されているとおりにお客様の個人情報を引き続き処理する可能性があることを認識し、これに同意するものとします。 将来的に当社の情報慣行が変更された場合は、お客様が新しい情報慣行をオプトアウトできるように、ポリシーの変更をサイトに掲載します。 自分の情報がどのように使用されるかについて懸念がある場合は、定期的にサイトを確認することをお勧めします。
<br /><br />
Ⅲ． 情報の保護方法
<br /><br />
当社は、お客様の情報を不正アクセスから保護するために設計されたセキュリティ対策を実施しています。 あなたのアカウントはアカウントのパスワードによって保護されており、パスワードを公開せず、使用後は必ずアカウントからログアウトすることで、個人情報を安全に保つための措置を講じることをお勧めします。 当社は、暗号化、ファイアウォール、セキュア ソケット レイヤー テクノロジーなどの特定の技術的セキュリティ対策を実装することにより、潜在的なセキュリティ侵害からお客様の情報をさらに保護します。 ただし、これらの措置は、ファイアウォールや安全なサーバー ソフトウェアの侵害によってお客様の情報がアクセス、開示、変更、または破壊されないことを保証するものではありません。 当社のサービスを使用することにより、お客様はこれらのリスクを理解し、引き受けることに同意するものとします。
<br /><br />
IV. 個人情報の使用に関するあなたの権利
<br /><br />
あなたには、当社がマーケティング目的であなたに連絡することをいつでも阻止する権利があります。 当社がユーザーにプロモーション通信を送信する場合、ユーザーは各プロモーション電子メールに記載されている配信停止の手順に従うことで、それ以降のプロモーション通信をオプトアウトできます。 また、サイト内で当社からのマーケティング情報の受信を希望しないことを示すこともできます。 サイト内で購読解除またはオプトアウトすることでお客様がプロモーション設定を行っているにもかかわらず、当社はプライバシー ポリシーの定期的な更新などの管理電子メールを引き続きお客様に送信する場合があることにご注意ください。
<br /><br />
V. 他のウェブサイトへのリンク
<br /><br />
サービスの一部として、当社は他の Web サイトやアプリケーションへのリンク、またはそれらとの互換性を提供する場合があります。 ただし、当社は、それらの Web サイトで採用されているプライバシー慣行、またはそこに含まれる情報やコンテンツについては責任を負いません。 このプライバシー ポリシーは、サイトおよびサービスを通じて当社が収集した情報にのみ適用されます。 したがって、このプライバシー ポリシーは、当社のサイト上のリンクを選択することによって、または当社のサービスを介してアクセスされるサードパーティの Web サイトの使用には適用されません。 別の Web サイトまたはアプリケーションを通じて、またはその上で本サービスにアクセスまたは使用する場合、そのサイトまたはアプリケーションへのアクセスまたは使用には、その他の Web サイトまたはアプリケーションのプライバシー ポリシーが適用されます。 他の Web サイトを使用する前に、その Web サイトのプライバシーに関する声明を読むことをお勧めします。
<br /><br />
VI. プライバシーポリシーの変更
<br /><br />
当社は、本ポリシーおよび利用規約をいつでも変更する権利を留保します。 当社は、プライバシー ポリシーの重要な変更について、お客様のアカウントで指定された主要な電子メール アドレスに通知を送信するか、当社のサイトに目立つ通知を掲載することによってお客様に通知します。 重要な変更は、通知から 30 日後に有効になります。 重要でない変更または明確化は直ちに有効になります。 サイトとこのプライバシー ページの更新情報を定期的に確認する必要があります。
<br /><br />
VII. お問い合わせ
<br /><br />
このプライバシー ポリシーまたはこのサイトの慣行に関してご質問がある場合は、info@ NFT.io に電子メールを送信してご連絡ください。
<br /><br />
最終更新日: このプライバシー ポリシーの最終更新日は 2022 年 4 月 11 日です。
`;
