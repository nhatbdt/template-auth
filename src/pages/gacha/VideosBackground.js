import classNames from 'classnames';
import { inject, observer } from 'mobx-react';
import React, { useEffect, useRef, useState } from 'react';

import Misc from '../../utils/misc';

function VideosBackground({
  videoRef,
  animationEnded,
  isSubmited,
  handleEndedVideo,
  handleEndedVideoResult,
  // present,
  resultUrl,
  introUrl,
  isShowVideoResult,
  handleShowVideoResult,
  quizVideo,
  gachaTicketId,
  modelSelected,
}) {
  const [allowedAudio, setAllowedAudio] = useState(false);
  const videoResult1Ref = useRef(null);

  const isMobile = Misc.isMobile;
  // const isAndroid = /Android/i.test(navigator.userAgent);
  // const isMuted = isMobile && isAndroid;
  const isMuted = false;

  useEffect(() => {
    const timer = setTimeout(() => {
      if (resultUrl) videoResult1Ref.current?.play();
    }, 12000);

    return () => clearTimeout(timer);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [resultUrl]);

  useEffect(() => {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      // request permission to sound device
      navigator.mediaDevices
        .getUserMedia({ audio: true })
        .then(function (stream) {
          // request allowed
          // eslint-disable-next-line no-console
          console.log('request permission to audio allowed');
          setAllowedAudio(true);
        })
        .catch(function (error) {
          // request rejected
          // eslint-disable-next-line no-console
          console.error('request permission to audio rejected:', error);
          setAllowedAudio(false);
        });
    } else {
      // eslint-disable-next-line no-console
      console.log('browser not support getUserMedia');
      setAllowedAudio(false);
    }
  }, []);

  return (
    <>
      {animationEnded && modelSelected && isSubmited && introUrl && (
        <video
          className={classNames('video-bg-inside')}
          onEnded={handleEndedVideoResult}
          muted={isMuted || !allowedAudio}
          autoPlay
          playsInline
        >
          <source src={process.env.REACT_APP_CLOUD_FRONT_URL + introUrl} type="video/mp4" />
        </video>
      )}

      {isShowVideoResult && gachaTicketId && resultUrl && (
        <video
          ref={videoResult1Ref}
          className="video-bg-inside"
          onEnded={handleShowVideoResult}
          muted={isMuted || !allowedAudio}
          autoPlay
          playsInline
        >
          <source src={process.env.REACT_APP_CLOUD_FRONT_URL + resultUrl} type="video/mp4" />
        </video>
      )}

      {animationEnded && gachaTicketId && !isSubmited && (
        <video className="video-bg-inside" autoPlay loop playsInline muted={isMuted || !allowedAudio}>
          <source src={process.env.REACT_APP_CLOUD_FRONT_URL + '/gacha/background.mp4'} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      )}

      {quizVideo && gachaTicketId && (
        <video
          ref={videoRef}
          className={classNames('video-bg-inside', { hidden: animationEnded })}
          onEnded={handleEndedVideo}
          autoPlay
          playsInline
          muted={isMuted || !allowedAudio}
        >
          <source src={process.env.REACT_APP_CLOUD_FRONT_URL + quizVideo} type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      )}

      {!isMobile && !gachaTicketId && (
        <video
          id="first-video"
          className={classNames('video-bg-inside', { hidden: quizVideo && gachaTicketId })}
          autoPlay
          playsInline
          loop
          muted={isMuted || !allowedAudio}
        >
          <source src="https://d3dax4s58tii0r.cloudfront.net/gacha/quiz/black.mp4" type="video/mp4" />
          Your browser does not support the video tag.
        </video>
      )}
    </>
  );
}

// export default VideosBackground;
export default inject(stores => ({
  gachaStore: stores.gacha,
}))(observer(({ gachaStore, ...props }) => <VideosBackground {...props} gachaTicketId={gachaStore.gachaTicketId} />));
