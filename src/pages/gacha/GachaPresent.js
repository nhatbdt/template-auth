import React from 'react';
import styled from 'styled-components';
import classNames from 'classnames';
import { t } from 'i18next';
import Media from '../../utils/media';
import AspectRatio from '../../components/aspect-ratio';
import { Images } from '../../theme';
import Thumbnail from '../../components/thumbnail';
import Misc from '../../utils/misc';

function GachaPresent({ present, isShowGift, setIsShowGift, animationResultEnded }) {
  return (
    <PresentContainerStyled className={classNames('control', { 'show-gift': animationResultEnded && isShowGift })}>
      <div className="container">
        <PresentStyled>
          <AspectRatio ratio={'18:9'} bgColor={'transparent'}>
            <CloseBtnStyled onClick={() => setIsShowGift(false)}>x</CloseBtnStyled>
            <div className="content">
              <div className="frame-left" style={{ ...present?.effects }}>
                <div
                  className="frame-child"
                  style={{
                    backgroundImage: 'url("' + Images.PRODUCT_BG + '")',
                  }}
                >
                  <div className="gif-infor">
                    <Thumbnail url={present?.imageUrl} size={Misc.isMobile ? 140 : 180} />
                    <div className="text">
                      <p className="text-content">{present?.categoryName}</p>
                      <div className="underline" style={{ ...present?.effects }} />
                    </div>
                  </div>
                </div>
              </div>
              <div className="frame-right">
                <h2 className="title">{t('common:gacha.you_got')}</h2>
                <div className="content" style={{ ...present?.effects }}>
                  <p className="name">{present?.title}</p>
                </div>
              </div>
            </div>
          </AspectRatio>
        </PresentStyled>
      </div>
    </PresentContainerStyled>
  );
}

export default GachaPresent;

const CloseBtnStyled = styled.span`
  position: absolute;
  top: 0;
  right: 20px;
  font-size: 26px;
  font-weight: 500;
  color: #fff;
  cursor: pointer;
`;

const PresentStyled = styled.div`
  cursor: pointer;
  width: 800px;
  max-width: 90%;
  flex-direction: column;
  gap: 28px;
  ${Media.lessThan(Media.SIZE.LG)} {
    /* width: 160px; */
    width: 60vh;
    gap: 4px;
  }

  .content {
    width: 100%;
    height: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    .frame-left {
      z-index: 1000;
      border-radius: 2px;
      width: 66%;
      height: 100%;
      padding: 8px;
      clip-path: polygon(22% 0, 100% 0%, 80% 100%, 0 100%);
      /* background: linear-gradient(89.83deg, #dd37e2 3.13%, #d164f7 42.14%, #9835c6 99.67%); */
      .frame-child {
        background-color: #ccc;
        width: 100%;
        height: 100%;
        clip-path: polygon(calc(22% + 3px) 0, calc(100% - 3px) 0%, calc(80% - 3px) 100%, 3px 100%);
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
        display: flex;
        .gif-infor {
          display: flex;
          flex-direction: column;
          justify-content: space-between;
          align-items: center;
          width: 60%;
          min-width: 200px;
          height: 70%;
          min-height: 300px;
          margin: auto;
          ${Media.lessThan(Media.SIZE.LG)} {
            justify-content: space-around;
          }
          ${Media.lessThan(Media.SIZE.MD)} {
            justify-content: space-around;
            min-height: 200px;
          }
          .text {
            width: 100%;
            margin-left: -20%;
            .text-content {
              color: #fff;
              font-family: Noto Sans;
              font-size: 34px;
              font-style: italic;
              font-weight: 800;
              ${Media.lessThan(Media.SIZE.LG)} {
                font-size: 26px;
              }
              ${Media.lessThan(Media.SIZE.MD)} {
                font-size: 20px;
              }
            }
            .underline {
              height: 8px;
              clip-path: polygon(2% 0, 100% 0%, 98% 100%, 0 100%);
              /* background: linear-gradient(89.83deg, #dd37e2 3.13%, #d164f7 42.14%, #9835c6 99.67%); */
              ${Media.lessThan(Media.SIZE.LG)} {
                height: 4px;
              }
              ${Media.lessThan(Media.SIZE.LG)} {
                height: 3px;
              }
            }
          }
        }
      }
    }

    .frame-right {
      z-index: 100;
      margin-left: -16%;
      padding: 28px;
      background-color: #1b1e24;
      border-radius: 2px;
      width: 50%;
      height: 46%;
      clip-path: polygon(0 0, 100% 0%, 80% 100%, 0 100%);
      overflow: hidden;

      .title {
        color: #fff;
        font-family: 'Noto Sans';
        font-weight: 800;
        font-style: italic;
        font-size: 46px;
        margin: 0;
        /* line-height: 68px; */
        ${Media.lessThan(Media.SIZE.LG)} {
          font-size: 34px;
          margin-bottom: 8px;
        }
        ${Media.lessThan(Media.SIZE.MD)} {
          font-size: 26px;
          margin-bottom: 4px;
        }
      }
      .content {
        height: 60px;
        max-height: 40%;
        clip-path: polygon(0 0, 86% 0%, 80% 100%, 0 100%);
        /* background: linear-gradient(89.83deg, #dd37e2 3.13%, #d164f7 42.14%, #9835c6 99.67%); */

        .name {
          color: #fff;
        }
      }
    }
  }

  .infor {
    display: flex;
    width: 100%;
    height: 50px;
    border: 2px solid #ccc;
    color: #fff;
    margin-bottom: 24px;
    ${Media.lessThan(Media.SIZE.LG)} {
      height: 40px;
    }
    .title {
      margin: auto;
      font-weight: 500;
    }
  }
`;

const PresentContainerStyled = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
  display: flex;
  opacity: 0;
  visibility: hidden;

  @keyframes fadeInAnimation {
    0% {
      opacity: 0;
      transform: translateX(-50%) translateY(-50%) scale(0);
    }
    100% {
      opacity: 1;
      transform: translateX(-50%) translateY(-50%) scale(1);
    }
  }
  &.show-gift {
    opacity: 0;
    visibility: visible;
    animation: fadeInAnimation 1.2s ease-in-out forwards;
    top: 45%;
  }
  .container {
    /* max-width: 960px; */
    max-width: 100vw;
    /* width: 1080px; */
    width: 1200px;
    margin: auto;
    display: flex;
    justify-content: space-around;
    align-items: stretch;
    gap: 12px 40px;
    ${Media.lessThan(Media.SIZE.LG)} {
      max-width: calc(100vh - 120px - 20px);
    }
  }
`;
