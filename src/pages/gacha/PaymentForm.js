import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
import Button from '../../components/button';
import { useTranslation } from 'react-i18next';
import MaskLoading from '../../components/mask-loading';
import { inject, observer } from 'mobx-react';
import i18n from '../../translations/i18n';
import { onSign } from '../product-details/utils/checkout';
import Confirmable from '../../components/confirmable';
import {
  approveMaxNew,
  checkBalance,
  checkCorrespondingNetwork,
  getAllowanceByToken,
  onTransfer,
} from '../../utils/web3';
import { CHAIN_LIST } from '../../constants/chains';
import Format from '../../utils/format';
import CURRENCIES from '../../constants/currencies';
import { getRateByCurrency } from '../../utils/rates';
import { firestore } from '../../services/firebase';
import PRICES from '../../constants/prices';
import Thumbnail from '../../components/thumbnail';
import { Images } from '../../theme';
import Storage from '../../utils/storage';
import Media from '../../utils/media';

function PaymentForm({
  onClose,
  onPaid,
  currentChainId,
  authStore,
  gachaStore,
  gachaInfo,
  fetchData,
  setAnimationEnded,
}) {
  const { t } = useTranslation(['product_details', 'common']);

  const { yenFee } = gachaInfo;
  const currentChainData = CHAIN_LIST[currentChainId];
  const currency = currentChainData.erc20Token[0].symbol;
  const fractionDigits = currentChainData?.erc20Token[0].fractionDigits;
  const price = +(yenFee / getRateByCurrency(currency, authStore.initialData)).toFixed(fractionDigits);

  // ['CREDIT', 'SLASH_FI']
  const handlePayment = async paymentType => {
    if (onClose) onClose();
    MaskLoading.open({});

    const isDemo = process.env.REACT_APP_GACHA_DEMO_FLG;
    if (isDemo && isDemo == 1) {
      setTimeout(async () => {
        await gachaStore.setPaymentSuccess();
        MaskLoading.close();
      }, 1000);
    } else {
      try {
        const fireStoreGacha = await firestore?.gacha()?.add({
          gachaId: gachaInfo?.id,
          paymentStatus: false,
          createdAt: moment().format(),
        });

        const langKey = i18n.language.toUpperCase();
        const { success, data } = await gachaStore.getGachaPaymentInfo({
          paymentType,
          chainId: currentChainId,
          langKey,
          firestoreId: fireStoreGacha.id,
        });
        if (success) {
          if (paymentType === 'SLASH_FI') Storage.set('URL_ACTIVE', `/gacha`);
          window.location = data.url;
        }
      } catch (error) {
        // eslint-disable-next-line no-console
        console.log('catch: ', { error });
      } finally {
        MaskLoading.close();
      }
    }
  };

  const onCheckPrice = async yenFee => {
    const gachaPriceResult = await gachaStore.getGacha();

    if (!gachaPriceResult.success || !gachaPriceResult.data) throw new Error('GET_PRODUCT_PRICE_NULL');

    if (yenFee !== gachaPriceResult.data.yenFee) {
      MaskLoading.close();
      const ok = await Confirmable.open({
        content: t('change_price_message', {
          currentPrice: Format.price(yenFee),
          changedPrice: Format.price(gachaPriceResult.data.price),
          currency: CURRENCIES.JPY,
        }),
      });

      // eslint-disable-next-line no-throw-literal
      if (!ok) throw { error: 'CANCELED' };
      MaskLoading.open({});
    }
  };

  // eslint-disable-next-line no-unused-vars
  const paymentGachaListener = documentId =>
    new Promise((resolve, reject) => {
      const timeOut = setTimeout(() => {
        unsubscribe();
        // eslint-disable-next-line prefer-promise-reject-errors
        reject('ERROR_TIMEOUT');
      }, 180000);

      const unsubscribe = firestore
        .gacha()
        .doc(documentId)
        .onSnapshot(
          snapshot => {
            const data = snapshot.data();

            if (data?.paymentStatus) {
              resolve(data);

              unsubscribe();
              clearTimeout(timeOut);
            }
          },
          e => reject(e),
        );
    });

  const onValidate = async (chainId, yenFee, price, currency) => {
    await onCheckPrice(yenFee, chainId);
    await checkCorrespondingNetwork(chainId);
    await checkBalance(authStore, { currency }, price);
  };

  const handleTransfer = async (price, currency, data) => {
    // if (Misc.isMobile) {
    //   await Confirmable.open({
    //     content: t('product_details:you_signed'),
    //     hideCancelButton: true,
    //     acceptButtonText: t('common:ok'),
    //   });
    // }

    const publicAddress = authStore.initialData?.publicAddress;
    const transactionId = await onTransfer(publicAddress, t, data, PRICES.GAS_LIMIT, currency, price);

    firestore.logs().add({
      gachaId: gachaInfo?.id,
      createdAt: moment().format('YYYY/MM/DD HH:mm'),
      from: publicAddress,
      transactionId,
      price,
      currency,
    });

    return transactionId;
  };

  const onPurchase = async (currency, price, chainId) => {
    const fireStoreGacha = await firestore?.gacha()?.add({
      gachaId: gachaInfo?.id,
      paymentStatus: false,
      createdAt: moment().format(),
    });

    // eslint-disable-next-line no-console
    console.log('fireStoreGacha: ', { fireStoreGacha });
    const purchaseResult = await gachaStore.createGachaMetaMaskPayment({
      chainId,
      fee: price,
      currency,
      firestoreId: fireStoreGacha.id,
    });

    if (!purchaseResult.success) {
      // eslint-disable-next-line no-console
      console.log({ purchaseResult });
      throw purchaseResult.data;
    }

    try {
      await handleTransfer(price, currency, purchaseResult?.data?.functionSignature);
    } catch (e) {
      await gachaStore.cancelPaymentMetaMask({
        gachaHistoryId: purchaseResult?.data?.gachaHistoryId,
      });
      throw e;
    }

    await paymentGachaListener(fireStoreGacha.id);

    MaskLoading.open({
      message: (
        <>
          {t('messages.caution_1')}
          <br />
          {t('messages.caution_2')}
        </>
      ),
    });
  };

  const handlePaymentMetaMask = async () => {
    if (onClose) onClose();
    MaskLoading.open({
      message: (
        <>
          {t('messages.waiting_1')}
          <br />
          {t('messages.waiting_2')}
        </>
      ),
    });

    const { publicAddress } = authStore.initialData;
    const paymentAddress = CHAIN_LIST[currentChainId]?.exchangeContractAddress;
    try {
      await onSign(authStore);
      await onValidate(currentChainId, yenFee, price, currency);

      const allowance = await getAllowanceByToken(currency, publicAddress, paymentAddress);
      if (allowance < price) {
        await approveMaxNew(publicAddress, t, currency);
      }

      await onPurchase(currency, price, currentChainId);
      if (onPaid) await onPaid();
    } catch (e) {
      // eslint-disable-next-line no-console
      console.log({ e });
      MaskLoading.close();
      // const firestoreLogId = await firestore.logs().add({
      //   gachaId: gachaInfo?.id,
      //   createdAt: moment().format('YYYY/MM/DD HH:mm'),
      //   publicAddress,
      //   paymentAddress,
      //   currentChainId,
      //   price,
      //   currency,
      //   error: e.error?.toString() ?? '',
      //   errorMessage: e.message?.toString() ?? '',
      // });
      // alert(firestoreLogId?.id);
      if (e?.code !== 4001 && !e?.message?.includes('MetaMask Personal Message')) {
        Confirmable.open({
          content: t(`validation_messages:${e.error || e.message || 'SOMETHING_WENT_WRONG'}`),
          hideCancelButton: true,
        });
      }
    } finally {
      setAnimationEnded(false);
      if (fetchData) await fetchData();
      MaskLoading.close();
    }
  };

  return (
    <PaymentFormStyled className="payment-form">
      <Thumbnail url={Images.PAYMENT} size={148} className="payment-logo" />
      <h2 className="text">
        {t('common:gacha.price_for_payment', {
          price: gachaInfo?.yenFee,
          currency: t('common:yen'),
          cryptoPrice: price,
          cryptoCurrency: currency,
        })}
      </h2>
      <h4 className="text">{t('common:gacha.select_payment')}</h4>
      <div className="actions">
        {/* <Button size="large" className="payment-button" onClick={() => handlePayment('CREDIT')}>
          {t('credit_card_payment')}
        </Button> */}
        <Button size="large" className="payment-button" onClick={() => handlePayment('SLASH_FI')}>
          {t('pay_with_slash')}
        </Button>
        <Button size="large" className="payment-button" onClick={() => handlePaymentMetaMask()}>
          {t('metamask_payment')}
        </Button>
      </div>
    </PaymentFormStyled>
  );
}

// export default PaymentForm;
export default inject(stores => ({
  authStore: stores.auth,
  gachaStore: stores.gacha,
}))(
  observer(({ authStore, gachaStore, ...props }) => {
    return (
      <PaymentForm
        {...props}
        authStore={authStore}
        gachaStore={gachaStore}
        currentChainId={authStore.currentChainId}
        gachaInfo={gachaStore.gachaInfo}
      />
    );
  }),
);

const PaymentFormStyled = styled.div`
  padding-top: 20px;
  padding-bottom: 20px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  .text {
    color: #fff;
    font-size: 18px;
    ${Media.lessThan(Media.SIZE.SM)} {
      text-align: center;
    }
  }
  .body {
    position: relative;
  }
  .actions {
    margin-top: 16px;
    width: 100%;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-wrap: wrap;
    gap: 16px;
    button {
      font-size: 14px;
      min-width: 226px;
      height: 52px;
      background-color: #045afc;
      border-radius: 9999px;
    }
  }
`;
