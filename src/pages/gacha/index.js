import React, { useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import Page from '../../components/page';
import { useTranslation } from 'react-i18next';

import { Colors, Images } from '../../theme';
import { useHistory } from 'react-router-dom/cjs/react-router-dom';
import Media from '../../utils/media';
import ModalComponent from '../../components/modal';
import { login } from '../../utils/conditions';
import MaskLoading from '../../components/mask-loading';
import { inject, observer } from 'mobx-react';
import PaymentForm from './PaymentForm';
import classNames from 'classnames';
import Confirmable from '../../components/confirmable';
import { CloseOutlined, StepForwardOutlined } from '@ant-design/icons';
import { GACHA_RESULTS, GACHA_MODEL_LIST, GACHA_QUIZ_LIST } from '../../constants/gacha';
import GachaPresent from './GachaPresent';
import ModelSelector from './ModelSelector';
import VideosBackground from './VideosBackground';

function GachaSelectModel({ gachaStore, gachaInfo, gachaTicketId, loggedIn }) {
  const { t, i18n } = useTranslation('common');
  const history = useHistory();

  const videoRef = useRef(null);

  const [introducttionModal, setIntroductionModal] = useState({ isOpen: false });
  const [modelSelected, setModelSelected] = useState(null);
  const [paymentPopup, setPaymentPopup] = useState({ isOpen: false });
  const [animationEnded, setAnimationEnded] = useState(false);
  const [animationResultEnded, setAnimationResultEnded] = useState(false);
  const [isSubmited, setIsSubmited] = useState(false);
  const [isShowGift, setIsShowGift] = useState(true);
  const [present, setPresent] = useState({ animation: { resultUrl: null, introUrl: null } });
  const [isShowVideoResult, setIsShowVideoResult] = useState(false);
  const [quizVideo, setQuizVideo] = useState(null);

  const handlePlay = async () => {
    const isDemo = process.env.REACT_APP_GACHA_DEMO_FLG;
    if (isDemo && isDemo == 1) {
      setTimeout(async () => {
        MaskLoading.open({});
        // clearTimeout(timer);
        setIsSubmited(true);
        const data = {
          gachaResult: 'PERFECT',
          imageUrl:
            'https://d3dax4s58tii0r.cloudfront.net/photo/product/images_1688553049191-removebg-preview_1689148488616.png',
          categoryName: 'Category Name',
          title: 'Product name',
          winningNumber: 3,
          chosenNumber: 2,
          winnerFlg: false,
          colorMode: 'BLACK',
        };
        const { gachaResult } = data;
        const effects = GACHA_RESULTS.find(item => item.result === gachaResult)?.effects;
        // -- get intro video uri and result
        const intro = GACHA_RESULTS.find(item => item.result === gachaResult)?.intro;
        const result = GACHA_MODEL_LIST.find(item => item.value === data?.chosenNumber)[data?.colorMode.toLowerCase()];
        // create animation obj
        const animation = {
          introUrl: intro,
          resultUrl: data?.winningNumber === data?.chosenNumber ? result?.win : result?.lose,
        };
        setPresent({ ...data, effects, animation });
        MaskLoading.close();
      }, 12000);
    } else {
      try {
        const { success, data } = await gachaStore.submitGacha({
          langKey: i18n.language?.toUpperCase(),
          selectedNumber: modelSelected,
        });

        if (success) {
          MaskLoading.open({});
          setTimeout(async () => {
            setIsSubmited(true);
            // MaskLoading.close();
            const { gachaResult, colorMode, chosenNumber } = data;
            const effects = GACHA_RESULTS.find(item => item.result === gachaResult)?.effects;
            // -- get intro video uri and result
            const intro = GACHA_RESULTS.find(item => item.result === gachaResult)?.intro;
            const result = GACHA_MODEL_LIST.find(item => item.value === chosenNumber)[colorMode?.toLowerCase()];
            // create animation obj
            const animation = {
              introUrl: intro,
              resultUrl: data?.winningNumber === chosenNumber ? result?.win : result?.lose,
            };
            setPresent({ ...data, effects, animation });
          }, 12000);
        }
      } catch (error) {
        setIsSubmited(false);
        // eslint-disable-next-line no-console
        console.log({ error });
        await fetchData();
      } finally {
        MaskLoading.close();
      }
    }
  };

  const handleEndedVideo = async () => {
    setAnimationEnded(true);
    videoRef.current.pause();
  };

  const handleEndedVideoResult = async () => {
    setIsShowVideoResult(true);
    setAnimationEnded(true);
    // videoRef.current.pause();
  };

  const handleShowVideoResult = async () => {
    setAnimationEnded(true);
    setAnimationResultEnded(true);
    // videoRef.current.pause();
  };

  const fetchData = async () => {
    MaskLoading.open({});
    const isDemo = process.env.REACT_APP_GACHA_DEMO_FLG;
    if (isDemo && isDemo == 1) {
      setTimeout(async () => {
        await gachaStore.setPaymentSuccess();
        MaskLoading.close();
        // setIntroductionModal({ isOpen: true });
        videoRef.current?.play();
      }, 1000);

      // set quiz video
      var colorMode = 'BLACK';
      const quizVideo = GACHA_QUIZ_LIST.find(item => item.colorMode === colorMode)?.quizVideo;
      setQuizVideo(quizVideo);
    } else {
      try {
        const { success, data } = await gachaStore.getGacha();
        if (success) {
          const { unusedProduct } = data;
          let gachaHistoryId = null;

          // const quizVideo = GACHA_QUIZ_LIST.find(item => item.colorMode === data?.colorMode)?.quizVideo;
          // setQuizVideo(quizVideo);

          if (loggedIn) {
            const { success, data: paidGachaTiketData } = await gachaStore.getPaidGachaTiket();
            if (success) {
              if (paidGachaTiketData?.gachaHistoryId) {
                gachaHistoryId = paidGachaTiketData?.gachaHistoryId;
                const quizVideo = GACHA_QUIZ_LIST.find(
                  item => item.colorMode === paidGachaTiketData?.colorMode,
                )?.quizVideo;
                setQuizVideo(quizVideo);
                setIntroductionModal({ isOpen: false });
                // videoRef.current?.play();
              } else {
                gachaHistoryId = null;
                setIntroductionModal({ isOpen: true });
              }

              if (unusedProduct === 0 && !gachaHistoryId) {
                // eslint-disable-next-line no-throw-literal
                throw { error: 'gacha_not_available' };
              }
            }
          } else {
            // setIntroductionModal({ isOpen: true });
          }
        } else {
          // eslint-disable-next-line no-throw-literal
          throw { error: 'gacha_not_available' };
        }
      } catch (e) {
        MaskLoading.close();
        if (e.error === 'gacha_not_available') {
          await Confirmable.open({
            content: t('common:gacha.no_gacha_product'),
            hideCancelButton: true,
            acceptButtonText: t('common:gacha.return_to_homepage'),
          });
          MaskLoading.open({});
          history.push('/');
        }
        // eslint-disable-next-line no-console
        console.log(e);
      } finally {
        MaskLoading.close();
        // videoRef.current?.play();
        if (!loggedIn) {
          setIntroductionModal({ isOpen: true });
        }
      }
    }
  };

  const handleBuyNow = () => {
    const isDemo = process.env.REACT_APP_GACHA_DEMO_FLG;
    if (isDemo && isDemo == 1) {
      // demo flow
      MaskLoading.open({});
      setTimeout(async () => {
        MaskLoading.close();
        setIntroductionModal({ isOpen: false });
        setPaymentPopup({ isOpen: true });
      }, 1000);
    } else {
      if (!gachaInfo?.unusedProduct) {
        return;
      }
      setPaymentPopup({ isOpen: true });
    }
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loggedIn]);

  useEffect(() => {
    if (modelSelected) handlePlay();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modelSelected]);

  return (
    <Page className="gacha-page1">
      <WrapedStyled>
        {/* <LogoStyled>
          <img className="logo" src={Images.GACHA_LOGO_IMAGE} alt="GACHA_LOGO_IMAGE" />
        </LogoStyled> */}

        <WheelStyled className="wheel">
          <VideosBackground
            videoRef={videoRef}
            // videoResult1Ref={videoResult1Ref}
            animationEnded={animationEnded}
            isSubmited={isSubmited}
            handleEndedVideo={handleEndedVideo}
            handleEndedVideoResult={handleEndedVideoResult}
            resultUrl={present?.animation?.resultUrl}
            introUrl={present?.animation?.introUrl}
            isShowVideoResult={isShowVideoResult}
            handleShowVideoResult={handleShowVideoResult}
            quizVideo={quizVideo}
            gachaTicketId={gachaTicketId}
            modelSelected={modelSelected}
          />

          <ModelSelector
            isSubmited={isSubmited}
            animationEnded={animationEnded}
            modelSelected={modelSelected}
            setModelSelected={setModelSelected}
          />

          <GachaPresent
            present={present}
            isShowGift={isShowGift}
            setIsShowGift={setIsShowGift}
            animationResultEnded={animationResultEnded}
          />

          {!isSubmited && !!gachaTicketId && (
            <>
              <ActionsStyled className={classNames({ 'skip-act': !animationEnded })}>
                <div onClick={handleEndedVideo} className={classNames('button', 'skip', { show: !animationEnded })}>
                  <p>{t('common:gacha.skip_btn')}</p>
                  <StepForwardOutlined style={{ fontSize: 24 }} />
                </div>
              </ActionsStyled>
            </>
          )}

          {isSubmited && animationResultEnded && (
            <ActionsResultStyled>
              <ButtonStyled>
                <div onClick={() => history.push('/')} className="button back">
                  {t('common:gacha.go_to_homepage_btn')}
                </div>
              </ButtonStyled>
              <ButtonStyled>
                <div onClick={() => history.push('/gacha')} className="button">
                  {t('common:gacha.resume_playing')}
                </div>
              </ButtonStyled>
            </ActionsResultStyled>
          )}
        </WheelStyled>
      </WrapedStyled>

      {introducttionModal.isOpen && (
        <StyledModal
          className="customer-modal"
          open={introducttionModal.isOpen}
          closable={false}
          title={null}
          padding={0}
          width={790}
          wrapClassName="modal-gacha"
        >
          <GachaContent>
            <img className="gacha-img" src={Images.GACHA_MODAL_IMAGE} alt="GACHA_MODAL_IMAGE" />

            <div className="wrap-right">
              <div className="wrap">
                <p className="p1">{t('gacha.modal.title_1')}</p>

                <p className="p2">{t('gacha.modal.content_1')}</p>
              </div>

              <div onClick={login(handleBuyNow)} className="button">
                {t('gacha.modal.play_now')}
              </div>
              <div onClick={() => history.push('/')} className="button cancel">
                {t('gacha.modal.cancel')}
              </div>
            </div>
          </GachaContent>
        </StyledModal>
      )}

      <PaymentModalStyled
        title={null}
        open={paymentPopup?.isOpen}
        onCancel={() => setPaymentPopup({ isOpen: false })}
        closeIcon={<CloseOutlined style={{ fontSize: 20, padding: 6, background: '#ccc', borderRadius: 999 }} />}
        className="customer-modal"
        width={786}
      >
        <PaymentForm
          fetchData={fetchData}
          setAnimationEnded={setAnimationEnded}
          onClose={() => setPaymentPopup({ isOpen: false })}
          onPaid={() => {
            setIntroductionModal({ isOpen: false });
            videoRef?.current?.play();
          }}
        />
      </PaymentModalStyled>
    </Page>
  );
}

// export default GachaSelectModel;
export default inject(stores => ({
  authStore: stores.auth,
  gachaStore: stores.gacha,
}))(
  observer(({ authStore, gachaStore, ...props }) => (
    <GachaSelectModel
      {...props}
      gachaStore={gachaStore}
      gachaInfo={gachaStore.gachaInfo}
      currentChainId={authStore.currentChainId}
      gachaTicketId={gachaStore.gachaTicketId}
      loggedIn={authStore?.loggedIn}
    />
  )),
);

const ButtonStyled = styled.div`
  margin-top: 20px;
  /* display: inline-block; */
  gap: 6px 12px;
  position: relative;

  .button {
    padding: 13px 68px;
    background-color: #045afc;
    color: #ffffff;
    border-radius: 9999px;
    cursor: pointer;
    font-weight: bold;
    margin-bottom: 10px;

    &.back {
      color: #000;
      background-color: #a6cfeb;
    }
  }

  img {
    position: absolute;
    top: 5px;
    right: -55px;
  }
`;

const PaymentModalStyled = styled(ModalComponent)`
  .ant-modal-title {
    color: #fff;
    font-size: 24px;
  }
  .ant-modal-header,
  .ant-modal-body {
    background-color: ${Colors.BOX_BACKGROUND};
    border: none;
  }

  /* .ant-modal-content {
    ${Media.lessThan(Media.SIZE.LG)} {
      transform: rotate(90deg);
    }
  } */

  .ant-modal-body {
    padding-top: 14px !important;
  }
`;

const StyledModal = styled(ModalComponent)``;

const ActionsResultStyled = styled.div`
  padding-bottom: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 4px 10px;
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
`;

const ActionsStyled = styled.div`
  text-align: center;
  display: inline-block;
  gap: 6px 12px;
  position: absolute;
  bottom: 20px;
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
  ${Media.lessThan(Media.SIZE.LG)} {
    bottom: 70px;
  }
  ${Media.lessThan(Media.SIZE.LG)} {
    left: 50%;
    /* bottom: 23%; */
    bottom: 0;
    /* left: 0;
    bottom: 230px;
    transform: translateX(50%); */
    /* transform: translateX(50%) translateY(50%); */
  }

  .price {
    color: aqua;
    font-style: italic;
    font-size: 16px;
  }

  .button {
    padding: 10px 60px;
    background-color: #045afc;
    color: #ffffff;
    border-radius: 9999px;
    cursor: pointer;
    font-weight: bold;
    margin-top: 2px;
    margin-bottom: 10px;

    &.submit {
      color: #000;
      background-color: #a6cfeb;
    }
  }
  .submit,
  .skip {
    /* visibility: hidden; */
    display: none;
    &.show {
      /* visibility: visible; */
      display: block;
    }
  }

  &.skip-act {
    display: flex;
    align-items: flex-end;
    justify-content: flex-end;
    width: 100%;
    .skip {
      display: flex;
      justify-content: center;
      align-items: center;
      min-width: 220px;
      color: #000;
      background-color: rgb(166 198 235 / 46%);
      margin-right: 5%;
      ${Media.lessThan(Media.SIZE.LG)} {
        width: 220px;
        margin-right: 12%;
      }
      ${Media.lessThan(Media.SIZE.SM)} {
        margin-right: 16%;
      }
    }
  }

  img {
    position: absolute;
    top: 5px;
    right: -55px;
  }
`;

const WrapedStyled = styled.div`
  width: 100vw;
  height: 100vh;
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const WheelStyled = styled.div`
  position: fixed;
  /* top: 0; */
  /* top: 92px; */
  bottom: 0;
  left: 0;
  width: 100%;
  /* height: 100%; */
  height: calc(100% - 92px);
  ${Media.lessThan(Media.SIZE.LG)} {
    transform: rotate(90deg);
    bottom: unset;
    left: unset;
    width: calc(100vh - 60px - 60.14px);
    height: 100vw;
  }

  .video-bg-out-screen {
    position: absolute;
    top: 50%;
    /* top: 0; */
    left: 50%;
    min-width: 100%;
    min-height: 100%;
    /* min-height: calc(100vh - 92px); */
    width: auto;
    height: auto;
    z-index: -100;
    /* max-width: 100%; */
    transform: translateX(-50%) translateY(-50%);
    /* transform: translateX(-50%) translateY(0); */
    /* transform: translateX(-50%) translateY(calc(-50% + 150px)); */
    // filter: opacity(65%);

    &.hidden {
      opacity: 0;
    }
  }

  .video-bg-inside {
    position: absolute;
    top: 50%;
    /* top: 0; */
    left: 50%;
    max-width: 100%;
    max-height: 100%;
    /* min-height: calc(100vh - 92px); */
    width: auto;
    height: auto;
    z-index: -100;
    /* max-width: 100%; */
    transform: translateX(-50%) translateY(-50%);
    /* transform: translateX(-50%) translateY(0); */
    /* transform: translateX(-50%) translateY(calc(-50% + 150px)); */
    // filter: opacity(65%);

    &.hidden {
      opacity: 0;
    }
  }
`;

const GachaContent = styled.div`
  padding: 45px 35px;
  display: flex;
  background-color: #1b1e24;
  gap: 14px 22px;
  ${Media.lessThan(Media.SIZE.MD)} {
    display: block;
    padding: 10px;
    text-align: center;
  }

  .gacha-img {
    max-width: 100%;
  }

  .wrap-right {
    text-align: justify;
    /* padding-left: 30px; */
    flex-direction: column;
    width: calc(100% - 330px);
    justify-content: space-between;
    display: flex;
    gap: 16px;
    ${Media.lessThan(Media.SIZE.MD)} {
      width: 100%;
      /* padding-right: 0; */
      margin-top: 10px;
      padding: 16px;
    }

    .p1 {
      color: #fff;
      font-size: 26px;
      /* font-family: Noto Sans JP; */
      font-weight: 700;
      ${Media.lessThan(Media.SIZE.LG)} {
        font-size: 20px;
      }
    }

    .p2 {
      margin-top: 10px;
      color: #a8aeba;
      font-size: 18px;
      ${Media.lessThan(Media.SIZE.LG)} {
        font-size: 16px;
      }
    }

    .button {
      display: flex;
      /* width: 195px; */
      width: 100%;
      max-width: 400px;
      height: 50px;
      border-radius: 9999px;
      justify-content: center;
      align-items: center;
      background-color: #045afc;
      cursor: pointer;
      margin: 0px auto;
      color: #f9fafb;
      &.cancel {
        background-color: rgb(166 198 235 / 46%);
      }
    }
  }
`;
