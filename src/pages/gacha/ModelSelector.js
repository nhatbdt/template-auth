import React from 'react';
import styled from 'styled-components';
import classNames from 'classnames';
import AspectRatio from '../../components/aspect-ratio';
import { GACHA_MODEL_LIST } from '../../constants/gacha';
import Media from '../../utils/media';
import { inject, observer } from 'mobx-react';
import { Images } from '../../theme';
import { t } from 'i18next';
import i18n from '../../translations/i18n';

function ModelSelector({ gachaTicketId, isSubmited, animationEnded, modelSelected, setModelSelected }) {
  return (
    <>
      <SelectorControlStyled
        className={classNames('control', 'animation-before', {
          show: animationEnded && !isSubmited,
          'user-selected-model': modelSelected,
        })}
      >
        <div className="container">
          {GACHA_MODEL_LIST?.map(model => (
            <FactorStyled
              key={model.value}
              htmlFor={`model-${model.value}`}
              className={classNames({
                'selected-model': modelSelected && modelSelected === model.value,
                'no-selected-model': modelSelected && modelSelected !== model.value,
              })}
            >
              <AspectRatio ratio={'9:18'} bgColor={'transparent'}>
                <input
                  disabled={!gachaTicketId}
                  className="model-selector"
                  type="radio"
                  name="model"
                  id={`model-${model.value}`}
                  value={model.value}
                  onChange={({ target }) => setModelSelected(+target.value)}
                  checked={modelSelected === model.value}
                />
                <div
                  className="thumbnail"
                  style={{
                    backgroundImage: `url("${model.thumbnail}")`,
                  }}
                />
              </AspectRatio>
            </FactorStyled>
          ))}
        </div>
      </SelectorControlStyled>

      {modelSelected && (
        <SelectorControlStyled
          className={classNames('control', 'animation-after', {
            show: animationEnded && !isSubmited,
            'user-selected-model': modelSelected,
          })}
        >
          <div className="container">
            <ChatBoxStyled>
              <div className="chat-content">
                <p className="item-chat-content item-1" style={{ width: i18n.language === 'ja' ? '12em' : '16em' }}>
                  {t('common:gacha.modal.chat_1')}
                </p>
                <p className="item-chat-content item-2" style={{ width: i18n.language === 'ja' ? '20em' : '21em' }}>
                  {t('common:gacha.modal.chat_2')}
                </p>
                <p className="item-chat-content item-3" style={{ width: i18n.language === 'ja' ? '11em' : '16.5em' }}>
                  {t('common:gacha.modal.chat_3')}
                </p>
              </div>
              <div className="spike" />
            </ChatBoxStyled>

            {GACHA_MODEL_LIST?.map(model => (
              <FactorStyled
                key={model.value}
                htmlFor={`model-${model.value}`}
                className={classNames({
                  'selected-model': modelSelected && modelSelected === model.value,
                  'no-selected-model': modelSelected && modelSelected !== model.value,
                })}
              >
                <AspectRatio ratio={'9:18'} bgColor={'transparent'}>
                  <div
                    className="thumbnail"
                    style={{
                      backgroundImage: `url("${model.thumbnail}")`,
                    }}
                  />
                </AspectRatio>
              </FactorStyled>
            ))}
          </div>
        </SelectorControlStyled>
      )}
    </>
  );
}

// export default ModelSelector;
export default inject(stores => ({
  gachaStore: stores.gacha,
}))(observer(({ gachaStore, ...props }) => <ModelSelector {...props} gachaTicketId={gachaStore.gachaTicketId} />));

const ChatBoxStyled = styled.div`
  max-width: 310px;
  min-height: 52px;
  padding: 14px;
  position: absolute;
  /* top: 50%; */
  top: 6%;
  /* left: 50%; */
  left: 55%;
  /* transform: translateX(-50%) translateY(-50%); */
  transform: translateY(-50%);
  border-radius: 20px;
  background-color: #040c1a;

  opacity: 0;
  -webkit-animation: appear 2s;
  animation: appear 2s;
  -webkit-animation-delay: 3s;
  animation-delay: 3s;
  -webkit-animation-fill-mode: forwards;
  animation-fill-mode: forwards;

  @keyframes appear {
    0% {
      opacity: 0;
    }
    1% {
      opacity: 0.1;
    }
    99% {
      opacity: 0.9;
    }
    100% {
      opacity: 1;
    }
  }

  .spike {
    position: absolute;
    left: -14px;
    bottom: -3px;
    width: 25px;
    height: 36px;
    background-image: url(${Images.SPIKE});
  }

  .chat-content {
    .item-chat-content {
      color: #fff;
      border-right: 0.15em solid orange;
      font-family: 'Courier';
      font-size: 14px;
      white-space: nowrap;
      overflow: hidden;
    }

    .item-1 {
      /* width: 16em; */
      opacity: 0;
      /* -webkit-animation: type 2s steps(40, end); */
      animation: type 2s steps(40, end);
      /* -webkit-animation-delay: 2s; */
      animation-delay: 2s;
      /* -webkit-animation-fill-mode: forwards; */
      animation-fill-mode: forwards;
    }
    .item-2 {
      /* width: 21em; */
      opacity: 0;
      -webkit-animation: type2 2s steps(40, end);
      animation: type2 2s steps(40, end);
      -webkit-animation-delay: 4s;
      animation-delay: 4s;
      -webkit-animation-fill-mode: forwards;
      animation-fill-mode: forwards;
    }
    .item-3 {
      /* width: 16.5em; */
      opacity: 0;
      -webkit-animation: type3 2.8s steps(20, end), blink 0.5s step-end infinite alternate;
      animation: type3 2.8s steps(20, end), blink 0.5s step-end infinite alternate;
      -webkit-animation-delay: 6s;
      animation-delay: 6s;
      -webkit-animation-fill-mode: forwards;
      animation-fill-mode: forwards;
    }
  }

  @keyframes type {
    0% {
      width: 0;
    }
    1% {
      opacity: 1;
    }
    99% {
      border-right: 0.15em solid orange;
    }
    100% {
      opacity: 1;
      border: none;
    }
  }

  @keyframes type2 {
    0% {
      width: 0;
    }
    1% {
      opacity: 1;
    }
    99% {
      border-right: 0.15em solid orange;
    }
    100% {
      opacity: 1;
      border: none;
    }
  }

  @keyframes type3 {
    0% {
      width: 0;
    }
    1% {
      opacity: 1;
    }
    100% {
      opacity: 1;
    }
  }

  @keyframes blink {
    50% {
      border-color: transparent;
    }
  }
`;

const FactorStyled = styled.label`
  cursor: pointer;
  width: 300px;
  max-width: 30%;
  ${Media.lessThan(Media.SIZE.LG)} {
    width: 160px;
  }

  &:hover {
    .thumbnail {
      z-index: 999;
      filter: brightness(120%);
    }
    .infor {
      /* filter: brightness(105%); */
      /* color: aqua; */
    }
  }

  .model-selector {
    position: absolute;
    top: 0;
    left: 0;
    height: 0;
  }

  .model-selector:checked + .thumbnail {
    filter: brightness(150%);
    /* border-bottom: 2px solid aqua; */
  }

  .thumbnail {
    width: 100%;
    height: 100%;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
  }

  .infor {
    z-index: 0;
    margin-top: -16px;
    display: flex;
    width: 100%;
    height: 50px;
    color: #fff;
    margin-bottom: 24px;
    ${Media.lessThan(Media.SIZE.LG)} {
      height: 40px;
    }
    img {
      width: 100%;
      height: auto;
    }
    .title {
      margin: auto;
      font-weight: 500;
    }
  }
`;

const SelectorControlStyled = styled.div`
  visibility: hidden;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translateX(-50%) translateY(-50%);
  display: flex;
  ${Media.lessThan(Media.SIZE.LG)} {
    /* left: 42.5%; */
    /* transform: translateX(-56.5%) translateY(-50%); */
  }
  .container {
    position: relative;
    /* max-width: 960px; */
    max-width: 100vw;
    /* width: 1080px; */
    width: 1200px;
    margin: auto;
    display: flex;
    justify-content: space-around;
    align-items: stretch;
    gap: 12px 40px;
    ${Media.lessThan(Media.SIZE.LG)} {
      /* max-width: calc(100vh - 120px - 20px); */
      max-width: calc(90vh - 120px - 20px);
      justify-content: space-evenly;
    }
  }

  &.show {
    visibility: visible;
  }
  &.animation-before {
    &.user-selected-model {
      /* animation: fadeOut 2s, fadeIn 2s;
      animation-delay: 0s, 2s; */
      animation: fadeOut 1.2s;
      -webkit-animation: fadeOut 1.2s;
      animation-delay: 0s;
      -webkit-animation-delay: 0s;
      animation-fill-mode: forwards;
      -webkit-animation-fill-mode: forwards;
    }
  }
  &.animation-after {
    transform: scale(0.05) translate3D(-50%, -50%, 0);
    opacity: 0;
    &.user-selected-model {
      animation: fadeIn 1.2s;
      animation-delay: 1.2s;
      animation-fill-mode: forwards;
    }
    .container {
      position: relative;
      .no-selected-model {
        display: none;
      }
    }
  }

  @keyframes fadeOut {
    0% {
      zoom: 1;
      opacity: 1;
    }
    10% {
      zoom: 0.9;
      opacity: 0.9;
    }
    20% {
      zoom: 0.8;
      opacity: 0.8;
    }
    30% {
      zoom: 0.7;
      opacity: 0.7;
    }
    40% {
      zoom: 0.6;
      opacity: 0.6;
    }
    50% {
      zoom: 0.5;
      opacity: 0.5;
    }
    60% {
      zoom: 0.4;
      opacity: 0.4;
    }
    60% {
      zoom: 0.3;
      opacity: 0.3;
    }
    80% {
      zoom: 0.2;
      opacity: 0.2;
    }
    90% {
      zoom: 0.15;
      opacity: 0.15;
    }
    99% {
      zoom: 0.1;
      opacity: 0.01;
      width: 100%;
      height: 100%;
    }
    100% {
      zoom: 0.05;
      opacity: 0;
    }
  }

  @keyframes fadeIn {
    0% {
      transform: translate3D(-50%, -50%, 0) scale(0.05);
      -webkit-transform: translate3D(-50%, -50%, 0) scale(0.05);
      -moz-transform: translate3D(-50%, -50%, 0) scale(0.05);
      opacity: 0;
    }
    20% {
      transform: translate3D(-50%, -50%, 0) scale(0.2);
      -webkit-transform: translate3D(-50%, -50%, 0) scale(0.2);
      -moz-transform: translate3D(-50%, -50%, 0) scale(0.2);
      opacity: 0.2;
    }
    30% {
      transform: translate3D(-50%, -50%, 0) scale(0.3);
      -webkit-transform: translate3D(-50%, -50%, 0) scale(0.3);
      -moz-transform: translate3D(-50%, -50%, 0) scale(0.3);
      opacity: 0.3;
    }
    40% {
      transform: translate3D(-50%, -50%, 0) scale(0.4);
      -webkit-transform: translate3D(-50%, -50%, 0) scale(0.4);
      -moz-transform: translate3D(-50%, -50%, 0) scale(0.4);
      opacity: 0.4;
    }
    50% {
      transform: translate3D(-50%, -50%, 0) scale(0.5);
      -webkit-transform: translate3D(-50%, -50%, 0) scale(0.5);
      -moz-transform: translate3D(-50%, -50%, 0) scale(0.5);
      opacity: 0.5;
    }
    60% {
      transform: translate3D(-50%, -50%, 0) scale(0.6);
      -webkit-transform: translate3D(-50%, -50%, 0) scale(0.6);
      -moz-transform: translate3D(-50%, -50%, 0) scale(0.6);
      opacity: 0.6;
    }
    70% {
      transform: translate3D(-50%, -50%, 0) scale(0.7);
      -webkit-transform: translate3D(-50%, -50%, 0) scale(0.7);
      -moz-transform: translate3D(-50%, -50%, 0) scale(0.7);
      opacity: 0.7;
    }
    80% {
      transform: translate3D(-50%, -50%, 0) scale(0.8);
      -webkit-transform: translate3D(-50%, -50%, 0) scale(0.8);
      -moz-transform: translate3D(-50%, -50%, 0) scale(0.8);
      opacity: 0.8;
    }
    90% {
      transform: translate3D(-50%, -50%, 0) scale(0.9);
      -webkit-transform: translate3D(-50%, -50%, 0) scale(0.9);
      -moz-transform: translate3D(-50%, -50%, 0) scale(0.9);
      opacity: 0.9;
    }
    100% {
      transform: translate3D(-50%, -50%, 0) scale(1);
      -webkit-transform: translate3D(-50%, -50%, 0) scale(1);
      -moz-transform: translate3D(-50%, -50%, 0) scale(1);
      opacity: 1;
    }
  }
`;
