import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation, useTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';
import QS from 'query-string';

import Page from '../../components/page';
import Container from '../../components/container';
import Typography from '../../components/typography';
// import ToolBar from '../../components/tool-bar'
import FetchableList from '../../components/fetchable-list';
import ProductCard from '../../components/product-card';
import Footer from '../../app/footer';
import Media from '../../utils/media';
import { Images, Colors } from '../../theme';
import { Radio, Dropdown, Button, Menu } from 'antd';
import { DownOutlined } from '@ant-design/icons';
import Clickable from '../../components/clickable';
import classnames from 'classnames';

const StyledDiv = styled.div`
  position: relative;

  .text-primary {
    color: ${Colors.PRIMARY};
  }

  .visible-mb {
    display: none;

    ${Media.lessThan(Media.SIZE.MD)} {
      display: block;
    }
  }

  .visible-pc {
    display: block;

    ${Media.lessThan(Media.SIZE.MD)} {
      display: none;
    }
  }

  .ant-btn-primary {
    color: #14151c;
    text-shadow: none;
  }

  .open-filter-mobile {
    font-size: 16px;
    padding: 15px;
    border-radius: 0;
    height: 58px;

    img {
      margin-right: 10px;
    }
  }

  .close-filter-bar {
    height: 46px;
    width: 230px;
    padding: 10px 20px;
    margin: 0 auto;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .tool-bar {
    position: fixed;
    top: 73px;
    width: 100%;
    background-color: rgba(21, 23, 36, 0.8);
    backdrop-filter: blur(20px);
    z-index: 3;
    @supports ((-webkit-backdrop-filter: none) or (backdrop-filter: none)) {
      background-color: rgba(21, 23, 36, 0.5);
      backdrop-filter: blur(20px);
    }

    .search-input {
      max-width: 688px;
      margin: 0 auto;

      ${Media.lessThan(Media.SIZE.XXL)} {
        max-width: 600px;
      }

      ${Media.lessThan(Media.SIZE.XXL)} {
        max-width: 500px;
      }
    }

    .search-box-divider {
      display: none;
    }
  }

  .top-section {
    height: 35vw;
    min-height: 350px;
    padding-top: 73px;
    position: relative;
    background-size: 100%;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    background-attachment: fixed;
    background-repeat: no-repeat;
    position: absolute;
    top: 0;
    left: 0;
    z-index: -1;
    display: block;
    width: 100%;

    ${Media.lessThan(Media.SIZE.XL)} {
      background-size: auto 400px;
    }

    ${Media.lessThan(Media.SIZE.MD)} {
      /* background-image: url(''); //BACKGROUND_MB */
      padding-top: 65px;
      background-size: 100%;
    }
  }

  .content {
    margin-top: 60px;
    position: relative;

    ${Media.lessThan(Media.SIZE.MD)} {
      padding-top: 30px;
      margin-top: 30px;
    }

    .product-main-container {
      display: flex;
      flex-wrap: wrap;

      .product-filter-bar {
        width: 350px;
        min-height: 100%;
        background-color: #1d1d24;
        transition: all 0.2s;

        ${Media.lessThan(Media.SIZE.XXL)} {
          width: 350px;
        }

        ${Media.lessThan(Media.SIZE.XL)} {
          width: 280px;
        }

        ${Media.lessThan(Media.SIZE.MD)} {
          position: fixed;
          width: 100%;
          left: -100%;
          top: 60px;
          z-index: 8;
          height: calc(100vh - 60px);
          padding-top: 5px;

          &.open {
            left: 0;
          }
        }

        .filter-bar-inner {
          position: sticky;
          top: 60px;
          .filter-bar-header {
            display: flex;
            align-items: center;
            justify-content: space-between;
            padding: 23px 35px;
            border-bottom: 1px solid ${Colors.WHITE_12};

            ${Media.lessThan(Media.SIZE.LG)} {
              padding: 23px 25px;
            }

            ${Media.lessThan(Media.SIZE.MD)} {
              padding: 23px 20px;
            }

            .bar-header-left {
              color: ${Colors.PRIMARY};

              img {
                margin-right: 18px;
              }

              ${Media.lessThan(Media.SIZE.MD)} {
                color: #ffffff;

                img {
                  -webkit-filter: brightness(0) invert(1);
                  filter: brightness(0) invert(1);
                }
              }
            }

            .bar-header-right {
              color: ${Colors.WHITE_20};
              font-size: 13px;

              .ant-btn {
                color: ${Colors.WHITE_20};
                border: none;
                padding: 0;

                &.disabled {
                  pointer-events: none;
                  user-select: none;
                  cursor: default;
                }
              }
            }
          }

          .filter-bar-body {
            padding: 25px 35px;
            max-height: calc(100vh - 140px);
            overflow: auto;
            // max-height: calc(100% - 226px);
            ${Media.lessThan(Media.SIZE.LG)} {
              padding: 25px;
            }

            ${Media.lessThan(Media.SIZE.MD)} {
              padding: 25px 20px 100px 20px;
              max-height: calc(100vh - 140px);
              overflow: auto;
            }

            .filter-item {
              margin-bottom: 56px;

              &.hidden {
                display: none !important;
              }

              ${Media.lessThan(Media.SIZE.MD)} {
                margin-bottom: 30px;
              }

              .filter-item-title {
                margin-bottom: 20px;
                color: #ffffff;
              }

              .filter-item-body {
                .ant-checkbox-group {
                  display: flex;
                  flex-direction: column;

                  .ant-checkbox-wrapper {
                    padding: 10px 14px;
                    border-bottom: 1px solid #1d1d24;
                    font-size: 13px;
                    margin-right: 0;
                    background-color: #313343;

                    &:first-child {
                      border-radius: 4px 4px 0 0;
                    }

                    &:last-child {
                      border-bottom: none;
                      border-radius: 0 0 4px 4px;
                    }

                    &.ant-checkbox-wrapper-checked {
                      color: ${Colors.PRIMARY};
                    }
                  }
                }

                .radio-style-checkbox {
                  display: flex;
                  flex-direction: column;

                  .checkbox-item {
                    padding: 0;
                    border-bottom: 1px solid #1d1d24;
                    font-size: 13px;
                    margin-right: 0;
                    background-color: #313343;

                    &:first-child {
                      border-radius: 4px 4px 0 0;
                    }

                    &:last-child {
                      border-bottom: none;
                      border-radius: 0 0 4px 4px;
                    }

                    .ant-radio-wrapper {
                      margin: 0;
                      padding: 10px 14px;
                      font-size: 13px;
                      width: 100%;

                      .ant-radio {
                        .ant-radio-inner {
                          width: 18px;
                          height: 18px;
                          background-color: #20232e;
                          border-color: #20232e;
                          border-style: solid;
                          border-width: 1px;
                          border-radius: 3px;

                          &:after {
                            border-radius: 3px;
                            position: absolute;
                            display: table;
                            border: 2px solid #212330;
                            border-top: 0;
                            border-left: 0;
                            content: ' ';
                            top: 4px;
                            border-radius: 0;
                            width: 6px;
                            height: 12px;
                            background: transparent;
                            opacity: 0;
                          }
                        }
                      }
                    }

                    &.active {
                      color: ${Colors.PRIMARY};

                      .ant-radio-wrapper {
                        .ant-radio {
                          color: ${Colors.PRIMARY};
                          border-radius: 3px;

                          .ant-radio-inner {
                            background-color: ${Colors.PRIMARY} !important;
                            border-color: ${Colors.PRIMARY} !important;

                            &:after {
                              top: 7px;
                              transform: rotate(45deg) scale(1) translate(-50%, -50%);
                              opacity: 1;
                              transition: all 0.2s cubic-bezier(0.12, 0.4, 0.29, 1.46) 0.1s;
                            }
                          }
                        }
                      }
                    }
                  }
                }

                .ant-collapse {
                  background-color: transparent;

                  > .ant-collapse-item {
                    border-bottom: 2px solid ${Colors.WHITE_12};
                    margin-bottom: 22px;

                    &:last-child {
                      margin-bottom: 0;
                    }

                    > .ant-collapse-header {
                      padding: 0 20px 22px 0;
                      font-size: 13px;
                      color: ${Colors.WHITE_50};

                      .ant-collapse-arrow {
                        left: auto;
                        right: 20px;
                        padding: 0;
                        color: #ffffff;
                      }
                    }

                    > .ant-collapse-content {
                      .ant-collapse-content-box {
                        padding: 0 0 25px 0;
                      }
                    }
                  }
                }

                .list-category {
                  display: flex;
                  flex-wrap: wrap;
                  padding: 0;
                  margin: 0;

                  li {
                    list-style: none;
                    margin-right: 10px;
                    margin-bottom: 11px;

                    &:last-child {
                      margin-right: 0;
                    }

                    a {
                      color: #fff;
                      border: 1px solid ${Colors.WHITE_12};
                      border-radius: 20px;
                      padding: 3px 18px;
                      background-color: #2b2c3d;
                      font-size: 13px;
                      display: flex;
                    }
                  }
                }
              }
            }
          }
        }
      }

      .product-main-content {
        width: calc(100% - 350px);
        padding: 0 80px 40px 80px;
        position: relative;

        ${Media.lessThan(Media.SIZE.XXL)} {
          padding: 0 40px 40px 40px;
          width: calc(100% - 350px);
        }

        ${Media.lessThan(Media.SIZE.XL)} {
          width: calc(100% - 280px);
          padding: 0 30px 40px 30px;
        }

        ${Media.lessThan(Media.SIZE.MD)} {
          width: 100%;
          padding: 0 20px 40px 20px;
        }

        .sort-box {
          position: absolute;
          right: 80px;
          top: 22px;
          z-index: 1;

          .ant-btn {
            border: none;
            display: flex;
            align-items: center;
            font-size: 16px;

            img {
              height: 16px;
              margin-right: 10px;
            }

            .anticon {
              margin-left: 10px;
              font-size: 10px;
            }
          }

          ${Media.lessThan(Media.SIZE.XXL)} {
            right: 40px;
          }

          ${Media.lessThan(Media.SIZE.LG)} {
            right: 30px;
          }

          ${Media.lessThan(Media.SIZE.MD)} {
            right: 20px;
            top: 1px;

            .ant-btn {
              font-size: 12px;
              padding-right: 0;
              padding-left: 0;

              img {
                height: 12px;
              }
            }
          }
        }

        .product-tab-wrapper {
          .ant-tabs-top > .ant-tabs-nav .ant-tabs-ink-bar {
            height: 1px;
          }

          .ant-tabs {
            .ant-tabs-nav {
              margin-bottom: 40px;

              ${Media.lessThan(Media.SIZE.MD)} {
                margin-bottom: 16px;
              }

              &:before {
                border-bottom: 1px solid ${Colors.WHITE_12};
              }

              .ant-tabs-nav-list {
                .ant-tabs-tab {
                  padding: 26px 20px 26px 8px;
                  font-size: 16px;
                  color: ${Colors.WHITE_40};
                  + .ant-tabs-tab {
                    margin-left: 50px;
                  }
                  &.ant-tabs-tab-active {
                    border-bottom: 2px solid #fff100 !important;
                    .ant-tabs-tab-btn {
                      font-weight: bold;
                    }
                  }
                }

                ${Media.lessThan(Media.SIZE.MD)} {
                  .ant-tabs-tab {
                    padding: 8px 15px 8px 0;
                    font-size: 12px;

                    + .ant-tabs-tab {
                      margin-left: 10px;
                    }
                  }
                }

                ${Media.lessThan(Media.SIZE.XXS)} {
                  .ant-tabs-tab {
                    padding: 8px 12px 8px 0;
                    font-size: 12px;

                    + .ant-tabs-tab {
                      margin-left: 6px;
                    }
                  }
                }
              }
            }
          }
        }

        .container {
          padding: 0;
        }
      }
    }
  }

  ${Media.greaterThan(Media.SIZE.XXXL)} {
    .col-20-percent {
      flex: 0 0 20%;
      max-width: 20%;
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .tool-bar {
      top: 65px;
      background-color: transparent;
      backdrop-filter: none;
      position: absolute;
      padding: 0;

      .search-input {
        max-width: 100%;
        width: 100%;
        padding: 0;
        background-color: rgba(24, 26, 33, 0.7);
        background-blend-mode: none;
      }
    }
  }

  .open-filter-mobile {
    width: 100%;
    position: fixed;
    bottom: 60px;
    left: 0;
    z-index: 8;
  }
`;

@withTranslation()
@withRouter
@inject(stores => ({
  productsStore: stores.products,
}))
@observer
class Products extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
  };

  constructor(props) {
    super(props);
    const { match, t } = props;

    this._query = decodeURIComponent(match.params.query || '');
    this.state = {
      isSearch: !!match.params.query,
      openFilterMobile: false,
      filterMenuItems: [],
      // dataFilter: [
      //   {
      //     type: 'sortBy',
      //     value: match?.params?.sortBy
      //   },
      //   {
      //     type: 'categoryName',
      //     value: match?.params?.categoryName
      //   },
      //   {
      //     type: 'currency',
      //     value: match?.params?.currency
      //   },
      //   {
      //     type: 'status',
      //     value: match?.params?.status
      //   }
      // ]
    };

    this._orderMenuItems = [
      {
        name: t('products:sort.newest_to_oldest'),
        value: 'NEWEST_TO_OLDEST',
      },
      {
        name: t('products:sort.biggest_price'),
        value: 'BIGGEST_PRICE',
      },
      {
        name: t('products:sort.smallest_price'),
        value: 'SMALLEST_PRICE',
      },
      {
        name: t('products:sort.most_view'),
        value: 'MOST_VIEW',
      },
      {
        name: t('products:sort.less_view'),
        value: 'LESS_VIEW',
      },
      {
        name: t('products:sort.last_modified'),
        value: 'LAST_MODIFIED',
      },
    ];
  }

  async componentDidMount() {
    const { productsStore, t, i18n, match, history } = this.props;

    if (match.path === '/:lang/products') {
      history.replace('/products/none/none/none/none?query=');
    }

    const { success, data } = await productsStore.getProductFilterData({
      langKey: i18n.language.toUpperCase(),
    });

    if (success) {
      this.setState({
        filterMenuItems: [
          {
            name: t('products:filter.category'),
            items: data[1].value.map(item => ({
              name: item,
              value: item,
            })),
          },
          {
            name: t('products:filter.buy_currency'),
            items: [
              {
                name: 'ETH',
                value: 'ETH',
              },
              {
                name: 'USDT',
                value: 'USDT',
              },
            ],
          },
        ],
      });
    }
  }

  _onChange = (type, value) => {
    const { history, match } = this.props;
    const q = QS.parse(history.location.search).query || '';
    const data = {
      ...match.params,
    };

    const sortByParam =
      match?.params?.sortBy !== 'none' && match?.params?.sortBy !== undefined ? match?.params?.sortBy : '';
    const categoryNameParam =
      match?.params?.categoryName !== 'none' && match?.params?.categoryName !== undefined
        ? match?.params?.categoryName
        : '';
    const currencyParam =
      match?.params?.currency !== 'none' && match?.params?.currency !== undefined ? match?.params?.currency : '';
    const statusParam =
      match?.params?.status !== 'none' && match?.params?.status !== undefined ? match?.params?.status : 'SALE';

    data[type] = data[type] === 'categoryName' ? categoryNameParam : value;
    data[type] = data[type] === 'currency' ? currencyParam : value;

    const {
      sortBy = sortByParam,
      categoryName = categoryNameParam,
      currency = currencyParam,
      status = statusParam,
      // query = q,
    } = data;

    history.replace(`/products/${sortBy}/${categoryName}/${currency}/${status}?query=${q}`);

    this._onSortOrFilter(data);
  };

  _clearFilter = () => {
    const { history } = this.props;
    const querySearch = QS.parse(history.location.search).query || '';
    history.replace(`/products/none/none/none/none?query=${querySearch}`);
    const data = {
      sortBy: '',
      categoryName: '',
      currency: '',
      status: 'SALE',
      query: querySearch,
    };

    this._onSortOrFilter(data);
  };

  _onSortOrFilter = values => {
    let payload = {};

    if (values.sortBy !== 'none') {
      payload = {
        ...payload,
        sortBy: values.sortBy,
      };
    }
    if (values.categoryName !== 'none') {
      payload = {
        ...payload,
        categoryName: values.categoryName,
      };
    }
    if (values.currency !== 'none') {
      payload = {
        ...payload,
        currency: values.currency,
      };
    }
    if (values.status !== 'none') {
      payload = {
        ...payload,
        status: values.status,
      };
    }

    if (values.query) {
      payload = {
        ...payload,
        query: values.query,
      };
    }

    // eslint-disable-next-line no-console
    console.log(payload);

    // this._newProductFetchableList.fetchDataWithNewPayload(payload)
  };

  _onChangeTab = key => {
    if (+key === 1) {
      this._onChange('status', 'SALE');
    } else {
      this._onChange('status', 'RESALE');
    }
  };

  _renderOrderMenu = () => {
    const { match } = this.props;
    const { sortBy } = match.params;

    return (
      <Menu className="sortby-dropdown">
        {this._orderMenuItems.map((item, index) => (
          <Menu.Item key={index}>
            <Clickable
              key={index}
              onClick={() => this._onChange('sortBy', item.value)}
              className={classnames('menu-item', { active: item.value === sortBy })}
            >
              <Typography>{item.name}</Typography>
            </Clickable>
          </Menu.Item>
        ))}
      </Menu>
    );
  };

  _renderFilterBar = () => {
    const { openFilterMobile, filterMenuItems } = this.state;
    const { t, match } = this.props;
    const categoryNameParam =
      match?.params?.categoryName !== 'none' && match?.params?.categoryName !== undefined
        ? match?.params?.categoryName
        : '';
    const currencyParam =
      match?.params?.currency !== 'none' && match?.params?.currency !== undefined ? match?.params?.currency : '';

    return (
      <div className={classnames('product-filter-bar', { open: openFilterMobile })}>
        <div className="filter-bar-inner">
          <div className="filter-bar-header">
            <div className="bar-header-left">
              <img src={Images.FILTER_ICON} alt="Filter" />
              {t('products:filter.filter')}
            </div>
            <div className="bar-header-right">
              <Button
                className={
                  categoryNameParam !== '' || currencyParam !== '' ? 'visible-pc text-primary' : 'visible-pc disabled'
                }
                onClick={() => this._clearFilter()}
              >
                {t('products:filter.clear_filter')}
              </Button>
              <Button
                className={
                  categoryNameParam !== '' || currencyParam !== '' ? 'visible-mb text-primary' : 'visible-mb disabled'
                }
                onClick={() => this._clearFilter()}
              >
                {t('products:filter.clear_filter')}
              </Button>
            </div>
          </div>
          <div className="filter-bar-body">
            {filterMenuItems.map((parentItem, parentIndex) => (
              <div
                className={classnames(parentItem.items.length > 0 ? 'filter-item' : 'filter-item hidden')}
                key={parentIndex}
              >
                <Typography className="filter-item-title" size="large">
                  {parentItem.name}
                </Typography>
                <div className="filter-item-body">
                  <Radio.Group className="radio-style-checkbox">
                    {parentItem.items.map((item, index) => (
                      <Clickable
                        key={index}
                        onClick={() => this._onChange(parentIndex === 0 ? 'categoryName' : 'currency', item.value)}
                        className={classnames('checkbox-item', {
                          // active: (parentIndex === 0 ? categoryNameParam : currencyParam) === item.value
                          active: (parentIndex === 0 ? categoryNameParam : currencyParam) === item.value,
                        })}
                      >
                        <Radio value={item.value}>{item.name}</Radio>
                      </Clickable>
                    ))}
                  </Radio.Group>
                </div>
              </div>
            ))}
            <div className="visible-mb">
              <Button
                type="primary"
                className="close-filter-bar"
                onClick={() => this.setState({ openFilterMobile: !openFilterMobile })}
              >
                {t('products:filter.apply_filter')}
              </Button>
            </div>
          </div>
        </div>
      </div>
    );
  };

  _renderProductItem = item => {
    const { history } = this.props;

    return (
      <ProductCard
        onClick={() =>
          item.cultureNickname === 'japanese-porn'
            ? history.push(`/product-details/${item.id}`)
            : history.push(`/cultures/${item.cultureNickname}/culture-details/${item.id}`)
        }
        item={item}
      />
    );
  };

  // _onSearchSubmit = (value) => {
  //   this._newProductFetchableList.fetchDataWithNewPayload({
  //     query: value
  //   })

  //   this.setState({
  //     isSearch: true
  //   })
  // }

  _renderListBox = () => {
    const { productsStore, i18n, t, match, history } = this.props;
    const query = QS.parse(history.location.search).query || '';
    const { isSearch } = this.state;
    const sortByParam =
      match?.params?.sortBy !== 'none' && match?.params?.sortBy !== undefined ? match?.params?.sortBy : 'RANDOM';
    const categoryNameParam =
      match?.params?.categoryName !== 'none' && match?.params?.categoryName !== undefined
        ? match?.params?.categoryName
        : '';
    const currencyParam =
      match?.params?.currency !== 'none' && match?.params?.currency !== undefined ? match?.params?.currency : '';
    const statusParam =
      match?.params?.status !== 'none' && match?.params?.status !== undefined ? match?.params?.status : 'SALE';

    let payload = {
      langKey: i18n.language.toUpperCase(),
      findType: 'NEW_PRODUCT',
      randomNumber: productsStore.randomNumber,
      nickname: 'japanese-porn',
      query,
    };
    // if (match.path === '/:lang/products/category/:id') {
    //   payload.categoryIds = match.params.id
    // }
    // if (match.path === '/:lang/products/key-word/:query') {
    //   payload.query = this._query
    // }

    payload.sortBy = sortByParam;
    payload.categoryName = categoryNameParam;
    payload.currency = currencyParam;
    payload.status = statusParam;
    payload.query = query;

    return (
      <section className="idol-list">
        {isSearch && (
          <div className="title-box">
            <Typography className="section-title" bold>
              {t('common:search_results')}
            </Typography>
          </div>
        )}
        <FetchableList
          ref={ref => {
            this._newProductFetchableList = ref;
          }}
          colClassName="col-20-percent"
          colSpanXl={6}
          colSpanMd={8}
          colSpanSm={12}
          limit={12}
          noDataMessage={t('common:product_notfound')}
          keyExtractor={(item, index) => index}
          action={productsStore.getSearchProducts}
          items={productsStore.searchProducts.items}
          total={productsStore.searchProducts.total}
          page={productsStore.searchProducts.page}
          payload={payload}
          renderItem={this._renderProductItem}
        />
      </section>
    );
  };

  render() {
    const { openFilterMobile } = this.state;
    const { t, match } = this.props;
    const sortBy = match.params.sortBy || 'none';
    const status = match.params.status || 'none';

    // const { TabPane } = Tabs
    return (
      <Page
        onEndReached={() => {
          this._newProductFetchableList.loadMore();
        }}
        className="PAGE PRODUCTS"
      >
        <StyledDiv>
          <div className="top-section">
            <div className="_gradient" />
          </div>
          <div className="visible-mb">
            <Button
              type="primary"
              className="open-filter-mobile"
              onClick={() => this.setState({ openFilterMobile: !openFilterMobile })}
            >
              <img src={Images.FILTER_BLACK_ICON} alt="Filter icon" />
              {t('products:filter.filter')}
            </Button>
          </div>
          <div className="content">
            <div className="product-main-container">
              {this._renderFilterBar()}
              <div className="product-main-content">
                <div className="sort-box">
                  <Dropdown trigger="click" overlay={this._renderOrderMenu}>
                    <Button>
                      <img src={Images.SORT_ICON} alt="icon sort" />
                      <span>
                        <span>
                          {this._orderMenuItems.find(item => item?.value === sortBy)?.name || t('products:sort.sort')}
                        </span>
                        <DownOutlined />
                      </span>
                    </Button>
                  </Dropdown>
                </div>
                <div className="product-tab-wrapper">
                  <div className="ant-tabs ant-tabs-top">
                    <div className="ant-tabs-nav">
                      <div className="ant-tabs-nav-wrap">
                        <div className="ant-tabs-nav-list">
                          <div
                            className={classnames('ant-tabs-tab', {
                              'ant-tabs-tab-active bold': status === 'none' || status === 'SALE',
                            })}
                            onClick={() => this._onChangeTab(1)}
                          >
                            <div className="ant-tabs-tab-btn">{t('products:filter.new_product')}</div>
                          </div>
                          <div
                            className={classnames('ant-tabs-tab', { 'ant-tabs-tab-active bold': status === 'RESALE' })}
                            onClick={() => this._onChangeTab(2)}
                          >
                            <div className="ant-tabs-tab-btn">{t('products:filter.resale_product')}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  {/* <Tabs onChange={this._onChangeTab} defaultActiveKey={1}>
                    <TabPane
                      tab={t('products:filter.new_product')}
                      key="1"
                    />
                    <TabPane
                      tab={t('products:filter.resale_product')}
                      key="2"
                    />
                  </Tabs> */}
                  <Container fluid>{this._renderListBox()}</Container>
                </div>
              </div>
            </div>
          </div>
        </StyledDiv>
        <Footer />
      </Page>
    );
  }
}

const ProductsWrapper = props => {
  const { i18n } = useTranslation();

  return <Products {...props} key={i18n.language} />;
};

export default ProductsWrapper;
