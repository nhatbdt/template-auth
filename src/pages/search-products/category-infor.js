import { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink, withRouter } from 'react-router-dom';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';
import { Select } from 'antd';
import QS from 'query-string';

import { Colors, Images } from '../../theme';
import Media from '../../utils/media';
import Typography from '../../components/typography';
import Clickable from '../../components/clickable';
import { TABSMARKET } from './constantTab';
import { getAllowedCurrencies } from '../../utils/products';

const StyledDiv = styled.div`
  padding: 0px 15px;
  background-color: #0f1114;

  .all-category {
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    margin-top: 50px;

    .list-categories {
      display: flex;
      /* max-width: 990px; */
      max-width: 1460px;
      width: 100%;
      border-bottom: 1px solid rgba(101, 103, 107, 0.1);
      justify-content: center;
      padding-bottom: 25px;
      margin-bottom: 25px;
      gap: 12px 10px;
      flex-wrap: wrap;
      /* ${Media.lessThan(Media.SIZE.MD)} {
        flex-wrap: wrap;
      } */
      .menu-item {
        display: flex;
        justify-content: center;
        align-items: center;
        max-width: 150px;
        width: 100%;
        padding: 6px 10px;
        &:hover:not(.active_category) {
          border-radius: 90px;
          background-color: #1b1e24;
        }
        /* ${Media.lessThan(Media.SIZE.LG)} { */
        ${Media.lessThan(Media.SIZE.XXL)} {
          background-color: #1b1e24;
          border-radius: 90px;
        }
        p {
          font-family: 'Noto Sans JP', sans-serif;
          text-align: center;
          font-weight: 500;
          color: ${Colors.TEXT};
          font-size: 16px;
          ${Media.lessThan(Media.SIZE.XL)} {
            font-size: 13px;
          }
          ${Media.lessThan(Media.SIZE.SM)} {
            font-size: 11px;
          }
        }
      }
      .active_category {
        background: #045afc;
        border-radius: 90px;
      }
    }
    .market-type {
      display: flex;
      /* max-width: 990px; */
      max-width: 1200px;
      width: 100%;
      justify-content: space-between;

      .type {
        display: flex;

        .default {
          border-radius: 30px;
          background-color: #1b1e24;
          padding: 8px 20px;
          text-align: center;
          margin-right: 10px;
          font-weight: 500;
          color: ${Colors.TEXT};
        }

        .active {
          background: #00153c;
          border-radius: 9999px;
          color: #045afc;
        }
      }

      .block_sort {
        display: flex;
        flex-wrap: wrap;
        margin: 0;
      }

      .sort_by {
        display: flex;
        align-items: center;
        border-radius: 100px;
        border: 1px solid ${Colors.BORDER};
        padding: 0px 12px;
        margin-left: 10px;
        position: relative;

        .ant-select {
          min-width: 150px;
          ${Media.lessThan(Media.SIZE.SM)} {
            min-width: 130px;
          }
        }
        .ant-select-selector {
          border: none !important;
          box-shadow: none !important;
          padding-left: 22px;
          .ant-select-arrow {
            top: 50%;
          }
          .ant-select-selection-item,
          .ant-select-selection-placeholder {
            padding-right: 0px;
          }
        }
        .ant-select-arrow {
          right: 3px;
        }

        .icon_select {
          position: absolute;
        }
      }

      ${Media.lessThan(Media.SIZE.LG)} {
        flex-direction: column;
        align-items: center;
        gap: 20px;
        .block_sort {
          /* margin-top: 20px; */
          & > .sort_by:first-child {
            margin-left: 0px;
          }
        }
      }
    }
  }
`;

@withTranslation()
@withRouter
@inject(stores => ({
  categoriesStore: stores.categories,
}))
@observer
class CategoryInfo extends Component {
  static propTypes = {
    categoriesStore: PropTypes.object,
  };

  state = {
    initial: true,
    categoryDetailInfo: {},
    // currencies: [
    //   {
    //     label: 'Currency',
    //     value: 'none',
    //   },
    //   {
    //     label: 'PRX',
    //     value: 'PRX',
    //   },
    //   {
    //     label: 'ETH',
    //     value: 'ETH',
    //   },
    // ],
  };

  _fetchData = async () => {
    const { categoriesStore, location, i18n } = this.props;
    const { id = 'all' } = QS.parse(location.search) || {};

    const res = await categoriesStore.getCategoryDetail({
      id,
      langKey: i18n.language.toUpperCase(),
    });
    if (res.success) {
      this.setState({
        categoryDetailInfo: res.data,
        initial: false,
      });
    } else {
      this.setState({
        initial: false,
      });
    }
  };

  _checkToActive = id => {
    // const { match } = this.props;
    // const { params } = match;
    const { location } = this.props;
    const { id: idSelected = 'all' } = QS.parse(location.search) || {};

    // if (id === params.id) {
    if (id === idSelected) {
      return 'active_category';
    }
    return '';
  };

  tabActive = (tabItem, tabselected) => {
    if (tabselected === tabItem) {
      return 'active';
    }
    return '';
  };

  componentDidMount() {
    const { categoriesStore, history, location, i18n, t } = this.props;
    const { tab = TABSMARKET[0] } = QS.parse(location.search) || {};
    categoriesStore.getListCategory({ langKey: i18n.language.toUpperCase(), limit: 100 });

    if (!TABSMARKET.includes(tab)) {
      history.goBack();
    } else {
      this._fetchData();
    }

    const currencyOptions = getAllowedCurrencies();
    this.setState({
      allowedCurrencies: [
        {
          label: t(`products:filter.currency`),
          value: '',
        },
        ...currencyOptions,
      ],
    });
  }

  render() {
    const { t, i18n, categoriesStore, location, history } = this.props;
    const { allowedCurrencies } = this.state;
    const {
      id = 'all',
      currency = 'Currency',
      sort = 'RANDOM',
      tab = TABSMARKET[0],
      query = '',
    } = QS.parse(location.search) || {};
    const { listCategory } = categoriesStore;

    return (
      <StyledDiv className="Category-Info">
        <div className="all-category">
          <div className="list-categories">
            <NavLink
              exact
              to={`/${i18n.language}/search-products?id=all&tab=${tab}&currency=${currency}&sort=${sort}&query=${query}`}
              className={`menu-item ${this._checkToActive('all')}`}
            >
              <Typography>{t('products:filter.all_nfts')}</Typography>
            </NavLink>
            {listCategory.map(item => (
              <NavLink
                exact
                key={item.id}
                to={`/${i18n.language}/search-products?id=${item.id}&tab=${tab}&currency=${currency}&sort=${sort}&query=${query}`}
                className={`menu-item ${this._checkToActive(item.id.toString())}`}
              >
                <Typography>{item.name}</Typography>
              </NavLink>
            ))}
          </div>
          <div className="market-type">
            <div className="type">
              {TABSMARKET.map((tabItem, index) => (
                <Clickable
                  key={index}
                  className={`effect default ${this.tabActive(tabItem, tab)}`}
                  onClick={() => {
                    history.push(
                      `/search-products?id=${id}&tab=${tabItem}&currency=${currency}&sort=${sort}&query=${query}`,
                    );
                  }}
                >
                  {t(`products:filter.${tabItem}`)}
                </Clickable>
              ))}
            </div>

            <div className="block_sort">
              <div className="sort_by">
                <img src={Images.COLOR_CURRENCY_ICON} alt="currency" className="icon_select" />
                <Select
                  placement="bottomRight"
                  placeholder={t(`products:filter.currency`)}
                  // options={[
                  //   {
                  //     label: t(`products:filter.currency`),
                  //     value: '',
                  //   },
                  //   {
                  //     label: 'ETH',
                  //     value: 'ETH',
                  //   },
                  // ]}
                  options={allowedCurrencies}
                  suffixIcon={<img src={Images.COLOR_DOWN_ICON} alt="down_icon" />}
                  onChange={currency => {
                    history.push(
                      `/search-products?id=${id}&tab=${tab}&currency=${currency}&sort=${sort}&query=${query}`,
                    );
                  }}
                  defaultValue={currency}
                  dropdownMatchSelectWidth={180}
                />
              </div>
              <div className="sort_by">
                <img src={Images.COLOR_SORTBY_ICON} alt="currency" className="icon_select" />
                <Select
                  placement="bottomRight"
                  placeholder={t(`products:sort.sort`)}
                  options={[
                    {
                      label: t(`products:sort.sort`),
                      value: 'RANDOM',
                    },
                    {
                      label: t(`products:sort.newest_to_oldest`),
                      value: 'NEWEST_TO_OLDEST',
                    },
                    {
                      label: t(`products:sort.biggest_price`),
                      value: 'BIGGEST_PRICE',
                    },
                    {
                      label: t(`products:sort.smallest_price`),
                      value: 'SMALLEST_PRICE',
                    },
                    {
                      label: t(`products:sort.last_modified`),
                      value: 'LAST_MODIFIED',
                    },
                  ]}
                  suffixIcon={<img src={Images.COLOR_DOWN_ICON} alt="down_icon" />}
                  onChange={sortBy => {
                    history.push(
                      `/search-products?id=${id}&tab=${tab}&currency=${currency}&sort=${sortBy}&query=${query}`,
                    );
                  }}
                  defaultValue={sort}
                  dropdownMatchSelectWidth={220}
                />
              </div>
            </div>
          </div>
        </div>
      </StyledDiv>
    );
  }
}

export default CategoryInfo;
