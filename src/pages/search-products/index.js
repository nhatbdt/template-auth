import { inject, observer } from 'mobx-react';
import PropTypes from 'prop-types';
import QS from 'query-string';
import { Component } from 'react';
import { useTranslation, withTranslation } from 'react-i18next';
import { Link, withRouter } from 'react-router-dom';
import styled from 'styled-components';

import Footer from '../../app/footer';
import FetchableList from '../../components/fetchable-list';
import Page from '../../components/page';
import ProductCard from '../../components/product-card-new';
import { WHITE_CHEVRON_UP_ICON } from '../../theme/images';
import Media from '../../utils/media';
import CategoryInfor from './category-infor';
import CategoryInput from './category-search';
import { currencyNative, TABS, TABSMARKET } from './constantTab';

const StyledDiv = styled.div`
  font-family: 'Noto Sans JP', sans-serif;
  width: 100%;
  margin: 0 auto;
  position: relative;
  ${Media.lessThan(Media.SIZE.MD)} {
    padding: 0 15px;
  }
  .list-items {
    padding: 40px 40px;
    max-width: 1540px;
    margin: 0 auto;
    ${Media.lessThan(Media.SIZE.MD)} {
      padding: 0;
    }
  }
  .button-scroll {
    position: fixed;
    bottom: calc(100vh - 370px);
    right: 40px;
    background: red;
    z-index: 11111;
    width: 32px;
    height: 32px;
    background: #376587;
    border-radius: 81px;
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .product-card-new {
    min-width: unset !important;
    max-width: 430px;
    margin: 0 auto;
  }
`;

@withTranslation()
@withRouter
@inject(stores => ({
  productsStore: stores.products,
  lendingStore: stores.lending,
}))
@observer
class CategoryDetails extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
    lendingStore: PropTypes.object,
  };

  state = {
    isShowButtonScroll: false,
    randomNumber: Math.floor(Math.random() * 999),
  };

  _renderProductItem = (item, index) => {
    const { history } = this.props;
    // const { tab } = QS.parse(location.search)

    return (
      <ProductCard
        // onClick={() => history.push(tab === TABS.lending ? `/product-details/${item.id}/lending` : `/product-details/${item.id}`)}
        onClick={() => history.push(`/product-details/${item.id}`)}
        item={item}
        index={index}
        className="product-card-new"
        styleType="black"
        isShowStatus
        isShowLending={item.lendingTitle === 'LENDING'}
      />
    );
  };

  onScrollTop = () => {
    window.scrollTo({ top: 0, behavior: 'smooth' });
  };

  onScroll = offset => {
    const { isShowButtonScroll } = this.state;
    if (offset <= 0 && !isShowButtonScroll) {
      this.setState({ isShowButtonScroll: !this.state.isShowButtonScroll });
    }
    if (offset > 0 && isShowButtonScroll) {
      this.setState({ isShowButtonScroll: false });
    }
  };

  // tabCurrent = () => {
  tabCurrent = tab => {
    let params_query;

    // const { match } = this.props;
    // const { tab } = match.params;

    if (tab === TABS.resale_product) {
      params_query = {
        status: 'RESALE',
        productType: 'NORMAL',
      };
    } else if (tab === TABS.lending) {
      params_query = {
        status: '',
        productType: 'NORMAL',
      };
    } else if (tab === TABS.auction) {
      params_query = {
        status: '',
        findType: 'NEW_PRODUCT',
        productType: 'AUCTION',
      };
    } else {
      params_query = {
        status: 'SALE',
        productType: 'NORMAL',
      };
    }
    return params_query;
  };

  render() {
    // const { productsStore, match, location, t, i18n } = this.props;
    const { productsStore, lendingStore, location, t, i18n } = this.props;
    const { isShowButtonScroll, randomNumber } = this.state;
    // const query = QS.parse(location.search).query || '';
    // const { id, currency, sort } = match.params;
    const {
      id = 'all',
      currency = '',
      sort = 'RANDOM',
      query = '',
      tab = TABSMARKET[0],
    } = QS.parse(location.search) || {};

    const gutter = [
      { xs: 8, sm: 15, md: 20, xl: 20 },
      { xs: 8, sm: 15, md: 20, xl: 20 },
    ];

    return (
      <Page
        onEndReached={() => {
          this._fetchableList.loadMore();
        }}
        onScroll={this.onScroll}
        className="SEARCH-CATEGORY-DETAIL"
      >
        <div>
          <CategoryInput t={t} />

          <CategoryInfor i18n={i18n} t={t} location={location} />

          <StyledDiv>
            <div className="category-main-container">
              <div className="list-items">
                <FetchableList
                  showLoadMoreButton
                  className="list-top-product"
                  ref={ref => {
                    this._fetchableList = ref;
                  }}
                  keyExtractor={(item, index) => index}
                  noDataMessage={t('common:no_item')}
                  action={tab === TABS.lending ? lendingStore.getListLending : productsStore.getProductItems}
                  items={tab === TABS.lending ? lendingStore.lendings?.items : productsStore.productItems?.items}
                  page={tab === TABS.lending ? lendingStore.lendings?.page : productsStore.productItems?.page}
                  total={tab === TABS.lending ? lendingStore.lendings?.total : productsStore.productItems?.total}
                  colSpanXl={6}
                  colSpanMd={8}
                  colSpanSm={12}
                  colSpanXs={24}
                  limit={12}
                  gutter={gutter}
                  payload={{
                    query,
                    findType: tab === TABS.lending ? 'ALL' : 'NEW_PRODUCT',
                    langKey: i18n.language?.toUpperCase(),
                    categoryId: id === 'all' ? '' : id,
                    currency: currencyNative.includes(currency) ? currency : '',
                    sortBy: sort?.toUpperCase(),
                    randomNumber,
                    ...this.tabCurrent(tab),
                  }}
                  renderItem={this._renderProductItem}
                />
              </div>
            </div>
            {isShowButtonScroll && (
              <Link className="button-scroll" onClick={this.onScrollTop}>
                <img src={WHITE_CHEVRON_UP_ICON} alt="scroll top" />
              </Link>
            )}
          </StyledDiv>
        </div>

        <Footer />
      </Page>
    );
  }
}

const CategoryDetailsWrapper = props => {
  const { i18n } = useTranslation();

  return <CategoryDetails {...props} key={i18n.language} />;
};

export default CategoryDetailsWrapper;
