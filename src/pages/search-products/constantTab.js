export const TABS = {
  new_product: 'new_product',
  resale_product: 'resale_product',
  lending: 'lending',
  auction: 'auction',
};

export const TABSMARKET = ['new_product', 'resale_product', 'lending', 'auction'];

export const currencyNative = ['USDT', 'WMATIC'];

export const sortBy = [
  {
    sort: 'sort',
    value: 'RANDOM',
  },
  {
    sort: 'newest_to_oldest',
    value: 'NEWEST_TO_OLDEST',
  },
  {
    sort: 'biggest_price',
    value: 'BIGGEST_PRICE',
  },
  {
    sort: 'smallest_price',
    value: 'SMALLEST_PRICE',
  },
  {
    sort: 'last_modified',
    value: 'LAST_MODIFIED',
  },
];
