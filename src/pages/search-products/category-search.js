import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import QS from 'query-string';

import { COLOR_SEARCH_ICON, COLOR_ENTER_ICON } from '../../theme/images';
import Media from '../../utils/media';
import { Input } from 'antd';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { TABSMARKET } from './constantTab';

const StyledDiv = styled.div`
  height: 100px;
  margin-top: 92px;
  background: #15171c;
  position: relative;
  ${Media.lessThan(Media.SIZE.LG)} {
    margin-top: 60px;
    height: 75px;
  }

  .block-search {
    max-width: 672px;
    width: 90%;
    display: flex;
    align-items: center;
    background: #15171c;
    border: 1px solid #65676b;
    box-shadow: 0px 10px 15px -3px rgba(0, 0, 0, 0.1), 0px 4px 6px -4px rgba(0, 0, 0, 0.1);
    border-radius: 9999px;
    position: absolute;
    left: 50%;
    bottom: 0px;
    transform: translate(-50%, 50%);
    padding: 7px 10px 7px 27px;
    ${Media.lessThan(Media.SIZE.MD)} {
      padding: 5px 5px 5px 27px;
    }

    input {
      border: none;
      color: #fff;
      font-weight: 400;
      background-color: transparent;
      &:focus {
        box-shadow: none;
      }
      &::placeholder {
        color: #6a6a6a;
      }
    }
    img {
      transition: 0.5s;
    }

    .search-btn {
      cursor: pointer;
      height: 44px;
      &:hover {
        opacity: 0.5;
      }
      ${Media.lessThan(Media.SIZE.MD)} {
        height: 36px;
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    margin-top: 60px;
  }
`;

const CategoryInfo = () => {
  const history = useHistory();
  const { t } = useTranslation('products');
  const [search, setSearch] = useState('');
  const {
    id = 'all',
    currency = 'Currency',
    sort = 'RANDOM',
    query = '',
    tab = TABSMARKET[0],
  } = QS.parse(window.location.search) || {};

  useEffect(() => {
    setSearch(query);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleSearch = () =>
    history.push(
      `/search-products?id=${id}&tab=${tab}&currency=${currency}&sort=${sort}&query=${search?.toLowerCase()}`,
    );

  const handleKeyDown = e => {
    if (e?.key === 'Enter') handleSearch();
  };

  return (
    <StyledDiv className="CATEGORY-SEARCH">
      <div className="block-search">
        <img src={COLOR_SEARCH_ICON} alt="" />
        <Input
          placeholder={t('filter.type_keywords')}
          onKeyDown={handleKeyDown}
          value={search}
          defaultValue={query}
          onChange={e => setSearch(e.target.value)}
        />
        <img className="search-btn" src={COLOR_ENTER_ICON} alt="search" onClick={handleSearch} />
      </div>
    </StyledDiv>
  );
};

export default CategoryInfo;
