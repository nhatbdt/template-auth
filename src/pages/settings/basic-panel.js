import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import { withTranslation } from 'react-i18next';
// import { CopyOutlined } from '@ant-design/icons'

import Typography from '../../components/typography';
import Clickable from '../../components/clickable';
import Button from '../../components/button';
import Field from '../../components/field';
import FileInput from '../../components/file-input';
import Input from '../../components/input';
import { Images, Colors } from '../../theme';
import { history } from '../../store';
import Misc from '../../utils/misc';
import Media from '../../utils/media';
import { Divider } from 'antd';
import Thumbnail from '../../components/thumbnail';
import i18n from '../../translations/i18n';
import ERROR_MESSAGES from '../../translations/error-messages';

const StyledDiv = styled.div`
  max-width: 1000px;
  padding: 50px;
  margin: 0 auto;
  ${Media.lessThan(Media.SIZE.MD)} {
    padding: 55px 20px;
  }
  ${Media.lessThan(Media.SIZE.SM)} {
    padding: 55px 0;
  }

  .max-allowed-size {
    font-size: 11px;
    text-align: center;
    color: ${Colors.TEXT};
  }

  .border-thumbnail {
    margin: 0 auto;
  }

  .title {
    font-weight: 600;
    font-size: 26px;
    color: ${Colors.TITLE};
  }

  .line {
    margin: 10px 0;
    background: ${Colors.LINE};
    opacity: 0.5;
  }

  .content {
    width: 100%;

    .flex-box {
      display: flex;
      /* flex-wrap: wrap; */
      gap: 60px 62px;
      max-width: 100%;
      padding-top: 40px;
      ${Media.lessThan(Media.SIZE.LG)} {
        flex-direction: column;
      }

      .avatar {
        .error-box {
          .error-message {
            text-align: center;
            margin: 0 auto;
          }
        }
      }
      ._left {
        ._label {
          display: none;
          margin-bottom: 17px;
          color: ${Colors.TEXT};
          ${Media.lessThan(Media.SIZE.LG)} {
            display: block;
          }
        }
        .input-file {
          .border-thumbnail {
            background-color: #65676b;
            position: unset;
            ${Media.lessThan(Media.SIZE.LG)} {
              margin: auto;
            }
          }
        }
        .__name {
          text-align: center;
          margin: 0 auto;
          padding-top: 16px;
          color: ${Colors.TEXT};
          font-weight: 400;
          font-size: 12px;
        }
      }

      ._right {
        flex: 1;
        width: 100%;
        .field-box {
          margin-bottom: 20px;

          .field {
            &.username-field {
              .ant-input {
                height: 44px;
                color: ${Colors.TEXT};
                border: none;
                border-radius: 14px;
                background-color: ${Colors.BOX_BACKGROUND};
                padding: 0 14px;
                &:focus {
                  border: 1px solid ${Colors.BLUE_1};
                }
                &::placeholder {
                  color: ${Colors.PLACEHOLDER};
                }
              }
            }

            &.email-field {
              .ant-input-affix-wrapper {
                padding: 0;
                height: 44px;
                background-color: ${Colors.BOX_BACKGROUND};
                border: none;
                border-radius: 14px;
                .ant-input-prefix {
                  padding: 0 14px;
                  border-right: 1px solid ${Colors.BORDER};
                }
                .ant-input {
                  color: ${Colors.TEXT};
                  &::placeholder {
                    color: ${Colors.PLACEHOLDER};
                  }
                }
              }

              &.disabled {
                .ant-input-affix-wrapper,
                .ant-input-prefix,
                .ant-input-affix-wrapper-disabled .ant-input[disabled] {
                  background-color: #0c2338;
                }
              }
            }
          }
          &:last-child {
            margin-bottom: 0;
          }

          ._label {
            margin-bottom: 17px;
            color: ${Colors.TEXT};
          }

          .public-address-box {
            border: none;
            border-radius: 14px;
            overflow: hidden;
            display: flex;
            flex-direction: column;
            margin-bottom: 23px;

            ._content-box {
              background-color: #0c2338;
              display: flex;
              justify-content: space-between;
              align-items: center;
              min-height: 44px;
              padding-left: 14px;
              padding-right: 14px;
              width: 100%;

              .typography {
                color: ${Colors.TEXT};

                &.private-key {
                  max-width: 100%;
                  white-space: nowrap;
                  overflow: hidden;
                  text-overflow: ellipsis;
                }
              }
              .anticon {
                font-size: 20px;
                color: #9aa8bd;
              }
              .actions {
                width: 80px;
                display: flex;
                justify-content: space-around;
                align-items: center;
              }
            }

            ._action-box {
              display: flex;
              flex: 1;
              justify-content: flex-end;
              align-items: center;
              padding: 0 22px;

              > * {
                margin-right: 21px;
                font-size: 14px;
                height: 43px;
                padding: 0 39px;

                &:last-child {
                  margin-right: 0;
                  color: ${Colors.RED_2};
                  border: 1px solid ${Colors.RED_2};
                }
              }
            }
          }
        }

        .bottom-box {
          display: flex;
          justify-content: flex-start;

          button {
            background-color: ${Colors.BUTTON_BACKGROUND};
            color: ${Colors.BUTTON_TEXT};
            font-weight: 400;
            font-size: 16px;
            line-height: 24px;
            width: 100%;
            min-height: 50px;
            &:hover {
              opacity: 0.8;
            }
          }
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .content {
      width: 100%;

      .field-box {
        .public-address-box {
          ._content-box {
            padding-left: 15px;
            padding-right: 15px;
            height: 50px;

            p {
              white-space: nowrap;
              overflow: hidden;
              text-overflow: ellipsis;
              margin-right: 10px;
            }
          }
        }
      }
    }
  }
`;

const validationSchema = yup.object().shape({
  name: yup.string().nullable().max(30, 'MAX_LENGTH_30').required('NAME_REQUIRED'),
  bio: yup.string().nullable().max(30, 'MAX_LENGTH_30'),
  email: yup.string().email('EMAIL_INVALID').max(30, 'MAX_LENGTH_30').nullable().required('EMAIL_REQUIRED'),
});

const ALLOWED_MAX_SIZE_IMAGE = 5 * 1024 * 1024;
@withTranslation(['settings', 'common'])
@inject(stores => ({
  authStore: stores.auth,
  usersStore: stores.users,
  assetsStore: stores.assets,
}))
@observer
class BasicPanel extends Component {
  static propTypes = {
    authStore: PropTypes.object,
    usersStore: PropTypes.object,
    assetsStore: PropTypes.object,
  };

  state = {
    user: {},
    loading: false,
    view: false,
  };

  async componentDidMount() {
    const { usersStore, authStore } = this.props;

    const { success, data } = await usersStore.getUserInfo(
      authStore.initialData?.userId || localStorage.getItem('USER_ID'),
    );
    if (success) {
      this.setState({
        user: data,
      });
    }
  }

  _onSubmit = async values => {
    const { assetsStore } = this.props;

    this.setState({
      loading: true,
    });

    if (values?.userImage?.file) {
      const { success, data } = await assetsStore.uploadFiles({
        files: [values.userImage.file],
        type: 'USER_PROFILE',
      });
      if (success) {
        let dataImage = data[0];
        values.userImage = dataImage;
        this._updateUserInfo(values);
      }
    } else {
      this._updateUserInfo(values);
    }
  };

  _updateUserInfo = async values => {
    const { usersStore, authStore } = this.props;
    const { user } = this.state;
    const { success } = await usersStore.updateUserInfo({
      id: user.userId,
      ...values,
    });

    if (success) {
      await authStore.getInitialData();
      history.push('/my-page');
    } else {
      this.setState({
        loading: false,
      });
    }
  };

  _onCopyToClipboard = value => {
    const { t } = this.props;

    Misc.copyToClipboard(value, t('common:copy_to_clipboard'));
  };

  _onValidate = values => {
    let errors = {};
    const { userImage } = values;
    if (userImage?.file && userImage?.file?.size > ALLOWED_MAX_SIZE_IMAGE) {
      const errorMessages = ERROR_MESSAGES[i18n.language]['error-messages'];
      errors = { userImage: errorMessages.ERROR_OVER_ALLOWED_SIZE };
    }

    return errors;
  };

  _renderForm = ({ handleSubmit }) => {
    const { authStore, t, privateKey } = this.props;
    const { loading, user, view } = this.state;
    const url = user?.userImage;

    return (
      <Form className="content">
        <div className="flex-box">
          <div className="_left field-box avatar">
            <Typography className="_label" bold size="big">
              {t('user_image')}
            </Typography>
            <Field
              name="userImage"
              type="image"
              url={url}
              size={128}
              className="input-file"
              placeholderUrl={Images.GRAY_AVATA_DEFAULT}
              accept="image/png,image/gif,image/jpg,image/jpeg"
              component={FileInput}
              // maxAllowedSize={5}
            />
            <Field.Label className="placeholder" key={'userImage'}>
              {t('change_image')}
            </Field.Label>
            <p className="max-allowed-size">{`(${t('settings:max_allowed_size')} 5MB)`}</p>
          </div>
          <div className="_right">
            <div className="field-box">
              <Typography className="_label" bold size="big">
                {t('public_address')}
              </Typography>
              <div className="public-address-box">
                <div className="_content-box">
                  <Typography size="large">{authStore.initialData?.publicAddress}</Typography>
                  <Clickable onClick={() => this._onCopyToClipboard(authStore.initialData?.publicAddress)}>
                    <img src={Images.GRAY_COPY_ICON} alt="copy" />
                  </Clickable>
                </div>
              </div>
            </div>
            {privateKey && (
              <div className="field-box">
                <Typography className="_label" bold size="big">
                  {t('private_key')}
                </Typography>
                <div className="public-address-box">
                  <div className="_content-box">
                    <Typography className="private-key" size="large">
                      {view ? privateKey : '*'.repeat(64)}
                    </Typography>
                    <div className="actions">
                      <Clickable
                        onClick={() =>
                          this.setState(state => ({
                            view: !state.view,
                          }))
                        }
                      >
                        <img src={view ? Images.GRAY_UNVIEW_ICON : Images.GRAY_VIEW_ICON} alt="copy" />
                      </Clickable>
                      <Clickable onClick={() => this._onCopyToClipboard(privateKey)}>
                        <img src={Images.GRAY_COPY_ICON} alt="view" />
                      </Clickable>
                    </div>
                  </div>
                </div>
              </div>
            )}

            <div className="field-box">
              <Typography className="_label" bold size="big">
                {t('account_name')}
              </Typography>
              <Field className="username-field" name="name" simple component={Input} placeholder={t('enter_text')} />
            </div>
            <div className="field-box">
              <Typography className="_label" bold size="big">
                {t('email_address')}
              </Typography>
              <Field
                className={authStore.initialData?.email ? "email-field disabled" : "email-field"}
                name="email"
                simple
                component={Input}
                prefix={<Thumbnail url={Images.GRAY_EMAIL_ICON} size={16} />}
                placeholder={t('Example@email.com')}
                disabled={authStore.initialData?.email}
              />
            </div>
            <div className="bottom-box">
              <Button onClick={handleSubmit} loading={loading}>
                {t('save')}
              </Button>
            </div>
          </div>
        </div>
      </Form>
    );
  };

  render() {
    const { user } = this.state;
    const { t } = this.props;

    return (
      <StyledDiv>
        <Typography className="title" bold>
          {t('profile_settings')}
        </Typography>
        <Divider className="line" />
        <Formik
          enableReinitialize
          validateOnChange={false}
          validateOnBlur={false}
          initialValues={{
            name: user.name,
            bio: user.bio,
            email: user.email,
            userImage: user.userImage,
          }}
          validate={this._onValidate}
          validationSchema={validationSchema}
          onSubmit={this._onSubmit}
          component={this._renderForm}
        />
      </StyledDiv>
    );
  }
}

export default BasicPanel;
