import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { WALLET_ADAPTERS } from '@web3auth/base';
import Page from '../../components/page';
import Footer from '../../app/footer';
import Media from '../../utils/media';
import { Colors } from '../../theme';
import BasicPanel from './basic-panel';
import { useWeb3Auth } from '../../contexts/web3auth/web3auth';

const Content = styled.div`
  border-top: solid 1px rgb(0 0 0 / 5%);
  display: flex;

  .side-menu {
    width: 349px;
    height: 100%;
    // border-right: solid 1px rgba(255, 255, 255, 0.07);
    margin-top: 80px;
    .side-item {
      height: 88px;
      padding: 20px 53px;
      display: flex;
      align-items: center;
      // border-bottom: solid 1px rgba(255, 255, 255, 0.07);
      opacity: 0.6;
      transition: background-color 0.2s, opacity 0.2s;
      background-color: transparent;
      p {
        color: ${Colors.BLUE_1};
      }

      &.active {
        background-color: transparent;
        color: #ffffff;
        opacity: 1;
        p {
          color: #ffffff;
        }
      }
    }
  }

  .page-body {
    flex: 1;
    padding: 90px 20px;
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    flex-direction: column;

    .side-menu {
      width: auto;
      border-right: none;
      border-bottom: solid 1px rgb(0 0 0 / 5%);
      display: flex;

      .side-item {
        flex: 1;
        height: 70px;
        padding: 0;
        justify-content: center;
      }
    }

    .page-body {
      flex: none;
      padding: 20px 15px;
    }
  }
`;

const Settings = () => {
  const { getPrivateKey } = useWeb3Auth();
  const [privateKey, setPrivateKey] = useState('');

  useEffect(() => {
    const init = async () => {
      const privKey = await getPrivateKey();
      setPrivateKey(privKey);
    };
    const web3authAdapter = localStorage.getItem('Web3Auth-cachedAdapter');
    const isLMAdapter = web3authAdapter && web3authAdapter === WALLET_ADAPTERS.METAMASK;
    if (!isLMAdapter) init();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Page>
      <Content>
        <div className="page-body">
          <BasicPanel privateKey={privateKey} />
        </div>
      </Content>
      <Footer />
    </Page>
  );
};

export default Settings;
