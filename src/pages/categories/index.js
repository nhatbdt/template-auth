import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation, useTranslation } from 'react-i18next';
import { Row, Col } from 'antd';

import Page from '../../components/page';
import Container from '../../components/container';
import Clickable from '../../components/clickable';
import Typography from '../../components/typography';
import Footer from '../../app/footer';
import Media from '../../utils/media';
import { BLACK_2, BLACK_1 } from '../../theme/colors';

const StyledDiv = styled.div`
  padding-bottom: 100px;

  .top-section {
    height: 400px;
    position: relative;

    background-position: center; // background-image: url(''); BACKGROUND
    background-size: cover;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
    opacity: 0.9;

    ._gradient {
      height: 400px;
      background-image: linear-gradient(to top, ${BLACK_2}, transparent);
    }
  }

  .content {
    margin-top: -320px;
    position: relative;

    section {
      .title-box {
        margin-bottom: 20px;
        display: flex;
        align-items: center;

        .section-title {
          font-size: 22px;
        }

        ._total {
          margin-left: 23px;
          color: #ffffff;
          opacity: 0.5;
          margin-top: 3px;
        }
      }

      .category-box {
        height: 155px;
        background-color: ${BLACK_1};
        border-radius: 4px;
        box-shadow: 0 0 5px 0 rgba(0, 0, 0, 0.04);
        background-size: cover;
        background-position: center;
        overflow: hidden;

        ._mask {
          width: 100%;
          height: 100%;
          display: flex;
          justify-content: center;
          align-items: center;
          background-color: rgba(0, 0, 0, 0.5);
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    position: relative;

    .tool-bar {
      position: absolute;
      left: 0;
      right: 0;
      top: 0;
    }

    .top-section {
      height: 188px;

      ._gradient {
        height: 188px;
      }
    }

    .content {
      margin-top: -150px;

      section {
        .title-box {
          margin-bottom: 11px;
          justify-content: space-between;

          .section-title {
            font-size: 18px;
          }

          ._total {
            margin-left: 0;
            font-size: 12px;
          }
        }
      }
    }
  }
`;

@withTranslation()
@inject(stores => ({
  productsStore: stores.products,
}))
@observer
class Categories extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
  };

  componentDidMount() {
    const { productsStore, i18n } = this.props;

    productsStore.getProductCategories({
      langKey: i18n.language.toUpperCase(),
    });
  }

  _renderListBox = () => {
    const { productsStore, history } = this.props;

    return (
      <section>
        {/* <div className="title-box"> */}
        {/*  <Typography className="section-title" bold> */}
        {/*    {t('common:header.category')} */}
        {/*  </Typography> */}
        {/* </div> */}
        <Row
          gutter={[
            { xs: 15, sm: 15, md: 22 },
            { xs: 15, sm: 15, md: 22 },
          ]}
        >
          {productsStore.categories.map(item => (
            <Col key={item.id} md={4} xs={12}>
              <Clickable
                className="category-box"
                onClick={() => history.push(`/products/none/${item.name}/none/none?query=`)}
                style={{
                  backgroundImage: `url('${item.image}')`,
                }}
              >
                <div className="_mask">
                  <Typography size="large">{item.name}</Typography>
                </div>
              </Clickable>
            </Col>
          ))}
        </Row>
      </section>
    );
  };

  render() {
    return (
      <Page>
        <StyledDiv>
          <div className="top-section">
            <div className="_gradient" />
          </div>
          <div className="content">
            <Container fluid>{this._renderListBox()}</Container>
          </div>
        </StyledDiv>
        <Footer />
      </Page>
    );
  }
}

const WrapCategoryRout = props => {
  const { i18n } = useTranslation();

  return <Categories {...props} key={i18n.language} />;
};

export default WrapCategoryRout;
