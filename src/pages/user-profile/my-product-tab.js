import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';

import Container from '../../components/container';
import Typography from '../../components/typography';
// import Button from '../../components/button'
import FetchableList from '../../components/fetchable-list';
import ProductCard from '../../components/product-card-new';
import { history } from '../../store';
import { forwardInnerRef } from '../../utils/high-order-functions';
import Media from '../../utils/media';
import { Colors, Images } from '../../theme';
// import ButtonSwitch from '../../components/button-switch';
import MaskLoading from '../../components/mask-loading';
import { sign, approveTokenErc721A } from '../../utils/web3';
import Confirmable from '../../components/confirmable';
import Clickable from '../../components/clickable';
import Button from '../../components/button';
import { LENDING_STATUS, STAKING_STATUS } from '../../constants/statuses'
import OfferModal from './../product-details/sell-box/offer-modal';

const StyledDiv = styled.div`
  padding-top: 25px;

  .PRODUCT-CARD-NEW {
    margin-bottom: 20px;
  }

  .container {
    padding: 0px;
    .fetchable-no-data {
      p {
        color: #65676b;
      }
    }
  }
  .title-box {
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
    align-items: center;
    margin-bottom: 33px;
    background-color: ${Colors.BG_HEADER};
    border-radius: 8px;
    padding: 12px 20px;

    ._left {
      display: flex;
      align-items: center;
      gap: 4px 10px;
      ${Media.lessThan(Media.SIZE.SM)} {
        flex-direction: column;
        align-items: flex-start;
      }

      ._total {
        font-weight: 400;
        font-size: 18px;
        color: ${Colors.BLUE_TEXT};
      }
      .typography {
        &.section-title {
          font-weight: 500;
          font-size: 18px;
          color: ${Colors.HEADER};
        }
      }
    }
    ._right {
    }
  }

  .divider {
    height: 1px;
    background: #3d4756;
    width: 100%;
    margin-top: 80px;
    margin-bottom: 70px;
    opacity: 0.21;
  }

  .outer-product-item {
    .product-item-action-box {
      padding: 15px 24px 0;
      display: flex;
      flex-wrap: wrap;
      gap: 10px 10px;
      justify-content: center;
      align-items: center;

      > button {
        width: calc(50% - 10px);
        height: fit-content;
        min-height: 36px;
        padding: 0 10px;
        font-size: 13px;
        font-weight: bold;
        border-radius: 30px;
        background-color: ${Colors.BUTTON_BACKGROUND};

        &.light {
          background-color: white;
          color: #222;
          // border: 1px solid ${Colors.PRIMARY};
        }

        &.danger {
          background: #9b040d;
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    padding-top: 15px;
  }
`;

const gutter = [
  { xs: 8, sm: 15, md: 25, xl: 25 },
  { xs: 8, sm: 15, md: 20, xl: 20 },
];

@withTranslation()
@forwardInnerRef
@inject(stores => ({
  productsStore: stores.products,
  offersStore: stores.offers,
  authStore: stores.auth,
}))
@observer
class MyProductTab extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
    offersStore: PropTypes.object,
    authStore: PropTypes.object,
  };

  state = {
    isShowOwnedItems: true,
    isShowSellingItems: false,
    isShowResaleItems: false,
    isShowResoldItems: false,
  };

  loadMore = () => {
    this._fetchableList.loadMore();
  };

  _onProductActionButtonClick = (item, sellAction) => {
    history.push(`/product-details/${item.id}/${sellAction}`);
  };

  _sign = async () => {
    const { authStore } = this.props;
    const userNonceResult = await authStore.getUserNonce(authStore.initialData.userId);

    if (!userNonceResult.success) {
      throw userNonceResult.data;
    }

    const signature = await sign(userNonceResult.data.nonce, authStore.initialData.publicAddress);

    return signature;
  };

  _onChangeResellOfferFlag = async (product, checked) => {
    const { offersStore, authStore, t } = this.props;

    MaskLoading.open({});

    try {
      const signature = await this._sign();

      if (!checked) {
        await offersStore.resellOfferFlag({
          productId: product?.id,
          signature,
        });

        product.setData({
          canOfferFlag: false,
        });

        MaskLoading.close();
        return null;
      }

      const registerOfferFlagResult = await offersStore.registerOfferFlag({
        productId: product.id,
      });

      if (!registerOfferFlagResult.success) throw registerOfferFlagResult.data;

      const approveNftHash = await approveTokenErc721A(
        authStore.initialData.publicAddress,
        registerOfferFlagResult.data.approvePublicKey,
        product.tokenId,
        t,
      );

      const resellOfferFlag = await offersStore.resellOfferFlag({
        productId: product?.id,
        signature,
        approveNftHash,
      });

      if (!resellOfferFlag.success) throw resellOfferFlag.data;

      await Confirmable.open({
        content: t('product_details:offer.resale_offer_flag_success'),
        hideCancelButton: true,
      });

      MaskLoading.close();

      history.push(`/product-details/${product.id}`);
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
    }

    MaskLoading.close();

    return null;
  };

  _renderProductItem = ({ item, sellAction, lendingAction, showCountOffer = false }) => {
    const { t } = this.props;
    const isStatusLending = [
      LENDING_STATUS.WAIT_LENDING,
      LENDING_STATUS.IN_LENDING,
      LENDING_STATUS.LENDING,
      LENDING_STATUS.WAIT_BORROW,
      LENDING_STATUS.BORROW,
      LENDING_STATUS.EXPIRATION,
      LENDING_STATUS.WAIT_WITHDRAW
    ].includes(item?.lendingDetailDTO?.status);

    const isStatusStaking = [
      STAKING_STATUS.UN_STAKING_PROGRESS,
      STAKING_STATUS.STAKING_PROGRESS,
      STAKING_STATUS.STAKING,
    ].includes(item?.stakingStatus);
    
    return (
      <div className="outer-product-item">
        <ProductCard
          onClick={() => history.push(`/product-details/${item.id}`)}
          item={item}
          showCountOffer={showCountOffer}
          className="card-light"
          isShowStatus
          isShowCategoryName
          showActions
        />
        {!isStatusStaking && (sellAction || lendingAction) ? (
          <div className="product-item-action-box">
            {sellAction && (
              <Button
                className={sellAction !== 'sell' ? 'danger bold' : 'bold'}
                disabled={!!isStatusLending}
                onClick={() => this._onProductActionButtonClick(item, sellAction)}
              >
                {sellAction === 'sell' ? t('product_details:sell.resell') : t('product_details:sell.cancel_listing_2')}
              </Button>
            )}
            {lendingAction && (
              <Button
                className={lendingAction !== 'create-lending' ? 'danger bold' : 'bold'}
                disabled={!!isStatusLending}
                onClick={() => this._onProductActionButtonClick(item, lendingAction)}
              >
                {lendingAction === 'create-lending'
                  ? t('product_details:lending.lending')
                  : t('product_details:lending.cancel_lending')}
              </Button>
            )}

            {lendingAction && (
              <Button
                className={'bold'}
                disabled={!!isStatusLending}
                onClick={() => this._offerModal.open(item)}
              >
                {t('product_details:offer.offer_setting')}
              </Button>
            )}

            {/* <ButtonSwitch
              disabled={['NEW', 'INIT'].includes(item.productBid?.status) && item.typeSale === 'LIMITED'}
              checked={item.canOfferFlag}
              onChange={checked => this._onChangeResellOfferFlag(item, checked)}
            /> */}

            {/* <ButtonSwitch
              disabled={['NEW', 'INIT'].includes(item.productBid?.status) && item.typeSale === 'LIMITED'}
              checked={item.canOfferFlag}
              onChange={checked => this._onChangeResellOfferFlag(item, checked)}
            /> */}

            <OfferModal
              ref={ref => {
                this._offerModal = ref;
              }}
              history={history}
            />
          </div>
        ) : null}
      </div>
    );
  };

  _renderMyProductSection = () => {
    const { productsStore, authStore, i18n, t } = this.props;
    const { isShowOwnedItems } = this.state;

    return (
      <>
        <Clickable
          className="title-box"
          onClick={() => this.setState(state => ({ isShowOwnedItems: !state.isShowOwnedItems }))}
        >
          <div className="_left">
            <Typography size="big" className="section-title" bold>
              {t('user_profile:owned_products')}
            </Typography>
            <Typography className="_total">
              <b>{productsStore.myProducts?.total || authStore.initialData?.ownedCount}</b> {t('common:item')}
            </Typography>
          </div>
          <div className="_right">
            <div>
              <img src={isShowOwnedItems ? Images.GRAY_ARROW_UP_ICON : Images.GRAY_ARROW_DOWN_ICON} alt="arrow-down" />
            </div>
          </div>
        </Clickable>
        {isShowOwnedItems && (
          <FetchableList
            showLoadMoreButton
            loadMoreColor="blue"
            loadMoreText={t('common:see_more')}
            ref={ref => {
              this._fetchableList = ref;
            }}
            noDataMessage={t('common:no_assets')}
            keyExtractor={(item, index) => index}
            action={productsStore.getMyProducts}
            items={productsStore.myProducts?.items}
            total={productsStore.myProducts?.total}
            page={productsStore.myProducts?.page}
            colSpanXl={6}
            colSpanMd={8}
            colSpanSm={12}
            colSpanXs={24}
            limit={12}
            gutter={gutter}
            payload={{
              langKey: i18n.language.toUpperCase(),
            }}
            renderItem={item => this._renderProductItem({ item, sellAction: 'sell', lendingAction: 'create-lending' })}
          />
        )}
      </>
    );
  };

  _renderMySellProductSection = () => {
    const { productsStore, authStore, i18n, t } = this.props;
    const { isShowSellingItems } = this.state;

    return (
      <>
        <Clickable
          onClick={() => this.setState(state => ({ isShowSellingItems: !state.isShowSellingItems }))}
          className="title-box"
        >
          <div className="_left">
            <Typography size="giant" className="section-title" bold>
              {t('user_profile:sell_products')}
            </Typography>
            <Typography className="_total">
              <b>{productsStore.mySellProducts.length || authStore.initialData?.sellingCount}</b> {t('common:item')}
            </Typography>
          </div>
          <div className="_right">
            <div>
              <img
                src={isShowSellingItems ? Images.GRAY_ARROW_UP_ICON : Images.GRAY_ARROW_DOWN_ICON}
                alt="arrow down"
              />
            </div>
          </div>
        </Clickable>
        {isShowSellingItems && (
          <FetchableList
            noDataMessage={t('common:no_assets')}
            keyExtractor={(item, index) => index}
            action={productsStore.getMySellProducts}
            items={productsStore.mySellProducts}
            colSpanXl={6}
            colSpanMd={8}
            colSpanSm={12}
            colSpanXs={24}
            limit={12}
            gutter={gutter}
            payload={{
              langKey: i18n.language.toUpperCase(),
            }}
            renderItem={item => this._renderProductItem({ item, sellAction: 'cancel' })}
          />
        )}
      </>
    );
  };

  _renderMySoldProductSection = () => {
    const { productsStore, authStore, i18n, t } = this.props;
    const { isShowResoldItems } = this.state;

    return (
      <>
        <Clickable
          onClick={() => this.setState(state => ({ isShowResoldItems: !state.isShowResoldItems }))}
          className="title-box"
        >
          <div className="_left">
            <Typography size="giant" className="section-title" bold>
              {t('user_profile:sold_products')}
            </Typography>
            <Typography className="_total">
              <b>{productsStore.mySoldProducts.length || authStore.initialData?.resoldCount}</b> {t('common:item')}
            </Typography>
          </div>
          <div className="_right">
            <div>
              <img src={isShowResoldItems ? Images.GRAY_ARROW_UP_ICON : Images.GRAY_ARROW_DOWN_ICON} alt="arrow down" />
            </div>
          </div>
        </Clickable>
        {isShowResoldItems && (
          <FetchableList
            noDataMessage={t('common:no_assets')}
            keyExtractor={(item, index) => index}
            action={productsStore.getMySoldProducts}
            items={productsStore.mySoldProducts}
            colSpanXl={6}
            colSpanMd={8}
            colSpanSm={12}
            colSpanXs={24}
            limit={12}
            gutter={gutter}
            payload={{
              langKey: i18n.language.toUpperCase(),
            }}
            renderItem={item => this._renderProductItem({ item })}
          />
        )}
      </>
    );
  };

  _renderMyOfferProductSection = () => {
    const { isShowResaleItems } = this.state;
    const { productsStore, authStore, i18n, t } = this.props;
    const showCountOffer = true;

    return (
      <>
        <Clickable
          onClick={() => this.setState(state => ({ isShowResaleItems: !state.isShowResaleItems }))}
          className="title-box"
        >
          <div className="_left">
            <Typography size="giant" className="section-title" bold>
              {t('user_profile:offer_products')}
            </Typography>
            <Typography className="_total">
              <b>{productsStore.myOfferProducts.length || authStore.initialData?.resaleCount}</b> {t('home:item')}
            </Typography>
          </div>
          <div className="_right">
            <div>
              <img src={isShowResaleItems ? Images.GRAY_ARROW_UP_ICON : Images.GRAY_ARROW_DOWN_ICON} alt="arrow down" />
            </div>
          </div>
        </Clickable>
        {isShowResaleItems && (
          <FetchableList
            noDataMessage={t('common:no_assets')}
            keyExtractor={(item, index) => index}
            action={productsStore.getMyOfferProducts}
            items={productsStore.myOfferProducts}
            colSpanXl={6}
            colSpanMd={8}
            colSpanSm={12}
            colSpanXs={24}
            limit={12}
            gutter={gutter}
            payload={{
              langKey: i18n.language.toUpperCase(),
            }}
            renderItem={item => this._renderProductItem({ item, sellAction: 'sell', showCountOffer, lendingAction: 'create-lending' })}
          />
        )}
      </>
    );
  };

  render() {
    return (
      <StyledDiv>
        <Container fluid>
          {this._renderMyProductSection()}
          {this._renderMySellProductSection()}
          {this._renderMyOfferProductSection()}
          {this._renderMySoldProductSection()}


        </Container>
      </StyledDiv>
    );
  }
}

export default MyProductTab;
