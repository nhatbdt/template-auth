import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';
// eslint-disable-next-line import/no-extraneous-dependencies
import { CheckOutlined } from '@ant-design/icons';
import { Dropdown } from 'antd';
import classnames from 'classnames';

import Container from '../../components/container';
import FetchableList from '../../components/fetchable-list';
import ProductCard from '../../components/product-card-new';
import Clickable from '../../components/clickable';
import Typography from '../../components/typography';
import { history } from '../../store';
import { forwardInnerRef } from '../../utils/high-order-functions';
import Media from '../../utils/media';
import { Colors, Images } from '../../theme';

const StyledDiv = styled.div`
  margin-top: 40px;
  .no-padding {
    padding: 0px;
  }
  .toolbar {
    height: 48px;
    margin-bottom: 30px;
    display: flex;
    align-items: center;
    .container {
      padding: 0px;
      max-width: 2000px;
    }
    .toolbar-content {
      display: flex;
      justify-content: flex-start;

      .ant-dropdown-trigger {
        width: 160px;
        border-radius: 50px;
        padding: 6px 20px;
        border: 1px solid #d1d5db;
        justify-content: space-between;
        p {
          color: #65676b;
        }
        .content_selected {
          display: flex;
          align-items: center;
        }
      }

      .icon_filter {
        margin-right: 10px;
      }
      .button {
        display: flex;
        align-items: center;

        .typography {
          margin-right: 5px;
        }
      }
    }
  }

  .sort_by {
    display: flex;
    align-items: center;
    border-radius: 100px;
    border: 1px solid #d1d5db;
    padding: 0px 12px;
    margin-left: 10px;
    position: relative;

    .ant-select {
      width: 150px;
    }
    .ant-select-selector {
      border: none;
      box-shadow: none;
      padding-left: 22px;

      .ant-select-selection-item {
        color: #65676b;
      }
      .ant-select-selection-placeholder {
        color: #65676b;
      }
      .ant-select-arrow {
        top: 50%;
      }
      .ant-select-selection-item,
      .ant-select-selection-placeholder {
        padding-right: 0px;
      }
    }
    .ant-select-arrow {
      right: 3px;
    }

    .icon_select {
      position: absolute;
    }
  }
  ${Media.lessThan(Media.SIZE.MD)} {
    padding-top: 15px;
  }
`;
const MenuBox = styled.div`
  margin-top: 9px;
  padding: 25px 35px;
  background-color: #376587;
  border-radius: 5px;
  box-shadow: 0 3px 6px -4px rgb(0 0 0 / 12%), 0 6px 16px 0 rgb(0 0 0 / 8%), 0 9px 28px 8px rgb(0 0 0 / 5%);

  .group-item {
    margin-bottom: 10px;
    border-bottom: 1px solid #c8c8c8;
    padding-bottom: 10px;

    &:last-child {
      margin-bottom: 0;
      border-bottom: none;
      padding-bottom: 0;
    }

    .group-item-title {
      margin-bottom: 10px;
    }
  }

  .menu-item {
    position: relative;
    margin-bottom: 8px;
    display: flex;
    align-items: center;

    p {
      color: #ffffff;
    }

    &:last-child {
      margin-bottom: 0;
    }

    .check-icon {
      position: absolute;
      left: -15px;
      font-size: 11px;
      margin-right: 5px;
      opacity: 0;
      transition: opacity 0.2s;
      color: ${Colors.BLUE_1};
    }

    &.active {
      .check-icon {
        opacity: 1;
      }

      .typography {
        color: ${Colors.BLUE_1};
      }
    }
  }
`;

@withTranslation()
@forwardInnerRef
@inject(stores => ({
  productsStore: stores.products,
}))
@observer
class Gifts extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
  };

  state = {
    filter: '',
  };

  loadMore = () => {
    this._fetchableList.loadMore();
  };

  _onFilterMenuClick = item => {
    this.setState({
      filter: item,
    });

    this._fetchableList.fetchDataWithNewPayload({
      session: item.value,
    });
  };

  _renderOrderMenu = () => {
    const { t } = this.props;
    const { filter } = this.state;
    const options = [
      {
        name: t('product_details:bid.on_going'),
        value: 'BIDDING',
      },
      {
        name: t('product_details:bid.success'),
        value: 'SUCCESS',
      },
      {
        name: t('product_details:bid.fail'),
        value: 'FAILED',
      },
    ];

    return (
      <MenuBox>
        {options.map((item, index) => (
          <Clickable
            key={index}
            onClick={() => this._onFilterMenuClick(item)}
            className={classnames('menu-item', {
              active: item.value === filter.value,
            })}
          >
            <CheckOutlined className="check-icon" />
            <Typography>{item.name}</Typography>
          </Clickable>
        ))}
      </MenuBox>
    );
  };

  _renderProductItem = item => <ProductCard onClick={() => history.push(`/product-details/${item.id}`)} item={item} />;

  render() {
    const { productsStore, i18n, t } = this.props;
    const { filter } = this.state;

    return (
      <StyledDiv className="GIFTS-TAB">
        <div className="toolbar">
          <Container>
            <div className="toolbar-content">
              <Dropdown trigger="click" overlay={this._renderOrderMenu}>
                <Clickable className="button">
                  <div className="content_selected">
                    <img src={Images.COLOR_FILTER_ICON} alt="COLOR_FILTER_ICON" className="icon_filter" />
                    <Typography>{filter?.name || t('common:filter.filter') || 'Filter'}</Typography>
                  </div>
                  <img src={Images.COLOR_DOWN_ICON} alt="down_icon" />
                </Clickable>
              </Dropdown>
            </div>
          </Container>
        </div>

        <Container fluid className="no-padding">
          <FetchableList
            ref={ref => {
              this._fetchableList = ref;
            }}
            noDataMessage={t('common:no_assets')}
            keyExtractor={(item, index) => index}
            action={productsStore.getMyBidProducts}
            items={productsStore.myBidProducts.items}
            total={productsStore.myBidProducts.total}
            page={productsStore.myBidProducts.page}
            payload={{
              langKey: i18n.language.toUpperCase(),
              session: 'BIDDING',
            }}
            renderItem={this._renderProductItem}
          />
        </Container>
      </StyledDiv>
    );
  }
}

export default Gifts;
