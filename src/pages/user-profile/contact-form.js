import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Formik, Form } from 'formik';
// import * as yup from 'yup'
import { inject, observer } from 'mobx-react';
import { Translation } from 'react-i18next';

import Input from '../../components/input';
import Loading from '../../components/loading';
import Button from '../../components/button';
import Field from '../../components/field';
import Confirmable from '../../components/confirmable';
import Typography from '../../components/typography';
import { BLACK_1, BLACK_2, RED, RED_2, BLUE_1 } from '../../theme/colors';
import Media from '../../utils/media';
import Container from '../../components/container';
// import Thumbnail from '../../components/thumbnail'
// import Clickable from '../../components/clickable'
import Page from '../../components/page';

const StyledDiv = styled.div`
  .loading {
    height: 550px;
  }

  .top-box {
    height: 262px;
    background-size: cover;
    background-position: center;
    display: flex;
    justify-content: flex-end;
    background-color: ${BLACK_1};

    .button-group {
      margin-right: 24px;
      margin-top: 24px;
      display: flex;
      box-shadow: 0 0 12px 0 rgba(0, 0, 0, 0.11);
      height: 32px;
      border-radius: 17px;
      overflow: hidden;
      background-color: white;

      div {
        height: 100%;
        width: 37px;
        display: flex;
        justify-content: center;
        align-items: center;
        border-right: 1px solid #70707012;

        &.rounded {
          width: 33px;
        }
      }
    }
  }

  .user-info-box {
    margin-bottom: 70px;

    .container {
      display: flex;

      .avatar {
        margin-top: -131px;
        box-shadow: 0 0 12px 0 rgba(0, 0, 0, 0.16);
        min-width: 0;
        z-index: 1;
      }

      ._right {
        padding-left: 33px;

        ._label {
          margin-top: -61px;
          height: 41px;
          padding: 0 73px;
          display: flex;
          align-items: center;
          background-color: ${BLACK_2};
          border-radius: 0 21px 21px 0;
          box-shadow: 0 0 12px 0 rgba(0, 0, 0, 0.16);
          margin-left: -73px;
          margin-bottom: 43px;

          .typography {
            opacity: 0.66;
          }
        }

        .user-name {
          font-size: 32px;
          margin-bottom: 12px;
        }

        .public-address {
          display: flex;
          align-items: center;

          .typography {
            opacity: 0.35;
            margin-right: 18px;
          }

          img {
            margin-top: -3px;
          }
        }
      }
    }
  }

  .error-message {
    display: none;
  }
  .input-container {
    display: flex;
    align-items: center;
    margin-bottom: 20px;

    input {
      border: 1px solid #cccccc;
      &:focus {
        border: 1px solid ${BLUE_1};
      }
      &.error {
        border: 1px solid ${RED};
      }
    }

    .readonly {
      input {
        color: rgba(51, 51, 51, 0.5) !important;
        background: rgba(51, 51, 51, 0.1) !important;
      }
    }
    p {
      width: 30%;
      span {
        color: ${RED};
      }
    }

    > div {
      width: 70%;
      padding-left: 10px;
    }

    .error {
      color: ${RED};
      width: 100%;
    }
  }

  .action-box {
    text-align: center;
    margin-top: 30px;
  }

  .content {
    background-color: inherit;
    padding: 36px 66px 66px;

    .label-box {
      display: flex;
      align-items: center;

      .hint {
        margin-left: 10px;
        border: solid 1px #e51109;
        width: 18px;
        height: 18px;
        border-radius: 9px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: #e51109;
        font-size: 15px;
      }
    }

    .divider {
      display: flex;
      align-items: center;
      justify-content: center;
      padding: 0 60px;

      .line {
        width: 1px;
        height: 100%;
        background-color: #707070;
        opacity: 0.3;
      }
    }
  }

  .descript-box {
    background-color: #eaf0fc;
    padding: 30px;
    border-radius: 8px;
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .content {
      padding: 20px 0;
    }

    .input-container {
      flex-direction: column;
      p {
        width: 100%;
        padding-bottom: 10px;
        span {
          color: red;
        }
      }

      > div {
        width: 100%;
        padding-left: 0;
      }
    }

    .user-info-box {
      .container {
        flex-direction: column;

        .avatar {
          margin: 0 auto;
          margin-top: -131px;
        }

        ._right {
          padding-left: 0;

          ._label {
            margin-top: 15px;
            border-radius: 21px;
            margin-left: 0;
          }
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.XS)} {
    .user-info-box .container ._right .public-address {
      .typography {
        font-size: 13px;
      }
      .effect {
        display: none;
      }
    }
  }
`;

@inject(stores => ({
  authStore: stores.auth,
  usersStore: stores.users,
  identityStore: stores.identities,
}))
@observer
class SellModal extends Component {
  static propTypes = {
    authStore: PropTypes.object,
    usersStore: PropTypes.object,
    identityStore: PropTypes.object,
  };

  state = {
    loading: false,
  };

  _onSubmit = async (values, t, resetForm) => {
    this.setState({
      loading: true,
    });

    try {
      const { identityStore, user, language } = this.props;
      const res = await identityStore.registerIdentity({ userId: user?.userId, ...values });
      this.setState({
        loading: true,
      });

      if (res?.success) {
        Confirmable.open({
          content:
            language === 'ja' ? (
              <>
                {t('user_profile:contact_form.message.success1')}
                <br />
                {t('user_profile:contact_form.message.success2')}
                <br />
                {t('user_profile:contact_form.message.success3')}
                <br />
                {t('user_profile:contact_form.message.success4')}
              </>
            ) : (
              <>
                {t('user_profile:contact_form.message.success1')}
                <br />
                {t('user_profile:contact_form.message.success2')}
              </>
            ),
          hideCancelButton: true,
          acceptButtonText: t('common:close'),
          width: language === 'ja' ? 400 : 700,
        });
        this.props.onReload();
        resetForm();
      } else {
        Confirmable.open({
          content: t('user_profile:contact_form.message.error'),
          hideCancelButton: true,
          acceptButtonText: t('common:close'),
        });
      }
    } catch (e) {
      let errorMessage = t('user_profile:contact_form.message.error');
      if (e === 'ERROR_TIMEOUT') {
        errorMessage = 'Time out';
      }

      Confirmable.open({
        content: errorMessage,
        hideCancelButton: true,
        acceptButtonText: t('common:close'),
      });

      // eslint-disable-next-line no-console
      console.error(e);
    }

    this.setState({
      loading: false,
    });
  };

  _validate = (values, t) => {
    let errors = {};
    const regex = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
    if (!values.benefit) {
      errors.benefit = t('user_profile:contact_form.message.benefit_required');
    }
    if (!values.name) {
      errors.name = t('user_profile:contact_form.message.name_required');
    }
    if (!values.phoneNumber) {
      errors.phoneNumber = t('user_profile:contact_form.message.phone_required');
    } else if (values.phoneNumber % 1 !== 0) {
      errors.phoneNumber = t('user_profile:contact_form.message.phone_format');
    } else if (values.phoneNumber.toString().length > 20) {
      errors.phoneNumber = t('user_profile:contact_form.message.phone_format');
    }
    if (!values.address) {
      errors.address = t('user_profile:contact_form.message.address_required');
    }

    if (!values.email) {
      errors.email = t('user_profile:contact_form.message.email_required');
    } else if (!regex.test(values.email)) {
      errors.email = t('user_profile:contact_form.message.email_format');
    }

    return errors;
  };

  _renderForm = ({ handleSubmit, values, errors, touched }, t) => {
    const { authStore, user } = this.props;
    const { loading } = this.state;

    return (
      <Form onSubmit={handleSubmit}>
        <div className="content">
          <div className="content-side">
            <div className="bottom-box">
              <div className="input-container">
                <Typography>
                  {t('user_profile:contact_form.benefit')}
                  <span>*</span>
                </Typography>
                <div>
                  <Field
                    className="input-box"
                    type="text"
                    name="benefit"
                    placeholder=""
                    boundedBelow={1}
                    simple
                    component={Input}
                    maxLength={255}
                    // value={values?.benefit || ''}
                  />
                  {touched.benefit && errors.benefit && <Typography className="error">{errors.benefit}</Typography>}
                </div>
              </div>
              <div className="input-container">
                <Typography>
                  {t('user_profile:contact_form.public_address')}
                  <span>*</span>
                </Typography>
                <Field
                  className="input-box readonly"
                  type="text"
                  name="public_address"
                  value={authStore.initialData.publicAddress}
                  placeholder=""
                  boundedBelow={1}
                  simple
                  component={Input}
                  readOnly
                />
              </div>
              <div className="input-container">
                <Typography>{t('user_profile:contact_form.des_benefit')}</Typography>
              </div>
              <div className="input-container">
                <Typography>
                  {t('user_profile:contact_form.name')}
                  <span>*</span>
                </Typography>
                <div>
                  <Field
                    className="input-box"
                    type="text"
                    name="name"
                    placeholder=""
                    boundedBelow={1}
                    simple
                    component={Input}
                    maxLength={50}
                    // value={values?.name}
                  />
                  {touched.name && errors.name && <Typography className="error">{errors.name}</Typography>}
                </div>
              </div>
              <div className="input-container">
                <Typography>
                  {t('user_profile:contact_form.address')}
                  <span>*</span>
                </Typography>
                <div>
                  <Field
                    className="input-box"
                    type="text"
                    name="address"
                    placeholder=""
                    boundedBelow={1}
                    simple
                    component={Input}
                    maxLength={255}
                    // value={values?.address}
                  />
                  {touched.address && errors.address && <Typography className="error">{errors.address}</Typography>}
                </div>
              </div>
              <div className="input-container">
                <Typography>
                  {t('user_profile:contact_form.phone_number')}
                  <span>*</span>
                </Typography>
                <div>
                  <Field
                    className="input-box"
                    type="number"
                    name="phoneNumber"
                    placeholder=""
                    boundedBelow={1}
                    simple
                    component={Input}
                    // value={values?.phoneNumber}
                  />
                  {touched.phoneNumber && errors.phoneNumber && (
                    <Typography className="error">{errors.phoneNumber}</Typography>
                  )}
                </div>
              </div>
              <div className="input-container">
                <Typography>
                  {t('user_profile:contact_form.email')}
                  <span>*</span>
                </Typography>
                <div>
                  <Field
                    className="input-box"
                    type="text"
                    name="email"
                    placeholder=""
                    boundedBelow={1}
                    simple
                    component={Input}
                    maxLength={255}
                    // value={values?.email}
                  />
                  {touched.email && errors.email && <Typography className="error">{errors.email}</Typography>}
                </div>
              </div>

              <div className="descript-box">
                <Typography>{t('user_profile:contact_form.des1')}</Typography>
                <Typography>{t('user_profile:contact_form.des2')}</Typography>
                <Typography>{t('user_profile:contact_form.des3')}</Typography>
                <Typography>{t('user_profile:contact_form.des4')}</Typography>
              </div>

              <div className="action-box">
                <Button
                  size="large"
                  disabled={
                    !values.name ||
                    !values.phoneNumber ||
                    !values.email ||
                    !values.address ||
                    !values.benefit ||
                    (values.name === user.name &&
                      values.phoneNumber === user.phoneNumber &&
                      values.address === user.address &&
                      values.benefit === user.benefit &&
                      values.email === user.email)
                  }
                  onClick={handleSubmit}
                  background={RED_2}
                  loading={loading}
                  type="submit"
                  className="blue"
                >
                  {t('user_profile:contact_form.submit')}
                </Button>
              </div>
            </div>
          </div>
        </div>
      </Form>
    );
  };

  render() {
    const { initing } = this.state;
    // const { user } = this.props;

    return (
      <Page>
        <StyledDiv>
          <Container>
            {initing ? (
              <Loading size="small" className="loading" />
            ) : (
              <Translation>
                {t => (
                  <Formik
                    enableReinitialize
                    validateOnChange
                    validateOnBlur
                    initialValues={{
                      benefit: '',
                      name: '',
                      address: '',
                      email: '',
                      phoneNumber: '',
                    }}
                    validate={values => this._validate(values, t)}
                    onSubmit={(values, { resetForm }) => this._onSubmit(values, t, resetForm)}
                    component={data => this._renderForm(data, t)}
                  />
                )}
              </Translation>
            )}
          </Container>
        </StyledDiv>
      </Page>
    );
  }
}

export default SellModal;
