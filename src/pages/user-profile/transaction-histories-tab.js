/* eslint-disable no-return-assign */
import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withTranslation } from 'react-i18next';
import { inject, observer } from 'mobx-react';
import moment from 'moment';
import { Pagination } from 'antd';

import { Colors, Images } from '../../theme';
import Typography from '../../components/typography';
import Thumbnail from '../../components/thumbnail';
import NoDataBox from '../../components/no-data-box';
import Container from '../../components/container';
import Media from '../../utils/media';
import Misc from '../../utils/misc';
import Format from '../../utils/format';
import Clickable from '../../components/clickable';
import { history } from '../../store';
import { PAGINATION_PAGE_SIZE } from '../../configs';

const StyledDiv = styled.div`
  margin-top: 40px;

  .container {
    padding: 0px;
  }

  .top-box {
    display: flex;
    padding-bottom: 29px;
    border-bottom: 2px solid rgb(0 0 0 / 15%);
    margin-bottom: 17px;

    .typography {
      font-size: 13px;
    }
  }

  .date-box {
    position: relative;

    .item-box-comment {
      position: absolute;
      right: -11px;
      top: -4px;
    }
  }

  .table-outer {
    width: 100%;
    overflow-y: auto;

    table {
      min-width: 954px;
      width: 100%;

      thead {
        tr {
          th {
            font-size: 14px;
            font-weight: bold;
            color: ${Colors.HEADER};
            text-align: left;
            text-transform: capitalize;
            padding-bottom: 10px;

            .typography {
              color: ${Colors.TEXT};
            }

            &:nth-child(1) {
              /* width: 10%; */
              width: 90px;
              /* background-color: red; */
            }

            &:nth-child(2) {
              /* width: 10%; */
              width: 80px;
            }

            &:nth-child(3) {
              /* width: 10%; */
              width: 140px;
            }

            &:nth-child(4) {
              /* width: 10%; */
              width: 100px;
            }

            &:nth-child(5) {
              /* width: 10%; */
              width: 90px;
            }

            &:nth-child(6) {
              /* width: 10%; */
              width: 50px;
            }

            &:nth-child(7) {
              /* width: 11%; */
              width: 150px;
            }

            &:nth-child(8) {
              /* width: 5%; */
              width: 150px;
              text-align: right;
            }

            /* &:nth-child(10) {
              width: 11%;
            } */
          }
        }
      }

      tbody {
        tr {
          height: 53px;
          border-bottom: 1px solid #292d33;

          td {
            color: ${Colors.TEXT};
            .transfer-icon {
              opacity: 0.3;
              width: 28px;
            }
            .horizon {
              display: flex;
              align-items: center;

              .transaction-link {
                display: flex;
                align-items: center;
              }

              .avatar {
                margin-right: 9px;
                min-width: 0;
              }

              .user-name {
                font-size: 14px;
                color: ${Colors.TEXT};
                overflow: hidden;
                text-overflow: ellipsis;
                max-width: 180px;
                white-space: nowrap;
                flex: 1;
                margin-right: 5px;
              }
            }

            .date {
              color: ${Colors.TEXT};
            }
            .date-box {
              p {
                text-align: right;
              }
            }
          }
          .NEW {
            color: #27add8;
          }
          .OFFERING {
            color: #dfa819;
          }
          .BUYING {
            color: #27add8;
          }
          .CANCEL {
            color: #c5c5c5;
          }
          .SUCCESS {
            color: #05a532;
          }
          .WAIT_CONFIRM {
            color: #05a532;
          }
          .TOKEN_PENDING {
            color: #27add8;
          }
          .FAIL {
            color: #c81a1a;
          }
          .WAIT_LENDING {
            color: #dfa819;
          }
          .LENDING {
            color: #27add8;
          }
          .IN_LENDING {
            color: #27add8;
          }
          .WAIT_BORROW {
            color: #dfa819;
          }
          .BORROW {
            color: #27add8;
          }
          .FINISH {
            color: #05a532;
          }
          .EXPIRATION {
            color: #c81a1a;
          }
          .WAIT_CANCEL,
          .WAIT_WITHDRAW {
            color: #05a532;
          }
          & > td:first-child {
            color: #045afc;
            font-weight: 500;
          }
        }
      }
    }
  }

  .ant-pagination {
    text-align: center;
    margin-top: 30px;
    .ant-pagination-options {
      display: none;
    }

    .ant-pagination-item {
      border-color: #9aa8bd;
      a {
        color: #9aa8bd;
      }
    }

    .ant-pagination-item-active {
      border-color: #3cc8fc;
      a {
        color: #3cc8fc;
      }
    }

    .ant-pagination-next {
      button {
        border: 1px solid ${Colors.TEXT} !important;
        svg {
          color: ${Colors.TEXT} !important;
        }
      }
      &:hover {
        button {
          border: 1px solid #3cc8fc;
          svg {
            color: #3cc8fc;
          }
        }
      }
      &.ant-pagination-disabled {
        button {
          border: 1px solid #434343 !important;
          svg {
            color: #434343 !important;
          }
        }
        &:hover {
          button {
            border: 1px solid #434343 !important;
            svg {
              color: #434343 !important;
            }
          }
        }
      }
    }

    .ant-pagination-prev {
      button {
        border: 1px solid ${Colors.TEXT} !important;
        svg {
          color: ${Colors.TEXT} !important;
        }
      }
      &:hover {
        button {
          border: 1px solid ${Colors.TEXT} !important;
          svg {
            color: ${Colors.TEXT} !important;
          }
        }
      }
      &.ant-pagination-disabled {
        button {
          border: 1px solid #434343 !important;
          svg {
            color: #434343 !important;
          }
        }
        &:hover {
          button {
            border: 1px solid #434343 !important;
            svg {
              color: #434343 !important;
            }
          }
        }
      }
    }

    .ant-pagination-item-container .ant-pagination-item-ellipsis {
      color: ${Colors.TEXT} !important;
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    margin-top: 15px;
    .table-outer {
      overflow-x: auto;

      table {
        table-layout: fixed;

        thead {
          tr {
            th {
              &:nth-child(1) {
                width: 130px;
              }

              &:nth-child(2) {
                width: 130px;
              }

              &:nth-child(3) {
                width: 200px;
              }

              &:nth-child(4) {
                width: 100px;
              }

              &:nth-child(5) {
                width: 150px;
              }

              &:nth-child(6) {
                width: 150px;
              }

              &:nth-child(7) {
                width: 180px;
              }

              /* &:nth-child(8) {
                width: 50px;
              } */
              &:nth-child(9) {
                width: 200px;
              }
              /* &:nth-child(10) {
                width: 150px;
              } */
            }
          }
        }
      }
    }
  }
`;

@withTranslation('product_details')
@inject(stores => ({
  product: stores.products.currentProductDetails,
  productsStore: stores.products,
}))
@observer
class TransactionHistoriesTab extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
  };

  state = {
    initial: true,
    data: [],
    total: 0,
    comment: '',
  };

  async componentDidMount() {
    const page = 1;
    this._getTransactionsHistories(page);
  }

  _getTransactionsHistories = async page => {
    const { productsStore, i18n } = this.props;

    const res = await productsStore.getMyTransactionHistories({
      page,
      limit: PAGINATION_PAGE_SIZE,
      langKey: i18n.language.toUpperCase(),
    });

    if (res?.success) {
      this.setState({
        data: res?.data?.result,
        initial: false,
        total: res?.data?.total,
      });
    }
  };

  _onClickPage = page => {
    this._getTransactionsHistories(page);
    window.scroll({
      top: 100,
      behavior: 'smooth',
    });
  };

  _onGotoProductDetail = item => () => {
    history.push(`/product-details/${item.productId}`);
  };

  _renderItem = (item, index) => {
    const { t } = this.props;
    const timeFormat = 'YYYY/M/D HH:mm';

    return (
      <tr key={index}>
        <td>
          {item.historyOrderType === 'BID'
            ? t('transaction_history_status.auction')
            : item.historyOrderType === 'NORMAL'
            ? t('transaction_history_status.normal')
            : item.historyOrderType === 'OFFER'
            ? t('transaction_history_status.offer')
            : item.historyOrderType === 'CREDIT'
            ? t('transaction_history_status.credit_card')
            : item.historyOrderType === 'CRYPTO'
            ? t('transaction_history_status.crypto')
            : item.historyOrderType === 'BORROW'
            ? t('transaction_history_status.borrow')
            : item.historyOrderType === 'LENDING'
            ? t('transaction_history_status.lending')
            : item.historyOrderType === 'GACHA'
            ? t('transaction_history_status.gacha')
            : item.historyOrderType === 'AIRDROP'
            ? t('transaction_history_status.airdrop')
            : t('transaction_history_status.resale')}
        </td>
        <td
          className={`
          ${
            item?.historyStatus === 'NEW'
              ? 'NEW'
              : item?.historyStatus === 'OFFERING'
              ? 'OFFERING'
              : item?.historyStatus === 'BUYING'
              ? 'BUYING'
              : item?.historyStatus === 'CANCEL'
              ? 'CANCEL'
              : item?.historyStatus === 'SUCCESS' || item?.historyStatus === 'COMPLETED'
              ? 'SUCCESS'
              : item?.historyStatus === 'WAIT_CONFIRM' || item?.historyStatus === 'WAIT_TO_TRANSFER'
              ? 'WAIT_CONFIRM'
              : item?.historyStatus === 'INIT'
              ? 'INIT'
              : item?.historyStatus === 'TOKEN_PENDING'
              ? 'TOKEN_PENDING'
              : item?.historyStatus === 'WAIT_LENDING'
              ? 'WAIT_LENDING'
              : item?.historyStatus === 'LENDING'
              ? 'LENDING'
              : item?.historyStatus === 'IN_LENDING'
              ? 'IN_LENDING'
              : item?.historyStatus === 'WAIT_BORROW'
              ? 'WAIT_BORROW'
              : item?.historyStatus === 'BORROW'
              ? 'BORROW'
              : item?.historyStatus === 'FINISH'
              ? 'FINISH'
              : item?.historyStatus === 'EXPIRATION'
              ? 'EXPIRATION'
              : item?.historyStatus === 'WAIT_CANCEL'
              ? 'WAIT_CANCEL'
              : 'FAIL'
          }
        `}
        >
          {item?.historyStatus === 'NEW'
            ? t('offer.buyer_status.NEW')
            : item?.historyStatus === 'OFFERING'
            ? t('offer.buyer_status.OFFERING')
            : item?.historyStatus === 'BUYING'
            ? t('offer.buyer_status.BUYING')
            : item?.historyStatus === 'CANCEL'
            ? t('offer.buyer_status.CANCEL')
            : item?.historyStatus === 'SUCCESS' || item?.historyStatus === 'COMPLETED'
            ? t('offer.buyer_status.SUCCESS')
            : item?.historyStatus === 'WAIT_CONFIRM' || item?.historyStatus === 'WAIT_TO_TRANSFER'
            ? t('offer.buyer_status.WAIT_CONFIRM')
            : item?.historyStatus === 'INIT'
            ? t('offer.buyer_status.INIT')
            : item?.historyStatus === 'TOKEN_PENDING'
            ? t('offer.buyer_status.TOKEN_PENDING')
            : item?.historyStatus === 'WAIT_LENDING'
            ? t('offer.buyer_status.WAIT_LENDING')
            : item?.historyStatus === 'LENDING'
            ? t('offer.buyer_status.LENDING')
            : item?.historyStatus === 'IN_LENDING'
            ? t('offer.buyer_status.IN_LENDING')
            : item?.historyStatus === 'WAIT_BORROW'
            ? t('offer.buyer_status.WAIT_BORROW')
            : item?.historyStatus === 'BORROW'
            ? t('offer.buyer_status.BORROW')
            : item?.historyStatus === 'FINISH'
            ? t('offer.buyer_status.FINISH')
            : item?.historyStatus === 'EXPIRATION'
            ? t('offer.buyer_status.EXPIRATION')
            : item?.historyStatus === 'WAIT_CANCEL'
            ? t('offer.buyer_status.WAIT_CANCEL')
            : t('offer.buyer_status.FAIL')}
        </td>
        <td>
          <div className="horizon">
            <Clickable className="transaction-link" onClick={this._onGotoProductDetail(item)}>
              <Thumbnail
                size={30}
                rounded
                className="avatar"
                placeholderUrl={Images.USER_PLACEHOLDER}
                url={item.productImage}
              />
              <Typography poppins className="user-name">
                {item.productName}
              </Typography>
            </Clickable>
          </div>
        </td>
        <td>{item.historyOrderType === 'CREDIT' ? 'JPY' : item?.currency}</td>
        <td>
          <Typography size="large" bold poppins>
            {item.historyOrderType === 'CREDIT' ? Format.price(item.productYenPrice) : Format.price(item.productPrice)}
          </Typography>
        </td>
        {/* <td>
          {item?.quantity || '-'}
        </td> */}
        <td>
          <div className="horizon">
            <Thumbnail
              size={30}
              rounded
              className="avatar"
              placeholderUrl={Images.USER_PLACEHOLDER}
              url={item.fromAvatar}
            />
            <Typography poppins className="user-name">
              {item.fromName || Misc.trimPublicAddress(item.fromPublicAddress, 6)}
            </Typography>
          </div>
        </td>

        <td>
          <div className="horizon">
            <Thumbnail
              size={30}
              rounded
              className="avatar"
              placeholderUrl={Images.USER_PLACEHOLDER}
              url={item.toAvatar}
            />
            <Typography poppins className="user-name">
              {item.toName || Misc.trimPublicAddress(item.toPublicAddress, 6)}
            </Typography>
          </div>
        </td>
        <td>
          <div className="date-box">
            <Typography size="small" className="date">
              {moment(item.createdAt).format(timeFormat)}
            </Typography>
          </div>
        </td>
      </tr>
    );
  };

  render() {
    const { t } = this.props;
    const { data, initial, total } = this.state;
    // console.log("---->", data);
    return (
      <StyledDiv>
        <Container fluid>
          {!initial && data?.length === 0 ? (
            <NoDataBox message={t('no_histories')} />
          ) : (
            <>
              <div className="table-outer">
                <table>
                  <thead>
                    <tr>
                      <th>{t('common:event')}</th>
                      <th>{t('common:status')}</th>
                      <th>{t('common:item')}</th>
                      <th>{t('common:header.currency')}</th>
                      <th>{t('common:price')}</th>
                      {/* <th>{t('common:header.quantity')}</th> */}
                      <th>{t('common:seller')}</th>
                      {/* <th> </th> */}
                      <th>{t('common:buyer')}</th>
                      <th>{t('common:date_time')}</th>
                    </tr>
                  </thead>
                  <tbody>{data?.map(this._renderItem)}</tbody>
                </table>
              </div>
              {data?.length < total && (
                <Pagination onChange={this._onClickPage} total={total} responsive pageSize={PAGINATION_PAGE_SIZE} />
              )}
            </>
          )}
        </Container>
      </StyledDiv>
    );
  }
}

export default TransactionHistoriesTab;
