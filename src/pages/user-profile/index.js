import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';

import Page from '../../components/page';
import Container from '../../components/container';
import Clickable from '../../components/clickable';
import TabBar from '../../components/tab-bar';
import Thumbnail from '../../components/thumbnail';
import Typography from '../../components/typography';
import Footer from '../../app/footer';
import { BLUE_1 } from '../../theme/colors';
import Misc from '../../utils/misc';
import Media from '../../utils/media';
import Loading from '../../components/loading';
import MyProductTab from './my-product-tab';
import Gifts from './gifts-tab';
import TransactionHistories from './transaction-histories-tab';
import Wishs from './wishs-tab';
// import ContactForm from './contact-form';
import MyProductErc1155 from './my-product-erc1155';
import { Colors, Images } from '../../theme';
import Lending from './lending-tab';
import Staking from './staking-tab';

const StyledDiv = styled.div`
  padding-bottom: 140px;
  padding-left: 40px;
  padding-right: 40px;
  width: 100%;
  max-width: 1540px;
  margin: 122px auto 140px;
  ${Media.lessThan(Media.SIZE.LG)} {
    margin-top: 90px;
  }
  ${Media.lessThan(Media.SIZE.SM)} {
    padding: 15px 15px;
  }

  .top-box {
    height: 340px;
    background-size: cover;
    background-position: center;
    display: flex;
    justify-content: flex-end;
    background-color: #eaf0fc;
    background-image: url(${Images.BG_MYPAGE});
    border-radius: 20px;
    ${Media.lessThan(Media.SIZE.MD)} {
      height: 150px;
    }

    .button-group {
      margin-right: 24px;
      margin-top: 12px;
      display: flex;
      box-shadow: 0 0 12px 0 rgba(0, 0, 0, 0.11);
      height: 32px;
      border-radius: 17px;
      overflow: hidden;
      background-color: #ffffff;

      div {
        height: 100%;
        width: 37px;
        display: flex;
        justify-content: center;
        align-items: center;

        &.rounded {
          width: 33px;
        }
      }
    }
  }

  .user-info-box {
    margin-bottom: 70px;
    ${Media.lessThan(Media.SIZE.MD)} {
      padding-top: 10px;
      margin-bottom: 40px;
    }

    .container {
      display: flex;
      ${Media.lessThan(Media.SIZE.MD)} {
        flex-direction: column;
        align-items: center;
        justify-content: center;
      }

      .avatar {
        margin-top: -100px;
        box-shadow: 0 0 12px 0 rgba(0, 0, 0, 0.16);
        min-width: 0;
        margin-left: 65px;
        z-index: 1;
        border: 8px solid #0f1114;
        background-color: #333;
        ${Media.lessThan(Media.SIZE.LG)} {
          margin-left: 20px;
        }
      }

      ._right {
        padding-left: 17px;
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        /* width: calc(100% - 190px - 65px); */
        flex: 1;
        width: 100%;
        ${Media.lessThan(Media.SIZE.MD)} {
          padding-left: 0;
          min-width: 0;
          flex: 1;
        }

        ._label {
          margin-top: -61px;
          height: 41px;
          padding: 0 73px;
          display: flex;
          align-items: center;
          background-color: #ffffff;
          border-radius: 0 21px 21px 0;
          box-shadow: 0 0 12px 0 rgba(0, 0, 0, 0.16);
          margin-left: -73px;
          margin-bottom: 43px;
          ${Media.lessThan(Media.SIZE.MD)} {
            display: none;
          }

          .typography {
            color: #333;
          }
        }

        .wrap-name {
          width: 100%;
        }

        .user-name {
          font-weight: 700;
          font-size: 26px;
          line-height: 26px;
          margin-top: 24px;
          color: #fff;

          ${Media.lessThan(Media.SIZE.MD)} {
            max-width: 100%;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
          }
        }

        .public-address {
          display: flex;
          align-items: center;
          margin-top: 4px;

          .typography {
            font-weight: 400;
            font-size: 16px;
            line-height: 23px;
            color: ${Colors.TEXT};
            ${Media.lessThan(Media.SIZE.MD)} {
              font-size: 11px;
            }
          }

          img {
            margin-top: -3px;
          }
        }

        .wrap-action {
          /* display: flex; */
          display: none;
          flex-wrap: wrap;
          gap: 10px 20px;
          justify-content: center;
          align-items: center;

          .upload_icon,
          .action_icon {
            cursor: pointer;
          }

          ${Media.lessThan(Media.SIZE.MD)} {
            display: none;
          }
        }
      }
    }

    .fluid {
      padding: 0;
    }
  }

  .tab-box {
    .mypage-tab {
      justify-content: center;
      overflow: visible;
      flex-wrap: wrap;

      &:after {
        display: none;
      }

      .tab-item {
        opacity: 1;
        height: 47px;
        margin-right: 20px;
        margin-bottom: 20px;
        p {
          color: ${Colors.TEXT};
        }
        &:hover {
          p {
            color: ${Colors.BUTTON_TEXT};
          }
        }
        &:after {
          display: none;
        }
        &:last-child {
          margin-right: 0px;
        }
      }
      .tab-item.active {
        background: ${Colors.BUTTON_BACKGROUND};
        border-radius: 30px;
        p {
          color: ${Colors.BUTTON_TEXT};
        }
      }
    }
  }

  .loadmore-button {
    background-color: ${BLUE_1};
    border-color: ${BLUE_1};
    color: #ffffff;
  }
`;

@withTranslation(['user_profile', 'common'])
@inject(stores => ({
  authStore: stores.auth,
  usersStore: stores.users,
}))
@observer
class UserProfile extends Component {
  static propTypes = {
    authStore: PropTypes.object,
    usersStore: PropTypes.object,
  };

  state = {
    user: {},
    loadingUser: true,
  };

  async componentDidMount() {
    this._getUserInfo();
  }

  _getUserInfo = async () => {
    const { usersStore, authStore, history } = this.props;

    const { data, success } = await usersStore.getUserInfo(
      authStore.initialData.userId || localStorage.getItem('USER_ID'),
    );

    if (success) {
      await authStore.getInitialData();
      this.setState({
        user: data,
        loadingUser: false,
      });
    } else {
      this.setState({
        loadingUser: false,
      });
      history.push('/');
    }
  };

  _onTabChange = key => {
    const { history } = this.props;

    history.push(`/my-page/${key}`);
  };

  _onCopyToClipboard = value => {
    const { t } = this.props;

    Misc.copyToClipboard(value, t('common:copy_to_clipboard'));
  };

  render() {
    const { match, authStore, t, i18n } = this.props;
    const { user, loadingUser } = this.state;
    const currentTab = match.params.tab || 'my-product';
    const TABS = [
      // {
      //   name: 'ERC721',
      //   key: 'my-product',
      //   icon: null, // BLACK_IMAGE_ICON,
      // },
      // {
      //   name: 'ERC1155',
      //   key: 'product-erc1155',
      //   icon: null, // BLACK_ABOUT_ICON,
      // },
      {
        name: t('user_profile:purchased_items'),
        key: 'my-product',
        icon: null, // BLACK_IMAGE_ICON,
      },
      {
        name: t('user_profile:transaction_history'),
        key: 'transaction-histories',
        icon: null, // BLACK_CLOCK_ICON,
      },
      // aution
      // {
      //   name: t('user_profile:gifts'),
      //   key: 'gifts',
      //   icon: null, // BLACK_GIFT_ICON,
      // },
      {
        name: t('user_profile:wish_list'),
        key: 'wish-list',
        icon: null, // BLACK_WISH_ICON,
      },
      {
        name: t('user_profile:lending_nft'),
        key: 'lending-nft',
        icon: null, // BLACK_WISH_ICON,
      },
      {
        name: t('user_profile:borrow_nft'),
        key: 'borrow-nft',
        icon: null, // BLACK_WISH_ICON,
      },
      {
        name: t('user_profile:staking_nft'),
        key: 'staking-nft',
        icon: null, // BLACK_WISH_ICON,
      },
    ];

    return (
      <Page
        onEndReached={() => {
          this._myProductTab?.loadMore();
        }}
      >
        <StyledDiv>
          {loadingUser ? (
            <Loading />
          ) : (
            <>
              <div className="top-box">
                {/* <div className='button-group'>
                  <Clickable
                    className='rounded'
                    onClick={() => history.push("/settings/basic")}
                  >
                    <img src={Images.BLUE_SETTING_ICON} alt='' />
                  </Clickable>
                </div> */}
              </div>
              <div className="user-info-box">
                <Container className="fluid">
                  <Thumbnail
                    rounded
                    size={190}
                    url={user.userImage}
                    className="avatar"
                    placeholderUrl={Images.GRAY_AVATA_DEFAULT}
                  />

                  <div className="_right">
                    {/* <div className='_label'>
                      <Typography size='big'>{user.bio || "-"}</Typography>
                    </div> */}

                    <div className="wrap-name">
                      <Typography className="user-name" bold>
                        {user.name || 'Unknown'}
                      </Typography>
                      <div className="public-address">
                        <Typography>{authStore.initialData.publicAddress}</Typography>
                        &nbsp;
                        <Clickable onClick={() => this._onCopyToClipboard(authStore.initialData.publicAddress)}>
                          <img src={Images.GRAY_COPY_ICON} alt="copy" />
                        </Clickable>
                      </div>
                    </div>

                    <div className="wrap-action">
                      <img src={Images.GRAY_SHARE_ICON} alt="upload" className="upload_icon" />
                      <img src={Images.GRAY_ADVANCED_ICON} alt="action" className="action_icon" />
                    </div>
                  </div>
                </Container>
              </div>
            </>
          )}
          <div className="tab-box">
            <TabBar
              items={TABS}
              activeTabKey={currentTab}
              onChange={this._onTabChange}
              className="mypage-tab tab-blue-mypage"
            />
          </div>
          <div className="content-box">
            {currentTab === 'my-product' && (
              <MyProductTab
                key={i18n.language}
                innerRef={ref => {
                  this._myProductTab = ref;
                }}
              />
            )}
            {currentTab === 'product-erc1155' && <MyProductErc1155 />}
            {currentTab === 'gifts' && (
              <Gifts
                key={i18n.language}
                innerRef={ref => {
                  this._myProductTab = ref;
                }}
              />
            )}
            {currentTab === 'transaction-histories' && (
              <TransactionHistories
                key={i18n.language}
                innerRef={ref => {
                  this._productsTab = ref;
                }}
              />
            )}
            {currentTab === 'wish-list' && (
              <Wishs
                key={i18n.language}
                innerRef={ref => {
                  this._productsTab = ref;
                }}
              />
            )}
            {currentTab === 'lending-nft' && (
              <Lending
                key={i18n.language}
                innerRef={ref => {
                  this._productsTab = ref;
                }}
              />
            )}
            {currentTab === 'borrow-nft' && (
              <Lending
                key={i18n.language}
                innerRef={ref => {
                  this._productsTab = ref;
                }}
                isBorrow={true}
              />
            )}
            {currentTab === 'staking-nft' && (
              <Staking
                key={i18n.language}
                innerRef={ref => {
                  this._productsTab = ref;
                }}
              />
            )}
            {/* {currentTab === 'form-benefit' && (
              <ContactForm
                key={i18n.language}
                innerRef={ref => {
                  this._productsTab = ref;
                }}
                user={user}
                onReload={this._getUserInfo}
                language={i18n.language}
              />
            )} */}
          </div>
        </StyledDiv>
        <Footer />
      </Page>
    );
  }
}

export default UserProfile;
