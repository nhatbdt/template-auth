import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';

import Container from '../../components/container';
import FetchableList from '../../components/fetchable-list';
import ProductCard from '../../components/product-card-new';
import { history } from '../../store';
import { forwardInnerRef } from '../../utils/high-order-functions';
import Media from '../../utils/media';
import Button from '../../components/button';
import { LENDING_STATUS } from '../../constants/statuses'

// import mockData from './product-mock-data.json';

const StyledDiv = styled.div`
  margin-top: 40px;
  .no-padding {
    padding: 0px;
  }

  .outer-product-item {
    .product-item-action-box {
      padding: 15px 24px 0;
      display: flex;
      flex-wrap: wrap;
      gap: 6px 10px;
      justify-content: center;
      align-items: center;

      > button {
        flex: 1;
        height: fit-content;
        min-height: 36px;
        padding: 0 10px;
        font-size: 13px;
        font-weight: bold;
        border-radius: 30px;
        background-color: #045AFC;

        &.danger {
          background: #9b040d;
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    padding-top: 15px;
  }
`;

@withTranslation('common')
@forwardInnerRef
@inject(stores => ({
  productsStore: stores.products,
  lendingStore: stores.lending,
}))
@observer
class Lending extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
    lendingStore: PropTypes.object,
  };

  loadMore = () => {
    this._fetchableList.loadMore();
  };

  _onProductActionButtonClick = (item) => {
    history.push(`/product-details/${item.id}`);
  };

  _renderProductItem = item => {
    const { t, isBorrow } = this.props;
    const isExpiration = item?.lendingStatus === LENDING_STATUS.EXPIRATION

    return (
      <div className="outer-product-item">
        <ProductCard
          onClick={() => history.push(`/product-details/${item.id}`)}
          item={item}
          className="card-light"
          isShowLending
          isShowStatus
        />
        {!isBorrow && (
          <div className="product-item-action-box">
            <Button
              className={isExpiration ? 'bold' : 'danger bold'}
              disabled={item?.lendingStatus === LENDING_STATUS.IN_LENDING}
              onClick={() => this._onProductActionButtonClick(item)}
            >
              {isExpiration ? t('product_details:lending.withdraw_lending') : t('product_details:lending.cancel_lending')}
            </Button>
          </div>
        )}
      </div>
    )
  }

  render() {
    const { lendingStore, t, i18n, isBorrow } = this.props;
    const gutter = [
      { xs: 8, sm: 15, md: 25, xl: 25 },
      { xs: 8, sm: 15, md: 20, xl: 20 },
    ];

    return (
      <StyledDiv>
        <Container fluid className="no-padding">
          <FetchableList
            showLoadMoreButton
            loadMoreText={t('common:see_more')}
            ref={ref => {
              this._fetchableList = ref;
            }}

            noDataMessage={t('common:no_assets')}
            keyExtractor={(item, index) => index}
            action={isBorrow ? lendingStore.borrowLendingOwned : lendingStore.lendingOwned}
            items={isBorrow ? lendingStore.listBorrowLendingOwned?.items : lendingStore.listLendingOwned?.items}
            total={isBorrow ? lendingStore.listBorrowLendingOwned?.total : lendingStore.listLendingOwned?.total}
            page={isBorrow ? lendingStore.listBorrowLendingOwned?.page : lendingStore.listLendingOwned?.page}
            colSpanXl={6}
            colSpanMd={8}
            colSpanSm={12}
            colSpanXs={24}
            gutter={gutter}
            payload={{
              findType: 'ALL',
              langKey: i18n.language.toUpperCase(),
              limit: 12

            }}
            renderItem={this._renderProductItem}
          />
        </Container>
      </StyledDiv>
    );
  }
}

export default Lending;
