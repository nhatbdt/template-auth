import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';

import Container from '../../components/container';
import FetchableList from '../../components/fetchable-list';
import ProductCard from '../../components/product-card-new';
import { history } from '../../store';
import { forwardInnerRef } from '../../utils/high-order-functions';
import Media from '../../utils/media';

const StyledDiv = styled.div`
  margin-top: 40px;
  .no-padding {
    padding: 0px;
  }
  ${Media.lessThan(Media.SIZE.MD)} {
    padding-top: 15px;
  }
`;

@withTranslation()
@forwardInnerRef
@inject(stores => ({
  productsStore: stores.products,
}))
@observer
class Wishs extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
  };

  loadMore = () => {
    this._fetchableList.loadMore();
  };

  _renderProductItem = item => (
    <ProductCard
      onClick={() => history.push(`/product-details/${item.id}`)}
      item={item}
      className="card-light"
      isShowStatus
    />
  );

  render() {
    const { productsStore, i18n, t } = this.props;
    const gutter = [
      { xs: 8, sm: 15, md: 25, xl: 25 },
      { xs: 8, sm: 15, md: 20, xl: 20 },
    ];

    return (
      <StyledDiv>
        <Container fluid className="no-padding">
          <FetchableList
            showLoadMoreButton
            loadMoreText={t('common:see_more')}
            ref={ref => {
              this._fetchableList = ref;
            }}
            noDataMessage={t('common:no_assets')}
            keyExtractor={(item, index) => index}
            action={productsStore.getProductsWish}
            items={productsStore.myProductsWish?.items}
            total={productsStore.myProductsWish?.total}
            page={productsStore.myProductsWish?.page}
            colSpanXl={6}
            colSpanMd={8}
            colSpanSm={12}
            colSpanXs={24}
            gutter={gutter}
            payload={{
              langKey: i18n.language.toUpperCase(),
              actionCode: 'WISH',
              limit: 12,
            }}
            renderItem={this._renderProductItem}
          />
        </Container>
      </StyledDiv>
    );
  }
}

export default Wishs;
