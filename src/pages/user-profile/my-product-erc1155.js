import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';

import Container from '../../components/container';
import FetchableList from '../../components/fetchable-list';
import ProductCard from '../../components/product-card-new';
import { history } from '../../store';
import { forwardInnerRef } from '../../utils/high-order-functions';
import Media from '../../utils/media';
// import Button from '../../components/button'

const StyledDiv = styled.div`
  padding-top: 40px;

  .no-padding {
    padding: 0px;
  }
  button {
    width: 100%;
    border-radius: 30px;
    margin-top: 9px;
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    padding-top: 15px;
  }
`;

@withTranslation()
@forwardInnerRef
@inject(stores => ({
  productsStore: stores.products,
}))
@observer
class Gifts extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
  };

  loadMore = () => {
    this._fetchableList.loadMore();
  };

  _onProductActionButtonClick = (item, sellAction) => {
    history.push(`/product-details/${item.id}/${sellAction}`);
  };

  _renderProductItem = item => {
    // const { t } = this.props;
    // const isSelling = item.canResellERC1155;
    // const sellAction = !isSelling ? 'cancel-1155' : 'sell-1155';

    return (
      <div className="outer-product-item">
        <ProductCard onClick={() => history.push(`/product-details/${item.id}`)} item={item} />
        {/* <Button
          className={(isSelling) ? 'blue bold' : 'yellow bold'}
          onClick={() => this._onProductActionButtonClick(item, sellAction)}
        >
          {isSelling ? t('product_details:sell.resell') : t('product_details:sell.cancel_listing_2')}
        </Button> */}
      </div>
    );
  };

  render() {
    const { productsStore, i18n, t } = this.props;

    return (
      <StyledDiv>
        <Container fluid className="no-padding">
          <FetchableList
            showLoadMoreButton
            loadMoreColor="blue"
            loadMoreText={t('common:see_more')}
            ref={ref => {
              this._fetchableList = ref;
            }}
            noDataMessage={t('common:no_assets')}
            keyExtractor={(item, index) => index}
            action={productsStore.getProductErc1155}
            items={productsStore.erc1155Product.items}
            total={productsStore.erc1155Product.total}
            page={productsStore.erc1155Product.page}
            colSpanMd={12}
            payload={{
              langKey: i18n.language.toUpperCase(),
            }}
            renderItem={item => this._renderProductItem(item)}
          />
        </Container>
      </StyledDiv>
    );
  }
}

export default Gifts;
