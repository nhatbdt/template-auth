import Confirmable from '../../../components/confirmable';
import MaskLoading from '../../../components/mask-loading';
import { WRAPPED_CURRENCIES } from '../../../constants/currencies';
import Format from '../../../utils/format';
import Storage from '../../../utils/storage';

import {
  getWeb3Instance,
  // getBalance,
  // excuteTransaction,
  approveMax,
  checkValidNetwork,
  excuteTransactionNew,
  getBalanceNew,
  approveMaxNew,
  // excuteTransactionErc1155
} from '../../../utils/web3';

const onCheckNetwork = async () => {
  const isNetworkValid = await checkValidNetwork();

  if (!isNetworkValid) {
    throw new Error('NETWORK_INCORRECT');
  }
};

const onCheckPrice = async (product, productsStore, t, nickname = '') => {
  const getProductPriceResult = nickname
    ? await productsStore.getAuthorProductPrice({ productId: product.id, nickname })
    : await productsStore.getProductPrice({ productId: product.id });

  if (!getProductPriceResult.success || !getProductPriceResult.data) throw new Error('GET_PRODUCT_PRICE_NULL');

  if (product.price !== getProductPriceResult.data.price) {
    MaskLoading.close();
    const ok = await Confirmable.open({
      content: t('change_price_message', {
        currentPrice: Format.price(product.price),
        changedPrice: Format.price(getProductPriceResult.data.price),
        currency: product.currency,
      }),
    });

    // if (!ok) throw { error: 'CANCELED' };
    if (!ok) throw new Error('CANCELED');
    MaskLoading.open({});

    product.setData({
      price: getProductPriceResult.data.price,
      yenPrice: getProductPriceResult.data.yenPrice,
    });
  }
};

const onCheckBalance = async (product, authStore) => {
  // const { prx, eth } = await getBalance(authStore.initialData.publicAddress);
  const { native, wNative, usdt } = await getBalanceNew(authStore.initialData.publicAddress);
  let isEnoughBalance = true;
  // if (product.currency === CURRENCIES.ETH) {
  //   if (product.price > eth) isEnoughBalance = false;
  // } else if (product.currency === CURRENCIES.PRX) {
  //   if (product.price > prx) isEnoughBalance = false;
  // }
  if (WRAPPED_CURRENCIES.includes(product.currency)) {
    if (product.price > wNative) isEnoughBalance = false;
  } else {
    if (product.price > usdt) isEnoughBalance = false;
  }

  // eslint-disable-next-line no-console
  console.log({ native, wNative, usdt });
  if (!isEnoughBalance) {
    throw new Error('BALANCE_NOT_ENOUGH');
  }

  return { native, wNative, usdt };
};

const onSign = async authStore => {
  const userId = Storage.get('USER_ID');
  const { web3 } = await getWeb3Instance();
  const userNonceResult = await authStore.getUserNonce(userId);

  if (!userNonceResult.success) {
    throw userNonceResult.data;
  }

  const message = `I am signing my one-time nonce: ${userNonceResult.data.nonce}`;
  const signature = await web3.eth.personal.sign(message, authStore.initialData.publicAddress, '');

  return signature;
};

const getNonceNumberBlock = async (authStore) => {
  const userId = Storage.get('USER_ID');
  const publicAddress = Storage.get('PUBLIC_ADDRESS');
  const { web3 } = await getWeb3Instance();
  const numberBlock = await web3.eth.getBlockNumber();
  const { data } = await authStore.getUserNonce(userId);
  const nonceContract = await web3.eth.getTransactionCount(authStore.initialData.publicAddress || publicAddress);
  return {
    numberBlock,
    nonceContract,
    nonce: data.nonce
  };
}

const onTransfer = async (publicAddress, t, data, gasLimit, currency, price, type) => {
  // const transactionHash = await excuteTransaction(publicAddress, t, data, gasLimit, currency, price);
  const transactionHash = await excuteTransactionNew(publicAddress, t, data, gasLimit, currency, price, type);
  const { web3 } = type === 'lending' ? await getWeb3Instance() : await getWeb3Instance();
  let tx = await web3.eth.getTransactionReceipt(transactionHash);

  let count = 0;
  const maxCount = 20;
  while (count < maxCount && (!tx || !tx.blockHash)) {
    await new Promise(resolve => setTimeout(resolve, 3000));
    tx = await web3.eth.getTransactionReceipt(transactionHash);
    count++;
  }

  if (!tx?.status || tx?.status === '0x0') {
    Confirmable.open({
      content: t('validation_messages:TRANSACTION_FAILED'),
      hideCancelButton: true,
    });

    return;
  }

  return transactionHash;
};

const onCheckApproveMax = async (t, authStore, product) => {
  const publicAddress = authStore.initialData.publicAddress;
  const data = await approveMax(publicAddress, t, product.currency);
  if (data) return true;
  return false;
};

const onCheckApproveMaxNew = async (t, authStore, product) => {
  const publicAddress = authStore.initialData.publicAddress;
  const data = await approveMaxNew(publicAddress, t, product.currency);
  if (data) return true;
  return false;
};

const onError = (e, t) => {
  let errorMessage = t('messages.transaction_failed');

  if (e === 'NETWORK_INCORRECT') {
    errorMessage = t('messages.network_incorrect');
  } else if (e === 'BALANCE_NOT_ENOUGH' || e?.message === 'BALANCE_NOT_ENOUGH') {
    errorMessage = t('messages.balance_not_enough');
  } else if (e === 'PRODUCT_HOLDING') {
    errorMessage = (
      <>
        {t('messages.product_holding_1')}
        <br />
        <br />
        {t('messages.product_holding_2')}
      </>
    );
  } else if (e === 'CANNOT_CHECKOUT') {
    errorMessage = (
      <>
        {t('messages.cannot_checkout_1')}
        <br />
        <br />
        {t('messages.cannot_checkout_2')}
      </>
    );
  } else if (
    e === 'OUT_OF_WALLET' ||
    ['ERROR_MASTER_PUBLIC_KEY_NOT_FOUND', 'ERROR_MASTER_PUBLIC_KEY_IS_USING'].includes(e?.error)
  ) {
    errorMessage = (
      <>
        {t('messages.out_of_wallet_1')}
        <br />
        {t('messages.out_of_wallet_2')}
      </>
    );
  } else if (e?.error === 'network_not_corresponding') {
    errorMessage = <>{t('validation_messages:network_not_corresponding')}</>;
  } else if (e?.error === 'WALLET_NOT_CORRESPONDING') {
    errorMessage = <>{t('validation_messages:WALLET_NOT_CORRESPONDING')}</>;
  } else if (e?.error === 'ERROR_PRODUCT_NOT_FOUND') {
    errorMessage = (
      <>
        {t('messages.error_product_not_found_1')}
        <br />
        <br />
        {t('messages.error_product_not_found_2')}
      </>
    );
  } else if (e?.error === 'ERROR_USER_CAN_BUY_ONE_PRODUCT') {
    errorMessage = (
      <>
        {t('messages.error_user_can_buy_one_product_1')}
        <br />
        {t('messages.error_user_can_buy_one_product_2')}
      </>
    );
  } else if (e?.error === 'ERROR_USER_VERIFY_SIGNATURE_FAIL') {
    errorMessage = (
      <>
        {t('messages.error_user_verify_signature_fail_1')}
        <br />
        {t('messages.error_user_verify_signature_fail_2')}
      </>
    );
  } else if (e?.error === 'ERROR_USER_NOT_OWN_TOKEN') {
    errorMessage = <>{t('validation_messages:ERROR_USER_NOT_OWN_TOKEN')}</>;
  } else if (e?.error === 'ERROR_CAN_NOT_BORROW_THIS_PRODUCT_BAD_REQUEST') {
    errorMessage = <>{t('validation_messages:ERROR_CAN_NOT_BORROW_THIS_PRODUCT_BAD_REQUEST')}</>;
  } else if (e === 'GET_PRODUCT_PRICE_NULL') {
    errorMessage = <>{t('validation_messages:GET_PRODUCT_PRICE_NULL')}</>;
  } else if (
    e?.message?.includes('User denied message signature') ||
    e?.message?.includes('User denied transaction signature')
  ) {
    errorMessage = <>{t('messages.transaction_rejected')}</>;
  }

  if (e?.error !== 'CANCELED') {
    Confirmable.open({
      content: errorMessage,
      hideCancelButton: true,
    });
  }
};

export {
  onCheckApproveMax,
  onCheckApproveMaxNew,
  onTransfer,
  onSign,
  onError,
  onCheckNetwork,
  onCheckPrice,
  onCheckBalance,
  getNonceNumberBlock,
};
