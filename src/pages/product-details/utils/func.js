import { LENDING_STATUS } from '../../../constants/statuses';

export const checkProcessing = (product) => {
  const isBuying = product?.productBuying
    || [
      LENDING_STATUS.WAIT_LENDING,
      LENDING_STATUS.WAIT_BORROW,
      LENDING_STATUS.WAIT_CANCEL,
      LENDING_STATUS.WAIT_WITHDRAW
    ].includes(product?.lendingDetailDTO?.status)
    || [LENDING_STATUS.WAIT_BORROW].includes(product?.borrowDetailDTO?.status)

  return isBuying
}
