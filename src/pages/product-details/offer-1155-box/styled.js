import styled from 'styled-components';
import Media from '../../../utils/media';
import { Colors } from '../../../theme';

const StyledDiv = styled.div`
  .loading {
    height: 550px;
  }
  .action-box-btn {
    color: white;
  }
  .ant-checkbox-checked .ant-checkbox-inner {
    background-color: #3cc8fc;
    border-color: #3cc8fc;
  }
  .ant-checkbox-checked::after {
    border: 1px solid #376587;
  }

  .modal-header {
    height: 60px;
    display: flex;
    align-items: center;
    padding: 30px 50px;

    p {
      font-size: 22px;
    }
  }

  .content {
    background-color: inherit;
    display: flex;
    /* padding: 36px 66px 66px; */
    padding: 30px 50px 50px;

    .label-box {
      display: flex;
      align-items: center;

      &.space {
        justify-content: space-between;
      }

      ._left {
        display: flex;
        align-items: center;
      }

      .typography {
        span {
          color: #e55509;
          font-weight: bold;
        }
      }

      .hint {
        margin-left: 10px;
        border: solid 1px #045AFC;
        width: 18px;
        height: 18px;
        border-radius: 9px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: #045AFC;
        font-size: 15px;
      }
    }

    .content-side {
      flex: 1;

      &:first-child {
        flex: 1.3;

        .normal-panel {
          .field-box {
            margin-top: 10px;

            .input-wrapper {
              display: flex;
              border: solid 1px #dedede;
              border-radius: 8px;
              overflow: hidden;
              min-height: 77px;

              .price_input {
                display: flex;

                .input-unit-box {
                  padding: 0 20px;
                  display: flex;
                  align-items: center;
                  justify-content: center;

                  .nbng-icon {
                    width: 33px;
                    height: 33px;
                    background-color: #ffffff;
                    border-radius: 17px;
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    box-shadow: 0 0 6px 0 rgb(0 0 0 / 16%);
                    margin-right: 8px;
                    justify-content: center;
                    align-items: center;
                    overflow: hidden;

                    img {
                      width: 25px;
                      height: 25px;
                      object-fit: cover;
                    }
                  }

                  .arrow-icon {
                    margin-left: 20px;
                  }
                }
              }

              ._input-outter {
                flex: 1;
                // background: white;
                height: 100%;
                display: flex;
                flex-direction: column;
                max-width: 245px;
                overflow: hidden;

                .input-box {
                  flex: 1;
                  height: 100%;

                  > div {
                    height: 100%;
                  }

                  input {
                    border: none;
                    border-radius: 0;
                    font-weight: bold;
                    font-size: 18px;
                    font-family: 'Poppins', sans-serif;
                    padding: 0 22px;
                    text-align: right;
                    height: 100%;
                    padding-top: 10px;
                    /* color: #fff; */
                    color: #282828;

                    &:hover,
                    &:focus {
                      box-shadow: none;
                    }
                  }

                  .error-box {
                    display: none;
                  }
                }

                .number-box {
                  /* display: flex;
                  justify-content: flex-end; */
                  padding-bottom: 7px;
                  padding-right: 24px;

                  .typography {
                    opacity: 0.4;
                    text-align: right;
                  }

                  .number-box-divider {
                    width: 1px;
                    background: black;
                    opacity: 0.08;
                    margin: 0 10px;
                  }
                }
              }
            }

            .error-text {
              text-align: right;
              margin-top: 5px;
              color: red;
            }
          }

          .available-currencies {
            display: flex;
            align-items: center;
            margin-top: 13px;
            margin-bottom: 34px;

            .typography {
              margin-right: 22px;
            }

            .available-currency-item {
              margin-right: 10px;
              height: 24px;
              border-radius: 6px;
              background-color: #376587;
              padding: 0 10px;
              display: flex;
              align-items: center;
              font-size: 16px;
              color: #fdfefe;

              &:last-child {
                margin-right: 0;
              }
            }
          }

          ._input-outter {
            .simple.input {
              border: 1px solid #dedede;
              color: #282828;
            }
          }

          .message-text-area {
            margin-top: 10px;

            > div {
              textarea {
                border-radius: 8px;
                border: solid 1px #dedede;
                padding: 10px;
                box-shadow: none !important;
                /* min-height: 241px; */
                min-height: 100px;
                color: #282828;
              }
            }
          }
        }
      }

      &:last-child {
        .top-box {
          margin-bottom: 49px;

          .label-box {
            margin-bottom: 27px;
          }

          .data-field-divider {
            height: 1px;
            background: linear-gradient(to right, #b0b0b0 30%, rgba(255, 255, 255, 0) 0%);
            background-size: 6px 3px;
            margin-bottom: 10px;
          }

          .text-field {
            display: flex;
            margin-bottom: 10px;

            &:last-child {
              margin-bottom: 0;
            }

            ._name {
              /* width: 180px; */
              width: fit-content;
              color: #b7b7b7;
            }

            ._value {
              flex: 1;
              text-align: right;
              color: #3cc8fc;
            }
          }
        }

        .hret-box {
          height: 169px;
          border-radius: 15px;
          background-color: #fff8f7;
          display: flex;
          justify-content: center;
          align-items: center;
          margin-top: 15px;
          margin-bottom: 40px;

          img {
            width: 40px;
            margin-right: 24px;
          }

          .typography {
            margin-right: 10px;
            color: ${Colors.PRIMARY};

            &:last-child {
              margin-right: 0;
            }
          }
        }

        .bottom-box {
          margin-top: 210px;

          .accept-terms-checkbox {
            span {
              color: #282828;
              &.ant-checkbox {
                border: 1px solid #376587;
              }
            }
            .ant-checkbox-wrapper {
              > span {
                &:last-child {
                  font-size: 12px;
                  opacity: 0.5;
                }
              }
            }

            .error-message {
              display: none;
            }
          }

          .action-box {
            display: flex;
            align-items: center;
            margin-top: 33px;
            justify-content: center;

            button {
              height: 50px;
              padding: 0 35px;
              font-size: 16px;
              background: #045afc;
              border-radius: 16px;
            }

            .cancel-button {
              height: 50px;
              margin-left: 41px;
              display: flex;
              .typography {
                margin: auto;
                font-size: 16px;
                color: #282828;
                white-space: nowrap;
              }
            }
          }
        }
      }
    }

    .divider {
      display: flex;
      align-items: center;
      justify-content: center;
      /* padding: 0 60px; */
      padding: 0 40px;

      .line {
        width: 1px;
        height: 100%;
        background-color: #707070;
        opacity: 0.3;
      }

      .transfer-icon {
        background-color: inherit;
        position: absolute;
        padding-bottom: 6px;

        img {
          width: 25px;
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .modal-header {
      height: 50px;
      padding: 0 20px;

      p {
        font-size: 18px;
      }
    }

    .content {
      flex-direction: column;
      padding: 15px;

      .content-side {
        &:first-child {
          .normal-panel {
            .field-box {
              .input-wrapper {
                flex-direction: column;
                height: auto;

                .custom-dropdown {
                  background-color: red;
                }

                .price_input {
                  display: flex;
                  justify-content: center;
                  padding-top: 5px;

                  .input-unit-box {
                    padding: 0;

                    .nbng-icon {
                      width: 30px;
                      height: 30px;

                      img {
                        width: 21px;
                      }
                    }
                  }
                }

                ._input-outter {
                  .input-box {
                    height: 100%;
                    input {
                      padding-top: 0;
                      padding: 0 15px;
                    }
                  }

                  .number-box {
                    flex-direction: column;
                    align-items: flex-end;
                    padding-right: 15px;

                    .number-box-divider {
                      display: none;
                    }
                  }
                }
              }
            }
          }
        }

        &:last-child {
          .top-box {
            margin-bottom: 20px;

            .label-box {
              margin-bottom: 20px;
            }

            .text-field {
              margin-bottom: 7px;

              ._name {
                /* width: 130px; */
                width: fit-content;
                font-size: 14px;
              }

              ._value {
                flex: 1;
                text-align: right;
                font-size: 14px;
              }
            }
          }

          .bottom-box {
            .action-box {
              margin-top: 20px;
              flex-direction: column;

              button {
                height: 50px;
                padding: 0 35px;
                font-size: 16px;
              }

              .cancel-button {
                margin-left: 0;
                margin-top: 20px;
                margin-bottom: 20px;

                .typography {
                  font-size: 16px;
                  /* color: #ffffff; */
                  color: #282828;
                  white-space: nowrap;
                }
              }
            }
          }
        }
      }

      .divider {
        padding: 20px 0;

        .line {
          height: 1px;
          width: 100%;
        }

        .transfer-icon {
          padding-bottom: 0;
          padding: 0 5px;
          margin-bottom: 5px;

          img {
            width: 22px;
          }
        }
      }
    }
  }

  .mb-2 {
    margin-bottom: 20px;
  }

  .mb-1 {
    margin-bottom: 10px;
  }
`;

export { StyledDiv };
