import { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import { inject, observer } from 'mobx-react';
import { Translation } from 'react-i18next';

import Input from '../../../components/input';
import TextArea from '../../../components/text-area';
import Modal from '../../../components/modal';
import Clickable from '../../../components/clickable';
import Confirmable from '../../../components/confirmable';
import Loading from '../../../components/loading';
import Button from '../../../components/button';
import HintTooltip from '../../../components/hint-tooltip';
import Checkbox from '../../../components/checkbox';
import Field from '../../../components/field';
import Typography from '../../../components/typography';
import { Images } from '../../../theme';
import Format from '../../../utils/format';
import CURRENCIES from '../../../constants/currencies';
import { getBalance, approveMax, allowancePrx, allowanceWeth } from '../../../utils/web3';
import UniswapInterfaceModal from '../offer-box/uniswap-interface-modal';
import { onSign } from '../utils/checkout';
import { StyledDiv } from './styled';
import ProductInputAmount from '../../../components/product-input-amount';
import MaskLoading from '../../../components/mask-loading';
import { getRateByCurrency } from '../../../utils/rates';
import { MAX_LENGTH_INPUT_VALUE_FRACTION, MAX_LENGTH_INPUT_VALUE_INTEGER } from '../../../constants/common';

const SUPPORT_CURRENCIES = ['ETH', 'PRX'];

@inject(stores => ({
  productsStore: stores.products,
  offersStore: stores.offers,
  authStore: stores.auth,
  product: stores.products.currentProductDetails,
}))
@observer
class OfferModal extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
    authStore: PropTypes.object,
    product: PropTypes.object,
    offersStore: PropTypes.object,
  };

  state = {
    isOpen: false,
    loading: false,
    initing: true,
    selectedCurrency: CURRENCIES.ETH,
  };

  open = async () => {
    this.setState({
      isOpen: true,
      selectedCurrency: CURRENCIES.ETH,
      initing: false,
    });
  };

  close = () => this.setState({ isOpen: false });

  _checkBalance = async (offerPrice, t) => {
    const { authStore } = this.props;
    const { selectedCurrency } = this.state;

    let isEnoughBalance = false;
    const { prx, weth } = await getBalance(authStore.initialData.publicAddress);

    if (selectedCurrency === CURRENCIES.PRX) {
      isEnoughBalance = +offerPrice <= prx;
    }
    if (selectedCurrency === CURRENCIES.ETH) {
      isEnoughBalance = +offerPrice <= weth;
    }
    // if (!isEnoughBalance) throw { error: t('validation_messages:ERROR_BALANCE_NOT_ENOUGH') }
    if (!isEnoughBalance) throw new Error(t('validation_messages:ERROR_BALANCE_NOT_ENOUGH'));
  };

  _onSubmit = async (values, t) => {
    const { product, offersStore, authStore } = this.props;
    const { selectedCurrency } = this.state;

    this.setState({
      loading: true,
    });

    try {
      const registerOfferData = await offersStore.registerOffer({
        productId: product.id,
        offerPrice: values.offerPrice,
      });

      if (!registerOfferData?.success) {
        // throw registerOfferData?.data;
        MaskLoading.close();
        return;
      }

      const signature = await onSign(authStore);
      await this._checkBalance(values.offerPrice, t);

      const { publicAddress } = authStore.initialData;

      let approveHash = null;

      const approvePublicAddress = process.env.EXCHANGE_CONTRACT_ADDRESS;

      const allowance =
        selectedCurrency === CURRENCIES.PRX
          ? await allowancePrx(publicAddress, approvePublicAddress)
          : await allowanceWeth(publicAddress, approvePublicAddress);

      if (allowance < +values.offerPrice) {
        approveHash = await approveMax(publicAddress, t, selectedCurrency);
      }

      const { success, data } = await offersStore.createOffer1155({
        approveTokenTransactionHash: approveHash,
        offerMessage: values.message,
        offerPrice: values.offerPrice,
        offerId: registerOfferData.data.offerId,
        productId: product.id,
        currency: selectedCurrency,
        value: +values.value,
        signature,
      });

      if (!success) {
        throw data;
      } else {
        await offersStore.getOffers1155({
          page: 1,
          productId: product.id,
          limit: 10,
        });
      }
      this.close();

      Confirmable.open({
        content: t('product_details:offer.create_offer_success'),
        hideCancelButton: true,
      });
    } catch (e) {
      this.close();

      // eslint-disable-next-line no-console
      console.error(e);
      if (
        (e.error === 'Your balance is insufficient.' ||
          e.error === 'ウォレットの残高が不足してます！' ||
          e.error === '您的钱包余额不足！') &&
        selectedCurrency === 'ETH'
      ) {
        this.close();

        const ok = await Confirmable.open({
          content: t('validation_messages:ERROR_NATIVE_BALANCE_NOT_ENOUGH'),
          acceptButtonText: 'Swap WETH',
        });

        if (ok) {
          this._uniswapInterfaceModal.open(values.bidPrice);
        }
      } else {
        Confirmable.open({
          content:
            e?.code === 4001
              ? t('validation_messages:CANCEL_CONFIRM_META_MASK')
              : t(`validation_messages:${e.error || 'SOMETHING_WENT_WRONG'}`),
          hideCancelButton: true,
        });
      }
    }

    this.setState({
      loading: false,
    });
  };

  _onSelectCurrency = currnecy => {
    this.setState({
      selectedCurrency: currnecy,
    });
  };

  _validate = (values, t) => {
    const { product } = this.props;
    let errors = {};

    if (+values.offerPrice <= 0) {
      errors = {
        offerPrice: t('MIN_PRICE_THAN_MORE_0'),
      };
    } else if (!isNaN(+values.offerPrice) && values.offerPrice) {
      const sOfferPrice = values.offerPrice?.toString();
      let symbol = null;
      if (sOfferPrice.includes('.')) {
        symbol = '.';
      } else if (sOfferPrice.includes(',')) {
        symbol = ',';
      }

      if (!symbol) {
        if (sOfferPrice?.length > MAX_LENGTH_INPUT_VALUE_INTEGER) {
          errors = {
            offerPrice: t('MAX_LENGTH_PRICE'),
          };
        }
      } else {
        const arr = sOfferPrice.split(symbol);
        if (arr[0]?.length > MAX_LENGTH_INPUT_VALUE_INTEGER || arr[1]?.length > MAX_LENGTH_INPUT_VALUE_FRACTION) {
          errors = {
            offerPrice: t('MAX_LENGTH_PRICE'),
          };
        }
      }
    }

    if (+values.value <= 0) {
      errors = {
        value: t('validation_messages:MIN_VALUE_THAN_MORE_0'),
      };
    }

    if (+values.value > product?.totalQuantity - product.holdingQuantity) {
      errors = {
        value: t('validation_messages:VALUE_MAX', { x: product?.totalQuantity - product.holdingQuantity }),
      };
    }

    return errors;
  };

  _renderNumberBox = price => {
    const { authStore } = this.props;
    const { selectedCurrency } = this.state;
    // const { ethToJpy, prxToJpy, usdToJpy } = authStore.initialData;
    const masterData = authStore.initialData;
    const pricesJpy = getRateByCurrency(CURRENCIES[selectedCurrency], masterData);

    return (
      <div className="number-box">
        <Typography poppins size="small">
          {/* {Format.price(pricesJpy / usdToJpy)} USD */}
          {Format.price(pricesJpy / getRateByCurrency(CURRENCIES.USD, masterData))} USD
        </Typography>
        {/* <div className="number-box-divider" /> */}
        <Typography poppins size="small">
          {Format.price(pricesJpy)} JPY
        </Typography>
      </div>
    );
  };

  _renderNormalPanel = (values, errors, t) => {
    const { selectedCurrency } = this.state;

    return (
      <div className="normal-panel">
        <ProductInputAmount
          selectedCurrency={selectedCurrency}
          values={values}
          errors={errors}
          onSelect={this._onSelectCurrency}
        />
        <div className="available-currencies">
          <Typography size="large">{t('product_details:offer.available_currencies')}</Typography>
          {SUPPORT_CURRENCIES.map((item, index) => (
            <div className="available-currency-item" key={index}>
              {item}
            </div>
          ))}
        </div>
        <div className="mt-2 mb-2">
          <Typography size="large" className="mb-1">
            {t('product_details:offer.value')}
          </Typography>
          <div className="_input-outter">
            <Field
              className="input-box bold"
              type="number"
              name="value"
              maxLength={9}
              placeholder={0}
              simple
              component={Input}
            />
          </div>
        </div>
        <div className="label-box space">
          <div className="_left">
            <Typography size="big" bold>
              {t('product_details:offer.offer_message')}
            </Typography>
            <HintTooltip content={t('product_details:offer.hint_2')} />
          </div>
          <Typography size="nity">
            {t('product_details:offer.characters')}：{values.message?.length || 0}
          </Typography>
        </div>
        <Field
          className="message-text-area"
          name="message"
          placeholder={t('product_details:offer.message_placeholder')}
          component={TextArea}
        />
      </div>
    );
  };

  _renderForm = ({ handleSubmit, values, errors }, t) => {
    const { loading, selectedCurrency } = this.state;

    const INFO_DATA = [
      {
        name: t('product_details:offer.price'),
        value: Format.price(values.offerPrice || 0),
      },
      {
        name: t('product_details:offer.currency'),
        value: selectedCurrency,
      },
    ];

    return (
      <Form>
        <div className="modal-header">
          <Typography bold>{t('product_details:offer.put_a_limit_price')}</Typography>
        </div>
        <div className="content">
          <div className="content-side">{this._renderNormalPanel(values, errors, t)}</div>
          <div className="divider">
            <div className="line" />
            <div className="transfer-icon">
              <img src={Images.BLACK_TRANSFER_ICON} alt="" />
            </div>
          </div>
          <div className="content-side">
            <div className="top-box">
              <div className="label-box">
                <Typography size="big" bold>
                  {t('product_details:sell.content')}
                </Typography>
              </div>
              {INFO_DATA.map((item, index) => (
                <Fragment key={index}>
                  {item.line && <div className="data-field-divider" />}
                  <div className="text-field" key={index}>
                    <Typography className="_name" size="large">
                      {item.name}
                    </Typography>
                    <Typography className="_value" size="large">
                      {item.value}
                    </Typography>
                  </div>
                </Fragment>
              ))}
            </div>
            <div className="bottom-box">
              <Field showError={false} className="accept-terms-checkbox" name="acceptTerms" component={Checkbox}>
                {t('product_details:bid.accept')}
              </Field>
              <div className="action-box">
                <Button
                  className="action-box-btn"
                  disabled={!values.acceptTerms}
                  onClick={handleSubmit}
                  // background={Colors.BLUE_4}
                  loading={loading}
                >
                  {t('product_details:offer.put_limit_with_content')}
                </Button>
                <Clickable className="cancel-button" onClick={this.close}>
                  <Typography bold size="big">
                    {t('product_details:sell.cancel')}
                  </Typography>
                </Clickable>
              </div>
            </div>
          </div>
        </div>
      </Form>
    );
  };

  render() {
    const { isOpen, loading, initing } = this.state;

    return (
      <>
        <Modal
          className="custom-modal"
          open={isOpen}
          onCancel={this.close}
          destroyOnClose
          maskClosable={!loading}
          closable={!loading}
          width={960}
          padding={0}
        >
          <StyledDiv>
            {initing ? (
              <Loading size="small" className="loading" />
            ) : (
              <Translation>
                {t => (
                  <Formik
                    enableReinitialize
                    validateOnChange={true}
                    validateOnBlur={false}
                    initialValues={{
                      acceptTerms: false,
                      value: 1,
                    }}
                    validate={values => this._validate(values, t)}
                    validationSchema={yup.object().shape({
                      offerPrice: yup.number().nullable().required(t('PRICE_REQUIRED')),
                      acceptTerms: yup.bool().oneOf([true]),
                    })}
                    onSubmit={values => this._onSubmit(values, t)}
                    component={data => this._renderForm(data, t)}
                  />
                )}
              </Translation>
            )}
          </StyledDiv>
        </Modal>
        <UniswapInterfaceModal
          ref={ref => {
            this._uniswapInterfaceModal = ref;
          }}
        />
      </>
    );
  }
}

export default OfferModal;
