import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
// eslint-disable-next-line import/no-extraneous-dependencies
import { DownOutlined } from '@ant-design/icons';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';

import Typography from '../../../components/typography';
import Clickable from '../../../components/clickable';

const OfferNotificationBarStyled = styled(Clickable)`
  background: linear-gradient(0deg, rgba(55, 101, 135, 0.08) 0%, rgba(52, 146, 181, 1) 45%);
  border-radius: 5px;
  height: 38px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 20px;
  color: white;
  margin-bottom: 30px;
`;

@withTranslation('product_details')
@inject(stores => ({
  offersStore: stores.offers,
  product: stores.products.currentProductDetails,
}))
@observer
class OfferNotificationBar extends Component {
  static propTypes = {
    offersStore: PropTypes.object,
    product: PropTypes.object,
  };

  state = {};

  _onScrollToSection = () => {
    const element = document.getElementById('offer-list');
    element.scrollIntoView({
      behavior: 'smooth',
      block: 'start',
    });
  };

  render() {
    const { offersStore, t } = this.props;

    if (offersStore.countOffer < 1) return null;

    return (
      <OfferNotificationBarStyled onClick={this._onScrollToSection}>
        <Typography bold>{t('offer.offer_number_notification', { x: offersStore.countOffer })}</Typography>
        <DownOutlined />
      </OfferNotificationBarStyled>
    );
  }
}

export default OfferNotificationBar;
