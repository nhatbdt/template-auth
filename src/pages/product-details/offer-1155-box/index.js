import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import moment from 'moment';
import { withTranslation } from 'react-i18next';
import classnames from 'classnames';
import { Pagination } from 'antd';

import Typography from '../../../components/typography';
import Thumbnail from '../../../components/thumbnail';
import NoDataBox from '../../../components/no-data-box';
import Clickable from '../../../components/clickable';
import { Images, Colors } from '../../../theme';
import MessageOfferModal from './offer-message-modal';
import { checkIsApproveForAll, approveAllErc1155 } from '../../../utils/web3';
import { onSign } from '../utils/checkout';

const OffersBoxStyled = styled.div`
  padding-top: 0px;
  display: flex;
  flex-direction: column;

  .title {
    color: #333333;
    font-size: 13px;
  }
  .user-name {
    /* color: #FDFEFE !important; */
    color: #282828;
  }
  .table-outer {
    overflow-x: auto;
    overflow-y: hidden;
    width: 100%;
    table {
      width: 100%;
      min-width: 700px;
      margin-top: 20px;
      table-layout: fixed;

      thead {
        tr {
          th {
            padding: 0 8px;
            font-weight: 700;
            font-size: 14px;
            color: ${Colors.TEXT};
            text-align: left;
            &.create-at {
              width: 120px;
            }
            &.from {
              width: 120px;
            }
          }
        }
      }

      tbody {
        tr {
          height: 53px;
          border-bottom: 1px solid rgb(255 255 255 / 8%);

          td {
            padding: 0 8px;
            color: ${Colors.TEXT};
            .typography {
              color: ${Colors.TEXT};
            }

            .status-text {
              &.success {
                color: #03ad0a;
              }
            }

            .action-button {
              white-space: nowrap;
              p {
                text-align: center;
              }
            }

            .horizon {
              display: flex;
              align-items: center;

              .new-dot {
                width: 6px;
                height: 6px;
                border-radius: 3px;
                margin-right: 10px;

                &.show {
                  background-color: ${Colors.BLUE};
                }
              }

              .avatar {
                margin-right: 9px;
                min-width: 0;
              }

              .user-name {
                font-size: 13px;
                color: ${Colors.BLUE};
                overflow: hidden;
                text-overflow: ellipsis;
                max-width: 180px;
                white-space: nowrap;
                flex: 1;
                margin-right: 5px;
              }

              .fire-icon {
                margin-right: 10px;
              }

              .heat-number {
                color: ${Colors.PRIMARY};
              }
            }

            .cancel-button {
              height: 32px;
              font-size: 11px;
              padding: 0 15px;
            }
          }
        }
      }
      .no-data-box {
        height: 100px;
        display: flex;
        align-items: center;
      }
    }
  }
`;

@withTranslation('product_details')
@inject(stores => ({
  product: stores.products.currentProductDetails,
  authStore: stores.auth,
  offersStore: stores.offers,
}))
@observer
class OffersBox extends PureComponent {
  static propTypes = {
    product: PropTypes.object,
    authStore: PropTypes.object,
    offersStore: PropTypes.object,
  };

  constructor(props) {
    super(props);
    const { authStore, product } = props;

    this._isMyproduct = authStore.initialData.userId === product.userId;
  }

  _getListOffer = async page => {
    const { offersStore, product } = this.props;
    await offersStore.getOffers1155({
      page,
      productId: product.id,
      limit: 1,
    });
  };

  _onClickPage = page => {
    this._getListOffer(page);
  };

  _onCancelOffer = async item => {
    const { product, offersStore, authStore, t } = this.props;

    try {
      const signature = await onSign(authStore);

      let approveHash = null;
      const { publicAddress } = authStore.initialData;

      const isApproveForAll = await checkIsApproveForAll(publicAddress, t);

      if (!isApproveForAll) {
        approveHash = await approveAllErc1155(publicAddress, t);
      }

      const { success } = await offersStore.cancelOffer({
        productId: product.id,
        offerId: item?.id,
        signature,
        approveHash,
      });

      if (success) {
        this._getListOffer(1);
      }
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
    }
  };

  _onApproveOffer = item => {
    this._messageOfferModal.open(item, true);
  };

  _onOpenMessageModal = item => {
    this._messageOfferModal.open(item);
  };

  _renderItem = (item, index) => {
    const { t, authStore, product } = this.props;
    const timeFormat = 'YYYY/M/D HH:mm';
    const { userId } = authStore.initialData;
    const isOwner = item?.userId === userId;

    return (
      <tr key={`${item?.userId}_${index}`}>
        <td>
          <div className="horizon">
            <div
              className={classnames('new-dot', {
                show: item?.isNew,
              })}
            />
            <Thumbnail
              size={30}
              rounded
              className="avatar"
              placeholderUrl={Images.USER_PLACEHOLDER}
              url={item?.userAvatar}
            />
            <Typography poppins className="user-name">
              {item?.userName || item?.userPublicAddress}
            </Typography>
          </div>
        </td>
        <td>
          <div className="horizon">
            {item?.offerMessage ? (
              <Clickable onClick={() => this._onOpenMessageModal(item)}>
                <img className="fire-icon" src={Images.COLOR_MSG_ICON} alt="" />
              </Clickable>
            ) : (
              <Clickable>
                <img className="fire-icon" src={Images.GRAY_MSG_ICON} alt="" />
              </Clickable>
            )}
          </div>
        </td>
        <td>
          <Typography size="large" bold poppins>
            {item?.offerPrice}
          </Typography>
        </td>
        <td>{item?.value}</td>
        <td>{item?.currency}</td>
        <td>{item?.status ? t(`offer.buyer_status.${item?.status}`) : '-'}</td>
        <td>
          <Typography size="small" className="date">
            {moment(item?.createdAt).format(timeFormat)}
          </Typography>
        </td>
        <td>
          {item?.status === 'OFFERING' &&
            !product?.productBuying &&
            product.holdingQuantity >= item?.value &&
            !isOwner && (
              <Clickable className="action-button" onClick={() => this._onApproveOffer(item)}>
                <Typography size="small" bold primary>
                  {`${t('offer.accept')} >`}
                </Typography>
              </Clickable>
            )}
          {isOwner && ['OFFERING', 'NEW'].includes(item?.status) && (
            <Clickable className="action-button" onClick={() => this._onCancelOffer(item)}>
              <Typography size="small" bold primary>
                {t('common:cancel')}
              </Typography>
            </Clickable>
          )}
        </td>
      </tr>
    );
  };

  render() {
    const { t, offersStore } = this.props;
    const { offers1155 } = offersStore;
    const { items, total } = offers1155;

    return (
      <OffersBoxStyled id="offer-list">
        <div className="table-outer">
          <table>
            <thead>
              <tr>
                <th className="from">{t('common:from')}</th>
                <th className="">&nbsp;</th>
                <th className="amount">{t('offer.amount')}</th>
                <th>{t('erc1155.quantity')}</th>
                <th className="currency">{t('common:header.currency')}</th>
                <th className="status">{t('common:status')}</th>
                <th className="create-at">{t('offer.create_at')}</th>
                <th className="action">&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              {items.length > 0 ? (
                items.map(this._renderItem)
              ) : (
                <tr>
                  <td colSpan={8}>
                    <NoDataBox className="no-data-box" />
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        {items?.length < total && (
          <div className="pagination">
            <Pagination onChange={this._onClickPage} total={total} responsive pageSize={1} />
          </div>
        )}
        <MessageOfferModal
          ref={ref => {
            this._messageOfferModal = ref;
          }}
        />
      </OffersBoxStyled>
    );
  }
}

export default OffersBox;
