import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { Translation } from 'react-i18next';
import moment from 'moment';
import classnames from 'classnames';

import Modal from '../../../components/modal';
import Typography from '../../../components/typography';
import Clickable from '../../../components/clickable';
import Loading from '../../../components/loading';
import Toast from '../../../components/toast';
import Media from '../../../utils/media';
import Format from '../../../utils/format';
import { Images, Colors } from '../../../theme';
import Thumbnail from '../../../components/thumbnail';
import Button from '../../../components/button';
import { checkIsApproveForAll, approveAllErc1155 } from '../../../utils/web3';
import { onSign, onTransfer } from '../utils/checkout';
import Misc from '../../../utils/misc';
import Storage from '../../../utils/storage';

const MsgOfferModalStyled = styled.div`
  .loading-box {
    height: 550px;
  }

  .modal-header {
    height: 110px;
    display: flex;
    align-items: center;
    padding: 0 66px;

    p {
      font-size: 22px;
    }
  }

  .content {
    background-color: inherit;
    display: flex;
    padding: 36px 66px 66px;
    flex-direction: column;

    .row {
      display: flex;
      flex-direction: row;
      flex: 1;
      width: 100%;

      .title {
        width: 120px;
      }

      .username {
        color: ${Colors.PRIMARY};
      }

      .horizon {
        display: flex;
        align-items: center;

        .avatar {
          margin-right: 9px;
          min-width: 0;
        }

        .fire-icon {
          margin-right: 23px;
        }

        .heat-number {
          color: ${Colors.PRIMARY};
        }
      }
    }

    .offer-message-label {
      margin: 40px 0 20px;
    }

    .message-box {
      padding: 21px 16px;
      position: relative;
      white-space: pre-wrap;
      word-break: break-word;
      /* border: 1px solid #555; */
      border: 1px solid #dedede;
      color: #282828;
      border-radius: 4px;

      .message-footer {
        position: absolute;
        bottom: 0;
        left: 0;
        height: 75px;
        width: 100%;
        display: flex;
        justify-content: center;
        color: #e51109;
        font-size: 12px;
        align-items: flex-end;
        padding: 13px;
        text-align: center;
        background-image: linear-gradient(to bottom, rgba(255, 248, 247, 0), rgba(255, 248, 247, 0.92) 50%, #fff8f7);
      }

      &.trim {
        max-height: 120px;
        overflow: hidden;

        &.expand {
          max-height: none;
          padding-bottom: 50px;

          .message-footer {
            height: 50px;
          }
        }
      }
    }

    .footer {
      display: flex;
      flex-direction: row;
      margin-top: 64px;
      align-items: center;
      justify-content: center;

      .accept-button {
        margin-right: 41px;
        color: #fff;
        background-color: #045afc;
      }

      .red-btn {
        color: #e51109;
      }
    }
    .red-btn {
      color: #e51109;
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .modal-header {
      height: 50px;
      padding: 0 20px;

      p {
        font-size: 18px;
      }
    }

    .content {
      flex-direction: column;
      padding: 15px;

      .row {
        display: flex;
        flex-direction: row;
        flex: 1;
        width: 100%;
      }
    }
  }
`;
@inject(stores => ({
  product: stores.products.currentProductDetails,
  offersStore: stores.offers,
  authStore: stores.auth,
}))
@observer
class MsgOfferModal extends Component {
  static propTypes = {
    product: PropTypes.object,
    offersStore: PropTypes.object,
    authStore: PropTypes.object,
  };

  state = {
    initing: false,
    isOpen: false,
    offerDetails: null,
    isShowAcceptButton: false,
    loading: false,
    commissionFee: 0,
    isExpandLongMessage: false,
  };

  open = async (offerDetails, isShowAcceptButton = false) => {
    const { offersStore, product } = this.props;

    this.setState({
      initing: isShowAcceptButton,
      isOpen: true,
      offerDetails,
      isShowAcceptButton,
    });

    if (!isShowAcceptButton) return null;

    const { success, data } = await offersStore.getOfferInfos({
      productId: product.id,
      offerId: offerDetails.id,
    });

    if (success) {
      this.setState({
        initing: false,
        commissionFee: data.commissionFee,
      });
    } else {
      this.setState({
        initing: false,
      });
    }

    return null;
  };

  close = () => this.setState({ isOpen: false, initing: false });

  _onToggleLongMessage = () => {
    const { isExpandLongMessage } = this.state;

    this.setState({
      isExpandLongMessage: !isExpandLongMessage,
    });
  };

  _onUpdateOffer = async (signature, t) => {
    const userId = Storage.get('USER_ID');
    const { authStore, offersStore, product } = this.props;
    const { offerDetails } = this.state;
    const userNonceResult = await authStore.getUserNonce(userId);

    if (!userNonceResult.success) {
      throw userNonceResult.data;
    }

    const confirmResult = await offersStore.confirmOffer1155({
      productId: product.id,
      offerId: offerDetails.id,
      nonce: userNonceResult.data.nonce,
      signature,
    });

    if (!confirmResult.success) throw confirmResult.data;

    if (confirmResult.data?.holding) {
      product.setHolding(true);

      // throw { error: t('validation_messages:PRODUCT_HOLDING') };
      throw new Error(t('validation_messages:PRODUCT_HOLDING'));
    }
    this.setState({
      confirmOfferData: confirmResult,
    });
    const data = confirmResult.data?.smartContractVerifySignature;
    const gasLimit = 300000;
    const { publicAddress } = authStore.initialData;

    const currency = offerDetails.currency === 'ETH' ? 'WETH' : offerDetails.currency;

    const transactionId = await onTransfer(publicAddress, t, data, gasLimit, currency, offerDetails.offerPrice);

    const updateOffer = await offersStore.updateOffer1155({
      contractTransactionId: transactionId,
      productId: product.id,
      orderId: confirmResult.data.orderId,
      offerId: offerDetails.id,
    });
    if (!updateOffer.success) {
      // throw { error: 'UPDATE_OFFER_TRANSACTION_FAIL' }
      throw new Error('UPDATE_OFFER_TRANSACTION_FAIL');
    }
  };

  _onAcceptOffer = async t => {
    const { product, authStore, offersStore } = this.props;
    const { offerDetails } = this.state;

    this.setState({
      loading: true,
    });

    this.isCancelUpdateOffer = false;

    try {
      await new Promise(resolve => {
        setTimeout(() => {
          resolve();
        }, 10000);
      });

      const { publicAddress } = authStore.initialData;

      const isApproveForAll = await checkIsApproveForAll(publicAddress, t);

      if (!isApproveForAll) {
        await approveAllErc1155(publicAddress, t);
      }

      const signature = await onSign(authStore);
      await this._onUpdateOffer(signature, t);

      product.setData({
        status: 'SOLD',
        productBuying: true,
      });
      offerDetails.setData({
        status: 'BUYING',
      });
      this.close();
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);

      if (e?.message?.includes('User denied transaction signature')) {
        this.isCancelUpdateOffer = true;
      }

      if (e.error && !['ERROR_CANCEL_APPROVE_FEE'].includes(e.error)) {
        Toast.error(t(`validation_messages:${e.error}`));
      }
    }

    if (this.isCancelUpdateOffer) {
      const { confirmOfferData } = this.state;

      await offersStore.cancelUpdateOffer({
        orderId: confirmOfferData.data.orderId,
        offerId: offerDetails.id,
      });
      this.close();
    }

    this.setState({
      loading: false,
    });

    return null;
  };

  _renderContent = () => {
    const { product } = this.props;
    const { loading, offerDetails, isShowAcceptButton, commissionFee, isExpandLongMessage } = this.state;
    const receiveAmount = offerDetails?.offerPrice - commissionFee;
    const isLongMessage = (offerDetails?.offerMessage?.length || 0) > 100;

    return (
      <Translation>
        {t => (
          <>
            <div className="modal-header">
              <Typography bold>{t('product_details:offer.content')}</Typography>
            </div>
            <div className="content">
              <div className="row" style={{ marginBottom: 16 }}>
                <Typography className="title" secondary size="large" bold>
                  {t('product_details:offer.user')}
                </Typography>
                <div style={{ flex: 1, display: 'flex', justifyContent: 'space-between' }}>
                  <Typography size="big" bold poppins className="username">
                    {offerDetails?.userName || Misc.trimPublicAddress(offerDetails?.userPublicAddress, 4)}
                  </Typography>
                  <Thumbnail
                    size={30}
                    rounded
                    className="avatar"
                    placeholderUrl={Images.USER_PLACEHOLDER}
                    url={offerDetails?.userAvatar}
                  />
                </div>
              </div>
              <div className="row" style={{ margin: '16px 0', fontWeight: 500 }}>
                <Typography className="title" secondary size="large" bold>
                  {t('product_details:offer.create_at')}
                </Typography>
                <Typography size="big" bold>
                  {moment(offerDetails?.createdAt).format('YYYY/MM/DD HH:mm:ss')}
                </Typography>
              </div>
              <div className="row" style={{ margin: '16px 0', fontWeight: 500 }}>
                <Typography className="title" secondary size="large" bold>
                  {t('product_details:offer.amount')}
                </Typography>
                <Typography size="big" bold>
                  {offerDetails?.offerPrice} {offerDetails?.currency}
                </Typography>
              </div>
              {product.owned && isShowAcceptButton && (
                <>
                  <div className="row" style={{ margin: '16px 0', fontWeight: 500 }}>
                    <Typography className="title" secondary size="large" bold>
                      {t('product_details:offer.commission_fee')}
                    </Typography>
                    <Typography size="big" bold>
                      {Format.price(commissionFee)} {offerDetails?.currency}
                    </Typography>
                  </div>
                  <div className="row" style={{ margin: '16px 0', fontWeight: 500 }}>
                    <Typography className="title" secondary size="large" bold>
                      {t('product_details:offer.receive')}
                    </Typography>
                    <Typography size="big" bold>
                      {Format.price(receiveAmount)} {offerDetails?.currency}
                    </Typography>
                  </div>
                </>
              )}
              {offerDetails?.offerMessage && (
                <>
                  <Typography size="big" className="offer-message-label" bold>
                    {t('product_details:offer.offer_message')}
                  </Typography>
                  <div className={classnames('message-box', { trim: isLongMessage, expand: isExpandLongMessage })}>
                    {offerDetails?.offerMessage}
                    {isLongMessage && (
                      <Clickable className="message-footer" onClick={this._onToggleLongMessage}>
                        {!isExpandLongMessage ? t('product_details:offer.show_more') : t('product_details:offer.show_less')}
                      </Clickable>
                    )}
                  </div>
                </>
              )}
              {isShowAcceptButton && (
                <div className="footer">
                  <Button
                    disabled={receiveAmount < 0}
                    loading={loading}
                    className="accept-button"
                    onClick={() => this._onAcceptOffer(t)}
                  >
                    {t('product_details:offer.sell_with_person')}
                  </Button>
                  {!loading && (
                    <Clickable className="red-btn" onClick={this.close}>
                      <Typography poppins size="large" bold>
                        {t('common:close')}
                      </Typography>
                    </Clickable>
                  )}
                </div>
              )}
              {!isShowAcceptButton && (
                <div className="footer">
                  <Clickable className="red-btn" onClick={this.close}>
                    <Typography poppins size="large" bold>
                      {t('common:close')}
                    </Typography>
                  </Clickable>
                </div>
              )}
            </div>
          </>
        )}
      </Translation>
    );
  };

  render() {
    const { isOpen, loading, initing } = this.state;

    return (
      <Modal
        open={isOpen}
        onCancel={this.close}
        destroyOnClose
        maskClosable={!loading}
        closable={!loading}
        width={611}
        padding={0}
      >
        <MsgOfferModalStyled>
          {initing ? <Loading className="loading-box" size="small" /> : this._renderContent()}
        </MsgOfferModalStyled>
      </Modal>
    );
  }
}

export default MsgOfferModal;
