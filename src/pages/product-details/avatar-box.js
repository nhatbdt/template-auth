import { Component, memo } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';

import Media from '../../utils/media';
import Clickable from '../../components/clickable';
import { Images } from '../../theme';
import { login } from '../../utils/conditions';

const AvatarBoxStyled = styled.div`
  .avatar-box-content {
    max-width: 612px;
    max-height: 612px;
    position: relative;
    display: flex;
    align-items: center;
    justify-content: center;

    .thumbnail {
      position: relative;
      border-radius: 4px;
      ${Media.lessThan(Media.SIZE.MD)} {
        max-width: unset;
        max-height: unset;
        border-radius: 0;
        width: 100%;
      }
    }

    .swiper-container {
      width: 612px;
      max-width: 100%;

      position: relative;
      display: flex;
      flex-wrap: wrap;
      justify-content: flex-end;
      ${Media.lessThan(Media.SIZE.LG)} {
        z-index: unset;
        justify-content: space-between;
      }
      ${Media.lessThan(Media.SIZE.MD)} {
        width: 240px;
      }
      ${Media.lessThan(Media.SIZE.SM)} {
        width: 100%;
      }
      .swiper-button-prev,
      .swiper-button-next {
        color: #fff;
        &:after {
          font-size: 30px;
        }
      }
      .swiper-slide {
        width: 100%;
        padding-top: 100%;
        border-radius: 20px;
        background-repeat: no-repeat;
        background-size: contain;
        background-position: center;

        .item {
          width: 100%;
          height: 100%;
          display: flex;
          justify-content: center;
          align-items: center;
          background: rgba(33, 35, 48, 0.8);
          position: relative;

          &:hover {
            opacity: 1;
          }

          img {
            width: 100%;
            max-width: 463px;
            height: auto;
            max-height: 80%;
            object-fit: contain;

            &.thumbnail-blur {
              filter: blur(15px);
            }
          }
        }

        .favorite-action {
          display: flex;
          align-items: center;
          gap: 6px;
          position: absolute;
          top: 20px;
          left: 20px;
          .favorite-icon {
            width: 40px;
            ${Media.lessThan(Media.SIZE.LG)} {
              width: 36px;
            }
            ${Media.lessThan(Media.SIZE.MD)} {
              width: 40px;
            }
          }
          .favorite-count {
            font-weight: 600;
            font-size: 18px;
            color: #fff;
          }
        }
      }
    }
  }
`;

@withTranslation('product_details')
@inject(stores => ({
  productsStore: stores.products,
  lendingStore: stores.lending,
  product: stores.products.currentProductDetails,
}))
@observer
class AvatarBox extends Component {
  static propTypes = {
    product: PropTypes.object,
    lendingStore: PropTypes.object,
  };

  onLike = async () => {
    // e.stopPropagation();
    const { productsStore, product } = this.props;

    const { success, data } = await productsStore.favorite({
      productId: product?.id,
    });

    if (success) {
      product.setData({
        wishCount: data,
        isUserWished: !product?.isUserWished,
      });
    }
  };

  render() {
    const { product } = this.props;

    return (
      <AvatarBoxStyled>
        <div className="avatar-box-content">
          <div className="swiper-container">
            <div
              className="swiper-slide"
              style={{
                backgroundImage: `url('${product?.imageUrl}')`,
              }}
            >
              {/* <div className="item">
                <img
                  className="thumbnail"
                  src={product?.imageUrl}
                  alt=""
                />
              </div> */}
              <div className="favorite-action">
                <Clickable onClick={login(this.onLike)}>
                  <img
                    className="favorite-icon"
                    src={product?.isUserWished ? Images.RED_HEART_ICON : Images.GREY_HEART_ICON}
                    alt=""
                  />
                </Clickable>
                <span className="favorite-count">{product?.wishCount || 0}</span>
              </div>
            </div>
          </div>
        </div>
      </AvatarBoxStyled>
    );
  }
}

export default memo(AvatarBox);
