import { PureComponent, Fragment } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import moment from 'moment';
import { withTranslation } from 'react-i18next';
import classnames from 'classnames';
import lodash from 'lodash';
import numeral from 'numeral';

import Typography from '../../../components/typography';
import Thumbnail from '../../../components/thumbnail';
import Confirmable from '../../../components/confirmable';
import Button from '../../../components/button';
import { Images, Colors } from '../../../theme';
import { firestore } from '../../../services/firebase';
import Format from '../../../utils/format';
import Media from '../../../utils/media';

const Divider = styled.div`
  height: 1px;
  background-image: linear-gradient(to right, rgb(87, 87, 87) 10%, rgba(255, 255, 255, 0) 0%);
  background-size: 6px 3px;
  width: 100%;
  margin-bottom: 10px;

  ${Media.lessThan(Media.SIZE.MD)} {
    display: none;
  }
`;

const BidHistoriesBoxStyled = styled.div`
  padding-top: 31px;
  width: 100%;
  overflow-x: scroll;

  .title {
    color: #fff;
    font-size: 13px;
  }

  .auction-status-box {
    border-bottom: none;
    padding-bottom: 34px;

    ._title {
      margin-bottom: 14px;
    }

    ._status-box {
      display: flex;

      ._status-box-side {
        margin-right: 16px;

        &:last-child {
          margin-right: 0;
          flex: 1;
        }

        ._status-box-side-title {
          opacity: 0.25;
          margin-bottom: 12px;
        }

        .status-box-text {
          width: 135px;
          height: 80px;
          background-color: #1a219b;
          border-radius: 6px;
          display: flex;
          justify-content: center;
          align-items: center;
          font-size: 22px;
          font-weight: bold;
          /* color: #3cc8fc; */
          color: ${Colors.TEXT};
        }

        .transfer-status-box {
          height: 80px;
          box-shadow: 0 0 12px 0 rgba(0, 0, 0, 0.16);
          border-radius: 6px;
          display: flex;
          flex-direction: column;
          overflow: hidden;
          background-color: ${Colors.BLACK_1};

          ._inner-box {
            display: flex;
            align-items: center;
            flex: 1;

            .transfer-item-box {
              display: flex;
              flex: 1;
              align-items: center;
              opacity: 0.45;

              &:first-child {
                padding-left: 15px;
              }

              &:last-child {
                padding-right: 15px;
              }

              ._transfer-number {
                width: 33px;
                height: 33px;
                border-radius: 18px;
                display: flex;
                justify-content: center;
                align-items: center;
                border: 1px solid #ffffff;
                margin-right: 11px;
                min-width: 0;
              }

              ._transfer-label {
                margin-right: 10px;
                flex: 1.3;
              }

              ._transfer-status {
                font-size: 20px;
                flex: 1;
                white-space: nowrap;
              }

              &.active {
                opacity: 1;
              }
            }

            .divider-box {
              padding: 0 28px;
              opacity: 0.5;
              color: #fff;
            }
          }

          ._bottom-line {
            background-color: rgba(183, 183, 183, 0.1);
            display: flex;

            .thumb {
              height: 4px;
              background-color: #045afc;
              width: 45%;
            }

            &.right {
              justify-content: flex-end;
            }
          }
        }
      }
    }
  }

  .auction-info-box {
    padding-bottom: 14px;

    ._top {
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin-bottom: 25px;

      ._side-box {
        .current-price {
          font-size: 38px;
          line-height: 1.3;
          color: #3cc8fc;

          span {
            font-size: 16px;
            margin-left: 6px;
            color: #3cc8fc;
          }
        }

        .current-yen-price {
          opacity: 0.5;
        }

        .bid-button {
          padding: 0 95px;
          display: flex;
          align-items: center;
          justify-content: center;
        }
      }
    }

    ._bottom {
      display: flex;
      justify-content: space-between;

      .item {
        flex: 1;
        display: flex;
        justify-content: space-between;

        .item-left {
          flex: 1;
        }
        ._name {
          opacity: 0.35;
        }

        ._value {
          span {
            font-size: 12px;
            margin-left: 5px;
          }
        }

        .line {
          position: relative;
          flex: 1;
          &:after {
            position: absolute;
            content: '';
            width: 1px;
            top: 0;
            left: 25%;
            height: 100%;
            background-color: #fff;
            opacity: 0.1;
          }
        }

        &:nth-child(3) {
          .line {
            &:after {
              left: 50%;
            }
          }
        }
        &:nth-child(4) {
          .line {
            &:after {
              width: 0;
            }
          }
        }
      }
    }
  }
  table {
    display: block;
    width: 100%;
    min-width: 460px;
    margin-top: 28px;

    thead {
      tr {
        th {
          font-size: 12px;
          font-weight: normal;
          color: #b7b7b7;
          text-align: left;

          &:nth-child(1) {
            width: 10%;
            padding-left: 19px;
          }

          &:nth-child(2) {
            /* width: 30%; */
            width: 40%;
          }

          &:nth-child(3) {
            /* width: 15%; */
            width: 25%;
          }

          &:nth-child(4) {
            width: 15%;
          }

          &:nth-child(5) {
            /* width: 30%; */
            width: 10%;
          }
        }
      }
    }

    tbody {
      tr {
        height: 53px;
        border-bottom: 2px solid rgb(255 255 255 / 8%);

        &.processing {
          td {
            &:nth-child(5) {
              p {
                color: #3cc8fc;
              }
            }
          }
        }

        &:nth-child(1) {
          background: #00153b;
          border-bottom: 1px solid rgb(255 255 255 / 8%);
        }

        &:nth-child(1),
        &:nth-child(2),
        &:nth-child(3) {
          td {
            &:nth-child(1),
            &:nth-child(2) {
              p {
                color: #3cc8fc;
                &.date-time {
                  color: ${Colors.PLACEHOLDER};
                  font-size: 12px;
                }
              }
            }
          }
        }
        td {
          &:nth-child(1) {
            padding-left: 19px;
          }

          &:nth-child(5) {
            text-align: right;
          }

          .status-text {
            &.success {
              color: #fff;
            }

            &.processing {
              color: #3cc8fc;
            }
          }

          .horizon {
            display: flex;
            align-items: center;

            .avatar {
              margin-right: 9px;
              min-width: 0;
            }

            .user-name {
              font-size: 13px;
              color: ${Colors.RED_2};
              overflow: hidden;
              text-overflow: ellipsis;
              max-width: 180px;
              white-space: nowrap;
              flex: 1;
              margin-right: 5px;
            }
            .date-time {
              color: ${Colors.PLACEHOLDER};
              font-size: 12px;
            }

            .fire-icon {
              margin-right: 23px;
            }

            .heat-number {
              color: #3cc8fc;
            }
          }

          .date {
            color: ${Colors.TEXT};
          }

          .cancel-button {
            height: 32px;
            font-size: 11px;
            padding: 0 15px;
          }
        }

        &.canceled {
          td {
            opacity: 0.5;
            .user-name {
                color: ${Colors.TEXT};
              }
            }
          }
        }
      }
    }

    &.ended {
      tbody {
        tr {
          height: 53px;
          border-bottom: 2px solid rgb(255 255 255 / 8%);

          &:nth-child(1),
          &:nth-child(2),
          &:nth-child(3) {
            background-color: transparent;
            background-image: none;
          }

          &.success {
            background: #00153b;
            border-bottom: 1px solid rgb(255 255 255 / 8%);
          }
        }
      }
    }
  }
  .time-range-box {
    display: flex;
    flex-direction: column;

    .typography {
      margin-bottom: 8px;
      text-align: right;

      &:last-child {
        margin-bottom: 0;
      }

      span {
        font-size: 12px;
        margin-right: 13px;
        opacity: 0.35;
      }
    }
  }

  ${Media.lessThan(Media.SIZE.XXL)} {
    .auction-status-box {
      ._status-box {
        ._status-box-side {
          .transfer-status-box {
            ._inner-box {
              .transfer-item-box {
                ._transfer-status {
                  font-size: 18px;
                }
              }
            }
          }
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    padding-top: 0;
    margin-bottom: 30px;

    .auction-status-box {
      ._status-box {
        flex-direction: column;

        ._status-box-side {
          margin-right: 0;

          .status-box-text {
            width: auto;
            flex: 1;
            margin-bottom: 20px;
            background-color: #1a219b;
          }

          .transfer-status-box {
            ._inner-box {
              .transfer-item-box {
                &:first-child {
                  padding-left: 5px;
                }

                &:last-child {
                  padding-right: 5px;
                }

                ._transfer-number {
                  width: 20px;
                  height: 20px;

                  .typography {
                    font-size: 12px;
                  }
                }

                ._transfer-label {
                  margin-right: 5px;
                  font-size: 12px;
                  flex: 2;
                }

                ._transfer-status {
                  font-size: 14px;
                }
              }

              .divider-box {
                padding: 0 14px;
              }
            }

            ._bottom-line {
              .thumb {
                height: 5px;
              }
            }
          }
        }
      }
    }

    .time-range-box {
      .typography {
        margin-bottom: 4px;
      }
    }

    table {
      margin-top: 15px;
      table-layout: fixed;

      thead {
        tr {
          th {
            &:nth-child(1) {
              width: 10%;
            }

            &:nth-child(2) {
              display: none;
            }

            &:nth-child(3) {
              width: 25%;
            }

            &:nth-child(4) {
              width: 25%;
            }

            &:nth-child(5) {
              width: 40%;
            }
            // &:nth-child(6) {
            //   width: 15%;
            // }
          }
        }
      }
      tbody {
        tr {
          height: 53px;
          border-bottom: 2px solid rgb(255 255 255 / 8%);

          td {
            .date {
              // display: none;
            }

            &:nth-child(2) {
              display: none;
            }

            .cancel-button {
              margin-left: -80px;
              height: 32px;
              font-size: 11px;
              padding: 0 15px;
            }
          }
        }
      }
    }
    .auction-info-box {
      ._bottom {
        flex-wrap: wrap;
        .item {
          width: 50%;
          flex: unset;
          padding-bottom: 20px;

          &:nth-child(2) {
            .line {
              &:after {
                width: 0;
              }
            }
          }
          &:nth-child(3) {
            .line {
              &:after {
                left: 25%;
              }
            }
          }
        }
      }
    }
    .field-box {
      .input-wrapper {
        .input-box {
          input {
            border: 1px solid;
          }
        }
      }
    }
  }
  ${Media.lessThan(Media.SIZE.XS)} {
    .title {
      padding-top: 20px;
    }
    .auction-info-box {
      ._bottom {
        flex-wrap: wrap;
        .item {
          width: 50%;
          flex: unset;
          padding-bottom: 20px;

          &:nth-child(2) {
            .line {
              &:after {
                width: 0;
              }
            }
          }
          &:nth-child(3) {
            .line {
              &:after {
                left: 25%;
              }
            }
          }
        }
      }
    }
    table {
      thead {
        tr {
          th {
            text-align: center;
          }
        }
      }
      tbody {
        tr {
          height: 35px;
          td {
            p {
              text-align: center;
            }

            .cancel-button {
              margin-left: -48px;
              height: 32px;
              font-size: 11px;
              padding: 0 12px;
            }
          }
        }
      }
      &.ended {
        tbody {
          tr {
            height: 35px;
            &.success {
              background: transparent;
              background: linear-gradient(0deg, rgba(55, 101, 135, 0.08) 0%, rgba(52, 146, 181, 1) 85%);
            }
          }
        }
      }
    }
  }
  ${Media.lessThan(Media.SIZE.XXXS)} {
    table tbody tr td .cancel-button {
      margin-left: -30px;
    }
  }
`;

@withTranslation('product_details')
@inject(stores => ({
  authStore: stores.auth,
  bidStore: stores.bid,
  product: stores.products.currentProductDetails,
  productsStore: stores.products,
}))
@observer
class BidHistoriesBox extends PureComponent {
  static propTypes = {
    product: PropTypes.object,
    authStore: PropTypes.object,
    bidStore: PropTypes.object,
    productStore: PropTypes.object,
  };

  state = {
    loading: null,
    priceRates: null,
    firstTwoBidHistories: [],
  };

  async componentDidMount() {
    const { productsStore, product, authStore } = this.props;

    this._initing = true;
    const getPriceRateResult = await productsStore.getPriceRate();
    const priceRates = lodash.reduce(
      getPriceRateResult?.data,
      (result, item) => {
        result[item?.name] = +item?.value;
        return result;
      },
      {},
    );
    this.setState({
      priceRates,
    });

    const isAuctionEnded = product?.productBid?.status !== 'NEW';

    this._unsubscribe = firestore
      .bidHistories()
      .where('productId', '==', product?.id)
      .where('productBidId', '==', product?.productBid?.id)
      .onSnapshot(snapshot => {
        let data = [];

        snapshot.forEach(doc => {
          const docData = doc?.data();
          const obj = {
            ...docData,
            bidPrice: +docData?.bidPrice,
            id: doc.id,
          };
          data.push(obj);
        });

        // data = lodash.orderBy(data, 'bidPrice', 'desc');
        data = lodash.orderBy(data, ['createdAt'], ['desc'])?.slice(0, 100);

        let firstTwoBidHistories = [];
        let biggestBidPrice = 0;

        data.forEach(item => {
          if (item.userId === authStore.initialData.userId && item.userBidStatus === 'NEW') {
            firstTwoBidHistories.push(item);
          }

          if (!biggestBidPrice && !['CANCEL', 'FAIL'].includes(item.userBidStatus)) {
            biggestBidPrice = +item.bidPrice;
          }
        });

        firstTwoBidHistories = firstTwoBidHistories.slice(0, 2);

        this.setState({
          firstTwoBidHistories,
        });

        isAuctionEnded && productsStore.setBidsHistory(data);
      });
  }

  componentWillUnmount() {
    this._unsubscribe();
  }

  _onCancelBid = async item => {
    const { product, bidStore, t } = this.props;

    const ok = await Confirmable.open({
      content: t('product_details:bid.cancel_this_bid'),
      cancelButtonText: t('common:close'),
    });

    if (ok) {
      this.setState({
        loading: 'cancel_bid',
      });

      try {
        await bidStore.cancelBid({
          productId: product.id,
          id: item.productBidHistoryId,
        });
      } catch (e) {
        // eslint-disable-next-line no-console
        console.error(e);
      }

      this.setState({
        loading: null,
      });
    }
  };

  _renderItem = (item, index) => {
    const { t, product } = this.props;
    const { loading, firstTwoBidHistories } = this.state;
    const timeFormat = 'YYYY/MM/DD HH:mm:ss';

    return (
      <tr
        key={index}
        className={classnames({
          success: item.buyingFlag,
          processing: item.userBidStatus === 'NEW',
          canceled: item.userBidStatus === 'CANCEL',
        })}
      >
        <td>
          <Typography size="oversmall" bold className="index-number">
            {index + 1}
          </Typography>
        </td>
        <td>
          <div className="horizon">
            <Thumbnail
              size={30}
              rounded
              className="avatar"
              placeholderUrl={Images.USER_PLACEHOLDER}
              url={item.userAvatar}
            />
            <div>
              <Typography poppins className="user-name">
                {`@${item?.userName || item?.userPublicAddress || 'Unknown'}`}
              </Typography>
              <Typography poppins className="date-time">
                {moment(item.createdAt).format(timeFormat)}
              </Typography>
            </div>
          </div>
        </td>
        <td>
          <Typography size="large" bold poppins>
            {Format.price(item.bidPrice)}
          </Typography>
        </td>
        <td>
          <Typography size="small" className="date">
            {product?.currency}
          </Typography>
        </td>
        <td>
          <Typography size="small" className={classnames('status-text', item.userBidStatus?.toLowerCase())}>
            {item.userBidStatus === 'CANCEL'
              ? t('bid.cancel')
              : ['FAIL'].includes(item.userBidStatus)
              ? t('bid.fail')
              : ['SUCCESS'].includes(item.userBidStatus) && item.buyingFlag
              ? t('bid.highest_bidder')
              : ['SUCCESS'].includes(item.userBidStatus)
              ? t('bid.waiting')
              : t('bid.waiting')}
          </Typography>
        </td>
        <td>
          {item.id === firstTwoBidHistories[0]?.id && product.isBidValid && product.isCancelBidTimeValid && (
            <Button
              loading={loading === 'cancel_bid'}
              className="cancel-button"
              onClick={() => this._onCancelBid(item)}
            >
              {t('common:cancel')}
            </Button>
          )}
        </td>
      </tr>
    );
  };

  _renderAuctionStatusBox = () => {
    const { t, product } = this.props;
    const { transferNBNGStatus, transferTokenStatus, currency } = product.productBid;
    // const tokenWithdrawName = currency === 'ETH' ? t('bid.withdraw_eth') : t('bid.withdraw_prx');
    const tokenWithdrawName = t('bid.withdraw_curency', { currency });

    const step = transferNBNGStatus !== 'SUCCESS' ? 1 : transferTokenStatus !== 'SUCCESS' ? 2 : 2;
    const statuses = [
      {
        name: tokenWithdrawName,
        status: transferNBNGStatus && t(`bid.${transferNBNGStatus.toLowerCase()}`),
      },
      {
        name: t('bid.transfer_nft'),
        status: transferTokenStatus && t(`bid.${transferTokenStatus.toLowerCase()}`),
      },
    ];

    return (
      <div className="auction-status-box">
        <div className="_status-box">
          <div className="_status-box-side">
            <Typography size="small" className="_status-box-side-title">
              {t('bid.auction_status')}
            </Typography>
            <div className="status-box-text bold">{t('bid.finished')}</div>
          </div>
          <div className="_status-box-side">
            <Typography size="small" className="_status-box-side-title">
              {t('bid.auction_process')}
            </Typography>
            <div className="transfer-status-box">
              <div className="_inner-box">
                {statuses.map((item, index) => (
                  <Fragment key={index}>
                    <div className={classnames('transfer-item-box', { active: step === index + 1 })}>
                      <div className="_transfer-number">
                        <Typography bold size="big" poppins>
                          {index + 1}
                        </Typography>
                      </div>
                      <Typography className="_transfer-label" bold>
                        {item.name}
                      </Typography>
                      <Typography className="_transfer-status" poppins bold>
                        {item.status}
                      </Typography>
                    </div>
                    {index === 0 && <div className="divider-box">{'>>>'}</div>}
                  </Fragment>
                ))}
              </div>
              <div className={classnames('_bottom-line', { right: step === 2 })}>
                <div className="thumb" />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  _renderTimeRangeBox = () => {
    const { product, t } = this.props;
    const { startTime, endTime } = product.productBid;

    return (
      <div className="time-range-box">
        <Typography poppins>
          <span>{t('bid.start_time')}</span>
          {moment(startTime).format('YYYY/MM/DD HH:mm:ss')}
        </Typography>
        <Typography poppins>
          <span>{t('bid.end_time')}</span>
          {moment(endTime).format('YYYY/MM/DD HH:mm:ss')}
        </Typography>
      </div>
    );
  };

  _renderAuctionInfoBox = () => {
    const { t, product } = this.props;
    const { startPrice, expectPrice, biggestBidPrice, biggestBidPriceYen, yenStartPrice, expectPriceDisplay } =
      product.productBid;
    const { priceRates } = this.state;
    const USD_TO_JPY = priceRates?.USD_TO_JPY;

    const items = [
      {
        label: t('bid.start_price'),
        value: Format.price(startPrice),
      },
      {
        label: t('bid.expect_price'),
        value: Format.price(expectPriceDisplay),
      },
      // {
      //   label: t('bid.buy_out'),
      //   value: '-　　',
      // },
      {
        label: t('bid.decision_price'),
        value: expectPrice ? Format.price(expectPrice) : t('bid.expect_price_null'),
        hideUnit: !expectPrice,
      },
    ];

    return (
      <div className="auction-info-box">
        <div className="_top">
          <div className="_side-box">
            <Typography size="oversmall">{t('bid.current_price')}</Typography>
            <Typography poppins className="current-price" bold>
              {Format.price(biggestBidPrice || startPrice)}
              <span>{product?.currency}</span>
            </Typography>
            <Typography poppins className="current-yen-price">
              {Format.price(biggestBidPriceYen || yenStartPrice)}
              {t('common:yen')}
              {' / '}
              {numeral((biggestBidPriceYen || yenStartPrice) * (1 / USD_TO_JPY)).format('0,0.0000')} {t('common:usd')}
            </Typography>
          </div>
          <div className="_side-box">{this._renderTimeRangeBox()}</div>
        </div>
        <div className="_bottom">
          {items.map((item, index) => (
            <div className="item" key={index}>
              <div className="item-left">
                <Typography size="small" className="_name">
                  {item.label}
                </Typography>
                <Typography className="_value" poppins size="big">
                  {item.value}
                  {!item.hideUnit && <span>{product?.currency}</span>}
                </Typography>
              </div>
              {/* <div className="line" /> */}
            </div>
          ))}
        </div>
      </div>
    );
  };

  render() {
    const { t, product, productsStore } = this.props;

    if (productsStore?.bidsHistory?.length < 1) return null;

    const isAuctionEnded = product.productBid.status !== 'NEW';

    return (
      <>
        <BidHistoriesBoxStyled>
          {isAuctionEnded && this._renderAuctionStatusBox()}
          {isAuctionEnded && this._renderAuctionInfoBox()}
          {isAuctionEnded && <Divider />}
          <Typography className="title">{t('bid.bid_history')}</Typography>
          <table className={classnames({ ended: isAuctionEnded })}>
            <thead>
              <tr>
                <th>#</th>
                <th>{t('product_details:bid.bid_and_time')}</th>
                <th>{t('product_details:bid.amount')}</th>
                <th>{t('common:header.currency')}</th>
                <th>{t('common:status')}</th>
                <th> </th>
              </tr>
            </thead>
            <tbody>{productsStore?.bidsHistory?.map(this._renderItem)}</tbody>
          </table>
        </BidHistoriesBoxStyled>
      </>
    );
  }
}

export default BidHistoriesBox;
