import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import moment from 'moment';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';

import Button from '../../../components/button';
import Typography from '../../../components/typography';
import { Colors } from '../../../theme';
import { login } from '../../../utils/conditions';
import Format from '../../../utils/format';
import Media from '../../../utils/media';
import { PRODUCT_STATUSES } from '../../../constants/statuses';
import { firestore } from '../../../services/firebase';
import lodash from 'lodash';
import BidModal from './bid-modal';
import CURRENCIES from '../../../constants/currencies';
import { getRateByCurrency } from '../../../utils/rates';

const BidBoxStyled = styled.div`
  margin-top: 20px;
  padding: 24px 30px;
  border-radius: 10px;
  background-color: #15171c;
  > * {
    border-bottom: 1px solid #e5e5e5;

    &:last-child {
      border-bottom: none;
    }
  }

  .auction-info-box {
    padding-bottom: 14px;

    ._top {
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin-bottom: 25px;

      ._side-box {
        width: 100%;
        .typography {
          color: ${Colors.TEXT};
          font-size: 16px;
          font-weight: 500;
        }
        .current-price {
          font-size: 26px;
          line-height: 1.3;
          color: ${Colors.BLUE_TEXT};

          span {
            font-size: 16px;
            margin-left: 6px;
            color: ${Colors.BLUE_TEXT};
          }
        }

        .current-yen-price {
          opacity: 0.5;
        }

        .bid-button {
          text-align: right;
          padding: 0 95px;
          display: flex;
          align-items: center;
          justify-content: center;
          background-color: #376587;
          color: #ffffff;
          font-size: 16px;
          height: 40px;
          width: 100%;
          background: #045afc;
          border-radius: 9999px;
        }
      }
    }

    ._bottom {
      display: flex;
      justify-content: space-between;

      .item {
        ._name {
          /* opacity: 0.35; */
          font-weight: 500;
        }

        ._value {
          span {
            font-size: 12px;
            margin-left: 5px;
          }
        }
      }
    }
  }

  .countdown-box {
    padding: 10px 0;
    padding-bottom: 0;
    display: flex;
    justify-content: space-between;
    align-items: center;

    ._side-box {
      .countdown-text {
        font-size: 34px;

        span {
          font-size: 16px;
          margin: 0 5px;
        }
      }
    }
  }

  .time-range-box {
    display: flex;
    flex-direction: column;

    .typography {
      margin-bottom: 8px;
      text-align: right;

      &:last-child {
        margin-bottom: 0;
      }

      span {
        font-size: 12px;
        margin-right: 13px;
        opacity: 0.35;
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .auction-info-box {
      ._top {
        justify-content: flex-start;
        flex-direction: column;

        ._side-box {
          display: flex;
          flex-direction: column;
          align-items: center;

          .current-yen-price {
            margin-bottom: 20px;
          }

          .bid-button {
            padding: 0 60px;
          }
        }
      }

      ._bottom {
        justify-content: flex-start;
        flex-wrap: wrap;

        .item {
          width: 50%;
          display: flex;
          flex-direction: column;
          align-items: center;
        }
      }
    }

    .countdown-box {
      justify-content: flex-start;
      flex-direction: column;

      ._side-box {
        display: flex;
        flex-direction: column;
        align-items: center;

        &:first-child {
          margin-bottom: 10px;
        }
      }
    }

    .time-range-box {
      .typography {
        margin-bottom: 4px;
      }
    }
  }

  ${Media.lessThan(Media.SIZE.XXXS)} {
    .countdown-box ._side-box .countdown-text {
      font-size: 30px;
    }
  }
`;

@withTranslation(['product_details', 'common'])
@inject(stores => ({
  authStore: stores.auth,
  product: stores.products.currentProductDetails,
  productsStore: stores.products,
}))
@observer
class BidBox extends Component {
  static propTypes = {
    product: PropTypes.object,
    authStore: PropTypes.object,
    productsStore: PropTypes.object,
  };

  constructor(props) {
    super(props);
    const { product } = props;
    const currentTime = moment(product.currentTime);
    const startTime = moment(product.productBid?.startTime);
    const endTime = moment(product.productBid.endTime);

    const remainingSecondNumber = endTime.diff(currentTime, 's');
    this._validPoint = endTime.diff(startTime, 's');

    this.state = this._handleState(product, remainingSecondNumber);
  }

  componentDidMount() {
    const { remainingSecondNumber } = this.state;
    const { product, authStore, productsStore } = this.props;

    if (remainingSecondNumber > 0) {
      this._initInterval();
      document.addEventListener('visibilitychange', this._handleVisibilityChange);
    }

    this._initing = true;
    this._unsubscribe = firestore
      .bidHistories()
      .where('productId', '==', product?.id)
      .where('productBidId', '==', product?.productBid?.id)
      .onSnapshot(snapshot => {
        let data = [];

        snapshot.forEach(doc => {
          const docData = doc?.data();
          const obj = {
            ...docData,
            bidPrice: +docData?.bidPrice,
            id: doc.id,
          };
          data.push(obj);
        });

        // data = lodash.orderBy(data, 'bidPrice', 'desc');
        data = lodash.orderBy(data, ['createdAt'], ['desc'])?.slice(0, 100);

        let firstTwoBidHistories = [];
        let biggestBidPrice = 0;

        data.forEach(item => {
          if (item?.userId === authStore?.initialData?.userId && item?.userBidStatus === 'SUCCESS') {
            firstTwoBidHistories.push(item);
          }

          if (!biggestBidPrice && !['CANCEL', 'FAIL'].includes(item?.userBidStatus)) {
            biggestBidPrice = +item?.bidPrice;
          }
        });

        firstTwoBidHistories = firstTwoBidHistories.slice(0, 2);
        productsStore.setBidsHistory(data);
        productsStore.setFirstTwoBidHistories(firstTwoBidHistories);

        if (this._initing) {
          this._initing = false;
        } else {
          let hasNewBid = false;
          snapshot.docChanges().forEach(change => {
            if (change.type === 'added') {
              hasNewBid = true;
            }
          });
          this._onReceiveNewBid(data[0], hasNewBid, biggestBidPrice);
        }
      });
  }

  componentWillUnmount() {
    clearInterval(this._interval);
    if (this._timer) clearTimeout(this._timer);
    document.removeEventListener('visibilitychange', this._handleVisibilityChange);
    this._unsubscribe();
  }

  _init = () => {
    const { product } = this.props;
    const currentTime = moment(product.currentTime);
    const startTime = moment(product.productBid?.startTime);
    const endTime = moment(product.productBid.endTime);

    const remainingSecondNumber = endTime.diff(currentTime, 's');
    this._validPoint = endTime.diff(startTime, 's');

    this.setState(this._handleState(product, remainingSecondNumber));
  };

  // check onscreen
  _handleVisibilityChange = async () => {
    if (document.visibilityState !== 'visible') {
      // not onscreen
    } else {
      // comback onscreen
      const { product, productsStore } = this.props;
      await productsStore?.getProductDetails({ id: product?.id });
      this._init();
    }
  };

  _onReceiveNewBid = (newBid, hasNewBid, biggestBidPrice) => {
    const { product, productsStore } = this.props;
    product.productBid.setData({
      // **add 10min to endTime if has new bid**
      // endTime: hasNewBid
      //   ? +moment(product.productBid.endTime).add(10, 'minutes').format('x')
      //   : product.productBid.endTime,
      biggestBidPrice,
    });

    product?.setData({ price: biggestBidPrice });

    const expectPrice = product?.productBid?.expectPrice;
    if (expectPrice && biggestBidPrice >= expectPrice) {
      productsStore.getProductDetails({ id: product?.id });
    }
  };

  _initInterval = () => {
    this._interval = setInterval(() => {
      const { product, productsStore } = this.props;
      let { remainingSecondNumber } = this.state;
      remainingSecondNumber -= 1;

      if (remainingSecondNumber === this._validPoint) {
        product.setData({ status: PRODUCT_STATUSES.SALE, bidFlag: true });
      }

      if (remainingSecondNumber < 0) {
        const isProductHasBid = !!product.productBid.biggestBidPrice;
        clearInterval(this._interval);
        product.setData({
          status: isProductHasBid ? PRODUCT_STATUSES.SOLD : product.status,
          bidFlag: false,
        });
        this._timer = setTimeout(async () => {
          await productsStore.getProductDetails({ id: product?.id });
        }, 6000);

        return null;
      }

      this.setState(this._handleState(product, remainingSecondNumber));

      return null;
    }, 1000);
  };

  _handleState = (product, remainingSecondNumber) => {
    const isTimeValid = remainingSecondNumber <= this._validPoint && remainingSecondNumber > 0;
    const remainingTime = moment().startOf('month').seconds(remainingSecondNumber);

    return {
      isTimeValid,
      remainingSecondNumber,
      second: remainingTime.format('ss'),
      minute: remainingTime.format('mm'),
      hour: remainingTime.format('HH'),
      day: +remainingTime.format('DD') - 1,
    };
  };

  _onBid = async () => {
    this._bigModal.open();
  };

  _renderAuctionInfoBox = isShowCountdown => {
    const { t, product, authStore } = this.props;
    const { startPrice, expectPrice, biggestBidPrice, yenStartPrice, expectPriceDisplay } = product.productBid;
    const isBidButtonActive = authStore.initialData.userId !== product.userId && product.isBidValid;

    const items = [
      {
        label: t('bid.start_price'),
        value: Format.price(startPrice),
      },
      {
        label: t('bid.expect_price'),
        value: Format.price(expectPriceDisplay),
      },
      // {
      //   label: t('bid.buy_out'),
      //   value: '-　　',
      // },
      {
        label: t('bid.decision_price'),
        value: expectPrice ? Format.price(expectPrice) : t('bid.expect_price_null'),
        hideUnit: !expectPrice,
      },
    ];

    const masterData = authStore.initialData;
    const biggestBidPriceYen = getRateByCurrency(CURRENCIES[product.currency], masterData) * biggestBidPrice;

    return (
      <div className="auction-info-box">
        <div className="_top">
          <div className="_side-box">
            <Typography size="oversmall">{t('bid.current_price')}</Typography>
            <Typography poppins className="current-price" bold>
              {Format.price(biggestBidPrice || startPrice)}
              <span>{product?.currency}</span>
            </Typography>
            <Typography poppins className="current-yen-price">
              {Format.price(biggestBidPriceYen || yenStartPrice, '0,0')}
              {t('common:yen')}
            </Typography>
          </div>
          <div className="_side-box">
            {isShowCountdown ? (
              <Button size="large" disabled={!isBidButtonActive} className="bid-button" onClick={login(this._onBid)}>
                {t('bid.put_a_limit')}
              </Button>
            ) : (
              this._renderTimeRangeBox()
            )}
          </div>
        </div>
        <div className="_bottom">
          {items.map((item, index) => (
            <div className="item" key={index}>
              <Typography size="small" className="_name">
                {item.label}
              </Typography>
              <Typography className="_value" poppins size="big">
                {item.value}
                {!item.hideUnit && <span>{product?.currency}</span>}
              </Typography>
            </div>
          ))}
        </div>
      </div>
    );
  };

  _renderTimeRangeBox = () => {
    const { product, t } = this.props;
    const { startTime, endTime } = product.productBid;

    return (
      <div className="time-range-box">
        <Typography poppins>
          <span>{t('bid.start_time')}</span>
          {moment(startTime).format('YYYY/MM/DD HH:mm:ss')}
        </Typography>
        <Typography poppins>
          <span>{t('bid.end_time')}</span>
          {moment(endTime).format('YYYY/MM/DD HH:mm:ss')}
        </Typography>
      </div>
    );
  };

  _renderCountdownBox = () => {
    const { t } = this.props;
    const { second, minute, hour, day } = this.state;

    return (
      <div className="countdown-box">
        <div className="_side-box">
          <Typography className="countdown_label" size="oversmall" secondary>
            {t('bid.remaining')}
          </Typography>
          <Typography poppins className="countdown-text" bold>
            {day > 9 ? '' : '0'}
            {day}
            <span>{t('bid.day')}</span>
            {hour}
            <span>{t('bid.hour')}</span>
            {minute}
            <span>{t('bid.minute')}</span>
            {second}
            <span>{t('bid.second')}</span>
          </Typography>
        </div>
        <div className="_side-box">{this._renderTimeRangeBox()}</div>
      </div>
    );
  };

  render() {
    const { product } = this.props;
    const { isTimeValid } = this.state;

    const isShowCountdown = isTimeValid && product?.productBid?.status === 'NEW';

    return (
      <>
        <BidBoxStyled>
          {this._renderAuctionInfoBox(isShowCountdown)}
          {isShowCountdown && this._renderCountdownBox()}
        </BidBoxStyled>
        <BidModal
          width={560}
          ref={ref => {
            this._bigModal = ref;
          }}
        />
      </>
    );
  }
}

export default BidBox;
