import styled from 'styled-components';
import Media from '../../../utils/media';
import { Colors } from '../../../theme';

const StyledDiv = styled.div`
  .modal-title {
    font-size: 22px;
    font-weight: 700;
    align-items: center;
    margin-bottom: 33px;
  }

  .field-box {
    /* margin-bottom: 42px; */

    ._label {
      margin-bottom: 17px;
    }

    .input-wrapper {
      display: flex;
      border: solid 1px #dedede;
      border-radius: 8px;
      overflow: hidden;

      .price_input {
        display: flex;

        ._label_price {
          display: none;
        }

        .input-unit-box {
          width: 166px;
          display: flex;
          align-items: center;
          justify-content: center;
          /* background-color: #dedede66; */

          .nbng-icon {
            width: 33px;
            height: 33px;
            background-color: #ffffff;
            border-radius: 17px;
            display: flex;
            justify-content: center;
            box-shadow: 0 0 6px 0 rgb(0 0 0 / 16%);
            margin-right: 8px;
            justify-content: center;
            align-items: center;
            overflow: hidden;

            img {
              width: 25px;
              height: 25px;
              object-fit: cover;
            }
          }

          .arrow-icon {
            margin-left: 20px;
          }
        }
      }

      .input-box {
        flex: 1;

        input {
          border: none;
          border-radius: 0;
          font-weight: bold;
          font-size: 24px;
          font-family: 'Poppins', sans-serif;
          padding: 0 22px;
          text-align: right;
          height: 50px;
          color: #282828;

          &:hover,
          &:focus {
            box-shadow: none;
          }
        }

        .error-box {
          position: absolute;
          bottom: 5px;
          right: 22px;

          p {
            font-size: 11px;
          }
        }
      }
    }

    .jpy-rate {
      opacity: 0.63;
      text-align: right;
      /* margin-top: 10px; */
      /* margin-right: 22px; */
    }

    .jpy-price {
      opacity: 0.8;
      text-align: right;
      margin-top: 6px;
      margin-right: 22px;
    }

    .accept-terms-checkbox {
      margin-top: 20px;
      margin-bottom: 20px;
      span {
        /* color: #282828; */
        color: ${Colors.TEXT};
        &.ant-checkbox {
          /* border: 1px solid #282828; */
          border: 1px solid ${Colors.BORDER};
        }
      }
      .ant-checkbox-wrapper {
        > span {
          /* color: #282828; */
          color: ${Colors.TEXT};
          &:last-child {
            font-size: 12px;
            opacity: 0.5;
          }
        }
        .ant-checkbox-inner {
          /* border-color: #045AFC; */
          background-color: transparent;
        }
      }
    }
  }

  .bottom-box {
    display: flex;
    justify-content: center;
    flex-wrap: wrap;
    gap: 4px 12px;

    button {
      background-color: #045afc;
      /* padding: 0 82px; */
      padding: 0 40px;
      min-width: 140px;
      border-radius: 999px;
      &.cancel-btn {
        background-color: transparent;
        border-radius: 9999px;
        border: 1px solid ${Colors.BORDER};
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .field-box {
      ._label {
        display: none;
      }

      .input-wrapper {
        flex-direction: column;
        border: none;
        border-radius: 0;

        .price_input {
          display: flex;
          justify-content: space-between;

          ._label_price {
            display: inline;
          }

          .input-unit-box {
            background-color: transparent;
            display: flex;
            justify-content: flex-end;
            margin-bottom: 17px;
            .nbng-icon {
              margin-right: 8px;
              img {
                width: 25px;
              }
            }
          }
        }
        .input-box {
          input {
            border-radius: 8px;
            border: solid 1px #3f414c;
          }
        }
      }
    }
  }
  ${Media.lessThan(Media.SIZE.XXXS)} {
    .field-box {
      .input-wrapper {
        .input-box {
          input {
            border-radius: 8px;
            border: solid 1px #3f414c;
          }
        }
      }
    }
  }
  /* .ant-checkbox-wrapper:hover .ant-checkbox-inner,
  .ant-checkbox:hover .ant-checkbox-inner,
  .ant-checkbox-input:focus + .ant-checkbox-inner {
  } */
  /* border-color: #3CC8FC; */

  /* .ant-checkbox-checked::after {
    border-color: #3cc8fc;
  } */
`;

export { StyledDiv };
