import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import { inject, observer } from 'mobx-react';
import moment from 'moment';
import { Translation } from 'react-i18next';
import styled from 'styled-components';

import Modal from '../../../components/modal';
import Button from '../../../components/button';
import Checkbox from '../../../components/checkbox';
import Confirmable from '../../../components/confirmable';
import Field from '../../../components/field';
import Typography from '../../../components/typography';
import { firestore } from '../../../services/firebase';
import {
  getWeb3Instance,
  checkCorrespondingNetwork,
  checkBalance,
  getAllowanceByToken,
  approveMaxNew,
} from '../../../utils/web3';
import { Colors } from '../../../theme';
import Format from '../../../utils/format';
import CURRENCIES from '../../../constants/currencies';
import { StyledDiv } from './styled';
import { getRateByCurrency } from '../../../utils/rates';
import { CHAIN_LIST } from '../../../constants/chains';
import ProductInputPrice from '../../../components/product-input-price';
import MaskLoading from '../../../components/mask-loading';
import { MAX_LENGTH_INPUT_VALUE_FRACTION, MAX_LENGTH_INPUT_VALUE_INTEGER } from '../../../constants/common';

const validationSchema = yup.object().shape({
  bidPrice: yup.number().nullable().required('PRICE_REQUIRED'),
});

@inject(stores => ({
  bidStore: stores.bid,
  authStore: stores.auth,
  productsStore: stores.products,
  product: stores.products.currentProductDetails,
}))
@observer
class BidModal extends Component {
  static propTypes = {
    product: PropTypes.object,
    authStore: PropTypes.object,
    bidStore: PropTypes.object,
    productsStore: PropTypes.object,
  };

  state = {
    isOpen: false,
    loading: false,
  };

  componentWillUnmount() {
    if (this._unsubscribe) {
      this._unsubscribe();
    }
  }

  open = async () => {
    this.setState({ isOpen: true });
  };

  close = () => this.setState({ isOpen: false });

  _bidQueueListener = documentId =>
    new Promise((resolve, reject) => {
      const timeOut = setTimeout(() => {
        this._unsubscribe();
        // eslint-disable-next-line prefer-promise-reject-errors
        reject('ERROR_TIMEOUT');
      }, 180000);

      this._unsubscribe = firestore
        .bids()
        .doc(documentId)
        .onSnapshot(
          snapshot => {
            const data = snapshot.data();

            if (data?.status) {
              resolve(data);

              this._unsubscribe();
              clearTimeout(timeOut);
            }
          },
          e => reject(e),
        );
    });

  // _checkBalance = async bidPrice => {
  //   const { product, authStore } = this.props;

  //   const isEnoughBalance = await checkEnoughBalance(
  //     product.currency === CURRENCIES.ETH ? CURRENCIES.WETH : product.currency,
  //     bidPrice,
  //     authStore.initialData.publicAddress,
  //   );

  //   // eslint-disable-next-line no-throw-literal
  //   if (!isEnoughBalance) throw { error: 'ERROR_BALANCE_NOT_ENOUGH' };
  // };

  _onSubmit = async (values, t) => {
    const { product, bidStore, authStore, productsStore } = this.props;

    this.setState({
      loading: true,
    });
    MaskLoading.open({});

    this._productBidHistoryId = null;

    try {
      const { web3 } = await getWeb3Instance();
      await checkCorrespondingNetwork(product.chainId);

      // eslint-disable-next-line no-throw-literal
      if (!web3) throw { error: 'ERROR' };

      // await this._checkBalance(values.bidPrice);
      await checkBalance(authStore, product, values.bidPrice);

      const fireStoreBid = await firestore.bids().add({
        productId: product.id,
        status: false,
        createdAt: moment().format(),
      });

      const registerBidResult = await bidStore.registerBid({
        firestoreOrderId: fireStoreBid.id,
        productId: product.id,
      });

      if (!registerBidResult.success) {
        throw registerBidResult.data;
      }

      const bidQueueResult = await this._bidQueueListener(fireStoreBid.id);

      // eslint-disable-next-line no-throw-literal
      if (bidQueueResult.userBidOtherProduct) throw { error: 'ERROR_USER_BID_OTHER_PRODUCT', bidQueueResult };
      // eslint-disable-next-line no-throw-literal
      if (bidQueueResult.otherUserBidProduct) throw { error: 'ERROR_OTHER_USER_BID_PRODUCT', bidQueueResult };
      if (+bidQueueResult.biggestBidPrice >= +values.bidPrice) {
        // eslint-disable-next-line no-throw-literal
        throw { error: 'ERROR_BID_PRICE_MUST_BE_BIGGER', bidQueueResult };
      }

      this._productBidHistoryId = bidQueueResult.productBidHistoryId;

      const { publicAddress } = authStore.initialData;
      // const paymentAddress = process.env.EXCHANGE_CONTRACT_ADDRESS;
      const paymentAddress = CHAIN_LIST[product?.chainId]?.exchangeContractAddress;

      let approveHash = null;

      // const allowance =
      //   (await product.currency) === CURRENCIES.PRX
      //     ? await allowancePrx(publicAddress, paymentAddress)
      //     : await allowanceWeth(publicAddress, paymentAddress);
      const allowance = await getAllowanceByToken(product.currency, publicAddress, paymentAddress);

      if (allowance < +values.bidPrice) {
        // approveHash = await approveMax(publicAddress, t, product.currency);
        approveHash = await approveMaxNew(publicAddress, t, product.currency);
        const allowanceAfter = await getAllowanceByToken(product.currency, publicAddress, paymentAddress);
        if (allowanceAfter < +values.bidPrice) {
          const error = { error: 'APPROVED_NOT_ENOUGH' };
          throw error;
        }
      }

      const bidResult = await bidStore.bid({
        bidPrice: values.bidPrice,
        productBidHistoryId: bidQueueResult.productBidHistoryId,
        productId: product.id,
        approveHash,
      });

      if (!bidResult.success) {
        throw bidResult.data;
      } else {
        // const { prxToJpy, ethToJpy } = authStore.initialData;
        const masterData = authStore.initialData;
        // const yenPrice = product.currency === CURRENCIES.PRX ? +prxToJpy * +values.bidPrice : +ethToJpy * +values.bidPrice;
        const yenPrice = getRateByCurrency(CURRENCIES[product.currency], masterData) * +values.bidPrice;
        product.setData({
          yenPrice,
          price: +values.bidPrice,
        });

        if (product.productBid.expectPrice && +values.bidPrice >= product.productBid.expectPrice) {
          product.setData({
            bidFlag: false,
          });
        }

        MaskLoading.close();
        Confirmable.open({
          content: t('product_details:bid.bid_success'),
          hideCancelButton: true,
        });
      }
    } catch (e) {
      MaskLoading.close();
      // eslint-disable-next-line no-console
      console.error(e);

      if (this._productBidHistoryId) {
        bidStore.cancelBid({
          productId: product.id,
          id: this._productBidHistoryId,
        });
      }

      if (e?.code === 4001 || e?.message?.includes('MetaMask Personal Message')) {
        this.setState({
          loading: false,
        });
        return;
      }

      let errorMessage = t('product_details:bid.bid_failed');
      if (e === 'ERROR_TIMEOUT') {
        errorMessage = t('product_details:bid.time_out');
      } else if (e.error === 'ERROR_BID_PRICE_MUST_BE_BIGGER') {
        errorMessage = t('product_details:bid.bid_price_bigger', {
          x: `${Format.price(e.bidQueueResult?.biggestBidPrice) || e.message}${product?.currency}`,
        });
      } else if (e.error === 'ERROR_USER_BID_OTHER_PRODUCT') {
        errorMessage = t('product_details:bid.bid_other_product');
      } else if (e.error === 'ERROR_OTHER_USER_BID_PRODUCT') {
        errorMessage = t('product_details:bid.other_bid_product');
      } else if (e.error === 'BALANCE_NOT_ENOUGH') {
        errorMessage = t('product_details:messages.balance_not_enough');
      } else {
        errorMessage = t(`validation_messages:${e.error}`);
      }

      const ok = await Confirmable.open({
        content: errorMessage,
        hideCancelButton: true,
      });

      if (ok) {
        await productsStore.getProductDetails({ id: product?.id });
      }
    } finally {
      MaskLoading.close();
      this.close();

      this.setState({
        loading: false,
      });
    }
  };

  _validate = (values, t) => {
    const { product } = this.props;
    const { biggestBidPrice, startPrice } = product.productBid;
    let errors = {};

    if (+values.bidPrice <= 0) {
      errors = {
        bidPrice: t('validation_messages:MIN_PRICE_THAN_MORE_0'),
      };
    } else if (!isNaN(+values.bidPrice) && values.bidPrice) {
      const sBidPrice = values.bidPrice?.toString();
      let symbol = null;
      if (sBidPrice.includes('.')) {
        symbol = '.';
      } else if (sBidPrice.includes(',')) {
        symbol = ',';
      }

      if (!symbol) {
        if (sBidPrice?.length > MAX_LENGTH_INPUT_VALUE_INTEGER) {
          errors = {
            bidPrice: t('validation_messages:MAX_LENGTH_PRICE'),
          };
        }
      } else {
        const arr = sBidPrice.split(symbol);
        if (arr[0]?.length > MAX_LENGTH_INPUT_VALUE_INTEGER || arr[1]?.length > MAX_LENGTH_INPUT_VALUE_FRACTION) {
          errors = {
            bidPrice: t('validation_messages:MAX_LENGTH_PRICE'),
          };
        }
      }
    }

    if (+values.bidPrice <= (biggestBidPrice || startPrice)) {
      errors = {
        bidPrice: t('validation_messages:BID_PRICE_BIGGER', {
          x: `${biggestBidPrice || startPrice || 0}${product?.currency}`,
        }),
      };
    }

    return errors;
  };

  _renderForm = ({ handleSubmit, values, errors }, t) => {
    const { authStore, productsStore, product } = this.props;
    const { loading } = this.state;
    // const { ethToJpy, prxToJpy } = authStore.initialData;
    const masterData = authStore.initialData;
    // const currency = product?.currency === 'ETH' ? 'WETH' : product?.currency;
    const currency = product?.currency;
    const prices = getRateByCurrency(CURRENCIES[product?.currency], masterData);

    return (
      <Form noValidate>
        <Typography bold className="modal-title" center>
          {t('product_details:bid.place_a_bid')}
        </Typography>
        <div className="field-box">
          {/* <Typography className="_label" bold size="large">
            {t('common:price')}
          </Typography> */}
          {/* <div className="input-wrapper">
            <div className="price_input">
              <Typography className="_label_price" bold size="large">
                {t('common:price')}
              </Typography>
              <div className="input-unit-box">
                <div className="nbng-icon">
                  <img
                    src={Images[product?.currency]}
                    alt="icon"
                  />
                </div>
                <Typography poppins bold size="huge">
                  {product?.currency}
                </Typography>
              </div>
            </div>
            <Field className="input-box bold" type="number" name="bidPrice" maxLength={9} simple component={Input} />
          </div> */}
          <ProductInputPrice
            selectedCurrency={currency}
            values={values}
            errors={errors}
            productsStore={productsStore}
            authStore={authStore}
            inputName="bidPrice"
          />
          {prices && (
            <>
              <Typography className="jpy-rate" size="small" poppins>
                {t('product_details:bid.exchange_text', { currency, x: Format.price(prices) })}
              </Typography>
              {/* <Typography className="jpy-price" size="small" poppins bold>
                {Format.price(+values.bidPrice * prices)}
                {t('common:yen')}
              </Typography> */}
            </>
          )}
          <Field showError={false} className="accept-terms-checkbox" name="acceptTerms" component={Checkbox}>
            {t('product_details:bid.accept')}
          </Field>
        </div>
        <div className="bottom-box">
          <Button
            size="large"
            disabled={!values.acceptTerms || +values?.bidPrice <= 0}
            onClick={handleSubmit}
            background={Colors.BLUE_4}
            loading={loading}
          >
            {t('product_details:bid.put_a_limit')}
          </Button>
          <Button size="large" className="cancel-btn" onClick={this.close}>
            {t('common:cancel')}
          </Button>
        </div>
      </Form>
    );
  };

  render() {
    const { product } = this.props;
    const { isOpen, loading } = this.state;

    return (
      <ModalStyled
        open={isOpen}
        onCancel={this.close}
        destroyOnClose
        maskClosable={!loading}
        closable={!loading}
        width={640}
        padding={0}
        className="modal-bid custom-modal"
      >
        <StyledDiv>
          <Translation>
            {t => (
              <Formik
                enableReinitialize
                validateOnChange={false}
                validateOnBlur={false}
                initialValues={{
                  bidPrice: +product.productBid.biggestBidPrice || product.productBid.startPrice || 0,
                  acceptTerms: false,
                }}
                validate={values => this._validate(values, t)}
                validationSchema={validationSchema}
                onSubmit={values => this._onSubmit(values, t)}
                component={data => this._renderForm(data, t)}
              />
            )}
          </Translation>
        </StyledDiv>
      </ModalStyled>
    );
  }
}

export default BidModal;

const ModalStyled = styled(Modal)`
  .ant-modal-content {
    padding: 20px 40px;
    .modal-title {
      margin-bottom: 24px;
    }
  }

  .field-box .accept-terms-checkbox .ant-checkbox-wrapper span {
    color: ${Colors.TEXT} !important;
  }
`;
