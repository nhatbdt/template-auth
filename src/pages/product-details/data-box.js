import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import classnames from 'classnames';
import { withTranslation } from 'react-i18next';
import { CopyToClipboard } from 'react-copy-to-clipboard';

import { Colors, Images } from '../../theme';
import Media from '../../utils/media';
import { CHAIN_LIST } from '../../constants/chains';
import moment from 'moment';

const DataBoxStyled = styled.div`
  display: flex;
  flex-direction: column;

  .item-detail-label {
    background: #15171c;
    color: #fff;
    margin: 24px 0 10px 0;
    border-radius: 8px;
    /* font-family: 'Noto Sans JP'; */
    font-style: normal;
    font-weight: 500;
    font-size: 18px;
    padding: 7px 15px;
    ${Media.lessThan(Media.SIZE.MD)} {
      display: none;
    }
    ${Media.lessThan(Media.SIZE.SM)} {
      display: block;
    }
  }

  ._label {
    font-weight: 400;
    font-size: 12px;
    color: #fff;

    span {
      color: #fff;
    }
  }

  .secondary-items {
    display: flex;
    /* justify-content: space-between; */
    /* gap: 75px; */
    margin-top: 15px;
    flex-flow: wrap;

    .data-information-item {
      width: calc(100% / 3);
    }
    ${Media.lessThan(Media.SIZE.XL)} {
      margin-top: 10px;
      gap: 18px;
      flex-direction: column;
    }
  }

  .contract-address-style {
    word-break: break-word;
    color: ${Colors.TEXT};
    font-weight: 400;
    font-size: 16px;
  }
  .tooltip {
    position: relative;
    display: inline-block;
  }
  .icon-layout {
    padding: 0px 10px;
    cursor: pointer;
  }
  .tooltip .tooltiptext {
    visibility: hidden;
    /* background-color: #ffffff;
    color: #000000; */
    background-color: ${Colors.BOX_BACKGROUND};
    color: ${Colors.TEXT};

    text-align: center;
    border-radius: 6px;
    padding: 5px;
    position: absolute;
    z-index: 1;
    bottom: 150%;
    left: 50%;
    margin-left: -75px;
    opacity: 0;
    transition: opacity 0.3s;
    /* font-family: 'Noto Sans JP'; */
    font-style: normal;
    font-weight: 400;
    font-size: 12px;
    line-height: 21px;
    white-space: nowrap;
  }

  .tooltip .tooltiptext::after {
    content: '';
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #376587 transparent transparent transparent;
  }
  .tooltip:hover .tooltiptext {
    visibility: visible;
    opacity: 1;
  }

  .data-information-item {
    display: flex;
    flex-direction: column;
    align-items: flex-start;

    &.horizontal {
      ${Media.lessThan(Media.SIZE.XL)} {
        width: 100%;
        flex-direction: row;
        flex-wrap: wrap;
        gap: 0 10px;
        justify-content: space-between;
      }
    }

    .data-information-item-content {
      color: ${Colors.TEXT};
      font-weight: 400;
      font-size: 16px;
    }
  }

  .modal-container {
    background: red;
  }

  .data-section {
    border-bottom: 2px solid rgb(255 255 255 / 8%);
    padding-bottom: 29px;
    margin-bottom: 28px;
    margin-top: 15px;

    ${Media.lessThan(Media.SIZE.MD)} {
      margin: 0;
      border-bottom: 0;
      padding-bottom: 0;
    }

    &:last-child {
      margin-bottom: 0;
      border-bottom: none;
      ${Media.lessThan(Media.SIZE.MD)} {
        border-bottom: 1px solid rgb(255 255 255 / 8%);
      }
    }

    &.only-mobile {
      display: none;
      ${Media.lessThan(Media.SIZE.MD)} {
        display: block;
      }
    }

    .data-section-body {
      /* padding: 5px 0 20px; */
      ${Media.lessThan(Media.SIZE.MD)} {
        /* padding: 5px 20px 20px; */
        margin-top: 16px;
        border-bottom: none;

        &.mobileShow {
          display: block;
        }
      }
      ._description {
        font-size: 16px;
        color: ${Colors.TEXT};
        white-space: pre-wrap;
        word-break: break-word;
        opacity: 0.7;
        ${Media.lessThan(Media.SIZE.MD)} {
          font-size: 14px;
        }
      }
    }
  }
`;
@withTranslation('settings')
@inject(stores => ({
  product: stores.products.currentProductDetails,
}))
@observer
class DataBox extends Component {
  static propTypes = {
    product: PropTypes.object,
  };

  state = {
    mobileExpandSection: null,
  };

  onCopyText = (data) => {
    if (navigator.clipboard && window.isSecureContext) {
      navigator.clipboard.writeText(data?.contractAddress);
    }
  };

  render() {
    const { product, t, className, nftToken } = this.props;
    const nftCategory = nftToken?.find(item => item.value === product?.nftCategory);
    const nftType = nftCategory?.type?.find(item => item.value === product?.nftType);
    const nftSubType = nftType?.subType?.find(item => item.value === product?.nftSubType);
    const { mobileExpandSection } = this.state;

    return (
      <DataBoxStyled className={className}>
        <div className="data-section">
          <div className="data-information">
            <div className="item-detail-label">{t('common:detail')}</div>
            <div className="data-information-item">
              <div className="_label">
                <span>{t('contract_address')}</span>
              </div>
              <div>
                <CopyToClipboard text={product?.contractAddress}>
                  <span className="contract-address-style">
                    {product?.contractAddress}
                    <div className="tooltip">
                      <span className="tooltiptext">{t('common:copy_to_clipboard')}</span>
                      <img
                        onClick={() => this.onCopyText(product)}
                        className="icon-layout"
                        src={Images.WHITE_COPPY_ICON}
                        alt="Copied to Clipboard"
                      />
                    </div>
                  </span>
                </CopyToClipboard>
              </div>
            </div>
            <div className="secondary-items">
              <div className="data-information-item horizontal">
                <div className="_label">{t('token_id')}</div>
                <div className="data-information-item-content">
                  <span>{product?.tokenId}</span>
                </div>
              </div>
              <div className="data-information-item horizontal">
                <div className="_label">{t('token_standard')}</div>
                <div className="data-information-item-content">
                  <span>{product?.tokenStandard}</span>
                </div>
              </div>
              <div className="data-information-item horizontal">
                <div className="_label">{t('chain')}</div>
                <div className="data-information-item-content">
                  <span>{CHAIN_LIST[product?.chainId]?.displayName}</span>
                </div>
              </div>
            </div>
            <div className="secondary-items">
              <div className="data-information-item horizontal">
                <div className="_label">{t('nft_category')}</div>
                <div className="data-information-item-content">
                  <span>{nftCategory ? nftCategory.label : '--'}</span>
                </div>
              </div>
              <div className="data-information-item horizontal">
                <div className="_label">{t('nft_type')}</div>
                <div className="data-information-item-content">
                  <span>{nftType ? nftType.label : '--'}</span>
                </div>
              </div>
              <div className="data-information-item horizontal">
                <div className="_label">{t('nft_sub_type')}</div>
                <div className="data-information-item-content">
                  <span>{nftSubType ? nftSubType.label : '--'}</span>
                </div>
              </div>
            </div>
          </div>

          {product?.productAttributes?.length ? (
            <div className="data-information">
              <div className="item-detail-label">{t('product_attributes')}</div>

              <div className="secondary-items">
                {product?.productAttributes?.map((item, index) => {
                  if (!item?.attribute) {
                    return null;
                  }
                  let domItem = (
                    <React.Fragment key={index}>
                      <div
                        style={{
                          marginTop: index > 2 ? '5px' : '0',
                        }}
                        className="data-information-item horizontal"
                      >
                        <div className="_label">{t(`attribute_${item?.attribute}`)}</div>
                        <div className="data-information-item-content">
                          {item?.attribute === 3 || item?.attribute === 4 ? (
                            <span>{item?.value ? moment(Number(item?.value)).format('YYYY/MM/DD HH:mm:ss') : ''}</span>
                          ) : item?.attribute === 1 ? (
                            <span>{item?.value ? t(`attributes_level.label_${item?.value}`) : ''}</span>
                          ) : item?.attribute === 5 ? (
                            <span>{item?.value ? t(`${item.value.toLowerCase()}`) : ''}</span>
                          ) : (
                            <span>{item?.value || ''}</span>
                          )}
                        </div>
                      </div>
                    </React.Fragment>
                  );

                  return domItem;
                })}
              </div>
            </div>
          ) : (
            ''
          )}

          <div className="item-detail-label">{t('common:description')}</div>
          <div
            className={classnames('data-section-body', {
              mobileShow: mobileExpandSection === 'basic',
            })}
          >
            <div className="_description">{product?.description}</div>
          </div>
        </div>
      </DataBoxStyled>
    );
  }
}

export default DataBox;
