import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation, useTranslation } from 'react-i18next';

import Clickable from '../../components/clickable';
import Typography from '../../components/typography';
import Media from '../../utils/media';

const CategoriesStyled = styled.div`
  justify-content: flex-start;
  display: flex;
  margin: 0;
  width: 100%;
  flex-wrap: wrap;
  margin-top: 20px;
  .category-item {
    border: 1px solid rgba(255, 255, 255, 0.1);
    border-radius: 23px;
    margin-right: 10px;
    margin-bottom: 10px;
    &:last-child {
      margin-right: 0;
    }

    p {
      color: #fff;
      font-size: 14px;
      padding: 5px 20px;
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .category-item {
      background: #2b2c3d;
      p {
        font-size: 12px;
        padding: 5px 15px;
      }
    }
  }
`;

@withTranslation()
@inject(stores => ({
  productsStore: stores.products,
}))
@observer
class Categories extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
  };

  componentDidMount() {
    const { productsStore, i18n } = this.props;

    productsStore.getProductCategories({
      langKey: i18n.language.toUpperCase(),
    });
  }

  render() {
    const { productsStore, history } = this.props;
    return (
      <CategoriesStyled>
        {productsStore.categories.map(item => (
          <div key={item.id} className="category-item">
            <Clickable className="category-box" onClick={() => history.push(`/products/none/${item.name}/none/none`)}>
              <Typography size="large">{item.name}</Typography>
            </Clickable>
          </div>
        ))}
      </CategoriesStyled>
    );
  }
}

const CategoriesWraped = props => {
  const { i18n } = useTranslation();

  return <Categories {...props} key={i18n.language} />;
};

export default CategoriesWraped;
