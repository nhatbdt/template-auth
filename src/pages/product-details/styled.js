import styled from 'styled-components';
import { Colors } from '../../theme';
import Media from '../../utils/media';

const StyledDiv = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  ._left {
    display: flex;
    flex-direction: row;
    justify-content: center;
    flex: 2;

    .line {
      width: 1px;
      height: auto;
      background-color: #fff;
      opacity: 0.08;
    }

    .user-info-box {
      flex: 1;

      .user-info {
        display: flex;
        margin-top: 10px;
        align-items: center;

        .user-avatar {
          width: 40px;
          height: 40px;
        }

        .user-name {
          /* color: #3CC8FC; */
          color: #282828;
          padding-left: 10px;
          font-size: 14px;
          flex: 1;
        }
      }
    }

    ._price-box {
      display: flex;
      flex-direction: column;
      flex: 1;
      padding-left: 30px;

      ._label {
        // margin-right: 15px;
        // margin-top: 15px;
      }

      ._yen-price {
        font-size: 15px;
        margin-left: 4px;
        opacity: 0.5;
      }

      ._timer {
        b {
          font-size: 34px;
          font-weight: 500;
          margin: 0 4px;
        }
      }
    }

    .transaction-text {
      margin-top: 5px;
      display: flex;
      color: rgba(255, 255, 255, 0.35);

      .transaction-link {
        margin-left: 15px;
        color: ${Colors.PRIMARY};
      }
    }
  }

  ._right {
    display: flex;
    flex-direction: column;
    align-items: center;
    flex: 1;

    .holding-caution {
      color: ${Colors.PRIMARY};
      margin-top: 10px;
    }
  }

  .checkout-button {
    /* width: 150px; */
    width: 100%;
    min-width: 200px;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    height: 50px;
    color: #fff;

    ._small {
      font-size: 12px;
    }
  }

  .payment-button {
    /* width: 185px; */
    flex: 1;
    min-width: 200px;
    color: #fff;
    padding: 0;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    /* background-color: #376587 !important; */
    /* border: 1px solid #376587; */
    border-radius: 4px;
    color: #fdfefe;
    height: 50px;
    /* right: 2vh; */

    ._small {
      font-size: 12px;
    }
  }
  /* .payment-button:hover {
    color:black;
  } */

  .checkout-parent {
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;

    .remain {
      flex: 1;

      p {
        text-align: center;
      }
    }

    .bid-button {
      margin-top: 10px;
      width: 230px;
      padding: 0;
      flex: 2;
    }
  }

  .audio-only-mobile {
    display: none;
  }

  ${Media.lessThan(Media.SIZE.XL)} {
    flex-direction: column;
    align-items: unset;
    justify-content: unset;

    ._left {
      margin-bottom: 14px;

      ._price-box {
        justify-content: space-between;

        ._label {
          margin-right: 0;
          font-size: 12px;
        }

        ._inner {
          display: flex;
          flex-direction: column;
          align-items: flex-end;
        }

        ._yen-price {
          font-size: 12px;
          margin-top: -6px;
        }

        ._timer {
          b {
            font-size: 29px;
          }
        }
      }
    }

    /* .checkout-button {
      width: 100%;
    } */

    .bid-button {
      width: 100%;
    }

    .audio-only-mobile {
      display: flex;
      align-items: center;

      .controls {
        padding-top: 0;
        padding-left: 5px;
        width: 35px;

        .wave-container {
          display: none;
        }

        img {
          margin-top: -3px;
        }
      }

      .time {
        opacity: 0.5;
      }
    }
  }

  ${Media.lessThan(Media.SIZE.SM)} {
    ._left ._price-box {
      ._timer b {
        font-size: 18px;
      }

      ._yen-price {
        font-size: 11px;
      }
    }
  }

  ${Media.lessThan(Media.SIZE.XXXS)} {
    ._left ._price-box ._timer b {
      font-size: 16px;
    }
  }

  ${Media.lessThan(Media.SIZE.XXL)} {
    /* .btn-checkout{
      margin-right: -20px !important;
    } */
    ._left {
      margin-right: 25px;
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    /* .btn-checkout{
      display: contents !important;
    } */
    .payment-button {
      bottom: 8px;
      width: 100%;
      left: 0px;
    }
  }

  .btn-checkout {
    /* display: -webkit-flex; */
    /* padding: 5px 0 5px 15px; */
    margin-top: 16px;
    width: 100%;
    display: grid;
    grid-template-columns: auto auto;
    gap: 16px;
    button {
      font-size: 14px;
      height: 42px;
      background-color: #045afc;
      border-radius: 9999px;
    }

    &.now-on-resale {
      grid-template-columns: auto;
    }
  }
`;

export default StyledDiv;
