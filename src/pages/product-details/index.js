import { Component } from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import { withTranslation, useTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';
import { ArrowLeftOutlined } from '@ant-design/icons';

import Storage from '../../utils/storage';
import { login } from '../../utils/conditions';
import Page from '../../components/page';
import Container from '../../components/container';
import Loading from '../../components/loading';
import TabBar from '../../components/tab-bar';
import Button from '../../components/button';
import AvatarBox from './avatar-box';
import NameBox from './name-box';
import DataBox from './data-box';
import CheckoutBox from './checkout-box';
import BidBox from './bid-box';
import SellBox from './sell-box';
import TransactionHistoriesBox from './transaction-histories-box';
import AuctionHistory from './bid-box/bid-histories-box';
import OffersBox from './offer-box';
import OfferNotificationBar from './offer-box/offer-notification-bar';
import OfferModal from './offer-box/offer-modal';
import TopBar from './top-bar';
import ListingBox from './listings-box';
import Offer1155Box from './offer-1155-box';
import Offer1155Modal from './offer-1155-box/offer-modal';
import LendingDetails from './sell-box/lending-detail';
import BorrowDetails from './sell-box/borrow-detail';

import { StyledDiv } from './styled-index';
// import Image3DBox from './image-3d-box';
import { Divider } from 'antd';
import ChartBox from './chart-box';
import { useWeb3Auth } from '../../contexts/web3auth/web3auth';
import { LENDING_STATUS } from '../../constants/statuses';
import { checkProcessing } from './utils/func';

@withTranslation(['product_details', 'validation_messages'])
@withRouter
@inject(stores => ({
  productsStore: stores.products,
  lendingStore: stores.lending,
  authStore: stores.auth,
  product: stores.products.currentProductDetails,
  offersStore: stores.offers,
  stakingStore: stores.staking,
}))
@observer
class ProductDetails extends Component {
  static propTypes = {
    authStore: PropTypes.object,
    productsStore: PropTypes.object,
    lendingStore: PropTypes.object,
    product: PropTypes.object,
    offersStore: PropTypes.object,
    stakingStore: PropTypes.object,
  };

  constructor(props) {
    super(props);

    this.state = {
      initing: true,
      isLending: false,
      currentTab: 'offer',
      nftToken: []
    };
  }

  async componentDidMount() {
    const { productsStore, match, history, i18n, offersStore, stakingStore } = this.props;
    const { sellAction, id } = match.params;

    const { data, success } = await productsStore.getProductDetails({
      id: id,
      langKey: i18n.language.toUpperCase(),
    });

    const resNft = await stakingStore.getListNftCategory({ langKey: i18n.language.toUpperCase() });

    const isLending = [
      LENDING_STATUS.WAIT_LENDING,
      LENDING_STATUS.IN_LENDING,
      LENDING_STATUS.LENDING,
      LENDING_STATUS.WAIT_BORROW,
      LENDING_STATUS.BORROW,
      LENDING_STATUS.WAIT_CANCEL,
      LENDING_STATUS.EXPIRATION,
      LENDING_STATUS.WAIT_WITHDRAW,
    ].includes(data?.lendingDetailDTO?.status);

    if (success) {
      if (
        sellAction === 'create-lending' &&
        [
          LENDING_STATUS.WAIT_LENDING,
          LENDING_STATUS.IN_LENDING,
          LENDING_STATUS.LENDING,
          LENDING_STATUS.WAIT_BORROW,
          LENDING_STATUS.BORROW,
          LENDING_STATUS.WAIT_CANCEL,
          LENDING_STATUS.EXPIRATION,
          LENDING_STATUS.WAIT_WITHDRAW,
        ].includes(data?.lendingDetailDTO?.status)
      )
        history.push(`/product-details/${id}`);

      if (!data || (data.status === 'SALE' && data?.parentProductId && !data?.productResell?.reselling)) {
        history.replace('/');

        return;
      }

      data?.tokenStandard === 'ERC721'
        ? await offersStore.getOffers({
          page: 1,
          productId: id,
        })
        : await offersStore.getOffers1155({
          page: 1,
          productId: id,
          limit: 10,
        });
    }

    this.setState({
      initing: false,
      isLending,
      currentTab: data?.productBid && data?.typeSale === 'LIMITED' && data?.status === 'NEW' ? 'auction' : 'offer',
      nftToken: resNft?.data
    });
  }

  _onTabChange = key => {
    this.setState({
      currentTab: key,
    });
  };

  _onClickOffer = () => {
    const { product } = this.props;
    if (product?.tokenStandard === 'ERC721') {
      this._bigModal.open();
    } else {
      this._big1155Modal.open();
    }
  };

  _renderResellBox = () => {
    const { product } = this.props;
    const isErc721 = product?.tokenStandard === 'ERC721';
    const userId = Storage.get('USER_ID');

    if (isErc721 && (userId !== product?.userId || !product.productResell)) return null;
    if (!isErc721 && (!product?.holdingQuantity || product?.holdingQuantity <= 0)) return;
    return <SellBox />;
  };

  _renderContent = () => {
    const { history, product, t, offersStore, signMessage } = this.props;
    const { currentTab, isLending } = this.state;
    const isAuctionEnded = product?.productBid?.status !== 'NEW';
    const isErc721 = product?.tokenStandard === 'ERC721';
    const isErc1155 = product?.tokenStandard === 'ERC1155';
    const isBuying = checkProcessing(product);

    // const allow3DFile = /(\.glb|\.gltf)$/i;

    // const isImage3D = product?.metaMaskImageUrl ? allow3DFile.exec(product?.metaMaskImageUrl) : null;

    const TAB_ITEMS = [
      // {
      //   name: t("item_detail"),
      //   key: "information",
      // },
      {
        name: t('offer.offer'),
        key: 'offer',
        value: isErc721 ? offersStore?.countOffer : offersStore?.countOffer1155,
      },
      {
        name: t('auction'),
        key: 'auction',
      },
      {
        name: t('transaction_history'),
        key: 'transaction-histories',
      },
    ];

    const TAB_ITEMS_1155 = [
      // {
      //   name: t("item_detail"),
      //   key: "information",
      // },
      {
        name: t('offer.offer'),
        key: 'offer',
        value: isErc721 ? offersStore?.countOffer : offersStore?.countOffer1155,
      },
      {
        name: t('auction'),
        key: 'auction',
      },
      {
        name: t('common:filter.resell'),
        key: 'listing',
        isErc1155: true,
      },
      {
        name: t('transaction_history'),
        key: 'transaction-histories',
      },
    ];

    const pathname = this.props.location?.pathname
    const isShowButtonBack = pathname.split('/').includes('webview')
    return (
      <div className="wrapper">
        {!!isShowButtonBack && (
          <div className='btn-back' onClick={() => history?.goBack()}>
            <ArrowLeftOutlined />
          </div>
        )}
        <section className="top-section">
          {/* {isImage3D ? <Image3DBox metaMaskImageUrl={product?.metaMaskImageUrl} /> : <AvatarBox />} */}
          <AvatarBox />
          <DataBox className="data-box" nftToken={this.state.nftToken} history={this.props.history} />
        </section>
        <Container className={product?.productBid && 'content-section-bid'}>
          <div className="content-section">
            <div className="_side">
              <TopBar />
              <NameBox isLending={isLending} />
              {isErc721 && !isLending && <OfferNotificationBar />}
              {isLending && product?.lendingDetailDTO?.status === LENDING_STATUS.IN_LENDING ? (
                <>
                  <LendingDetails product={product} />
                  {/* {this._renderResellBox()} */}
                </>
              ) : isLending &&
                [LENDING_STATUS.LENDING, LENDING_STATUS.EXPIRATION].includes(product?.lendingDetailDTO?.status) ? (
                !isBuying && (
                  <BorrowDetails
                    product={product}
                    isCancelPage={product?.owned}
                    isExpiration={product?.lendingDetailDTO?.status === LENDING_STATUS.EXPIRATION}
                  />
                )
              ) : (
                <>
                  <ChartBox />

                  <div className="bid-mobile">{product?.productBid && !isAuctionEnded && <BidBox />}</div>
                  {product?.isAllowOffer &&
                    product?.canOfferFlag &&
                    product?.offerNftTransactionStatus === 'SUCCESS' &&
                    !product.owned &&
                    !isBuying &&
                    (isErc721 || (isErc1155 && product?.holdingQuantity < product?.totalQuantity)) && (
                      <Button
                        disabled={product?.productBuying}
                        className="offer-button"
                        background="white"
                        onClick={login(this._onClickOffer)}
                      >
                        {t('offer.put_a_limit_price')}
                      </Button>
                    )}

                  {!isBuying && this._renderResellBox()}

                  <div className="checkout-container">
                    <div
                      className={`checkout-inside ${product?.productBid && !isAuctionEnded && 'checkout-inside-bid'}`}
                    >
                      {product?.productBid && !isAuctionEnded ? (
                        <div className="bid-pc">{<BidBox />}</div>
                      ) : (
                        <CheckoutBox />
                      )}
                    </div>
                  </div>
                  <Divider className="line" />

                  <TabBar
                    items={isErc721 ? TAB_ITEMS : TAB_ITEMS_1155}
                    activeTabKey={currentTab}
                    onChange={this._onTabChange}
                    className="tab-container tab-yellow"
                  />
                  {currentTab === 'offer' && (
                    <>
                      {/* {isErc721 && !isLending && <OffersBox />}
                      {isErc1155 && !isLending && <Offer1155Box />} */}
                      {(!product?.productBid || product?.productBid?.status === 'FINISHED') &&
                        product?.isAllowOffer &&
                        product.canOfferFlag && (
                          <>
                            {isErc721 && <OffersBox />}
                            {isErc1155 && <Offer1155Box />}
                          </>
                        )}
                    </>
                  )}
                  {currentTab === 'auction' && (
                    <>
                      {product?.productBid && !product?.isApproveOffer && (
                        <div style={{ paddingBottom: 40 }}>
                          <AuctionHistory />
                        </div>
                      )}
                    </>
                  )}
                  {currentTab === 'listing' && (
                    <div style={{ paddingBottom: 40 }}>
                      <ListingBox />
                    </div>
                  )}
                  {currentTab === 'transaction-histories' && <TransactionHistoriesBox />}
                </>
              )}
            </div>
            <DataBox className="data-box" nftToken={this.state.nftToken} history={this.props.history} />
          </div>
        </Container>
        {isErc721 && (
          <OfferModal
            t={t}
            history={this.props.history}
            signMessage={signMessage}
            ref={ref => {
              this._bigModal = ref;
            }}
          />
        )}
        {isErc1155 && (
          <Offer1155Modal
            ref={ref => {
              this._big1155Modal = ref;
            }}
          />
        )}
      </div>
    );
  };

  render() {
    const { initing } = this.state;

    return (
      <Page>
        <StyledDiv>
          <div className="product-details-container">{initing ? <Loading /> : this._renderContent()}</div>
        </StyledDiv>
      </Page>
    );
  }
}

export default inject(stores => ({
  authStore: stores.auth,
}))(
  observer(({ authStore, ...props }) => {
    const { i18n } = useTranslation();
    const { signMessage } = useWeb3Auth();

    return <ProductDetails {...props} key={`${i18n.language}-${authStore.loggedIn}`} signMessage={signMessage} />;
  }),
);
