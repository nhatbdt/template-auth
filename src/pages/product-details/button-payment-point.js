import { Component } from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';

import { checkProcessing } from './utils/func'
import Button from '../../components/button';

@withTranslation(['product_details', 'common'])
@inject(stores => ({
  product: stores.products.currentProductDetails,
}))
@observer
class ButtonPaymentPoint extends Component {
  static propTypes = {
    product: PropTypes.object,
    isCheckoutButtonActive: PropTypes.bool,
    onPayment: PropTypes.func,
  };

  render() {
    const { product, onPayment, isCheckoutButtonActive, t } = this.props;
    const maxAmountJpyCanBuy = 99999999;
    const isBuying = checkProcessing(product)
    const isErc721 = product?.tokenStandard === 'ERC721';
    const countRemain = product?.remainQuantity;
    const isErc1155 = product?.tokenStandard === 'ERC1155';
    const isReselling = product?.productResell?.reselling;
    const isValid = product?.status === 'SALE' && product.displayStatus === 'PUBLIC';
    const isCanBuy = product?.totalCreditChargePrice <= maxAmountJpyCanBuy;

    return (
      <>
        {product?.status !== 'SOLD' && (
          <>
            {isErc721 && isCheckoutButtonActive && !isBuying && !isReselling && !product?.owned && isCanBuy && (
              <Button size="large" className="payment-button" onClick={onPayment}>
                {t('payment_point')}
              </Button>
            )}
            {isErc1155 && countRemain > 0 && !isBuying && !isReselling && isValid && isCanBuy && (
              <Button size="large" className="payment-button" onClick={onPayment}>
                {t('payment_point')}
              </Button>
            )}
          </>
        )}
      </>
    );
  }
}

export default ButtonPaymentPoint;
