import styled from 'styled-components';
import { Colors } from '../../../theme';
import Media from '../../../utils/media';

const StyledDiv = styled.div`
  padding-top: 0px;
  display: flex;
  flex-direction: column;

  .title {
    color: #333333;
    font-size: 13px;
  }
  .user-name {
    /* color: #FDFEFE !important; */
    color: #282828;
  }

  table {
    width: 100%;
    margin-top: 28px;
    table-layout: fixed;

    thead {
      tr {
        th {
          font-size: 12px;
          font-weight: normal;
          /* color: #b7b7b7; */
          color: #282828;

          &:nth-child(1) {
            width: 20%;
          }

          &:nth-child(2) {
            width: 7%;
          }

          &:nth-child(3) {
            width: 10%;
          }

          &:nth-child(4) {
            width: 10%;
          }

          &:nth-child(5) {
            width: 10%;
          }

          &:nth-child(6) {
            width: 15%;
          }

          &:nth-child(7) {
            width: 15%;
            padding-left: 15px;
          }
        }
      }
    }

    tbody {
      tr {
        height: 53px;
        border-bottom: 1px solid rgb(255 255 255 / 8%);

        td {
          &:nth-child(4),
          &:nth-child(5) {
            /* color: #FDFEFE; */
            color: #282828;
          }
          &:nth-child(7) {
            padding-left: 15px;
          }

          .status-text {
            &.success {
              color: #03ad0a;
            }
          }

          .action-button {
            white-space: nowrap;
          }

          .horizon {
            display: flex;
            align-items: center;

            .new-dot {
              width: 6px;
              height: 6px;
              border-radius: 3px;
              margin-right: 10px;

              &.show {
                background-color: #3cc8fc;
              }
            }

            .avatar {
              margin-right: 9px;
              min-width: 0;
            }

            .user-name {
              font-size: 13px;
              color: ${Colors.RED_2};
              overflow: hidden;
              text-overflow: ellipsis;
              max-width: 180px;
              white-space: nowrap;
              flex: 1;
              margin-right: 5px;
            }

            .fire-icon {
              margin-right: 23px;
            }

            .heat-number {
              color: ${Colors.PRIMARY};
            }
          }

          .date {
            /* color: #FDFEFE */
            color: #282828;
          }

          .cancel-button {
            height: 32px;
            font-size: 11px;
            padding: 0 15px;
          }
        }
      }
    }
  }

  .no-data-box {
    height: 100px;
    display: flex;
    align-items: center;
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    padding-top: 0;
    margin-bottom: 30px;

    .table-outer {
      overflow-x: auto;
    }

    table {
      margin-top: 15px;
      table-layout: fixed;

      thead {
        tr {
          th {
            &:nth-child(1) {
              width: 160px;
            }

            &:nth-child(2) {
              width: 40px;
            }

            &:nth-child(3) {
              width: 100px;
            }

            &:nth-child(4) {
              width: 60px;
            }

            &:nth-child(5) {
              width: 60px;
            }

            &:nth-child(6) {
              width: 100px;
            }

            &:nth-child(7) {
              width: 100px;
            }
          }
        }
      }
      tbody {
        tr {
          height: 53px;
          border-bottom: 1px solid #f1f1f1;

          &:nth-child(1) {
          }

          td {
            &:nth-child(3) {
              text-align: center;
            }

            &:nth-child(5) {
              text-align: right;
              padding-right: 0;
            }

            .cancel-button {
              height: 32px;
              font-size: 11px;
              padding: 0 15px;
            }
          }
        }
      }
    }
  }

  .pagination {
    display: flex;
    justify-content: flex-end;
    align-items: center;
    margin: 20px 0;
  }
`;

export { StyledDiv };
