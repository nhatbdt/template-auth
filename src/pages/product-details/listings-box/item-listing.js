import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';
import moment from 'moment';

import Typography from '../../../components/typography';
import Thumbnail from '../../../components/thumbnail';
import Clickable from '../../../components/clickable';
import Confirmable from '../../../components/confirmable';
import MaskLoading from '../../../components/mask-loading';
import Toast from '../../../components/toast';
import { Images } from '../../../theme';
import { login } from '../../../utils/conditions';
import Misc from '../../../utils/misc';
import { firestore } from '../../../services/firebase';
import { allowancePrx } from '../../../utils/web3';

import CURRENCIES from '../../../constants/currencies';
import PRICES from '../../../constants/prices';

import { onSign, onTransfer, onCheckApproveMax, onError, onCheckNetwork, onCheckBalance } from '../utils/checkout';

@withTranslation('product_details')
@inject(stores => ({
  authStore: stores.auth,
  offersStore: stores.offers,
  sellStore: stores.sell,
  product: stores.products.currentProductDetails,
  paymentStore: stores.payment,
}))
@observer
class ListingBox extends PureComponent {
  static propTypes = {
    authStore: PropTypes.object,
    offersStore: PropTypes.object,
    sellStore: PropTypes.object,
    product: PropTypes.object,
    paymentStore: PropTypes.object,
    onCancelSuccess: PropTypes.func,
    onSell1155Success: PropTypes.func,
  };

  _transfer = async (productSeller, data) => {
    const { authStore, t, product } = this.props;

    if (Misc.isMobile) {
      await Confirmable.open({
        content: t('product_details:you_signed'),
        hideCancelButton: true,
        acceptButtonText: t('common:ok'),
      });
    }

    const { publicAddress } = authStore.initialData;

    const transactionId = await onTransfer(
      publicAddress,
      t,
      data,
      PRICES.GAS_LIMIT,
      productSeller.currency,
      productSeller.resellPrice,
    );

    firestore.logs().add({
      transactionId,
      productId: product.id,
      createdAt: moment().format('YYYY/MM/DD HH:mm'),
      currency: productSeller?.currency,
      from: publicAddress,
      to: authStore.initialData.publicAddress,
      reselling: true,
      price: productSeller?.resellPrice,
    });

    return transactionId;
  };

  _orderListener = documentId =>
    new Promise((resolve, reject) => {
      const timeOut = setTimeout(() => {
        this._unsubscribe();
        // eslint-disable-next-line prefer-promise-reject-errors
        reject('ERROR_TIMEOUT');
      }, 180000);

      this._unsubscribe = firestore
        .orders()
        .doc(documentId)
        .onSnapshot(
          snapshot => {
            const data = snapshot.data();

            if (data?.status) {
              resolve(data);

              this._unsubscribe();
              clearTimeout(timeOut);
            }
          },
          e => reject(e),
        );
    });

  _purchase = async (signature, productSeller) => {
    const { paymentStore, authStore, t, product, onSell1155Success } = this.props;

    const purchaseResult = await paymentStore.purchareProduct1155({
      signature,
      productId: product.id,
      value: productSeller.sellerQuantity,
      typeSale: 'RESALE_NORMAL',
      productResellId: productSeller.id,
    });

    if (!purchaseResult.success) {
      throw purchaseResult.data;
    }

    this._orderId = purchaseResult.data.orderId;

    const fireStoreOrder = await firestore.orders().add({
      productId: product?.id,
      status: false,
      createdAt: moment().format(),
    });

    const checkAllowResult = await paymentStore.checkAllowPurchase({
      productId: product?.id,
      firestoreOrderId: fireStoreOrder.id,
    });

    if (!checkAllowResult.success) {
      throw checkAllowResult.data;
    }

    const orderResult = await this._orderListener(fireStoreOrder.id);

    if (!orderResult.userCanBuy) {
      throw new Error('CANNOT_CHECKOUT');
    }

    MaskLoading.open({
      message: (
        <>
          {t('messages.caution_1')}
          <br />
          {t('messages.caution_2')}
        </>
      ),
    });

    const data = purchaseResult.data.smartContractVerifySignature;
    const transactionId = await this._transfer(productSeller, data);
    const confirmResult = await paymentStore.confirmPurchaseProduct({
      ...purchaseResult.data,
      contractTransactionId: transactionId,
    });

    if (!confirmResult.success) throw confirmResult.data;
    else {
      if (productSeller.currency === CURRENCIES.ETH) {
        authStore.setBalance(authStore.initialData.nativeBalance - productSeller.resellPrice);
      }

      MaskLoading.close();

      Confirmable.open({
        content: (
          <>
            {t('messages.success_1')}
            <br />
            {t('messages.success_2')}
          </>
        ),
        hideCancelButton: true,
      });

      product.setData({
        productBuying: true,
      });

      onSell1155Success();
    }
  };

  _onValidate = async productSell => {
    const { authStore } = this.props;
    const productFormat = {
      ...productSell,
      price: productSell.resellPrice,
    };
    await onCheckNetwork();
    await onCheckBalance(productFormat, authStore);
  };

  _onSell = async productSell => {
    const { t, paymentStore, authStore } = this.props;

    MaskLoading.open({
      message: (
        <>
          {t('messages.waiting_1')}
          <br />
          {t('messages.waiting_2')}
        </>
      ),
    });

    this.isCancelUpdate = false;

    try {
      if (productSell.currency === CURRENCIES.ETH) {
        const signature = await onSign(authStore);
        await this._onValidate(productSell);
        await this._purchase(signature, productSell);
      } else {
        const { publicAddress } = authStore.initialData;
        const paymentAddress = process.env.EXCHANGE_CONTRACT_ADDRESS;

        const allowance = await allowancePrx(publicAddress, paymentAddress);
        if (allowance > productSell.resellPrice) {
          const signature = await onSign(authStore);
          await this._onValidate(productSell);
          await this._purchase(signature, productSell);
        } else {
          const isApproveMax = await onCheckApproveMax(t, authStore, productSell);
          if (isApproveMax) {
            const signature = await onSign(authStore);
            await this._onValidate(productSell);
            await this._purchase(signature, productSell);
          }
        }
      }
    } catch (e) {
      if (e?.message?.includes('User denied transaction signature')) {
        this.isCancelUpdate = true;
      }
      onError(e, t);

      // eslint-disable-next-line no-console
      console.error(e);
    }

    if (this.isCancelUpdate) {
      paymentStore.cancelPurchaseProductNormal({
        productId: productSell.id,
        orderId: this._orderId,
      });
    }

    MaskLoading.close();
  };

  _onCancelResell = async () => {
    const { t, sellStore, authStore, onCancelSuccess, product } = this.props;

    const ok = await Confirmable.open({
      content: t('sell.are_you_sure_to_cancel_listing'),
      cancelButtonText: t('common:close_modal'),
      acceptButtonText: t('common:ok'),
    });

    if (!ok) return null;

    try {
      const signature = await onSign(authStore);

      const { success } = await sellStore.cancelSell1155({
        signature,
        productId: product.id,
      });

      if (success) {
        Confirmable.open({
          content: t('sell.cancel_sell_success'),
          hideCancelButton: true,
        });

        product.setData({
          canResellERC1155: true,
        });

        onCancelSuccess();
      }
    } catch (e) {
      Toast.error(t('sell.cancel_sell_failed'));
    }
  };

  render() {
    const { t, productSell, authStore, index } = this.props;

    return (
      <tr key={`${productSell.sellerId}_${index}`}>
        <td>
          <div className="horizon">
            <Thumbnail
              size={30}
              rounded
              className="avatar"
              placeholderUrl={Images.USER_PLACEHOLDER}
              url={productSell.sellerAvatar}
            />
            <Typography poppins className="user-name">
              {productSell.sellerName || productSell.sellerPublicAddress}
            </Typography>
          </div>
        </td>
        <td> {productSell.resellPrice} </td>
        <td> {productSell.currency} </td>
        <td>
          <Typography size="large" bold poppins>
            {productSell.sellerQuantity}
          </Typography>
        </td>
        <td>
          {authStore.initialData.userId !== productSell.sellerId && (
            <Clickable className="action-button" onClick={login(() => this._onSell(productSell))}>
              <Typography size="small" bold primary>
                {`${t('offer.buy')} >`}
              </Typography>
            </Clickable>
          )}
          {authStore.initialData.userId === productSell.sellerId && (
            <Clickable className="action-button" onClick={login(this._onCancelResell)}>
              <Typography size="small" bold primary>
                {t('common:cancel')}
              </Typography>
            </Clickable>
          )}
        </td>
      </tr>
    );
  }
}

export default ListingBox;
