import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';
import { Pagination } from 'antd';

import NoDataBox from '../../../components/no-data-box';
import ItemListing from './item-listing';
import { Colors } from '../../../theme';

const ListtingBoxStyled = styled.div`
  padding-top: 0px;
  display: flex;
  flex-direction: column;

  .title {
    color: #333333;
    font-size: 13px;
  }
  .user-name {
    color: #282828;
  }

  .table-outer {
    overflow-x: auto;
    overflow-y: hidden;
    width: 100%;
    table {
      width: 100%;
      min-width: 500px;
      margin-top: 20px;
      table-layout: fixed;

      thead {
        tr {
          th {
            padding: 0 8px;
            font-weight: 700;
            font-size: 14px;
            color: ${Colors.TEXT};
            text-align: left;
            &.create-at {
              width: 120px;
            }
            &.from {
              width: 120px;
            }
          }
        }
      }

      tbody {
        tr {
          height: 53px;
          border-bottom: 1px solid rgb(255 255 255 / 8%);

          td {
            padding: 0 8px;
            color: ${Colors.TEXT};
            .typography {
              color: ${Colors.TEXT};
            }
            .action-button {
              color: #fff;
              border-radius: 999px;
              background-color: #045afc;
              white-space: nowrap;
              display: flex;
              align-items: center;
              justify-content: center;
              font-weight: 500;
              &.cancel-btn {
                background-color: #fd8718ba;
              }
            }

            .horizon {
              display: flex;
              align-items: center;

              .new-dot {
                width: 6px;
                height: 6px;
                border-radius: 3px;
                margin-right: 10px;

                &.show {
                  background-color: ${Colors.BLUE};
                }
              }

              .avatar {
                margin-right: 9px;
                min-width: 0;
              }

              .user-name {
                font-size: 13px;
                color: ${Colors.BLUE};
                overflow: hidden;
                text-overflow: ellipsis;
                max-width: 180px;
                white-space: nowrap;
                flex: 1;
                margin-right: 5px;
              }

              .fire-icon {
                margin-right: 10px;
              }

              .heat-number {
                color: ${Colors.PRIMARY};
              }
            }

            .cancel-button {
              height: 32px;
              font-size: 11px;
              padding: 0 15px;
            }
          }
        }
      }
    }
    .no-data-box {
      height: 100px;
      display: flex;
      align-items: center;
    }
  }
`;

@withTranslation('product_details')
@inject(stores => ({
  product: stores.products.currentProductDetails,
  authStore: stores.auth,
  offersStore: stores.offers,
  sellStore: stores.sell,
}))
@observer
class ListingBox extends PureComponent {
  static propTypes = {
    product: PropTypes.object,
    authStore: PropTypes.object,
    offersStore: PropTypes.object,
    sellStore: PropTypes.object,
  };

  state = {
    listingsData: [],
  };

  async componentDidMount() {
    this.getSellListing();
  }

  getSellListing = async (page = 1) => {
    const { sellStore, product } = this.props;
    await sellStore.getSellLisings({
      productId: product.id,
      limit: 20,
      page,
    });
  };

  _onClickPage = page => {
    this.getSellListing(page);
  };

  _renderItem = items => {
    return items.map((item, index) => (
      <ItemListing
        productSell={item}
        index={index}
        key={index}
        onCancelSuccess={this.getSellListing}
        onSell1155Success={this.getSellListing}
      />
    ));
  };

  render() {
    const { t, sellStore } = this.props;
    const { sellListings } = sellStore;
    const { items, total } = sellListings;

    return (
      <ListtingBoxStyled id="offer-list">
        <div className="table-outer">
          <table>
            <thead>
              <tr>
                <th className="from">{t('common:from')}</th>
                <th className="amount">{t('offer.amount')}</th>
                <th className="currency">{t('common:header.currency')}</th>
                <th>{t('erc1155.quantity')}</th>
                <th className="action">&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              {items.length > 0 ? (
                this._renderItem(items)
              ) : (
                <tr>
                  <td colSpan={5}>
                    <NoDataBox className="no-data-box" />
                  </td>
                </tr>
              )}
            </tbody>
          </table>
          {items?.length < total && (
            <div className="pagination">
              <Pagination onChange={this._onClickPage} total={total} responsive pageSize={1} />
            </div>
          )}
        </div>
      </ListtingBoxStyled>
    );
  }
}

export default ListingBox;
