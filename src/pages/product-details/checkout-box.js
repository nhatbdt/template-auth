import { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';
import lodash from 'lodash';
import numeral from 'numeral';

import { getRateByCurrency } from '../../utils/rates';
import CURRENCIES from '../../constants/currencies';
import Confirmable from '../../components/confirmable';
import MaskLoading from '../../components/mask-loading';
import { login } from '../../utils/conditions';
import Storage from '../../utils/storage';
import { firestore } from '../../services/firebase';
import { checkCorrespondingNetwork, getAllowanceByToken } from '../../utils/web3';
import PRICES from '../../constants/prices';
import {
  onSign,
  onTransfer,
  // onCheckApproveMax,
  onError,
  onCheckPrice,
  // onCheckNetwork,
  onCheckBalance,
  onCheckApproveMaxNew,
  getNonceNumberBlock,
} from './utils/checkout';
import ButtonCheckout from './button-checkout-box';
// import ButtonPayment from './button-payment-box';
import ButtonPaymentSlash from './button-payment-slash-box';
import ButtonPaymentPoint from './button-payment-point';
import CheckoutBoxStyled from './styled';
import { CHAIN_LIST } from '../../constants/chains';

@withTranslation(['product_details', 'common'])
@inject(stores => ({
  paymentStore: stores.payment,
  authStore: stores.auth,
  product: stores.products.currentProductDetails,
  productsStore: stores.products,
}))
@observer
class CheckoutBox extends Component {
  static propTypes = {
    product: PropTypes.object,
    authStore: PropTypes.object,
    paymentStore: PropTypes.object,
    productStore: PropTypes.object,
  };

  constructor(props) {
    super(props);
    const { product } = props;
    const startTime = product?.startTime;
    const endTime = product?.endTime;
    const currentTime = product?.currentTime;
    this.state = {
      priceRates: null,
    };

    this._isTimeValid =
      product?.typeSale !== 'LIMITED' ||
      (moment(currentTime).diff(moment(startTime), 's') >= 0 && moment(endTime).diff(moment(currentTime), 's') >= 0);
  }

  componentDidMount = async () => {
    const { productsStore } = this.props;
    const getPriceRateResult = await productsStore.getPriceRate();
    const priceRates = lodash.reduce(
      getPriceRateResult?.data,
      (result, item) => {
        result[item?.name] = +item?.value;
        return result;
      },
      {},
    );
    this.setState({
      priceRates,
    });
  };

  componentWillUnmount() {
    if (this._unsubscribe) {
      this._unsubscribe();
    }
  }

  // _onTransactionLinkClick = transaction => {
  //   window.open(`${Configs.ETHERSCAN_DOMAIN}/tx/${transaction}`);
  // };

  _transfer = async (product, data) => {
    const { authStore, t } = this.props;

    // if (Misc.isMobile) {
    //   await Confirmable.open({
    //     content: t('product_details:you_signed'),
    //     hideCancelButton: true,
    //     acceptButtonText: t('common:ok'),
    //   });
    // }

    const publicAddress = authStore.initialData.publicAddress;
    const transactionId = await onTransfer(publicAddress, t, data, PRICES.GAS_LIMIT, product.currency, product.price);

    firestore.logs().add({
      productId: product?.id,
      createdAt: moment().format('YYYY/MM/DD HH:mm'),
      currency: product?.currency,
      from: authStore.initialData.publicAddress,
      to: product?.user?.publicAddress?.trim(),
      transactionId,
      reselling: !!product.productResell?.reselling,
      price: product?.price,
    });

    return transactionId;
  };

  _orderListener = documentId =>
    new Promise((resolve, reject) => {
      const timeOut = setTimeout(() => {
        this._unsubscribe();
        // eslint-disable-next-line prefer-promise-reject-errors
        reject('ERROR_TIMEOUT');
      }, 180000);

      this._unsubscribe = firestore
        .orders()
        .doc(documentId)
        .onSnapshot(
          snapshot => {
            const data = snapshot.data();

            if (data?.status) {
              resolve(data);

              this._unsubscribe();
              clearTimeout(timeOut);
            }
          },
          e => reject(e),
        );
    });

  _purchase = async (signature, product, nonceContract, numberBlock) => {
    // const { paymentStore, authStore, t } = this.props;
    const { paymentStore, t } = this.props;

    const tokenStandard = product?.tokenStandard;

    const purchaseResult =
      tokenStandard === 'ERC721'
        ? await paymentStore.purchareProduct({
            signature,
            nonceContract,
            numberBlock,
            productId: product?.id,
          })
        : await paymentStore.purchareProduct1155({
            signature,
            productId: product?.id,
            typeSale: 'NORMAL',
          });

    if (!purchaseResult.success) {
      throw purchaseResult.data;
    }

    if (purchaseResult.data?.holding) {
      if (product?.tokenStandard === 'ERC721') {
        product.setHolding(true);
      }

      throw new Error('PRODUCT_HOLDING');
    }

    this._orderId = purchaseResult.data?.orderId;

    const fireStoreOrder = await firestore?.orders()?.add({
      productId: product?.id,
      status: false,
      createdAt: moment().format(),
    });

    const checkAllowResult = await paymentStore.checkAllowPurchase({
      productId: product?.id,
      firestoreOrderId: fireStoreOrder.id,
    });

    if (!checkAllowResult.success) {
      throw checkAllowResult.data;
    }

    const orderResult = await this._orderListener(fireStoreOrder.id);

    if (!orderResult.userCanBuy) {
      throw new Error('CANNOT_CHECKOUT');
    }

    MaskLoading.open({
      message: (
        <>
          {t('messages.caution_1')}
          <br />
          {t('messages.caution_2')}
        </>
      ),
    });

    const data = purchaseResult.data?.smartContractVerifySignature;

    const transactionId = await this._transfer(product, data);

    const confirmResult = await paymentStore.confirmPurchaseProduct({
      ...purchaseResult.data,
      contractTransactionId: transactionId,
    });

    if (!confirmResult.success) throw confirmResult.data;
    else {
      // if (product.currency === 'ETH') {
      //   authStore.setEthBalance(authStore.initialData?.ethBalance - product.price);
      // }

      MaskLoading.close();

      Confirmable.open({
        content: (
          <>
            {t('messages.success_1')}
            <br />
            {t('messages.success_2')}
          </>
        ),
        hideCancelButton: true,
      });

      if (product?.tokenStandard === 'ERC721') {
        product.setData({
          status: 'SOLD',
        });
      }

      product.setData({
        productBuying: true,
      });
    }
  };

  _onValidate = async product => {
    const { productsStore, t, authStore } = this.props;
    await onCheckPrice(product, productsStore, t);
    // await onCheckNetwork();
    await checkCorrespondingNetwork(product?.chainId);
    await onCheckBalance(product, authStore);
  };

  _onCheckout = async () => {
    const { t, paymentStore, authStore, product } = this.props;

    MaskLoading.open({
      message: (
        <>
          {t('messages.waiting_1')}
          <br />
          {t('messages.waiting_2')}
        </>
      ),
    });

    this.isCancelUpdate = false;

    try {
      // if (product.currency === CURRENCIES.ETH) {
      //   const signature = await onSign(authStore);
      //   await this._onValidate(product);
      //   await this._purchase(signature, product);
      // } else {
      //   const { publicAddress } = authStore.initialData;
      //   const paymentAddress = Configs.EXCHANGE_CONTRACT_ADDRESS;

      //   const allowance = await allowancePrx(publicAddress, paymentAddress);

      //   if (allowance > product.price) {
      //     const signature = await onSign(authStore);
      //     await this._onValidate(product);
      //     await this._purchase(signature, product);
      //   } else {
      //     const isApproveMax = await onCheckApproveMaxNew(t, authStore, product);
      //     if (isApproveMax) {
      //       const signature = await onSign(authStore);
      //       await this._onValidate(product);
      //       await this._purchase(signature, product);
      //     }
      //   }
      // }
      const signature = await onSign(authStore);
      const { nonceContract, numberBlock } = await getNonceNumberBlock(authStore);
      await this._onValidate(product);

      const { publicAddress } = authStore.initialData;
      const paymentAddress = CHAIN_LIST[product?.chainId]?.exchangeContractAddress;

      const allowance = await getAllowanceByToken(product?.currency, publicAddress, paymentAddress);

      if (allowance < product.price) {
        await onCheckApproveMaxNew(t, authStore, product);
        const { nonceContract, numberBlock } = await getNonceNumberBlock(authStore);
        await this._purchase(signature, product, nonceContract, numberBlock);
      } else {
        await this._purchase(signature, product, nonceContract, numberBlock);
      }

    } catch (e) {
      MaskLoading.close();
      // eslint-disable-next-line no-console
      console.error(e);
      // const firestoreLogId = await firestore.logs().add({
      //   productId: product?.id,
      //   createdAt: moment().format('YYYY/MM/DD HH:mm'),
      //   isCancelUpdate: this.isCancelUpdate,
      //   paymentAddress: CHAIN_LIST[product?.chainId]?.exchangeContractAddress,
      //   error: e.error?.toString() ?? '',
      //   errorMessage: e.message?.toString() ?? '',
      // });
      // alert(firestoreLogId?.id);

      if (e?.code !== 4001 && !e?.message?.includes('MetaMask Personal Message')) {
        onError(e, t);
      } else if (this._orderId) {
        this.isCancelUpdate = true;
      }
    } finally {
      if (this.isCancelUpdate) {
        paymentStore.cancelPurchaseProductNormal({
          productId: product?.id,
          orderId: this._orderId,
        });
      }

      const { productsStore, i18n } = this.props;
      try {
        await productsStore.getProductDetails({
          id: product?.id,
          langKey: i18n.language.toUpperCase(),
        });
      } catch (error) {
        // eslint-disable-next-line no-console
        console.log(error);
      } finally {
        MaskLoading.close();
      }
    }
  };

  // _onPayment = async () => {
  //   const { paymentStore, product, i18n, t } = this.props;
  //   try {
  //     const creditCardUrl = await paymentStore.creditCardPayment({
  //       productId: product?.id,
  //       langKey: i18n.language.toUpperCase(),
  //     });

  //     if (!creditCardUrl.success) throw creditCardUrl.data;
  //     window.location.href = creditCardUrl.data.url;
  //   } catch (e) {
  //     if (e?.code != 4001) {
  //       Confirmable.open({
  //         content: t(`validation_messages:${e.error?.toUpperCase() || 'SOMETHING_WENT_WRONG'}`),
  //         hideCancelButton: true,
  //       });
  //     }
  //   }
  // };

  _onPaymentPoint = async () => {
    const { paymentStore, product, t, authStore } = this.props;
    const masterData = authStore.initialData;
    const ok = await Confirmable.open({
      content: t('messages.payment_point', {
        point: numeral(product?.yenPrice * (1 / getRateByCurrency(CURRENCIES.POINT, masterData))).format('0,0.000'),
      }),
      hideCancelButton: false,
    });
    if (ok) {
      try {
        MaskLoading.open({});
        const { success } = await paymentStore.paymentPoint({
          productId: product?.id,
        });
        if (success) {
          MaskLoading.close();
          const ok = await Confirmable.open({
            content: t('messages.point_success'),
            hideCancelButton: true,
          });
          if (ok) {
            product.setData({
              status: 'SOLD',
              productBuying: true,
            });
          }
        } else {
          MaskLoading.close();
        }
      } catch (e) {
        if (e?.code != 4001) {
          Confirmable.open({
            content: t(`validation_messages:${e.error?.toUpperCase() || 'SOMETHING_WENT_WRONG'}`),
            hideCancelButton: true,
          });
        }
        MaskLoading.close();
      }
    }
  };

  _onPaymentSlash = async () => {
    const { paymentStore, product, t } = this.props;
    try {
      const { success, data } = await paymentStore.paymentWithSlash({
        productId: product?.id,
      });

      if (!success) throw data;
      Storage.set('URL_ACTIVE', `/product-details/${product?.id}`);
      window.location.href = data.url;
    } catch (e) {
      if (e?.code != 4001) {
        Confirmable.open({
          content: t(`validation_messages:${e.error?.toUpperCase() || 'SOMETHING_WENT_WRONG'}`),
          hideCancelButton: true,
        });
      }
    }
  };

  render() {
    const { authStore, product } = this.props;
    const isCheckoutButtonActive =
      authStore.initialData.userId !== product?.userId && this._isTimeValid && product?.isCheckoutValid;
      const isErc721 = product?.tokenStandard === 'ERC721';
      const isReselling = product?.productResell?.reselling;
    // const { priceRates } = this.state;
    // const USD_TO_JPY = priceRates?.USD_TO_JPY;
    // const isErc721 = product?.tokenStandard === 'ERC721';

    return (
      <CheckoutBoxStyled>
        {/* <div className="_left">
          {isErc721 && (
            <div className="user-info-box">
              <div className="_side-box">
                <Typography size="oversmall" className="user-label secondary">
                  {t('owner')}
                </Typography>
                <div className="user-info">
                  <Thumbnail className="user-avatar" size={30} rounded placeholderUrl={Images.USER_PLACEHOLDER} />
                  <Typography className="user-name" bold>
                    @{' '}
                    {product?.ownerName
                      ? product?.ownerName
                      : product?.status === 'SALE' && product?.productResell?.reselling
                      ? 'Unknown'
                      : ['SALE', 'NEW'].includes(product?.status)
                      ? 'Admin'
                      : 'Unknown'}
                  </Typography>
                </div>
              </div>
            </div>
          )} */}

        {/* {isErc721 && <div className="line" />} */}

        {/* <div className="_price-box">
            <Typography className="_label" size="oversmall" secondary>
              {product?.status === 'SOLD' ? t('sold_price') : t('available_price')}
            </Typography>
            <div className="_inner">
              {product?.status !== 'SOLD' ? (
                <>
                  <Typography poppins className="_timer" size="large" bold>
                    <b>{Format.price(product?.price)}</b>
                    {product?.currency}
                  </Typography>
                  <Typography className="_yen-price" bold>
                    {numeral(product?.yenPrice).format('0,0')} {t('common:yen')}
                    {' / '}
                    {numeral(product?.yenPrice * (1 / USD_TO_JPY)).format('0,0.000')} {t('common:usd')}
                  </Typography>
                </>
              ) : (
                <Typography poppins className="_timer" size="large" bold>
                  <b>{numeral(product?.yenPrice).format('0,0')}</b>
                  {t('common:yen')}
                </Typography>
              )}
            </div>
          </div>
        </div> */}
        <div className={`btn-checkout ${isReselling && isErc721 && !product?.owned && 'now-on-resale'}`}>
          {/* <ButtonPayment onPayment={login(this._onPayment)} isCheckoutButtonActive={isCheckoutButtonActive} /> */}
          <ButtonPaymentSlash onPayment={login(this._onPaymentSlash)} isCheckoutButtonActive={isCheckoutButtonActive} />
          <ButtonPaymentPoint onPayment={login(this._onPaymentPoint)} isCheckoutButtonActive={isCheckoutButtonActive} />
          <ButtonCheckout onCheckout={login(this._onCheckout)} isCheckoutButtonActive={isCheckoutButtonActive} />
        </div>
      </CheckoutBoxStyled>
    );
  }
}

export default CheckoutBox;
