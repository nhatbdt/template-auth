import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withTranslation } from 'react-i18next';
import { inject, observer } from 'mobx-react';

import { Colors, Images } from '../../theme';
import Format from '../../utils/format';
import Media from '../../utils/media';
import numeral from 'numeral';
import Typography from '../../components/typography';
import Thumbnail from '../../components/thumbnail';
import { Divider } from 'antd';
import Misc from '../../utils/misc';
import { getRateByCurrency } from '../../utils/rates';
import CURRENCIES from '../../constants/currencies';
import { checkProcessing } from './utils/func';

const NameBoxStyled = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 20px;

  .value-box {
    display: flex;
    align-items: center;
    margin: 10px 0;

    .row {
      display: flex;
      margin-right: 10px;
      align-items: center;
      .typography {
        color: ${Colors.TEXT};
      }

      img {
        margin-right: 5px;
      }
    }
  }

  .line {
    margin: 10px 0;
    background: #292d33;
    opacity: 0.5;
  }

  ._timer {
    color: ${Colors.TEXT};
  }

  .text-process {
    text-align: center;
    color: ${Colors.BLUE};
    font-weight: 500;
    margin-top: 15px;
  }
  .mb {
    margin-bottom: 5px;
    display: inline-block;
  }
  .holder-box {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    gap: 8px;
    margin-top: 10px;

    .avata {
      border-radius: 999px;
    }
    .holder-style {
      font-weight: 400;
      font-size: 14px;
      color: ${Colors.TEXT};
      .holder-style-name {
        font-weight: 500;
        font-size: 16px;
        color: #fff;
        max-width: 300px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }
    }
  }
  .price-purchase-style {
    .label {
      font-weight: 500;
      font-size: 16px;
      color: ${Colors.TEXT};
      margin-bottom: 10px;
    }
    ._inner {
      display: flex;
      align-items: flex-end;
      gap: 10px;
      border: 2px solid #22c55e;
      border-radius: 12px;
      width: fit-content;
      padding: 8px 16px;
      .price-purchase-value {
        font-family: 'Noto Sans JP', sans-serif;
        color: #22c55e;
        font-style: normal;
        font-weight: 700;
        font-size: 28px;
        line-height: 32px;
        ${Media.lessThan(Media.SIZE.SM)} {
          line-height: 20px;
          font-size: 20px;
        }
      }
      ._yen-price {
        color: ${Colors.TEXT};
        font-size: 18px;
        line-height: 32px;
        font-style: italic;
        font-weight: 400;
        ${Media.lessThan(Media.SIZE.SM)} {
          line-height: 20px;
          font-size: 14px;
        }
      }
    }
  }
  .auction-end-noti-box {
    height: 48px;
    background: linear-gradient(0deg, #261404, #261404), linear-gradient(0deg, #f87a1f, #f87a1f);
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 6px;
    margin-bottom: 28px;
    border: 1px solid #f87a1f;

    .typography {
      font-weight: 400;
      font-size: 16px;
      color: #f87a1f;
    }
  }
  .product-description-title {
    font-style: normal;
    font-weight: 600;
    font-weight: 600;
    font-size: 36px;
    color: #fff;
    max-width: 90%;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    ${Media.lessThan(Media.SIZE.SM)} {
      font-size: 26px;
    }
  }

  .field-group {
    display: flex;
    align-items: center;

    .field {
      display: flex;
      align-items: center;
      margin-right: 34px;

      &:last-child {
        margin-right: 0;
      }

      img {
        margin-right: 9px;
      }

      .heat-label {
        color: ${Colors.RED_2};
      }

      .typography {
        opacity: 0.35;

        &.active {
          color: #3cc8fc;
          opacity: 1;
        }
      }
    }
  }

  .product-description-outer {
    .product-description {
      font-size: 24px;
    }

    .field-group {
      display: none;
    }
  }

  .product-name {
    font-size: 18px;
    margin-bottom: 51px;
    opacity: 0.3;
  }

  .user-info-box {
    max-width: 670px;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding-bottom: 17px;
    border-bottom: 2px solid rgb(255 255 255 / 8%);

    ._side-box {
      display: flex;
      align-items: center;

      .user-label {
        margin-right: 28px;
        opacity: 0.35;
      }

      .user-avatar {
        margin-right: 8px;
      }

      .user-name {
        color: ${Colors.TEXT};
        font-size: 13px;
        margin-right: 8.5px;
      }
    }
  }

  .product-status {
    display: flex;
    justify-content: space-between;
    align-items: center;
    .product-description {
      border: 0.5px solid #3cc8fc;
      display: inline-block;
      padding: 7px 35px;
      border-radius: 23px;

      p,
      button {
        font-size: 15px;
        color: #3cc8fc;
        // text-transform: uppercase;
      }
      ${Media.lessThan(Media.SIZE.MD)} {
        padding: 5px 20px;
        p {
          font-size: 12px;
        }
      }
    }
    .field-group {
      .field {
        flex-direction: column;
        justify-content: center;
        align-items: center;
        img {
          margin-right: 0;
        }
      }
      &.filed-group-favorite {
        .field {
          flex-direction: column;
          justify-content: center;
          align-items: center;
          .active {
            color: #fff;
          }
          img {
            padding-right: 0;
            padding-left: 0;
            width: 16px;
            height: 16px;
          }
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    padding-top: 17px;
    margin-bottom: 19px;

    .product-description-outer {
      margin-bottom: 10px;
      display: flex;
      justify-content: space-between;

      .product-description {
        font-size: 32px;
      }

      .field-group {
        display: flex;

        .field {
          margin-right: 23px;

          &:last-child {
            margin-right: 0;
          }

          img {
            margin-right: 5px;
          }

          .typography {
            font-size: 12px;
          }
        }
      }
    }

    .product-name {
      font-size: 14px;
      margin-bottom: 0;
    }

    .user-info-box {
      display: none;
    }
    .product-status {
      .field-group {
        .field {
          flex-direction: row;
          img {
            padding-left: 10px;
          }
        }
        &.filed-group-favorite {
          .field {
            flex-direction: row-reverse;
            p {
              padding-left: 10px;
            }
          }
        }
      }
    }
  }
`;

@withTranslation(['common', 'product_details'])
@inject(stores => ({
  product: stores.products.currentProductDetails,
  authStore: stores.auth,
  productsStore: stores.products,
  lendingStore: stores.lending,
}))
@observer
class NameBox extends Component {
  static propTypes = {
    product: PropTypes.object,
    authStore: PropTypes.object,
    productsStore: PropTypes.object,
    lendingStore: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      priceRates: null,
    };
  }

  render() {
    const { product, t, authStore, isLending } = this.props;
    // const { loggedIn } = authStore;
    // const { usdToJpy } = authStore.initialData;
    const masterData = authStore.initialData;
    const truncateOwnerAddress = Misc.trimPublicAddress(product?.ownerAddress, 5);
    const isBuying = checkProcessing(product);
    // const isErc1155 = product?.tokenStandard === 'ERC1155';

    return (
      <NameBoxStyled>
        {
          // product?.productBid &&
          //   product?.productBid?.status !== 'NEW' &&
          //   !product?.isApproveOffer &&
          //   product?.isApproveOffer !== null &&
          product?.productBid &&
            !['NEW', 'INIT', 'EXPIRED'].includes(product?.productBid?.status) &&
            product?.offerNftTransactionStatus !== 'SUCCESS' &&
            product?.productBid?.transferTokenStatus !== 'SUCCESS' && (
              <div className="auction-end-noti-box">
                <Typography bold size="huge">
                  {t('product_details:auction_has_ended')}
                </Typography>
              </div>
            )
        }
        <div className="product-description-outer">
          <Typography size="large" className="product-description-title">
            {product?.title || product?.name}
          </Typography>
        </div>

        <div className="holder-box">
          <Thumbnail className="avata" size={40} url={product?.ownerAvatar || Images.GRAY_AVATA_DEFAULT} />
          <div className="holder-style mb">
            <p>{isLending ? t('common:lender') : t('product_details:holder')}</p>

            <p className="holder-style-name">{product?.ownerName ? product.ownerName : truncateOwnerAddress}</p>
          </div>
        </div>
        {/* {product?.tokenStandard === 'ERC1155' && (
          <div className="value-box">
            <div className="row">
              <img src={Images.OWNER_ICON} alt="owner" />
              <Typography>
                {product?.ownerQuantity ?? 0}
                &nbsp;
                {product?.owners > 1 ? t('product_details:erc1155.owners') : t('product_details:erc1155.owner')}
              </Typography>
            </div>
            <div className="row">
              <img src={Images.TOTAL_ICON} alt="owner" />
              <Typography>
                {product?.remainQuantity ?? 0}/{product?.totalQuantity ?? 0}
              </Typography>
            </div>
            {loggedIn && (
              <div className="row">
                <img src={Images.USER_ICON} alt="owner" />
                <Typography>
                  {t('product_details:erc1155.you_own')} {product?.holdingQuantity || 0}
                </Typography>
              </div>
            )}
          </div>
        )} */}

        {!isLending && (
          <>
            <Divider className="line" />
            <div className="price-purchase-style">
              <span className="label mb">
                {product?.status === 'SOLD' ? t('product_details:sold_price') : t('product_details:available_price')}
              </span>
              <div className="_inner">
                {/* {product?.status !== 'SOLD' ? (
                  <>
                    <Typography poppins className="_timer mb price-purchase-value" size="large" bold>
                      <b>{Format.price(product?.price)}</b>&nbsp;
                      {product?.currency}
                    </Typography>
                    <Typography className="_yen-price" bold>
                      (≈{numeral(product?.yenPrice).format('0,0')} {t('common:yen')}
                      {' / '}
                      {numeral(product?.yenPrice * (1 / getRateByCurrency(CURRENCIES.USD, masterData))).format(
                        '0,0.000',
                      )}{' '}
                      {t('common:usd')})
                    </Typography>
                  </>
                ) : (
                  <Typography poppins className="_timer mb price-purchase-value" size="large" bold>
                    <b>{numeral(product?.yenPrice).format('0,0')}</b>
                    &nbsp;
                    {t('common:yen')}
                  </Typography>
                )} */}
                <Typography poppins className="_timer mb price-purchase-value" size="large" bold>
                  <b>{Format.price(product?.price)}</b>&nbsp;
                  {product?.currency}
                </Typography>
                <Typography className="_yen-price" bold>
                  (≈{numeral(product?.yenPrice).format('0,0')} {t('common:yen')}
                  {' / '}
                  {/* {numeral(product?.yenPrice * (1 / usdToJpy)).format('0,0.000')} {t('common:usd')}) */}
                  {numeral(product?.yenPrice * (1 / getRateByCurrency(CURRENCIES.USD, masterData))).format(
                    '0,0.000',
                  )} {t('common:usd')}
                  {' / '}
                  {numeral(product?.yenPrice * (1 / getRateByCurrency(CURRENCIES.POINT, masterData))).format(
                    '0,0.000',
                  )} {t('common:point')})
                </Typography>
              </div>
            </div>
          </>
        )}
        {/* {isBuying && isErc1155 && <Typography className="text-process">{t('common:processing')}</Typography>} */}
        {isBuying && <Typography className="text-process">{t('common:processing')}</Typography>}
      </NameBoxStyled>
    );
  }
}

export default NameBox;
