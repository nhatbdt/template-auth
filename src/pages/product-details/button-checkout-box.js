import { Component } from 'react';
import PropTypes from 'prop-types';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';

import { checkProcessing } from './utils/func'

import Button from '../../components/button';
import { Colors } from '../../theme';

@withTranslation(['product_details', 'common'])
@inject(stores => ({
  product: stores.products.currentProductDetails,
}))
@observer
class ButtonCheckoutBox extends Component {
  static propTypes = {
    product: PropTypes.object,
    isCheckoutButtonActive: PropTypes.bool,
    onCheckout: PropTypes.func,
  };

  render() {
    const { product, onCheckout, isCheckoutButtonActive, i18n, t } = this.props;

    const isBuying = checkProcessing(product)
    const isErc721 = product?.tokenStandard === 'ERC721';
    const isReselling = product?.productResell?.reselling;
    const countRemain = product?.remainQuantity;
    const isErc1155 = product?.tokenStandard === 'ERC1155';
    const isValid = product?.status === 'SALE' && product?.displayStatus === 'PUBLIC';

    return (
      <>
        {product?.status !== 'SOLD' && (
          <>
            {isReselling && isErc721 && !product?.owned && (
              <Button
                size="large"
                disabled={!isCheckoutButtonActive}
                className="payment-button"
                background={Colors.RED_2}
                onClick={onCheckout}
              >
                {i18n.language === 'en' ? (
                  <>
                    {t('buy_now')}
                    {isErc721 && <span className="_small">{t('with_expect_price')}</span>}
                  </>
                ) : (
                  <>
                    {isErc721 && <span className="_small">{t('with_expect_price')}</span>}
                    {t('buy_now')}
                  </>
                )}
              </Button>
            )}
            {!isReselling && isErc721 && isCheckoutButtonActive && !product?.owned && (
              <Button size="large" className="payment-button" onClick={onCheckout}>
                {product?.holding ? t('product_details:during_trading') : t('buy_now')}
              </Button>
            )}
            {!isReselling && isErc1155 && countRemain > 0 && !isBuying && isValid && (
              <Button size="large" className="payment-button" onClick={onCheckout}>
                {t('buy_now')}
              </Button>
            )}
          </>
        )}
      </>
    );
  }
}

export default ButtonCheckoutBox;
