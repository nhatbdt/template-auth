import styled from 'styled-components';
import Media from '../../utils/media';
import { Colors } from '../../theme';

const StyledDiv = styled.div`
  margin-top: 92px;
  padding-top: 50px;
  ${Media.lessThan(Media.SIZE.LG)} {
    margin-top: 50px;
  }
  ${Media.lessThan(Media.SIZE.MD)} {
    margin-top: 40px;
  }
  ${Media.lessThan(Media.SIZE.SM)} {
    margin-top: 25px;
  }

  .line {
    margin: 10px 0;
    background: #292d33;
    opacity: 0.5;
  }

  .product-details-container {
    width: 100%;
    max-width: 1460px;
    /* padding: 0 40px; */
    padding: 0;
    margin: 0 auto;

    ${Media.lessThan(Media.SIZE.XL)} {
      padding: 0 15px;
    }
    ${Media.lessThan(Media.SIZE.MD)} {
      padding: 0;
    }
  }

  .wrapper {
    position: relative;
    display: flex;
    justify-content: space-between;
    gap: 20px;

    ${Media.lessThan(Media.SIZE.MD)} {
      flex-direction: column;
    }

    .btn-back {
      position: absolute;
      bottom: calc(100% + 50px);
      left: 20px;
      width: 44px;
      height: 44px;
      display: flex;
      justify-content: center;
      align-items: center;
      border: 1px solid #A8AEBA;
      border-radius: 50%;
      color: #A8AEBA;
      background-color: #0f1114;
      cursor: pointer;

      &:hover {
        background-color: #045afc;
        border: 1px solid #fff;
        color: #fff;
      }
    }

    .top-section {
      padding: 0 20px;
      width: fit-content;
      max-width: 612px;
      ${Media.lessThan(Media.SIZE.XL)} {
        height: auto;
        max-width: 400px;
        overflow: initial;
      }
      ${Media.lessThan(Media.SIZE.LG)} {
        max-width: 240px;
      }
      ${Media.lessThan(Media.SIZE.MD)} {
        width: 100%;
        max-width: 100%;
        overflow: hidden;
        display: flex;
        gap: 16px;
      }
      ${Media.lessThan(Media.SIZE.SM)} {
        flex-direction: column;
        .data-box {
          display: none;
        }
      }
    }

    .container {
      overflow: hidden;
      flex: 1;
      position: relative;
      padding-bottom: 120px;
      ${Media.lessThan(Media.SIZE.LG)} {
        width: 100%;
        height: auto;
        /* overflow: initial; */
        max-width: 100%;

        .checkout-container {
          width: 100%;
          padding: 0;
          z-index: 9;
          bottom: 60px;
          margin-bottom: 15px;

          .checkout-inside {
            .sell-button {
              position: inherit;
              width: 100%;
              margin-top: 20px;
            }
          }
        }

        .content-section {
          .tab-container {
            margin-top: 15px;
            .tab-item {
              flex: none;
              justify-content: flex-start;
            }
          }
        }
      }

      &.content-section-bid {
        padding-bottom: 60px;
      }
    }
  }

  .checkout-button {
    background-color: #376587;
  }
  .user-name {
    /* color:#3CC8FC; */
    color: #282828;
  }
  .top-section {
    position: relative;
  }

  .content-section {
    display: flex;

    .data-box {
      display: none;
      ${Media.lessThan(Media.SIZE.SM)} {
        display: block;
      }
    }

    ._side {
      max-width: 100%;
      /* margin-right: 89px; */

      &:first-child {
        flex: 1;
      }

      &:last-child {
        margin-right: 0;
        width: 463px;
      }

      .tab-item {
        &.active {
          &:after {
            height: 1px;
          }
        }
      }
    }

    .tab-container {
      margin-top: 35px;
      padding: 10px;
      display: flex;
      justify-content: flex-start;
      /* border-bottom: 1px solid #3F414C; */
      /* padding-bottom: 1px; */
      padding-left: 0;
      overflow-x: auto;
      overflow-y: clip;

      .tab-item {
        padding: 0;
        margin-right: 20px;

        /* &.active {
          &:after {
            bottom: -1px;
          }
        } */
      }
    }
  }

  .checkout-container {
    margin-bottom: 35px;

    .checkout-inside {
      position: relative;

      .sell-button {
        position: absolute;
        right: 20px;
        top: 50%;
        margin-top: -22px;
      }
    }
  }

  .gradient-background {
    background-image: linear-gradient(to top, #2a2c3d, rgba(33, 35, 48, 0));
  }

  .bid-mobile {
    display: none;
  }

  .offer-button {
    /* background-color: ${Colors.BLUE_4}; */
    margin-top: 20px;
    background-color: #045afc;
    width: 100%;
    /* color:#FDFEFE; */
    color: #f9fafb;
    border-radius: 9999px;
  }

  ${Media.lessThan(Media.SIZE.LG)} {
    padding-bottom: 40px;

    .bid-mobile {
      display: block;
    }

    .bid-pc {
      display: none;
    }

    .tab-bar {
      height: 60px;

      .container {
        justify-content: flex-end;
      }
    }

    .content-section {
      flex-direction: column;

      ._side {
        margin-right: 0;

        &:first-child {
          width: auto;
          flex: 1;
        }
      }
    }

    .checkout-container {
      .checkout-inside-bid {
        background-color: transparent;
      }
    }
  }

  ${Media.lessThan(Media.SIZE.SM)} {
    .checkout-container .checkout-inside {
      padding: 10px;
    }
  }
`;

export { StyledDiv };
