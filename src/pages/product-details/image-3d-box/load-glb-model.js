/* eslint-disable no-unused-expressions */
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

function loadGLBModel(scene, glbPath, options) {
  const { receiveShadow, castShadow } = options;

  return new Promise((resolve, reject) => {
    const loader = new GLTFLoader();

    loader.load(
      glbPath,
      gltf => {
        const obj = gltf.scene;
        obj.name = 'primez';
        obj.position.y = 0;
        obj.position.x = 0;
        obj.receiveShadow = receiveShadow;
        obj.castShadow = castShadow;
        scene.add(obj);

        obj.traverse(function (child) {
          if (child.isMesh) {
            child.castShadow = castShadow;
            child.receiveShadow = receiveShadow;
          }
        });
        scene.add(gltf.scene);

        gltf.animations;
        gltf.scene;
        gltf.scenes;
        gltf.cameras;
        gltf.asset;

        resolve(gltf);
      },
      undefined,
      function (error) {
        // eslint-disable-next-line no-console
        console.log(error);
        reject(error);
      },
    );
  });
}

export { loadGLBModel };
