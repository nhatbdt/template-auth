import React, { useEffect, useRef, useState } from 'react';
import * as THREE from 'three';
import styled from 'styled-components';

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { loadGLBModel } from './load-glb-model';
import Media from '../../../utils/media';

const Content = styled.div`
  height: 100%;
  width: 100%;
  position: relative;

  display: flex;
  justify-content: center;
  align-items: center;

  span {
    position: absolute;
    left: 50%;
    top: 50%;
    color: #333;
  }

  ${Media.lessThan(Media.SIZE.SM)} {
    canvas {
      width: 100% !important;
      height: 400px !important;
    }
  }
`;

const GBL = ({ metaMaskImageUrl }) => {
  const gblRef = useRef();
  const [loading, setLoading] = useState(true);
  const [renderer, setRenderer] = useState();

  let req = null;
  let frame = 0;
  let action;

  useEffect(() => {
    _onLoad3DImage();

    return () => {
      cancelAnimationFrame(req);
      action.stop();
      window.removeEventListener('resize', handleWindowResize(renderer), false);
      renderer.dispose();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleWindowResize = renderer => {
    const HEIGHT = window.innerHeight;
    const WIDTH = window.innerWidth;
    renderer.setSize(WIDTH / 2, HEIGHT);
    // camera.aspect = WIDTH / HEIGHT
    // camera.updateProjectionMatrix()
  };

  const _onLoad3DImage = async () => {
    const { current: container } = gblRef;

    if (container && !renderer) {
      const screenWidth = container.clientWidth;
      const screenHeight = container.clientHeight;

      const renderer = new THREE.WebGLRenderer({
        antialias: true,
        alpha: true,
      });

      renderer.setPixelRatio(window.devicePixelRatio);
      renderer.setSize(screenWidth, screenHeight);

      renderer.outputEncoding = THREE.sRGBEncoding;
      container.appendChild(renderer.domElement);

      setRenderer(renderer);

      const scene = new THREE.Scene();
      const clock = new THREE.Clock();

      const loadResult = await loadGLBModel(scene, metaMaskImageUrl, {
        receiveShadow: true,
        castShadow: true,
      });

      setLoading(false);

      const box3 = new THREE.Box3().setFromObject(loadResult.scene);
      const size = new THREE.Vector3();
      const s = box3.getSize(size);
      const scaleValue = Math.abs(s.z) + Math.abs(box3.max.y) || 5.6;

      const vectorNumber = box3?.max?.z > 0 ? 0 : 1;

      const camera = new THREE.OrthographicCamera(-scaleValue, scaleValue, scaleValue, -scaleValue, 0.2, 50000);

      const target = new THREE.Vector3(vectorNumber, 0, 0);
      const ambienLight = new THREE.AmbientLight(0xffffff, 0.2);
      scene.add(ambienLight);

      const spotLight = new THREE.SpotLight(0xffffff, 1);
      spotLight.position.set(0, 0, 50);
      spotLight.castShadow = true;
      scene.add(spotLight);

      const light = new THREE.PointLight(0xffffff, 10, 100, 100);
      light.position.set(50, 50, 50);
      scene.add(light);

      const directionalLight = new THREE.DirectionalLight(0xffffff, 1);
      scene.add(directionalLight);

      const controls = new OrbitControls(camera, renderer.domElement);

      const axis = new THREE.Vector3(0, vectorNumber, 0);
      const angle = (90 * Math.PI) / 180;

      target.applyAxisAngle(axis, angle);
      controls.autoRotate = true;
      controls.target = target;

      // aniamation
      const mixer = new THREE.AnimationMixer(loadResult.scene);

      if (mixer && loadResult.animations.length > 0) {
        action = mixer.clipAction(loadResult.animations[loadResult.animations.length - 1]).play();
      }

      const animate = () => {
        if (mixer && loadResult.animations.length > 0) {
          const delta = clock.getDelta();
          mixer.update(delta);
        }

        req = requestAnimationFrame(animate);
        frame = frame <= 30 ? frame + 1 : frame;

        if (frame <= 30) {
          camera.position.y = 30;
          camera.position.x = 30;
          camera.position.z = 30;

          camera.lookAt(target);
        } else {
          controls.update();
        }
        renderer.render(scene, camera);
      };

      if (loadResult) {
        animate();
      }

      window.addEventListener('resize', handleWindowResize(renderer), false);
    }
  };

  return <Content ref={gblRef}>{loading && <span>Loading...</span>}</Content>;
};

export default GBL;
