import React from 'react'
import styled from 'styled-components'
import GBL from './gbl'
import GLTF from './gltf'

const Content = styled.div`
  background: #fff;
  width: 100%;
  height: 100%;
`

const Image3DBox = ({ metaMaskImageUrl }) => {
  const allow3DFile = /(\.glb)$/i;

  const isGlbFile = allow3DFile.exec(metaMaskImageUrl)

  return (
    <Content>
      {isGlbFile ? (
        <GBL metaMaskImageUrl={metaMaskImageUrl} />
      ) : (
        <GLTF metaMaskImageUrl={metaMaskImageUrl} />
      )}
    </Content>
  )
}

export default Image3DBox