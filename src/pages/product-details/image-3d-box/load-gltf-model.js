/* eslint-disable no-unused-expressions */
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

function loadGLTFModel(scene, glbPath) {
  return new Promise((resolve, reject) => {
    const loader = new GLTFLoader();

    loader.load(
      glbPath,
      gltf => {
        scene.add(gltf.scene);
        gltf.scene.scale.set(0.1, 0.1, 0.1);

        gltf.animations;
        gltf.scene;
        gltf.scenes;
        gltf.cameras;
        gltf.asset;

        resolve(gltf);
      },
      undefined,
      function (error) {
        // eslint-disable-next-line no-console
        console.log(error);
        reject(error);
      },
    );
  });
}

export { loadGLTFModel };
