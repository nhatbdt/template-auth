import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Modal, Popover } from 'antd';
import { FacebookShareButton, TwitterShareButton } from 'react-share';
import { withTranslation } from 'react-i18next';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';

import Clickable from '../../components/clickable';
import { Images, Colors } from '../../theme';
import Media from '../../utils/media';
import Typography from '../../components/typography';
// import { login } from '../../utils/conditions';
import StatusTag from '../../components/status-tag';
import { getStatusText } from '../../utils/products';
import Format from '../../utils/format';
import numeral from 'numeral';
import QRCode from 'react-qr-code';
import { getRateByCurrency } from '../../utils/rates';
import CURRENCIES from '../../constants/currencies';

const TopBarStyled = styled.div`
  margin-bottom: 20px;

  ._content {
    display: flex;
    justify-content: space-between;
    align-items: flex-start;
    /* padding-top: 28px; */

    .side-box {
      display: flex;
      flex-wrap: wrap;
      gap: 4px 10px;

      .category {
        border-radius: 20px;
        padding: 2px 10px;
        width: fit-content;
        color: #045afc;
        font-weight: 500;
        background-color: #e0ebff;
        max-width: 300px;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
      }

      .button-group {
        display: flex;
        align-items: center;
        gap: 26px;

        .row {
          display: flex;
          justify-content: center;
          align-items: center;
          margin-right: 10px;
          img {
            padding-right: 6px;
            display: flex;
          }
          span {
            font-size: 14px;
          }
        }

        .rounded,
        .advanced {
          display: flex;
          align-items: center;
        }
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    margin-bottom: 0;

    ._content {
      .side-box {
        flex-wrap: wrap;
        margin-right: 15px;

        .tag {
          margin-right: 5px;
          margin-bottom: 5px;
        }
      }
    }
  }
`;

const ShareBox = styled.div`
  display: flex;
  flex-direction: column;
  background-color: transparent;
  border-radius: 8px;

  button {
    /* margin-bottom: 6px; */
    padding: 3px 10px !important;
    color: ${Colors.BLUE_4} !important;

    /* &:last-child {
      margin-bottom: 0;
    } */
    &:hover {
      /* background-color: ${Colors.PRIMARY} !important; */
      background-color: #46464f !important;
      color: #fff !important;
    }
  }
  /* button + button {
    border-top: 1px solid #757575 !important;
  } */

  &.new-share-box {
    flex-direction: row;
    border-radius: 20px;

    button {
      border-top: none !important;
      width: 85px;

      &:first-child {
        border-radius: 20px 0 0 20px;
      }

      &:last-child {
        border-radius: 0 20px 20px 0;
      }
    }

    .btn-share {
      cursor: pointer;
      border: none;
      outline: none;
      background: transparent;
    }
  }
`;

const ModalComponent = styled(Modal)`
  background-color: transparent;

  .ant-modal-content {
    border-radius: 20px;
    color: #ffffff;
    background: #1b1e24;

    .ant-modal-close-x {
      svg {
        fill: #6f7d95;
      }
    }

    .ant-modal-body {
      padding: 35px 20px;

      .row-top {
        display: flex;

        img {
          object-fit: cover;
        }

        .info {
          padding-left: 15px;

          .label {
            border-radius: 9999px;
            background: #e0ebff;
            color: #045afc;
            padding: 4px 10px;
            font-size: 12px;
            display: initial;
          }

          .name {
            margin-top: 5px;
            font-weight: 700;
            font-size: 20px;
          }

          .price-text {
            font-size: 14px;
            color: #a8aeba;
          }

          ._inner {
            display: flex;
            align-items: flex-end;
            gap: 10px;
            width: fit-content;
            .price-purchase-value {
              font-family: 'Noto Sans JP', sans-serif;
              color: #22c55e;
              font-style: normal;
              font-weight: 700;
              font-size: 18px;
              line-height: 32px;
              ${Media.lessThan(Media.SIZE.SM)} {
                line-height: 20px;
                font-size: 20px;
              }
            }
            ._yen-price {
              color: ${Colors.TEXT};
              font-size: 18px;
              line-height: 32px;
              font-style: italic;
              font-weight: 400;
              ${Media.lessThan(Media.SIZE.SM)} {
                line-height: 20px;
                font-size: 14px;
              }
            }
          }
        }
      }

      .row-content {
        width: 270px;
        height: 270px;
        margin: 0px auto;
        margin-top: 34px;
      }

      .row-bottom {
        display: flex;
        margin-top: 20px;
        justify-content: center;

        .btn {
          cursor: pointer;
          display: flex;
          align-items: center;

          .ic {
            height: 42px;
            width: 42px;
            background-color: #a8aeba;
            border-radius: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            margin-right: 8px;
          }

          /* &:last-child {
            margin-left: 30px;
          } */
        }
      }
    }
  }
`;

@withRouter
@withTranslation('common')
@inject(stores => ({
  productsStore: stores.products,
  product: stores.products.currentProductDetails,
  authStore: stores.auth,
}))
@observer
class TopBar extends Component {
  static propTypes = {
    product: PropTypes.object,
    productsStore: PropTypes.object,
    authStore: PropTypes.object,
  };

  state = {
    isOpenModalQR: false,
  };

  onLike = async () => {
    const { productsStore, product } = this.props;

    const { success, data } = await productsStore.favorite({
      productId: product?.id,
    });

    if (success) {
      product.setData({
        wishCount: data,
        isUserWished: !product?.isUserWished,
      });
    }
  };

  downloadQR = id => {
    const svg = document.getElementById('qrcode');
    const svgData = new XMLSerializer().serializeToString(svg);
    const canvas = document.createElement('canvas');
    const ctx = canvas.getContext('2d');
    const img = new Image();
    img.onload = function () {
      canvas.width = img.width;
      canvas.height = img.height;
      ctx.drawImage(img, 0, 0);
      const pngFile = canvas.toDataURL('image/png');

      const downloadLink = document.createElement('a');
      downloadLink.download = `qrcode-product-${id}`;
      downloadLink.href = `${pngFile}`;
      downloadLink.click();
    };

    img.src = 'data:image/svg+xml;base64,' + btoa(svgData);
  };

  render() {
    const { product, t, i18n, authStore } = this.props;
    // const { loggedIn } = authStore;
    // const { usdToJpy } = authStore.initialData;
    const masterData = authStore.initialData;
    const { isOpenModalQR } = this.state;

    const shareUrl = `${process.env.REACT_APP_DOMAIN}/${i18n.language}/product-details/${product?.id}`
    return (
      <TopBarStyled>
        <div className="_content">
          <div className="side-box">
            <Typography className="category">{product?.categoryName}</Typography>
            <StatusTag item={product} text={getStatusText(product, t)} />
          </div>
          <div className="side-box">
            <div className="button-group">
              {/* <div className="row">
                <img className="icon-layout" src={Images.GRAY_VIEW_ICON} alt="" />
                <span>{product?.countView || 0}</span>
              </div> */}
              {/* <div className="row favorite-action">
                <Clickable onClick={login(this.onLike)}>
                  <img
                    className="icon-layout"
                    src={product?.isUserWished ? Images.RED_HEART_ICON : Images.GREY_HEART_ICON}
                    alt=""
                    width={28}
                  />
                </Clickable>
                <span>{product?.wishCount || 0}</span>
              </div> */}
              <Popover
                // showArrow={false}
                overlayClassName="product-detail"
                content={
                  <ShareBox className="share-box new-share-box">
                    <FacebookShareButton url={shareUrl}>
                      <img src={Images.SHARE_FACEBOOK_ICON} alt="SHARE_FACEBOOK_ICON" />
                      <p>Facebook</p>
                    </FacebookShareButton>
                    <TwitterShareButton url={shareUrl}>
                      <img src={Images.SHARE_TWITTER_ICON} alt="SHARE_TWITTER_ICON" />
                      <p>Twitter</p>
                    </TwitterShareButton>

                    <button className="btn-share" onClick={() => this.setState({ isOpenModalQR: true })}>
                      <img src={Images.SHARE_QR_ICON} alt="SHARE_QR_ICON" />
                      <p>QR Code</p>
                    </button>
                    {/* <LineShareButton url={shareUrl}>Line</LineShareButton> */}
                  </ShareBox>
                }
                placement="left"
                trigger="hover"
              >
                <Clickable className="rounded">
                  <img src={Images.GRAY_SHARE_ICON} alt="" />
                </Clickable>
              </Popover>
              <Clickable className="advanced">
                <img src={Images.GRAY_ADVANCED_ICON} alt="" />
              </Clickable>
            </div>
          </div>
        </div>

        <ModalComponent
          width={500}
          open={isOpenModalQR}
          onCancel={() => this.setState({ isOpenModalQR: false })}
          destroyOnClose
          footer={null}
        >
          <div className="row-top">
            <img height={110} width={110} src={product?.imageUrl} alt={product?.imageUrl} />

            <div className="info">
              <div className="label">{product?.categoryName}</div>
              <div className="name">{product?.name}</div>
              <div className="price-text">{t('settings:buy_price')}</div>

              <div className="_inner">
                {/* {product?.status !== 'SOLD' ? (
                  <>
                    <Typography poppins className="_timer mb price-purchase-value" size="large" bold>
                      <b>{Format.price(product?.price)}</b>&nbsp;
                      {product?.currency}
                    </Typography>
                    <Typography className="_yen-price" bold>
                      (≈{numeral(product?.yenPrice).format('0,0')} {t('common:yen')}
                      {' / '}
                      {numeral(product?.yenPrice * (1 / getRateByCurrency(CURRENCIES.USD, masterData))).format(
                        '0,0.000',
                      )}{' '}
                      {t('common:usd')})
                    </Typography>
                  </>
                ) : (
                  <Typography poppins className="_timer mb price-purchase-value" size="large" bold>
                    <b>{numeral(product?.yenPrice).format('0,0')}</b>
                    &nbsp;
                    {t('common:yen')}
                  </Typography>
                )} */}
                <Typography poppins className="_timer mb price-purchase-value" size="large" bold>
                  <b>{Format.price(product?.price)}</b>&nbsp;
                  {product?.currency}
                </Typography>
                <Typography className="_yen-price" bold>
                  (≈{numeral(product?.yenPrice).format('0,0')} {t('common:yen')}
                  {' / '}
                  {/* {numeral(product?.yenPrice * (1 / usdToJpy)).format('0,0.000')} {t('common:usd')}) */}
                  {numeral(product?.yenPrice * (1 / getRateByCurrency(CURRENCIES.USD, masterData))).format(
                    '0,0.000',
                  )}{' '}
                  {t('common:usd')})
                </Typography>
              </div>
            </div>
          </div>

          <div className="row-content">
            <QRCode id="qrcode" value={shareUrl} />
          </div>

          <div className="row-bottom">
            {/* <div className="btn">
              <div className="ic">
                <img src={Images.SHARE_LINK_ICON} alt="SHARE_LINK_ICON" />
              </div>

              <p>Share</p>
            </div> */}

            <div className="btn" onClick={() => this.downloadQR(product?.id)}>
              <div className="ic">
                <img src={Images.SHARE_DOWNLOAD_ICON} alt="SHARE_DOWNLOAD_ICON" />
              </div>

              <p>{t('settings:download')}</p>
            </div>
          </div>
        </ModalComponent>
      </TopBarStyled>
    );
  }
}

export default TopBar;
