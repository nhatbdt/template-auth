import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import {
  Chart,
  LineElement,
  PointElement,
  LineController,
  CategoryScale,
  LinearScale,
  Filler,
  Tooltip,
} from 'chart.js';
import zoomPlugin from 'chartjs-plugin-zoom';
import moment from 'moment';
import { withTranslation } from 'react-i18next';

import Typography from '../../components/typography';
import Media from '../../utils/media';
import { Colors } from '../../theme';

Chart.defaults.color = Colors.TEXT;
Chart.defaults.font.size = 12;
Chart.defaults.font.family = "'Noto Sans JP', sans - serif";

Chart.register(LineElement, PointElement, LineController, CategoryScale, LinearScale, Filler, zoomPlugin, Tooltip);

const ChartBoxStyled = styled.div`
  margin-top: 43px;

  .section-title {
    display: none;
  }

  .currency-label {
    /* margin-left: 20px; */
    font-weight: 500;
    font-size: 16px;
    color: ${Colors.TEXT};
    opacity: 0.9;
  }

  /* .info-box {
    padding: 17px 0;
    display: flex;
    background-color: ${Colors.BLACK_1};
    border-radius: 6px;
    display: flex;
    justify-content: center;
    margin-top: 28px;
    justify-content: space-around;

    .item-box {
      .typography {
        &:last-child {
          font-size: 24px;

          span {
            font-size: 16px;
            margin-left: 5px;
          }
        }
      }
    }

    .divider {
      display: none;
    }
  } */

  ${Media.lessThan(Media.SIZE.MD)} {
    margin-top: 17px;
    padding-bottom: 12px;

    .section-title {
      display: block;
      margin-bottom: 10px;
    }

    /* .info-box {
      background-color: transparent;
      align-items: center;
      justify-content: unset;
      padding: 0;
      margin-top: 15px;

      .item-box {
        flex: 1;

        .typography {
          &:last-child {
            font-size: 20px;
            text-align: right;

            span {
              font-size: 12px;
            }
          }
        }
      }

      .divider {
        display: block;
        width: 1px;
        background-color: rgba(255, 255, 255, 0.08);
        height: 40px;
        margin: 0 12px;
      }
    } */
  }
`;

@withTranslation()
@inject(stores => ({
  product: stores.products.currentProductDetails,
  productsStore: stores.products,
}))
@observer
class ChartBox extends Component {
  static propTypes = {
    product: PropTypes.object,
    productsStore: PropTypes.object,
  };

  state = {
    averagePrice: null,
    totalPrice: null,
  };

  componentDidMount() {
    this._chart = new Chart(this._canvas, {
      type: 'line',
      data: {
        datasets: [
          {
            backgroundColor: '#045afc3b',
            borderColor: '#045AFC',
            pointBackgroundColor: '#045AFC',
            fill: true,
          },
        ],
      },
      options: {
        responsive: true,
        layout: {
          padding: {
            top: 0,
          },
        },
        elements: {
          line: {
            tension: 0.3,
          },
        },
        plugins: {
          // zoom: {
          //   zoom: {
          //     wheel: {
          //       enabled: true,
          //     },
          //     pinch: {
          //       enabled: true,
          //     },
          //     drag: {
          //       enabled: true,
          //     },
          //     mode: 'x',
          //     speed: 100,
          //   },
          // },
          tooltip: {
            callbacks: {
              label(context) {
                return ` ${context.formattedValue} JPY`;
              },
            },
          },
        },
        scales: {
          y: {
            grid: {
              color: 'rgb(255 255 255 / 20%)',
            },
          },
          x: {
            grid: {
              color: 'rgb(255 255 255 / 6%)',
            },
          },
        },
      },
    });

    this._fetchData();
  }

  _fetchData = async () => {
    const { productsStore, product } = this.props;
    if (!product?.id) return;

    const { success, data } = await productsStore.getPriceChartData({
      productId: product.id,
    });

    if (success) {
      this.setState({
        averagePrice: data.averagePrice,
        totalPrice: data.totalPrice,
      });

      this._updateChart(data.data);
    }
  };

  _updateChart = data => {
    const labels = data.map(item => moment(item.createdAt).format('M/D'));
    const newData = data.map(item => item.price);

    this._chart.data.labels = labels;
    this._chart.data.datasets[0].data = newData;
    this._chart.update();
  };

  render() {
    // const { t } = this.props;
    // const { averagePrice, totalPrice } = this.state;

    return (
      <ChartBoxStyled>
        <Typography className="section-title" size="small">
          価格データ
        </Typography>
        <Typography className="currency-label" size="small" secondary>
          JPY
        </Typography>
        <canvas
          height={80}
          ref={ref => {
            this._canvas = ref;
          }}
        />
        {/* <div className="info-box">
          <div className="item-box">
            <Typography size="small" secondary>
              {t('product_details:all_average_price')}
            </Typography>
            <Typography bold>
              {averagePrice ? Format.price(averagePrice) : '-'}
              <span>JPY</span>
            </Typography>
          </div>
          <div className="divider" />
          <div className="item-box">
            <Typography size="small" secondary>
              {t('product_details:all_volume')}
            </Typography>
            <Typography bold>
              {totalPrice ? Format.price(totalPrice) : '-'}
              <span>JPY</span>
            </Typography>
          </div>
        </div> */}
      </ChartBoxStyled>
    );
  }
}

export default ChartBox;
