import styled from 'styled-components';
import { Colors } from '../../../theme';
import Media from '../../../utils/media';

const SellModalStyled = styled.div`
  background-color: #1b1e24;
  color: #fff;

  .loading {
    height: 550px;
  }

  .modal-header {
    display: flex;
    align-items: center;
    padding: 30px 28px 0;

    p {
      font-size: 20px;
      color: #fff;
      font-weight: 500;
    }
  }

  .modal-header-lending {
    /* height: 60px; */
    display: flex;
    align-items: center;
    padding: 40px 28px 0;

    p {
      font-size: 20px;
      font-weight: 600;
      color: #fff;
    }
  }

  .content {
    background-color: inherit;
    display: flex;
    /* padding: 36px 66px 66px; */
    padding: 30px 50px 50px;

    .box-price-avg {
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin-bottom: 18px;
      margin-top: 10px;

      .value {
        color: #045afc;
      }
    }

    .label-box {
      display: flex;
      align-items: center;

      .hint {
        margin-left: 10px;
        border: solid 1px #045AFC;
        width: 18px;
        height: 18px;
        border-radius: 9px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: #045AFC;
        font-size: 15px;
      }

      > p {
        color: #fff;
        font-size: 16px;
        font-weight: 500;
      }
    }

    .content-side {
      flex: 1;
      padding-top: 30px;

      .offer-switch {
        display: flex;
        justify-content: flex-start;
        align-items: center;
        margin-bottom: 20px;

        .label {
          margin-right: 70px;
        }
      }

      &:first-child {
        flex: 1.3;

        .tab-box {
          display: flex;
          margin-bottom: 20px;

          .tab-item {
            flex: 1;
            border: solid 1px rgba(255, 255, 255, 0.05);
            height: 50px;
            display: flex;
            justify-content: center;
            align-items: center;
            transition: background-color 0.2s;
            font-size: 16px;
            opacity: 0.3;

            &:first-child {
              border-radius: 50px;
              border-right: none;
            }

            &:last-child {
              border-radius: 50px;
              border-left: 1px solid rgba(255, 255, 255, 0.05);
            }

            &.active {
              background-color: #045AFC;
              color: #F9FAFB;
            }
          }
        }

        .normal-panel {
          .field-box {
            ._input-outter {
              .input-value {
                .simple.input {
                  color: #282828;
                  border: 1px solid #dedede;
                }
              }
            }

            .input-unit-box p {
              color: #fff;
              font-size: 18px;
            }

            .input-wrapper ._input-outter .input-box input {
              font-size: 18px;
            }

            .input-wrapper ._input-outter .number-box p {
              font-size: 10px;
            }
          }

          .input-price {
            .field-box {
              width: 100%;
              max-width: 100%;
            }
            
            .field-box .input-price-wrapper ._input-outter .input-box input {
              opacity: 1;
            }

            .field-box .input-price-wrapper ._input-outter .input-box .ant-input-number {
              height: auto;
            }
          }
        }

        
      }

      &:last-child {
        .top-box {
          margin-bottom: 65px;

          .label-box {
            margin-bottom: 27px;
          }

          .data-field-divider {
            height: 1px;
            background: linear-gradient(to right, rgba(255, 255, 255, 0.24) 30%, rgba(255, 255, 255, 0) 0%);
            background-size: 6px 3px;
            margin-bottom: 10px;
          }

          .text-field {
            display: flex;
            margin-bottom: 10px;

            &:last-child {
              margin-bottom: 0;
            }

            ._name {
              width: 150px;
              /* width: fit-content; */
              color: #a8aeba;
              font-size: 14px;
            }

            ._value {
              flex: 1;
              text-align: left;
              color: #045AFC;
              font-size: 14px;
            }
          }
        }

        .bottom-box {
          .accept-terms-checkbox {
            .ant-checkbox-wrapper {
              > span {
                /* color: #282828; */
                color: ${Colors.TEXT};
                &:last-child {
                  font-size: 12px;
                  opacity: 0.5;
                }
              }
              .ant-checkbox-inner {
                /* border-color: #045AFC; */
                background-color: transparent;
              }
            }
          }

          
        }
      }

      .action-box {
            display: flex;
            align-items: center;
            margin-top: 33px;
            justify-content: center;
            flex-wrap: wrap;
            gap: 6px 20px;

            button {
              /* height: 50px; */
              height: 45px;
              padding: 0 35px;
              font-size: 16px;
              /* background-color: #045afc; */
              background-color: ${Colors.BUTTON_BACKGROUND};
            }

            .cancel-button {
              height: 45px;
              display: flex;
              padding: 0 20px;
              border-radius: 9999px;
              border: 1px solid ${Colors.BORDER};

              .typography {
                margin: auto;
                font-size: 16px;
                /* color: #282828; */
                color: ${Colors.TEXT};
                white-space: nowrap;
              }
            }
          }
    }

    .divider {
      display: flex;
      align-items: center;
      justify-content: center;
      /* padding: 0 60px; */
      padding: 0 20px;

      .line {
        width: 1px;
        height: 100%;
        background-color: #707070;
        opacity: 0.3;
      }

      .transfer-icon {
        background-color: inherit;
        position: absolute;
        padding-bottom: 6px;
        filter: none !important;

        img {
          width: 25px;
        }
      }
    }
  }

  .ant-checkbox-checked .ant-checkbox-inner {
    border-color: #045afc;
    background-color: #045afc !important;
  }

  .content.content-lending {
    padding: 0 28px 40px;

    .label {
      color: #fff;
      font-size: 16px;
      font-weight: 500;
    }

    .mt-18 {
      margin-top: 18px;
    }

    .curency {
      display: flex;
      margin-top: 5px;

      .containers {
        display: inline-block;
        position: relative;
        cursor: pointer;
        font-size: 16px;
        user-select: none;
        padding-left: 26px;
        color: #fff;

        &:not(:first-child) {
          margin-left: 30px;
        }
      }
      .containers .input {
        position: absolute;
        opacity: 0;
        cursor: pointer;
      }
      .checkmark {
        position: absolute;
        top: 3px;
        left: 0;
        width: 18px;
        height: 18px;
        background: transparent;
        border-radius: 50%;
        border: 1px solid #a8aeba;
      }

      .checkmark:after {
        content: '';
        position: absolute;
        display: none;
      }
      .containers .checkmark:after {
        top: 50%;
        left: 50%;
        width: 12px;
        height: 12px;
        border-radius: 50%;
        border: solid 1px transparent;
        transform: translate(-50%, -50%) rotate(45deg);
      }
      .containers .input:checked ~ .checkmark {
        border: 1px solid #045afc;
      }
      .containers .input:checked ~ .checkmark:after {
        display: block;
        background: #045afc;
      }
    }

    .price {
      padding: 15px 10px 0px 15px;
      border-radius: 10px;
      border: 1px solid #65676b;
      margin-top: 7px;

      .top {
        display: flex;

        .left {
          justify-content: center;
          align-items: center;
          display: flex;
          span {
            font-size: 18px;
            font-weight: 700;
            margin-left: 15px;
          }
        }

        input {
          width: 100%;
          border: none;
          outline: none;
          font-size: 20px;
          font-weight: 700;
          color: #fff;
          background: transparent;
          text-align: right;

          &::-webkit-outer-spin-button,
          &::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
          }
        }
      }

      .bottom {
        display: flex;
        justify-content: flex-end;
        padding-bottom: 5px;

        div {
          font-size: 10px;
          color: #a8aeba;

          &:not(:first-child) {
            margin-left: 15px;
          }
        }
      }
    }

    .select-tag {
      width: 100%;
      background: transparent;
      color: #fff;
      font-size: 18px;
      border: none;
      margin-top: 7px;

      .ant-select-selection-item {
        color: #fff;
        border-radius: 10px;
        padding: 12px 15px;
      }

      .ant-select-selector {
        border: 1px solid #65676b;
        height: initial;
        border-radius: 10px;
      }

      .ant-select-item-option::before {
        display: none;
      }

      .ant-select-dropdown {
        border: 1px solid #65676b;
        border-radius: initial !important;
      }

      .ant-select-arrow {
        color: #65676b;
      }
    }

    .content-side {
      &:first-child {
        flex: 1.2;
      }

      .top-box {
        .label-box {
          margin-bottom: 7px;
        }

        .text-field {
          .typography._name {
            font-size: 14px;
            width: 150px;
            color: #a8aeba;
          }
          .typography._value {
            text-align: left;
            color: #045afc;
            font-size: 14px;
          }
        }
      }

      &:last-child {
        .top-box {
          margin-bottom: 0;
        }
      }

      .bottom-box {
        margin-top: 97px;

        
      }
    }

    .divider {
      .transfer-icon {
        filter: none !important;
      }
    }

    .content-side-2 {
      padding-top: 40px;
    }
  }

  .input-price {
    .field-box {
      width: 100%;
      max-width: 100%;
    }
    
    .field-box .input-price-wrapper ._input-outter .input-box input {
      opacity: 1;
    }

    .field-box .input-price-wrapper ._input-outter .input-box .ant-input-number {
      height: auto;
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    .modal-header {
      height: 50px;
      padding: 0 20px;

      p {
        font-size: 18px;
      }
    }

    .content {
      flex-direction: column;
      padding: 15px;

      .content-side {
        &:first-child {
          .tab-box {
            margin-bottom: 20px;

            .tab-item {
              height: 50px;
            }
          }

          /* .normal-panel {
            .field-box {
              margin-top: 10px;

              .input-wrapper {
                flex-direction: column;
                height: auto;

                .price_input {
                  display: flex;
                  justify-content: flex-start;
                  padding-top: 5px;
                  padding-left: 10px;

                  .input-unit-box {
                    padding: 0;

                    .nbng-icon {
                      width: 30px;
                      height: 30px;

                      img {
                        width: 21px;
                      }
                    }
                  }
                }

                ._input-outter {
                  .input-box {
                    input {
                      padding-top: 0;
                      padding: 0 15px;
                    }
                  }

                  .number-box {
                    flex-direction: column;
                    align-items: flex-end;
                    padding-right: 15px;

                    .number-box-divider {
                      display: none;
                    }
                  }
                }
              }
            }
          } */
        }

        &:last-child {
          .top-box {
            margin-bottom: 50px;

            .label-box {
              margin-bottom: 20px;
            }

            .text-field {
              margin-bottom: 7px;

              ._name {
                /* width: 130px; */
                width: fit-content;
                font-size: 14px;
              }

              ._value {
                flex: 1;
                text-align: right;
                font-size: 14px;
              }
            }
          }

          .bottom-box {
            .action-box {
              margin-top: 20px;
              flex-direction: column;

              button {
                height: 50px;
                padding: 0 35px;
                font-size: 16px;
              }

              .cancel-button {
                margin-left: 0;
                margin-top: 20px;
                margin-bottom: 20px;

                .typography {
                  font-size: 16px;
                  /* color: #ffffff; */
                  /* color: #282828; */
                  white-space: nowrap;
                }
              }
            }
          }
        }
      }

      .divider {
        padding: 20px 0;

        .line {
          height: 1px;
          width: 100%;
        }

        .transfer-icon {
          padding-bottom: 0;
          padding: 0 5px;
          margin-bottom: 2px;

          img {
            width: 22px;
          }
        }
      }
    }
  }
`;

export { SellModalStyled };
