import { Component } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import { inject, observer } from 'mobx-react';
import { Translation } from 'react-i18next';

import Modal from '../../../components/modal';
import Clickable from '../../../components/clickable';
import Button from '../../../components/button';
import Switch from '../../../components/switch';
import Confirmable from '../../../components/confirmable';
import Typography from '../../../components/typography';
import { Colors } from '../../../theme';
import { onSign, onError } from '../utils/checkout';
import { SellModalStyled } from './styled';
import MaskLoading from '../../../components/mask-loading';
import ProductInputAmount from '../../../components/product-input-amount';
import { CHAIN_LIST } from '../../../constants/chains';
import {
  sign,
  checkCorrespondingNetwork,
  approveTokenErc721ANew,
} from '../../../utils/web3';
import { MAX_LENGTH_INPUT_VALUE_FRACTION, MAX_LENGTH_INPUT_VALUE_INTEGER } from '../../../constants/common';

@inject(stores => ({
  authStore: stores.auth,
  offersStore: stores.offers,
  productsStore: stores.products,
  lendingStore: stores.lending,
  product: stores.products.currentProductDetails,
}))
@observer
class OfferModal extends Component {
  static propTypes = {
    product: PropTypes.object,
    authStore: PropTypes.object,
    offersStore: PropTypes.object,
    lendingStore: PropTypes.object,
    productsStore: PropTypes.object,
  };

  state = {
    isOpen: false,
    loading: false,
    initing: true,
    currentTab: 'NORMAL',
  };

  open = async (product) => {
    const { productsStore } = this.props
    const { data } = await productsStore.getProductDetails({ id: product.id });

    this.setState({
      isOpen: true,
      currentTab: 'NORMAL',
      initing: false,
      product: data
    });
  };

  close = () => this.setState({ isOpen: false });

  _sign = async () => {
    const { authStore } = this.props;
    const userNonceResult = await authStore.getUserNonce(authStore.initialData.userId);

    if (!userNonceResult.success) {
      throw userNonceResult.data;
    }

    const signature = await sign(userNonceResult.data.nonce, authStore.initialData.publicAddress);

    return signature;
  };

  _onSubmit = async (values, t, resetForm) => {
    const { product } = this.state
    const { offersStore, authStore } = this.props

    MaskLoading.open({});

    try {
      const signature = await onSign(authStore);
      await checkCorrespondingNetwork(product?.chainId);
      const paymentAddress = CHAIN_LIST[product?.chainId]?.exchangeContractAddress;

      if (!values.isCheckedOffer) {
        await offersStore.resellOfferFlag({
          productId: product?.id,
          canOfferFlag: values.isCheckedOffer,
          signature,
          minimumOfferPrice: values.offerPrice,
        });

        MaskLoading.close();
        resetForm({})
        return null;
      }

      let approveNftHash = await approveTokenErc721ANew(
        authStore.initialData.publicAddress,
        paymentAddress,
        product.tokenId,
        t,
      );

      const resellOfferFlag = await offersStore.resellOfferFlag({
        productId: product?.id,
        canOfferFlag: values.isCheckedOffer,
        signature,
        approveNftHash,
        minimumOfferPrice: values.offerPrice,
      });

      if (!resellOfferFlag.success) throw resellOfferFlag.data;

      MaskLoading.close();
      resetForm({})
      this.setState({
        loading: false,
        isOpen: false,
      });

      await Confirmable.open({
        content: t('product_details:offer.resale_offer_flag_success'),
        hideCancelButton: true,
      });
    } catch (error) {
      MaskLoading.close();
      if (error.error) {
        return Confirmable.open({
          content: t(`validation_messages:${error.error}`),
          hideCancelButton: true,
        });
      }

      if (error?.code !== 4001 && !error?.message?.includes('MetaMask Personal Message')) {
        onError(error, t);
      }
      // eslint-disable-next-line no-console
      console.error(error);
    } finally {
      MaskLoading.close();
      resetForm({})
      this.setState({
        loading: false,
        isOpen: false,
      });
    }
  }

  _validate = (values, t) => {
    let errors = {};

    if (!!values.isCheckedOffer) {
      if (+values.offerPrice < 0.001) {
        errors.offerPrice = t('MIN_PRICE_THAN_MORE_EQUAL_0001')
      } else if (!isNaN(+values.offerPrice) && values.offerPrice) {
        const sOfferPrice = values.offerPrice?.toString();
        let symbol = null;
        if (sOfferPrice.includes('.')) {
          symbol = '.';
        } else if (sOfferPrice.includes(',')) {
          symbol = ',';
        }

        if (!symbol) {
          if (sOfferPrice?.length > MAX_LENGTH_INPUT_VALUE_INTEGER) {
            errors.offerPrice = t('MAX_LENGTH_PRICE')
          }
        } else {
          const arr = sOfferPrice.split(symbol);
          if (arr[0]?.length > MAX_LENGTH_INPUT_VALUE_INTEGER || arr[1]?.length > MAX_LENGTH_INPUT_VALUE_FRACTION) {
            errors.offerPrice = t('MAX_LENGTH_PRICE')
          }
        }
      }
    }

    return errors;
  };

  _renderForm = ({ handleSubmit, values, errors, ...form }, t) => {
    const { loading, product } = this.state;
    const currencies = CHAIN_LIST[product?.chainId]?.erc20Token?.map(item => item.symbol);
    const selectedCurrency = currencies?.find((x) => x === values.currency)

    return (
      <Form>
        <div className="modal-header modal-header-lending">
          <Typography bold>
            {t('product_details:offer.offer_setting')}
          </Typography>
        </div>
        <div className="content content-lending">
          <div className="content-side">
            <div className='offer-switch'>
              <div className='label'>{t('product_details:offer.allow_offer')}</div>
              <Switch
                checked={values.isCheckedOffer}
                onChange={(value) => {
                  if (!value) form.setFieldValue('offerPrice', '')
                  form.setFieldValue('isCheckedOffer', value)
                }}
              />
            </div>

            {!!values.isCheckedOffer && (
              <ProductInputAmount
                className="input-price"
                titleInput={t('product_details:offer.price_minimum_offer')}
                currencies={currencies}
                selectedCurrency={selectedCurrency}
                values={values}
                errors={errors}
                onSelect={(value) => form.setFieldValue('currency', value)}
              />
            )}

            <div className="action-box">
              <Button
                size="large"
                onClick={handleSubmit}
                background={Colors.BLUE_4}
                loading={loading}
              >
                {t('product_details:offer.btn_create')}
              </Button>
              <Clickable className="cancel-button" onClick={this.close}>
                <Typography primary bold size="big">
                  {t('product_details:offer.btn_cancel')}
                </Typography>
              </Clickable>
            </div>
          </div>
        </div>
      </Form>
    );
  };

  render() {
    const { isOpen, loading, product } = this.state;
    const currencies = CHAIN_LIST[product?.chainId]?.erc20Token?.map(item => item.symbol);

    return (
      <Modal
        open={isOpen}
        onCancel={this.close}
        destroyOnClose
        maskClosable={!loading}
        // closable={!loading}
        closable={true}
        width={500}
        padding={0}
      >
        <SellModalStyled>
          <Translation>
            {t => (
              <Formik
                enableReinitialize
                validateOnChange
                validateOnBlur
                initialValues={{
                  currency: currencies?.[0],
                  offerPrice: product?.minimumOfferPrice || '',
                  isCheckedOffer: product?.canOfferFlag,
                }}
                validate={values => this._validate(values, t)}
                validationSchema={yup.object().shape({
                  offerPrice: yup.number().when('isCheckedOffer', {
                    is: true,
                    then: (schema) => schema.nullable().required(t('PRICE_REQUIRED')),
                  })
                })}
                onSubmit={(values, { resetForm }) => this._onSubmit(values, t, resetForm)}
                component={data => this._renderForm(data, t)}
              />
            )}
          </Translation>
        </SellModalStyled>
      </Modal>
    );
  }
}

export default OfferModal;
