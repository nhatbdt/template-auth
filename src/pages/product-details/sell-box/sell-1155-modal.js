import { Component } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import { inject, observer } from 'mobx-react';
import { Translation } from 'react-i18next';
import classnames from 'classnames';
import { Tooltip } from 'antd';

import Input from '../../../components/input';
import Modal from '../../../components/modal';
import Clickable from '../../../components/clickable';
import Loading from '../../../components/loading';
import Button from '../../../components/button';
import Checkbox from '../../../components/checkbox';
import Field from '../../../components/field';
import Confirmable from '../../../components/confirmable';
import Typography from '../../../components/typography';
import { approveAllErc1155, checkIsApproveForAll } from '../../../utils/web3';
import { Colors, Images } from '../../../theme';
import Format from '../../../utils/format';
import CURRENCIES from '../../../constants/currencies';
import { onSign } from '../utils/checkout';
import { SellModalStyled } from './styled';
import CurrenciesBox from '../../../components/currencies-box';
import ProductInputPrice from '../../../components/product-input-price';
import { getRateByCurrency } from '../../../utils/rates';

@inject(stores => ({
  authStore: stores.auth,
  productsStore: stores.products,
  sellStore: stores.sell,
  product: stores.products.currentProductDetails,
}))
@observer
class SellModal extends Component {
  static propTypes = {
    product: PropTypes.object,
    authStore: PropTypes.object,
    sellStore: PropTypes.object,
    productsStore: PropTypes.object,
  };

  state = {
    isOpen: false,
    loading: false,
    initing: true,
    currentTab: 'NORMAL',
    selectedCurrency: CURRENCIES.ETH,
    registerSellData: null,
  };

  open = async () => {
    const { sellStore, product } = this.props;

    this.setState({
      isOpen: true,
      currentTab: 'NORMAL',
      selectedCurrency: CURRENCIES.ETH,
      initing: true,
    });

    const registerSellResult = await sellStore.registerSell1155({
      productId: product.id,
      resellType: 'NORMAL',
      currency: CURRENCIES.ETH,
    });

    if (registerSellResult.success) {
      this.setState({
        registerSellData: registerSellResult.data,
        initing: false,
      });
    } else {
      this.close();
    }
  };

  close = () => this.setState({ isOpen: false });

  _onSubmit = async (values, t) => {
    const { sellStore, product, authStore } = this.props;
    const { selectedCurrency, registerSellData } = this.state;
    const { publicAddress } = authStore.initialData;
    this.setState({
      loading: true,
    });

    try {
      const signature = await onSign(authStore);

      let approveTokenTransactionHash = null;

      const isApproveForAll = await checkIsApproveForAll(publicAddress, t);

      if (!isApproveForAll) {
        approveTokenTransactionHash = await approveAllErc1155(publicAddress, t);
      }

      const sellResult = await sellStore.sell1155({
        currency: selectedCurrency,
        signature,
        approveTokenTransactionHash,
        price: values.sellPrice,
        productId: product.id,
        productResellId: registerSellData.productResellId,
        value: values.value,
      });

      if (!sellResult.success) {
        throw sellResult.data;
      } else {
        product.productResell.setData({
          resellCancelable: true,
          confirmResell: true,
          reselling: true,
        });

        product.setData({
          canResellERC1155: false,
        });

        await sellStore.getSellLisings({
          productId: product.id,
          limit: 20,
          page: 1,
        });

        Confirmable.open({
          content: t('product_details:sell.sell_success'),
          hideCancelButton: true,
        });
      }
    } catch (e) {
      let errorMessage = t('product_details:sell.sell_failed');
      if (e === 'ERROR_TIMEOUT') {
        errorMessage = 'Time out';
      }

      Confirmable.open({
        content: errorMessage,
        hideCancelButton: true,
        acceptButtonText: t('common:close'),
      });

      // eslint-disable-next-line no-console
      console.error(e);
    }

    this.close();

    this.setState({
      loading: false,
    });
  };

  _validate = (values, t) => {
    const { selectedCurrency, registerSellData } = this.state;
    let errors = {};

    if (+values.sellPrice <= 0) {
      errors = {
        sellPrice: t('validation_messages:PRICE_MIN', {
          x: `${Format.price(0)} ${selectedCurrency}`,
        }),
      };
    }

    if (+values.value > registerSellData.maxQuantity) {
      errors = {
        value: t('validation_messages:VALUE_MAX', {
          x: `${registerSellData.maxQuantity}`,
        }),
      };
    }

    if (+values.value <= 0) {
      errors = {
        value: t('validation_messages:MIN_VALUE_THAN_MORE_0', {
          x: `${registerSellData.maxQuantity}`,
        }),
      };
    }

    return errors;
  };

  _onTabSelect = item => {
    this.setState({
      currentTab: item.value,
    });
  };

  _onSelectCurrency = currency => {
    this.setState({
      selectedCurrency: currency,
    });
  };

  _renderNumberBox = price => {
    const { authStore } = this.props;
    const { selectedCurrency } = this.state;

    // const { ethToJpy, prxToJpy } = authStore.initialData;
    const masterData = authStore.initialData;

    // const pricesJpy = selectedCurrency === CURRENCIES.ETH ? ethToJpy * price : prxToJpy * price;
    const pricesJpy = getRateByCurrency(CURRENCIES[selectedCurrency], masterData) * price;

    return (
      <div className="number-box">
        <Typography poppins size="small">
          {Format.price(price) || 0} {selectedCurrency}
        </Typography>
        <div className="number-box-divider" />
        <Typography poppins size="small">
          {Format.price(pricesJpy)} JPY
        </Typography>
      </div>
    );
  };

  _renderNormalPanel = (values, errors, t) => {
    const { product } = this.props;
    const { selectedCurrency } = this.state;

    return (
      <div className="normal-panel">
        <CurrenciesBox onClick={this._onSelectCurrency} selectedCurrency={selectedCurrency} />
        <ProductInputPrice selectedCurrency={selectedCurrency} values={values} errors={errors} />
        <div className="field-box">
          <div className="_input-outter mt-2">
            <Typography size="large" bold>
              {'VALUE'}
            </Typography>
            <Field
              className="input-box bold input-value"
              type="number"
              name="value"
              maxLength={9}
              placeholder={0}
              boundedBelow={1}
              simple
              component={Input}
              min={1}
            />
            <Typography size="large" bold>
              Max value: {product.holdingQuantity}
            </Typography>
          </div>
        </div>
      </div>
    );
  };

  percentage = (value, percent) => (value / 100) * percent;

  _renderForm = ({ handleSubmit, values, errors }, t) => {
    const { loading, currentTab, selectedCurrency, /* convertedCommissionFee, */ registerSellData } = this.state;

    const { adminPercent } = registerSellData;

    // let receivePrice = values.sellPrice - convertedCommissionFee
    // receivePrice = receivePrice > 0 ? receivePrice : 0

    const adminFee = this.percentage(values.sellPrice, adminPercent);
    const totalEarned = values.sellPrice - adminFee;

    const numFixed = '0,0.000000';

    const TAB_ITEMS = [
      {
        name: t('product_details:sell.normal'),
        value: 'NORMAL',
      },
    ];
    const INFO_DATA = [
      {
        name: t('product_details:sell.form'),
        value: currentTab === 'NORMAL' ? t('product_details:sell.normal') : t('product_details:sell.auction'),
      },
      {
        name: t('product_details:sell.currency'),
        value: selectedCurrency,
      },
      {
        name: t('product_details:sell.price'),
        value: `${Format.price(values.sellPrice || 0, numFixed)} ${selectedCurrency}`,
      },
      {
        name: t('product_details:sell.transaction_fee'),
        value: `${Format.price(adminFee || 0, numFixed)} ${selectedCurrency}`,
      },
      {
        line: true,
        name: t('product_details:sell.receive_price'),
        value: `${Format.price(totalEarned, numFixed)} ${selectedCurrency}`,
      },
    ];

    return (
      <Form>
        <div className="modal-header">
          <Typography bold>{t('product_details:sell.resell')}</Typography>
        </div>
        <div className="content">
          <div className="content-side">
            <div className="tab-box">
              {TAB_ITEMS.map(item => (
                <Clickable
                  // onClick={() => this._onTabSelect(item)}
                  key={item.value}
                  className={classnames('tab-item', { active: item.value === currentTab })}
                >
                  {item.name}
                </Clickable>
              ))}
            </div>
            {currentTab === 'NORMAL' && this._renderNormalPanel(values, errors, t)}
          </div>
          <div className="divider">
            <div className="line" />
            <div className="transfer-icon">
              <img src={Images.GRAY_TRANSFER_ICON} alt="" />
            </div>
          </div>
          <div className="content-side">
            <div className="top-box">
              <div className="label-box">
                <Typography size="big" bold>
                  {t('product_details:sell.content')}
                </Typography>
                <Tooltip
                  title={
                    <Typography style={{ color: '#333333' }} size="small">
                      {t('product_details:sell.hint_3')}
                    </Typography>
                  }
                  color="white"
                  trigger="click"
                >
                  <Clickable className="hint">?</Clickable>
                </Tooltip>
              </div>
              {INFO_DATA.map((item, index) => (
                <div key={index}>
                  {item.line && <div className="data-field-divider" />}
                  <div className="text-field" key={index}>
                    <Typography className="_name" size="large">
                      {item.name}
                    </Typography>
                    <Typography className="_value" size="large">
                      {item.value}
                    </Typography>
                  </div>
                </div>
              ))}
            </div>
            <div className="bottom-box">
              <Field showError={false} className="accept-terms-checkbox" name="acceptTerms" component={Checkbox}>
                {t('product_details:bid.accept')}
              </Field>
              <div className="action-box">
                <Button
                  size="large"
                  disabled={!values.acceptTerms}
                  onClick={handleSubmit}
                  background={Colors.BLUE_4}
                  loading={loading}
                >
                  {t('product_details:sell.resell')}
                </Button>
                <Clickable className="cancel-button" onClick={this.close}>
                  <Typography text bold size="big">
                    {t('product_details:sell.cancel')}
                  </Typography>
                </Clickable>
              </div>
            </div>
          </div>
        </div>
      </Form>
    );
  };

  render() {
    const { isOpen, loading, initing } = this.state;

    return (
      <Modal
        open={isOpen}
        onCancel={this.close}
        destroyOnClose
        maskClosable={!loading}
        closable={!loading}
        width={960}
        padding={0}
      >
        <SellModalStyled>
          {initing ? (
            <Loading size="small" className="loading" />
          ) : (
            <Translation>
              {t => (
                <Formik
                  enableReinitialize
                  validateOnChange
                  validateOnBlur
                  initialValues={{
                    acceptTerms: false,
                    value: 1,
                  }}
                  validate={values => this._validate(values, t)}
                  validationSchema={yup.object().shape({
                    sellPrice: yup.number().nullable().required(t('PRICE_REQUIRED')),
                  })}
                  onSubmit={values => this._onSubmit(values, t)}
                  component={data => this._renderForm(data, t)}
                />
              )}
            </Translation>
          )}
        </SellModalStyled>
      </Modal>
    );
  }
}

export default SellModal;
