import React, { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withTranslation, useTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';
import { Divider } from 'antd';
import numeral from 'numeral';
import { sprintf } from "sprintf-js";
import styled from 'styled-components';
import { Images } from '../../../theme';
import Media from '../../../utils/media';
import CountdownComponent from '../../../components/countdown';
import Button from '../../../components/button';
import regex from '../../../utils/regex'
import { CHAIN_LIST } from '../../../constants/chains';
import MaskLoading from '../../../components/mask-loading';
import Confirmable from '../../../components/confirmable';
import { LENDING_TITLE } from '../../../constants/statuses'
import { onSign, onTransfer, onError } from '../utils/checkout';
import { login } from '../../../utils/conditions';
import { LENDING_STATUS } from '../../../constants/statuses'

import {
  getWeb3Instance,
  approveMaxNew,
  checkCorrespondingNetwork,
  checkBalance,
  getAllowanceByToken
} from '../../../utils/web3';

@withRouter
@withTranslation('product_details')
@observer
class BorrowDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dayAmount: null,
      isApproved: false,
    }

    this.countdownRef = React.createRef();
  }

  _checkBalance = async () => {
    const { dayAmount } = this.state
    const { authStore, product } = this.props;
    const totalValue = Number(dayAmount) * Number(product?.lendingDetailDTO?.pricePerDay)

    await checkBalance(authStore, product, totalValue)
  }

  handleApprove = async () => {
    const { dayAmount } = this.state
    const { t, history, authStore, match, product } = this.props
    const productId = match.params.id

    MaskLoading.open({});
    try {
      const { publicAddress } = authStore.initialData;
      const approvePublicAddress = CHAIN_LIST[product?.chainId]?.lendingContractAddress;
      const selectedCurrency = CHAIN_LIST[product.chainId]?.erc20Token[0]?.symbol
      const totalValue = Number(dayAmount) * Number(product?.lendingDetailDTO?.pricePerDay)

      await onSign(authStore);
      await checkCorrespondingNetwork(product?.chainId);
      await this._checkBalance();

      let allowance = await getAllowanceByToken(selectedCurrency, publicAddress, approvePublicAddress);
      if (productId && allowance < totalValue) {
        await approveMaxNew(
          publicAddress,
          t,
          selectedCurrency,
          'lending'
        );
      }

      // re-get allowance check with totalValue
      allowance = await getAllowanceByToken(selectedCurrency, publicAddress, approvePublicAddress);
      if (productId && allowance < totalValue) {
        this.setState({ isApproved: false })
      } else {
        this.setState({ isApproved: true })
      }

      MaskLoading.close();
      Confirmable.open({
        content: t('product_details:messages.approve_success'),
        hideCancelButton: true,
      });
    } catch (error) {
      if (error.error) {
        Confirmable.open({
          content: t(`validation_messages:${error.error || 'SOMETHING_WENT_WRONG'}`),
          hideCancelButton: true,
          onOk: () => history.push(`/product-details/${productId}`)
        });
      }
      // eslint-disable-next-line no-console
      console.error(error);
    } finally {
      MaskLoading.close();
    }
  }

  handleTransaction = async () => {
    const { dayAmount } = this.state
    const { t, authStore, lendingStore, match, history, product } = this.props
    const { borrowLendingProduct } = lendingStore
    const { publicAddress } = authStore.initialData;
    const productId = match.params.id
    const currency = product.currency;
    const gasLimit = 300000;

    MaskLoading.open({});
    try {
      const { lendingContract } = await getWeb3Instance();

      if (lendingContract) {
        const { success, data } = await borrowLendingProduct({
          productId,
          borrowDuration: Number(dayAmount)
        })

        if (success) {
          const dataRequestContract = await lendingContract.methods.borrow(
            product.tokenId, //_tokenId (uint256)
            dayAmount,//_borrowDuration (uint256)
            data.nonce,//nonce (uint256)
            data.time,//time (uint256)
            data.signature,//signature (bytes)
          )

          const transactionHash = await onTransfer(
            publicAddress,
            t,
            dataRequestContract.encodeABI(), //endCode
            gasLimit,
            currency,
            null,
            'lending'
          );

          if (transactionHash) {
            const { success } = await lendingStore.transactionLendingProduct({
              productId,
              title: LENDING_TITLE.BORROW,
              txHash: transactionHash,
            })

            if (success) {
              MaskLoading.close();
              Confirmable.open({
                content: t('product_details:messages.borrow_success'),
                hideCancelButton: true,
              });
            }
          }
        }
      }

      history.push(`/product-details/${productId}`)
    } catch (error) {
      if (error.error) {
        return Confirmable.open({
          content: t(`validation_messages:${error.error || 'SOMETHING_WENT_WRONG'}`),
          hideCancelButton: true,
          onOk: () => history.push(`/product-details/${productId}`)
        });
      }

      if (error?.code !== 4001 && !error?.message?.includes('MetaMask Personal Message')) {
        onError(error, t);
      }
      // eslint-disable-next-line no-console
      console.error(error);
    } finally {
      MaskLoading.close();
    }
  }

  handleCancelWithdraw = async () => {
    const { t, authStore, lendingStore, match, history, product, isExpiration } = this.props
    const { cancelLendingOwned, withdrawLendingOwned } = lendingStore
    const currency = product.currency;
    const productId = match.params.id
    const gasLimit = 300000;

    MaskLoading.open({});

    try {
      const { publicAddress } = authStore.initialData;

      await onSign(authStore);
      await checkCorrespondingNetwork(product?.chainId);
      const { lendingContract } = await getWeb3Instance();

      const isBorrowing = await lendingContract.methods.isBorrowing(
        product.tokenId, //_tokenId (uint256)
      ).call()

      if (!!isBorrowing) {
        const error = { error: 'ERROR_CAN_NOT_CANCEL_LEND_PRODUCT' }
        throw error
      }

      const { success, data } = isExpiration
        ? await withdrawLendingOwned({ productId })
        : await cancelLendingOwned({ productId })

      if (success && lendingContract) {
        const dataRequestContract = await lendingContract.methods.cancelLend(
          product.tokenId, //_tokenId (uint256)
          data.nonce,//nonce (uint256)
          data.time,//time (uint256)
          data.signature,//signature (bytes)
        )

        const transactionHash = await onTransfer(
          publicAddress,
          t,
          dataRequestContract.encodeABI(), //endCode
          gasLimit,
          currency,
          null,
          'lending'
        );

        if (transactionHash) {
          const { success } = await lendingStore.transactionLendingProduct({
            productId,
            title: isExpiration ? LENDING_TITLE.WITHDRAW : LENDING_TITLE.CANCEL,
            txHash: transactionHash,
          })

          if (success) {
            MaskLoading.close();
            Confirmable.open({
              content: isExpiration ? t('product_details:messages.withdraw_success') : t('product_details:messages.cancel_success'),
              hideCancelButton: true,
            });
          }
        }
      }

      history.push(`/product-details/${productId}`)
    } catch (error) {
      if (error.error) {
        Confirmable.open({
          content: t(`validation_messages:${error.error || 'SOMETHING_WENT_WRONG'}`),
          hideCancelButton: true,
          onOk: () => history.push(`/product-details/${productId}`)
        });
      }
      // eslint-disable-next-line no-console
      console.error(error);
    } finally {
      MaskLoading.close();
    }
  }

  render() {
    const { dayAmount, isApproved } = this.state
    const { t, isCancelPage, isExpiration, product } = this.props;

    return (
      <>
        <Divider className="line" />

        <ContentCountdown>
          <div className='title'>
            <img src={Images.GRAY_COUNTDOWN_ICON} alt='countdown lending' />
            <div className='text'>{t('product_details:lending.time_left_finish')}</div>
          </div>

          {!!isExpiration ? (
            <div className='label-expiration'>{t('product_details:lending.expired')}</div>
          ) : (
            <CountdownComponent
              date={product?.lendingDetailDTO?.offerExpirationEndTime}
            />
          )}
        </ContentCountdown>

        <Divider className="line" />

        <Content>
          <div className="rows">
            <div>{t('product_details:lending.price_per_day')}</div>

            <div className="value">
              <span className="txt">{numeral(product?.lendingDetailDTO?.pricePerDay).format('0,0.000')}</span>
              <img src={Images[product?.currency]} className="currency-icon" alt="PRICE_ICON" />
            </div>
          </div>

          <div className="rows">
            <div>{t('product_details:lending.max_rental_days')}</div>

            <div className="value">{product?.lendingDetailDTO?.rentalDuration} Days</div>
          </div>
        </Content>

        {!!isCancelPage ? (
          !!isExpiration ? (
            <ContentButton>
              <Button className="btn-cancel" onClick={login(this.handleCancelWithdraw)}>
                {t('product_details:lending.withdraw_lending')}
              </Button>
            </ContentButton >
          ) : (
            <ContentButton>
              <Button className="btn-cancel danger" onClick={login(this.handleCancelWithdraw)}>
                {t('product_details:lending.cancel_lending')}
              </Button>
            </ContentButton >
          )
        ) : !isExpiration && (
          <>
            <ContentInput>
              <div className="rows">
                <div className="label">{sprintf(
                  t('product_details:borrow.rental_days'),
                  product?.lendingDetailDTO?.rentalDuration
                )}</div>
                <div className="label-note">{t('product_details:borrow.rental_days_note')}</div>

                <div className="input-custom">
                  <input
                    placeholder={t('product_details:borrow.input_placeholder')}
                    type="number"
                    value={dayAmount}
                    disabled={!!isApproved}
                    onChange={(e) => {
                      const { value } = e.target
                      if (Number(value) === 0) {
                        this.setState({ dayAmount: '' })
                        return
                      }

                      if (!regex.integer.test((value)) && value !== '') {
                        return
                      } else {
                        this.setState({ dayAmount: value > product?.lendingDetailDTO?.rentalDuration ? product?.lendingDetailDTO?.rentalDuration : value })
                      }
                    }}
                  />
                </div>

                <div className="total">
                  <div className="text">{t('product_details:borrow.total_price')}</div>

                  <div className="value">
                    <span className="txt">{numeral(dayAmount * product?.lendingDetailDTO?.pricePerDay).format('0,0.000')}</span>
                    <img src={Images[product?.currency]} className="currency-icon" alt="PRICE_ICON" />
                  </div>
                </div>

                <div className="note">{t('product_details:borrow.note')}</div>
              </div>
            </ContentInput>

            <ContentButton>
              {!!isApproved ? (
                <Button
                  className="btn-cancel"
                  disabled={!!product.waitBorrow || [LENDING_STATUS.WAIT_LENDING, LENDING_STATUS.WAIT_BORROW].includes(product?.lendingDetailDTO?.status)}
                  onClick={login(this.handleTransaction)}
                >
                  {t('product_details:borrow.btn_borrow')}
                </Button>
              ) : (
                <Button
                  className="btn-cancel"
                  disabled={!dayAmount || !!product.waitBorrow || [LENDING_STATUS.WAIT_LENDING, LENDING_STATUS.WAIT_BORROW].includes(product?.lendingDetailDTO?.status)}
                  onClick={login(this.handleApprove)}
                >
                  {t('product_details:borrow.btn_approve')}
                </Button>
              )}
            </ContentButton >
          </>
        )}
      </>
    );
  }
}

const Content = styled.div`
  width: 100%;
  border-radius: 10px;
  color: #fff;
  font-size: 16px;
  background: #15171c;
  padding: 25px;
  margin-top: 20px;

  .currency-icon {
    width: 26px;
    height: auto;
  }

  .rows {
    padding-bottom: 18px;
    display: flex;
    justify-content: space-between;
    align-items: center;

    .value {
      font-size: 20px;
      font-weight: 700;
      display: flex;
      align-items: center;
    }

    img {
      margin-left: 10px;
    }

    &:not(:first-child) {
      padding-bottom: 0;
      padding-top: 18px;
      border-top: 1px solid #404248;
    }
  }
`;

const ContentCountdown = styled.div`
  margin-top: 20px;
  margin-bottom: 20px;

  .label-expiration {
    color: #fff;
    font-size: 16px;
  }

  .title {
    display: flex;
    justify-content: flex-start;
    align-items: center;
    margin-bottom: 15px;

    > img {
      width: 24px;
      height: 24px;
      margin-right: 8px;
    }

    .text {
      font-size: 16px;
      font-weight: 500;
      color: #A8AEBA;
    }
  }

  >div >.customer-countdown {
    width: 370px;
  }
`;

const ContentInput = styled.div`
  width: 100%;
  margin-top: 25px;
  font-family: sans-serif;

  .currency-icon {
    width: 26px;
    height: auto;
  }

  .label {
    color: #a8aeba;
    font-size: 16px;
  }

  .label-note {
    color: #045afc;
    font-size: 14px;
  }

  .input-custom {
    border-radius: 10px;
    border: 1px solid #65676b;
    background: #1b1e24;
    margin-top: 10px;

    input {
      background: transparent;
      border: none;
      outline: none;
      width: 100%;
      text-align: center;
      color: #fff;
      font-size: 20px;
      padding: 12px;
      font-weight: bold;

      &::-webkit-outer-spin-button,
      &::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
      }

      &::placeholder {
        color: #a8aeba;
      }
    }
  }

  .total {
    color: #fff;
    display: flex;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    margin-top: 12px;

    .text {
      font-size: 16px;
    }

    .value {
      font-size: 20px;
      font-weight: 700;
      display: flex;
      align-items: center;
    }

    img {
      margin-left: 10px;
    }
  }

  .note {
    color: #a8aeba;
    text-align: center;
    font-size: 16px;
    width: 68%;
    margin: 0px auto;
    margin-top: 10px;
  }

  ${Media.lessThan(Media.SIZE.SM)} {
    .note {
      width: 100%;
    }
  }
`;

const ContentButton = styled.div`
  margin-top: 42px;
  display: flex;
  justify-content: center;

  .btn-cancel {
    height: auto;
    cursor: pointer;
    display: inline-flex;
    border-radius: 9999px;
    background: #045afc;
    padding: 13px 50px;

    color: #ffffff;
    text-align: center;
    font-size: 16px;
    font-weight: 500;
    line-height: 24px;

    &.danger {
      background-color: #9b040d;
    }
  }
`;

export default inject(stores => ({
  authStore: stores.auth,
  lendingStore: stores.lending,
}))(
  observer(({ authStore, ...props }) => {
    const { i18n } = useTranslation();

    return <BorrowDetails {...props} key={`${i18n.language}-${authStore.loggedIn}`} authStore={authStore} i18n={i18n} />;
  }),
);
