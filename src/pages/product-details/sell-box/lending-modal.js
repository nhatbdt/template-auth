import { Component } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import { inject, observer } from 'mobx-react';
import { Translation } from 'react-i18next';
import BigNumber from 'bignumber.js';

import Modal from '../../../components/modal';
import Clickable from '../../../components/clickable';
import Button from '../../../components/button';
import Checkbox from '../../../components/checkbox';
import Field from '../../../components/field';
import Input from '../../../components/input';
import Confirmable from '../../../components/confirmable';
import Typography from '../../../components/typography';
import { Colors, Images } from '../../../theme';
import Format from '../../../utils/format';
import { onSign, onTransfer, onError } from '../utils/checkout';
import { SellModalStyled } from './styled';
import MaskLoading from '../../../components/mask-loading';
import ProductInputAmount from '../../../components/product-input-amount';
import { CHAIN_LIST } from '../../../constants/chains';
import { LENDING_TITLE } from '../../../constants/statuses';
import regex from '../../../utils/regex';
import {
  getWeb3Instance,
  approveTokenByErc721,
  checkCorrespondingNetwork,
  checkApprovedContract,
} from '../../../utils/web3';
import { MAX_LENGTH_INPUT_VALUE_FRACTION, MAX_LENGTH_INPUT_VALUE_INTEGER } from '../../../constants/common';

@inject(stores => ({
  authStore: stores.auth,
  productsStore: stores.products,
  lendingStore: stores.lending,
  product: stores.products.currentProductDetails,
}))
@observer
class LendingModal extends Component {
  static propTypes = {
    product: PropTypes.object,
    authStore: PropTypes.object,
    lendingStore: PropTypes.object,
    productsStore: PropTypes.object,
  };

  state = {
    isOpen: false,
    loading: false,
    initing: true,
    currentTab: 'NORMAL',
    registerSellData: null,
  };

  open = async () => {
    const { lendingStore, product } = this.props
    const { data } = await lendingStore.lendingPriceAvg({
      nftType: product?.nftType,
      nftSubType: product?.nftSubType,
      chainId: product?.chainId,
    })

    this.setState({
      isOpen: true,
      currentTab: 'NORMAL',
      initing: false,
      priceAvg: data
    });
  };

  close = () => this.setState({ isOpen: false });

  _onSubmit = async (values, t) => {
    const { lendingStore, productsStore, product, authStore, history } = this.props;
    const paymentAddress = CHAIN_LIST[product?.chainId]?.lendingContractAddress;
    const { publicAddress } = authStore.initialData;

    const currencies = CHAIN_LIST[product?.chainId]?.erc20Token?.map(item => item.symbol);
    const indexCurrency = currencies.findIndex((x) => x === values.currency)
    const currencyAddress = CHAIN_LIST[product?.chainId]?.erc20Token?.[indexCurrency]?.address;
    const decimals = CHAIN_LIST[product?.chainId]?.erc20Token?.[indexCurrency]?.decimals;
    const gasLimit = 300000;
    const params = {
      ...values,
      productId: product.id,
      pricePerDay: values.offerPrice
    }
    this.setState({
      loading: true,
    });

    MaskLoading.open({});
    try {
      const { lendingContract } = await getWeb3Instance();
      await onSign(authStore);
      await checkCorrespondingNetwork(product?.chainId);

      const checkApprove = await checkApprovedContract(product.tokenId, paymentAddress);
      if (!checkApprove) {
        await approveTokenByErc721(publicAddress, paymentAddress, product.tokenId, t);
      }

      const { success, data } = await lendingStore.registerLendingProduct(params)

      if (success && lendingContract) {
        const dataRequestLending = lendingContract.methods.lend(
          data.tokenId, //_tokenId (uint256)
          currencyAddress, //_paymentCurrency (address)
          new BigNumber(data.paymentPrice).multipliedBy(new BigNumber(10).pow(decimals)).toString(10), //_paymentPrice (uint256)
          data.maxRentalDuration,//_maxRentalDuration (uint256)
          data.offerDuration,//_offerDuration (uint256)
          data.nonce,//nonce (uint256)
          data.time,//time (uint256)
          data.signature,//signature (bytes)
        )

        const transactionHash = await onTransfer(
          publicAddress,
          t,
          dataRequestLending.encodeABI(), //endCode
          gasLimit,
          data.paymentCurrency,
          null,
          'lending'
        );

        if (transactionHash) {
          await lendingStore.transactionLendingProduct({
            productId: product.id,
            title: LENDING_TITLE.LENDING,
            txHash: transactionHash,
          })

          await productsStore.getProductDetails({
            id: product.id,
          })

          MaskLoading.close();
          this.setState({
            loading: false,
            isOpen: false,
          });
          Confirmable.open({
            content: t('product_details:messages.create_lending_success'),
            hideCancelButton: true,
          });
        }
      }

      history.push(`/product-details/${product.id}`);
    } catch (error) {
      // eslint-disable-next-line no-console
      console.error(error);
      MaskLoading.close();
      if (error.error) {
        return Confirmable.open({
          content: t(`validation_messages:${error.error}`),
          hideCancelButton: true,
          onOk: () => history.push(`/product-details/${product.id}`)
        });
      }

      if (error?.code !== 4001 && !error?.message?.includes('MetaMask Personal Message')) {
        onError(error, t);
      }
    } finally {
      MaskLoading.close();
      this.setState({
        loading: false,
        isOpen: false,
      });
    }
  };

  _validate = (values, t) => {
    let errors = {};

    if (+values.offerPrice < 0.001) {
      errors.offerPrice = t('MIN_PRICE_THAN_MORE_EQUAL_0001')
    } else if (!isNaN(+values.offerPrice) && values.offerPrice) {
      const sOfferPrice = values.offerPrice?.toString();
      let symbol = null;
      if (sOfferPrice.includes('.')) {
        symbol = '.';
      } else if (sOfferPrice.includes(',')) {
        symbol = ',';
      }

      if (!symbol) {
        if (sOfferPrice?.length > MAX_LENGTH_INPUT_VALUE_INTEGER) {
          errors.offerPrice = t('MAX_LENGTH_PRICE')
        }
      } else {
        const arr = sOfferPrice.split(symbol);
        if (arr[0]?.length > MAX_LENGTH_INPUT_VALUE_INTEGER || arr[1]?.length > MAX_LENGTH_INPUT_VALUE_FRACTION) {
          errors.offerPrice = t('MAX_LENGTH_PRICE')
        }
      }
    }

    if (+values.rentalDuration <= 0) {
      errors.rentalDuration = t('MIN_DAY_THAN_MORE_0')
    }

    if (+values.offerExpiration <= 0) {
      errors.offerExpiration = t('MIN_DAY_THAN_MORE_0')
    }
    return errors;
  };

  _onChangeDays = (e, form, fieldName) => {
    const { value } = e.target
    const MAX_DAYS = 365

    if (!regex.integer.test((value)) && value !== '') {
      return
    } else {
      if (Number(value) > MAX_DAYS) {
        form.setFieldValue(fieldName, MAX_DAYS)
      } else form.setFieldValue(fieldName, value)
    }
  }

  _renderForm = ({ handleSubmit, values, errors, ...form }, t) => {
    const { product } = this.props
    const { loading, priceAvg } = this.state;
    const currencies = CHAIN_LIST[product?.chainId]?.erc20Token?.map(item => item.symbol);
    const selectedCurrency = currencies.find((x) => x === values.currency)
    const numFixed = '0,0.000000';

    const INFO_DATA = [
      {
        name: t('product_details:lending.price'),
        value: `${Format.price(values.offerPrice || 0, numFixed)}`,
      },
      {
        name: t('product_details:lending.currency'),
        value: values.currency,
      },
      {
        name: t('product_details:lending.loan_duration'),
        value: `${values.rentalDuration} ${t('product_details:lending.days')}`,
      },
      {
        name: t('product_details:lending.offer_expiration'),
        value: `${values.offerExpiration} ${t('product_details:lending.days')}`,
      },
    ];

    return (
      <Form>
        <div className="modal-header modal-header-lending">
          <Typography bold>{t('product_details:lending.lending')}</Typography>
        </div>
        <div className="content content-lending">
          <div className="content-side">
            <div className="label">{t('product_details:lending.label_1')}</div>

            <div className='box-price-avg'>
              <div className='label'>{t('product_details:all_average_price')}</div>
              <div className='value'>{priceAvg ?? '-'} {selectedCurrency}</div>
            </div>
            <ProductInputAmount
              className="input-price"
              titleInput={t('product_details:lending.label_2')}
              currencies={currencies}
              selectedCurrency={selectedCurrency}
              values={values}
              errors={errors}
              onSelect={(value) => form.setFieldValue('currency', value)}
            />

            <div className="label mt-18">{t('product_details:lending.label_3')}</div>
            <Field
              className="dark-input select-tag"
              name="rentalDuration"
              suffix={t('product_details:lending.days')}
              component={Input}
              placeholder={0}
              onChange={(e) => this._onChangeDays(e, form, 'rentalDuration')}
            />

            <div className="label mt-18 mb-5">{t('product_details:lending.label_4')}</div>
            <Field
              showError={false}
              className="dark-input select-tag"
              name="offerExpiration"
              suffix={t('product_details:lending.days')}
              component={Input}
              placeholder={0}
              onChange={(e) => this._onChangeDays(e, form, 'offerExpiration')}
            />
          </div>

          <div className="divider">
            <div className="line" />
            <div className="transfer-icon">
              <img src={Images.BLUE_TRANSFER_ICON} alt="" />
            </div>
          </div>

          <div className="content-side content-side-2">
            <div className="top-box">
              <div className="label-box">
                <Typography size="big" bold>
                  {t('product_details:sell.content')}
                </Typography>
              </div>

              {INFO_DATA.map((item, index) => (
                <div key={index}>
                  {item.line && <div className="data-field-divider" />}
                  <div className="text-field" key={index}>
                    <Typography className="_name" size="large">
                      {item.name}
                    </Typography>
                    <Typography className="_value" size="large">
                      {item.value}
                    </Typography>
                  </div>
                </div>
              ))}
            </div>
            <div className="bottom-box">
              <Field
                showError={false}
                className="accept-terms-checkbox accept-terms-checkbox-lending"
                name="acceptTerms"
                component={Checkbox}
              >
                {t('product_details:lending.accept_term')}
              </Field>
              <div className="action-box">
                <Button
                  size="large"
                  disabled={!values.acceptTerms}
                  onClick={handleSubmit}
                  background={Colors.BLUE_4}
                  loading={loading}
                >
                  {t('product_details:lending.btn_create')}
                </Button>
                <Clickable className="cancel-button" onClick={this.close}>
                  <Typography primary bold size="big">
                    {t('product_details:lending.btn_cancel')}
                  </Typography>
                </Clickable>
              </div>
            </div>
          </div>
        </div>
      </Form>
    );
  };

  render() {
    const { isOpen, loading } = this.state;
    const { product } = this.props
    const currencies = CHAIN_LIST[product?.chainId]?.erc20Token?.map(item => item.symbol);

    return (
      <Modal
        open={isOpen}
        onCancel={this.close}
        destroyOnClose
        maskClosable={!loading}
        // closable={!loading}
        closable={true}
        width={900}
        padding={0}
      >
        <SellModalStyled>
          <Translation>
            {t => (
              <Formik
                enableReinitialize
                validateOnChange
                validateOnBlur
                initialValues={{
                  acceptTerms: false,
                  currency: currencies[0],
                  offerPrice: '',
                  rentalDuration: '7',
                  offerExpiration: '1',
                }}
                validate={values => this._validate(values, t)}
                validationSchema={yup.object().shape({
                  offerPrice: yup.number().nullable().required(t('PRICE_REQUIRED')),
                  rentalDuration: yup.number().nullable().required(t('RENTAL_DURATION_REQUIRED')),
                  offerExpiration: yup.number().nullable().required(t('OFFER_EXPIRATION_REQUIRED')),
                })}
                onSubmit={values => this._onSubmit(values, t)}
                component={data => this._renderForm(data, t)}
              />
            )}
          </Translation>
        </SellModalStyled>
      </Modal>
    );
  }
}

export default LendingModal;
