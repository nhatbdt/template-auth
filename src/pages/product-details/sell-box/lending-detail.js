import { Component } from 'react';
import { inject, observer } from 'mobx-react';
import { withTranslation, useTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';
import { Divider, Tooltip } from 'antd';
import numeral from 'numeral';
import styled from 'styled-components';
import moment from 'moment';
import Format from '../../../utils/format';
import { getRateByCurrency } from '../../../utils/rates';
import CURRENCIES from '../../../constants/currencies';
import Media from '../../../utils/media';

@withRouter
@withTranslation('product_details')
@observer
class LendingDetails extends Component {
  render() {
    const { t, i18n, product, authStore } = this.props;
    const masterData = authStore.initialData;
    const totalValue = product?.borrowDetailDTO?.pricePerDay * (product?.lendingDetailDTO?.borrowDuration || 0)
    return (
      <>
        <Divider className="line" />

        <Content>
          <div className='box-values'>
            <div className='total-price'>
              <p className='title'>{t('product_details:borrow.total_price')}</p>
              <p className='value'>{numeral(totalValue).format('0,0.000')}<span className='currency'> {product.currency}</span></p>
              <p className='sub-value'>{
                i18n.language.toUpperCase() === 'JA'
                  ? `${Format.price(totalValue * getRateByCurrency(CURRENCIES[product?.currency], masterData))} JPY`
                  : `${Format.price(
                    (totalValue * getRateByCurrency(CURRENCIES[product?.currency], masterData)) /
                    +getRateByCurrency(CURRENCIES.USD, masterData),
                  )} ${CURRENCIES.USD}`
              }</p>
            </div>

            <div className='info-borrow'>
              <div className='item'>
                <p className='label'>{t('product_details:lending.price_per_day')}</p>
                <p className='value'>{numeral(product?.borrowDetailDTO?.pricePerDay).format('0,0.000')} {product.currency}</p>
              </div>
              <div className='item'>
                <p className='label'>{t('product_details:lending.rental_day')}</p>
                <p className='value'>{product?.borrowDetailDTO?.borrowDuration || 0} {t('product_details:lending.days')}</p>
              </div>
              <div className='item'>
                <p className='label'>{t('product_details:lending.rental_duration')}</p>
                <p className='value'>{product?.lendingDetailDTO?.rentalDuration || 0} {t('product_details:lending.days')}</p>
              </div>
              <div className='item'>
                <p className='label'>{t('product_details:lending.offer_expiration')}</p>
                <p className='value'>{product?.lendingDetailDTO?.offerExpiration || 0} {t('product_details:lending.days')}</p>
              </div>
            </div>
          </div>

          <Divider className="line" />

          <div className='box-values-bottom'>
            <div className='borrower-info'>
              <p className='title'>{t('product_details:lending.borrower')}</p>
              <Tooltip
                title= {product?.borrowDetailDTO?.userName}
                color="black"
                trigger="hover"
              >        
                <p className='value'>{product?.borrowDetailDTO?.userName}</p>
              </Tooltip>
            </div>

            <div className='box-times'>
              <div className='time'>
                <div className='label'>{t('product_details:bid.start_time')} :</div>
                <div className='value'>{moment(product?.borrowDetailDTO?.borrowDurationStartTime).format('YYYY/MM/DD HH:mm:ss')}</div>
              </div>

              <div className='time'>
                <div className='label'>{t('product_details:bid.end_time')} :</div>
                <div className='value'>{moment(product?.borrowDetailDTO?.borrowDurationEndTime).format('YYYY/MM/DD HH:mm:ss')}</div>
              </div>
            </div>
          </div>

        </Content>
      </>
    );
  }
}

const Content = styled.div`
  width: 100%;
  border-radius: 10px;
  color: #fff;
  font-weight: 400;
  font-size: 16px;
  background: #15171c;
  padding: 15px 25px;
  margin-top: 20px;

  p {
    margin: 0;
  }

  .currency-icon {
    width: 26px;
    height: auto;
  }

  .box-values {
    .total-price {
      margin-bottom: 15px;

      .title {
        font-size: 16px;
      }

      .value {
        font-size: 26px;
        color: #1877F2;
        line-height: 1.4;
      }

      .currency {
        font-size: 16px;
      }

      .sub-value {
        font-size: 16px;
        color: #A8AEBA;
      }
    }

    .info-borrow {
      display: flex;
      justify-content: space-between;
      align-items: flex-start;

      .item {
        .label {
          font-size: 14px;
        }

        .value {
          font-size: 16px;
          color: #A8AEBA;
        }
      }
    }
  }

  .box-values-bottom {
    display: flex;
    justify-content: space-between;
    align-items: flex-start;

    .borrower-info {
      .title {
        font-size: 14px;
      }

      .value {
        font-size: 20px;
        color: #045AFC;
        white-space: nowrap;
        width: 250px;
        overflow: hidden;
        text-overflow: ellipsis;
      }

      ${Media.lessThan(Media.SIZE.MD)} {
        .value {
          width: 150px;
        }
      }
    }

    .box-times {
      .time {
        display: flex;
        justify-content: flex-end;
        align-items: flex-end;

        .label {
          width: 75px;
          font-size: 14px;
          margin-right: 15px;
        }

        .value {
          font-size: 16px;
          color: #A8AEBA;
        }
      }
    }
  }
`;

export default inject(stores => ({
  authStore: stores.auth,
}))(
  observer(({ authStore, ...props }) => {
    const { i18n } = useTranslation();

    return <LendingDetails {...props} key={`${i18n.language}-${authStore.loggedIn}`} i18n={i18n} authStore={authStore} />;
  }),
);
