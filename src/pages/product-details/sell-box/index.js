import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withTranslation } from 'react-i18next';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';

import Button from '../../../components/button';
import Typography from '../../../components/typography';
import Confirmable from '../../../components/confirmable';
import Toast from '../../../components/toast';
import SellErc721Modal from './sell-modal';
import OfferApprovel from '../offer-box/offer-approvel';
import SellErc1155Modal from './sell-1155-modal';
import { onSign } from '../utils/checkout';
import { Colors } from '../../../theme';
import { checkCorrespondingNetwork } from '../../../utils/web3';
import MaskLoading from '../../../components/mask-loading';
import LendingModal from './lending-modal';
import { LENDING_STATUS, STAKING_STATUS } from '../../../constants/statuses'
import { checkProcessing } from '../utils/func'

const SellBoxStyled = styled.div`
  margin-top: 30px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
  gap: 4px 10px;

  .resell-status {
    flex: 1;
    text-align: center;
    margin-bottom: 16px;
    min-width: calc(50% - 5px);
    /* margin-right: 10px; */
  }

  .sell-button {
    flex: 1;
    padding: 0;
    min-height: 45px;
    /* min-width: 50%; */
    /* background-color: ${Colors.BLUE_4}; */
    background-color: #045afc;
    border-radius: 9999px;
    color: #fff;

    &.sell-btn-1155 {
      margin-top: 20px;
      width: 100%;
    }
  }

  .offer-status-box {
    flex: 1;

    button {
      background-color: #045afc;
      border-radius: 9999px;
      color: #fff;
    }
  }

  @media screen and (max-width: 768px) {
    flex-direction: column;
    > div, > button {
      flex: 1;
      width: 100%;
      padding: 0;
      margin-bottom: 10px;
    }
  }
`;

@withRouter
@withTranslation('product_details')
@inject(stores => ({
  product: stores.products.currentProductDetails,
  authStore: stores.auth,
  sellStore: stores.sell,
}))
@observer
class SellBox extends Component {
  static propTypes = {
    product: PropTypes.object,
    sellStore: PropTypes.object,
    authStore: PropTypes.object,
  };

  state = {
    loading: false,
  };

  componentDidMount() {
    const { match, product } = this.props;
    const { sellAction } = match.params;
    const { productBuying, resellCancelable } = product.productResell;

    if (sellAction === 'sell' && !resellCancelable && !productBuying) {
      setTimeout(() => {
        this._sellModal.open();
      }, 300);
    }

    if (sellAction === 'sell-1155' && !resellCancelable && !productBuying) {
      setTimeout(() => {
        this._sell1155Modal.open();
      }, 300);
    }

    if (sellAction === 'cancel' && resellCancelable && !productBuying) {
      setTimeout(() => {
        this._onCancelSell();
      }, 300);
    }

    if (sellAction === 'cancel-1155') {
      setTimeout(() => {
        this._onCancelResell1155();
      }, 300);
    }

    if (sellAction === 'create-lending') {
      setTimeout(() => {
        this._lendingModal.open();
      }, 300);
    }
  }

  _onCancelSell = async () => {
    const { sellStore, product, authStore, t } = this.props;

    const ok = await Confirmable.open({
      content: t('sell.are_you_sure_to_cancel_listing'),
      cancelButtonText: t('common:close_modal'),
      acceptButtonText: t('common:ok'),
    });

    if (!ok) return null;
    MaskLoading.open({});

    try {
      this.setState({
        loading: true,
      });

      const signature = await onSign(authStore);
      await checkCorrespondingNetwork(product?.chainId);

      const { success } = await sellStore.cancelSell({
        signature,
        productId: product.id,
        resellType: 'NORMAL',
      });

      if (success) {
        Confirmable.open({
          content: t('sell.cancel_sell_success'),
          hideCancelButton: true,
        });

        product.setData({
          status: 'SOLD',
          productBuying: false,
        });

        product.productResell.setData({
          resellCancelable: false,
          confirmResell: false,
          reselling: false,
        });
      }
    } catch (e) {
      MaskLoading.close();
      if (e?.message?.includes('MetaMask Personal Message Signature') || [-32603, 4001].includes(e?.code)) return;

      Confirmable.open({
        content: t(`validation_messages:${e.message || e?.error || 'SOMETHING_WENT_WRONG'}`),
        hideCancelButton: true,
      });
      // Toast.error(t('sell.cancel_sell_failed'));
    } finally {
      MaskLoading.close();
      this.setState({
        loading: false,
      });
    }

    return null;
  };

  _onCancelResell1155 = async () => {
    const { t, sellStore, authStore, product } = this.props;

    const ok = await Confirmable.open({
      content: t('sell.are_you_sure_to_cancel_listing'),
      cancelButtonText: t('common:close_modal'),
      acceptButtonText: t('common:ok'),
    });

    if (!ok) return null;

    try {
      const signature = await onSign(authStore);

      const { success } = await sellStore.cancelSell1155({
        signature,
        productId: product.id,
      });

      if (success) {
        Confirmable.open({
          content: t('sell.cancel_sell_success'),
          hideCancelButton: true,
        });

        product.setData({
          canResellERC1155: true,
        });
      }
    } catch (e) {
      Toast.error(t('sell.cancel_sell_failed'));
    }
  };

  render() {
    const { product, t, match, history } = this.props;
    const { loading } = this.state;
    const isSellButtonActive = product.isSellValid;
    const { confirmResell, productBuying, resellCancelable, reselling } = product.productResell;
    const isErc721 = product?.tokenStandard === 'ERC721';
    const isErc1155 = product?.tokenStandard === 'ERC1155';
    const isBuying = checkProcessing(product)

    const { sellAction } = match.params;

    const isNotLendingBorrow = sellAction !== 'lending' && sellAction !== 'borrow';

    const isStatusLending = [
      LENDING_STATUS.WAIT_LENDING,
      LENDING_STATUS.IN_LENDING,
      LENDING_STATUS.LENDING,
      LENDING_STATUS.WAIT_BORROW,
      LENDING_STATUS.BORROW,
      LENDING_STATUS.WAIT_CANCEL,
      LENDING_STATUS.EXPIRATION,
      LENDING_STATUS.WAIT_WITHDRAW
    ].includes(product?.lendingDetailDTO?.status)

    const isStatusStaking = [
      STAKING_STATUS.UN_STAKING_PROGRESS,
      STAKING_STATUS.STAKING_PROGRESS,
      STAKING_STATUS.STAKING,
    ].includes(product?.stakingStatus);

    return !isStatusStaking && (
      <SellBoxStyled>
        {isErc721 && !(confirmResell || productBuying) && isNotLendingBorrow && <OfferApprovel />}
        {product.offerNftTransactionStatus && !productBuying && (confirmResell || productBuying) && (
          <div className="status-divider" />
        )}
        {(confirmResell || productBuying) && isErc721 && isNotLendingBorrow && (
          <Typography primary className="resell-status">
            {confirmResell ? t('sell.resel_is_confirming') : productBuying ? t('sell.resel_is_buying') : ''}
          </Typography>
        )}
        {isErc721 && isNotLendingBorrow && (
          <>
            {resellCancelable && !productBuying ? (
              <Button loading={loading} className="sell-button" onClick={() => this._onCancelSell()}>
                {t('sell.cancel_listing')}
              </Button>
            ) : (
              !productBuying && (
                <Button
                  disabled={!isSellButtonActive || reselling || !!isStatusLending}
                  className="sell-button"
                  onClick={() => this._sellModal.open()}
                  background={Colors.BLUE_4}
                >
                  {t('sell.resell')}
                </Button>
              )
            )}
            <SellErc721Modal
              ref={ref => {
                this._sellModal = ref;
              }}
              history={history}
            />
          </>
        )}

        {!confirmResell && !productBuying && product?.lendingDetailDTO?.status !== LENDING_STATUS.BORROW && !isBuying && (
          <>
            <Button
              disabled={!isSellButtonActive || reselling || !!isStatusLending}
              className="sell-button"
              onClick={() => this._lendingModal.open()}
              background={Colors.BLUE_4}
            >
              {t('product_details:lending.lending')}
            </Button>
          </>
        )}

        <LendingModal
          ref={ref => {
            this._lendingModal = ref;
          }}
          product={product}
          history={history}
        />

        {isErc1155 && product.holdingQuantity > 0 && product.canResellERC1155 && !isBuying && (
          <>
            <Button
              disabled={!isSellButtonActive}
              className="sell-button sell-btn-1155"
              onClick={() => this._sell1155Modal.open()}
              background={Colors.BLUE_4}
            >
              {t('sell.resell')}
            </Button>
            <SellErc1155Modal
              ref={ref => {
                this._sell1155Modal = ref;
              }}
            />
          </>
        )}
      </SellBoxStyled>
    );
  }
}

export default SellBox;
