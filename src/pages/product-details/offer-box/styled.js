import styled from 'styled-components';
import Media from '../../../utils/media';
import { Colors } from '../../../theme';

const StyledDiv = styled.div`
  .loading {
    height: 550px;
  }
  .action-box-btn {
    color: white;
    background-color: #045afc;
    width: 100%;
    border-radius: 9999px;
  }
  .ant-checkbox-checked .ant-checkbox-inner {
    border-color: #045afc;
    background-color: #045afc !important;
  }
  .ant-checkbox-checked::after {
    border: 1px solid #045afc;
  }

  .modal-header {
    height: 60px;
    display: flex;
    align-items: center;
    padding: 0 40px;

    p {
      font-size: 20px;
      color: #fff;
    }
  }

  .content {
    background-color: inherit;
    display: flex;
    padding: 10px 40px 40px;
    ${Media.lessThan(Media.SIZE.MD)} {
      flex-direction: column;
      padding: 10px 35px 30px;
    }
    ${Media.lessThan(Media.SIZE.SM)} {
      padding: 15px;
    }

    .box-price-avg {
      display: flex;
      justify-content: space-between;
      align-items: center;
      margin-bottom: 18px;
      margin-top: 10px;

      .value {
        color: #045afc;
      }
    }
    
    .label-box {
      display: flex;
      align-items: center;

      &.space {
        justify-content: space-between;
      }

      ._left {
        display: flex;
        align-items: center;
        .typography {
          color: #fff;
          font-weight: 500;
          font-size: 16px;
        }
      }

      .typography {
        &.characters {
          font-weight: 400;
          font-size: 14px;
          color: ${Colors.TEXT};
        }
        span {
          color: #e55509;
          font-weight: bold;
        }
      }

      .hint {
        margin-left: 10px;
        border: solid 1px #045afc;
        width: 18px;
        height: 18px;
        border-radius: 9px;
        display: flex;
        justify-content: center;
        align-items: center;
        color: #045afc;
        font-size: 15px;
      }

      > p {
        color: #fff;
        font-size: 16px;
      }
    }

    .content-side {
      flex: 1;
      /* min-width: 50%; */
      min-width: 260px;
      .label-box {
        .typography {
          &.confirm-content {
            color: #fff;
            font-size: 16px;
          }
        }
      }

      &:first-child {
        flex: 1.3;

        .normal-panel {
          .available-currencies {
            display: flex;
            align-items: center;
            margin-top: 13px;
            margin-bottom: 34px;

            .typography {
              margin-right: 22px;
              font-weight: 400;
              font-size: 14px;
              color: ${Colors.TEXT};
            }

            .available-currency-item {
              margin-right: 10px;
              height: 24px;
              border-radius: 6px;
              background-color: #0a3993;
              padding: 0 10px;
              display: flex;
              align-items: center;
              font-size: 16px;
              color: #fff;

              &:last-child {
                margin-right: 0;
              }
            }
          }

          .message-text-area {
            margin-top: 10px;

            > div {
              textarea {
                background: #1b1e24;
                border: 1px solid #65676b;
                border-radius: 10px;
                padding: 10px;
                box-shadow: none !important;
                min-height: 100px;
                font-weight: 500;
                font-size: 12px;
                color: ${Colors.TEXT};
                &::placeholder {
                  color: ${Colors.PLACEHOLDER};
                }
              }
            }
          }

          .field-box {
            max-width: 100%;

            ._input-outter {
              .input-value {
                .simple.input {
                  color: #282828;
                  border: 1px solid #dedede;
                }
              }
            }

            .input-unit-box p {
              color: #fff;
              font-size: 18px;
            }

            .input-wrapper ._input-outter .input-box input {
              font-size: 18px;
            }

            .input-wrapper ._input-outter .number-box p {
              font-size: 10px;
            }
          }

          .input-price {
            .field-box {
              width: 100%;
              max-width: 100%;
            }
            
            .field-box .input-price-wrapper ._input-outter .input-box input {
              opacity: 1;
            }

            .field-box .input-price-wrapper ._input-outter .input-box .ant-input-number {
              height: auto;
            }
          }
          /* ${Media.lessThan(Media.SIZE.MD)} {
            .field-box {
              .input-wrapper {
                flex-direction: column;
                height: auto;

                .custom-dropdown {
                  background-color: red;
                }

                .price_input {
                  display: flex;
                  justify-content: center;
                  padding-top: 5px;

                  .input-unit-box {
                    padding: 0;

                    .nbng-icon {
                      width: 30px;
                      height: 30px;

                      img {
                        width: 21px;
                      }
                    }
                  }
                }

                ._input-outter {
                  .input-box {
                    height: 100%;
                    input {
                      padding-top: 0;
                      padding: 0 15px;
                      color: #282828;
                    }
                  }

                  .number-box {
                    flex-direction: column;
                    align-items: flex-end;
                    padding-right: 15px;

                    .number-box-divider {
                      display: none;
                    }
                  }
                }
              }
            }
          } */
        }
      }

      &:last-child {
        .top-box {
          /* margin-bottom: 49px; */
          margin-bottom: 135px;
          ${Media.lessThan(Media.SIZE.MD)} {
            margin-bottom: 20px;
          }

          .label-box {
            margin-bottom: 27px;
            ${Media.lessThan(Media.SIZE.MD)} {
              margin-bottom: 20px;
            }
          }

          .data-field-divider {
            height: 1px;
            background: linear-gradient(to right, #b0b0b0 30%, rgba(255, 255, 255, 0) 0%);
            background-size: 6px 3px;
            margin-bottom: 10px;
          }

          .text-field {
            display: flex;
            margin-bottom: 0px;

            &:last-child {
              margin-bottom: 0;
            }

            ._name {
              width: 150px;
              color: #a8aeba;
              font-size: 14px;
            }

            ._value {
              flex: 1;
              text-align: left;
              color: #045AFC;
              font-size: 14px;
            }
            ${Media.lessThan(Media.SIZE.MD)} {
              .text-field {
                margin-bottom: 7px;

                ._name {
                  width: 130px;
                  font-size: 14px;
                }

                ._value {
                  font-size: 14px;
                }
              }
            }
          }
        }

        .hret-box {
          height: 169px;
          border-radius: 15px;
          background-color: #fff8f7;
          display: flex;
          justify-content: center;
          align-items: center;
          margin-top: 15px;
          margin-bottom: 40px;

          img {
            width: 40px;
            margin-right: 24px;
          }

          .typography {
            margin-right: 10px;
            color: ${Colors.PRIMARY};

            &:last-child {
              margin-right: 0;
            }
          }
        }

        .bottom-box {
          margin-top: 90px;

          .accept-terms-checkbox {
            span {
              /* color: #282828; */
              color: ${Colors.TEXT};
              &.ant-checkbox {
                /* border: 1px solid #282828; */
                border: 1px solid ${Colors.BORDER};
              }
            }
            .ant-checkbox-wrapper {
              > span {
                /* color: #282828; */
                color: ${Colors.TEXT};
                &:last-child {
                  font-size: 12px;
                  opacity: 0.5;
                }
              }
              .ant-checkbox-inner {
                /* border-color: #045AFC; */
                background-color: transparent;
              }
            }
          }

          .action-box {
            display: flex;
            align-items: center;
            margin-top: 33px;
            justify-content: center;
            ${Media.lessThan(Media.SIZE.MD)} {
              margin-top: 20px;
              flex-direction: column;
            }

            button {
              height: 50px;
              padding: 0 35px;
              font-size: 16px;
              ${Media.lessThan(Media.SIZE.MD)} {
                height: 50px;
                padding: 0 35px;
                font-size: 16px;
              }
            }

            .cancel-button {
              margin-left: 41px;
              height: 50px;
              padding: 0 35px;
              /* background-color: #ccc; */
              display: flex;
              border: 1px solid ${Colors.BORDER};
              border-radius: 9999px;
              .typography {
                margin: auto;
                font-size: 16px;
                /* color: #ffffff; */
                white-space: nowrap;
              }
              ${Media.lessThan(Media.SIZE.MD)} {
                margin-left: 0;
                margin-top: 20px;
                margin-bottom: 20px;

                .typography {
                  font-size: 16px;
                  /* color: #ffffff; */
                  color: #A8AEBA;
                  white-space: nowrap;
                }
              }
            }
          }
        }
      }
    }

    .divider {
      display: flex;
      align-items: center;
      justify-content: center;
      padding: 0 20px;
      ${Media.lessThan(Media.SIZE.MD)} {
        padding: 20px 0;
      }

      .line {
        width: 1px;
        height: 100%;
        background-color: #707070;
        opacity: 0.3;
        ${Media.lessThan(Media.SIZE.MD)} {
          height: 1px;
          width: 100%;
        }
      }

      .transfer-icon {
        background-color: inherit;
        position: absolute;
        padding-bottom: 6px;
        filter: none !important;
        ${Media.lessThan(Media.SIZE.MD)} {
          padding-bottom: 0;
          padding: 0 5px;
          margin-bottom: 5px;
        }

        img {
          width: 25px;
          ${Media.lessThan(Media.SIZE.MD)} {
            width: 22px;
          }
        }
      }
    }
  }
`;

export { StyledDiv };
