import { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Formik, Form } from 'formik';
import * as yup from 'yup';
import { inject, observer } from 'mobx-react';

import TextArea from '../../../components/text-area';
import Modal from '../../../components/modal';
import Clickable from '../../../components/clickable';
import Confirmable from '../../../components/confirmable';
import Loading from '../../../components/loading';
import Button from '../../../components/button';
// import HintTooltip from '../../../components/hint-tooltip';
import Checkbox from '../../../components/checkbox';
import Field from '../../../components/field';
import Typography from '../../../components/typography';
import { Colors, Images } from '../../../theme';
import Format from '../../../utils/format';
import CURRENCIES, { WRAPPED_CURRENCIES } from '../../../constants/currencies';
// import { getBalance, approveMax, allowancePrx, allowanceWeth, allowanceEth } from '../../../utils/web3';
import {
  // getBalance,
  // approveMax,
  // allowanceWeth,
  checkCorrespondingNetwork,
  getBalanceNew,
  approveMaxNew,
  getAllowanceByToken,
} from '../../../utils/web3';
import UniswapInterfaceModal from './uniswap-interface-modal';
import { onSign } from '../utils/checkout';
import { StyledDiv } from './styled';
import ProductInputAmount from '../../../components/product-input-amount';
import MaskLoading from '../../../components/mask-loading';
import { CHAIN_LIST } from '../../../constants/chains';
import { getRateByCurrency } from '../../../utils/rates';
import { MAX_LENGTH_INPUT_VALUE_FRACTION, MAX_LENGTH_INPUT_VALUE_INTEGER } from '../../../constants/common';

@inject(stores => ({
  productsStore: stores.products,
  offersStore: stores.offers,
  authStore: stores.auth,
  product: stores.products.currentProductDetails,
}))
@observer
class OfferModal extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
    authStore: PropTypes.object,
    product: PropTypes.object,
    offersStore: PropTypes.object,
  };

  state = {
    isOpen: false,
    loading: false,
    initing: true,
    selectedCurrency: null,
  };

  open = async () => {
    const { offersStore, product } = this.props;

    const { data } = await offersStore.offerPriceAvg({
      productId: product?.id
    })

    this.setState({
      isOpen: true,
      // selectedCurrency: CURRENCIES.ETH,
      selectedCurrency: CHAIN_LIST[product.chainId]?.erc20Token[0]?.symbol,
      initing: false,
      priceAvg: data
    });
  };

  close = () => this.setState({ isOpen: false });

  _checkBalance = async (offerPrice, t) => {
    const { authStore } = this.props;
    const { selectedCurrency } = this.state;

    let isEnoughBalance = false;
    // const { eth, prx, weth } = await getBalance(authStore.initialData.publicAddress);
    const {
      // native: nativeBalance,
      wNative: wNativeBalance,
      usdt: usdtBalance,
    } = await getBalanceNew(authStore.initialData.publicAddress);
    // eslint-disable-next-line no-console

    // if (selectedCurrency === CURRENCIES.PRX) {
    //   isEnoughBalance = +offerPrice <= prx;
    // }
    // if (selectedCurrency === CURRENCIES.ETH) {
    //   isEnoughBalance = +offerPrice <= weth;
    // }
    if (WRAPPED_CURRENCIES.includes(selectedCurrency)) {
      isEnoughBalance = +offerPrice <= wNativeBalance;
    } else {
      isEnoughBalance = +offerPrice <= usdtBalance;
    }
    // eslint-disable-next-line no-throw-literal
    if (!isEnoughBalance) throw { error: 'ERROR_BALANCE_NOT_ENOUGH' };
  };

  _onSubmit = async (values, t) => {
    MaskLoading.open({
      message: '',
    });
    // const { product, offersStore, authStore, signMessage } = this.props;
    const { history, product, offersStore, authStore } = this.props;
    const { selectedCurrency } = this.state;

    // this.setState({
    //   loading: true,
    // });

    try {
      const signature = await onSign(authStore);
      // const callSignature = hasConnectMetamask(onSign);
      // const signature = await callSignature(authStore);
      await checkCorrespondingNetwork(product?.chainId);

      // check balance
      await this._checkBalance(values?.offerPrice, t);

      const registerOfferData = await offersStore.registerOffer({
        productId: product.id,
        offerPrice: values.offerPrice,
      });

      if (!registerOfferData?.success) {
        // throw registerOfferData?.data;
        MaskLoading.close();
        return;
      }

      await this._checkBalance(values.offerPrice, t);

      const { publicAddress } = authStore.initialData;
      // const approvePublicAddress = process.env.REACT_APP_EXCHANGE_CONTRACT_ADDRESS;
      const approvePublicAddress = CHAIN_LIST[product?.chainId]?.exchangeContractAddress;

      let approveHash = null;

      // const allowance =
      //   selectedCurrency === CURRENCIES.PRX
      //     ? await allowancePrx(publicAddress, approvePublicAddress)
      //     : await allowanceWeth(publicAddress, approvePublicAddress);
      const allowance = await getAllowanceByToken(selectedCurrency, publicAddress, approvePublicAddress);

      if (allowance < +values.offerPrice) {
        // approveHash = await approveMax(publicAddress, t, selectedCurrency);
        approveHash = await approveMaxNew(publicAddress, t, selectedCurrency);
      }

      const { success, data } = await offersStore.createOffer({
        approveTokenTransactionHash: approveHash,
        offerMessage: values.message,
        offerPrice: values.offerPrice,
        offerId: registerOfferData.data.offerId,
        productId: product.id,
        currency: selectedCurrency,
        signature,
      });

      if (!success) {
        throw data;
      } else {
        await offersStore.getOffers({
          page: 1,
          productId: product.id,
        });
      }
      this.close();
      MaskLoading.close();

      Confirmable.open({
        content: t('product_details:offer.create_offer_success'),
        hideCancelButton: true,
      });

      history.push(`/product-details/${product.id}`)
    } catch (e) {
      MaskLoading.close();
      // this.close();
      if (e?.code === 4001 || e?.message?.includes('MetaMask Personal Message Signature')) return;
      if (
        (e.error === 'Your balance is insufficient.' ||
          e.error === 'ウォレットの残高が不足してます！' ||
          e.error === '您的钱包余额不足！') &&
        // selectedCurrency === 'ETH'
        WRAPPED_CURRENCIES.includes(selectedCurrency)
      ) {
        this.close();

        const ok = await Confirmable.open({
          content: t('validation_messages:ERROR_NATIVE_BALANCE_NOT_ENOUGH'),
          acceptButtonText: `Swap ${selectedCurrency}`,
          onOk: () => history.push(`/product-details/${product.id}`)
        });

        if (ok) {
          // const to = tokens.wrappedNative[product?.chainId]?.address;
          // this._uniswapInterfaceModal.open(values.bidPrice, selectedCurrency.replace('W', ''), to);
          this._uniswapInterfaceModal.open(values.bidPrice, selectedCurrency.replace('W', ''), selectedCurrency);
        }
      } else {
        if (e?.code != 4001) {
          Confirmable.open({
            content: t(`validation_messages:${e.error?.toUpperCase() || 'SOMETHING_WENT_WRONG'}`),
            hideCancelButton: true,
            onOk: () => history.push(`/product-details/${product.id}`)
          });
        }
      }
    } finally {
      MaskLoading.close();
    }

    this.setState({
      loading: false,
    });
  };

  _onSelectCurrency = currnecy => {
    this.setState({
      selectedCurrency: currnecy,
    });
  };

  _validate = (values) => {
    const { product } = this.props;
    let errors = {};

    if (+values.offerPrice <= 0) {
      errors = {
        offerPrice: 'MIN_PRICE_THAN_MORE_0',
      };
    } else if (+values.offerPrice < product?.minimumOfferPrice) {
      errors = {
        offerPrice: 'MINIMUM_PRICE_THAN_OR_EQUAL',
      };
    } else if (!isNaN(+values.offerPrice) && values.offerPrice) {
      const sOfferPrice = values.offerPrice?.toString();
      let symbol = null;
      if (sOfferPrice.includes('.')) {
        symbol = '.';
      } else if (sOfferPrice.includes(',')) {
        symbol = ',';
      }

      if (!symbol) {
        if (sOfferPrice?.length > MAX_LENGTH_INPUT_VALUE_INTEGER) {
          errors = {
            offerPrice: 'MAX_LENGTH_PRICE',
          };
        }
      } else {
        const arr = sOfferPrice.split(symbol);
        if (arr[0]?.length > MAX_LENGTH_INPUT_VALUE_INTEGER || arr[1]?.length > MAX_LENGTH_INPUT_VALUE_FRACTION) {
          errors = {
            offerPrice: 'MAX_LENGTH_PRICE',
          };
        }
      }
    }

    return errors;
  };

  _renderNumberBox = price => {
    const { authStore } = this.props;
    const { selectedCurrency } = this.state;
    // const { ethToJpy, prxToJpy, usdToJpy } = authStore.initialData;
    const masterData = authStore.initialData;
    const pricesJpy = getRateByCurrency(CURRENCIES[selectedCurrency], masterData) * price;

    return (
      <div className="number-box">
        <Typography poppins size="small">
          {Format.price(pricesJpy / getRateByCurrency(CURRENCIES.USD, masterData))} USD
        </Typography>
        {/* <div className="number-box-divider" /> */}
        <Typography poppins size="small">
          {Format.price(pricesJpy)} JPY
        </Typography>
      </div>
    );
  };

  _renderNormalPanel = (values, errors, t) => {
    const { selectedCurrency, priceAvg } = this.state;
    const { product } = this.props;
    const currencies = CHAIN_LIST[product?.chainId]?.erc20Token?.map(item => item.symbol);

    return (
      <div className="normal-panel">
        <div className='box-price-avg label-box space'>
          <div className="_left">
            <Typography size="big" bold>
              {t('product_details:all_average_price')}
            </Typography>
          </div>
          <div className='value'>{priceAvg ?? '-'} {selectedCurrency}</div>
        </div>
        <ProductInputAmount
          className="input-price"
          currencies={currencies}
          selectedCurrency={selectedCurrency}
          values={values}
          errors={errors}
          errorsValue={product?.minimumOfferPrice}
          onSelect={this._onSelectCurrency}
        />
        <div className="available-currencies">
          <Typography size="large">{t('product_details:offer.available_currencies')}</Typography>
          {currencies.map((item, index) => (
            <div className="available-currency-item" key={index}>
              {item}
            </div>
          ))}
        </div>
        <div className="label-box space">
          <div className="_left">
            <Typography size="big" bold>
              {t('product_details:offer.offer_message')}
            </Typography>
            {/* <HintTooltip content={t('product_details:offer.hint_2')} /> */}
          </div>
          <Typography size="nity" className="characters">
            {t('product_details:offer.characters')}：{values.message?.length || 0}
          </Typography>
        </div>
        <Field
          className="message-text-area"
          name="message"
          placeholder={t('product_details:offer.message_placeholder')}
          component={TextArea}
        />
      </div>
    );
  };

  _renderForm = ({ handleSubmit, values, errors }, t) => {
    const { loading, selectedCurrency } = this.state;

    const INFO_DATA = [
      {
        name: t('product_details:offer.price'),
        value: Format.price(values.offerPrice || 0),
      },
      {
        name: t('product_details:offer.currency'),
        value: selectedCurrency,
      },
    ];

    return (
      <Form>
        <div className="modal-header">
          <Typography bold>{t('product_details:offer.put_a_limit_price')}</Typography>
        </div>
        <div className="content">
          <div className="content-side">{this._renderNormalPanel(values, errors, t)}</div>
          <div className="divider">
            <div className="line" />
            <div className="transfer-icon">
              <img src={Images.BLUE_TRANSFER_ICON} alt="" />
            </div>
          </div>
          <div className="content-side">
            <div className="top-box">
              <div className="label-box">
                <Typography size="big" bold className="confirm-content">
                  {t('product_details:sell.content')}
                </Typography>
              </div>
              {INFO_DATA.map((item, index) => (
                <Fragment key={index}>
                  {item.line && <div className="data-field-divider" />}
                  <div className="text-field" key={index}>
                    <Typography className="_name" size="large">
                      {item.name}
                    </Typography>
                    <Typography className="_value" size="large">
                      {item.value}
                    </Typography>
                  </div>
                </Fragment>
              ))}
            </div>
            <div className="bottom-box">
              <Field showError={false} className="accept-terms-checkbox" name="acceptTerms" component={Checkbox}>
                {t('product_details:bid.accept')}
              </Field>
              <div className="action-box">
                <Button
                  className="action-box-btn"
                  disabled={!values.acceptTerms}
                  onClick={handleSubmit}
                  background={Colors.BLUE_4}
                  loading={loading}
                >
                  {t('product_details:offer.put_limit_with_content')}
                </Button>
                <Clickable className="cancel-button" onClick={this.close}>
                  <Typography bold size="big">
                    {t('product_details:sell.cancel')}
                  </Typography>
                </Clickable>
              </div>
            </div>
          </div>
        </div>
      </Form>
    );
  };

  render() {
    const { isOpen, loading, initing } = this.state;
    const { t } = this.props

    return (
      <>
        <Modal
          className="custom-modal"
          open={isOpen}
          onCancel={this.close}
          destroyOnClose
          maskClosable={!loading}
          closable={!loading}
          width={960}
          padding={0}
        >
          <StyledDiv>
            {initing ? (
              <Loading size="small" className="loading" />
            ) : (
              <Formik
                enableReinitialize
                // validateOnChange={false}
                validateOnBlur={false}
                initialValues={{
                  acceptTerms: false,
                }}
                validate={this._validate}
                validationSchema={yup.object().shape({
                  offerPrice: yup.number().nullable().required(t('PRICE_REQUIRED')),
                  acceptTerms: yup.bool().oneOf([true]),
                })}
                onSubmit={values => this._onSubmit(values, t)}
                component={data => this._renderForm(data, t)}
              />
            )}
          </StyledDiv>
        </Modal>
        <UniswapInterfaceModal
          ref={ref => {
            this._uniswapInterfaceModal = ref;
          }}
        />
      </>
    );
  }
}

export default OfferModal;
