import { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import moment from 'moment';
import { withTranslation } from 'react-i18next';
import classnames from 'classnames';

// import CURRENCIES from '../../../constants/currencies';
import Storage from '../../../utils/storage';
import Typography from '../../../components/typography';
import Thumbnail from '../../../components/thumbnail';
import NoDataBox from '../../../components/no-data-box';
import Clickable from '../../../components/clickable';
import { Images, Colors } from '../../../theme';
import MessageOfferModal from './offer-message-modal';
// import { allowancePrx, allowanceWeth, approveMax } from '../../../utils/web3';
import { onSign } from '../utils/checkout';

const OffersBoxStyled = styled.div`
  padding-top: 0px;
  display: flex;
  flex-direction: column;

  .title {
    color: #333333;
    font-size: 13px;
  }
  .user-name {
    color: #282828;
  }

  .table-outer {
    overflow-x: auto;
    overflow-y: hidden;
    width: 100%;
    table {
      width: 100%;
      min-width: 700px;
      margin-top: 20px;
      table-layout: fixed;

      thead {
        tr {
          th {
            padding: 0 8px;
            font-weight: 700;
            font-size: 14px;
            color: ${Colors.TEXT};
            text-align: left;
            &.create-at {
              width: 120px;
            }
            &.from {
              width: 220px;
            }
          }
        }
      }

      tbody {
        tr {
          height: 53px;
          border-bottom: 1px solid rgb(255 255 255 / 8%);

          td {
            padding: 0 8px;
            color: ${Colors.TEXT};
            .typography {
              color: ${Colors.TEXT};
            }
            .action-button {
              color: #fff;
              border-radius: 999px;
              background-color: #045afc;
              white-space: nowrap;
              display: flex;
              align-items: center;
              justify-content: center;
              font-weight: 500;
              width: fit-content;
              padding: 4px 10px;
              &.cancel-btn {
                background-color: #fd8718ba;
              }
            }

            .horizon {
              display: flex;
              align-items: center;

              .new-dot {
                width: 6px;
                height: 6px;
                border-radius: 3px;
                margin-right: 10px;

                &.show {
                  background-color: ${Colors.BLUE};
                }
              }

              .avatar {
                margin-right: 9px;
                min-width: 0;
              }

              .user-name {
                font-size: 13px;
                color: ${Colors.BLUE};
                overflow: hidden;
                text-overflow: ellipsis;
                max-width: 180px;
                white-space: nowrap;
                flex: 1;
                margin-right: 5px;
              }

              .fire-icon {
                margin-right: 4px;
              }

              .heat-number {
                color: ${Colors.PRIMARY};
              }
            }

            .cancel-button {
              height: 32px;
              font-size: 11px;
              padding: 0 15px;
            }
          }
        }
      }
    }

    .no-data-box {
      height: 100px;
      display: flex;
      align-items: center;
    }
  }
`;

@withTranslation('product_details')
@inject(stores => ({
  product: stores.products.currentProductDetails,
  authStore: stores.auth,
  offersStore: stores.offers,
}))
@observer
class OffersBox extends PureComponent {
  static propTypes = {
    product: PropTypes.object,
    authStore: PropTypes.object,
    offersStore: PropTypes.object,
  };

  _onCancelOffer = async item => {
    const { product, offersStore, authStore } = this.props;

    try {
      const signature = await onSign(authStore);

      let approveHash = null;
      const { success } = await offersStore.cancelOffer({
        productId: product.id,
        offerId: item?.id,
        signature,
        approveHash,
      });

      if (success) {
        await offersStore.getOffers({
          productId: product.id,
        });
      }
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);
    }
  };

  _onApproveOffer = item => {
    this._messageOfferModal.open(item, true);
  };

  _onOpenMessageModal = item => {
    this._messageOfferModal.open(item);
  };

  _renderItem = (item, index) => {
    const { t, product } = this.props;
    const userId = Storage.get('USER_ID');
    const timeFormat = 'YYYY/M/D HH:mm';

    return (
      <tr key={`${item?.userId}_${index}`}>
        <td>
          <div className="horizon">
            <div
              className={classnames('new-dot', {
                show: item?.isNew,
              })}
            />
            <Thumbnail
              size={26}
              rounded
              className="avatar"
              placeholderUrl={Images.USER_PLACEHOLDER}
              url={item?.userAvatar}
            />
            <Typography poppins className="user-name">
              {item?.userName || item?.userPublicAddress}
            </Typography>
            {/* </div>
        </td>
        <td>
          <div className="horizon"> */}
            {item?.offerMessage ? (
              <Clickable onClick={() => this._onOpenMessageModal(item)}>
                <img className="fire-icon" src={Images.COLOR_MSG_ICON} alt="" />
              </Clickable>
            ) : (
              <Clickable>
                <img className="fire-icon" src={Images.GRAY_MSG_ICON} alt="" />
              </Clickable>
            )}
          </div>
        </td>
        <td>
          <Typography size="large" bold poppins>
            {item?.offerPrice}
          </Typography>
        </td>
        <td>{item?.currency}</td>
        <td>
          {item?.userId === userId || product.owned
            ? t(`offer.buyer_status.${item?.status}`)
            : '-'}
        </td>
        <td>
          <Typography size="small" className="date">
            {moment(item?.createdAt).format(timeFormat)}
          </Typography>
        </td>
        <td>
          {userId === product.userId && item?.status === 'OFFERING' && !product?.productBuying && product.isApproveOffer && (
            <Clickable className="action-button" onClick={() => this._onApproveOffer(item)}>
              {`${t('offer.accept')}`}
            </Clickable>
          )}
          {userId === item?.userId && ['OFFERING', 'NEW'].includes(item?.status) && (
            <Clickable className="action-button cancel-btn" onClick={() => this._onCancelOffer(item)}>
              {t('common:cancel')}
            </Clickable>
          )}
        </td>
      </tr>
    );
  };

  render() {
    const { t, offersStore } = this.props;
    const { offers } = offersStore;

    return (
      <OffersBoxStyled id="offer-list">
        <div className="table-outer">
          <table>
            <thead>
              <tr>
                <th className="from">{t('common:from')}</th>
                {/* <th className="">&nbsp;</th> */}
                <th className="amount">{t('offer.amount')}</th>
                <th className="currency">{t('common:header.currency')}</th>
                <th className="status">{t('common:status')}</th>
                <th className="create-at">{t('offer.create_at')}</th>
                <th className="action">&nbsp;</th>
              </tr>
            </thead>
            <tbody>
              {offers.length > 0 ? (
                offers.map(this._renderItem)
              ) : (
                <tr>
                  <td colSpan={7}>
                    <NoDataBox className="no-data-box" />
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        <MessageOfferModal
          ref={ref => {
            this._messageOfferModal = ref;
          }}
        />
      </OffersBoxStyled>
    );
  }
}

export default OffersBox;
