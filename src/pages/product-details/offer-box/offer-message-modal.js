import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { Translation } from 'react-i18next';
import moment from 'moment';
import classnames from 'classnames';

import Modal from '../../../components/modal';
import Typography from '../../../components/typography';
import Clickable from '../../../components/clickable';
import Loading from '../../../components/loading';
import Toast from '../../../components/toast';
import Media from '../../../utils/media';
import Format from '../../../utils/format';
import { Images, Colors } from '../../../theme';
import Thumbnail from '../../../components/thumbnail';
import { getNonceNumberBlock, onSign, onTransfer } from '../utils/checkout';
import { Button } from 'antd';
import { checkCorrespondingNetwork } from '../../../utils/web3';
import i18n from '../../../translations/i18n';

const MsgOfferModalStyled = styled.div`
  .loading-box {
    height: 550px;
  }

  .modal-header {
    display: flex;
    align-items: center;
    padding-top: 30px;

    p {
      color: #fff;
      font-weight: 600;
      font-size: 20px;
    }
  }

  .content {
    background-color: inherit;
    display: flex;
    padding-top: 20px;
    padding-bottom: 30px;
    flex-direction: column;

    .row {
      display: flex;
      align-items: flex-start;
      flex: 1;
      margin-bottom: 10px;

      .label {
        color: #fff;
        width: 180px;
        font-weight: 400;
        font-size: 14px;
      }
      .value {
        color: #045afc;
        width: 120px;
        font-weight: 400;
        font-size: 14px;
      }

      .user-box {
        display: flex;
        align-items: center;
        gap: 10px;
        width: 120px;
        .username {
          width: calc(100% - 36px);
          color: ${Colors.PRIMARY};
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
        }
      }

      .horizon {
        display: flex;
        align-items: center;

        .avatar {
          margin-right: 9px;
          min-width: 0;
        }

        .fire-icon {
          margin-right: 23px;
        }

        .heat-number {
          color: ${Colors.PRIMARY};
        }
      }
    }

    .offer-message-label {
      color: #fff;
      margin: 40px 0 16px;
    }

    .message-box {
      padding: 16px 12px;
      position: relative;
      white-space: pre-wrap;
      word-break: break-word;
      border: 1px solid #65676b;
      color: ${Colors.TEXT};
      border-radius: 4px;

      .message-footer {
        position: absolute;
        bottom: 0;
        left: 0;
        height: 75px;
        width: 100%;
        display: flex;
        justify-content: center;
        color: #e51109;
        font-size: 12px;
        align-items: flex-end;
        padding: 13px;
        text-align: center;
        background-image: linear-gradient(to bottom, rgba(255, 248, 247, 0), rgba(255, 248, 247, 0.92) 50%, #fff8f7);
      }

      &.trim {
        max-height: 120px;
        overflow: hidden;

        &.expand {
          max-height: none;
          padding-bottom: 50px;

          .message-footer {
            height: 50px;
          }
        }
      }
    }

    .footer {
      margin-top: 30px;
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: center;
      gap: 10px;

      button {
        padding: 0 20px;
        font-weight: 500;
        font-size: 16px;
        line-height: 16px;
        min-width: 160px;
        min-height: 50px;
        border-radius: 999px;
        &.accept-button {
          background-color: #045afc;
        }
        &.cancel-btn {
          color: ${Colors.TEXT};
          border: 1px solid ${Colors.BORDER};
        }
      }
    }

    ${Media.lessThan(Media.SIZE.MD)} {
      .footer {
        flex-direction: column;

        button {
          flex: 1;
          width: 100%;
        }
      }
    }
  }
`;
@inject(stores => ({
  product: stores.products.currentProductDetails,
  offersStore: stores.offers,
  authStore: stores.auth,
  productsStore: stores.products,
}))
@observer
class MsgOfferModal extends Component {
  static propTypes = {
    product: PropTypes.object,
    offersStore: PropTypes.object,
    authStore: PropTypes.object,
    productsStore: PropTypes.object,
  };

  state = {
    initing: false,
    isOpen: false,
    offerDetails: null,
    isShowAcceptButton: false,
    loading: false,
    commissionFee: 0,
    isExpandLongMessage: false,
  };

  open = async (offerDetails, isShowAcceptButton = false) => {
    const { offersStore, product } = this.props;

    this.setState({
      initing: isShowAcceptButton,
      isOpen: true,
      offerDetails,
      isShowAcceptButton,
    });

    if (!isShowAcceptButton) return null;

    const { success, data } = await offersStore.getOfferInfos({
      productId: product.id,
      offerId: offerDetails.id,
    });

    if (success) {
      this.setState({
        initing: false,
        commissionFee: data.commissionFee,
      });
    } else {
      this.setState({
        initing: false,
      });
    }

    return null;
  };

  close = () => this.setState({ isOpen: false, initing: false });

  _onToggleLongMessage = () => {
    const { isExpandLongMessage } = this.state;

    this.setState({
      isExpandLongMessage: !isExpandLongMessage,
    });
  };

  _onUpdateOffer = async (signature, t, nonce, nonceContract, numberBlock) => {
    const { authStore, offersStore, product } = this.props;
    const { offerDetails } = this.state;

    const confirmResult = await offersStore.confirmOffer({
      productId: product.id,
      offerId: offerDetails.id,
      nonce,
      nonceContract,
      signature,
      numberBlock
    });

    if (!confirmResult.success) throw confirmResult.data;

    // if (!confirmResult.data.hasPublicKey) throw { error: t('validation_messages:OUT_OF_WALLET') }
    if (confirmResult.data?.holding) {
      product.setHolding(true);

      // eslint-disable-next-line no-throw-literal
      throw { error: t('validation_messages:PRODUCT_HOLDING') };
    }
    this.setState({
      confirmOfferData: confirmResult,
    });
    const data = confirmResult.data.smartContractVerifySignature;
    const gasLimit = 300000;
    const { publicAddress } = authStore.initialData;

    // const currency = offerDetails.currency === 'ETH' ? 'WETH' : offerDetails.currency;
    const currency = offerDetails.currency;

    const transactionId = await onTransfer(publicAddress, t, data, gasLimit, currency, offerDetails.offerPrice);

    const updateOffer = await offersStore.updateOffer({
      contractTransactionId: transactionId,
      productId: product.id,
      orderId: confirmResult.data.orderId,
      offerId: offerDetails.id,
      nonce,
      nonceContract,
      numberBlock
    });

    if (!updateOffer.success) {
      // eslint-disable-next-line no-throw-literal
      throw { error: 'UPDATE_OFFER_TRANSACTION_FAIL' };
    }
  };

  _onAcceptOffer = async t => {
    const { product, productsStore, authStore, offersStore } = this.props;
    const { offerDetails } = this.state;

    this.setState({
      loading: true,
    });

    this.isCancelUpdateOffer = false;

    try {
      await new Promise(resolve => {
        setTimeout(() => {
          resolve();
        }, 10000);
      });

      const signature = await onSign(authStore);
      const { nonce, numberBlock, nonceContract } = await getNonceNumberBlock(authStore);
      await checkCorrespondingNetwork(product?.chainId);
      await this._onUpdateOffer(signature, t, nonce, nonceContract, numberBlock);

      product.setData({
        status: 'SOLD',
        productBuying: true,
      });
      offerDetails.setData({
        status: 'BUYING',
      });
      this.close();
    } catch (e) {
      // eslint-disable-next-line no-console
      console.error(e);

      if (e?.message?.includes('User denied transaction signature')) {
        this.isCancelUpdateOffer = true;
      }

      if (e.error && !['ERROR_CANCEL_APPROVE_FEE'].includes(e.error)) {
        Toast.error(t(`validation_messages:${e.error}`));
      }
    } finally {
      await productsStore.getProductDetails({
        id: product.id,
        langKey: i18n.language.toUpperCase(),
      });

      if (this.isCancelUpdateOffer) {
        const { confirmOfferData } = this.state;

        await offersStore.cancelUpdateOffer({
          orderId: confirmOfferData.data.orderId,
          offerId: offerDetails.id,
        });
        this.close();
      }

      this.setState({
        loading: false,
      });
    }

    return null;
  };

  _renderContent = () => {
    const { product } = this.props;
    const { loading, offerDetails, isShowAcceptButton, commissionFee, isExpandLongMessage } = this.state;
    const receiveAmount = offerDetails?.offerPrice - commissionFee;
    const isLongMessage = (offerDetails?.offerMessage?.length || 0) > 100;

    return (
      <Translation>
        {t => (
          <>
            <div className="modal-header">
              <Typography bold>{t('product_details:offer.content')}</Typography>
            </div>
            <div className="content">
              <div className="row">
                <p className="label">{t('product_details:offer.user')}</p>
                <div className="user-box">
                  <Thumbnail
                    size={26}
                    rounded
                    className="avatar"
                    placeholderUrl={Images.USER_PLACEHOLDER}
                    url={offerDetails?.userAvatar}
                  />
                  <p size="big" className="username">
                    {offerDetails?.userName}
                  </p>
                </div>
              </div>
              <div className="row">
                <p className="label" size="large">
                  {t('product_details:offer.create_at')}
                </p>
                <p className="value">{moment(offerDetails?.createdAt).format('YYYY/MM/DD HH:mm')}</p>
              </div>
              <div className="row">
                <p className="label">{t('product_details:offer.amount')}</p>
                <p className="value">
                  {offerDetails?.offerPrice} {offerDetails?.currency}
                </p>
              </div>
              {product.owned && isShowAcceptButton && (
                <>
                  <div className="row">
                    <p className="label">{t('product_details:offer.commission_fee')}</p>
                    <p className="value">
                      {Format.price(commissionFee)} {offerDetails?.currency}
                    </p>
                  </div>
                  <div className="row">
                    <p className="label">{t('product_details:offer.receive')}</p>
                    <p className="value">
                      {Format.price(receiveAmount)} {offerDetails?.currency}
                    </p>
                  </div>
                </>
              )}
              {offerDetails?.offerMessage && (
                <>
                  <Typography size="big" className="offer-message-label" bold>
                    {t('product_details:offer.offer_message')}
                  </Typography>
                  <div className={classnames('message-box', { trim: isLongMessage, expand: isExpandLongMessage })}>
                    {offerDetails?.offerMessage}
                    {isLongMessage && (
                      <Clickable className="message-footer" onClick={this._onToggleLongMessage}>
                        {!isExpandLongMessage ? t('product_details:offer.show_more') : t('product_details:offer.show_less')}
                      </Clickable>
                    )}
                  </div>
                </>
              )}
              <div className="footer">
                {isShowAcceptButton && (
                  <Button
                    type="primary"
                    disabled={receiveAmount < 0}
                    loading={loading}
                    className="accept-button"
                    onClick={() => this._onAcceptOffer(t)}
                  >
                    {t('product_details:offer.sell_with_person')}
                  </Button>
                )}
                {!loading && (
                  <Button type="ghost" className="cancel-btn" onClick={this.close}>
                    {t('common:close')}
                  </Button>
                )}
              </div>
            </div>
          </>
        )}
      </Translation>
    );
  };

  render() {
    const { isOpen, loading, initing } = this.state;

    return (
      <Modal
        className="custom-modal"
        open={isOpen}
        onCancel={this.close}
        destroyOnClose
        maskClosable={!loading}
        closable={!loading}
        width={600}
        padding={30}
      >
        <MsgOfferModalStyled>
          {initing ? <Loading className="loading-box" size="small" /> : this._renderContent()}
        </MsgOfferModalStyled>
      </Modal>
    );
  }
}

export default MsgOfferModal;
