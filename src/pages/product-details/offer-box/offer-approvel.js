import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation } from 'react-i18next';

import { forwardInnerRef } from '../../../utils/high-order-functions';
import MaskLoading from '../../../components/mask-loading';
import { approveTokenErc721ANew, checkCorrespondingNetwork } from '../../../utils/web3';
import Typography from '../../../components/typography';
import Confirmable from '../../../components/confirmable';
import { Colors } from '../../../theme';
import { onSign } from '../utils/checkout';
import { CHAIN_LIST } from '../../../constants/chains';
// import OfferModal from './offer-modal';

const OfferApprovelStyled = styled.div`
  flex: 1;

  .btn-approvel {
    width: 100%;
    background-color: ${Colors.BLUE_4};
    height: 45px;
  }
`;

@withTranslation(['product_details', 'validation_messages'])
@forwardInnerRef
@inject(stores => ({
  productsStore: stores.products,
  offersStore: stores.offers,
  authStore: stores.auth,
  product: stores.products.currentProductDetails,
}))
@observer
class OfferApprovel extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
    offersStore: PropTypes.object,
    authStore: PropTypes.object,
    product: PropTypes.object,
  };

  _onApprovel = async () => {
    const { offersStore, product, authStore, t } = this.props;

    MaskLoading.open({});

    try {
      const signature = await onSign(authStore);
      await checkCorrespondingNetwork(product?.chainId);

      // const paymentAddress = Configs.EXCHANGE_CONTRACT_ADDRESS;
      // const paymentAddress = process.env.REACT_APP_EXCHANGE_CONTRACT_ADDRESS;
      const paymentAddress = CHAIN_LIST[product?.chainId]?.exchangeContractAddress;

      const approveNftHash = await approveTokenErc721ANew(
        authStore.initialData.publicAddress,
        paymentAddress,
        product.tokenId,
        t,
      );

      const res = await offersStore.approvelOffer({
        productId: product?.id,
        signature,
        approveNftHash,
      });

      if (!res.success) throw res.data;

      product.setData({
        isApproveOffer: true,
      });
      MaskLoading.close();

      await Confirmable.open({
        content: t('product_details:offer.approvel_offer_success'),
        hideCancelButton: true,
      });
    } catch (e) {
      Confirmable.open({
        content:
          e?.code === 4001
            ? t('validation_messages:CANCEL_CONFIRM_META_MASK')
            : t(`validation_messages:${e.error || 'SOMETHING_WENT_WRONG'}`),
        hideCancelButton: true,
      });
      // eslint-disable-next-line no-console
      console.error(e);
    } finally {
      MaskLoading.close();
    }
  };

  render() {
    const { product, t } = this.props;
    const { productBuying, confirmResell } = product.productResell;

    return (
      <OfferApprovelStyled>
        {/* <OfferModal
          ref={ref => {
            this._offerModal = ref;
          }}
          product={product}
          history={history}
        /> */}

        {!productBuying && product.isApproveOffer && !product.canOfferFlag && (
          <div className="offer-status-box">
            <Typography primary>
              {t(`offer.seller_status.${product.offerNftTransactionStatus}`)}
              {confirmResell && <span>,</span>}
            </Typography>
          </div>
        )}
        {/* {!productBuying && !product.isApproveOffer && !product.canOfferFlag && (
          <div className="offer-status-box">
            <Button 
              // onClick={this._onApprovel} 
              onClick={() => this._lendingModal.open()}
              className="btn-approvel" 
              background={Colors.BLUE_4}>
                {t('product_details:offer.offer')}
            </Button>
          </div>
        )} */}
      </OfferApprovelStyled>
    );
  }
}

export default OfferApprovel;
