import { Component } from 'react';
import styled from 'styled-components';

import Modal from '../../../components/modal';

const UniswapInterfaceModalStyled = styled.div`
  background-color: rgb(247, 248, 250);
`;

class UniswapInterfaceModal extends Component {
  state = {
    isOpen: false,
    offerPrice: 0,
  };

  // open = offerPrice => this.setState({ isOpen: true, offerPrice });
  open = (offerPrice, from, to) => this.setState({ isOpen: true, offerPrice, from, to });

  close = () => this.setState({ isOpen: false });

  render() {
    const { isOpen, offerPrice, from, to } = this.state;

    return (
      <Modal open={isOpen} onCancel={this.close} destroyOnClose padding={0}>
        <UniswapInterfaceModalStyled>
          <iframe
            title="uniswap-interface"
            // src={`https://app.uniswap.org/#/swap?exactField=input&exactAmount=${offerPrice}&inputCurrency=ETH&outputCurrency=${process.env.WETH_CONTRACT_ADDRESS}`}
            src={`https://app.uniswap.org/#/swap?exactField=input&exactAmount=${offerPrice}&inputCurrency=${from}&outputCurrency=${to}`}
            height="660px"
            width="100%"
            style={{
              border: 0,
              margin: '0 auto',
              // paddingTop: '70px',
              paddingTop: '40px',
              display: 'block',
              maxWidth: '600px',
              minWidth: '300px',
            }}
            id="myId"
          />
        </UniswapInterfaceModalStyled>
      </Modal>
    );
  }
}

export default UniswapInterfaceModal;
