import { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { withTranslation } from 'react-i18next';
import { inject, observer } from 'mobx-react';
import moment from 'moment';

import { Colors, Images } from '../../theme';
import Typography from '../../components/typography';
import Thumbnail from '../../components/thumbnail';
import NoDataBox from '../../components/no-data-box';
import Misc from '../../utils/misc';
import Format from '../../utils/format';

const TransactionHistoriesBoxStyled = styled.div`
  margin-top: 20px;

  .quantity-value {
    .typography {
      text-align: center;
    }
  }
  .table-outer {
    overflow-x: auto;
    overflow-y: hidden;
    width: 100%;
    table {
      width: 100%;
      min-width: 560px;
      table-layout: fixed;

      thead {
        tr {
          th {
            padding: 0 8px;
            font-weight: 700;
            font-size: 14px;
            color: ${Colors.TEXT};
            text-align: left;
          }
        }
      }

      tbody {
        tr {
          height: 53px;
          border-bottom: 1px solid rgb(255 255 255 / 8%);

          td {
            padding: 0 8px;
            font-size: 13px;
            color: ${Colors.TEXT};
            .typography {
              font-size: 13px;
              color: ${Colors.TEXT};
            }
            .transfer-icon {
              width: 28px;
            }

            .horizon {
              display: flex;
              align-items: center;

              .avatar {
                margin-right: 9px;
                min-width: 0;
              }

              .user-name {
                overflow: hidden;
                text-overflow: ellipsis;
                max-width: 180px;
                white-space: nowrap;
                flex: 1;
                margin-right: 5px;
              }
            }
          }
        }
      }
    }

    .no-data-box {
      height: 100px;
      display: flex;
      align-items: center;
    }
  }
`;

@withTranslation('product_details')
@inject(stores => ({
  product: stores.products.currentProductDetails,
  productsStore: stores.products,
}))
@observer
class TransactionHistoriesBox extends Component {
  static propTypes = {
    product: PropTypes.object,
    productsStore: PropTypes.object,
  };

  state = {
    initial: true,
    data: [],
  };

  async componentDidMount() {
    const { productsStore, product } = this.props;

    const { success, data } = await productsStore.getTransactionHistories({
      productId: product.id,
    });

    if (success) {
      this.setState({
        data,
        initial: false,
      });
    }
  }

  _renderItem = (item, index) => {
    const { product, t } = this.props;
    const timeFormat = 'YYYY/M/D HH:mm';

    return (
      <tr key={index}>
        <td>
          <Typography size="oversmall">
            {item.type === 'BID'
              ? t('transaction_history_status.auction')
              : item.type === 'NORMAL'
              ? t('transaction_history_status.normal')
              : item.type === 'RESALE_NORMAL'
              ? t('transaction_history_status.resale')
              : item.type === 'CREDIT'
              ? t('transaction_history_status.credit_card')
              : item.type === 'CRYPTO'
              ? t('transaction_history_status.crypto')
              : item.type === 'BORROW'
              ? t('transaction_history_status.borrow')
              : item.type === 'LENDING'
              ? t('transaction_history_status.lending')
              : item.type === 'GACHA'
              ? t('transaction_history_status.gacha')
                : item.type === 'AIRDROP'
                ? t('transaction_history_status.airdrop')
              : t('transaction_history_status.resale_offer')}
          </Typography>
        </td>
        <td>
          <Typography size="large" bold poppins>
            {item.type === 'CREDIT' ? Format.price(item.productYenPrice) : Format.price(item.price)}
          </Typography>
        </td>
        <td>
          <Typography size="oversmall">{item.type === 'CREDIT' ? 'JPY' : item?.currency}</Typography>
        </td>
        {product?.tokenStandard === 'ERC1155' && (
          <td className="quantity-value">
            <Typography size="oversmall">{item.quantity}</Typography>
          </td>
        )}
        <td>
          <div className="horizon">
            <Thumbnail
              size={26}
              rounded
              className="avatar"
              placeholderUrl={Images.USER_PLACEHOLDER}
              url={item.sellerAvatar}
            />
            <Typography poppins className="user-name">
              {Misc.trimString(item.sellerName, 15) || Misc.trimPublicAddress(item.sellerPublicAddress, 3)}
            </Typography>
          </div>
        </td>
        <td>
          <div className="horizon">
            <Thumbnail
              size={30}
              rounded
              className="avatar"
              placeholderUrl={Images.USER_PLACEHOLDER}
              url={item.buyerAvatar}
            />
            <Typography poppins className="user-name">
              {Misc.trimString(item?.buyerName, 15) || Misc.trimPublicAddress(item.buyerPublicAddress, 3)}
            </Typography>
          </div>
        </td>
        <td>
          <Typography size="oversmall" className="date">
            {moment(item.createdAt).format(timeFormat)}
          </Typography>
        </td>
      </tr>
    );
  };

  render() {
    const { product, t } = this.props;
    const { data, initial } = this.state;

    return (
      <TransactionHistoriesBoxStyled>
        <div className="table-outer">
          <table>
            <thead>
              <tr>
                <th className="event">{t('common:event')}</th>
                <th className="price">{t('common:price')}</th>
                <th className="currency">{t('common:header.currency')}</th>
                {product?.tokenStandard === 'ERC1155' && <th className="quantity">{t('common:header.quantity')}</th>}
                <th className="seller">{t('common:seller')}</th>
                <th className="buyer">{t('common:buyer')}</th>
                <th className="date_time">{t('common:date_time')}</th>
              </tr>
            </thead>
            <tbody>
              {!initial && data.length === 0 ? (
                <tr>
                  <td colSpan={7}>
                    <NoDataBox className="no-data-box" />
                  </td>
                </tr>
              ) : (
                data.map(this._renderItem)
              )}
            </tbody>
          </table>
        </div>
      </TransactionHistoriesBoxStyled>
    );
  }
}

export default TransactionHistoriesBox;
