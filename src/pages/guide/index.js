import React from 'react';
import Page from '../../components/page';
import { useTranslation } from 'react-i18next';
import Footer from '../../app/footer';
import { Images } from '../../theme';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { GuideCol, GuideRow, GuideStyled, SectionPanel, TabsStyled } from './style';

export default function Guide() {
  const { t } = useTranslation('common');
  const history = useHistory();
  const location = useLocation();

  const onClick = type => () => {
    history.replace(`/${type}`);
  };

  const items = [
    {
      label: t('guide.panel_label_1'),
      key: '#1',
      children: (
        <SectionPanel>
          <section className="l-section js-headerShadow" id="primez01">
            <div className="inner">
              {/* <h2 className='h3'> NFTに必要なもの</h2> */}
              <p className="section-title">{t('guide.section_1.text_1')}</p>
              <div className="l_2x1">
                <div className="elm">
                  <p className="img-icon">
                    <img src={Images.IETH} alt="ETHEREUM" />
                  </p>
                  <p className="title-p mt-10">
                    <strong>{t('guide.section_1.text_2')}</strong>
                  </p>
                  <p className="title-p">
                    <strong>{t('guide.section_1.text_3')}</strong>
                    <p>
                      {t('guide.section_1.text_4')}
                      <Link href="https://bitflyer.com/ja-jp/" target="_blank" rel="noreferrer">
                        bitflyer
                      </Link>
                      ,&nbsp;&nbsp;
                      <Link href="https://coincheck.com/ja/" target="_blank" rel="noreferrer">
                        coincheck
                      </Link>
                      ,&nbsp;&nbsp;
                      <Link href="https://www.binance.com/ja" target="_blank" rel="noreferrer">
                        BINANCE
                      </Link>
                    </p>
                  </p>
                </div>
                <div className="elm">
                  <p className="img-icon">
                    <img src={Images.IMTM} alt="METAMASK" />
                  </p>
                  {/* <h3 className='title'>ウォレット</h3> */}
                  <div>
                    <p className="title-p mt-10">
                      <strong>{t('guide.section_1.text_5')}</strong>
                    </p>
                    <p className="title-p">
                      <strong>{t('guide.section_1.text_6')}</strong>
                      <p>
                        <strong>
                          {t('guide.section_1.text_7')}
                          <Link>{t('guide.section_1.text_8')}</Link>
                        </strong>
                      </p>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </SectionPanel>
      ),
    }, // remember to pass the key prop
    {
      label: t('guide.panel_label_2'),
      key: '#2',
      children: (
        <SectionPanel>
          <section className="l-section" id="primez02">
            <div className="inner">
              {/* <h2 className='h3'> NFTの全体像</h2> */}
              <p className="section-title">
                {t('guide.section_2.text_1')}
                <br />
                {t('guide.section_2.text_2')}
              </p>
              <img className="l-img" src={Images.ZENTAI} alt=" NFT system diagram"></img>
            </div>
          </section>
        </SectionPanel>
      ),
    },
    {
      label: t('guide.panel_label_3'),
      key: '#3',
      children: (
        <SectionPanel>
          <section className="l-section" id="primez03">
            <div className="inner">
              {/* <h2 className='h3'> NFTをはじめるステップ</h2> */}
              <p className="section-title">{t('guide.section_3.group_1.text_1')}</p>
              <div className="m_flowBlock num1">
                <div className="row-number">1.</div>
                <div className="wrapBlock">
                  <div className="elm">
                    <div className="txt">
                      <h3 className="txt-title">{t('guide.section_3.group_1.text_2')}</h3>
                      <p>{t('guide.section_3.group_1.text_3')}</p>
                      <p>{t('guide.section_3.group_1.text_4')}</p>
                    </div>
                    <div className="img"></div>
                  </div>
                  <div className="elm">
                    <div className="txt">
                      <h3 className="txt-title">{t('guide.section_3.group_1.text_5')}</h3>
                      <p>{t('guide.section_3.group_1.text_6')}</p>
                      <p>
                        <small>{t('guide.section_3.group_1.text_7')}</small>
                      </p>
                      <p>
                        <p>
                          <strong>{t('guide.section_3.group_1.text_8')}</strong>
                        </p>
                        <div>
                          <Link href="https://bitflyer.com/ja-jp/" target="_blank" rel="noreferrer">
                            Bitflyer
                          </Link>
                          &nbsp;&nbsp;・&nbsp;&nbsp;
                          <Link href="https://coincheck.com/ja/" target="_blank" rel="noreferrer">
                            Coincheck
                          </Link>
                          &nbsp;&nbsp;・&nbsp;&nbsp;
                          <Link href="https://www.binance.com/ja" target="_blank" rel="noreferrer">
                            BINANCE
                          </Link>
                        </div>
                      </p>
                    </div>
                    <div className="img">
                      <img className="step1-1-svg" src={Images.STEP_1_1} alt="step1_1_1"></img>
                    </div>
                  </div>
                  <div className="elm none-border">
                    <div className="txt">
                      <h3 className="txt-title">{t('guide.section_3.group_1.text_8')}</h3>
                      <p>{t('guide.section_3.group_1.text_9')}</p>
                      <p>
                        <small>{t('guide.section_3.group_1.text_10')}</small>
                      </p>
                    </div>
                    <div className="img">
                      <img src={Images.STEP_1_2} alt="step1_2"></img>
                    </div>
                  </div>
                </div>
              </div>
              <div className="m_flowBlock num2">
                <div className="row-number">2.</div>
                <div className="wrapBlock">
                  <div className="elm">
                    <div className="txt">
                      <h3 className="txt-title">{t('guide.section_3.group_2.text_1')}</h3>
                      <p>
                        {t('guide.section_3.group_2.text_2')}
                        <p>{t('guide.section_3.group_2.text_3')}</p>
                      </p>
                    </div>
                    <div className="img"></div>
                  </div>
                  <div className="elm">
                    <div className="txt">
                      <h3 className="txt-title">{t('guide.section_3.group_2.text_4')}</h3>
                      <p>{t('guide.section_3.group_2.text_5')}</p>
                      <p>
                        <p>
                          <strong>{t('guide.section_3.group_2.text_6')}</strong>
                        </p>
                        <Link href="https://metamask.io/" target="_blank" rel="noreferrer">
                          {t('guide.section_3.group_2.text_7')}
                        </Link>
                      </p>
                    </div>
                    <div className="img">
                      <img src={Images.STEP_2_1} alt="STEP_2_1"></img>
                    </div>
                  </div>
                  <div className="elm none-border">
                    <div className="txt">
                      <h3 className="txt-title">{t('guide.section_3.group_2.text_8')}</h3>
                      <p>
                        <p>{t('guide.section_3.group_2.text_9')}</p>
                        <p>{t('guide.section_3.group_2.text_10')}</p>
                        <p>
                          <small>
                            {t('guide.section_3.group_2.text_11')}
                            <p>{t('guide.section_3.group_2.text_12')}</p>
                          </small>
                        </p>
                      </p>
                    </div>
                    <div className="img">
                      <img src={Images.STEP_2_2} alt="STEP_2_2"></img>
                    </div>
                  </div>
                </div>
              </div>
              <div className="m_flowBlock num3">
                <div className="row-number">3.</div>
                <div className="wrapBlock">
                  <div className="elm none-border">
                    <div className="txt">
                      <h3 className="txt-title">{t('guide.section_3.group_3.text_1')}</h3>
                      <p>{t('guide.section_3.group_3.text_2')}</p>
                      <p>{t('guide.section_3.group_3.text_3')}</p>
                      <p>{t('guide.section_3.group_3.text_4')}</p>
                      <p>{t('guide.section_3.group_3.text_5')}</p>
                      <p>{t('guide.section_3.group_3.text_6')}</p>
                      <p>{t('guide.section_3.group_3.text_7')}</p>
                      <p>{t('guide.section_3.group_3.text_8')}</p>
                      <p>{t('guide.section_3.group_3.text_9')}</p>
                    </div>
                    <div className="img">
                      <img src={Images.STEP_4_1} alt="step4_1"></img>
                    </div>
                  </div>
                </div>
              </div>
              <div className="m_flowBlock num4">
                <div className="row-number">4.</div>
                <div className="wrapBlock">
                  <div className="elm none-border">
                    <div className="txt num4-txt">
                      <h3 className="txt-title">{t('guide.section_3.group_4.text_1')}</h3>
                      <p>
                        {t('guide.section_3.group_4.text_2')}
                        <p>{t('guide.section_3.group_4.text_3')}</p>
                        <p>{t('guide.section_3.group_4.text_4')}</p>
                        <p>{t('guide.section_3.group_4.text_5')}</p>
                      </p>
                      <h4>
                        <Link onClick={onClick('buy-nft')}>{t('guide.section_3.group_4.text_6')}</Link>
                      </h4>
                      <p>
                        <small>{t('guide.section_3.group_4.text_7')}</small>
                      </p>
                    </div>
                    <div className="img">
                      <img src={Images.STEP_5_1} alt="STEP_5_1" />
                    </div>
                  </div>
                </div>
              </div>
              <div className="m_flowBlock num5">
                <div className="row-number">5.</div>
                <div className="wrapBlock">
                  <div className="elm none-border">
                    <div className="txt">
                      <h3 className="txt-title">{t('guide.section_3.group_5.text_1')}</h3>
                      <p>{t('guide.section_3.group_5.text_2')}</p>
                      <p>{t('guide.section_3.group_5.text_3')}</p>
                      <p>
                        <strong>{t('guide.section_3.group_5.text_4')}</strong>
                      </p>
                      <p>{t('guide.section_3.group_5.text_5')}</p>
                      <p>{t('guide.section_3.group_5.text_6')}</p>
                      <p>{t('guide.section_3.group_5.text_7')}</p>
                      <p>{t('guide.section_3.group_5.text_8')}</p>
                      <p>{t('guide.section_3.group_5.text_9')}</p>
                      <p>{t('guide.section_3.group_5.text_10')}</p>
                      <p>
                        <small>
                          {t('guide.section_3.group_5.text_11')}
                          <p>
                            {t('guide.section_3.group_5.text_12')}
                            <p>{t('guide.section_3.group_5.text_13')}</p>
                          </p>
                        </small>
                      </p>
                    </div>
                    <div className="img">
                      <img src={Images.STEP_6_1} alt="STEP_6_1" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </SectionPanel>
      ),
    },
  ];

  const tabActive = `${location.hash?.length ? location.hash : '#1'}`;

  return (
    <Page>
      <GuideStyled>
        <GuideRow className="top">
          <GuideCol className="title" span={24}>
            <div>{t('guide.title')}</div>
          </GuideCol>

          <GuideCol className="content" span={24}>
            <TabsStyled
              activeKey={tabActive}
              className="tabs"
              items={items}
              onChange={key => history.push(`/guide${key}`)}
              centered
            />
          </GuideCol>
        </GuideRow>
      </GuideStyled>

      <Footer />
      {/* <Footer /> */}
    </Page>
  );
}
