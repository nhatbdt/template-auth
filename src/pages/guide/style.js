import { Col, Layout, Row, Tabs } from 'antd';
import Media from '../../utils/media';
import styled from 'styled-components';

export const GuideStyled = styled(Layout)`
  &.ant-layout {
    font-family: sans-serif;
    margin: 0px auto;
    margin-top: 92px;
    background: #0f1114;
    color: #ffffff;
    height: 100%;
    max-width: 1540px;
  }

  ${Media.lessThan(Media.SIZE.SM)} {
    &.ant-layout {
      margin-top: 60px;
      width: 100%;
      padding: 0 10px;
      padding-bottom: 80px;
    }
  }
`;

export const GuideRow = styled(Row)`
  &.top {
    width: calc(1440px - 250px + 60px);
    margin: 0px auto;
  }

  ${Media.lessThan(Media.SIZE.SM)} {
    &.top {
      width: 100%;
      padding: 0;
    }
  }
`;

export const GuideCol = styled(Col)`
  &.title {
    margin-top: 50px;
    font-style: normal;
    font-weight: 600;
    font-size: 30px;
    line-height: 45px;
    color: #ffffff;
    text-align: center;
  }

  &.content {
    margin-top: 20px;
  }
`;

export const TabsStyled = styled(Tabs)`
  &.ant-tabs {
    .ant-tabs-nav {
      .ant-tabs-tab {
        /* width: 154px;
        height: 47px; */
        padding: 12px 28px;
        display: flex;
        align-items: center;
        text-align: center;
        justify-content: center;
        background: transparent;
        border-radius: 30px;

        .ant-tabs-tab-btn {
          font-style: normal;
          font-weight: 500;
          font-size: 16px;
          line-height: 23px;
          text-transform: capitalize;
          color: #a8aeba;
        }
      }

      .ant-tabs-tab-active {
        background: #045afc;

        .ant-tabs-tab-btn {
          color: #ffffff;
        }
      }

      .ant-tabs-ink-bar {
        background: transparent;
      }

      &::before {
        border-color: transparent;
      }
    }

    .ant-tabs-content-holder {
      .ant-tabs-content {
        top: -15px;
      }
    }
  }

  ${Media.lessThan(Media.SIZE.SM)} {
    &.ant-tabs {
      .ant-tabs-nav {
        .ant-tabs-tab {
          padding: 12px;
          margin-left: initial;

          .ant-tabs-tab-btn {
            font-size: 14px;
          }
        }
      }
    }
  }
`;

export const SectionPanel = styled.div`
  .inner {
    padding: 0 2%;
  }

  .l-section {
    margin: 0;
    /* padding: 2rem 0; */
    text-align: center;
    p {
      /* font-family: 'Noto Sans JP'; */
      font-style: normal;
      font-size: 14px;
      color: #a8aeba;
      font-weight: 400;
      line-height: 29px;
    }
    .section-title {
      font-size: 20px;
      font-weight: bold;
      margin-top: 16px;
    }
  }
  h2.h3,
  .h2.h3 {
    /* font-family: 'Noto Sans JP'; */
    font-style: normal;
    font-weight: 500;
    font-size: 20px;
    line-height: 28px;
    align-items: center;
    color: #a8aeba;
  }
  .l_2x1 {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-around;
    margin-top: 36px;
    /* margin: 4rem -0.5em 0; */

    .elm {
      max-width: 510px;
      max-height: 340px;
      flex: 1;
      flex-basis: 30%;
      border-radius: 10px;
      margin: 0.5em;
      /* max-width: 65vh; */
      text-align: center;
      /* padding: 0px 45px; */
    }
  }

  p {
    margin: 0;
    word-break: break-all;
    line-height: 2;
  }
  img {
    width: 100%;
    height: auto;
  }
  .l-img {
    width: 100% !important;
    margin-top: 30px;
    padding: 78px 26px;
    background: #15171c;
    border-radius: 20px;
  }
  .m_flowBlock {
    /* border-radius: 6px; */
    border-radius: 20px;
    /* margin: 2rem 0; */
    /* background: #376587; */
    background: #15171c;
    position: relative;
    width: 100%;
    /* border: 1px solid #376587; */
    padding: 10px 40px;
    display: flex;

    &:not(:first-child) {
      margin-top: 30px;
    }
  }
  .wrapBlock .elm {
    padding: 2.5rem 0;
    border-bottom: 1px solid #67a0c3;
    border-bottom: 1px solid rgba(41, 45, 51, 0.5);
    display: flex;
    align-items: flex-start;
    padding: 20px 0;
  }
  .elm .txt {
    /* font-family: 'Noto Sans JP'; */
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 20px;
    color: #a8aeba;
    flex: 5;
    text-align: left !important;
    .txt-title {
      color: #ffffff;
      font-weight: 700;
    }
  }
  .elm .img {
    flex: 2.5;
    padding-left: 2rem;
  }
  h3,
  .h3 {
    letter-spacing: 0.1em;
    margin: 0 0 0.8em 0;
    color: #a8aeba;
    /* font-family: 'Noto Sans JP'; */
    font-style: normal;
    font-weight: 500;
    font-size: 16px;
    line-height: 22px;
  }

  .num1 {
    margin-top: 26px;
  }
  .chu {
    font-size: 80%;
    background: rgba(39, 111, 218, 0.03);
    padding: 1.5rem;
    border-radius: 6px;
  }
  h4 {
    color: #a8aeba;
  }
  .step1-1-svg {
    background-color: #0c2338;
    border-radius: 6px;
  }
  small {
    color: #a8aeba;
    font-size: 14px !important;
  }
  .none-border {
    border-bottom: none !important;
  }
  .img-icon {
    width: 100%;
    height: 100%;
    /* margin-left: 8vh !important; */

    img {
      width: calc(100% - (100% - 340px));
      height: 100%;
    }
  }

  .row-number {
    font-style: normal;
    font-weight: 700;
    font-size: 40px;
    line-height: 58px;
    color: #ffffff;
    width: 37px;
  }

  .wrapBlock {
    margin-left: 20px;
    width: calc(100% - 37px);
  }

  ${Media.lessThan(Media.SIZE.XXXL)} {
    .img-icon {
      /* margin-left: 7vh !important; */
    }
    ${Media.lessThan(Media.SIZE.XS)} {
      .img-icon {
        /* margin-right: 7vh !important; */
      }
    }
    ${Media.lessThan(Media.SIZE.SM)} {
      .m_flowBlock {
        padding: 15px;
        flex-direction: column;
      }

      .row-number {
        width: 100%;
        text-align: center;
      }

      .elm {
        display: grid;
      }
      .elm .img {
        padding-left: 0rem;
      }
      .elm_h4 {
        display: grid;
        line-height: 10px;
        margin-top: 0px;
        font-size: 12px;
      }

      font-size: 12px;

      .inner {
        padding: 0px 5%;
      }
      .left {
        padding: 40px 5%;
      }

      .img-icon {
        width: initial;
        height: initial;
        margin-right: initial !important;
        margin-left: initial !important;
      }

      .l_2x1 {
        flex-direction: column;

        .elm {
          max-width: 100%;
          max-height: 100%;
          padding: 0;

          .img-icon {
            img {
              width: 100%;
            }
          }
        }
      }

      .wrapBlock {
        margin-left: 0;
        width: 100%;

        .elm {
          width: 100%;
          display: block;
        }

        .none-border {
          padding-bottom: 0;
        }
      }
    }

    .title {
      font-size: 24px !important;
      line-height: 32px !important;

      ${Media.lessThan(Media.SIZE.XXL)} {
        font-size: 16px !important;
        line-height: 30px !important;
      }
    }

    .title-p {
      font-size: 16px !important;
      line-height: 32px !important;

      ${Media.lessThan(Media.SIZE.XXL)} {
        font-size: 16px !important;
        line-height: 30px !important;
      }
    }
  }

  .mt-10 {
    margin-top: 10px;
  }
`;
