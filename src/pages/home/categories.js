import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import SwiperCore, { Autoplay, Pagination } from 'swiper'; // Navigation
import { Swiper, SwiperSlide } from 'swiper/react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';

import Media from '../../utils/media';
import Clickable from '../../components/clickable';
import SectionHeading from '../../components/section-heading';
import { useTranslation, withTranslation } from 'react-i18next';
import NoDataBox from '../../components/no-data-box';
import { Colors } from '../../theme';

SwiperCore.use([Autoplay, Pagination]);

const StyledDiv = styled.div`
  margin-top: 48px;

  .swiper-pagination {
    display: none;
  }

  .title {
    font-weight: 700;
    font-size: 24px;
    line-height: 36px;
    color: #282828;
    margin-top: 60px;
    margin-bottom: 0;
  }

  .sub-title {
    /* font-family: 'Noto Sans JP'; */
    font-style: normal;
    font-weight: 400;
    font-size: 20px;
    line-height: 24px;
    color: ${Colors.TEXT};
    margin-top: -4px;
    margin-bottom: 16px;
  }

  .category-item {
    /* width: 245px; */
    position: relative;
    border-radius: 10px 10px 0px 0px;
    overflow: hidden;
    display: block;
    border: 1px solid rgba(0, 0, 0, 0.1);
    border-radius: 10px;
    background-color: #15171c;

    .category-image {
      width: 100%;
      height: 170px;
      min-height: 170px;
      object-fit: contain;
    }

    .category-content {
      padding: 12px;
      display: flex;
      align-items: center;
      justify-content: center;
      text-transform: capitalize;
      align-items: center;
      font-weight: 700;
      font-size: 16px;

      .category-name {
        color: ${Colors.TEXT};
        font-size: 16px;
        font-weight: 700;
        text-transform: capitalize;
        max-width: 90%;
        overflow: hidden;
        text-overflow: ellipsis;
      }

      ${Media.lessThan(Media.SIZE.MD)} {
        padding: 17px 20px 10px 20px;

        .category-name {
          font-size: 11px;
        }
      }

      .category-logo {
        height: 50px;

        ${Media.lessThan(Media.SIZE.MD)} {
          height: 28px;
        }
      }

      .category-name {
        text-align: center;
      }
    }

    .count-items {
      text-align: center;
      color: #045afc;
      font-weight: 500;
      font-size: 16px;
      padding-bottom: 16px;
    }
  }

  .swiper-container {
    padding-bottom: 30px;
  }
  .swiper-pagination {
    position: absolute;
    bottom: 0 !important;
    right: 0;
    left: 0;
    width: 100%;
    display: flex;
    justify-content: center;
    padding: 0;

    .swiper-pagination-bullet {
      width: 10px;
      height: 10px;
      border-radius: 50%;
      background: #6fa9ff;
      margin: 3px 6px;
      box-shadow: 0 0 6px rgba(0, 0, 0, 0.27);
    }

    .swiper-pagination-bullet-active {
      background: #3cc8fc;
    }

    ${Media.lessThan(Media.SIZE.MD)} {
      .swiper-pagination-bullet {
        width: 6px;
        height: 6px;
      }
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    margin-bottom: 25px;
  }
`;

@withTranslation(['home'])
@inject(stores => ({
  categoriesStore: stores.categories,
}))
@withRouter
@observer
class Categories extends Component {
  static propTypes = {
    categoriesStore: PropTypes.object,
    // i18n: PropTypes.func,
    // history: PropTypes.any,
    // t: PropTypes.any,
  };

  _onClickViewAllHeading = url => {
    // console.log('url', url);
    const { history } = this.props;
    history.push(url);
  };

  async componentDidMount() {
    const { categoriesStore, i18n } = this.props;
    await categoriesStore.getListCategory({ langKey: i18n.language.toUpperCase(), limit: 100, page: 1 });
  }

  _onClick = item => {
    const { history } = this.props;
    history.push(`/category/${item.id}`);
  };

  _renderItem = item => {
    const { t } = this.props;
    return (
      <div className="category-col">
        <Clickable onClick={() => this._onClick(item)} className="category-item">
          <img className="category-image" src={item?.logoUrl} alt={item?.name} />
          <div className="category-content">
            {/* <div className="category-name">{Misc.trimString(item?.name, 14)}</div> */}
            <div className="category-name no-wrap">{item?.name}</div>
          </div>
          <div className="count-items">{`${item?.productCount ?? 0} ${item?.productCount > 1 ? t('items') : t('item')
            }`}</div>
        </Clickable>
      </div>
    );
  };

  render() {
    // const { categoriesStore, t } = this.props;
    const { categoriesStore, t } = this.props;
    const { listCategory } = categoriesStore;
    const breakpointsArr = {
      0: {
        slidesPerView: 1,
        spaceBetween: 8,
      },
      576: {
        slidesPerView: 2,
        spaceBetween: 10,
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 14,
      },
      992: {
        slidesPerView: 4,
        spaceBetween: 16,
      },
      1200: {
        slidesPerView: 5,
        spaceBetween: 20,
      },
    };

    return (
      <StyledDiv>
        <SectionHeading
          title={t('nft_category')}
        // readmore={t('home:view_all')}
        // linkReadmore="/search-category-detail/all/new_product/Currency/RANDOM"
        // onClickViewAll={this._onClickViewAllHeading}
        />
        <h3 className="sub-title">{t('nft_category_sub_title')}</h3>
        {listCategory?.length > 0 ? (
          <Swiper
            autoplay={{
              delay: 1500,
            }}
            onSwiper={ref => {
              this._swiper = ref;
            }}
            // spaceBetween={0}
            // slidesPerView={5}
            pagination={{
              clickable: true,
            }}
            modules={[Pagination]}
            breakpoints={breakpointsArr}
          // autoplayDisableOnInteraction={false}
          >
            {listCategory?.map((item, index) => (
              <SwiperSlide key={index}>{this._renderItem(item)}</SwiperSlide>
            ))}
          </Swiper>
        ) : (
          <NoDataBox />
        )}
      </StyledDiv>
    );
  }
}

const CategoriesWrap = props => {
  const { i18n } = useTranslation();

  return <Categories {...props} key={i18n.language} />;
};

export default CategoriesWrap;
