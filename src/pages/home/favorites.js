import React, { useContext, useEffect, useState } from 'react';
import styled from 'styled-components';
import { MobXProviderContext } from 'mobx-react';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import SectionHeading from '../../components/section-heading';
import ProductCard from '../../components/product-card-new';
import Media from '../../utils/media';

const Container = styled.div`
  margin-top: 40px;
  .collection-item {
    display: flex;
    align-items: center;
    gap: 15px;
    padding-bottom: 15px;
    border-bottom: 1px solid #59667b;
    margin-bottom: 15px;
    img {
      width: 87px;
      height: 56px;
      border-radius: 6px;
    }
  }
  .product-list {
    display: flex;
    justify-content: space-evenly;
    gap: 24px;

    ${Media.lessThan(Media.SIZE.LG)} {
      flex-wrap: wrap;
    }
    .product-card-new {
      /* min-width: 312px; */
      min-width: 260px;
      /* max-width: 412px; */
      max-width: 480px;
      ${Media.greaterThan(Media.SIZE.LG)} {
        min-width: unset !important;
      }
    }
  }
`;

const Favorites = () => {
  const history = useHistory();
  const { products } = useContext(MobXProviderContext);
  const { t, i18n } = useTranslation(['home']);
  const [favoriteList, setFavoriteList] = useState([]);

  const fetchData = async () => {
    const { success, data } = await products.getNewProducts({
      limit: 4,
      page: 1,
      findType: 'NEW_PRODUCT',
      langKey: i18n.language.toUpperCase(),
      sortBy: 'MOST_WISH',
    });

    if (success) {
      const result = products.parseProducts(data?.result);
      setFavoriteList(result);
    }
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [i18n.language]);

  const renderProductItem = (item, index = 0, showNumberRanking = false) => {
    item = {
      ...item,
      itemColor: '#1E3676',
      backgroundColor: '#F3F4F8',
      statusTextLight: true,
    };

    return (
      <ProductCard
        key={`item-${index}`}
        onClick={() => history.push(`/product-details/${item.id}`)}
        item={item}
        index={index}
        className="product-card-new"
        isShowCategoryName
        isShowStatus
        numShowText={40}
      />
    );
  };

  if (favoriteList?.length > 0)
    return (
      <Container>
        <SectionHeading
          title={t('home:favorite_nft')}
          readmore={t('home:view_all')}
          linkReadmore="/favorites"
          onClickViewAll={() => history.push('/favorites')}
        />
        <div className="product-list">{favoriteList.map((item, index) => renderProductItem(item, index))}</div>
      </Container>
    );

  return null;
};

export default Favorites;
