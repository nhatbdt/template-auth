import React, { useContext, useEffect, useState } from 'react';
import styled from 'styled-components';
import { MobXProviderContext } from 'mobx-react';
import { useHistory } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import SectionHeading from '../../components/section-heading';
import TopNFTCard from '../../components/product-card-top';
import Media from '../../utils/media';
import NoDataBox from '../../components/no-data-box';

const Container = styled.div`
  /* margin-top: 40px; */
  .list-collection {
    display: flex;
    justify-content: space-between;
    /* align-items: flex-start; */
    align-items: stretch;
    gap: 24px;
    ${Media.lessThan(Media.SIZE.MD)} {
      flex-direction: column;
    }

    .block {
      flex: 1;
      background: #000000;
      opacity: 0.7;
      box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);
      border-radius: 20px;
      padding: 20px 18px;
      max-width: 800px;
      margin: 0 auto;
      ${Media.lessThan(Media.SIZE.MD)} {
        width: 100%;
        padding: 4px 24px;
      }
      ${Media.lessThan(Media.SIZE.SM)} {
        width: 100%;
        padding: 2px 8px;
      }
    }

    .block-two {
      > div {
        &:last-child {
          .collection-image {
            gap: 6px;
          }
        }
      }
    }
  }
`;

const TopNfts = () => {
  const history = useHistory();
  const { products } = useContext(MobXProviderContext);
  const { t, i18n } = useTranslation(['home', 'ads']);
  const [topNfts, setTopNfts] = useState([]);

  const renderItem = (item, index) => {
    return (
      <TopNFTCard
        key={`top-nft-${index}`}
        onClick={() => history.push(`/product-details/${item.id}`)}
        item={item}
        index={index + 1}
      />
    );
  };

  useEffect(() => {
    const getNfts = async () => {
      const { success, data } = await products.getTopProducts({
        limit: 10,
        page: 1,
        findType: 'NEW_PRODUCT',
        langKey: i18n.language.toUpperCase(),
        sortBy: 'MOST_VOLUME',
      });

      if (success) {
        const result = data.result;
        setTopNfts(result);
      }
    };
    getNfts();
  }, [i18n.language, products]);

  const onViewAllCreator = () => {
    history.push('/top-nfts');
  };

  return (
    <Container>
      <SectionHeading
        title={t('home:top_nft')}
        readmore={t('home:view_all')}
        linkReadmore="/top-nfts"
        onClickViewAll={onViewAllCreator}
      />
      {topNfts?.length > 0 ? (
        <div className="list-collection">
          <div className="block">
            {topNfts.slice(0, Math.round(topNfts?.length / 2)).map((item, index) => renderItem(item, index))}
          </div>
          {topNfts?.length > 1 && (
            <div className={`block ${topNfts?.length === 10 && 'block-two'}`}>
              {topNfts
                .slice(Math.round(topNfts?.length / 2), 10)
                .map((item, index) => renderItem(item, index + Math.round(topNfts?.length / 2)))}
            </div>
          )}
        </div>
      ) : (
        <NoDataBox />
      )}
    </Container>
  );
};

export default TopNfts;
