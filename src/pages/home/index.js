import React, { useEffect } from 'react';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';

import Page from '../../components/page';
import Footer from '../../app/footer';
import Media from '../../utils/media';
import Storage from '../../utils/storage';

import TopBanner from './top-banner';
import TopNFTs from './top-nfts';
import LetStarted from './let-started';
import Categories from './categories';
import Auctions from './auctions';
import NftItems from './nft-items';
import News from './news';
import { Images } from '../../theme';
import Favorites from './favorites';
import Lending from './lending';

const StyledDiv = styled.div`
  padding-bottom: 30px;
  margin-top: 60px;
  background-color: #0f1114;

  ${Media.lessThan(Media.SIZE.MD)} {
    padding-top: 30px;
    margin-top: 30px;
  }

  /* .swiper-pagination-bullets, */
  .swiper-pagination {
    display: none;
  }

  .section-odd {
    background-color: #0f1114;
  }
  .section-even {
    padding-top: 50px;
    padding-bottom: 50px;
    background-image: url(${`${Images.SECTION_BG_1}`});
    background-repeat: no-repeat;
    background-size: cover;
  }

  .container {
    width: 100%;
    padding: 0 40px;
    margin: 0 auto;

    ${Media.lessThan(Media.SIZE.MD)} {
      padding: 0 15px;
    }
  }

  .ant-modal-content {
    border-radius: 10px !important;
    .ant-modal-body {
      padding: 0 !important;
    }
  }
`;

const Home = () => {
  const history = useHistory();


  useEffect(() => {
    const urlActive = Storage.get('URL_ACTIVE');

    if (urlActive) history.push(urlActive);

    return () => Storage.remove('URL_ACTIVE');
    // eslint-disable-next-line
  }, []);


  return (
    <Page>
      <StyledDiv>
        <section className="section-odd">
          <div className="container">
            <TopBanner />
            <Categories />
            <News />
            <Auctions />
          </div>
        </section>
        <section className="section-even">
          <div className="container">
            <TopNFTs />
          </div>
        </section>
        <section className="section-odd">
          <div className="container">
            <Lending />
            <NftItems />
            <Favorites />
            {/* <Creators /> */}
            <LetStarted />
          </div>
        </section>
      </StyledDiv>
      <Footer />
    </Page>
  );
};

export default Home;
