import React, { useEffect, useContext, useState } from 'react';
import styled from 'styled-components';
import { Row, Col } from 'antd';
import { Link, useHistory } from 'react-router-dom';
import { observer, MobXProviderContext } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import SectionHeading from '../../components/section-heading';
import Typography from '../../components/typography';
import Media from '../../utils/media';

const Container = styled.div`
  margin-bottom: 60px;
  margin-top: 0px;
  padding: 0px -50px;
  background: #ffffff;

  .title_creator {
    padding-top: 50px;
    h2 {
      font-size: 24px;
      color: #282828;
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    margin-bottom: 30px;
  }

  .list-started {
    .col {
      margin-bottom: 30px;
      /* min-height: 360px; */
      max-height: 120px;
      position: relative;

      ${Media.lessThan(Media.SIZE.XXL)} {
        /* min-height: 360px; */
        max-height: 120px;
      }
      ${Media.lessThan(Media.SIZE.MD)} {
        max-height: 120px;
      }
      ${Media.lessThan(Media.SIZE.SM)} {
        /* min-height: 200px; */
        max-height: 120px;
        .image-box img {
          min-height: 30vh !important;
        }
      }
      ${Media.lessThan(Media.SIZE.XS)} {
        max-height: 120px;
        .image-box img {
          min-height: 25vh !important;
        }
      }
      ${Media.lessThan(Media.SIZE.XXS)} {
        /* min-height: 180px;
        max-height: 180px; */
        max-height: 120px;
      }
      .started-item {
        /* box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1); */
        border: 2px solid rgba(0, 0, 0, 0.1);
        backdrop-filter: blur(24px);
        border-radius: 6px;
        display: flex;
        flex-direction: row;
        border-radius: 20px;
        overflow: hidden;
        transition: all 0.2s;
        margin-bottom: 15px;
        position: relative;
        align-items: center;
        padding: 25px 20px;

        ${Media.lessThan(Media.SIZE.XL)} {
          backdrop-filter: inherit;
        }

        gap: 10px;
        height: 100%;
        .image-box {
          text-align: center;
          display: flex;
          img {
            height: 70px;
            border-radius: 100px;
            width: 70px;
            margin-right: 10px;
          }
          .infor_box {
            display: flex;
            flex-direction: column;
            justify-content: center;
            .item_name {
              font-size: 16px;
              color: #282828;
            }
            .total_items {
              display: flex;
              .item_amount {
                font-size: 14px;
                color: #045afc;
                margin-right: 4px;
              }
              .items {
                font-size: 14px;
                color: #65676b;
              }
            }
          }
        }

        &:hover {
          box-shadow: 0 8px 25px rgba(0, 0, 0, 0.1);
          .comming-soon {
            opacity: 1;
            transition: all 0.2s;
          }
        }
        .content {
          height: 40%;
          position: absolute;
          bottom: 0px;
          display: flex;
          flex-direction: column;
          align-items: flex-start;
          -webkit-box-pack: end;
          justify-content: flex-end;
          width: 100%;
          padding: 30px 22px 16px;
          background: linear-gradient(0deg, rgb(0, 0, 0) 0%, rgba(0, 0, 0, 0) 100%);

          .title {
            padding: 12px 30px 0 0;
            width: 100%;
            color: #fff;
          }
          .description {
            padding: 12px 30px 24px 30px;
            width: 100%;
          }
        }
      }
      .st_item {
        position: absolute;
        color: rgba(40, 40, 40, 0.2);
        font-size: 30px;
        right: 10px;
      }
    }
  }
`;

const Creators = observer(() => {
  const { creators } = useContext(MobXProviderContext);
  const [listCreator, setListCreator] = useState([]);
  const { t, i18n } = useTranslation();
  const history = useHistory();

  useEffect(() => {
    const getCreators = async () => {
      const res = await creators.getListCreatorTop({
        limit: 8,
        page: 1,
        sortBy: 'MOST_WISH',
        langKey: i18n.language.toUpperCase(),
      });

      if (res.success) {
        const data = res.data.result;
        setListCreator(data);
      }
    };

    getCreators();
  }, [creators, i18n.language]);

  const _onViewAllCreator = () => {
    history.push('/creators');
  };

  return (
    <Container>
      <SectionHeading
        title="Top List Creators"
        readmore={t('home:view_all')}
        onClickViewAll={_onViewAllCreator}
        className="title_creator"
      />
      <div className="list-started">
        <Row
          gutter={[
            {
              xs: 15,
              sm: 15,
              md: 20,
              xl: 20,
            },
            {
              xs: 0,
              sm: 0,
              md: 0,
            },
          ]}
        >
          {listCreator.map((item, index) => (
            <Col key={index} xl={6} md={6} sm={12} xs={12} className="col">
              <Link to={`/creator/${item.id}`} className="started-item">
                <div className="image-box">
                  <img src={item.imageUrl} alt={item.name} />
                  <div className="infor_box">
                    <Typography className="item_name">{item.name}</Typography>
                    <div className="total_items">
                      <Typography className="item_amount">{item.totalProduct}</Typography>
                      <Typography className="items">items</Typography>
                    </div>
                  </div>
                </div>

                <Typography className="st_item">{index + 1}</Typography>
              </Link>
            </Col>
          ))}
        </Row>
      </div>
    </Container>
  );
});

export default Creators;
