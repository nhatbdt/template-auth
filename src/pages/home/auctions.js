import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { inject, observer } from 'mobx-react';
import { withTranslation, useTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';

import FetchableList from '../../components/fetchable-list';
import ProductCard from '../../components/product-card-new';
import Media from '../../utils/media';
import SectionHeading from '../../components/section-heading';

const StyledDiv = styled.div`
  padding-bottom: 30px;
  margin-top: 60px;

  ${Media.lessThan(Media.SIZE.MD)} {
    padding-top: 30px;
    margin-top: 30px;
  }

  .container {
    width: 100%;
    padding: 0 20px;
    margin: 0 auto;

    ${Media.lessThan(Media.SIZE.MD)} {
      padding: 0 15px;
    }
  }

  ${Media.greaterThan(Media.SIZE.LG)} {
    .ant-col {
      padding-left: 10px !important;
      padding-right: 10px !important;
    }
  }
  ${Media.greaterThan(Media.SIZE.XXXL)} {
    .ant-col {
      padding-left: 10px !important;
      padding-right: 10px !important;
    }
  }

  &.hidden {
    display: none;
  }
`;

@withTranslation(['home', 'ads'])
@inject(stores => ({
  productsStore: stores.products,
}))
@withRouter
@observer
class Auctions extends Component {
  static propTypes = {
    productsStore: PropTypes.object,
    history: PropTypes.any,
    // i18n: PropTypes.func,
    // t: PropTypes.object,
  };

  _onClickViewAllHeading = url => {
    const { history } = this.props;
    history.push(url);
  };

  _renderProductItem = (item, index = 0, showNumberRanking = false) => {
    const { history } = this.props;
    item = {
      ...item,
      itemColor: '#1E3676',
      backgroundColor: '#F3F4F8',
      statusTextLight: true,
    };

    return (
      <ProductCard
        onClick={() => history.push(`/product-details/${item.id}`)}
        item={item}
        index={index}
        showNumberRanking={showNumberRanking}
        className="product-card"
        isShowCategoryName
        isShowEndTime
      />
    );
  };

  render() {
    const { productsStore, i18n, t } = this.props;
    return (
      <StyledDiv className={`${!productsStore.auctionProduct.items.length && 'hidden'}`}>
        <SectionHeading
          title="Auction Items"
          readmore={t('home:view_all')}
          linkReadmore="/search-products?tab=auction"
          onClickViewAll={this._onClickViewAllHeading}
        />
        <FetchableList
          className="list-top-product"
          ref={ref => {
            this._newProductFetchableList = ref;
          }}
          keyExtractor={(item, index) => index}
          noDataMessage={t('common:no_item')}
          action={productsStore.getAuctionProduct}
          items={productsStore.auctionProduct.items}
          page={1}
          colSpanXs={24}
          colSpanMd={12}
          colSpanXl={6}
          limit={4}
          payload={{
            findType: 'NEW_PRODUCT',
            langKey: i18n.language.toUpperCase(),
            productType: 'AUCTION',
            sortBy: 'LAST_MODIFIED',
          }}
          renderItem={this._renderProductItem}
        />
      </StyledDiv>
    );
  }
}

const AuctionsWrap = props => {
  const { i18n } = useTranslation();

  return <Auctions {...props} key={i18n.language} />;
};

export default AuctionsWrap;
