import React, { memo } from 'react';
import styled from 'styled-components';
import { Row, Col } from 'antd';
import { Link } from 'react-router-dom';

import { Images } from '../../theme';
import SectionHeading from '../../components/section-heading';
import Typography from '../../components/typography';
import Media from '../../utils/media';
import { useTranslation } from 'react-i18next';

const Container = styled.div`
  margin-bottom: 60px;
  margin-top: 70px;
  .heading_get_started {
    margin-bottom: 30px;
    align-items: center;
    justify-content: center;
    h2 {
      color: #282828;
      font-weight: 700;
      font-size: 36px;
      line-height: 36px;
      text-align: center;
      color: #ffffff;
    }
  }
  ${Media.lessThan(Media.SIZE.MD)} {
    margin-bottom: 30px;
  }

  .list-started {
    .started-item {
      backdrop-filter: blur(24px);
      border-radius: 6px;
      display: flex;
      flex-direction: column;
      border-radius: 6px;
      overflow: hidden;
      transition: all 0.2s;
      margin-bottom: 15px;
      position: relative;
      align-items: center;
      ${Media.lessThan(Media.SIZE.XL)} {
        backdrop-filter: inherit;
      }
      .image-box {
        text-align: center;
        /* position: relative; */
        width: 100%;

        img {
          width: 100%;
          object-fit: contain;

          ${Media.lessThan(Media.SIZE.MD)} {
            /* height: 25vh; */
            max-width: 90%;
          }
        }
        .description {
          /* position: absolute; */
          bottom: 0;
          left: 0;
          right: 0;
          text-align: center;
          color: #fff;
          font-weight: 700;
          font-size: 18px;
          line-height: 26px;
          /* padding-bottom: 14px; */
          padding-top: 10px;
          ${Media.lessThan(Media.SIZE.XXL)} {
            padding-bottom: 10px;
          }
          ${Media.lessThan(Media.SIZE.XL)} {
            padding-bottom: 5px;
          }
          ${Media.lessThan(Media.SIZE.LG)} {
            padding-bottom: 18px;
            font-size: 26px;
          }
          ${Media.lessThan(Media.SIZE.LG)} {
            padding-bottom: 17px;
            font-size: 24px;
          }
          ${Media.lessThan(Media.SIZE.MD)} {
            padding-bottom: 8px;
            font-size: 16px;
          }
          ${Media.lessThan(Media.SIZE.SM)} {
            padding-bottom: 4px;
            font-size: 18px;
            line-height: 18px;
          }
          ${Media.lessThan(Media.SIZE.XS)} {
            padding-bottom: 6px;
            font-size: 12px;
            line-height: 12px;
          }
        }
      }
      .comming-soon {
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        left: 0;
        border-radius: 6px;
        background-color: rgba(0, 0, 0, 0.4);
        display: flex;
        align-items: center;
        justify-content: center;
        color: #ffffff;
        font-size: 20px;
        font-weight: bold;
        opacity: 0;
        transition: all 0.2s;
      }
      &:hover {
        box-shadow: 0 8px 25px rgba(0, 0, 0, 0.1);
        .comming-soon {
          opacity: 1;
          transition: all 0.2s;
        }
      }
    }
  }
`;

const ListLetStarted = () => {
  const { t } = useTranslation('home');
  const listGetStarted = [
    {
      image: Images.GET_STARTED_3,
      title: 'MCC',
      url: process.env.REACT_APP_CASINO_URL,
    },
    {
      image: Images.GET_STARTED_1,
      title: t('how_to_started'),
      url: '/guide#2',
    },
    {
      image: Images.GET_STARTED_2,
      title: t('how_to_buy'),
      url: '/guide#3',
    },
    // {
    //   image: Images.GET_STARTED_3,
    //   title: 'BURN the purchased item',
    //   url: '#',
    //   commingSoon: true
    // }
  ];
  return (
    <Container>
      <SectionHeading title={t('let_tarted')} readmore="" className="heading_get_started" />
      <div className="list-started">
        <Row
          gutter={[
            {
              xs: 15,
              sm: 15,
              md: 20,
              xl: 20,
            },
            {
              xs: 0,
              sm: 0,
              md: 0,
            },
          ]}
        >
          {listGetStarted.map((item, index) => (
            <Col key={index} xl={8} lg={8} md={24} sm={24} xs={24}>
              {item.commingSoon ? (
                <div className="started-item" rel="noreferrer">
                  <div className="image-box">
                    <img src={item.image} alt={item.title} />
                  </div>
                  <Typography className="title">{item.title}</Typography>
                  <div className="comming-soon bold">Coming soon…</div>
                </div>
              ) : index ? (
                <Link to={item.url} className="started-item">
                  <div className="image-box">
                    <img src={item.image} alt={item.title} />
                    <Typography className="description">{item.title}</Typography>
                  </div>
                </Link>
              ) : (
                <a href={item.url} target="_blank" rel="noopener noreferrer" className="started-item">
                  <div className="image-box">
                    <img src={item.image} alt={item.title} />
                    <Typography className="description">{item.title}</Typography>
                  </div>
                </a>
              )}
            </Col>
          ))}
        </Row>
      </div>
    </Container>
  );
};

export default memo(ListLetStarted);
