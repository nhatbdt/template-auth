import React, { useContext } from 'react';
import styled from 'styled-components';
import { MobXProviderContext } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import ProductCard from '../../components/product-card-new';
import Media from '../../utils/media';
import SectionHeading from '../../components/section-heading';
import { useHistory } from 'react-router-dom';
import FetchableListWrapper from '../../components/fetchable-list';
import { Colors } from '../../theme';

const StyledDiv = styled.div`
  padding-bottom: 30px;
  margin-top: 75px;
  ${Media.lessThan(Media.SIZE.MD)} {
    padding-top: 30px;
    margin-top: 30px;
  }

  .title {
    font-style: normal;
    font-weight: 700;
    font-size: 36px;
    line-height: 36px;
    color: #282828;
  }

  .sub-title {
    margin-top: -14px;
    margin-bottom: 20px;
    font-style: normal;
    font-weight: 400;
    font-size: 20px;
    line-height: 36px;
    color: ${Colors.TEXT};
  }

  .view-all-btn {
    border-radius: 60px;
    margin: 30px auto;
    background: #045afc;
    min-width: 100px;
  }

  .product-list {
    display: flex;
    justify-content: space-evenly;
    flex-wrap: wrap;
  }
  .product-card-new {
    margin: 0 auto;
    max-width: 480px;
    ${Media.greaterThan(Media.SIZE.LG)} {
      min-width: unset !important;
    }
  }
`;

const NftItems = () => {
  const history = useHistory();
  const { products } = useContext(MobXProviderContext);
  const { t, i18n } = useTranslation(['home', 'ads']);

  const onClickViewAllHeading = url => {
    history.push(url);
  };

  const renderProductItem = (item, index = 0, showNumberRanking = false) => {
    return (
      <ProductCard
        key={`item-${index}`}
        onClick={() => history.push(`/product-details/${item.id}`)}
        item={item}
        index={index}
        className="product-card-new"
        isShowCategoryName
        isShowStatus
        numShowText={40}
      />
    );
  };

  return (
    <StyledDiv>
      <SectionHeading
        title={t('home:new_items')}
        readmore={t('home:view_all')}
        linkReadmore="/search-products"
        onClickViewAll={onClickViewAllHeading}
      />
      <h3 className="sub-title">{t('home:new_items_des')}</h3>

      <FetchableListWrapper
        colSpanXl={6}
        colSpanLg={8}
        colSpanMd={8}
        colSpanSm={24}
        colSpanXs={24}
        noDataMessage={`${t('common:no_data')}`}
        keyExtractor={(item, index) => index}
        action={products.getProductItems}
        items={products.productItems.items}
        total={products.productItems.total}
        page={products.productItems.page}
        limit={12}
        payload={{
          findType: 'NEW_PRODUCT',
          langKey: i18n.language.toUpperCase(),
          sortBy: 'LAST_MODIFIED',
        }}
        renderItem={renderProductItem}
      />
    </StyledDiv>
  );
};

export default NftItems;
