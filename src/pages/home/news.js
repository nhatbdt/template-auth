import React, { useEffect } from 'react';
import styled from 'styled-components';
import { Link, useHistory } from 'react-router-dom';
import { observer } from 'mobx-react';
import { useTranslation } from 'react-i18next';

import SectionHeading from '../../components/section-heading';
import Media from '../../utils/media';
import newsStore from '../../store/news';
import moment from 'moment';
import { Colors } from '../../theme';

const Container = styled.div`
  margin-bottom: 60px;
  margin-top: 0px;
  padding: 0px -50px;

  .title-news {
    padding-top: 50px;
    h2 {
      color: #fff;
      font-weight: 700;
      font-size: 24px;
    }
  }

  ${Media.lessThan(Media.SIZE.MD)} {
    margin-bottom: 30px;

    .title-news {
      padding-top: 20px;
    }
  }

  .list-started {
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: stretch;
    ${Media.lessThan(Media.SIZE.SM)} {
      flex-direction: column;
      justify-content: center;
      align-items: center;
    }

    .item {
      /* margin: 0 auto; */
      margin-bottom: 16px;
      width: calc(100% - 60px / 4);
      max-width: 100%;
      background: #15171c;
      box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);
      border-radius: 20px;
      padding: 10px;

      .thumbnail {
        border-radius: 20px 20px 0 0;
        max-width: 100%;
        width: 100%;
        height: 240px;
        background-repeat: no-repeat;
        background-size: contain;
        background-position: center;
      }

      .content {
        padding: 5px 10px 14px;
        height: calc(100% - 240px + 15px);
        display: flex;
        flex-direction: column;
        justify-content: space-between;

        .title {
          padding-top: 5px;
          font-weight: 700;
          font-size: 20px;
          line-height: 29px;
          color: #fff;
          margin-bottom: 0;

          overflow: hidden;
          text-overflow: ellipsis;
          display: -webkit-box;
          -webkit-box-orient: vertical;
          -webkit-line-clamp: 2; /* limit to 2 lines */
          width: 90%;

          ${Media.lessThan(Media.SIZE.MD)} {
            font-size: 11px;
          }
        }
        .time {
          color: ${Colors.TEXT};
          font-weight: 500;
          font-size: 12px;
          line-height: 17px;
          letter-spacing: 1px;
          text-transform: uppercase;
          ${Media.lessThan(Media.SIZE.MD)} {
            font-size: 10px;
          }
        }
      }
    }
    .item + .item {
      margin-left: 10px;
    }
  }
`;

const News = observer(() => {
  const { t, i18n } = useTranslation();
  const history = useHistory();

  useEffect(() => {
    const initFunc = async () => {
      await newsStore.fetchNews({
        limit: 4,
        page: 1,
        langKey: i18n.language.toUpperCase(),
      });
    };

    initFunc();
  }, [i18n.language]);

  const _onViewAllCreator = () => {
    history.push('/news');
  };

  const { news } = newsStore;

  return (
    <Container>
      {news?.result?.length > 0 && (
        <>
          <SectionHeading
            title={t('home:news')}
            readmore={t('home:view_all')}
            onClickViewAll={_onViewAllCreator}
            className="title-news"
          />
          <div className="list-started">
            {news.result?.map((item, index) => {
              const publicMoment = item?.publicDate ? moment(item?.publicDate).format('MMM DD, YYYY') : '';
              return (
                <Link to={`/news/${item?.id || ''}`} className="item" key={index}>
                  <div
                    className="thumbnail"
                    style={{
                      backgroundImage: `url('${item?.thumbnailUrl}')`,
                    }}
                  />
                  <div className="content">
                    <h3 className="title">{item?.title || ''}</h3>
                    <p className="time">{`${publicMoment}${item?.readTime ? t('common:read_time', { readTime: item?.readTime }) : ''
                      }`}</p>
                  </div>
                </Link>
              );
            })}
          </div>
        </>
      )}
    </Container>
  );
});

export default News;
