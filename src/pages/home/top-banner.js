import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import SwiperCore, { Autoplay } from 'swiper'; // Pagination
import { Swiper, SwiperSlide } from 'swiper/react';
import { inject, observer } from 'mobx-react';
import { withRouter } from 'react-router-dom';

import Banner from '../../components/banner';
import Media from '../../utils/media';
import GachaBanner from '../../components/gacha-banner';

SwiperCore.use([Autoplay]);

const StyledDiv = styled.div`
  width: 74%;
  max-width: calc(100% - 264px - 10px);
  border-radius: 0;

  .swiper-initialized {
    min-height: 400px;
    height: 100% !important;
  }

  .swiper-pagination {
    display: none;
  }

  ${Media.lessThan(Media.SIZE.SM)} {
    width: 100%;
    max-width: unset;
    margin-bottom: 20px;
  }
`;

const GachaBannerStyled = styled.div`
  width: 24%;
  min-width: 264px;
  ${Media.lessThan(Media.SIZE.SM)} {
    display: none;
  }
  &.sm {
    display: none;

    ${Media.lessThan(Media.SIZE.SM)} {
      width: 100%;
      display: block;
    }
  }
`;

const TopBanerStyled = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: stretch;
  margin-top: 60px;
  ${Media.lessThan(Media.SIZE.SM)} {
    margin-top: 16px;
  }
`;

const NoBannerStyled = styled.div`
  color: white;
  border-radius: 20px;
  background: rgba(0, 0, 0, 0.2);
  backdrop-filter: blur(67px);
  background-color: #15171c;
  width: 74%;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 16px;
`;

@inject(stores => ({
  bannersStore: stores.banner,
  productsStore: stores.products,
  gachaStore: stores.gacha,
}))
@withRouter
@observer
class TopBanner extends Component {
  static propTypes = {
    bannersStore: PropTypes.object,
    productsStore: PropTypes.object,
    history: PropTypes.any,
  };

  async componentDidMount() {
    const { bannersStore, gachaStore } = this.props;
    await bannersStore.getBanners();
    await gachaStore.getGacha();
  }

  _onClickDetail = item => {
    if (item.url) {
      window.open(item.url);
    }
  };

  _renderProductItem = item => {
    const { history } = this.props;
    item = {
      ...item,
      itemColor: '#1E3676',
      backgroundColor: '#F3F4F8',
      statusTextLight: true,
    };
    if (!item) {
      return '';
    }
    return <GachaBanner onClick={() => history.push(`/gacha`)} item={item} className="product-card" />;
  };

  render() {
    const { bannersStore, gachaStore } = this.props;
    const { banners } = bannersStore;

    // if (banners.length === 0) return null;

    return (
      <>
        <TopBanerStyled className="top-banner">
          {banners.length ? (
            <StyledDiv>
              <Swiper
                autoplay={{
                  delay: 4000,
                }}
                speed={700}
                onSwiper={ref => {
                  this._swiper = ref;
                }}
                spaceBetween={0}
                slidesPerView={1}
                pagination={{
                  clickable: true,
                }}
                // modules={[Pagination]}
              >
                {banners.map((item, index) => (
                  <SwiperSlide key={index}>
                    <Banner item={item} onClickDetail={this._onClickDetail} hideSeeMoreLink />
                  </SwiperSlide>
                ))}
              </Swiper>
            </StyledDiv>
          ) : (
            <NoBannerStyled>No banner</NoBannerStyled>
          )}
          <GachaBannerStyled>{this._renderProductItem(gachaStore.gachaInfo?.toJSON())}</GachaBannerStyled>
        </TopBanerStyled>
        <GachaBannerStyled className="sm">{this._renderProductItem(gachaStore.gachaInfo?.toJSON())}</GachaBannerStyled>
      </>
    );
  }
}

const TopComponent = props => {
  return <TopBanner {...props} />;
};

export default TopComponent;
