import { types } from 'mobx-state-tree'

import { Model } from '../utils/mobx-model-helper'
import {
  getUserNonce,
  verifySignature,
  login,
  getInitialData,
  updateAccelerationRate
} from '../api/auth'

const TYPES = {
  GET_USER_NONCE: 1,
  VERIFY_SIGNATURE: 2,
  LOGIN: 3,
  GET_INITIAL_DATA: 4,
  UPDATE_ACCELERATION_RATE: 5
}

const Creator = types.model('Creator')
  .props({
    id: types.maybeNull(types.number),
    nftContractAddress: types.maybeNull(types.string),
    name: types.maybeNull(types.string),
    featuredProductNumber: types.maybeNull(types.number),
    accelerationRate: types.maybeNull(types.number),
    accelerationRateCreator: types.maybeNull(types.number)
  })

  const InitialData = types.model('InitialData')
  .props({
    userId: types.maybeNull(types.number),
    publicAddress: types.maybeNull(types.string),
    ethBalance: types.number,
    nbngBalance: types.number,
    usdToJpy: types.number,
    gokuToJpy: types.number,
    wishCount: types.number,
    viewPrivateProduct: types.boolean,
    userImage: types.maybeNull(types.string),
    creator: types.maybeNull(Creator)
  })

  const AuthStore = Model.named('AuthStore')
  .props({
    loggedIn: types.boolean,
    currency: types.string,
    initialData: InitialData
  })
  .actions((self) => ({
    setLoggedIn(value) {
      self.loggedIn = value
    },

    setCurrency(value) {
      self.currency = value
    },

    setInitialData(data) {
      Object.keys(data).forEach((key) => {
        self.initialData[key] = data[key]
      })
    },

    setNbngBalance(value) {
      self.initialData = {
        ...self.initialData,
        nbngBalance: value
      }
    },

    setBalance({ eth }) {
      self.initialData = {
        ...self.initialData,
        ethBalance: eth
      }
    },

    setWishCount(value) {
      self.initialData = {
        ...self.initialData,
        wishCount: self.initialData.wishCount + value
      }
    },

    logout() {
      self.loggedIn = false
      self.initialData = {
        ...self.initialData,
        userId: null,
        publicAddress: null,
        nbngBalance: 0
      }
    },

    getUserNonce(payload) {
      return self.request({
        type: TYPES.GET_USER_NONCE,
        api: getUserNonce,
        payload
      })
    },

    verifySignature(payload) {
      return self.request({
        type: TYPES.VERIFY_SIGNATURE,
        api: verifySignature,
        payload
      })
    },

    login(payload) {
      return self.request({
        type: TYPES.LOGIN,
        api: login,
        payload
      })
    },

    getInitialData(payload) {
      return self.request({
        type: TYPES.GET_INITIAL_DATA,
        api: getInitialData,
        payload,
        onSuccess: (result) => {
          if (!result) return
          self.initialData = {
            ...self.initialData,
            ...result
            // ,
            // creator: result.creator
          }
        }
      })
    },

    updateAccelerationRate(payload) {
      return self.request({
        type: TYPES.UPDATE_ACCELERATION_RATE,
        api: updateAccelerationRate,
        payload
      })
    }
  }))

export {
  TYPES
}
export default AuthStore.create({
  loggedIn: false,
  currency: 'nbng',
  initialData: {
    ethBalance: 0,
    nbngBalance: 0,
    usdToJpy: 0,
    gokuToJpy: 0,
    wishCount: 0,
    viewPrivateProduct: false,
    creator: null
  }
})
