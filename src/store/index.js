import { createBrowserHistory } from 'history';
import lodash from 'lodash';
import { Provider } from 'mobx-react';
import PropTypes from 'prop-types';
import React from 'react';
import { Router } from 'react-router-dom';

// import Configs from '../configs';
import i18n from '../translations/i18n';
import assets from './assets';
import auth from './auth';
import banner from './banners';
import bid from './bid';
import categories from './categories';
import creators from './creators';
import events from './events';
import identities from './identities';
import news from './news';
import offers from './offers';
import payment from './payment';
import products from './products';
import sell from './sell';
import ui from './ui';
import users from './users';

export const history = createBrowserHistory({ basename: process.env.REACT_APP_BASE_NAME });

const { push, replace } = history;

history.push = (path, state) => {
  push(lodash.isObject(path) ? path : `/${i18n.language}${path}`, state);
};

history.replace = (path, state) => {
  replace(lodash.isObject(path) ? path : `/${i18n.language}${path}`, state);
};

const stores = {
  ui,
  assets,
  auth,
  products,
  payment,
  users,
  events,
  bid,
  sell,
  identities,
  offers,
  banner,
  categories,
  creators,
  news
};

const AppProvider = ({ children }) => (
  <Provider {...stores}>
    <Router history={history}>{children}</Router>
  </Provider>
);

AppProvider.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

export default AppProvider;
